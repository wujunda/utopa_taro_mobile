import Taro, { useRouter } from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import JSBridge from '@/utils/jsbridge/index';
import right from '@/assets/special/integral-mall/right.png';
import GoodsCard from '../GoodsCard';

import './index.scss';

const List = (props) => {
  const router = useRouter();
  const crm_channel_id = router.params.crm_channel_id || null;
  const { columns, inApp } = props;
  function toMore(item) {
    const { id, dataType, sortRule } = item;

    if (inApp) {
      let url = window.location.href.split('#');
      let u = url[0];
      let h5Page =
        u +
        '#' +
        `/pages/integral/newList/index?id=${id}&dataType=${dataType}&sortRule=${sortRule}`;
      // 是否隐藏头部
      let appBar = { isHidenNaviBar: false };
      let data = {
        cmd: '',
        url: crm_channel_id ? `${h5Page}&crm_channel_id=${crm_channel_id}` : h5Page,
        h5Params: JSON.stringify(appBar)
      };
      JSBridge.Common.GTBridge_Common_Base_CMD(data, () => {
        console.log('查看栏目');
      });
    } else {
      if (crm_channel_id) {
        Taro.navigateTo({
          url: `/pages/integral/newList/index?id=${id}&dataType=${dataType}&sortRule=${sortRule}&crm_channel_id=${crm_channel_id}`
        });
      } else {
        Taro.navigateTo({
          url: `/pages/integral/newList/index?id=${id}&dataType=${dataType}&sortRule=${sortRule}`
        });
      }
    }
  }

  return (
    <View className="integral-mall-list">
      {!!columns &&
        columns.map((column: any) => (
          <View key={column.id} style={{ overflow: 'hidden', position: 'relative' }}>
            <View className="integral-mall-list-more" onClick={() => toMore(column)}>
              <Text className="integral-mall-list-more-text">更多</Text>
              <Image src={right} className="integral-mall-list-more-icon" />
            </View>
            <View className="integral-mall-list-title">{column.name}</View>
            {!!column.pointProducts &&
              column.pointProducts.map((item, index) => {
                return (
                  index < 4 && (
                    <View
                      style={{
                        display: 'inline-block',
                        marginRight: index % 2 === 0 ? '5px' : '0'
                      }}
                    >
                      <GoodsCard key={item.productId} goods={item} inApp={inApp} />
                    </View>
                  )
                );
              })}
          </View>
        ))}
    </View>
  );
};

export default List;
