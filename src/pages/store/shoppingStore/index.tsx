import Taro, {
  useDidShow,
  usePageScroll,
  useEffect,
  useShareAppMessage,
  useState,
  useRouter,
  useReachBottom
} from '@tarojs/taro';
import { ScrollView, View, Text, Image, Swiper, SwiperItem } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import Wrap from '../../../components/Wrap';
import Tag from '../../../components/Tag';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import BashIt from '../../../components/BashIt';
// import Card from '../../../components/Card';

import Score from '../../../components/Score';
import store_icons_down from '../../../assets/shoppingStore/store_icons_down.png';
import store_tag2 from '../../../assets/shoppingStore/store-tag2.png';
import authentication from '../../../assets/shoppingStore/authentication.png';
// import ShoppingGoods from '../../../components/shopGoods';
import icon_phone_orange from '../../../assets/shoppingStore/icon_phone_orange.png';
import store_tag1 from '../../../assets/shoppingStore/store-tag1.png';
import store_tag3 from '../../../assets/shoppingStore/store-tag3@2x.png';
import store_tag4 from '../../../assets/shoppingStore/store-tag4.png';
import share from '../../../assets/shoppingStore/share.png';
import iconUp from '../../../assets/shoppingStore/store_icons_up.png';
import useSession from '../../../hooks/useSession';
import useRequest from '../../../hooks/useRequest';
import SubScroll from '../components/subScroll';
import { images } from '../../../images';
import mask from '../../../assets/clothingBusiness/mengban.png';
// import location from '../../../assets/shoppingStore/store_loaction@2x.png';
import {
  IStore,
  getStoreActivityList,
  getStoreList,
  getStoreDetail,
  getAdvertisementByStoreId,
  unfavorite,
  favorite,
  // getProductsByBusinessId,
  userGetCoupon
} from '../../../services/store';
import Router from '../../../utils/router';
import { Glo } from '../../../utils/utils';
import './index.scss';

type PageStateProps = {
  number: number;
  statusHeight: number;
  closeWidth: number;
  loading: boolean;
  isLogin: boolean;
  cart: any;
};

type PageDispatchProps = {
  // addCart: (any) => void;
};
type IProps = PageStateProps & PageDispatchProps;
let callBottom = {};
const Index: Taro.FC<IProps> = (props) => {
  const [bHeight, setBHeight] = useState<number>(0);
  useDidShow(() => {
    Taro.getSystemInfo({
      success: function (res) {
        console.log('window高度');
        console.log(res.screenHeight);
        setBHeight(res.screenHeight);
      }
    });
  });
  useReachBottom(() => {
    console.log('到底了');
    console.log('onReachBottom');
    if (callBottom.fn) {
      callBottom.fn();
    }
  });
  const [currentTab, setcurrentTab] = useState(0);
  const [begin, setBegin] = useState(true);
  const [categoryId, setcategoryId] = useState(0);
  const [currentTag, setcurrentTag] = useState(0);
  const [more, setMore] = useState(false);
  const router = useRouter();
  const [haveSession] = useSession();
  // 店铺信息
  let param = {
    storeId: Number(router.params.storeId),
    businessId: Number(router.params.businessId)
  };
  const [state, update, loading] = useRequest<IStore>(param, getStoreDetail);
  if (haveSession && state === null && update && !loading) {
    update();
  }
  const [fixed, setFixed] = useState<boolean>(false);
  const [topHeight, setTopHeight] = useState<number>(0);
  usePageScroll((res) => {
    if (res.scrollTop > topHeight && topHeight != 0 && !fixed) {
      setFixed(true);
    }
    if (res.scrollTop < topHeight && topHeight != 0 && fixed) {
      setFixed(false);
    }
  });
  useEffect(() => {
    if (state) {
      if (state && state.data.store.isFavorite) {
        setLike(true);
      }
      const query = Taro.createSelectorQuery();
      query.select('#topbox1').boundingClientRect();
      query.selectViewport().scrollOffset();
      query.exec(function (res) {
        console.warn('top 高度');
        let height = res[0].height;
        console.warn(height);
        setTopHeight(height);
      });
    }
  }, [state]);

  const [like, setLike] = useState<boolean>(false);
  // 关注 || 取消关注
  const dolike = async () => {
    if (props.isLogin) {
      let res;
      if (state && state.data.store.isFavorite) {
        res = await unfavorite({
          storeId: Number(router.params.storeId),
          favoriteId: state.data.store.id
        });
      } else if (state && !state.data.store.isFavorite) {
        res = await favorite({
          storeId: Number(router.params.storeId),
          favoriteId: state.data.store.id
        });
      }
      if (res.code === 0) {
        setLike(!like);
        Taro.showToast({ title: '操作成功', icon: 'none' });
        if (update) {
          update();
        }
      }
    } else {
      Router.goLogin();
    }
  };
  if (process.env.TARO_ENV === 'weapp') {
    useShareAppMessage((res) => {
      if (res.from === 'button') {
        // 来自页面内转发按钮
        console.log(res.target);
      }
      return {
        title: state ? state.data.store.name : '优托邦GO',
        path: `/pages/store/shoppingStore/index?businessId=${Number(
          router.params.storeId
        )}&storeId=${Number(router.params.businessId)}&storeType=TEMPLATE_THREE`
      };
    });
  }

  // 店铺活动列表
  const [actives, update2, loading2] = useRequest<IStore>(param, getStoreActivityList);
  if (haveSession && actives === null && update2 && !loading2) {
    update2();
  }
  let list = actives && actives.data.items;
  const [activeList, update4, loading4] = useRequest<IStore>(param, getStoreList);
  if (haveSession && activeList === null && update4 && !loading4) {
    update4();
  }
  const [menuHeight, setMenuHeight] = useState<number>(0);
  useEffect(() => {
    if (categoryId || actives) {
      const query = Taro.createSelectorQuery();
      query.select('#utp-shoppingStores-menu1').boundingClientRect();
      query.selectViewport().scrollOffset();
      query.exec(function (res) {
        console.warn('分类top 高度');
        let height = res[0].height;
        console.warn(height);
        setMenuHeight(height);
      });
    }
  }, [categoryId, actives, more]);

  let StoreList = (activeList && activeList.data.list) || [];
  // console.log(StoreList, '-->>');

  if (actives && actives.data.items && actives.data.items.length > 0 && begin) {
    StoreList.unshift({
      id: 57765,
      name: '促销活动',
      children: actives && actives.data.items
    });
    setBegin(false);
  }

  const [advertisements, update3, loading3] = useRequest<IStore>(param, getAdvertisementByStoreId);
  if (haveSession && advertisements === null && update3 && !loading3) {
    update3();
  }

  // 领取优惠券
  const getCoupon = async (payload) => {
    let pay = {
      activityId: payload.activityId,
      businessId: router.params.businessId,
      storeId: router.params.storeId,
      couponCode: payload.couponCode
    };
    let res = await userGetCoupon(pay);
    if (res.code === 0) {
      Taro.showToast({ title: '领取成功', icon: 'none' });
    } else {
      Taro.showToast({ title: res.msg, icon: 'none' });
    }
  };

  return (
    <View className="utp-shoppingStores" id="utp-shoppingStores">
      <View
        className="utp-shoppingStores-top"
        style={{ paddingTop: props.statusHeight + 'px', paddingRight: props.closeWidth + 'px' }}
      >
        <View className="utp-shoppingStores-top-hd" style={{ height: '44px' }}>
          <View
            className="utp-shoppingStores-top-hd-backs utp-cnt"
            onClick={() => {
              Router.goBack();
            }}
          >
            <View className="utp-shoppingStores-top-hd-back">
              <Img src={images.store_back} width={48} />
            </View>
          </View>
          <View
            className="utp-shoppingStores-top-hd-sch utp-cnt"
            onClick={() => {
              console.log('搜索');
              Taro.navigateTo({
                url: '/pages/packOrder/search/index'
              });
            }}
          >
            <Txt title="搜索" size={28} color="low" />
          </View>
          <View
            className="utp-shoppingStores-top-hd-scan utp-cnt"
            onClick={() => {
              Glo.scan();
            }}
          >
            <Txt title="扫码购" size={28} color="black" />
          </View>
          <View className="utp-shoppingStores-top-hd-more">
            <View className="utp-shoppingStores-top-hd-more-gang"></View>
            <View
              className="utp-shoppingStores-top-hd-more-item utp-cnt"
              onClick={() => {
                dolike();
              }}
            >
              {!like && <Img src={images.store_like} width={40} />}
              {like && <Img src={images.store_liked} width={40} />}
            </View>
            <View className="utp-shoppingStores-top-hd-more-item utp-cnt">
              <Img src={images.store_more} width={40} />
            </View>
          </View>
        </View>
      </View>
      <View
        id="topbox1"
        className="utp-shoppingStore"
        style={{ marginTop: props.statusHeight + 44 + 'px' }}
      >
        <View className="utp-shoppingStore-topbox1">
          <Image src={mask} className="utp-shoppingStore-bgMask utp-shoppingStore-bg" />
          <Image
            className="utp-shoppingStore-bgWrap"
            src={state && state.data.store && state.data.store.bgStoreUrl}
          />
          <View className="utp-shoppingStore-storeInfo">
            <Wrap type={3} top>
              <View className="utp-shoppingStore-storeInfo-card">
                <Wrap type={2} justifyContent="flex-start" top>
                  <View
                    className="utp-shoppingStore-titles"
                    onClick={() => {
                      Router.goStoreInfo(
                        router.params.storeId,
                        state && state.data.store.latitude,
                        state && state.data.store.longitude
                      );
                    }}
                  >
                    {!!state &&
                      !!state.data &&
                      !!state.data.store &&
                      !!state.data.store.selfSupport && (
                        <View className="utp-shoppingStore-titles-tag">
                          <Tag>自营</Tag>
                        </View>
                      )}
                    <Text className="utp-shoppingStore-title taro-text">
                      {'\t'}
                      {(state && state.data.store.name) || ''}
                    </Text>
                    <Image src={iconUp} className="utp-shoppingStore-titles-img" />
                  </View>
                  <View className="utp-shoppingStore-storeInfo-score">
                    <Wrap type={2}>
                      <Image
                        src={authentication}
                        className="utp-shoppingStore-storeInfo-score-img"
                      />
                      <Score num={state && state.data.store.star} />
                      {state &&
                        state.data.store.logisticsModel.map((i, index) => {
                          return (
                            <View key={index} className="utp-shoppingStore-lables">
                              <Wrap>
                                <Img
                                  src={
                                    i === '即时达' || i === '周边送'
                                      ? store_tag1
                                      : i === '扫码自取'
                                      ? store_tag3
                                      : i === '门店自提'
                                      ? store_tag4
                                      : i === '全国送'
                                      ? store_tag2
                                      : store_icons_down
                                  }
                                  width={24}
                                />
                                <Txt title={i === '周边送' ? '即时达' : i} />
                              </Wrap>
                            </View>
                          );
                        })}
                    </Wrap>
                  </View>

                  <View className="utp-shoppingStore-advbox">
                    <Wrap justifyContent="space-between">
                      {state &&
                        state.data.store.picUrls.length > 2 &&
                        state.data.store.picUrls.map((i, ii) => {
                          if (ii < 3) {
                            return (
                              <Image src={i} className="utp-shoppingStore-banner-img" key={ii} />
                            );
                          }
                        })}
                    </Wrap>
                    <View>
                      {router.params.storeType !== 'TEMPLATE_TWO' && (
                        <View className="utp-shoppingStore-storeInfo-address">
                          <Wrap type={1} justifyContent="space-between">
                            <Text className="utp-shoppingStore-storeInfo-addressInfo">
                              {(state && state.data.store.address) || ''}
                            </Text>
                            <Wrap>
                              {/* <Img src={location} width={32} /> */}
                              <View className="utp-shoppingStore-storeInfo-centerLine"></View>
                              <Img
                                src={icon_phone_orange}
                                width={32}
                                onClick={() => {
                                  Taro.makePhoneCall({
                                    phoneNumber: state && state.data.store.servicePhone //仅为示例，并非真实的电话号码
                                  });
                                }}
                              ></Img>
                            </Wrap>
                          </Wrap>
                        </View>
                      )}
                      {state && state.data.store.coupons && state.data.store.coupons.length > 0 && (
                        <View className="utp-shoppingStore-advbox-disbox">
                          <Wrap type={2}>
                            {state.data.store.coupons.map((c) => {
                              return (
                                <View
                                  className="utp-shoppingStore-storeInfo-Discount"
                                  key={c.activityId}
                                >
                                  <Wrap type={2}>
                                    <Text className="utp-shoppingStore-storeInfo-Coupon">
                                      {c.couponName}
                                    </Text>
                                    <Text
                                      className="utp-shoppingStore-storeInfo-reduction"
                                      onClick={() => {
                                        getCoupon(c);
                                      }}
                                    >
                                      领券
                                    </Text>
                                  </Wrap>
                                </View>
                              );
                            })}
                          </Wrap>
                        </View>
                      )}
                    </View>
                  </View>
                </Wrap>
                {router.params.storeType === 'TEMPLATE_TWO' && (
                  <View onClick={() => {}} className="utp-shoppingStore-storeIcon">
                    <Img src={state && state.data.store.logoUrl} width={120}></Img>
                  </View>
                )}
              </View>
            </Wrap>
          </View>
        </View>
        <View className="utp-shoppingStore-banner">
          {advertisements &&
            advertisements.data.items &&
            advertisements.data.items[0] &&
            advertisements.data.items[0].advertisementList &&
            advertisements.data.items[0].advertisementList.length === 1 && (
              <Image
                className="utp-shoppingStore-swiper-img1"
                src={advertisements && advertisements.data.items[0].advertisementList[0].image}
              />
            )}
          {advertisements &&
            advertisements.data.items &&
            advertisements.data.items[0] &&
            advertisements.data.items[0].advertisementList &&
            advertisements.data.items[0].advertisementList.length !== 1 && (
              <Swiper
                indicatorColor="#999"
                indicatorActiveColor="#333"
                circular
                indicatorDots
                autoplay
                className="utp-shoppingStore-swipers"
              >
                {advertisements.data.items[0].advertisementList.map((i, ii) => {
                  return (
                    <SwiperItem key={ii} className="utp-shoppingStore-swiper">
                      {true && <Image src={i.image} className="utp-shoppingStore-swiper-img" />}

                      {false && (
                        <View
                          onClick={() => {
                            Router.goHome(i.command);
                          }}
                        >
                          <Image src={i.image} className="utp-shoppingStore-swiper-img" />
                        </View>
                      )}
                    </SwiperItem>
                  );
                })}
              </Swiper>
            )}
        </View>

        <View
          className="utp-shoppingStores-position"
          style={{
            position: fixed ? 'fixed' : 'relative',
            top: fixed ? props.statusHeight + 44 + 'px' : 0
          }}
        >
          <View style={{ position: 'relative' }}>
            <ScrollView
              className="utp-shoppingStore-tabs"
              scrollY
              style={{
                height: bHeight - props.statusHeight - 44 + 'px'
              }}
            >
              <Wrap type={2} flexDirection="column" top>
                {StoreList &&
                  StoreList.map((item, index) => {
                    return (
                      <View
                        key={item.id}
                        className={
                          index === currentTab
                            ? 'utp-shoppingStore-tabs-1-active'
                            : 'utp-shoppingStore-tabs-1'
                        }
                        onClick={() => {
                          setcurrentTag(0);
                          setcurrentTab(index);
                          setBegin(false);
                          setcategoryId(0);
                          setTimeout(() => {
                            setcategoryId(item.foreCategoryId);
                          }, 0);
                        }}
                      >
                        <Wrap type={1}>
                          <Text className="utp-shoppingStore-tabs-text">{item.name}</Text>
                        </Wrap>
                        <View
                          className="utp-shoppingStore-tabs-prefix"
                          style={{ display: index === currentTab ? '' : 'none' }}
                        ></View>
                      </View>
                    );
                  })}
              </Wrap>
            </ScrollView>
          </View>
          <View className="utp-shoppingStores-menu1" id="utp-shoppingStores-menu1">
            <Wrap type={1} flexWrap="wrap">
              {categoryId
                ? StoreList[currentTab].children.map((i, inm) => {
                    if (inm < 5 && more == false) {
                      return (
                        <Text
                          key={i.foreCategoryId}
                          className={
                            inm === currentTag
                              ? 'utp-shoppingStore-mytabsList-goodstag-active'
                              : 'utp-shoppingStore-mytabsList-goodstag'
                          }
                          onClick={() => {
                            setcurrentTag(inm);
                            setcategoryId(0);
                            setTimeout(() => {
                              setcategoryId(i.foreCategoryId);
                            }, 0);
                          }}
                        >
                          {i.name}
                        </Text>
                      );
                    } else if (more) {
                      return (
                        <Text
                          key={i.foreCategoryId}
                          className={
                            inm === currentTag
                              ? 'utp-shoppingStore-mytabsList-goodstag-active'
                              : 'utp-shoppingStore-mytabsList-goodstag'
                          }
                          onClick={() => {
                            setcurrentTag(inm);
                            setcategoryId(0);
                            setTimeout(() => {
                              setcategoryId(i.foreCategoryId);
                            }, 0);
                          }}
                        >
                          {i.name}
                        </Text>
                      );
                    }
                  })
                : actives &&
                  actives.data.items &&
                  actives.data.items.map((item, tagindex) => {
                    if (tagindex < 3) {
                      return (
                        <Text
                          key={item.activityId}
                          className={
                            tagindex === currentTag
                              ? 'utp-shoppingStore-mytabsList-goodstag-active'
                              : 'utp-shoppingStore-mytabsList-goodstag'
                          }
                          onClick={() => {
                            setcurrentTag(tagindex);
                            setcategoryId(0);
                          }}
                        >
                          {item.label}
                        </Text>
                      );
                    }
                  })}

              {!!categoryId && StoreList[currentTab].children.length > 5 ? (
                <Text
                  className="utp-shoppingStore-mytabsList-goodstag"
                  onClick={() => {
                    //展开更多
                    setMore(!more);
                  }}
                >
                  {more ? '收起' : '更多'}
                </Text>
              ) : null}
            </Wrap>
          </View>
        </View>
        <View className="utp-shoppingStore-tabList">
          <Wrap type={1} top justifyContent="space-between">
            <View className="utp-shoppingStore-tabListScroll">
              <View className="utp-shoppingStore-mytabsList">
                <Wrap type={1} flexDirection="column">
                  <View
                    className="utp-shoppingStore-topbox"
                    style={{
                      minHeight: bHeight - props.statusHeight - 44 + 'px'
                    }}
                  >
                    <Wrap type={1} flexWrap="wrap">
                      <View
                        className="utp-shoppingStore-mytabsList-subbox"
                        style={{
                          marginTop: menuHeight + 'px'
                        }}
                      >
                        {!categoryId && !list && (
                          <SubScroll
                            callBottom={callBottom}
                            data={{
                              ...param,
                              categoryId: StoreList[0] && StoreList[0].foreCategoryId
                            }}
                          />
                        )}
                        {!!categoryId && (
                          <SubScroll callBottom={callBottom} data={{ ...param, categoryId }} />
                        )}
                        {!categoryId && list && list.length > 0 && (
                          <SubScroll
                            callBottom={callBottom}
                            defaultData={list}
                            index={currentTag}
                          />
                        )}
                      </View>
                    </Wrap>
                  </View>
                  <View></View>
                </Wrap>
              </View>
            </View>
          </Wrap>
        </View>
      </View>

      <View
        className="utp-shoppingStore-shopCart"
        onClick={() => {
          Taro.switchTab({
            url: '/pages/cart/index'
          });
        }}
      >
        <BashIt
          top={0}
          bottom={0}
          wrapTop={24}
          src={share}
          srcWidth={128}
          tips=""
          tipsHeight={20}
          tipsColor="#333333"
          tipsSize={18}
          num={props.number}
          numType={1}
        ></BashIt>
      </View>
    </View>
  );
};
Index.config = {
  navigationBarTitleText: '店铺',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5',
  // 不显示标题
  navigationStyle: 'custom'
};

const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight,
    closeWidth: state.cart.closeWidth,
    number: state.cart.number,
    post: state.cart.post,
    cart: state.cart,
    isLogin: state.cart.isLogin,
    loading: state.loading.effects['cart/addCart']
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};
// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
