import request from '../utils/request';

export interface ICouponItem {
  couponName: string;
  couponCode: string;
  couponType: number; //优惠券类型 1:满减券 2：折扣券
  conditionInfo: string; //优惠券条件信息 如:满xx减 yy元
  preferentialInfo: string; //优惠信息 如: ￥400
  businessName: string; //商户名称 (优惠券说明中显示)
  validTimeScope: string; //优惠券有效期
  useStatus: number; //使用状态 1：待使用 2：已使用 3：已过期  4：已失效
  couponTag: number; //	券标识  0:普通券 1：新人券 …
  conditionValue: number; //满减等条件
  couponValue: number; //优惠额
  businessCode: string; //商家编号
  obtainStatus:number;//领取状态 1:待领取 2：已领取 3：已领完
}
interface List {
  data: ICouponItem[];
  total: number;
}
export interface ICouponsList {
  data: {
    notUseData: List; //未使用优惠券
    usedData: List; //已使用优惠券
    outTimeData: List; //已过期优惠券
  };
}
// 登陆
export const myCouponList = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.coupon.myCouponList',
    ver: '1.0',
    params: JSON.stringify({
      pageNo: 1,
      pageSize: 20,
      status: ''
    })
  });
};
