import Taro from '@tarojs/taro';
import Api from './api';
import {
  getCart1,
  getCart,
  getChangeGoodsNumber,
  getChangeGoodsSku,
  getCheckGoods,
  getCheckStore,
  getCheckCart,
  deleteShopCartPro
} from '../services/cart';
import { getSession } from '../services/user';
import {
  getWaitSubmit1,
  getWaitSubmit,
  goAddCart,
  totalNumber,
  getCustomCategoryList
} from '../services/submit';
import Router from '../utils/router';
import { getMsgMenu } from '../services/msg';

export default {
  namespace: 'cart',

  // 数据
  state: {
    // 购物车
    carts: {},
    cartList: [],
    // 扫码购物
    carts1: {},
    cartList1: [],
    // 提交的商品数据
    submit: null,
    post: null,
    posts: [],
    categoryList: [],
    number: '',
    //
    session: '',
    isLogin: '',
    // 小程序状态栏高度
    statusHeight: 20,
    closeWidth: 90,
    unread: 0,
    icons: [],
    x: '',
    y: '',
    //x: 23.12463,
    //y: 113.36199,
    adsTitle: ''
  },

  // 订阅
  subscriptions: {},

  // 异步
  effects: {
    *savePostss(_, { put }) {
      yield put({
        type: 'savePosts',
        payload: _.payload
      });
      //Router.goBack();
    },
    *deleteShopCartPro(_, { call, put }) {
      let data = yield call(deleteShopCartPro, {ids:_.payload});
      if (!!data && data.code === 0) {
        Taro.showToast({
          title: '操作成功'
        });
        yield put({
          type: _.isScan ? 'syncCart1' : 'syncCart'
        });
      }
    },
    *addCart(_, { call, put }) {
      let data = yield call(goAddCart, _.payload);
      if (!!data && data.code === 0) {
        // 计算店铺购物车数量
        Taro.showToast({
          title: '添加购物车成功'
        });
        // 店铺商品添加商品计算总和
        if (_.payload.need) {
          yield put({
            type: 'getTotalNumber',
            payload: _.payload.storeId
          });
        }
        yield put({
          type: 'syncCart',
          payload: {}
        });
      } else {
        Taro.showToast({
          title: data.msg,
          icon: 'none'
        });
      }
    },
    *getCategoryList(_, { call, put }) {
      let payload = {
        useType: 1,
        accessToken: Taro.getStorageSync('login'),
        uid: Taro.getStorageSync('uid'),
        clientType: 1,
        sysId: 1,
        foreCategoryId: 2,
        pageSize: 20,
        pageNo: 1
      };
      let data = yield call(getCustomCategoryList, payload);
      if (!!data && data.code === 0) {
        yield put({
          type: 'saveCategoryList',
          payload: {
            data: data
          }
        });
      }
    },
    *getUnread(_, { call, put }) {
      let data = yield call(getMsgMenu);
      // console.log('获取到的session');
      // console.log(data);
      // console.log(data.data.sessionSecret);
      if (!!data && data.code === 0) {
        let rst = 0;
        data.data.records.map((item) => {
          rst += item.unreadCount;
        });
        yield put({
          type: 'saveUnread',
          payload: {
            data: rst
          }
        });
      }
    },

    *getSession(_, { call, put }) {
      return;
      let data = yield call(getSession);
      // console.log('获取到的session');
      // console.log(data);
      // console.log(data.data.sessionSecret);
      if (!!data && data.code === 0) {
        yield put({
          type: 'saveSession',
          payload: {
            data: data
          }
        });
      }
    },

    *getTotalNumber(_, { call, put }) {
      let data = yield call(totalNumber, _.payload);
      if (!!data && data.code === 0) {
        yield put({
          type: 'saveNumber',
          payload: {
            data: data
          }
        });
      }
    },
    *getSubmit(_, { call, put }) {
      let data = yield call(_.payload.isScan ? getWaitSubmit1 : getWaitSubmit, _.payload.post);
      if (!!data && data.code === 0) {
        yield put({
          type: 'saveSubmit',
          payload: {
            data: data
          }
        });
        if (_.payload.type === 1 && !_.payload.isScan) {
          Router.goSubmit();
        } else if (_.payload.type === 2) {
          Router.goBack();
        } else if (_.payload.isScan) {
          Router.goSubmit1();
        }
      } else {
        Taro.showToast({
          title: data.msg,
          icon: 'none'
        });
      }
    },

    *syncCart(_, { call, put }) {
      let data = yield call(getCart, { name: '我是异步数据' });
      if (!!data && data.code === 0) {
        let num = 0;

        if (data.data.totalNumber) {
          num = data.data.totalNumber;
        }
        if (num) {
          Taro.setTabBarBadge({
            index: 2,
            text: num.toString()
          });
        }
        yield put({
          type: 'saveCart',
          payload: {
            cart: data.data,
            code: data.code
          }
        });
      }
    },
    *syncCart1(_, { call, put }) {
      let data = yield call(getCart1, { name: '我是异步数据' });
      if (!!data && data.code === 0) {
        yield put({
          type: 'saveCart1',
          payload: {
            cart: data.data,
            code: data.code
          }
        });
      }
    },

    *changeGoods(_, { call, put }) {
      let data = yield call(getChangeGoodsNumber, _.payload);
      if (!!data && data.code === 0) {
        yield put({
          type: _.isScan ? 'syncCart1' : 'syncCart'
        });
      }
    },
    *changeGoodsSku(_, { call, put }) {
      let data = yield call(getChangeGoodsSku, _.payload);
      if (!!data && data.code === 0) {
        yield put({
          type: 'syncCart'
        });
      }
    },

    *checkGoods(_, { call, put }) {
      console.warn('选择扫码商品');
      console.warn(_.isScan);
      let data = yield call(getCheckGoods, _.payload);
      if (!!data && data.code === 0) {
        yield put({
          type: _.isScan ? 'syncCart1' : 'syncCart'
        });
      }
    },
    *checkStore(_, { call, put }) {
      console.warn('切换店铺');
      console.warn(_.isScan);
      let data = yield call(getCheckStore, _.payload);
      if (!!data && data.code === 0) {
        yield put({
          type: _.isScan ? 'syncCart1' : 'syncCart'
        });
      }
    },
    *checkCart(_, { call, put }) {
      let data = yield call(getCheckCart, _.payload);
      if (!!data && data.code === 0) {
        yield put({
          type: 'syncCart'
        });
      }
    },


    *wait(_, { call, put }) {
      let str = yield call(Api.api, { name: '我是异步数据' });
      yield put({
        type: 'save',
        payload: {
          name: str
        }
      });
    }
  },

  // 同步
  reducers: {
    updateCart(state) {
      return {
        ...state,
        cartList: state.cartList,
        carts: state.carts
      };
    },
    saveCart1(state, { payload }) {
      return {
        ...state,
        cartList1: payload.cart.shopCartDtos,
        carts1: {
          actuallyAmount: payload.cart.actuallyAmount,
          isAllSelect: payload.cart.isAllSelect,
          originalAmount: payload.cart.originalAmount,
          percentFee: payload.cart.percentFee,
          selectTotalNumber: payload.cart.selectTotalNumber,
          totalAmount: payload.cart.totalAmount,
          totalNumber: payload.cart.totalNumber,
          code: payload.code
        }
      };
    },

    saveCart(state, { payload }) {
      return {
        ...state,
        cartList: payload.cart.shopCartDtos,
        carts: {
          actuallyAmount: payload.cart.actuallyAmount,
          isAllSelect: payload.cart.isAllSelect,
          originalAmount: payload.cart.originalAmount,
          percentFee: payload.cart.percentFee,
          selectTotalNumber: payload.cart.selectTotalNumber,
          totalAmount: payload.cart.totalAmount,
          totalNumber: payload.cart.totalNumber,
          code: payload.code
        }
      };
    },
    saveSubmit(state, { payload }) {
      // console.log('保存订单数据');
      // console.log(payload.data);
      return {
        ...state,
        submit: payload.data
      };
    },
    savePosts1(state, { payload }) {
      return {
        ...state,
        posts: payload.data
      };
    },

    savePosts(state, { payload }) {
      let arr: any = [].concat(state.posts);
      let obj: any = payload.data;
      let have = false;
      arr.map((item) => {
        if (item.storeId === obj.storeId) {
          item = Object.assign(item, obj);
          have = true;
        }
      });
      if (!have) {
        arr.push(obj);
      }
      // console.log('保存的店铺数据');
      // console.log(arr);
      return {
        ...state,
        posts: arr
      };
    },
    savePost(state, { payload }) {
      return {
        ...state,
        post: payload.data
      };
    },
    saveSession(state, { payload }) {
      // console.log('保存SSSSSS');
      return {
        ...state,
        session: payload.data
      };
    },
    saveLogin(state, { payload }) {
      // console.log('登陆信息');
      return {
        ...state,
        isLogin: payload.data
      };
    },
    logout(state, {}) {
      Taro.removeTabBarBadge({
        index: 2
      });
      return {
        ...state,
        isLogin: ''
      };
    },
    saveStatusHeight(state, { height }) {
      // console.log('HHHHHHHHHHH');
      // console.log(height);
      return {
        ...state,
        statusHeight: height
      };
    },
    saveUnread(state, { payload }) {
      return {
        ...state,
        unread: payload.data
      };
    },

    saveNumber(state, { payload }) {
      return {
        ...state,
        number: payload.data.data.number
      };
    },
    saveXy(state, { payload }) {
      console.warn('icons数据');
      console.warn(payload);
      return {
        ...state,
        x: payload.x,
        y: payload.y
      };
    },

    saveIcons(state, { payload }) {
      console.warn('icons数据');
      console.warn(payload);
      return {
        ...state,
        icons: payload.icons
      };
    },
    saveAdsTitle(state, { payload }) {
      console.log('保存地址');
      console.log(payload);
      return {
        ...state,
        adsTitle: payload.adsTitle
      };
    },

    submitBefore(state) {
      return {
        ...state,
        submit: null,
        post: null,
        posts: []
      };
    },
    saveCategoryList(state, { payload }) {
      return {
        ...state,
        categoryList: payload.data.data.records
      };
    }
  }
};
