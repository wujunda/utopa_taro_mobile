import Taro from '@tarojs/taro';
import { ScrollView } from '@tarojs/components';

interface Props {
  scrollY: boolean;

  scrollX: boolean;
}

const GoodsContainer: Taro.FC<Props> = ({ children, scrollX, scrollY }) => {
  let component;

  if (scrollY || scrollX) {
    component = (
      <ScrollView scrollWithAnimation scrollX={scrollX} scrollY={scrollY}>
        {children}
      </ScrollView>
    );
  } else {
    component = children;
  }
  return component;
};

export default GoodsContainer;
