import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

import Img from '../Img';
import Wrap from '../Wrap';
import startActive from '@/assets/info/startActive.png';
import start from '@/assets/info/start.png';
import './index.scss';

interface IProps {
  num?: number;
  width?: number;
}
const Index: Taro.FC<IProps> = ({ num = 4, width = 18 }) => {
  let arr = [1, 2, 3, 4, 5];
  // console.log(arr);
  // console.log(num);
  return (
    <View className="utp-score">
      <Wrap>
        <Wrap>
          {arr.map((item, index) => {
            return (
              <View className="utp-score-box" key={index}>
                {item <= num && <Img src={startActive} width={width} />}
                {item > num && <Img src={start} width={width} />}
              </View>
            );
          })}
        </Wrap>
        <View></View>
      </Wrap>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
