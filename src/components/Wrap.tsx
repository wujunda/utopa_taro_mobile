import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

interface Props {
  children: any;
  type?: 1 | 2 | 3 | 4 | 5 | 6;
  top?: boolean;
  flexDirection?: 'row' | 'column';
  flexWrap?: 'nowrap' | 'wrap';
  Myheight?: number;
  click?: () => void;
  className?: string;
  justifyContent?: 'flex-start' | 'flex-end' | 'center' | 'space-between';
  alignItems?: 'flex-start' | 'flex-end' | 'center';
}

const Index: Taro.FC<Props> = ({
  children,
  type,
  top = false,
  flexDirection = 'row',
  flexWrap = 'wrap',
  Myheight,
  click,
  className,
  justifyContent = 'flex-start',
  alignItems = 'center'
}) => {
  let styles: any = {
    display: 'flex',
    flexDirection: flexDirection,
    justifyContent: justifyContent,
    alignItems: top ? 'flex-start' : alignItems,
    flexWrap: flexWrap,
    height: Myheight ? Taro.pxTransform(Myheight) : 'auto'
  };
  if (type === 1) {
    styles.height = '100%';
  }
  if (type === 2) {
    styles.justifyContent = 'flex-start';
  }
  if (type === 3) {
    styles.justifyContent = 'space-around';
    styles.height = '100%';
  }
  if (type === 4) {
    styles.justifyContent = 'center';
    styles.height = '100%';
  }
  if (type === 5) {
    styles.justifyContent = 'flex-end';
  }
  if (type === 6) {
    styles.justifyContent = 'flex-start';
    styles.height = '100%';
  }
  return (
    <View
      style={styles}
      onClick={() => {
        if (click) {
          click();
        }
      }}
      className={className}
    >
      {children}
    </View>
  );
};
Index.defaultProps = {};

export default Index;
