import Taro, { useState } from '@tarojs/taro';
import { View, ScrollView, Text } from '@tarojs/components';
//import Bar from '../../../components/bar';
import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Img from '../../../components/Img';
import Txt from '../../../components/Txt';
import goods from '../../../assets/goods.png';
import icon_edit from '../../../assets/mine/icon_edit.png';
import right from '../../../assets/right.png';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import useSession from '../../../hooks/useSession';
import useLogin from '../../../hooks/useLogin';
import useResetCount from '../../../hooks/useResetCount';
import { findPageUserWishe, ProductsType, IMsgInfo } from '../../../services/wishList';
import icon_jingqingqidai from '../../../assets/mine/icon_jingqingqidai.png';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  // console.log('注册注册');
  const [haveSession] = useSession();
  const [isLogin, updateLogin] = useLogin();
  if (haveSession && !isLogin) {
    if (updateLogin) {
      updateLogin();
    }
  }
  // 购物车数据
  // @ts-ignore
  const [state, hasMore, update, getMoreData, loading] = useRequestWIthMore<IHistory>(
    {},
    findPageUserWishe
  );
  const [can, update1] = useResetCount();
  // console.log(state, 'steate');
  if (
    haveSession &&
    isLogin &&
    (state === null || (state != null && state.length == 0)) &&
    update &&
    !loading
  ) {
    if (can) {
      update({});
      update1();
    }
  }

  var lists: IMsgInfo[] = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.data.records.map((it) => {
        lists.push(it);
      });
    });
  }
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          {loading && lists.length === 0 && <Loading />}
          <View className="utp-bd utp-bd-page utp-wishList">
            <View className="utp-wishList-create">
              {lists.length !== 0 &&
                lists.map((i, ii) => {
                  return (
                    <Wrap flexDirection="column" key={ii}>
                      <View
                        className="utp-wishList-goodsInfo"
                        onClick={() => {
                          Taro.navigateTo({
                            url: `/pages/wishList/addWishList/index?id=${i.id}`
                          });
                        }}
                      >
                        <Text className="utp-wishList-goodsInfo-wishTitle">{i.title}</Text>
                        <Wrap type={1} justifyContent="space-between">
                          <Wrap>
                            {i &&i.products &&i.products.map((u, uu) => {
                              return (
                                <View className="utp-wishList-goodsInfo-imgs" key={uu}>
                                  <Img src={u.skuPic != null && u.skuPic} width={100} />
                                </View>
                              );
                            })}
                          </Wrap>
                          <Wrap>
                            <View className="utp-wishList-goodsInfo-tips">
                              <Txt title={`共${i.totalCount}件`} />
                            </View>
                            <Img src={right} width={44} />
                          </Wrap>
                        </Wrap>
                      </View>
                    </Wrap>
                  );
                })}
              {lists.length === 0 && (
                <Wrap type={4} flexDirection="column">
                  <Img src={icon_jingqingqidai} width={400} height={300}></Img>
                  {false && (
                    <Text style={{ fontSize: 15, color: '#D1D1D1' }}>先创建一个心愿单吧~</Text>
                  )}
                  {true && <Text style={{ fontSize: 15, color: '#D1D1D1' }}>没有心愿单</Text>}
                </Wrap>
              )}
            </View>

              <View
                className="utp-wishList-create-icon_edit"
                onClick={() => {
                  Taro.navigateTo({
                    url: '/pages/wishList/addWishList/index'
                  });
                }}
              >
                <Img src={icon_edit} width={140}></Img>
              </View>
      
            {loading && lists.length !== 0 && <LoadingMore />}
            {!loading && !hasMore && lists.length > 0 && <LoadingNoneMore />}
          </View>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '我的心愿单',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};
export default Index;
