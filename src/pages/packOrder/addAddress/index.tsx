import Taro, { useDidShow, useState, useEffect, useRouter } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { Picker, View, Input, Switch } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import useStore from '../../../components/useNum';
import Box1 from '../../../components/Box1';
import Bton from '../../../components/Bton';
import Wrap from '../../../components/Wrap';
import Img from '../../../components/Img';
import Txt from '../../../components/Txt';
import Cnt from '../../../components/Cnt';
import right from '../../../assets/right.png';
import './index.scss';
import {
  getDelAddress,
  getEditAddress,
  getAddAddress,
  IAds,
  ISsq,
  getSsq
} from '../../../services/address';
import useRequest from '../../../hooks/useRequest';
import useSession from '../../../hooks/useSession';
import useLogin from '../../../hooks/useLogin';
import useResetCount from '../../../hooks/useResetCount';
import Router from '../../../utils/router';
import { Glo } from '../../../utils/utils';

function Index(props) {
  const [haveSession] = useSession();
  const [isLogin, updateLogin, lodingLoading] = useLogin();
  const [canL, updateL] = useResetCount();
  if (haveSession && !isLogin && !lodingLoading) {
    if (updateLogin) {
      if (canL) {
        updateLogin();
        updateL();
      }
    }
  }
  console.warn('是否登陆');
  console.warn(isLogin);
  // 用户数据
  const [ssq, update, loading] = useRequest<ISsq>('name=a&age=1', getSsq);
  const [can, update1] = useResetCount();
  if (
    haveSession &&
    isLogin &&
    (ssq === null || (ssq != null && ssq.code !== 0)) &&
    update &&
    !loading
  ) {
    if (can) {
      update();
      update1();
      // console.log(ssq);
    }
  }

  let num = useStore({ name: '' })[0];
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [number, setNumber] = useState('');
  const [def, setDef] = useState(true);
  const [tab, setTab] = useState('');
  const [tab1, setTab1] = useState('');
  const [ads, setAds] = useState<IAds | any>('');
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const [tabs] = useState(['家', '公司', '学校', '常用']);
  const [state, setState] = useState<any>({
    selector: [],
    selectorChecked: []
  });
  // 选择省市区
  const [crtSsq, setCrtSsq] = useState('');
  const [objs, setObjs] = useState<any>([]);
  if (state.selector.length === 0 && ssq != null && ssq.code === 0) {
    let obj = Object.assign({}, state);
    obj.selector[0] = ssq.data.areas[0].children;
    obj.selector[1] = ssq.data.areas[0].children[0].children;
    obj.selector[2] = ssq.data.areas[0].children[0].children[0].children;
  }
  // console.log('jjjjj');
  console.log(num);
  // console.log('props');
  console.log(props);
  let [crt, setCrt] = useState(-1);
  const [demo, setDemo] = useState(0);
  useEffect(() => {
    // console.log('demob变了');
  }, [demo]);
  const onColumnChange = (e) => {
    // console.log(e);
    let obj = Object.assign({}, state);
    if (ssq) {
      ssq.data.areas[0].children.map((item, index) => {
        // 改变第二列
        if (e.detail.column === 0 && index === e.detail.value) {
          obj.selector[1] = item.children;
          obj.selector[2] = item.children[0].children;
          setCrt(index);
        }
        // 改变第三列
        // console.log(333);
        // console.log(crt);
        if (e.detail.column === 1 && crt === index) {
          // console.log(obj.selectorChecked);
          item.children.map((item1, index1) => {
            if (index1 === e.detail.value) {
              obj.selector[2] = item1.children;
            }
          });
        }
      });
      setState(obj);
    }
  };
  const onChange = (e) => {
    // console.log(e);
    let obj = Object.assign({}, state);
    obj.selectorChecked = e.detail.value;
    setState(obj);
    let arr: any = [];
    arr.push(state.selector[0][e.detail.value[0]]);
    arr.push(state.selector[1][e.detail.value[1]]);
    arr.push(state.selector[2][e.detail.value[2]]);
    let str =
      state.selector[0][e.detail.value[0]].name +
      ' ' +
      state.selector[1][e.detail.value[1]].name +
      ' ' +
      state.selector[2][e.detail.value[2]].name;

    setCrtSsq(str);
    setObjs(arr);
    Taro.setStorage({
      key: 'LoctionObj',
      data: arr
    });

    Taro.setStorage({
      key: 'LoctionStr',
      data: str
    });

    Taro.setStorage({
      key: 'LoctionAds',
      data: ''
    });
    setAds('');
  };
  const [crtIndex, setCrtIndex] = useState<any>([]);
  const onDel = async () => {
    let rst = await getDelAddress({
      id: id
    });
    if (rst.code === 0) {
      Taro.setStorage({
        key: 'addressLoading',
        data: '1'
      });
      Router.goBack();
    }
  };

  const onSubmit = async () => {
    if (!name) {
      Taro.showToast({
        title: '请填写收货人姓名',
        icon: 'none'
      });
      return;
    }
    if (!Glo.isPhone(phone, 86)) {
      Taro.showToast({
        title: '请填写正确的收货人手机号',
        icon: 'none'
      });
      return;
    }
    if (objs.length === 0) {
      Taro.showToast({
        title: '请选择所在地区',
        icon: 'none'
      });
      return;
    }

    if (!ads) {
      Taro.showToast({
        title: '请选择收货地址',
        icon: 'none'
      });
      return;
    }
    let rst: any;
    if (isEdit) {
      rst = await getEditAddress({
        addressType: show1 && tab1 ? tab1 : tab,
        cityId: objs[1].code,
        clientType: 1,
        detailAddress: number,
        districtId: objs[2].code,
        isDefault: def ? 1 : 0,
        latitude: ads.point.lat,
        longitude: ads.point.lng,
        mapAddress: ads.title,
        phone: phone,
        provinceId: objs[0].code,
        sysId: 1,
        userName: name,
        id: id
      });
    } else {
      rst = await getAddAddress({
        addressType: show1 && tab1 ? tab1 : tab,
        cityId: objs[1].code,
        clientType: 1,
        detailAddress: number,
        districtId: objs[2].code,
        isDefault: def ? 1 : 0,
        latitude: ads.point.lat,
        longitude: ads.point.lng,
        mapAddress: ads.title,
        phone: phone,
        provinceId: objs[0].code,
        sysId: 1,
        userName: name
      });
    }
    if (rst.code === 0) {
      Taro.setStorage({
        key: 'address',
        data: rst
      });

      Taro.setStorage({
        key: 'addressLoading',
        data: '1'
      });
      Router.goBack();
    }
  };
  const router = useRouter();
  const [isEdit, setIsEdit] = useState(false);
  const [id, setId] = useState('');
  useEffect(() => {
    if (isEdit && objs.length === 0 && ssq && ssq.code === 0) {
      // console.log('编辑地址还原省市区');
      // console.log(ssq);
      // console.log(crtIndex);
      let obj = Object.assign({}, state);
      let str = '';
      let arr: any = [];
      if (ssq) {
        ssq.data.areas[0].children.map((item, index) => {
          // 改变第二列
          if (item.code === crtIndex[0]) {
            arr.push(item);
            obj.selectorChecked[0] = index;
            str = str + item.name + ' ';
            // console.log('地址1');
            // console.log(index);
            obj.selector[1] = item.children;
            //obj.selector[2] = item.children[0].children;
            setCrt(index);
            // 改变第三列
            // console.log(333);
            // console.log(crt);
            // console.log(obj.selectorChecked);
            item.children.map((item1, index1) => {
              if (item1.code === crtIndex[1]) {
                arr.push(item1);
                str = str + item1.name + ' ';
                obj.selectorChecked[1] = index1;
                // console.log('地址2');
                // console.log(index1);
                obj.selector[2] = item1.children;
                item1.children.map((item2, index2) => {
                  if (item2.code === crtIndex[2]) {
                    arr.push(item2);
                    obj.selectorChecked[2] = index2;
                    str = str + item2.name;
                  }
                });
              }
            });
          }
        });
        setObjs(arr);
        setCrtSsq(str);

        setState(obj);
      }
    }
  }, [isEdit, ssq]);
  // console.log('路由信息');
  // console.log(router);
  useDidShow(() => {
    var str = Taro.getStorageSync('LoctionStr');
    if (str) {
      var arr = Taro.getStorageSync('LoctionObj');
      setCrtSsq(str);
      setObjs(arr);
    }
    var ads = Taro.getStorageSync('LoctionAds');
    setAds(ads);
    var editAddress = Taro.getStorageSync('addressEdit');
    if (editAddress && !ads) {
      setId(editAddress.id);
      setCrtIndex([editAddress.provinceId, editAddress.cityId, editAddress.districtId]);
      setIsEdit(true);
      // console.log('编辑地址还原省市区1');
      // console.log(crtIndex);
      setName(editAddress.userName);
      setPhone(editAddress.phone);
      setNumber(editAddress.detailAddress ? editAddress.detailAddress : '');
      setDef(editAddress.isDefault === 1 ? true : false);
      let eTab = editAddress.addressType;
      setAds({
        title: editAddress.mapAddress,
        point: {
          lat: editAddress.latitude,
          lng: editAddress.longitude
        }
      });
      if (eTab) {
        let isTab = false;
        tabs.map((item) => {
          if (eTab === item) {
            setTab(eTab);
            isTab = true;
          }
        });
        if (!isTab) {
          setTab1(eTab);
          setShow1(true);
        }
      }
    }
  });

  return (
    <View className="utp-page-addAddress">
      <Box1 type={1}>
        <View className="utp-page-addAddress-item utp-solid">
          <Wrap type={1}>
            <View className="utp-page-addAddress-item-big">
              <Wrap type={1}>
                <View className="utp-page-addAddress-item-th">
                  <Txt title="收货人" size={26} color="deep" />
                </View>
                <View className="utp-page-addAddress-item-big">
                  <Input
                    className="utp-page-addAddress-item-ipt"
                    type="text"
                    placeholder="请填写收货人姓名"
                    value={name}
                    onInput={(e) => {
                      setName(e.detail.value);
                    }}
                  />
                </View>
              </Wrap>
            </View>
          </Wrap>
        </View>

        <View className="utp-page-addAddress-item utp-solid">
          <Wrap type={1}>
            <View className="utp-page-addAddress-item-big">
              <Wrap type={1}>
                <View className="utp-page-addAddress-item-th">
                  <Txt title="手机号码" size={26} color="deep" />
                </View>
                <View className="utp-page-addAddress-item-big">
                  <Input
                    className="utp-page-addAddress-item-ipt"
                    type="text"
                    placeholder="请填写收货人手机号"
                    value={phone}
                    onInput={(e) => {
                      setPhone(e.detail.value);
                    }}
                  />
                </View>
              </Wrap>
            </View>
          </Wrap>
        </View>

        <Picker
          mode="multiSelector"
          value={state.selectorChecked}
          range={state.selector}
          rangeKey="name"
          onChange={onChange}
          onColumnChange={onColumnChange}
        >
          <View className="utp-page-addAddress-item utp-solid">
            <Wrap type={1}>
              <View className="utp-page-addAddress-item-big">
                <Wrap type={1}>
                  <View className="utp-page-addAddress-item-th">
                    <Txt title="所在地区" size={26} color="deep" />
                  </View>
                  <View className="utp-page-addAddress-item-big">
                    <Input
                      className="utp-page-addAddress-item-ipt"
                      type="text"
                      disabled
                      value={crtSsq}
                      placeholder="请选择省市区"
                    />
                  </View>
                </Wrap>
              </View>
              <Img src={right} width={44} />
            </Wrap>
          </View>
        </Picker>

        <View
          className="utp-page-addAddress-item utp-solid"
          onClick={() => {
            if (objs.length === 0) {
              Taro.showToast({
                title: '请先选择所在地区',
                icon: 'none'
              });
              return;
            }
            Router.goAddressLocation();
          }}
        >
          <Wrap type={1}>
            <View className="utp-page-addAddress-item-big">
              <Wrap type={1}>
                <View className="utp-page-addAddress-item-th">
                  <Txt title="收货地址" size={26} color="deep" />
                </View>
                <View className="utp-page-addAddress-item-big">
                  <Input
                    className="utp-page-addAddress-item-ipt"
                    type="text"
                    disabled
                    value={ads ? ads.title : ''}
                    placeholder="请选择收货地址"
                  />
                </View>
              </Wrap>
            </View>
            <Img src={right} width={44} />
          </Wrap>
        </View>

        <View className="utp-page-addAddress-item utp-solid">
          <Wrap type={1}>
            <View className="utp-page-addAddress-item-big">
              <Wrap type={1} top>
                <View className="utp-page-addAddress-item-th">
                  <Txt title="详细地址" size={26} color="deep" />
                </View>
                <View className="utp-page-addAddress-item-big utp-page-addAddress-item-info">
                  <Input
                    className="utp-page-addAddress-item-ipt"
                    type="text"
                    placeholder="街道、门牌号等"
                    value={number}
                    onInput={(e) => {
                      setNumber(e.detail.value);
                    }}
                  />
                </View>
              </Wrap>
            </View>
          </Wrap>
        </View>
      </Box1>
      <View className="utp-page-addAddress-gang"></View>
      <View className="utp-page-addAddress-item utp-solid" style={{ backgroundColor: '#ffffff' }}>
        <Wrap type={1}>
          <View className="utp-page-addAddress-item-big">
            <View className="utp-page-addAddress-item-tags">
              <View className="utp-page-addAddress-item-th">
                <Txt title="标签" size={26} color="deep" />
              </View>
              <View className="utp-page-addAddress-item-big">
                <Wrap type={2}>
                  {tabs.map((item) => {
                    return (
                      <View
                        key={item}
                        className={
                          'utp-page-addAddress-item-tag ' +
                          (item === tab ? 'utp-page-addAddress-item-tag-0' : '')
                        }
                        onClick={() => {
                          setTab(item);
                          setShow(false);
                          setShow1(false);
                          setTab1('');
                        }}
                      >
                        <Cnt>
                          <Txt title={item} size={24} color={item === tab ? 'white' : 'deep'} />
                        </Cnt>
                      </View>
                    );
                  })}
                  <View>
                    {!show1 && !show && (
                      <View
                        className="utp-page-addAddress-item-tag"
                        onClick={() => {
                          setShow(true);
                        }}
                      >
                        <Cnt>
                          <Txt title="+" size={24} color="deep" />
                        </Cnt>
                      </View>
                    )}

                    {show1 && (
                      <View
                        className="utp-page-addAddress-item-tag utp-page-addAddress-item-tag-0 utp-page-addAddress-item-tag-e1"
                        onClick={() => {
                          setShow(true);
                        }}
                      >
                        <Cnt>
                          <Txt title={tab1} size={24} color="white" />
                        </Cnt>
                      </View>
                    )}

                    {show1 && (
                      <View
                        className="utp-page-addAddress-item-tag utp-page-addAddress-item-tag-0 utp-page-addAddress-item-tag-e"
                        onClick={() => {
                          //setShow(true);

                          setShow1(false);
                        }}
                      >
                        <Cnt>
                          <Txt title="编辑" size={24} color="white" />
                        </Cnt>
                      </View>
                    )}
                    {show && !show1 && (
                      <Wrap>
                        <View className="utp-page-addAddress-item-tag utp-page-addAddress-item-tag-1">
                          <Input
                            className="utp-page-addAddress-item-tag-1-txt"
                            type="text"
                            placeholder="请输入标签名称，最多5个字"
                            value={tab1}
                            onInput={(e) => {
                              setTab1(e.detail.value);
                            }}
                          />
                        </View>
                        {!tab1 && (
                          <View className="utp-page-addAddress-item-tag utp-page-addAddress-item-tag-2">
                            <Cnt>
                              <Txt title="确定" size={24} color="white" />
                            </Cnt>
                          </View>
                        )}

                        {!!tab1 && (
                          <View
                            className="utp-page-addAddress-item-tag utp-page-addAddress-item-tag-2 utp-page-addAddress-item-tag-2-1"
                            onClick={() => {
                              setShow1(true);
                              setTab('');
                            }}
                          >
                            <Cnt>
                              <Txt title="确定" size={24} color="white" />
                            </Cnt>
                          </View>
                        )}
                      </Wrap>
                    )}
                  </View>
                </Wrap>
              </View>
            </View>
          </View>
        </Wrap>
      </View>
      <View
        className="utp-page-addAddress-item utp-solid"
        onClick={() => {
          setDef(!def);
        }}
      >
        <Wrap type={1}>
          <View className="utp-page-addAddress-item-big">
            <Wrap type={1}>
              <View className="utp-page-addAddress-item-th utp-page-addAddress-item-th-1">
                <Txt title="设为默认地址" size={26} color="deep" />
              </View>
            </Wrap>
          </View>
          <Switch checked={def} color="#FF2F7B" />
        </Wrap>
      </View>

      <View
        className="utp-page-addAddress-ft"
        onClick={() => {
          setDemo(demo + 1);
        }}
      >
        {isEdit && (
          <Wrap justifyContent="space-between">
            <View
              style={{ width: Taro.pxTransform(250) }}
              onClick={() => {
                Taro.showModal({
                  title: '',
                  content: '确定要删除该地址吗？',
                  confirmText: '删除',
                  confirmColor: '#ff317c',
                  success: (res) => {
                    if (res.confirm) {
                      // console.log('用户点击确定');
                      onDel();
                      //onDel();
                    } else if (res.cancel) {
                      // console.log('用户点击取消');
                    }
                  }
                });
              }}
            >
              <Bton type={1}>删除</Bton>
            </View>
            <View onClick={onSubmit} style={{ width: Taro.pxTransform(250) }}>
              <Bton>保存</Bton>
            </View>
          </Wrap>
        )}
        {!isEdit && (
          <View onClick={onSubmit}>
            <Bton>保存</Bton>
          </View>
        )}
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '新增收货地址',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff'
};
// @ts-ignore
export default connect((state) => state)(Index);
