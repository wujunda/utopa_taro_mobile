const dotenv = require('dotenv');
const path = require('path');
const fs = require('fs');

const isDevEnv = process.env.NODE_ENV === 'development';

const envFilePath = path.resolve(__dirname, '..', '.env');

dotenv.config({
  path: fs.existsSync(envFilePath)
    ? envFilePath
    : path.resolve(__dirname, `env/${process.env.NODE_BUILD_ENV || 'prod'}.env`)
});

const config = {
  alias: {
    '@/components': path.resolve(__dirname, '..', 'src/components'),
    '@/utils': path.resolve(__dirname, '..', 'src/utils'),
    '@/hooks': path.resolve(__dirname, '..', 'src/hooks'),
    '@/assets': path.resolve(__dirname, '..', 'src/assets'),
    '@/services': path.resolve(__dirname, '..', 'src/services'),
    '@/store': path.resolve(__dirname, '..', 'src/store'),
    '@/constants': path.resolve(__dirname, '..', 'src/constants.ts'),
    '@/package': path.resolve(__dirname, '..', 'package.json'),
    '@/model': path.resolve(__dirname, '..', 'src/model'),
    '@/interface': path.resolve(__dirname, '..', 'src/interface')
  },

  projectName: 'taroDemo',
  date: '2020-3-11',
  designWidth: 750,
  deviceRatio: {
    '640': 2.34 / 2,
    '750': 1,
    '828': 1.81 / 2
  },
  sourceRoot: 'src',
  outputRoot: `dist/${process.env.TARO_ENV}`,
  babel: {
    sourceMap: true,
    presets: [
      [
        'env',
        {
          modules: false
        }
      ]
    ],
    plugins: [
      'transform-decorators-legacy',
      'transform-class-properties',
      'transform-object-rest-spread',
      [
        'transform-runtime',
        {
          helpers: false,
          polyfill: false,
          regenerator: true,
          moduleName: 'babel-runtime'
        }
      ]
    ]
  },
  plugins: ['@tarojs/plugin-sass', '@tarojs/plugin-terser'],
  defineConstants: {},
  mini: {
    postcss: {
      pxtransform: {
        enable: true,
        config: {}
      },
      url: {
        enable: true,
        config: {
          limit: 10240 // 设定转换尺寸上限
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    }
  },
  h5: {
    esnextModules: ['taro-ui'],
    devServer: {
      port: 8081,
      proxy: {
        '/testApi/': {
          target: 'http://127.0.0.1:8080',
          pathRewrite: {
            '^/testApi/': '/'
          },
          changeOrigin: true
        },
        '/api/': {
          target: 'https://api.myutopa.com',
          pathRewrite: {
            '^/api/': '/'
          },
          changeOrigin: true
        }
      }
    },
    publicPath: '/',
    staticDirectory: 'static',
    postcss: {
      autoprefixer: {
        enable: true,
        config: {
          browsers: ['last 3 versions', 'Android >= 4.1', 'ios >= 8']
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    }
  }
};

module.exports = function (merge) {
  if (isDevEnv) {
    return merge({}, config, require('./dev'));
  }
  return merge({}, config, require('./prod'));
};
