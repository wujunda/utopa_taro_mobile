import Taro from '@tarojs/taro';

import GoodsList from '@/components/GroupGoods/GoodsList';
import SingleImage from '@/components/GroupImageText/SingleImage';
import RowMultipleImage from '@/components/GroupImageText/RowMultipleImage';
import { COM_TYPES } from './types';

// import Goods1 from '@/components/Goods1';
// import Goods2 from '@/components/Goods2';
// import Goods3 from '@/components/Goods3';

// eslint-disable-next-line no-unused-vars
import { ComponentData } from '../interface';
// eslint-disable-next-line import/first
import NavSwiperCom from '@/components/NavSwiperCom';
// eslint-disable-next-line import/first
import HomeNavbarCom from '@/components/HomeNavbarCom';

interface Props extends Partial<ComponentData<any>> {
  type: string;
  uid: string;
}

export const MatchComponent: Taro.FC<Props> = (props) => {
  const { type, data = {} } = props;
  // console.log({ type });
  let component;

  switch (type) {
    // -----------商品----------------
    case COM_TYPES.goods_sku_goods1: {
      component = <GoodsList type="1x1" dataSource={data.dataSource} />;
      break;
    }
    case COM_TYPES.goods_sku_goods2: {
      component = <GoodsList type="1x2" dataSource={data.dataSource} />;
      break;
    }
    case COM_TYPES.goods_sku_goods3: {
      component = <GoodsList type="1x3" dataSource={data.dataSource} />;
      break;
    }
    // -------------图文--------------
    case COM_TYPES.iamge_single: {
      component = <SingleImage src={data.src} width="100%" />;
      break;
    }
    case COM_TYPES.iamge_row_multiple: {
      component = <RowMultipleImage dataSource={data} />;
      break;
    }
    case COM_TYPES.nav_swiper: {
      // console.log(data, 'swipeer');
      component = <NavSwiperCom dataSource={data.dataSource} />;
      break;
    }
    case COM_TYPES.nav_navbar: {
      // eslint-disable-next-line react/jsx-no-undef
      component = <HomeNavbarCom dataSource={data.dataSource} />;
      break;
    }
    default: {
      component = null;
    }
  }

  return component;
};

export default MatchComponent;
