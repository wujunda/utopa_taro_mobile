import Taro from '@tarojs/taro';
import JSBridge from '@/utils/jsbridge/index';
import { View, Image, Swiper, SwiperItem } from '@tarojs/components';
import './index.scss';

const USwiper = (props) => {
  const { banners } = props;

  const toCmd = (cmdParams) => {
    JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, () => {
      console.log('执行', cmdParams);
    });
  };

  return (
    <Swiper className="u-swiper" indicatorDots autoplay={false}>
      {!!banners &&
        banners.map((banner) => (
          <SwiperItem key={banner.id} onClick={() => toCmd(banner.command)}>
            <View className="u-swiper-item">
              <Image className="banner" src={banner.image} />
            </View>
          </SwiperItem>
        ))}
    </Swiper>
  );
};

export default USwiper;
