import Taro, { useState, useEffect, useRouter, useDidShow } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View, ScrollView, Input } from '@tarojs/components';
//import useStore from '../../../components/useNum';
import { connect } from '@tarojs/redux';
import Wrap from '../../../components/Wrap';
import Img from '../../../components/Img';
import Txt from '../../../components/Txt';
import Cnt from '../../../components/Cnt';
import SearchStore from '../../../components/SearchStore';
import Goods1 from '../../../components/Goods1';
import Tag from '../../../components/Tag';
import './index.scss';
import search from '../../../assets/search/search.png';
import del from '../../../assets/search/delete.png';
import ud from '../../../assets/search/ud.png';
import xdel from '../../../assets/search/icon_search_delete@2x.png';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import { ProductStoreInfo, ProductBaiscData } from '../../../interface/product';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import Bar from '../../../components/bar';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import {
  ISearch,
  getSearch,
  getSearchDel,
  ISearchFind,
  ISearchFindData,
  getSearchFind,
  ISearchHistoryData,
  ISearchHistory,
  getSearchHistory
} from '../../../services/search';
import { unfavorite, favorite } from '../../../services/store';
import useRequest from '../../../hooks/useRequest';
import Router from '../../../utils/router';

type PageStateProps = {
  user: {
    name: string;
  };
  example: any;
  statusHeight: number;
  closeWidth: number;
  loading: boolean;
};

type PageDispatchProps = {
  wait: () => void;
  clickEffict: () => void;
  toGetGoods: () => void;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = (props) => {
  const router = useRouter();
  let id = router.params.id;
  let isLiber = router.params.isLiber;
  let name = decodeURI(router.params.name);

  const [tag, setTag] = useState([
    {
      name: '自营',
      isCrt: false,
      post: {
        activityTag: undefined,
        propTag: 1
      },
      noPost: {
        activityTag: undefined,
        propTag: undefined
      }
    },
    {
      name: '即时达',
      isCrt: false,
      post: {
        activityTag: undefined,
        propTag: 2
      },
      noPost: {
        activityTag: undefined,
        propTag: undefined
      }
    },
    {
      name: '包邮',
      isCrt: false,
      post: {
        activityTag: undefined,
        propTag: 3
      },
      noPost: {
        activityTag: undefined,
        propTag: undefined
      }
    },
    {
      name: '折扣商品',
      isCrt: false,
      post: {
        activityTag: 1,
        propTag: undefined
      },
      noPost: {
        activityTag: undefined,
        propTag: undefined
      }
    },
    {
      name: '拼团商品',
      isCrt: false,
      post: {
        activityTag: 3,
        propTag: undefined
      },
      noPost: {
        activityTag: undefined,
        propTag: undefined
      }
    },
    {
      name: '7天无理由退货',
      isCrt: false,
      post: {
        activityTag: undefined,
        propTag: 4
      },
      noPost: {
        activityTag: undefined,
        propTag: undefined
      }
    }
  ]);
  const [haveSearch, setHaveSearch] = useState(false);
  const [check, setCheck] = useState(0);
  const [Value, setValue] = useState('');
  //const [searchValue, setSearchValue] = useState(false);
  const [sortOrder, setSortOrder] = useState(0);

  // 搜索历史
  const [stateHistory, updateHistory] = useRequest<ISearchHistory>('needLogin', getSearchHistory);
  // 发现
  const [stateFind] = useRequest<ISearchFind>('name=a&age=1', getSearchFind);

  const [control, setControl] = useState('start');
  // 搜索
  const [stateSearch, hasMore, updateSearch, getMoreData, loading5] = useRequestWIthMore<ISearch>(
    control,
    getSearch
  );
  if (0) {
    console.log(getMoreData);
  }
  //
  let history: ISearchHistoryData[] = [];
  if (
    stateHistory &&
    stateHistory.data &&
    stateHistory.data.records &&
    stateHistory.data.records.length > 0
  ) {
    history = stateHistory.data.records;
  }
  //
  let find: ISearchFindData[] = [];
  if (stateFind && stateFind.data && stateFind.data.records && stateFind.data.records.length > 0) {
    find = stateFind.data.records;
  }
  //
  let goods: ProductBaiscData[] = [];
  if (stateSearch && !id && stateSearch.length > 0) {
    stateSearch.map((item) => {
      if (item.data && item.data.productVersion2Result && item.data.productVersion2Result.records) {
        item.data.productVersion2Result.records.map((item1) => {
          goods.push(item1);
        });
      }
    });
  }

  if (stateSearch && id && stateSearch.length > 0) {
    stateSearch.map((item) => {
      if (item.data && item.data.records && item.data.records) {
        item.data.records.map((item1) => {
          console.log('分类商品');
          goods.push(item1);
        });
      }
    });
  }
  let stores: ProductStoreInfo[] = [];
  if (stateSearch && stateSearch.length > 0) {
    stateSearch.map((item) => {
      if (item.data && item.data.storeResult && item.data.storeResult.records) {
        item.data.storeResult.records.map((item1) => {
          stores.push(item1);
        });
      }
    });
  }

  const [post1, setPost1] = useState({});
  const [init, setInit] = useState(false);
  const onSearch = (post) => {
    console.warn('触发搜索0');
    setControl('start1');
    setPost1({ ...post1, ...post });
  };
  // 删除搜索记录
  const onDel = async () => {
    await getSearchDel({});
    if (updateHistory) {
      updateHistory();
    }
  };
  useEffect(() => {
    if (updateSearch && init) {
      updateSearch(post1);
      console.warn('触发搜索');
      setHaveSearch(true);

      if (updateHistory) {
        setTimeout(() => {
          updateHistory();
        }, 5000);
      }
    }
    setInit(true);
  }, [post1]);
  useDidShow(() => {
    if (id) {
      setTimeout(() => {
        onSearch(
          Object.assign(
            {},
            isLiber === '1'
              ? {
                  specialThemeBrandId: id,
                  categoryId: id,
                  type: 2
                }
              : {
                  categoryId: id
                }
          )
        );
      }, 0);
    }
  });

  if (loading5 && !stateSearch) {
    Taro.showLoading({
      title: ''
    });
  } else {
    Taro.hideLoading();
  }
  // 关注 || 取消关注
  const dolike = async (params) => {
    let res;
    if (params.isFavorite) {
      res = await unfavorite({ storeId: params.storeId, favoriteId: params.id });
    } else {
      res = await favorite({ storeId: params.storeId, favoriteId: params.id });
    }
    if (res.code === 0) {
      Taro.showToast({ title: '操作成功', icon: 'none' });
      // updateSearch && updateSearch({})
    }
  };

  return (
    <View className="utp-search" style={{ backgroundColor: '#f5f5f5' }}>
      <View style={{ height: props.statusHeight + 'px' }}></View>
      <View className="utp-search-hd-1" style={{ height: props.statusHeight + 'px' }}></View>
      <View
        className="utp-search-hd"
        style={{
          top: props.statusHeight + 'px',
          paddingBottom: haveSearch ? 0 : Taro.pxTransform(20)
        }}
      >
        {id && <Bar title={name} goBack={true} />}
        {!id && (
          <View
            style={{
              background: haveSearch ? '#fff' : '#FBFBFB',
              paddingRight: props.closeWidth + 'px'
            }}
            className="utp-search-hd-schs"
          >
            <Wrap type={1}>
              <View className="utp-search-hd-sch">
                <Wrap type={1}>
                  <View
                    onClick={() => {
                      //setSearchValue(true);
                    }}
                  >
                    <Img src={search} width={44} />
                  </View>
                  <View className="utp-search-hd-ipts">
                    <Input
                      value={Value}
                      onInput={(e) => {
                        setValue(e.detail.value);
                        // console.log('我再变化了----------', e.detail.value, Value);
                        if (!e.detail.value) {
                          //setSearchValue(false);
                          setHaveSearch(false);
                        }
                      }}
                      onConfirm={() => {
                        onSearch({
                          keyword: Value
                        });
                      }}
                      placeholder="请搜索商户或者商品"
                      className="utp-search-hd-ipts1"
                      confirmType="search"
                    />
                  </View>
                  {Value && (
                    <View
                      onClick={() => {
                        setValue('');
                        setHaveSearch(false);
                      }}
                    >
                      <Img src={xdel} width={44} />
                    </View>
                  )}
                </Wrap>
              </View>
              <View
                className="utp-search-hd-cancel"
                onClick={() => {
                  Router.goBack();
                }}
              >
                <Cnt>
                  <Txt title="取消" size={28} color="deep" />
                </Cnt>
              </View>
            </Wrap>
          </View>
        )}
        {haveSearch && (
          <View>
            <View className="utp-search-hd-sorts">
              <View className="utp-search-hd-sorts-gang"></View>
              <Wrap type={1} justifyContent="space-between">
                <View
                  className="utp-search-hd-sorts-box"
                  onClick={() => {
                    if (loading5) {
                      return;
                    }
                    setCheck(0);
                    setSortOrder(0);
                    onSearch({
                      searchType: 2,
                      sortType: 0
                    });
                  }}
                >
                  <Cnt>
                    <Txt title="综合排序" size={28} color="deep" bold={check === 0} />
                    {check === 0 && (
                      <View className="utp-search-hd-sorts-box-ft">
                        <Cnt>
                          <View className="utp-search-hd-sorts-box-ft-gang"></View>
                        </Cnt>
                      </View>
                    )}
                  </Cnt>
                </View>
                <View
                  className="utp-search-hd-sorts-box"
                  onClick={() => {
                    if (loading5) {
                      return;
                    }
                    setCheck(1);
                    setSortOrder(0);
                    onSearch({
                      searchType: 2,
                      sortType: 1
                    });
                  }}
                >
                  <Cnt>
                    <Txt title="销量" size={28} color="deep" bold={check === 1} />
                    {check === 1 && (
                      <View className="utp-search-hd-sorts-box-ft">
                        <Cnt>
                          <View className="utp-search-hd-sorts-box-ft-gang"></View>
                        </Cnt>
                      </View>
                    )}
                  </Cnt>
                </View>

                <View
                  className="utp-search-hd-sorts-box"
                  onClick={() => {
                    if (loading5) {
                      return;
                    }
                    if (sortOrder === 1) {
                      setSortOrder(2);
                    } else if (sortOrder === 2) {
                      setSortOrder(1);
                    } else {
                      setSortOrder(1);
                    }
                    setCheck(2);

                    onSearch({
                      searchType: 2,
                      sortType: 2,
                      sortOrder: sortOrder !== 1 ? 1 : 0 // 10
                    });
                  }}
                >
                  <Cnt>
                    <Wrap>
                      <Txt title="价格" size={28} color="deep" bold={check === 2} />
                      {sortOrder === 0 && (
                        <View className="utp-search-hd-prices">
                          <Img src={ud} width={14} height={24} />
                        </View>
                      )}

                      {sortOrder === 1 && (
                        <View className="utp-search-hd-prices utp-search-hd-prices-u">
                          <Img src={ud} width={14} height={24} />
                        </View>
                      )}
                      {sortOrder === 2 && (
                        <View className="utp-search-hd-prices utp-search-hd-prices-u">
                          <View className="utp-search-hd-prices-dd">
                            <Img src={ud} width={14} height={24} />
                          </View>
                        </View>
                      )}
                    </Wrap>
                    {check === 2 && (
                      <View className="utp-search-hd-sorts-box-ft">
                        <Cnt>
                          <View className="utp-search-hd-sorts-box-ft-gang"></View>
                        </Cnt>
                      </View>
                    )}
                  </Cnt>
                </View>

                <View
                  className="utp-search-hd-sorts-box"
                  onClick={() => {
                    if (loading5) {
                      return;
                    }
                    setCheck(3);
                    setSortOrder(0);

                    if (id) {
                      onSearch({
                        sortType: 3,
                        type: 0
                      });
                    } else {
                      onSearch({
                        searchType: 1
                      });
                    }
                  }}
                >
                  <Cnt>
                    <Txt
                      title={id ? '新品优先' : '店铺'}
                      size={28}
                      color="deep"
                      bold={check === 3}
                    />
                    {check === 3 && (
                      <View className="utp-search-hd-sorts-box-ft">
                        <Cnt>
                          <View className="utp-search-hd-sorts-box-ft-gang"></View>
                        </Cnt>
                      </View>
                    )}
                  </Cnt>
                </View>
              </Wrap>
            </View>

            {(check !== 3 || (check === 3 && id)) && (
              <ScrollView className="utp-search-hd-tags" scrollX>
                {tag.map((item) => {
                  return (
                    <View
                      className="utp-search-hd-tag"
                      key={item.name}
                      onClick={() => {
                        if (loading5) {
                          return;
                        }
                        let arr = tag;
                        let rst = item.isCrt;
                        arr.map((item1) => {
                          item1.isCrt = false;
                        });
                        if (!rst) {
                          item.isCrt = true;
                          onSearch(item.post);
                        } else {
                          item.isCrt = false;
                          onSearch(item.noPost);
                        }
                        setTag(arr);
                      }}
                    >
                      <Tag type={item.isCrt ? 11 : 13}>{item.name}</Tag>
                    </View>
                  );
                })}
              </ScrollView>
            )}
          </View>
        )}
      </View>

      {/* 无搜索内容时候 */}
      {!haveSearch && (
        <View className="utp-search-bd utp-search-bd-1">
          {history.length > 0 && (
            <View>
              <View className="utp-search-bd-th">
                <Wrap justifyContent="space-between">
                  <Txt title="搜索历史" size={24} height={50} bold />
                  <View
                    onClick={() => {
                      Taro.showModal({
                        title: '提示',
                        content: '确认删除所有记录？',
                        success: (res) => {
                          if (res.confirm) {
                            console.log('用户点击确定');
                            onDel();
                          } else if (res.cancel) {
                            console.log('用户点击取消');
                          }
                        }
                      });
                    }}
                  >
                    <Img src={del} width={40} />
                  </View>
                </Wrap>
              </View>
              <View className="utp-search-bd-box">
                <Wrap type={2}>
                  {history.map((item, index) => {
                    return (
                      <View
                        style={{
                          marginRight: Taro.pxTransform(20),
                          marginBottom: Taro.pxTransform(20)
                        }}
                        key={index}
                        onClick={() => {
                          setValue(item.content);
                          onSearch({
                            keyword: item.content
                          });
                        }}
                      >
                        <Tag type={Value === item.content ? 14 : 13}>{item.content}</Tag>
                      </View>
                    );
                  })}
                </Wrap>
              </View>
            </View>
          )}

          {find.length > 0 && (
            <View>
              <View className="utp-search-bd-th">
                <Wrap>
                  <Txt title="搜索发现" size={24} height={68} bold />
                </Wrap>
              </View>
              <View className="utp-search-bd-box">
                <Wrap type={2}>
                  {find.map((item) => {
                    return (
                      <View
                        style={{
                          marginRight: Taro.pxTransform(18),
                          marginBottom: Taro.pxTransform(18)
                        }}
                        key={item.url}
                        onClick={() => {
                          Router.goHome(item.command);
                        }}
                      >
                        <Tag type={14}>{item.keyWords}</Tag>
                      </View>
                    );
                  })}
                </Wrap>
              </View>
            </View>
          )}
        </View>
      )}

      {/* 有搜索内容时候 */}
      {haveSearch && (
        <View
          className={
            'utp-search-bd utp-search-bd-2 ' + (check === 3 && !id ? 'utp-search-bd-3' : '')
          }
        >
          {check !== 3 && (
            <View
              style={{
                paddingTop: Taro.pxTransform(20)
                // backgroundColor: '#f5f5f5'
              }}
            >
              {false && loading5 && goods.length === 0 && <Loading />}
              {!loading5 && goods.length === 0 && <LoadingNone />}
              {goods.map((item) => {
                return <Goods1 item={item} key={item.productId} isAdd={false} />;
              })}

              {loading5 && goods.length !== 0 && <LoadingMore />}
              {!loading5 && !hasMore && goods.length > 0 && <LoadingNoneMore />}
            </View>
          )}
          {check === 3 && id && (
            <View
              style={{
                paddingTop: Taro.pxTransform(20)
                // backgroundColor: '#f5f5f5'
              }}
            >
              {loading5 && goods.length === 0 && <Loading />}
              {!loading5 && goods.length === 0 && <LoadingNone />}
              {goods.map((item) => {
                return <Goods1 item={item} key={item.productId} isAdd={false} />;
              })}

              {loading5 && goods.length !== 0 && <LoadingMore />}
              {!loading5 && !hasMore && goods.length > 0 && <LoadingNoneMore />}
            </View>
          )}

          {check === 3 && !id && (
            <View
              style={{
                paddingTop: Taro.pxTransform(20),
                paddingLeft: Taro.pxTransform(20),
                paddingRight: Taro.pxTransform(20),
                backgroundColor: '#f5f5f5'
              }}
            >
              {false && loading5 && stores.length === 0 && <Loading />}
              {!loading5 && stores.length === 0 && <LoadingNone />}
              {stores.map((item) => {
                return (
                  <SearchStore
                    item={item}
                    key={item.storeId}
                    like={() => {
                      dolike(item);
                    }}
                  />
                );
              })}
              {loading5 && stores.length !== 0 && <LoadingMore />}
              {!loading5 && !hasMore && stores.length > 0 && <LoadingNoneMore />}
            </View>
          )}
        </View>
      )}
    </View>
  );
};

Index.config = {
  navigationBarTitleText: '搜索',
  disableScroll: false,
  navigationStyle: 'custom'
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight,
    closeWidth: state.cart.closeWidth
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
