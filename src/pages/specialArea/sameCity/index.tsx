/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable import/first */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-shadow */
import Taro, { useEffect, useState, useMemo, useCallback, useDidShow } from '@tarojs/taro';
import { View, ScrollView, Text } from '@tarojs/components';
import Safe from '@/components/safe';
// import Bar from '@/components/bar';
// import Wrap from '@/components/Wrap';
// import { spliceStr, priceToFixed } from '../common/tool';
import ViewSameCityGoods from './cityGoods';
import './index.scss';
import { SPECIAL_AREA_TYPE } from '../common/config';
// eslint-disable-next-line import/first
import { getSecMore } from '@/services/home';
import { FlowGoodsItem, FlowGoodsBox } from '../components/flowGoodsItem/index';
import { GoodsItem } from '../common/interface';
import { handleScroll } from '../common/tool';
import ListView, { LazyBlock } from "taro-listview";
import { Glo } from '@/utils/utils'

interface Props {
  type?: any;
  title?: string;
  context?: any;
}

const blankList = [
  {
    author: {},
    title: 'this is a example'
  },
  {
    author: {},
    title: 'this is a example'
  },
  {
    author: {},
    title: 'this is a example'
  },
  {
    author: {},
    title: 'this is a example'
  }
];

let pageIndex = 1;

export const Index: Taro.FC<Props> = (props) => {

  // 同城类
  const [sourceData, setSourceData] = useState<any>([]);
  const [pageNum, setPage] = useState<any>(0);
  const [state, setState] = useState<any>({
    isLoaded: false,
    error: false,
    hasMore: true,
    isEmpty: false,
    list: [
      {
        author: {},
        title: 'this is a example'
      },
      {
        author: {},
        title: 'this is a example'
      },
      {
        author: {},
        title: 'this is a example'
      },
      {
        author: {},
        title: 'this is a example'
      }
    ]
  })
  // useEffect(() => {
  //   refList.fetchInit();
  // }, []);
  useDidShow(() => {
    //  handleScroll();
    refList.fetchInit();
  });
  const getData = async (pIndex = pageNum) => {
    Glo.loading('正在刷新')
    try {
      if (pIndex === 1) {
        setState((state) => ({
          ...state,
          isLoaded: false
        }));
      }
      return { list: blankList, hasMore: true, isLoaded: pIndex === 1 };
    } catch (e) { } finally {
      setTimeout(() => {
        Glo.hideLoading()
      }, 1000)
    }

  }
  var refList: any = {};
  const pullDownRefresh = async () => {
    setPage(1)
    console.log('下拉刷新');
    const res = await getData(1);
    setState(res)

  };
  const onScrollToLower = async fn => {
    console.log('触发底部+', pageNum)
    const { list } = state;
    getData(setPage(pageNum + 1));
    // setState((state) => ({
    //   ...state,
    //   list: list.concat(newList),
    //   hasMore
    // }))
    fn();
  };
  const insRef = node => {
    refList = node;
  };
  const tips = { release: '加载中', activate: '下拉刷新', deactivate: '释放刷新' }
  return (
    <View className="skeleton lazy-view" style={{ fontSize: 12 }}>
      <ListView
        lazy
        indicator={tips}
        ref={node => insRef(node)}
        isLoaded={state.isLoaded}
        isError={state.error}
        hasMore={state.hasMore}
        style={{ height: '100vh', fontSize: 12 }}
        isEmpty={state.isEmpty}
        onPullDownRefresh={() => pullDownRefresh()}
        onScrollToLower={(e) => { onScrollToLower(e) }}
        lazyStorage="lazyView"
      >
        {state.list.map((item, index) => {
          return (
            <View className="item skeleton-bg" key={index}>
              <LazyBlock current={index} className="avatar" lazyStorage="lazyView">

              </LazyBlock>
              <View className="title skeleton-rect" style={{ height: 500 }}>{item.title}</View>
            </View>
          );
        })}
      </ListView>
      {
        !state.hasMore && <View>没有更多咯</View>
      }

    </View >

    /* <FlowGoodsItem name={111} /> */
    /* <Bar title={title || '同城好货'} /> */
    /* {
       SPECIAL_AREA_TYPE[title]
      } */
    /* <View className="sameCity">
        <View className="sameCity-head">
          <View className="sameCity-head-search"></View>
          <View style={{ marginTop: 10 }}>
            <ViewScrollTab
              active={2}
              onChange={(v: any) => {
                console.log(v);
              }}
            />
          </View>
        </View>
        <View>
          {sourceData &&
            sourceData.map((item: GoodsItem, idx) => {
              return (
                <View style={{ marginTop: 5 }} key={idx}>
                  <ViewSameCityItem
                    item={item}
                    goStore={() => console.log('进入店铺')}
                    add={() => console.log('1')}
                  />
                </View>
              );
            })}
        </View>
      </View> */
  );
};
Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '同城',
  disableScroll: true,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};
export default Index;
