import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import './index.scss';

interface Props {
  children: any;
  type?: 1 | 2 | 21 | 3 | 31 | 5 | 6 | 10 | 11 | 12 | 13 | 14 | 15;
  margin?: number;
}
const Index: Taro.FC<Props> = ({ children, type, margin = 0 }) => {
  let classView = 'utp-tag utp-cnt';
  if (type === 1) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-1';
  }
  if (type === 2) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-2';
  }
  if (type === 21) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-21';
  }
  if (type === 3) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-3';
  }
  if (type === 4) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-4';
  }
  if (type === 31) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-31';
  }
  if (type === 5) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-5';
  }
  if (type === 6) {
    classView = 'utp-tag utp-cnt utp-tag-6';
  }
  if (type === 10) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-10';
  }
  if (type === 11) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-11';
  }
  if (type === 12) {
    classView = 'utp-tag utp-cnt utp-tag-12';
  }
  if (type === 13) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-13';
  }
  if (type === 14) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-14';
  }
  if (type === 15) {
    classView = 'utp-solid utp-tag utp-cnt utp-tag-15';
  }

  return (
    <View className={classView} style={{ marginRight: margin }}>
      {type === 1 && (
        <Text
          style={{
            color: '#ff3441',
            fontSize: Taro.pxTransform(20)
          }}
        >
          {children}
        </Text>
      )}
      {type !== 1 && (
        <Text
          style={{
            color:
              type === 31
                ? '#ff2f7b'
                : type === 4
                ? '#25A6FA'
                : type === 5
                ? '#ff4488'
                : type === 10
                ? '#333333'
                : type === 11
                ? '#ff2f7b'
                : type === 12
                ? '#a6a6a6'
                : type === 13
                ? '#666666'
                : type === 14
                ? '#ff2f7b'
                : type === 15
                ? '#666666'
                : '#fff',
            fontSize: Taro.pxTransform(
              type === 10 || type === 11 || type === 12 || type === 13 || type === 14
                ? 26
                : type === 15
                ? 24
                : 20
            )
          }}
        >
          {children}
        </Text>
      )}
    </View>
  );
};
Index.defaultProps = {};

export default Index;
