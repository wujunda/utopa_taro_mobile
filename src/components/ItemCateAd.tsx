import Taro from '@tarojs/taro';
import BashIt from './BashIt';

type PageStateProps = {
  // counter: {
  //   num: number;
  // };
};

type PageDispatchProps = {};

type PageOwnProps = {
  src: any;
  tips: string;
  num?: number | string;
  onClick?: () => void;
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = (props) => {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */

  return (
    <BashIt
      top={0}
      bottom={10}
      src={props.src}
      srcWidth={150}
      srcHeight={84}
      tips=""
      tipsHeight={50}
      tipsColor="#333333"
      tipsSize={20}
      num={props.num}
      onClick={props.onClick}
    ></BashIt>
  );
};

Index.defaultProps = {};
export default Index;
