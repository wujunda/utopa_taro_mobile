import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import Wrap from '../../../../components/Wrap';
import Img from '../../../../components/Img';
import tui from '../../../../assets/order/tui.png';
import tui2 from '../../../../assets/order/aftersale.png';

interface IProps {
  type: any;
}

const refundType = {
  1: '仅退款',
  2: '退货退款'
};
const refundTypeStyle = {
  1: {
    color: '#FF2F7B',
    fontSize: '14px',
    marginLeft: '4px'
  },
  2: {
    color: '#FF2F7B',
    fontSize: '14px',
    marginLeft: '4px'
  }
};

const Index: Taro.FC<IProps> = ({ type }) => {
  return (
    <Wrap>
      <Img src={type === 1 ? tui : tui2} width={34} />
      <View style={refundTypeStyle[type]}>{refundType[type]}</View>
    </Wrap>
  );
};
Index.defaultProps = {};

export default Index;
