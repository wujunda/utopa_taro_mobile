import Taro, { Component, Config, pxTransform } from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import ads from '../../assets/sch/address.png';
import msg from '../../assets/sch/msg.png';
import scan from '../../assets/sch/scan.png';
import Wrap from '../../components/Wrap';
import Txt from '../../components/Txt';
import { images } from '../../images';

import './index.scss';
import home_search_icon_black from '../../assets/sch/home_search_icon_black.png';
import home_search_icon_white from '../../assets/sch/home_search_icon_white.png';
import home_icon_scan_white from '../../assets/sch/home_icon_scan_white.png';
import home_message_white from '../../assets/sch/home_message_white.png';
import home_right from '../../assets/sch/home_right.png';

interface IProps {
  type?: 1 | 2;
  // width?: number; //百分比的数字
  opacity?: number;
  onScan?: () => void;
  scrollTop?: number;
  closeWidth?: number;
}

const Index: Taro.FC<IProps> = ({ type = 1, opacity = 0, onScan, scrollTop = 0, closeWidth }) => {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  let width = 355;
  // console.log('我是搜索元素————----', (1 - opacity) * 88, opacity);

  return (
    <View className="utp-schBox">
      <View
        className="utp-sch-box"
        style={{ width: Taro.pxTransform(750 - (closeWidth ? closeWidth * 2 : 0)) }}
      >
        {type === 1 && (
          <View className="utp-sch">
            <View className="utp-sch-ads utp-cnt">
              <Image mode="aspectFit" className="utp-sch-i" src={images.jishida} />
            </View>
            <View
              className="utp-schs utp-cnt"
              onClick={() => {
                Taro.navigateTo({
                  url: '/pages/packOrder/search/index'
                });
              }}
            >
              <View className="utp-sch1 utp-solid">
                <View className="utp-sch2">
                  <Wrap type={2} Myheight={68}>
                    <Image mode="aspectFit" className="utp-sch1-ico" src={home_search_icon_black} />
                    <Txt title="搜索" size={28} color="low" height={25} />
                  </Wrap>
                </View>
              </View>
            </View>

            <View className="utp-sch-scan utp-cnt" onClick={onScan}>
              <Image mode="aspectFit" className="utp-sch-i" src={scan} />
            </View>
            <View className="utp-sch-msg utp-cnt">
              <Image mode="aspectFit" className="utp-sch-i" src={msg} />
            </View>
          </View>
        )}

        {type === 2 && (
          <View style={{ position: 'relative', height: '100%' }}>
            {scrollTop < 44 && (
              <View
                className="utp-sch"
                style={{
                  opacity: (40 - screenTop) / 10
                }}
              >
                <View className="utp-sch-ads utp-cnt">
                  <Image mode="aspectFit" className="utp-sch-i" src={images.jishida1} />
                </View>
                <View className="utp-sch-ads utp-cnt" style={{ width: Taro.pxTransform(50) }}>
                  <Image mode="aspectFit" className="utp-sch-i" src={images.ads1} />
                </View>

                <View className="utp-schs-distant">
                  <View>
                    <Wrap type={2}>
                      <Text className="utp-schs-distant-text">
                        中国广东省广州市天河区黄村立交……
                      </Text>
                    </Wrap>
                  </View>
                </View>

                <View className="utp-sch-scan utp-cnt" onClick={onScan}>
                  <Image mode="aspectFit" className="utp-sch-i" src={home_icon_scan_white} />
                </View>
                <View className="utp-sch-msg utp-cnt">
                  <Image mode="aspectFit" className="utp-sch-i" src={home_message_white} />
                </View>
              </View>
            )}
            {scrollTop > 40 && (
              <View
                className="utp-sch-longSch utp-cnt utp-sch-longSch-1"
                style={{
                  width: '100%'
                }}
              >
                <View className="utp-sch">
                  <View className="utp-sch-ads utp-cnt">
                    <Image mode="aspectFit" className="utp-sch-i" src={images.jishida} />
                  </View>
                  <View
                    className="utp-schs utp-cnt"
                    onClick={() => {
                      Taro.navigateTo({
                        url: '/pages/packOrder/search/index'
                      });
                    }}
                  >
                    <View className="utp-sch1 utp-solid">
                      <View className="utp-sch2">
                        <Wrap type={2} Myheight={68}>
                          <Image
                            mode="aspectFit"
                            className="utp-sch1-ico"
                            src={home_search_icon_black}
                          />
                          <Txt title="搜索" size={28} color="low" height={25} />
                        </Wrap>
                      </View>
                    </View>
                  </View>

                  <View className="utp-sch-scan utp-cnt" onClick={onScan}>
                    <Image mode="aspectFit" className="utp-sch-i" src={scan} />
                  </View>
                  <View className="utp-sch-msg utp-cnt">
                    <Image mode="aspectFit" className="utp-sch-i" src={msg} />
                  </View>
                </View>
              </View>
            )}
            {scrollTop < 40 && (
              <View
                className="utp-sch-longSch utp-cnt"
                style={{
                  width: '100%',
                  opacity: (40 - screenTop) / 10
                }}
                onClick={() => {
                  Taro.navigateTo({
                    url: '/pages/packOrder/search/index'
                  });
                }}
              >
                <View
                  className={'utp-sch-longSch-sch1'}
                  style={{
                    width: width
                  }}
                >
                  <View className="utp-sch2">
                    <Wrap type={2} Myheight={68}>
                      <Image
                        // mode="aspectFit"
                        className="utp-sch1-ico"
                        src={home_search_icon_white}
                      />
                      <Txt title="请搜索商户或者商品" size={28} color={'white'} />
                    </Wrap>
                  </View>
                </View>
              </View>
            )}
          </View>
        )}
      </View>
    </View>
  );
};

Index.defaultProps = {};

export default Index;
