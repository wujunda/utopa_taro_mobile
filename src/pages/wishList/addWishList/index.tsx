import { View, ScrollView, Text, Textarea, Image, Button, Input } from '@tarojs/components';
import Taro, { useShareAppMessage,useRouter, useEffect, useState } from '@tarojs/taro';
import Bar from '../../../components/bar';
>>>>>>> test
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Message from '../../../components/Message';
import Goods1 from '../../../components/Goods1';
import right from '../../../assets/right.png';
import check from '../../../assets/check.png';
import check2 from '../../../assets/cart/check.png';
import shareImg from '../../../assets/wishList/share.png';
import { images } from '../../../images';
import icon_jingqingqidai from '../../../assets/mine/icon_jingqingqidai.png';
import uncheck from '../../../assets/submit/uncheck.png';
import none from '../../../assets/wishList/none.png';
import { shareAppMessage, commonNavigateTo } from '@/hooks/useShareAppMessage';
import {
  getDetail,
  IMsgInfo,
  getWiseThemeList,
  update,
  ThemeType,
  save,
  saveShare
} from '../../../services/wishList';
import { getAddress } from '../../../services/address';
import './index.scss';

interface IProps {}



const Index: Taro.FC<IProps> = ({}) => {
  const router = useRouter();
  const [edit, setEdit] = useState(false);
  const [open, setOpen] = useState(false);
  const [open1, setOpen1] = useState(false);
  const [addressStr, setaddressStr] = useState({ index: -1, id: '' });
  // const [user, setUser] = useState('');
  const [addressList, seAtddressList] = useState([]);
  const [theme, setTheme] = useState(0);
  const [themeList, setThemeList] = useState([] as ThemeType[]);
  const [show, setShow] = useState(false);
  const [info, setInfo] = useState({} as IMsgInfo);
  const [wishId,setWishId]=useState('')
  let payload = {
    id: router.params.id
  };
  let nickname = Taro.getStorageSync('isLogin').nickName;
  const getTheme = async () => {
    let res = await getWiseThemeList();
    if (res.code === 0) {
      setThemeList(res.data.wiseThemeList);
    }
  };
  // 删除
  const del = (index: number) => {
    let temp = { ...info };
    temp.productList.splice(index, 1);
    setInfo(temp);
  };
  // 保存
  const submit = async () => {
    if (!info.title) {
      Taro.showToast({ title: '请输入心愿单标题', icon: 'none' });
      return;
    }
    if (!info.wiseThemeId) {
      Taro.showToast({ title: '请选择主题', icon: 'none' });
      return;
    }
    let proList = [];
    let obj = {};
    info.productList &&
      info.productList.forEach((i) => {
        obj = {
          businessId: i.businessId,
          productId: i.productId,
          qty: 1,
          skuId: i.skuId,
          storeId: i.storeId
        };
        proList.push(obj);
      });
    if (proList.length === 0) {
     
      Taro.showToast({ title: '请添加商品', icon: 'none' });
      return;
    }
    // 更新
    let pay = {
      shareStatement: info.shareStatement,
      title: info.title,
      wiseThemeId: info.wiseThemeId,
      products: proList,
    };
    let method = save;
    console.log(router.params.id,'router.params.id')
    if (router.params.id !='undefined') {
      //更新
      pay.id = router.params.id;
      method = update;
    }
    // 新建
    let res = await method(pay);
    if (res.code === 0) {
      Taro.showToast({title:'操作成功'})
      setTimeout(() => {
        Taro.navigateTo({ url: '/pages/wishList/myWishList/index' });
      }, 3000);
    }
  };
  //获取地址
  const getAdd = async () => {
    let res = await getAddress({});
    if (res.code === 0) {
      seAtddressList(res.data.address);
    }
  };
  const share = async () => {
    let res = await saveShare({
      wishId: router.params.id,
      nickName: Taro.getStorageSync('isLogin').nickName,
      userAddressId: addressStr.id
    });
    if (res.code === 0) {
      if(process.env.TARO_ENV === 'h5'){
        shareAppMessage({
          title:'我的心愿单',
          link: `http://www.myutopa.com/m/campaign/#/activity/wishlist/sharelist?wishShareId=${res.data.id}`
        });
      }else{
        setWishId(res.data.id)
      }
    }
  };
  if (process.env.TARO_ENV === 'weapp') {
    useShareAppMessage((res) => {
      if (res.from === 'button') {
        // 来自页面内转发按钮
        console.log(res.target);
      }
      if(wishId){
        return {
          title:'我的心愿单',
          path:`http://www.myutopa.com/m/campaign/#/activity/wishlist/sharelist?wishShareId=${wishId}`
        };
      }
    });
  }
  const getInfo = async () => {
    let res = await getDetail(payload); 
    if (res.code == 0) {
      setInfo(res.data);
    }
  };
  const getTime = () => {
    let time = new Date();
    let y = time.getFullYear();
    let m = time.getMonth() + 1 > 10 ? time.getMonth() + 1 : '0' + (time.getMonth() + 1);
    let d = time.getDate() >= 10 ? time.getDate() : '0' + time.getDate();
    let h = time.getHours() >= 10 ? time.getHours() : '0' + time.getHours();
    let min = time.getMinutes() >= 10 ? time.getMinutes() : '0' + time.getMinutes();
    let sec = time.getSeconds() >= 10 ? time.getSeconds() : '0' + time.getSeconds();
    return `${y}-${m}-${d} ${h}:${min}:${sec}`;
  };
  useEffect(() => {
    if (router.params.back && router.params.id) {
      setEdit(true);
      setInfo(Taro.getStorageSync('infoBox'));
    }else if(router.params.id){
      getInfo();
    } else {
      setEdit(true);
    }
  }, []);
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          {/* <Bar title="心愿单" rightTitle="编辑" /> */}
          <Wrap Myheight={88} justifyContent="flex-end">
            {edit && (
              <Txt
                title="取消"
                size={28}
                color="low"
                onClick={() => {
                  Taro.navigateTo({
                    url: '/pages/wishList/myWishList/index'
                  });
                }}
              />
            )}
            <View
              className="utp-addWishList-edit"
              onClick={() => {
                setEdit(!edit);
                if (edit) {
                  submit();
                }
              }}
            >
              <Txt title={edit ? '保存' : '编辑'} size={28} color="deep" />
            </View>
          </Wrap>

          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-addWishList"
            scrollY
          >
            <View className="utp-addWishList-content">
              <Textarea
                autoHeight
                disabled={edit ? false : true}
                value={info.title}
                className="utp-addWishList-content-Title"
                placeholder="请输入少于20字的心愿单标题"
                placeholder-style={{ color: '#333' }}
                onInput={(e) => {
                  let temp = { ...info };
                  temp.title = e.detail.value;
                  setInfo(temp);
                }}
              />
              <Wrap justifyContent="space-between">
                <Text className="utp-addWishList-content-time">
                  创建时间：{info.createTime || getTime()}
                </Text>
                <Wrap
                  click={() => {
                    edit && getTheme() && setShow(true);
                  }}
                >
                  <Text className="utp-addWishList-content-sort">
                    {info.wiseTheme || '选择主题'}
                  </Text>
                  <Img src={right} width={40} />
                </Wrap>
              </Wrap>
              <Textarea
                disabled={edit ? false : true}
                value={info.shareStatement}
                className="utp-addWishList-content-textContent"
                onInput={(e) => {
                  let temp = { ...info };
                  temp.shareStatement = e.detail.value;
                  setInfo(temp);
                }}
              />
            </View>
            <View className="utp-addWishList-goods">
              <Wrap justifyContent="space-between">
                <Text className="utp-addWishList-goods-title">心愿商品</Text>
                {edit && (
                  <Text
                    className="utp-addWishList-goods-addGoods"
                    onClick={() => {
                      if (!info.wiseTheme) {
                        //新建
                        if (!info.wiseTheme) {
                          Taro.showToast({
                            title: '请先选择主题',
                            icon: 'none'
                          });
                          return;
                        } else {
                          Taro.setStorage({
                            key: 'infoBox',
                            data: info
                          });
                          Taro.navigateTo({
                            url: `/pages/wishList/selectProduct/index?wiseThemeId=${info.wiseThemeId}&title=${info.wiseTheme}`
                          });
                        }
                      } else {
                        // 更新
                        Taro.setStorage({
                          key: 'infoBox',
                          data: info
                        });
                        Taro.navigateTo({
                          url: `/pages/wishList/selectProduct/index?id=${router.params.id}&wiseThemeId=${info.wiseThemeId}&title=${info.wiseTheme}`
                        });
                      }
                    }}
                  >
                    添加商品
                  </Text>
                )}
              </Wrap>
              {info.productList &&
                info.productList.length > 0 &&
                info.productList.map((p, pp) => {
                  return (
                    <View key={pp} className="utp-addWishList-goods-item">
                      <Goods1 item={p} isAdd />
                      {edit && (
                        <Image
                          src={images.sendMsg_clo}
                          className="utp-addWishList-goods-del"
                          onClick={() => {
                            del(pp);
                          }}
                        />
                      )}
                    </View>
                  );
                })}
              {info.productList && info.productList.length == 0 && (
                <View className="utp-addWishList-empty">
                  <Wrap flexDirection="column">
                    <Img src={icon_jingqingqidai} width={400} height={300} />
                    <Txt title="还没有添加商品到心愿单哦~" color="low" size={30} />
                  </Wrap>
                </View>
              )}
            </View>
          </ScrollView>
          {show && (
            <View
              className="utp-addWishList-mask"
              onClick={() => {
                setShow(false);
              }}
            >
              <View className="utp-addWishList-mask-content">
                {themeList.length > 0 &&
                  themeList.map((e, ee) => {
                    return (
                      <Wrap
                        justifyContent="space-between"
                        Myheight={100}
                        top
                        key={ee}
                        click={() => {
                          let temp = { ...info };
                          temp.wiseTheme = e.title;
                          temp.wiseThemeId = e.id;
                          temp.shareStatement = e.themeStatement;
                          setTheme(ee);
                          setInfo(temp);
                        }}
                      >
                        <Txt title={e.title} size={30} color={theme === ee ? 'active' : 'black'} />
                        {theme === ee && <Img src={check} width={36} />}
                      </Wrap>
                    );
                  })}
              </View>
            </View>
          )}
          <Message
            show={open}
            type={1}
            title="选择收货地址"
            rightBtn="下一步"
            rightBtnColor={addressList.length > 0 ? '#FF2F7B' : '#ddd'}
            onClose={() => {
              setOpen(false);
            }}
            onConfirm={() => {
              if (addressList.length > 0) {
                if (addressStr.id) {
                  setOpen1(true);
                  setOpen(false);
                } else {
                  Taro.showToast({ title: '请选择地址', icon: 'none' });
                }
              }
            }}
          >
            <View className="utp-addWishList-mask-addBox">
              {addressList && addressList.length > 0 ? (
                addressList.map((i, index) => {
                  return (
                    <Wrap
                      justifyContent="space-between"
                      className="utp-addWishList-mask-addressBox"
                      key={i.id}
                      click={() => {
                        if (index !== addressStr.index) {
                          setaddressStr({ index, id: i.id });
                        } else {
                          setaddressStr({ index: -1, id: '' });
                        }
                      }}
                    >
                      <Wrap flexDirection="column" justifyContent="space-between">
                        <Text className="utp-addWishList-mask-address1">{i.address}</Text>
                        <Wrap
                          justifyContent="flex-start"
                          className="utp-addWishList-mask-addressbtm"
                        >
                          <Txt title={i.phone} size={26} color="black" />
                          <View style={{ width: 10 }} />
                          <Txt title={i.userName} size={26} color="black" />
                        </Wrap>
                      </Wrap>
                      <Img src={addressStr.index === index ? check2 : uncheck} width={36} />
                    </Wrap>
                  );
                })
              ) : (
                <Wrap flexDirection="column">
                  <Img src={none} width={500} />
                  <Txt title="先创建一个收货地址吧" size={30} color="low" />
                </Wrap>
              )}
            </View>
          </Message>
          <Message
            show={open1}
            title="选择分享呢称"
            type={1}
            rightBtnColor="#FF2F7B"
            width={600}
            onConfirm={() => {
              setOpen1(false);
              share();
            }}
          >
            <View className="utp-addWishList-mask-nickname">
              {[0].map((i) => {
                return (
                  <Wrap top className="utp-addWishList-mask-nickname-item" key={i}>
                    <Img src={check2} width={36} />
                    <Wrap
                      className="utp-addWishList-mask-nickname-right"
                      flexDirection="column"
                      top
                    >
                      <Txt title="账号昵称" color="black" size={30} height={40} bold />
                      <View style={{ height: 5 }} />
                      <Txt title={nickname} color="black" size={26} height={40} />
                    </Wrap>
                  </Wrap>
                );
              })}

              <Wrap top>
                <Img src={check2} width={36} />
                <Wrap className="utp-addWishList-mask-nickname-right" flexDirection="column" top>
                  <Txt title="自定义昵称" color="black" size={30} height={40} bold />
                  <View style={{ height: 5 }} />
                  <Input
                    placeholder="输入自定义昵称"
                    className="utp-addWishList-mask-nickname-input"
                  />
                </Wrap>
              </Wrap>
            </View>
          </Message>
          {router.params.id && !edit && (
            <View
              className="utp-addWishList-share"
              onClick={() => {
                setOpen(true);
                getAdd();
              }}
            >
              <Img src={shareImg} width={128} />
            </View>
          )}
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};
export default Index;
