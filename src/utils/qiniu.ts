import Taro from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { upload } from './qiniuUploader';

var qiniu = require('qiniu-js');
const Qiniu = (token, file) => {
  return new Promise((r, e) => {
    if (process.env.TARO_ENV === 'weapp') {
      let fileName =
        Number(Math.random() * 10000).toFixed(0) +
        new Date().getTime() +
        Number(Math.random() * 10000).toFixed(0);
      upload({
        filePath: file,
        options: {
          key: fileName, // 可选
          region: 'SCN', // 可选(默认为'ECN')
          domain: '',
          uptoken: token, // 以下三选一
          uptokenURL: '',
          uptokenFunc: () => {
            return '[yourTokenString]';
          },
          shouldUseQiniuFileName: true // 默认true
        },
        before: () => {
          // 上传前
          console.log('上传图片');
          console.log(file);
          console.log(fileName);
          console.log(token);
        },
        success: (res) => {
          console.log('上传成功');
          console.log(res);
          let hash = res.hash;
          r(hash);
        },
        fail: () => {
          e();
          Taro.showToast({
            title: '上传图片出错，请重试!',
            icon: 'none'
          });
        },
        progress: () => {},
        complete: () => {}
      });
    } else {
      var putExtra = {
        fname: '',
        params: {},
        mimeType: [] || null
      };
      var config = {
        useCdnDomain: true,
        region: qiniu.region.z2
      };

      let fileName =
        Number(Math.random() * 10000).toFixed(0) +
        new Date().getTime() +
        Number(Math.random() * 10000).toFixed(0);
      var observable = qiniu.upload(file, fileName, token, putExtra, config);
      observable.subscribe(
        () => {},
        () => {
          e();
          Taro.showToast({
            title: '上传图片出错，请重试!',
            icon: 'none'
          });
        },
        async (res) => {
          r(res.key);
        }
      );
    }
  });
};

export default Qiniu;
