import Taro, { useState, useEffect } from '@tarojs/taro';
import { View } from '@tarojs/components';

import './index.scss';

interface Props {}
const Index: Taro.FC<Props> = ({}) => {
  const [init] = useState(true);
  useEffect(() => {
    alert(1);
    console.warn('INDEX init');
  }, [init]);
  return <View className="index">index</View>;
};
Index.defaultProps = {};

export default Index;
