import Taro, { useEffect, useState, usePageScroll } from '@tarojs/taro';
import { View, Text } from '@tarojs/components';

import './index.scss';
//import Schh from '../../components/schh';
import Sch from '../../components/sch';
//import Wrap from '../../components/Wrap';
//import Address from '../../components/Address';
//import Goods4 from '../../components/Goods4';
//import ShoppingGoods from '../../components/shopGoods';
//import AddCart from '../../components/AddCart';
//import CartSub from '../../components/CartSub';
import Swipe from '../../components/Swipe';
import Icons from '../../components/Icons';
import Img from '../../components/Img';
import Wrap from '../../components/Wrap';
import Txt from '../../components/Txt';
import Goods2s from '../../components/Goods2s';
import { connect } from '@tarojs/redux';
import Loading from '../../components/Loading';
import LoadingMore from '../../components/LoadingMore';
import LoadingNone from '../../components/LoadingNone';
import LoadingNoneMore from '../../components/LoadingNoneMore';
//import ItemHomeIcon1 from '../../components/ItemHomeIcon1';
//import useSession from '../../hooks/useSession';
//import Router from '../../utils/router';
import png1 from '../../assets/home/icon_wanqianhaowu@2x1.png';
import png2 from '../../assets/home/icon_wanqianhaowu2@2x.png';
import png3 from '../../assets/home/icon_wanqianhaowu3@2x.png';
import png4 from '../../assets/home/icon_wanqianhaowu4@2x.png';
//import Qrcode from '../../components/Qrcode';
import { ISwipe, IIcons, IGoods } from '../../interface/home';
import { getSecMore, IHome, ISec, getHome, getSec } from '../../services/home';
import useRequest from '../../hooks/useRequest';
import BigImg from './BigImg';
import Emini from './Emini';
import EminiFour from './EminiFour';
import useRequestWIthMore from '../../hooks/useRequestWIthMore';

type PageStateProps = {
  user: {
    name: string;
  };
  example: any;
  loading: boolean;
  statusHeight: number;
  closeWidth: number;
  unread: number;
  adsTitle: string;
};

type PageDispatchProps = {
  wait: () => void;
  clickEffict: () => void;
  toGetGoods: () => void;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

//let cishu: number = 0;
const Index: Taro.FC<IProps> = (props) => {
  let tagList = [
    { title: '万千好物', src: png1 },
    { title: '价优保障', src: png4 },
    { title: '实体体验', src: png2 },
    { title: '即时送达', src: png3 }
  ];
  // 首页数据
  // @ts-ignore
  const [home, update, loading] = useRequest<IHome>({}, getHome);
  // 首页数据
  // @ts-ignore
  const [sec, update1, loading1] = useRequest<ISec>({}, getSec);
  if (props.loading) {
    Taro.showLoading({
      title: 'loading'
    });
  } else {
    Taro.hideLoading();
  }
  const [opacity, setOpacity] = useState(0);
  usePageScroll((e) => {
    let size = e.scrollTop;
    let opa = size / (props.statusHeight + 44);
    if (opa > 1 && opacity !== 1) {
      setOpacity(opa);
    }
    if (opa <= 1) {
      setOpacity(opa);
    }
  });

  const [start, setStart] = useState('start%%');
  const [id, setId] = useState(0);
  // @ts-ignore
  const [state, hasMore, update11, getMoreData, loading6] = useRequestWIthMore<any>(
    start,
    getSecMore
  );
  useEffect(() => {
    if (sec && sec.code === 0) {
      setId(sec.data.customCategoryList[0].foreCategoryId);
    }
  }, [sec]);
  useEffect(() => {
    if (id) {
      setStart('start1%%' + id);
      if (update11) {
        update11({});
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  const [goods, setGoods] = useState<IGoods[]>([]);
  useEffect(() => {
    if (state && state.length > 0) {
      let arr: IGoods[] = [];
      state.map((item) => {
        item.data.records.map((item) => {
          arr.push(item);
        });
      });
      if (arr.length > 0) {
        setGoods(arr);
      }
    }
  }, [state]);
  console.log('renderIndex');

  return (
    <View className="utp-html">
      {loading && (
        <View className="utp-body">
          <View className="utp-bd" style={{ backgroundColor: '#f5f5f5' }}>
            <View
              style={{ paddingTop: Taro.pxTransform(100), width: Taro.pxTransform(750) }}
            ></View>
            <Loading />
          </View>
        </View>
      )}

      {!!home && home.code === 0 && (
        <View className="utp-body">
          <View
            className="utp-set2"
            style={{
              opacity: opacity,
              paddingTop: props.statusHeight + 'px',
              height: Taro.pxTransform(props.statusHeight * 2 + 88)
            }}
          >
            <Sch adsTitle={props.adsTitle} unread={props.unread} closeWidth={props.closeWidth} />
          </View>
          <View className="utp-bd" style={{ backgroundColor: '#f5f5f5' }}>
            <View className="utp-set1 utp-set1-1 utp-flex utp-flex-db" style={{}}>
              <View
                className="utp-set1-2 utp-flex utp-flex-db"
                style={{
                  paddingTop: props.statusHeight + 'px',
                  backgroundColor: 'transparent'
                }}
              >
                <Sch
                  type={2}
                  adsTitle={props.adsTitle}
                  unread={props.unread}
                  closeWidth={props.closeWidth}
                  onScan={() => {}}
                />
              </View>
            </View>

            {
              // 轮播start
            }
            {!!home &&
              home.code === 0 &&
              home.data.advertisementSpaceList.map((item) => {
                if (!item) {
                  return;
                }
                if (item.code == 'utopa_homepage_top_banner') {
                  item.advertisementList.map((item1) => {
                    item1.source = item1.image;
                  });
                  return <Swipe data={item.advertisementList as ISwipe[]} />;
                }
              })}
            {
              // 轮播end
            }

            {
              // 4小图标start
            }
            <View className="utp-home-fours">
              <View className="utp-home-tags">
                <View className="utp-home-tags-tagsbox">
                  {tagList.map((i, ii) => {
                    return (
                      <View key={ii} className="utp-home-tags-items">
                        <Img width={32} src={i.src} />
                        <Text className="utp-home-tags-title">{i.title}</Text>
                      </View>
                    );
                  })}
                </View>
              </View>
              <View className="utp-home-fours-icos">
                {!!home &&
                  home.code === 0 &&
                  home.data.memberConfList.map((item) => {
                    if (!item) {
                      return;
                    }
                    if (item.groupCode == 'top') {
                      return <EminiFour item={item.items} />;
                    }
                  })}
              </View>
            </View>
            {
              // 4小图标end
            }

            {
              // 小图标start
            }
            {!!home &&
              home.code === 0 &&
              home.data.memberConfList.map((item) => {
                if (item.groupCode === 'myApp') {
                  item.items.map((item1) => {
                    item1.source = item1.iconUrl;
                  });
                  return <Icons data={item.items as IIcons[]} />;
                }
              })}

            {
              // 小图标end
            }
            {
              // 大图广告和8块图片 start
            }
            {!!home &&
              home.code === 0 &&
              home.data.advertisementSpaceList.map((item) => {
                if (!item) {
                  return;
                }
                if (item.code == 'utopa_index_banner') {
                  return <BigImg arr={item.advertisementList} />;
                }
              })}
            {!!home &&
              home.code === 0 &&
              home.data.advertisementSpaceList.map((item) => {
                if (!item) {
                  return;
                }
                if (item.code == 'utopa_homepage_activity_banner') {
                  return <Emini arr={item.advertisementList} />;
                }
              })}
            {
              // 大图广告和8块图片end
            }
            <View className="utp-homeCate">
              <Wrap justifyContent="space-between">
                {sec &&
                  sec.code === 0 &&
                  sec.data.customCategoryList.map((item, index) => {
                    return (
                      <View
                        className="utp-homeCate-item"
                        onClick={() => {
                          if (loading6) {
                            return;
                          }
                          setId(item.foreCategoryId);
                        }}
                      >
                        {index !== 4 && (
                          <View className="utp-homeCate-item-gangs">
                            <View className="utp-homeCate-item-gang"></View>
                          </View>
                        )}
                        <View>
                          <Txt
                            title={item.name}
                            size={32}
                            color={id === item.foreCategoryId ? 'red' : 'deep'}
                          />
                          <View
                            className={
                              'utp-homeCate-item-tag ' +
                              (id === item.foreCategoryId ? 'utp-homeCate-item-tag-crt' : '')
                            }
                            style={{ marginTop: Taro.pxTransform(6) }}
                          >
                            <Txt
                              title={item.subTitle}
                              size={24}
                              color={id === item.foreCategoryId ? 'white' : 'low'}
                            />
                          </View>
                        </View>
                      </View>
                    );
                  })}
              </Wrap>
            </View>

            {
              // 商品start
            }
            {goods.length === 0 && (
              <View style={{ backgroundColor: '#f4f4f4' }}>
                {loading6 && goods.length === 0 && <Loading />}
                {!loading6 && goods.length === 0 && <LoadingNone />}
              </View>
            )}
            <Goods2s data={goods as IGoods[]} />

            {loading6 && goods.length !== 0 && <LoadingMore />}
            {!loading6 && !hasMore && goods.length > 0 && <LoadingNoneMore />}

            {
              //!!sec &&
              //sec.code === 0 &&
              //sec.data.customCategoryList.map((item) => {
              //return <Goods2s data={item.items as IGoods[]} />;
              //})
            }
            {
              // 商品end
            }
          </View>
        </View>
      )}
    </View>
  );
};
Index.config = {
  navigationBarTitleText: '首页',
  disableScroll: false,
  navigationStyle: 'custom'
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      name: state.user.name
    },
    example: state.example,
    statusHeight: state.cart.statusHeight,
    closeWidth: state.cart.closeWidth,
    unread: state.cart.unread,
    adsTitle: state.cart.adsTitle,
    loading: state.loading.effects['cart/addCart'] || state.loading.effects['sync/addCart']
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    wait: () => {
      dispatch({
        type: 'example/save',
        payload: {
          name: 'sdfqwiejfwie'
        }
      });
    },
    clickEffict: () => {
      dispatch({
        type: 'example/wait'
      });
    },
    toGetGoods: () => {
      dispatch({
        type: 'example/getGoods'
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
