import Taro, { useState } from '@tarojs/taro';
import { View, ScrollView, Text } from '@tarojs/components';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import Tab from '../../../components/Tab';
import Bar from '../../../components/bar';
import Yuan from '../../../components/Yuan';
import Time1 from '../../../assets/mine/icon_shouhou_list.png';
import Time2 from '../../../assets/mine/icon_shouhou_time_grey.png';
import useSession from '../../../hooks/useSession';
import './index.scss';
import useLogin from '../../../hooks/useLogin';
import useResetCount from '../../../hooks/useResetCount';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import Search from './search';
import ItemTopStatus from './ItemTopStatus';
import DayCountDown from './DayCountDown';
import Goods from './Goods';
import Bton from './Bton';
import ItemMoewScrollView from './ItemMoewScrollView';
import {
  AfterProps,
  myAfterSaleProductOrders,
  myAfterSaleAndSalingPage,
  recallAfterSaleOrdere
} from '../../../services/afterSale';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const [serviceTabIndex, setserviceTabIndex] = useState(0);
  const [searchContent, setSearchContent] = useState('');
  const [haveSession] = useSession();
  const [isLogin, updateLogin] = useLogin();
  if (haveSession && !isLogin) {
    if (updateLogin) {
      updateLogin();
    }
  }

  // 请求列表数据
  const [state, hasMore, run, getMoreData, loading] = useRequestWIthMore(
    '999',
    myAfterSaleProductOrders
  );
  if (false) {
    console.log(getMoreData);
  }
  const [can, update] = useResetCount();
  if (
    haveSession &&
    isLogin &&
    (state === null || (state != null && state.length == 0)) &&
    run &&
    !loading
  ) {
    if (can) {
      run('');
      update();
    }
  }

  const [state1, hasMore1, run1, getMoreData1, loading1] = useRequestWIthMore(
    { searchContent },
    myAfterSaleAndSalingPage
  );
  if (false) {
    console.log(getMoreData1);
  }
  const [can1, update1] = useResetCount();
  if (
    haveSession &&
    isLogin &&
    (state1 === null || (state1 != null && state1.length == 0)) &&
    run1 &&
    !loading1
  ) {
    if (can1) {
      run1('');
      update1();
    }
  }

  // @ts-ignore
  const withdraw = (payload) => {
    Taro.showModal({
      content: '确定撤回申请吗？',
      success: (res) => {
        if (res.confirm) {
          //确定
          recall(payload);
        }
      }
    });
  };
  const changeTime = (t: number) => {
    if (t) {
      let time = new Date(t);
      let y = time.getFullYear();
      let m = time.getMonth() + 1 > 10 ? time.getMonth() + 1 : '0' + (time.getMonth() + 1);
      let d = time.getDate() > 10 ? time.getDate() : '0' + time.getDate();
      return `${y}-${m}-${d}`;
    }
    return '';
  };
  // 撤销售后
  const recall = async (payload) => {
    let res = await recallAfterSaleOrdere({ afterSaleNo: payload });
    if (res.code === 0) {
      Taro.showToast({ icon: 'none', title: '撤销成功' });
      if (serviceTabIndex === 0) {
        run && run('');
      } else {
        run1 && run1('');
      }
    }
  };
  var list: AfterProps[] = [];
  if (state && state.length > 0 && state[0].data.records) {
    console.log(state[0].data.records);
    state.map((item) => {
      item.data.records.map((it) => {
        list.push(it);
      });
    });
  }

  var rightList: AfterProps[] = [];
  if (state1 && state1.length > 0 && state1[0].data.records) {
    state1.map((item) => {
      item.data.records.map((it) => {
        rightList.push(it);
      });
    });
    // rightList[0].products.push(rightList[0].products[0]);
  }

  // const refundWords = {
  //   1: '待商家审核',
  //   2: '商家已同意,系统退款中',
  //   3: '待买家退回商品',
  //   4: '商家已拒绝',
  //   5: '待商家收货处理退款',
  //   6: '商家已同意，系统退款中'
  // };
  const statusStr = {
    0: '待商家审核',
    1: '商家已拒绝，您需在',
    2: '待买家退回商品，您需在',
    3: '待商家收货处理退款，商家需在',
    4: '商家已拒绝，您需在',
    5: '商家已同意，系统退款中',
    6: '退款成功',
    7: '退款关闭'
  };
  /*
   top 订单status样式
  */
  // eslint-disable-next-line react/no-multi-comp
  /*
   bottom 订单text样式
  */
  // const ItemBottomStatus = (props: any) => {
  //   const { type } = props;
  //   let compose: any;
  //   try {
  //     compose = <Text>{refundWords[type]}</Text>;
  //     return compose;
  //   } catch (e) {
  //     return null;
  //   }
  // };
  /*
   订单倒计时
  */

  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-applyService">
          <Bar title="售后" />
          <Tab
            style={{ background: '#fff' }}
            barList={['申请售后', '申请记录']}
            defaultActiveKey={serviceTabIndex}
            onChange={(inn) => {
              setserviceTabIndex(inn);
            }}
          >
            {loading && list.length === 0 && serviceTabIndex === 0 && <Loading />}
            {!loading && list.length === 0 && serviceTabIndex === 0 && <LoadingNone />}
            {loading1 && rightList.length === 0 && serviceTabIndex === 1 && <Loading />}
            {!loading1 && rightList.length === 0 && serviceTabIndex === 1 && <LoadingNone />}
            <View
              className="utp-applyService-wrapBox"
              style={{ marginTop: serviceTabIndex === 1 ? 40 : 0 }}
            >
              <Wrap type={2} flexDirection="column">
                <View>
                  <ScrollView
                    refresherEnabled
                    refresherBackground="#333"
                    className="utp-bd utp-bd-page"
                    scrollY
                    style={{
                      left: 0
                    }}
                  >
                    {serviceTabIndex === 1 && (
                      <View
                        style={{
                          position: 'fixed',
                          top: '40px',
                          zIndex: 4555,
                          width: '100%'
                        }}
                      >
                        {serviceTabIndex === 1 && (
                          <Search
                            value="搜索"
                            onChange={(v) => {
                              console.log(v);
                            }}
                            onConfirm={(v) => {
                              setSearchContent(v);
                              run1 && run1({});
                            }}
                          ></Search>
                        )}
                      </View>
                    )}
                    {serviceTabIndex === 0 &&
                      list.map((item, index) => {
                        return (
                          <View className="utp-applyService-serviceGoods" key={index}>
                            <View className="utp-applyService-serviceTimes">
                              <Wrap type={1} justifyContent="space-between">
                                <View className="utp-applyService-serviceTimes-time">
                                  <Wrap type={1}>
                                    <Img src={Time1} width={28}></Img>
                                    <Text style={{ marginLeft: 3 }}>{item.orderNo}</Text>
                                  </Wrap>
                                </View>
                                <View className="utp-applyService-serviceTimes-time">
                                  <Wrap type={1} justifyContent="space-between">
                                    <Img src={Time2} width={28}></Img>
                                    <Text>{changeTime(item.createTime)}</Text>
                                  </Wrap>
                                </View>
                              </Wrap>
                            </View>

                            <View className="utp-applyService-goodsInfo" onClick={() => {}}>
                              <Wrap justifyContent="space-between">
                                {!!item && !!item.products && item.products.length === 1 && (
                                  <Wrap type={1}>
                                    <View className="utp-applyService-goodsInfo-imgs">
                                      <Img src={item.products[0].picIds} width={170} />
                                    </View>
                                    <View className="utp-applyService-goodsInfo-titles">
                                      <Text className="utp-applyService-goodsInfo-title">
                                        {item.products[0].name}
                                      </Text>
                                      <Text className="utp-applyService-goodsInfo-size">
                                        {item.products[0].norm1Value}
                                      </Text>
                                      <View className="utp-applyService-goodsInfo-price">
                                        <Yuan
                                          price={item && item.products[0].price}
                                          color="#FF2F7B"
                                          size={28}
                                        ></Yuan>
                                      </View>
                                    </View>
                                  </Wrap>
                                )}
                                <Goods item={item} />
                                <Wrap>
                                  <View className="utp-applyService-goodsInfo-tips">
                                    <Txt title={`共${item.goodsNum}件`} size={26} />
                                  </View>
                                </Wrap>
                              </Wrap>
                            </View>

                            <Bton item={item} />
                          </View>
                        );
                      })}
                    {serviceTabIndex === 1 &&
                      rightList.map((itt: any, imm) => {
                        return (
                          <View className="utp-applyService-serviceGoods" key={imm}>
                            <View className="utp-applyService-serviceTimes">
                              <Wrap type={1} justifyContent="space-between">
                                <View className="utp-applyService-serviceTimes-time">
                                  <Wrap type={1}>
                                    {!!itt && !!itt.refundType && (
                                      <ItemTopStatus type={itt.refundType} />
                                    )}
                                  </Wrap>
                                </View>
                                <View
                                  className="utp-applyService-serviceTimes-time"
                                  style={{ color: '#333333', fontSize: '14px' }}
                                >
                                  <Wrap type={1}>
                                    退款金额 :<Text>￥{(itt.refundMoney / 100).toFixed(2)}</Text>
                                    {/* <Img src={Time2} width={28}></Img>
                                    <Text>下单时间：{changeTime(itt.createTime)}</Text> */}
                                  </Wrap>
                                </View>
                              </Wrap>
                            </View>
                            <View
                              className="utp-applyService-goodsInfo"
                              onClick={() => {
                                console.log(itt);
                                // Taro.setStorage({
                                //   key: 'product',
                                //   data: itt
                                // });
                                Taro.navigateTo({
                                  url: `/pages/person/refundDetail/index?refundOrderNo=${itt.refundOrderNo}`
                                });
                              }}
                            >
                              <Wrap justifyContent="space-between">
                                {itt.refundSkuDTOList && itt.refundSkuDTOList.length === 1 && (
                                  <Wrap type={1}>
                                    <View className="utp-applyService-goodsInfo-imgs">
                                      <Img src={itt.refundSkuDTOList[0].pics || ''} width={170} />
                                    </View>
                                    <View className="utp-applyService-goodsInfo-titles">
                                      <Text className="utp-applyService-goodsInfo-title">
                                        {itt.refundSkuDTOList[0].productName || ''}
                                      </Text>
                                      <Text className="utp-applyService-goodsInfo-size">
                                        {itt.refundSkuDTOList[0].skuInfo || ''}
                                      </Text>
                                      <View className="utp-applyService-goodsInfo-price"></View>
                                      <Txt
                                        title={`退${itt.refundSkuDTOList[0].refundnums || 0}件`}
                                        color="deep"
                                        size={24}
                                        justifyContent="flex-start"
                                      />
                                    </View>
                                  </Wrap>
                                )}
                                {itt.refundSkuDTOList && itt.refundSkuDTOList.length > 1 && (
                                  <ItemMoewScrollView item={itt} />
                                )}
                                {/* <Wrap>
                                  <View className="utp-applyService-goodsInfo-tips">
                                    <Txt title={`共${itt.goodsTotalNum}件`} />
                                  </View>
                                </Wrap> */}
                              </Wrap>
                            </View>

                            <View
                              style={{ textAlign: 'center' }}
                              className="utp-applyService-ServiceBtn"
                            >
                              <View>
                                {[1, 2, 3, 4].includes(itt.status) ? (
                                  <View>
                                    {
                                      <DayCountDown
                                        status={itt.status || ''}
                                        timeDisparity={itt.timeDisparity}
                                      />
                                    }
                                  </View>
                                ) : (
                                  <Text style={{ display: 'inline-block', fontSize: '13px' }}>
                                    {statusStr[itt.status] || itt.underlabel}
                                  </Text>
                                )}
                                {/* <ItemBottomStatus type={itt.type} /> */}
                                {/* <Text className="utp-applyService-tips">
                                    状态：{itt.statusDetail}
                                  </Text>
                                  <Txt title={itt.closeReason} size={24} color="low" /> */}
                              </View>

                              {/* <Wrap>
                                <Text
                                  style={{
                                    display: itt.showRecallButton === 1 ? 'block' : 'none'
                                  }}
                                  className={
                                    itt.recalled
                                      ? 'utp-applyService-Btn utp-applyService-disableBtn'
                                      : 'utp-applyService-Btn'
                                  }
                                  onClick={() => {
                                    withdraw(itt.afterSaleNo);
                                  }}
                                >
                                  撤回申请
                                </Text>
                              </Wrap> */}
                            </View>
                          </View>
                        );
                      })}
                  </ScrollView>
                </View>
              </Wrap>
            </View>
            {loading && list.length !== 0 && serviceTabIndex === 0 && <LoadingMore />}
            {!loading && !hasMore && serviceTabIndex === 0 && <LoadingNoneMore />}
            {loading1 && rightList.length !== 0 && serviceTabIndex === 1 && <LoadingMore />}
            {!loading1 && !hasMore1 && serviceTabIndex === 1 && <LoadingNoneMore />}
          </Tab>
        </View>
      </View>
    </Safe>
  );
};
Index.config = {
  navigationBarTitleText: '退款/售后',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};

Index.defaultProps = {};
export default Index;
