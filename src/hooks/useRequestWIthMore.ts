import Taro, {
  useState,
  useEffect,
  useRef,
  useReachBottom,
  usePullDownRefresh
} from '@tarojs/taro';

import { defaultParams, REACH_BOTTOM_EVENT, PULL_DOWN_REFRESH_EVENT } from '../constants';
//import Taro from '@tarojs/taro';
import events from '@/utils/event_bus';
var debounce = require('lodash/debounce');

function useRequestWIthMore<T, S = string | any>(
  data: S,
  request: (data: S, params: any | null) => Promise<T | null>,
  par = {}
): [T[] | null, boolean, (any) => void, () => void, boolean] | [] {
  if (!data && data !== 0) {
    // bug?
    console.warn('useRequestWIthMore: no data');
    return [];
  }

  const [currData, setData] = useState<T[] | null>(null);
  const [hasMore, setHasMore] = useState<boolean>(true);
  const [params, setParams] = useState(Object.assign(par, defaultParams));
  // 存储唯一 id 用于匹配消息
  const pageReachBottomRef = useRef('');
  const pagePullDownRef = useRef('');
  const loadingRef = useRef(false);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    console.log('加载更多0');
    // @ts-ignore
    if (data && data.toString().indexOf('start') > -1) {
      // @ts-ignore
      if (data.toString().indexOf('start1') > -1) {
      } else {
        // console.log('停止');
        // console.log(data);
        return;
      }
    }
    console.log('加载更多1');
    if (hasMore) {
      loadingRef.current = true;
      setLoading(true);
      console.log(data, '加载更多2');
      request(data, params).then((data) => {
        // console.log(data, '----data-');
        // @ts-ignore
        if (data && data.code == 0) {
          if (currData) {
            setData([...currData, data]);
          } else {
            setData([data]);
          }
          // @ts-ignore
          if (
            data &&
            data.data &&
            ((data.data.records && data.data.records.length === 0) ||
              (data.data.items && data.data.items.length == 0) ||
              (data.data.page && data.data.page.records && data.data.page.records.length == 0))
          ) {
            loadingRef.current = false;
            setHasMore(false);
          }

          if (data && data.data && data.data.records === null) {
            loadingRef.current = false;
            setHasMore(false);
          }
          if (data && data.data && data.data.orders && data.data.orders.length === 0) {
            loadingRef.current = false;
            setHasMore(false);
          }
          if (data && data.result && data.result.records && data.result.records.length === 0) {
            loadingRef.current = false;
            setHasMore(false);
          }
        } else {
          Taro.showToast({
            title: (data && data.msg) || '系统异常',
            icon: 'none'
          });
        }
        loadingRef.current = false;
        setLoading(false);
        Taro.stopPullDownRefresh();
        Taro.hideLoading();
      });
      //.finally(() => {
      //loadingRef.current = false;
      //setLoading(false);
      //Taro.stopPullDownRefresh();
      //Taro.hideLoading();
      //});
    }
  }, [params]);

  usePullDownRefresh(() => {
    refresh();
  });

  useEffect(() => {
    events.on(REACH_BOTTOM_EVENT, (page: string) => {
      if (loadingRef.current) {
        return;
      }
      if (!pageReachBottomRef.current) {
        pageReachBottomRef.current = page;
      } else if (pageReachBottomRef.current !== page) {
        return;
      }
      getMoreData();
    });
    return () => {
      events.off(REACH_BOTTOM_EVENT);
    };
  }, []);

  useEffect(() => {
    // console.log('加载更多初始化');
    events.on(PULL_DOWN_REFRESH_EVENT, (page: string) => {
      //console.log(pagePullDownRef.currentm '--pagePullDownRef.current---')
      if (!pagePullDownRef.current) {
        pagePullDownRef.current = page;
      } else if (pagePullDownRef.current !== page) {
        return;
      }
      refresh();
    });
    return () => {
      events.off(PULL_DOWN_REFRESH_EVENT);
    };
  }, []);

  useReachBottom(() => {
    if (data && data.toString()) {
      console.log(data.toString());
    }
    moreOne(() => {
      if (!loading) {
        getMoreData();
      }
    });
  });

  const getMoreData = () => {
    setParams((params) => ({ ...params, page: params.page! + 1 }));
  };

  const refresh = (par?: any) => {
    console.log('刷新刷新');
    // console.log('搜索');
    // console.log(par);
    setData(null);
    setHasMore(true);
    setParams({ ...params, page: 1, ...par });
  };

  return [currData, hasMore, refresh, getMoreData, loading];
}
let moreOne = debounce((cb: any) => {
  cb();
}, 60);

export default useRequestWIthMore;
