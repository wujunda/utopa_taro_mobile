/* eslint-disable import/first */
/* eslint-disable no-shadow */
import Taro, { useRouter } from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import { COMMON_IMAGES } from '../../common/images';
import Wrap from '@/components/Wrap';
// eslint-disable-next-line import/first
import '../index.scss';

import { PriceFormat } from '@/utils/utils';

const ColumnGoodsItem = ({ item, addCard, goGoods }) => {
  return (
    <View className="goods-item">
      <View
        className="goods-item-img"
        onClick={(e) => {
          e.stopPropagation();
          goGoods && goGoods();
        }}
      >
        <Image src={item.productImage} className="img"></Image>
      </View>
      <View className="goods-item-content">
        <View className="name">{item.productName}</View>
        {/* <View className="tag">优惠券</View> */}
        <View className="bottom">
          <Wrap type={1} flexDirection="row" top justifyContent="space-between">
            <View className="price">
              <Wrap type={2}>
                <View className="price-tag">￥</View>
                <View className="price-num">{PriceFormat(item.salePrice)}</View>
              </Wrap>
            </View>
            <View
              className="add-btn"
              onClick={() => {
                addCard && addCard();
              }}
            >
              <Image
                src={COMMON_IMAGES.add_card}
                style={{
                  width: Taro.pxTransform(44),
                  height: Taro.pxTransform(44),
                  marginTop: -2
                }}
              ></Image>
            </View>
          </Wrap>
        </View>
      </View>
    </View>
  );
};
export default ColumnGoodsItem;
