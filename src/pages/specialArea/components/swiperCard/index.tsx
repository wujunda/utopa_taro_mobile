/* eslint-disable react/jsx-key */
/* eslint-disable no-unused-vars */

import Taro, { useState } from '@tarojs/taro';
import { View, Swiper, SwiperItem, Image } from '@tarojs/components';
// import Img from '@/components/Img';
import './index.scss';

interface Props {
  onClick?: (e: any) => void;
  vertical?: boolean;
  height?: number;
  sourceData: any[];
  background?: string;
  interval?: number;
  customKey?: any;
}

const SwiperCard: Taro.FC<Props> = ({
  onClick,
  vertical = false,
  height,
  sourceData,
  background,
  interval,
  customKey = 'image'
}) => {
  const HEIGHT = Taro.pxTransform(height as any) || Taro.pxTransform(280);
  return (
    <View
      className="swiper-box"
      style={{
        width: '100%',
        height: HEIGHT,
        position: 'relative',
        background: background || ''
      }}
    >
      <Swiper
        style={{ height: '100%', width: '100%' }}
        className=""
        indicatorDots
        vertical={vertical}
        circular
        autoplay
        interval={interval || 10000}
      >
        {sourceData &&
          sourceData.map((item) => {
            return (
              // eslint-disable-next-line react/jsx-key
              <SwiperItem onClick={() => onClick && onClick(item)}>
                <View className="item-image" style={{ width: '100%', height: HEIGHT }}>
                  <Image src={item[customKey] || ''} style={{ height: HEIGHT, width: '100%' }} />
                  {/* // <Img src={item[customKey] || ''}></Img> */}
                </View>
              </SwiperItem>
            );
          })}
      </Swiper>
    </View>
  );
};
export default SwiperCard;
