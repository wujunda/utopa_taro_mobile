import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import Wrap from '../../../components/Wrap';
import Txt from '../../../components/Txt';
import Tag from '../../../components/Tag';

interface IProps {
  item: any;
}
const Index: Taro.FC<IProps> = ({ item }) => {
  return (
    <Wrap>
      <View style={{ width: Taro.pxTransform(24) }}></View>
      <Tag type={5}>{item && item.lable}</Tag>
      <View style={{ width: Taro.pxTransform(10) }}></View>
      <Txt title={item.description} color="deep" size={26} height={52} />
      {!!item &&
        !!item.couponDescs &&
        item.couponDescs != null &&
        item.couponDescs != 'null' &&
        item.couponDescs.length > 0 &&
        item.couponDescs.map((item1) => {
          if (!item1) {
            return;
          }
          return (
            <View key={item1} style={{ marginRight: Taro.pxTransform(12) }}>
              <Tag type={6}>{item1}</Tag>
            </View>
          );
        })}
    </Wrap>
  );
};
Index.defaultProps = {};

export default Index;
