import { View, Input } from '@tarojs/components';
import './search.scss';
import search from '../../../assets/search/search.png';
import Taro from '@tarojs/taro';
import Img from '@/components/Img';
import Wrap from '@/components/Wrap';

interface IProps {
  onChange?: (val: any) => void;
  onConfirm: (v: any) => void;
  placeholder?: String;
  value?: any;
}
const Index: Taro.FC<IProps> = ({
  onChange,
  onConfirm,
  placeholder = '商品名称|订单号|售后单号',
  value
}) => {
  return (
    <View style={{ background: '#fff' }}>
      <View>
        <View className="search-hd-schs">
          <Wrap type={1}>
            <View className="search-hd-sch">
              <Wrap type={1}>
                <View>
                  <Img src={search} width={44} />
                </View>
                <View className="search-hd-ipts">
                  <Input
                    value={value}
                    onInput={(e) => {
                      onChange && onChange(e.detail.value);
                    }}
                    placeholder={placeholder as any}
                    className="search-hd-ipts1"
                    confirmType="search"
                    onConfirm={(e) => {
                      onConfirm(e.detail.value);
                    }}
                  />
                </View>
              </Wrap>
            </View>
          </Wrap>
        </View>
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
