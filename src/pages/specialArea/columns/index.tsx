/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable import/first */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-shadow */
import Taro, {
  useEffect,
  useState,
  // useMemo,
  // useCallback,
  useDidShow,
  useRouter
} from '@tarojs/taro';
import { View, Image } from '@tarojs/components';
import ListView from 'taro-listview';

import { getColumnProduct } from '../../../services/specialArea';
import { handleRequest, Glo } from '@/utils/utils';
import JSBridge from '@/utils/jsbridge/index';
import { INDIC_TEXT, goGoodsDetail } from '../common/config';
import { connect } from '@tarojs/redux';
import ColumnGoodsItem from './components/columnGoodsItem';
import './index.scss';

interface Props {
  type?: any;
  title?: string;
  context?: any;
}
type PageStateProps = {
  user: {
    inApp: boolean;
    isWeiXinFalg: boolean;
  };
};
type PageDispatchProps = {
  addToCart: (goodsInfo) => void;
};

export const Index: Taro.FC<Props> = (props: any) => {
  // 栏目
  const router = useRouter();
  const [state, setState] = useState<any>({
    isLoaded: false,
    error: false,
    hasMore: true,
    isEmpty: false,
    hasLoadIng: false
  });
  const PAGE_SIZE = 20;
  const [goods, setGoods] = useState<any>([]);
  const [postParams, setPostParams] = useState<any>({
    id: router.params.id,
    pageNo: 1,
    pageSize: PAGE_SIZE,
    dataType: 5
  });

  useEffect(() => {}, []);
  useDidShow(() => {
    refList.fetchInit();
  });
  //  栏目列表项目
  const _getColumnProduct = async (postParams, isRefsh = true) => {
    Glo.loading('正在加载..');
    let params = {
      ...postParams,
      pageNo: isRefsh ? 1 : postParams.pageNo
    };
    try {
      let res: any = await getColumnProduct(params);
      if (handleRequest(res)) {
        let data = res.data;
        // 刷新
        if (isRefsh) {
          setGoods(data.records);
          // 加载更多
        } else {
          let newList = goods.concat(data.records);
          console.log(newList, 'newList');
          setGoods(newList);
        }
      }
      let isMore = res.data && res.data.records.length < PAGE_SIZE;
      setPostParams((state) => ({
        ...state,
        pageNo: isMore ? state.pageNo : state.pageNo + 1
      }));
      setState((state) => ({
        ...state,
        isLoaded: true,
        hasMore: isMore ? false : true,
        hasLoadIng: false
      }));
      Glo.hideLoading();
    } catch (e) {
      console.log(e);
      Glo.hideLoading();
    }
    // finally {
    //   Glo.hideLoading();
    // }
  };
  var refList: any = {};
  const pullDownRefresh = async () => {
    setState((state) => ({
      ...state
    }));
    setPostParams((state) => ({
      ...state,
      pageNo: 1
    }));
    await _getColumnProduct(postParams);
  };
  const onScrollToLower = async () => {
    setState((state) => ({
      ...state,
      hasLoadIng: true
    }));
    if (!state.hasLoadIng && state.hasMore) {
      Glo.loading('加载中...');
      await _getColumnProduct(postParams, false);
    }
  };
  let insRef = (node) => {
    refList = node;
  };
  return (
    <View className="skeleton lazy-view" style={{ backgroundColor: '#f5f5f5' }}>
      <ListView
        lazy
        indicator={INDIC_TEXT}
        ref={(node) => insRef(node)}
        isLoaded={state.isLoaded}
        isError={state.error}
        hasMore={state.hasMore}
        style={{ height: '100vh' }}
        isEmpty={state.isEmpty}
        onPullDownRefresh={() => pullDownRefresh()}
        onScrollToLower={(e) => {
          onScrollToLower();
        }}
        lazyStorage="lazyView"
      >
        <View className="cloumn-box">
          {!!goods &&
            goods.map((item, index) => {
              return (
                <View className="item skeleton-bg" key={index}>
                  <ColumnGoodsItem
                    addCard={() => {
                      console.log('添加购物车');
                      if (props.user.inApp && process.env.TARO_ENV === 'h5') {
                        let params = item.productCommand;
                        JSBridge.Common.GTBridge_Common_Base_CMD(params, () => {});
                      } else {
                        props.addToCart({
                          productId: item.productId,
                          number: 1,
                          skuId: item.skuId,
                          storeId: item.storeId,
                          businessId: item.businessId
                        });
                      }
                    }}
                    goGoods={() => {
                      if (props.user.inApp && process.env.TARO_ENV === 'h5') {
                        let params = item.productCommand;
                        JSBridge.Common.GTBridge_Common_Base_CMD(params, (res) => {
                          console.log('执行', params);
                        });
                      } else {
                        goGoodsDetail(item.id, item.storeId);
                        // Taro.navigateTo({
                        //   url: `/pages/details/index?id=${item.id}&businessId=${item.businessId}&storeId=${item.storeId}`
                        // });
                      }
                    }}
                    item={item}
                  />
                </View>
              );
            })}
        </View>
        {!state.hasMore && (
          <View style={{ textAlign: 'center', color: '#999', fontSize: 12, marginTop: 30 }}>
            ---我也是有底线的---
          </View>
        )}
      </ListView>
    </View>
  );
};
Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '栏目',
  disableScroll: true,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5',
  navigationStyle: 'default'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    },
    cart: state.cart
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    addToCart: (payload) => {
      dispatch({
        type: 'cart/addCart',
        payload
      });
    }
  };
};
export default connect(mapStateToProps as any, mapDispatchToProps)(Index as any);
//export default Index;
