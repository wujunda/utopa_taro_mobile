import request from '../utils/request';
import Taro from '@tarojs/taro';

interface ProductDetails {
  productName: string;
  realSkuCode: string;
  productPic: string;
  norm1Name: string;
  norm1Value: string;
  measureName: string;
  isSelect: number;
  isCrt: boolean;
  price: number;
  saleStatus: number;
  saleType: number;
  id: number;
  actuallyAmount: number;
  number: number;
  productId: number;
  storeId: number;
  proId: number;
}
export interface ShopCartDtos {
  category: string;
  storeId: number;
  businessId: number;
  businessType: number;
  actuallyAmount: number;
  isCrt: boolean;
  productDetails: ProductDetails[];
}
export interface CouponDiscountInfo {
  couponName: string;
  discountValue: number;
}
export interface ICartData {
  actuallyAmount: number;
  isAllSelect: number;
  originalAmount: number;
  percentFee: number;
  selectTotalNumber: number;
  category: string;
  shopCartDtos: ShopCartDtos[];
  couponDiscountInfo: CouponDiscountInfo[];
}

export interface ICart {
  code: number;
  data: ICartData;
}
export const getMoveCart = (params: any) => {
  // console.log(params);
  console.warn('登陆数据');
  console.warn(Taro.getStorageSync('login'));
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.favoriteProductsAndDeleteFromShopCart',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};

export const getDelCart = (params: any) => {
  // console.log(params);
  console.warn('登陆数据');
  console.warn(Taro.getStorageSync('login'));
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.deleteShopCartPro',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};

export const getCart = (params: any) => {
  // console.log(params);
  console.warn('登陆数据');
  console.warn(Taro.getStorageSync('login'));
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.getShopCartDetail',
    ver: '1.0',
    params: JSON.stringify({})
  });
};

export const getCart1 = (params: any) => {
  // console.log(params);
  console.warn('登陆数据');
  console.warn(Taro.getStorageSync('login'));
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.getOfflineShopCartDetail',
    ver: '2.0',
    params: JSON.stringify({})
  });
};
export const getChangeGoodsSku = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.changeShopCart',
    ver: '2.0',
    params: JSON.stringify(params)
  });
};

export const getChangeGoodsNumber = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.updateShopCart',
    ver: '2.0',
    params: JSON.stringify({
      id: params.id,
      number: params.number
    })
  });
};
export const getCheckGoods = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.updateShopCart',
    ver: '2.0',
    params: JSON.stringify({
      id: params.id,
      isSelect: params.isSelect
    })
  });
};
export const getCheckStore = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.selectPartOrNot',
    ver: '2.0',
    params: JSON.stringify({
      storeId: params.storeId,
      isSelect: params.isSelect,
      ids: params.ids
    })
  });
};
export const getCheckCart = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.selectAllOrNone',
    ver: '2.0',
    params: JSON.stringify({
      shopCartType: 1,
      isSelectAll: params.isSelectAll
    })
  });
};

//获取店铺优惠券
export const  bussinessCouponList= (params: any) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.coupon.bussinessCouponList',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      clientType:1,
      sysId:1,
    })
  });
};

//优惠券点击领取
export const  userGetCoupon= (params: any) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.coupon.userGetCoupon',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      clientType:1,
      sysId:1,
    })
  });
};

//清空失效商品
export const  deleteShopCartPro = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.deleteShopCartPro',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      clientType:1,
      sysId:1,
    })
  });
};