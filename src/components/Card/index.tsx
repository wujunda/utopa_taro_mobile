import Taro from '@tarojs/taro';
import { View, Image } from '@tarojs/components';

import './index.scss';
import Img from '../Img';
import Txt from '../Txt';
import Cnt from '../Cnt';
import Wrap from '../Wrap';
import Tag from '../Tag';
import { ICouponItem } from '../../services/coupons';
import { images } from '../../images';
import Router from '../../utils/router';

interface IProps {
  type?: 1;
  height?: number;
  Title?: string;
  right?: boolean;
  ischeck?: boolean;
  choose?: (number) => void;
  onQuan?: () => void;
  data?: ICouponItem;
  source?: string;
  canUse?: boolean;
}
const Index: Taro.FC<IProps> = ({
  type,
  height,
  Title,
  right = false,
  ischeck = false,
  choose,
  data,
  source = '',
  onQuan,
  canUse = false
}) => {
  const USE_STATE_TYPE = {
    1: '去使用',
    2: '已使用',
    3: '已过期'
  };
  const OBTAIN_STATE_TYPE = {
    1: '领取',
    2: '去使用',
    3: '已领完 '
  };
  return (
    <View className="utp-card" onClick={choose}>
      {type !== 1 && <Image src={images.cardActive} className="utp-card-bg" />}
      {type === 1 && <Image src={images.card} className="utp-card-bg" />}
      <View className="utp-card-item">
        <Wrap type={2}>
          <View className="utp-card-item-tag">
            {!canUse && (
              <Tag type={type === 1 ? 21 : 2}>{data && data.couponType == 1 ? '满减' : '折扣'}</Tag>
            )}
            {canUse && <Tag type={type === 1 ? 21 : 2}>{data && data.label}</Tag>}
          </View>
          {source === 'details' && (
            <Txt
              title={(data && data.activityName) || '名称'}
              size={30}
              height={54}
              color={type === 1 ? 'low' : 'active'}
            ></Txt>
          )}
          {source !== 'details' && (
            <Txt
              title={(data && data.couponName) || '名称'}
              size={30}
              height={54}
              color={type === 1 ? 'low' : 'active'}
            ></Txt>
          )}
        </Wrap>
        <Wrap>
          {canUse && (
            <Txt
              title={data && data.activeTimeDesc}
              size={22}
              height={53}
              color={type === 1 ? 'morelow' : 'active'}
            ></Txt>
          )}
          {!canUse && (
            <Txt
              title={(data && data.validTimeScope) || '有效期'}
              size={22}
              height={53}
              color={type === 1 ? 'morelow' : 'active'}
            ></Txt>
          )}
        </Wrap>
        <View className={type === 1 ? 'utp-card-rules utp-card-rules-unactive' : 'utp-card-rules '}>
          <Wrap>
            {data && data.conditionValue === 0 && (
              <Txt
                title="无门槛"
                size={20}
                height={64}
                color={type === 1 ? 'morelow' : 'active'}
              ></Txt>
            )}

            {source === 'details' && (
              <View>
                {data && data.conditionValue !== 0 && (
                  <Txt
                    title={`${data.activityDesc}`}
                    size={20}
                    height={64}
                    color={type === 1 ? 'morelow' : 'active'}
                  ></Txt>
                )}
              </View>
            )}
            {source !== 'details' && (
              <View>
                {data && data.conditionValue !== 0 && (
                  <Txt
                    title={`满${data && data.conditionValue / 100}可用`}
                    size={20}
                    height={64}
                    color={type === 1 ? 'morelow' : 'active'}
                  ></Txt>
                )}
              </View>
            )}
          </Wrap>
        </View>
      </View>
      {!canUse && (
        <View
          className={type === 1 ? 'utp-card-btn utp-card-btn-unactive' : 'utp-card-btn '}
          onClick={() => {
            if (data && (data.useStatus === 1 || data.obtainStatus === 2)) {
              Router.goHome(data.command);
            } else if (data && data.useStatus === null) {
              if (onQuan) {
                onQuan();
              }
            }
          }}
        >
          <Cnt>
            {
              right ? (
                <Img src={ischeck ? images.check2 : images.uncheck2} width={36} />
              ) : !!data && !!data.useStatus ? (
                <Txt
                  size={28}
                  title={USE_STATE_TYPE[data.useStatus]}
                  color={[2, 3].includes(data.useStatus) ? 'low' : 'active'}
                />
              ) : (
                <Txt
                  size={28}
                  title={data && OBTAIN_STATE_TYPE[data.obtainStatus]}
                  color={data &&  [3].includes(data.obtainStatus) ? 'low' : 'active'}
                />
              )

              // (
              //   <Txt
              //     title={
              //       (data &&
              //         data.useStatus != null &&
              //         (data.useStatus === 1
              //           ? '去使用'
              //           : data.useStatus === 2
              //           ? '已使用'
              //           : data.useStatus === 3
              //           ? '已过期'
              //           : '已失效')) ||
              //       '领取'
              //     }
              //     size={28}
              //     color={type === 1 ? 'low' : 'active'}
              //   />
              // )
            }
          </Cnt>
        </View>
      )}
    </View>
  );
};
Index.defaultProps = {};

export default Index;
