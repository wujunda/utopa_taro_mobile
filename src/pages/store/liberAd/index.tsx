import Taro, { useRouter } from '@tarojs/taro';
import { View } from '@tarojs/components';

import Wrap from '../../../components/Wrap';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import { connect } from '@tarojs/redux';
import useRequest from '../../../hooks/useRequest';
import Router from '../../../utils/router';
import { IAd, IAdData, getLiberAd } from '../../../services/liber';
import './index.scss';

type PageStateProps = {};

type PageDispatchProps = {
  // addCart: (any) => void;
};
type IProps = PageStateProps & PageDispatchProps;
const Index: Taro.FC<IProps> = () => {
  const id = useRouter().params.id;

  const [state] = useRequest<IAdData>(id, getLiberAd);

  // 图片
  let list: IAd[] = [];
  if (state && state.code === 0) {
    //list = state.data.banners;
    list = state.data.list;
  }

  return (
    <View className="utp-liberAd">
      {list.map((item) => {
        return (
          <View key={item.groupName}>
            <View className="utp-liberAd-titles">
              <Wrap>
                <Txt title={item.groupName} color="deep" height={72} size={30} />
              </Wrap>
            </View>
            {item.groupDetails.map((item1) => {
              return (
                <View
                  className="utp-liberAd-imgs"
                  key={item1.name}
                  onClick={() => {
                    Router.goHome(item1.command);
                  }}
                >
                  <Img src={item1.image} width={690} height={(690 * item1.height) / item1.width} />
                </View>
              );
            })}
          </View>
        );
      })}
    </View>
  );
};
Index.config = {
  navigationBarTitleText: '品牌体验',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff'
  // 不显示标题
  //navigationStyle: 'custom'
};

const mapStateToProps: PageStateProps = (state) => {
  return {
    number: state.cart.number,
    post: state.cart.post,
    cart: state.cart,
    loading: state.loading.effects['cart/addCart']
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};
// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
