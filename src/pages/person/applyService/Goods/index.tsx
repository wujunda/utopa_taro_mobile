import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import Wrap from '../../../../components/Wrap';
import Img from '../../../../components/Img';

interface IProps {
  item: any;
}
const Index: Taro.FC<IProps> = ({ item }) => {
  return (
    <View>
      {!!item && !!item.products && item.products.length > 1 && (
        <Wrap type={1}>
          {item.products.map((i, ii) => {
            return (
              <View
                style={{ display: 'inline-block' }}
                className="utp-applyService-goodsInfo-imgs"
                key={ii}
              >
                <Img src={i.picIds} width={170} />
              </View>
            );
          })}
        </Wrap>
      )}
    </View>
  );
};
Index.defaultProps = {};

export default Index;
