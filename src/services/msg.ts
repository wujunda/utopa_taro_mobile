import request from '../utils/request';

export interface IMenu {
  id: number;
  name: string;
  imgUri: string;
  unreadCount: number;
}
export interface IMsgMenu {
  code: number;
  data: {
    records: IMenu[];
  };
}
export interface IMsgInfo {
  mId: string;
  title: string;
  content: string;
  createTime: number;
}
export interface IMsg {
  code: number;
  data: {
    records: IMsgInfo[];
  };
}

// 消息
export const getMsgMenu = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'base.message.listTopBarMessage',
    ver: '1.0',
    params: JSON.stringify({
      sysId: 1
    })
  });
};
export const getMsg = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'base.message.listGuideBarMessageDetail',
    ver: '1.0',
    params: JSON.stringify({
      sysId: 1,
      ...params
    })
  });
};
