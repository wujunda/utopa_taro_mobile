import Taro, { useState } from '@tarojs/taro';
import { connect } from '@tarojs/redux';

import { View, Text, ScrollView, Switch } from '@tarojs/components';
import Wrap from '../../components/Wrap';
import ItemHomeIcon from '../../components/ItemHomeIcon';

import './index.scss';
import home_image06 from '../../assets/home/home_image06.png';
import Fc from '../../components/Fc';
import Txt from '../../components/Txt';
import Img from '../../components/Img';
import Cnt from '../../components/Cnt';
import Box from '../../components/Box';
import Box1 from '../../components/Box1';
import Sch from '../../components/sch';
import MsgCenter from '../../components/MsgCenter';
//import Address from '../../components/Address';
//import Goods4 from '../../components/Goods4';
//import ShoppingGoods from '../../components/shopGoods';
import Bton from '../../components/Bton';
import Tag from '../../components/Tag';
import Sec from '../../components/Sec';
import Score from '../../components/Score';
import Brand from '../../components/Brand';
//import AddCart from '../../components/AddCart';
//import CartSub from '../../components/CartSub';
import Swipe from '../../components/Swipe';
import Icons from '../../components/Icons';
import Goods2s from '../../components/Goods2s';
import ItemMineIcon from '../../components/ItemMineIcon';
import OperateCart from '../../components/OperateCart';
import personal_icon_payment from '../../assets/mine/personal_icon_payment.png';
//import useSession from '../../hooks/useSession';

import icon_duihuandingdan from '../../assets/integral/icon_duihuandingdan.png';
import icon_jifenmingxi from '../../assets/integral/icon_jifenmingxi.png';
import icon_jifenshangcheng from '../../assets/integral/icon_jifenshangcheng.png';
import { ISwipe, IIcons, IGoods } from '../../interface/home';
import { images } from '../../images/index';
import { IHome, getHome, AdvertisementList } from '../../services/home';
import useRequest from '../../hooks/useRequest';

type PageStateProps = {
  user: {
    name: string;
  };
  example: any;
  loading: boolean;
};

type PageDispatchProps = {
  wait: () => void;
  clickEffict: () => void;
  toGetGoods: () => void;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

//let cishu: number = 0;
const Index: Taro.FC<IProps> = (props) => {
  // 首页数据
  const [home, update, loading] = useRequest<IHome>({}, getHome);
  let lunbo: AdvertisementList[] = [];
  if (home && home.code === 0) {
    home.data.advertisementSpaceList.map((item) => {
      if (item.code === 'utopa_homepage_top_banner') {
        lunbo = item.advertisementList;
      }
    });
  }
  lunbo.map((item) => {
    item.source = item.image;
  });

  // 获取session
  //const [haveSession] = useSession();

  // 店铺推荐商品
  const [integralInfo] = useState([
    {
      url: '/pages/store/shoppingStore/index',
      title: '电商外卖店铺',
      src: icon_jifenmingxi
    },
    {
      url: '/pages/store/clothingBusiness/index',
      title: '服饰店铺首页',
      src: icon_duihuandingdan
    },
    {
      url: '/pages/store/clothingSort/index',
      title: '服饰店铺分类',
      src: icon_jifenshangcheng
    },
    {
      url: '/pages/message/management/index',
      title: '编辑常用',
      src: icon_jifenmingxi
    },
    {
      url: '/pages/FreeChildren/freePlayHome/index',
      title: '儿童免费玩首页',
      src: icon_duihuandingdan
    },
    {
      url: '/pages/FreeChildren/freePlayList/index',
      title: '儿童免费玩列表',
      src: icon_jifenshangcheng
    },
    {
      url: '/pages/FreeChildren/freeVoucher/index',
      title: '儿童免费玩优惠',
      src: icon_jifenmingxi
    },
    {
      url: '/pages/integral/list/index',
      title: '积分列表',
      src: icon_jifenmingxi
    },
    {
      url: '/pages/details/index?id=7052948&storeId=774321&businessId=3430&type=1&jifen=1',
      title: '积分商品',
      src: icon_jifenmingxi
    },
    {
      url: '/pages/cart1/index',
      title: 'cart1',
      src: icon_jifenmingxi
    }
  ]);

  // console.log('首页');
  // console.log(props);
  //const [state, update, loading] = useRequest<IUser>('', (p) => {
  //console.log('我先看看首页的p', p);
  //return new Promise((r) => {
  //cishu++;
  //console.log('pppppppppppppppppppppppp');
  //console.log(cishu);
  //setTimeout(() => {
  //r({
  //name: '我是来自北方的狼' + cishu
  //});
  //}, 2000);
  //});
  //});
  //
  if (props.loading) {
    Taro.showLoading({
      title: 'loading'
    });
  } else {
    Taro.hideLoading();
  }

  const [fixed] = useState(false);
  const [opacity, setOpacity] = useState(0);
  const [scrollTop, setScrollTop] = useState(0);

  function onScroll(e) {
    // console.log('首页滑动');
    var top = e.detail.scrollTop;
    // console.log(top);
    //var el = document.getElementsByClassName("fixedBox")[0];
    //var size = el.offsetTop;
    //console.log(size);
    //if (top >= size && fixed == false) {
    //setFixed(true);
    //} else if (top < size && fixed == true) {
    //setFixed(false);
    //}

    let size = e.detail.scrollTop;
    // console.log(size);
    let opa = size / 76;
    setOpacity(opa > 1 ? 1 : opa);
    setScrollTop(top > 76 ? 76 : top);
  }
  //console.log('我先看看首页的state', state);
  var homeData = [
    {
      type: 'swipe',
      data: [
        {
          source: 'https://img.alicdn.com/tfs/TB1hNYOEVT7gK0jSZFpXXaTkpXa-520-280.jpg_q90_.webp',
          url: ''
        },
        {
          source: 'https://img.alicdn.com/simba/img/TB1HLV4ExD1gK0jSZFySuwiOVXa.jpg',
          url: ''
        }
      ]
    },
    {
      type: 'icons',
      data: [
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        },
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        },
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        },
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        },
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        },
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        },
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        },
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        },
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        },
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        },
        {
          source: images.home_icon,
          url: '',
          title: '扫码购物'
        }
      ]
    },
    {
      type: 'goods2',
      data: [
        {
          id: 21321,
          productImage:
            'https://gd2.alicdn.com/imgextra/i3/0/O1CN011ApyDb26pgeNOctKk_!!0-item_pic.jpg_400x400.jpg',
          // 商品标题
          productName: '蓝色系带半高领针织连衣裙',
          // 商品价格
          salePrice: 8888,
          // 促销价
          originPrice: 6666,
          // 是否自营
          isSelf: 1
        },
        {
          id: 21321,
          productImage:
            'https://gd2.alicdn.com/imgextra/i2/0/O1CN01rCT5rc26pgeSEjcXR_!!0-item_pic.jpg',
          // 商品标题
          productName: '白色一字肩伴娘礼服连衣裙',
          // 商品价格
          price: 5500,
          // 促销价
          originPrice: 4400,
          // 是否自营
          selfSupport: 1
        },

        {
          id: 21321,
          productImage:
            'https://gd2.alicdn.com/imgextra/i3/0/O1CN011ApyDb26pgeNOctKk_!!0-item_pic.jpg_400x400.jpg',
          // 商品标题
          productName: '蓝色系带半高领针织连衣裙',
          // 商品价格
          salePrice: 8888,
          // 促销价
          originPrice: 6666,
          // 是否自营
          selfSupport: 1
        },
        {
          id: 21321,
          productImage:
            'https://gd2.alicdn.com/imgextra/i2/0/O1CN01rCT5rc26pgeSEjcXR_!!0-item_pic.jpg',
          // 商品标题
          productName: '白色一字肩伴娘礼服连衣裙',
          // 商品价格
          salePrice: 5500,
          // 促销价
          originPrice: 4400,
          // 是否自营
          selfSupport: 1
        }
      ]
    }
  ];

  return (
    <View className="utp-html">
      <View className="utp-body">
        <ScrollView
          className="utp-bd"
          scrollY
          refresherEnabled
          refresherBackground="#333"
          onScroll={onScroll}
          // style={{ position: 'relative' }}
        >
          <View
            className="utp-set utp-set-1 utp-flex utp-flex-db"
            style={{
              backgroundColor: opacity === 0 ? 'transparent' : `rgba(250,250,250,${opacity})`,
              height:
                (1 - opacity) * 300 <= 160
                  ? Taro.pxTransform(160)
                  : Taro.pxTransform((1 - opacity) * 300)
            }}
          >
            <View className="utp-set utp-set-2 utp-flex utp-flex-db">
              <Sch
                type={2}
                opacity={opacity}
                scrollTop={scrollTop}
                onScan={() => {
                  // console.log('扫码!!!!!!!!!!!!!!!!!!!!!!!!!!!');
                }}
              />
            </View>
          </View>
          {!!home &&
            home.code === 0 &&
            home.data.advertisementSpaceList.map((item) => {
              if (item.code == 'utopa_homepage_top_banner') {
                item.advertisementList.map((item1) => {
                  item1.source = item1.image;
                });
                // 轮播
                return <Swipe data={item.advertisementList as ISwipe[]} />;
              }
              //else if (item.type == 'icons') {
              //// 小图标
              //return <Icons data={item.data as IIcons[]} />;
              //} else if (item.type == 'goods2') {
              //// 商品1*2
              //return <Goods2s data={item.data as IGoods[]} />;
              //}
            })}
          {!!home &&
            home.code === 0 &&
            home.data.memberConfList.map((item) => {
              if (item.groupCode === 'myApp') {
                item.items.map((item1) => {
                  item1.source = item1.iconUrl;
                });
                // 小图标
                return <Icons data={item.items as IIcons[]} />;
              }
            })}

          <Switch
            checked
            color="#FF2F7B"
            onChange={(e) => {
              // console.log('我是开关———————', e.detail.value);
            }}
          />

          <View
            onClick={() => {
              props.toGetGoods();
              // console.log('我是获取的数据', props.example.goodsList);
            }}
          >
            点我获取goods
          </View>

          <View
            style={{
              borderWidth: Taro.pxTransform(1),
              borderColor: 'red'
            }}
            className="utp-solid utp-cnt"
          >
            <View
              onClick={() => {
                props.clickEffict();
              }}
            >
              <Txt size={28} color="active" title="redux 点击改变数据触发异步"></Txt>
              <Text>{props.example.name}</Text>
            </View>
          </View>

          <View
            style={{
              marginTop: Taro.pxTransform(130),
              paddingBottom: Taro.pxTransform(80),
              borderWidth: Taro.pxTransform(1),
              borderColor: '#ccc'
            }}
            className="utp-solid"
          >
            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                paddingBottom: Taro.pxTransform(10),
                borderColor: 'red',
                backgroundColor: 'yellow'
              }}
              className="utp-solid"
            >
              <View onClick={() => {}}>
                <Txt size={28} title="Brand 品牌"></Txt>
                <View style={{ paddingBottom: Taro.pxTransform(10) }}>
                  <Brand />
                </View>
              </View>
            </View>

            <View>
              <OperateCart></OperateCart>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                paddingBottom: Taro.pxTransform(10),
                borderColor: 'red',
                backgroundColor: 'yellow'
              }}
              className="utp-solid"
            >
              <View onClick={() => {}}>
                <Txt size={28} title="无位置页面按钮"></Txt>
                <Wrap type={3}>
                  {integralInfo.map((item, index) => {
                    return (
                      <View
                        style={{ width: '25%' }}
                        key={index}
                        onClick={() => {
                          Taro.navigateTo({ url: item.url });
                        }}
                      >
                        <ItemHomeIcon tips={item.title} src={item.src} tipsSize={24} />
                      </View>
                    );
                  })}
                </Wrap>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                paddingBottom: Taro.pxTransform(10),
                borderColor: 'red',
                backgroundColor: 'yellow'
              }}
              className="utp-solid"
            >
              <View onClick={() => {}}>
                <Txt size={28} title="Address 地址"></Txt>
                <View style={{ paddingBottom: Taro.pxTransform(10) }}></View>
              </View>
            </View>
            <View className="fixedBox">
              <View
                style={{
                  height: '30px',
                  backgroundColor: 'pink',
                  top: 0,
                  right: 0,
                  left: 0,
                  zIndex: 10,
                  bottom: 0,
                  position: fixed ? 'fixed' : 'static'
                }}
              ></View>
              <View
                style={{
                  height: '30px',
                  backgroundColor: 'red',
                  top: 0,
                  right: 0,
                  left: fixed ? 'static' : '9999px',
                  zIndex: 1,
                  bottom: 0,
                  position: fixed ? 'static' : 'fixed'
                }}
              ></View>
            </View>
            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                paddingBottom: Taro.pxTransform(10),
                borderColor: 'red',
                backgroundColor: 'yellow'
              }}
              className="utp-solid"
            >
              <View onClick={() => {}}>
                <Txt size={28} title="MsgCenter 消息中心"></Txt>
                <View style={{ paddingBottom: Taro.pxTransform(10) }}>
                  <Box1>
                    <MsgCenter />
                    <MsgCenter />
                    <MsgCenter />
                  </Box1>
                </View>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                paddingBottom: Taro.pxTransform(10),
                borderColor: 'red',
                backgroundColor: 'yellow'
              }}
              className="utp-solid"
            >
              <View onClick={() => {}}>
                <Txt size={28} title="AddCart 添加到购物车"></Txt>
                <View style={{ paddingBottom: Taro.pxTransform(10) }}></View>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                paddingBottom: Taro.pxTransform(10),
                borderColor: 'red'
              }}
              className="utp-solid utp-cnt"
            >
              <View onClick={() => {}}>
                <Txt size={28} title="Score 评分"></Txt>
                <Score num={3} />
                <Score />
                <Score num={5} />
                <Score num={0} />
              </View>
            </View>
            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                paddingBottom: Taro.pxTransform(10),
                borderColor: 'red'
              }}
              className="utp-solid"
            >
              <View onClick={() => {}}>
                <Txt size={28} title="Sec 秒杀"></Txt>
                <Sec />
                <Sec type={0} />
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                paddingBottom: Taro.pxTransform(10),
                borderColor: 'red',
                backgroundColor: 'red'
              }}
              className="utp-solid"
            >
              <View>
                <Txt size={28} title="CartEdit 购物车编辑"></Txt>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                paddingBottom: Taro.pxTransform(10),
                borderColor: 'red',
                backgroundColor: 'red'
              }}
              className="utp-solid"
            >
              <View>
                <Txt size={28} title="CartSub 购物车提交"></Txt>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid utp-cnt"
            >
              <View style={{ width: '100%' }}>
                <Txt size={28} color="active" title="Card 领券"></Txt>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid"
            >
              <View>
                <Txt size={28} color="active" title="商品1*3"></Txt>
                <View
                  style={{
                    backgroundColor: 'red',
                    paddingLeft: Taro.pxTransform(20),
                    paddingTop: Taro.pxTransform(20),
                    paddingRight: Taro.pxTransform(20),
                    paddingBottom: Taro.pxTransform(20)
                  }}
                >
                  <Wrap>
                    {/* <Goods3 />
                    <Goods3 />
                    <Goods3 />
                    <Goods3 />
                    <Goods3 />
                    <Goods3 /> */}
                  </Wrap>
                </View>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid"
            >
              <View>
                <Txt size={28} color="active" title="商品1*2"></Txt>
                <View
                  style={{
                    backgroundColor: 'red',
                    paddingLeft: Taro.pxTransform(20),
                    paddingTop: Taro.pxTransform(20),
                    paddingRight: Taro.pxTransform(20),
                    paddingBottom: Taro.pxTransform(20)
                  }}
                ></View>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid"
            >
              <View>
                <Txt size={28} color="active" title="商品1*1列表"></Txt>
                <View
                  style={{
                    backgroundColor: 'red',
                    paddingLeft: Taro.pxTransform(20),
                    paddingTop: Taro.pxTransform(20),
                    paddingRight: Taro.pxTransform(20),
                    paddingBottom: Taro.pxTransform(20)
                  }}
                ></View>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                paddingBottom: Taro.pxTransform(8),
                borderColor: 'red',
                backgroundColor: '#fff'
              }}
              className="utp-solid utp-cnt"
            >
              <View
                onClick={() => {
                  props.wait();
                }}
              >
                <Txt size={28} color="active" title="Tag 标签"></Txt>
                <View className="utp-cnt">
                  <Tag>标签</Tag>
                </View>
                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={1}>type=1标签</Tag>
                </View>
                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={2}>type=2标签</Tag>
                </View>
                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={21}>type=21标签</Tag>
                </View>
                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={3}>type=3标签</Tag>
                </View>

                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={31}>type=31标签</Tag>
                </View>
                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={5}>type=5标签</Tag>
                </View>
                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={6}>type=6标签</Tag>
                </View>
                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={10}>type=10标签</Tag>
                </View>

                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={11}>type=11标签</Tag>
                </View>

                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={12}>type=12标签</Tag>
                </View>
                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={13}>type=13标签</Tag>
                </View>

                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={14}>type=14标签</Tag>
                </View>
                <View
                  style={{
                    paddingTop: Taro.pxTransform(5),
                    paddingBottom: Taro.pxTransform(5)
                  }}
                >
                  <Tag type={15}>type=15标签</Tag>
                </View>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid"
            >
              <Txt
                size={28}
                color="active"
                title={`
                ItemHomeIcon 首页图标和文字
                `}
              ></Txt>
              <ItemHomeIcon tips="扫码购物" src={home_image06} />
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid"
            >
              <Txt size={28} color="active" title="ItemMineIcon 我的图标和文字"></Txt>
              <ItemMineIcon tips="提示" src={personal_icon_payment} num={8} />
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid"
            >
              <Txt size={28} color="active" title="Goods 单个商品"></Txt>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid"
            >
              <Txt size={28} color="active" title="Sch 搜索组件"></Txt>
              <Sch />
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid"
            >
              <Txt size={28} color="active" title="Bton 按钮"></Txt>
              <Bton type={3} size="mini">
                size=mini小按钮
              </Bton>
              <Bton type={7}>渐变按钮</Bton>
              <Bton type="none">禁用按钮</Bton>
              <Bton type={3}>type3按钮</Bton>
              <Bton type={2}>type2按钮</Bton>
              <Bton type={1}>type1按钮</Bton>
              <Bton>默认按钮</Bton>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid utp-cnt"
            >
              <View
                style={{
                  width: '100%'
                }}
              >
                <Txt size={28} color="active" title="Box卡片盒子"></Txt>
                <View
                  style={{
                    width: '100%',
                    paddingTop: Taro.pxTransform(20),
                    backgroundColor: 'red'
                  }}
                  className="utp-div"
                >
                  <Box>
                    <Txt size={28} title="卡片"></Txt>
                    <Txt size={28} title="卡片"></Txt>
                    <Txt size={28} title="卡片"></Txt>
                    <Txt size={28} title="卡片"></Txt>
                    <Txt size={28} title="卡片"></Txt>
                    <Txt size={28} title="卡片"></Txt>
                  </Box>
                </View>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid utp-cnt"
            >
              <View>
                <Txt size={28} color="active" title="Wrap左中右排版组件"></Txt>
                <View
                  style={{
                    width: '100%'
                  }}
                >
                  <Wrap>
                    <Txt size={28} title="左"></Txt>
                    <Txt size={28} title="中"></Txt>
                    <Txt size={28} title="右"></Txt>
                  </Wrap>
                </View>
                <Txt size={28} color="active" title="type=1定高"></Txt>
                <View
                  style={{
                    width: Taro.pxTransform(750),
                    height: Taro.pxTransform(100)
                  }}
                >
                  <Wrap type={1}>
                    <Txt size={28} title="左"></Txt>
                    <Txt size={28} title="中"></Txt>
                    <Txt size={28} title="右"></Txt>
                  </Wrap>
                </View>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid utp-cnt"
            >
              <View>
                <Txt size={28} color="active" title="Cnt居中组件"></Txt>
                <View
                  className="utp-solid"
                  style={{
                    width: Taro.pxTransform(375),
                    height: Taro.pxTransform(375),
                    borderColor: 'pink',
                    borderWidth: Taro.pxTransform(4)
                  }}
                >
                  <Cnt>
                    <Txt size={28} title="我居中了"></Txt>
                  </Cnt>
                </View>
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid utp-cnt"
            >
              <View>
                <Txt size={28} color="active" title="Img图片"></Txt>
                <Txt title="默认宽度40"></Txt>
                <Img src={home_image06} />
                <Txt title="宽高一样只需传宽"></Txt>
                <Img src={home_image06} width={100} />
                <Txt title="宽高都传"></Txt>
                <Img src={home_image06} width={300} height={200} />
              </View>
            </View>

            <View
              style={{
                borderWidth: Taro.pxTransform(1),
                borderColor: 'red'
              }}
              className="utp-solid"
            >
              <Txt size={28} color="active" title="Txt文字"></Txt>
              <Txt title="我是文字默认颜色666默认大小24"></Txt>
              <Txt color="low" title="我是文字我是文字我是文字颜色low"></Txt>
              <Txt color="normal" title="我是文字我是文字我是文字颜色normal"></Txt>
              <Txt color="deep" title="我是文字我是文字我是文字颜色deep"></Txt>
              <Txt color="active" title="我是文字我是文字我是文字颜色active"></Txt>
              <Txt size={23} title="我是文字我是文字大小随意"></Txt>
              <Txt size={28} title="我是文字我是文字大小随意"></Txt>
              <Txt size={34} title="我是文字我是文字大小随意"></Txt>
              <Txt size={34} height={50} title="我是文字我是文字行高随意"></Txt>
              <Txt size={34} height={80} title="我是文字我是文字行高随意"></Txt>
              <Txt size={34} bold title="我是文字我是文字加粗加粗"></Txt>
              <Txt
                title="2020-11-24  16:00"
                dian
                size={24}
                color="white"
                backgroundColor="#D0D0D0"
                borderRadius={12}
                paddingLeftRight={16}
                height={40}
                time
              />
            </View>
          </View>

          <View>
            <Text>hhhhh</Text>
          </View>
          <Fc />
          <View>
            <Text>hhhhh</Text>
          </View>
          <View>
            <Text>hhhhh</Text>
          </View>
          <View>
            <Text>hhhhh</Text>
          </View>
          <View>
            <Text>hhhhh</Text>
          </View>
          <View>
            <Text>hhhhh</Text>
          </View>
          <View>
            <Text>hhhhh</Text>
          </View>
          <View>
            <Text>hhhhh</Text>
          </View>
          <View>
            <Text>hhhhh</Text>
          </View>
        </ScrollView>
        <View>
          <Text>hhhhh</Text>
        </View>
      </View>
    </View>
  );
};
Index.config = {
  navigationBarTitleText: '首页',
  disableScroll: true,
  navigationStyle: 'custom'
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      name: state.user.name
    },
    example: state.example,
    loading: state.loading.effects['cart/addCart'] || state.loading.effects['sync/addCart']
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    wait: () => {
      // console.log('首页触发dis');
      dispatch({
        type: 'example/save',
        payload: {
          name: 'sdfqwiejfwie'
        }
      });
    },
    clickEffict: () => {
      // console.log('首页触发异步的dis');
      dispatch({
        type: 'example/wait'
      });
    },
    toGetGoods: () => {
      // console.log('首页触发请求GetGoods');
      dispatch({
        type: 'example/getGoods'
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
