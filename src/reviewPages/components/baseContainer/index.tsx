import Taro from '@tarojs/taro';
import classnNmes from 'classnames';
// eslint-disable-next-line no-unused-vars
import { ComStyle } from '../../interface';
import './index.scss';

interface Props {
  style: Partial<ComStyle>;
  isEditor?: boolean;
}

const BaseContainer: Taro.FC<Props> = props => {
  const { children, style, isEditor } = props;
  const cls = classnNmes('base-container', {
    'base-container-active': isEditor
  });
  return (
    <view className={cls} style={style}>
      {children}
    </view>
  );
};

export default BaseContainer;
