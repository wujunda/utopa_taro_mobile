/* eslint-disable import/first */
/* eslint-disable no-shadow */
import Taro, { useRouter } from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
// eslint-disable-next-line import/first
import '../index.scss';

/*
 * 分类信息入口
 */
const ViewCollageItem = ({ classes, onClick }) => {
  return (
    classes && (
      <View className="collage-items-box">
        {classes.map((o: any) => {
          return (
            <View
              onClick={() => {
                onClick && onClick(o);
              }}
              className="items"
              style={{
                backgroundImage: 'url(' + o.image + ')',
                paddingTop: Taro.pxTransform(15),
                paddingLeft: Taro.pxTransform(20)
              }}
              key={o.id}
            >
              <View
                style={{ fontSize: Taro.pxTransform(30), textAlign: 'left', color: '#fff' }}
              ></View>
            </View>
          );
        })}
      </View>
    )
  );
};
export default ViewCollageItem;
