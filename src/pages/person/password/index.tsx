import Taro, { useState } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View, Input } from '@tarojs/components';
import useStore from '../../../components/useNum';
import { connect } from '@tarojs/redux';
// import useRequest from '../../../hooks/useRequest';
// import { IUse, getItem } from '../../../services/user';

import Img from '../../../components/Img';
import Bton from '../../../components/Bton';
import { images } from '../../../images';
import Txt from '../../../components/Txt';
import './index.scss';
import { Glo } from '../../../utils/utils';
import Time from './time';
import { getCheckCode, getCode } from '../../../services/user';
import  Router  from '../../../utils/router';

function Index(props) {
  const [time, setTime] = useState(0);
  let num = useStore({ name: '' })[0];
  console.log('jjjjj');
  console.log(num);
  console.log('props');
  console.log(props);
  // const [state, update, loading] = useRequest<IUse>('name=a&age=1', getItem);
  // console.log('更多');
  // console.log(state);
  // console.log(loading);
  // console.log(update);
  const [focus, setFocus] = useState(false);
  const [focus1, setFocus1] = useState(false);
  const [phone, setPhone] = useState('');
  const [code, setCode] = useState('');
  console.log(focus);
  const getCode1 = async () => {
    if (Glo.isPhone(phone, 86) && time === 0) {
      let data = await getCode({
        mobile: phone,
        sysId: 1
      });
      if (data.code === 0) {
        setTime(60);
      }
    }
  };
  let next = false;
  if (Glo.isPhone(phone, 86) && code.length === 6) {
    next = true;
  }
  const onNext = async () => {
    let data = await getCheckCode({
      checkCode: code,
      mobile: phone
    });
    if (data.code === 0) {
      Router.goSetPassword(code, phone);
    } else {
      Taro.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
  };

  return (
    <View className="utp-password">
      <View className="utp-password-box">
        <View
          className="utp-password-ipts"
          onClick={() => {
            setTimeout(() => {
              setFocus(true);
            }, 30);
          }}
        >
          <View className="utp-password-ipts-ico utp-cnt">
            {!(focus || Glo.isPhone(phone, 86)) && <Img src={images.set_phone} width={36} />}
            {(focus || Glo.isPhone(phone, 86)) && <Img src={images.set_phone_crt} width={36} />}
          </View>
          <View className="utp-password-ipts-ipt">
            <Input
              onInput={(e) => {
                let value = e.detail.value;
                setPhone(value);
              }}
              value={phone}
              focus={focus}
              onFocus={() => {
                setFocus(true);
              }}
              onBlur={() => {
                setFocus(false);
              }}
              type="text"
              placeholder="请输入您的手机号"
            />
          </View>
          {focus && phone && (
            <View
              className="utp-password-ipts-clo utp-cnt"
              onClick={() => {
                setPhone('');
              }}
            >
              <Img src={images.set_clo} width={28} />
            </View>
          )}
          <View
            className={
              'utp-password-ipts-btn utp-cnt ' +
              (Glo.isPhone(phone, 86) && time === 0 ? 'utp-password-ipts-btn-1' : '')
            }
            onClick={(e) => {
              e.stopPropagation;
              getCode1();
            }}
          >
            <Txt
              title={time === 0 ? '获取验证码' : `已发送 (${time})`}
              color={Glo.isPhone(phone, 86) && time === 0 ? 'red' : 'normal'}
            />
            {time !== 0 && (
              <Time
                time={time}
                callBack={(sec) => {
                  setTime(sec);
                  console.log(sec);
                }}
              />
            )}
          </View>
        </View>

        <View
          className="utp-password-ipts"
          onClick={() => {
            setTimeout(() => {
              setFocus1(true);
            }, 30);
          }}
        >
          <View className="utp-password-ipts-ico utp-cnt">
            {!(focus1 || code) && <Img src={images.set_save} width={36} />}
            {(focus1 || code) && <Img src={images.set_save_crt} width={36} />}
          </View>
          <View className="utp-password-ipts-ipt">
            <Input
              onInput={(e) => {
                let value = e.detail.value;
                setCode(value);
              }}
              value={code}
              focus={focus1}
              onFocus={() => {
                setFocus1(true);
              }}
              onBlur={() => {
                setFocus1(false);
              }}
              type="text"
              placeholder="请输入验证码"
            />
          </View>
          {focus1 && code && (
            <View
              className="utp-password-ipts-clo utp-cnt"
              onClick={() => {
                setCode('');
              }}
            >
              <Img src={images.set_clo} width={28} />
            </View>
          )}
        </View>
      </View>
      <View className="utp-password-box">
        {!next && (
          <Bton type={'none'} size="mini">
            下一步
          </Bton>
        )}
        {next && (
          <View onClick={onNext}>
            <Bton size="mini">下一步</Bton>
          </View>
        )}
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '重置登陆密码',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff'
};
// @ts-ignore
export default connect((state) => state)(Index);
