/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
/* eslint-disable import/first */
import Taro, { useState, useEffect, useRouter, useDidShow, useDidHide } from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import Safe from '@/components/safe';
import RefundGoodsItem from '@/components/RefundGoodsItem';
import './index.scss';
import moneyIcon from '../../../assets/refund_money.png';
import goodsIcon from '../../../assets/refund_goods.png';
import goRight from '../../../assets/mine/personal_icon_forward.png';
import { getByOrderDetailId } from '../../../services/afterSale';
import { handleRequest, Glo, inAppIsLogin, getAppToken } from '@/utils/utils';
import { _fetchToken } from '@/services/login';
import { connect } from '@tarojs/redux';

import JSBridge from '@/utils/jsbridge/index';

import SelRefundType from './SelRefundType/index';

interface GoodsItem {
  num?: number;
  picIds: String;
  name: String;
  norm1Name: String;
  price: number;
  image?: String;
}

type PageStateProps = {
  user: {
    inApp: boolean;
    isWeiXinFalg: boolean;
  };
};

export const Index: Taro.FC<GoodsItem> = (props: any) => {
  const router = useRouter();
  const REFUND_TYPE = [
    {
      icon: moneyIcon,
      title: '仅退款',
      text: '未收到货，或商家同意仅退款不退货',
      path: '/pages/person/applyRefund/index?type=1&orderLineId=' + router.params.orderLineId,
      type: 1,
      isHide: false
    },
    {
      icon: goodsIcon,
      title: '退货退款',
      text: '已收到货，需退回货物',
      path: '/pages/person/applyRefund/index?type=2&orderLineId=' + router.params.orderLineId,
      type: 2,
      isHide: false
    }
  ];

  const [goodsItem, setGoodsItem] = useState<any>({});
  const [orderId, setOrderId] = useState<any>('');
  const [goodsType, setGoodsType] = useState<any>(2);
  const [goList, setGolist] = useState<any>(REFUND_TYPE);

  useEffect(() => {
    Glo.loading('正在加载.');
    try {
      if (props.user.inApp) {
        (JSBridge as any).Common.Caller_Common_Base_getOpenToken('', (responsed) => {
          console.log(responsed, 'responsed');
          if (responsed) {
            _fetchToken({
              openToken: responsed
            }).then((res) => {
              if (res.code == 0) {
                console.log('代码测试');
                Taro.setStorage({
                  key: 'isLogin',
                  data: {
                    accessToken: res.data.accessToken
                  }
                });
                setTimeout(() => {
                  _getByOrderDetailId();
                }, 2000);
              } else {
                Glo.showToast('获取accessToken异常');
              }
            });
          } else {
            Glo.showToast('获取responsedy异常');
          }
        });
      } else {
        _getByOrderDetailId();
      }
    } catch (e) {
    } finally {
    }
  }, []);

  useDidShow(() => {});
  const _getByOrderDetailId = async () => {
    try {
      // if (props.user.inApp) {
      //   getAppToken();
      //   // let isLoginApp = await inAppIsLogin();
      //   // if (!isLoginApp) {
      //   //   await getAppToken();
      //   // }
      // }
      let lineDetailId = router && router.params.orderLineId;

      // fix 兼容 安卓 旧版APP 拼?参导致BUG
      if (lineDetailId.includes('?')) {
        let detailId = lineDetailId.split('?');
        lineDetailId = detailId[0];
      }
      let params: any = {
        orderdetail: lineDetailId
      };
      let res = await getByOrderDetailId(params);
      if (handleRequest(res as any)) {
        let data: any = res.data;
        setGoodsItem({
          name: data.name || '',
          image: data.pics[0] || '',
          norm1Name: data.skuProperties,
          num: data.num - data.cancelNum,
          price: data.payPrice || 0,
          picIds: data.pics[0] || ''
        });
        let goPages = REFUND_TYPE;
        if (data.goodsType === 2) {
          goPages[1].isHide = true;
        }
        setGolist(goPages);
        //  setGoodsType(data.goodsType);
        setOrderId(data.orderId);
        Glo.hideLoading();
      }
    } catch (e) {
      Glo.showToast('报错了');
      Glo.hideLoading();
    }
    // finally {
    //   Glo.hideLoading();
    // }
  };
  const goodsOption = {
    name: 'name',
    image: 'image'
  };

  //eslint-disable-next-line react/no-multi-comp
  const goPath = (item: any) => {
    if (item.path) {
      Taro.navigateTo({
        url: item.path + '&orderId=' + orderId
      });
    }
  };
  const isBorderStyle = {
    borderBottom: '1px solid #f5f5f5'
  };
  // eslint-disable-next-line react/no-multi-comp
  // const SelRefundType = () => {
  //   return (
  //     goList && (
  //       <View className="refund-type">
  //         {goList.map((item, index) => {
  //           return (
  //             !item.isHide && (
  //               <View
  //                 className="item"
  //                 style={(index === goList.length - 1 ? null : isBorderStyle) as any}
  //                 key={index}
  //                 onClick={() => goPath(item)}
  //               >
  //                 <Wrap type={2} flexDirection="row">
  //                   <View className="icon">
  //                     <Img src={item.icon} width={Taro.pxTransform(56)}></Img>
  //                   </View>
  //                   <View className="item-center">
  //                     <View className="title">{item.title}</View>
  //                     <View className="text">{item.text}</View>
  //                   </View>
  //                   <View className="item-right">
  //                     <Img src={goRight} width={48} height={48}></Img>
  //                   </View>
  //                 </Wrap>
  //               </View>
  //             )
  //           );
  //         })}
  //       </View>
  //     )
  //   );
  // };

  return (
    <Safe>
      {/* <Bar title="选择退款类型" /> */}
      <View className="refund-box">
        <View>
          <View>{!!goodsItem && <RefundGoodsItem options={goodsOption} item={goodsItem} />}</View>
        </View>
        {/* <MapItem /> */}
        <SelRefundType orderId={orderId} goList={goList} />
      </View>
    </Safe>
  );
};

Index.config = {
  navigationBarTitleText: '选择退款方式',
  enablePullDownRefresh: true,
  disableScroll: false,
  backgroundColor: '#f5f5f5',
  navigationStyle: 'custom'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    }
  };
};
export default connect(mapStateToProps as any)(Index as any);
//export default Index;
