import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';

import './index.scss';
import Tag from '../Tag';
import Txt from '../Txt';
import Wrap from '../Wrap';
import Img from '../Img';
import Yuan from '../Yuan';
import add1 from '../../assets/cart/add1.png';
import { connect } from '@tarojs/redux';
import right from '../../assets/cart_icon_forward_gray@2x.png';
// eslint-disable-next-line no-unused-vars
import { ProductBaiscData, ProductStoreInfo } from '../../interface';
import Router from '../../utils/router';

type PageStateProps = {
  cart: any;
  item: ProductBaiscData;
  isAdd?: boolean;
};

type PageDispatchProps = {
  addToCart: (goodsInfo) => void;
};

type PageOwnProps = {};

interface Props extends ProductBaiscData, ProductStoreInfo {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps & Props;

const Index: Taro.FC<IProps> = (props) => {
  const { item, isAdd = true } = props;
  let isXuni = false;
  if (item && item.goodsTypeNum === 2) {
    isXuni = true;
  }
  return (
    <View
      className="utp-goods1 utp-div"
      onClick={(e) => {
        e.preventDefault();
        if (!!item && [4, 5].includes(item.saleStatus)) {
          //已售罄  已下架  不可跳转
        } else {
          Taro.navigateTo({
            url: `/pages/details/index?id=${item.productId}&businessId=${item.businessId}&storeId=${item.storeId}`
          });
        }
      }}
    >
      <View className="utp-goods1-i utp-cnt">
        <Image
          className="utp-goods1-img"
          src={item && (item.productImage || item.picUrl || item.skuImgs[0])}
        />
      </View>
      <View className="utp-goods1-item">
        <View className="utp-goods1-titles">
          {!!item.selfSupport && (
            <View className="utp-goods1-titles-tag">
              <Tag>自营</Tag>
            </View>
          )}
          <Text
            className={
              item.selfSupport
                ? 'utp-goods1-title taro-text utp-goods1-indent'
                : 'utp-goods1-title taro-text'
            }
          >
            {!!item && (item.productName || item.name)}
          </Text>
        </View>
        <View className="utp-goods1-label">
          {
            !!item && item.tags && item.tags.length > 0 && item.tags.map((ite, index) => {
              return (
                <Tag type={4} key={index}>{ite}</Tag>
              )
            })
          }
        </View>
        <View className="utp-goods1-ft">
          <Wrap justifyContent="space-between">
            <Wrap className="utp-goods1-ft-top" alignItems="flex-end">
              {!!item && [4, 5].includes(item.saleStatus) ? (
                <View className="utp-goods1-moneys"></View>
              ) : (
                <View className="utp-goods1-moneys">
                  <Yuan price={item && (item.salePrice || item.price)} color="#FF2F7B" />
                </View>
              )}
              {!!item && (item.originPrice || item.originalPrice || item.skuPrice) && (
                <View className="utp-goods1-money-1">
                  <Yuan
                    price={item && (item.originPrice || item.originalPrice || item.skuPrice)}
                    color="#999"
                  />
                </View>
              )}
              {!!item && item.activeLabel && (
                <View className="utp-goods1-activelabel">{item.activeLabel}</View>
              )}
            </Wrap>
            {!isXuni && (
              <View
                className="utp-goods1-add utp-cnt"
                onClick={(e) => {
                  e.stopPropagation();
                  props.addToCart({
                    productId: item.productId,
                    number: 1,
                    skuId: item.skuId,
                    storeId: item.storeId,
                    businessId: item.businessId
                  });
                }}
                style={{ display: isAdd ? 'none' : 'block' }}
              >
                <Image className="utp-goods1-add-ico" src={add1} />
              </View>
            )}
          </Wrap>
          <View className="utp-goods1-stores">
            <Wrap type={2}>
              <Text className="utp-goods1-store">{item && item.storeName}</Text>
              <View
                className="utp-goods1-toStore"
                onClick={(e) => {
                  e.stopPropagation();
                  Router.goStore(item.storeTemplateCode, item.storeId, item.businessId);
                }}
              >
                <Txt title="进店" size={24} color="low" height={30} />
              </View>
              <Img src={right} width={22} />
            </Wrap>
          </View>
        </View>
      </View>
    </View>
  );
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    cart: state.cart
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    addToCart: (payload) => {
      dispatch({
        type: 'cart/addCart',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
