// import Taro from "@tarojs/taro";
import { View, ScrollView, Text, Image } from '@tarojs/components';
import Taro, { useState, useEffect } from '@tarojs/taro';

import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Img from '../../../components/Img';
import Score from '../../../components/Score';
import { myUnCommentList, myCommentList, DataType } from '../../../services/afterSale';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const [ReviewsTabIndex, setReviewsTabIndex] = useState(0);
  const [list, setList] = useState([] as DataType[]);
  const [unlist, setunList] = useState([] as DataType[]);
  const getList = async () => {
    let res = await myCommentList('');
    if (res.code == 0) {
      setunList(res.data.records.records);
    }
  };
  const getunList = async () => {
    let res = await myUnCommentList('');
    if (res.code == 0) {
      setList(res.data.records.records);
    }
  };
  useEffect(() => {
    getunList();
    getList();
  }, []);
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body ">
          <View className="utp-goodsReviews-ReviewsTab">
            <Wrap type={3}>
              {['待评价', '已评价'].map((item, index) => {
                return (
                  <View
                    key={index}
                    className={
                      index === ReviewsTabIndex
                        ? 'utp-goodsReviews-ReviewsTab-1-active'
                        : 'utp-goodsReviews-ReviewsTab-1'
                    }
                    onClick={() => {
                      // console.log('我切换', index);
                      setReviewsTabIndex(index);
                    }}
                  >
                    {item}
                  </View>
                );
              })}
            </Wrap>
          </View>

          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-goodsReviews-ReviewsGoods-Wrap"
            scrollY
          >
            {ReviewsTabIndex === 0 ? (
              <View>
                {list.map((item, index) => {
                  return (
                    <View key={index} className="utp-goodsReviews-ReviewsGoods">
                      <View
                        className="utp-goodsReviews-goodsInfo"
                        onClick={() => {
                          // Taro.navigateTo({
                          //   url: "/pages/order/index"
                          // });
                        }}
                      >
                        <Wrap type={1} top>
                          <View className="utp-goodsReviews-goodsInfo-imgs">
                            <Img src={item.picUrl} width={170} />
                          </View>
                          <View className="utp-goodsReviews-goodsInfo-titles">
                            <Text className="utp-goodsReviews-goodsInfo-title">{item.name}</Text>
                          </View>
                        </Wrap>
                      </View>
                      <View className="utp-goodsReviews-ReviewsBtn">
                        <Text
                          className="utp-goodsReviews-Btn"
                          onClick={() => {
                            Taro.setStorage({
                              key: 'list',
                              data: item
                            });
                            Taro.navigateTo({
                              url: `/pages/person/evaluate/index?orderDetailId=${item.orderDetailId}`
                            });
                          }}
                        >
                          评价
                        </Text>
                      </View>
                    </View>
                  );
                })}
              </View>
            ) : (
              <View>
                {unlist.map((o, oo) => {
                  return (
                    <View className="utp-goodsReviews-AlreadyReviews" key={oo}>
                      <Wrap flexDirection="column" top>
                        <View>
                          <View className="utp-goodsReviews-AlreadyReviews-goodsInfo">
                            <Wrap type={2}>
                              <View>
                                <Img src={o.productUrl} width={100}></Img>
                              </View>
                              <View className="utp-goodsReviews-AlreadyReviews-titlesBox">
                                <Wrap type={3} top flexDirection="column">
                                  <Text className="utp-goodsReviews-AlreadyReviews-titles">
                                    {o.name}
                                  </Text>
                                  <Score num={o.score} width={20} />
                                </Wrap>
                              </View>
                            </Wrap>
                          </View>
                        </View>
                        <View className="utp-goodsReviews-AlreadyReviews-Reviewscontent">
                          {o.comment}
                        </View>
                        {o.commentUrls && (
                          <View className="utp-goodsReviews-AlreadyReviews-ReviewsImgBox">
                            <Wrap type={2} top>
                              {o.commentUrls.map((p, pp) => {
                                return (
                                  <View
                                    className="utp-goodsReviews-AlreadyReviews-ReviewsImgBox-Img"
                                    key={pp}
                                  >
                                    <Image
                                      src={p}
                                      style={{ width: '100%', height: '100%' }}
                                    ></Image>
                                  </View>
                                );
                              })}
                            </Wrap>
                          </View>
                        )}
                        {/* {true && (
                          <View className="utp-goodsReviews-AlreadyReviews-storeRes">
                            <View className="utp-goodsReviews-AlreadyReviews-storeRes-content">
                              <Text className="utp-goodsReviews-AlreadyReviews-storeRes-content-1">
                                店铺回复:
                              </Text>
                              <Text className="utp-goodsReviews-AlreadyReviews-storeRes-content-2">
                                这里是店铺的回复，按实际字数显示，区域根据内容长度自行缩放展示全部
                              </Text>
                            </View>
                          </View>
                        )} */}
                      </Wrap>
                    </View>
                  );
                })}
              </View>
            )}
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};
Index.config = {
  navigationBarTitleText: '商品评价',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};

Index.defaultProps = {};
export default Index;
