import Taro, { useEffect, useState, useMemo } from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import Wrap from '@/components/Wrap';
import Img from '@/components/Img';
import addCartImg from '../../../assets/search/add.png';
import shanDian from '../../../assets/icon_shandian.png';
import { spliceStr, priceToFixed, PriceFormat } from '../common/tool';
import './index.scss';

const ViewSameCityGoods = (props: any) => {
  const { item, add, goStore } = props;

  return (
    item && (
      <View className="sameCity-item">
        <Wrap type={2} flexDirection="row" top>
          <View className="sameCity-item-img">
            <Img src={item.productImage} width={256}></Img>
          </View>
          <View className="sameCity-item-info">
            <View className="sameCity-item-info-title">
              <Text className="sameCity-item-info-name">
                {item.isSelf && (
                  <Text className="sameCity-item-info-tag" style={{ display: 'inline-block' }}>
                    自营
                  </Text>
                )}
                {item.productName}
              </Text>
            </View>
            <View className="sameCity-item-info-delivery">
              <Wrap type={2} flexDirection="row" top>
                <View className="delivery-tag">
                  <Img src={shanDian} width={32} height={32}></Img>
                </View>
                <Text className="delivery-text">预计{item.time}h送达</Text>
              </Wrap>
            </View>
            <View className="sameCity-item-info-price">
              <Wrap type={1} flexDirection="row" top justifyContent="space-between">
                <View className="now-price">
                  <Text>
                    <Text style={{ fontSize: Taro.pxTransform(28), display: 'inline-block' }}>
                      ￥
                    </Text>
                    {PriceFormat(item.salePrice)}
                    <Text
                      style={{
                        display: 'inline-block',
                        fontSize: Taro.pxTransform(24),
                        color: '#666',
                        marginLeft: 5,
                        textDecoration: 'line-through'
                      }}
                    >
                      ￥{PriceFormat(item.originPrice)}
                    </Text>
                  </Text>
                </View>

                <View className="add" onClick={() => add && add()}>
                  <Img src={addCartImg} width={44}></Img>
                </View>
              </Wrap>
            </View>
            <View
              className="sameCity-item-info-store"
              onClick={() => {
                goStore && goStore();
              }}
            >
              <Wrap top flexDirection="row">
                <Text> {spliceStr(item.storeName, 9)}</Text>
                <Text style={{ marginLeft: Taro.pxTransform(10) }}>{'进店>'}</Text>
              </Wrap>
            </View>
          </View>
        </Wrap>
      </View>
    )
  );
};
export default ViewSameCityGoods;
