import Taro from '@tarojs/taro';
import { View, Image } from '@tarojs/components';
import { formatTime } from '@/utils/utils';

import './details.scss';

interface DataProps {
  tradeTypeText: string;
  createTime: string;
  score: number;
  isEffective: number;
}

interface IProps {
  data: DataProps;
}

const Index: Taro.FC<IProps> = (props) => {
  return (
    <View className="utp-integral-details-card">
      <View className="utp-integral-details-card-left">
        <View className="utp-integral-details-card-left-top">
          <View className="utp-integral-details-card-left-top-name">{props.data.tradeTypeText}</View>
          {
            props.data.isEffective == 0 ? 
            <View className="utp-integral-details-card-left-top-label">待生效</View> :
            <View className="utp-integral-details-card-left-top-label">已生效</View>
          }
        </View>
        <View className="utp-integral-details-card-left-bottom">
          {
            (props && props.data) && (
              formatTime(props.data.createTime, 'Y.M.D h:m:s')
            )
          }
        </View>
      </View>
      <View className="utp-integral-details-card-right">+{props.data.score}</View>
    </View>
  );
};

Index.defaultProps = {};
export default Index;
