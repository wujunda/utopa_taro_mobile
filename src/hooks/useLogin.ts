import Taro, { useState, useEffect } from '@tarojs/taro';
import request from '../utils/request';

function useLogin(): [boolean, () => void, boolean] {
  const [isLogin, setIsLogin] = useState(false);
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(0);

  useEffect(() => {
    return;
    var login = Taro.getStorageSync('login');
    // console.log('请求登陆666');
    if (!login && !loading) {
      setLoading(true);
      request
        .post<any>('/app.do', {
          method: 'account.login.doLogin',
          ver: '1.0',
          params: {
            account: '13755713692',
            clientType: '1',
            keepAccess: '1',
            loginType: '2',
            password: '123456',
            sysId: '1',
            uid: ''
          }
        })
        .then((data) => {
          setLoading(false);
          console.warn('hooks Login');
          console.warn(data);

          if (data && data.code == 0) {
            console.warn('设置login');
            Taro.setStorage({
              key: 'login',
              data: data.data.accessToken
            });
            Taro.setStorage({
              key: 'uid',
              data: data.data.uid
            });
            var login = Taro.getStorageSync('login');
            console.warn('缓存login');
            console.warn(login);
            setIsLogin(true);
          } else {
            setIsLogin(false);
          }
        });
    } else {
      setIsLogin(true);
    }
  }, [count]);

  useEffect(() => {
    return () => {};
  }, []);

  const refresh = () => {
    console.warn('更新login');
    setCount(count + 1);
  };

  return [isLogin, refresh, loading];
}

export default useLogin;
