/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-key */
/* eslint-disable import/first */
import Taro, { useState, useEffect, useRouter, useMemo, useDidShow } from '@tarojs/taro';
import { View, Text, Picker, Input } from '@tarojs/components';
import './index.scss';
import Safe from '@/components/safe';
import Wrap from '@/components/Wrap';
import go_right from '../../../assets/more.png';
import Img from '@/components/Img';
import RefundGoodsItem from '@/components/RefundGoodsItem';
import {
  getRefundDetailInfo,
  recallAfterSaleOrdere,
  quitRefundOrder
} from '../../../services/afterSale';
import { getExpressCompanys, saveExpressCompanys } from '../../../services/common';
import {
  handleRequest,
  InitEndTime,
  Glo,
  parseTime,
  PriceFormat,
  timeDiffStr,
  inAppIsLogin,
  getAppToken
} from '@/utils/utils';
import { REFUND_DETAIL_STATUS, REFUND_TYPE } from '@/utils/common_enum';
import useSession from '@/hooks/useSession';
import useLogin from '@/hooks/useLogin';
import ViewPickReason from '../applyRefund/selectPick';
import {
  REFUND_STATUS_TEXT,
  REFUND_STATUS_TEXT_HEAD,
  WAIT_B_RECEIVED_TEXT_LIST,
  LIST_TEXT
} from './orderWords';
import JSBridge from '@/utils/jsbridge/index';
import { connect } from '@tarojs/redux';
import { _fetchToken } from '@/services/login';

interface IProps {}
const options = {
  image: 'pics'
};
type PageStateProps = {
  user: {
    inApp: boolean;
    isWeiXinFalg: boolean;
  };
};
// 中间内容灰色描述文字
const SMALL_LIST = {
  [REFUND_DETAIL_STATUS.WAIT_VERIFY]: LIST_TEXT,
  [REFUND_DETAIL_STATUS.WAIT_B_RECEIVED]: WAIT_B_RECEIVED_TEXT_LIST
};
// 头标题 headLabel
const BTN_STYLE = {
  border: '1px solid rgba(255,47,123,1)',
  opacity: '0.68',
  color: '#FF2F7B',
  background: '#fff',
  boxSizing: 'border-box'
};
const ACTION_BTN = [
  {
    text: '修改',
    type: 1,
    //某状态下显示的KEY
    showKey: [REFUND_DETAIL_STATUS.WAIT_VERIFY],
    isStyle: true
  },
  {
    text: '修改退款',
    type: 3,
    showKey: [REFUND_DETAIL_STATUS.B_REJECT],
    isStyle: true
  },
  {
    text: '填写物流',
    type: 4,
    showKey: [REFUND_DETAIL_STATUS.WAI_SEND_BACK],
    isStyle: true
  },
  {
    text: '跟踪物流',
    type: 5,
    showKey: [REFUND_DETAIL_STATUS.WAIT_B_RECEIVED],
    isStyle: false
  },
  {
    text: '修改物流',
    type: 6,
    showKey: [REFUND_DETAIL_STATUS.WAIT_B_RECEIVED],
    isStyle: true
  },
  {
    text: '撤销申请',
    type: 2,
    showKey: [
      REFUND_DETAIL_STATUS.WAIT_VERIFY,
      REFUND_DETAIL_STATUS.B_REJECT,
      REFUND_DETAIL_STATUS.WAI_SEND_BACK,
      REFUND_DETAIL_STATUS.REJECT_RECEIVED,
      REFUND_DETAIL_STATUS.WAIT_B_RECEIVED
    ],
    isStyle: false
  }
];
const TEXT_COLOR = '#FF2F7B';
const priceStyle = {
  color: TEXT_COLOR
};
const DATE_STYLE = {
  display: 'inline-block',
  color: '#fff',
  fontSize: Taro.pxTransform(28)
};

// eslint-disable-next-line react/no-multi-comp
const Index: Taro.FC<IProps> = (props: any): any => {
  //const [haveSession] = useSession();
  const router = useRouter();
  // const [isLogin, updateLogin] = useLogin();
  // const [haveSession] = useSession();
  // if (haveSession && !isLogin) {
  //   if (updateLogin) {
  //     updateLogin();
  //   }
  // }
  const [detailInfo, setDetail] = useState<any>({});
  const [midMsgKV, setMidMsgKV] = useState<any>([]);
  const [goods, setGoods] = useState<any>([]);
  const [popShow, setPopShow] = useState<any>(false);
  const [logisList, setLogisList] = useState<any>([]);
  const [timeDisparity, setTimeDisparity] = useState<any>();

  useEffect(() => {
    Glo.loading('正在加载.');
    try {
      if (props.user.inApp) {
        (JSBridge as any).Common.Caller_Common_Base_getOpenToken('', (responsed) => {
          console.log(responsed, 'responsed');
          if (responsed) {
            _fetchToken({
              openToken: responsed
            }).then((res) => {
              if (res.code === 0) {
                console.log('代码测试');
                Taro.setStorage({
                  key: 'isLogin',
                  data: {
                    accessToken: res.data.accessToken
                  }
                });
                setTimeout(() => {
                  getDetail();
                }, 2000);
              }
            });
          }
        });
      } else {
        getDetail();
      }
    } catch (e) {}
  }, []);
  useDidShow(() => {});

  // 获取详情
  const getDetail = async () => {
    let params: any = {
      refundOrderNo: router.params.refundOrderNo
    };
    try {
      const res = (await getRefundDetailInfo(params)) as any;
      if (handleRequest(res)) {
        let data = res.data;
        // 倒计时

        setTimeDisparity(timeDiffStr(data.refundDetailMasterBody.timeDisparity));
        setDetail(data.refundDetailMasterBody);
        data.refundDetailMasterBody &&
          data.refundDetailMasterBody.refundSkuDetails &&
          setGoods(data.refundDetailMasterBody.refundSkuDetails);
        if (data.midMsgKV)
          // eslint-disable-next-line no-unused-vars
          var mids: any = [];
        //处理键值对 数据 []
        mids = Object.keys(data.midMsgKV).map((val) => ({
          key: val,
          val: data.midMsgKV[val]
        }));
        setMidMsgKV(mids);
        console.log(mids, 'mids');
        /*
         等于该状态时候请求物流信息列表
        */
        let status: any = data.refundDetailMasterBody.status;
        if (
          data &&
          (status === REFUND_DETAIL_STATUS.WAI_SEND_BACK ||
            status === REFUND_DETAIL_STATUS.WAIT_B_RECEIVED ||
            status === REFUND_DETAIL_STATUS.REJECT_RECEIVED)
        ) {
          _getExpressCompanys();
        }
        Glo.hideLoading();
      }
    } catch (e) {
      // console.log(e);
    }
  };

  /***
   * 获取物流信息
   * ***/
  const _getExpressCompanys = async () => {
    let res = await getExpressCompanys();
    if (handleRequest(res as any)) {
      setLogisList(res.data);
      console.log(res);
    }
  };

  /*  
  撤消申请 
  */
  const _recallAfterSaleOrdere = async () => {
    let res = await quitRefundOrder({ refundOrderId: detailInfo.id });
    if (handleRequest(res as any)) {
      Glo.showToast('撤销成功');
      getDetail();
    }
  };
  /**
   * 各个状态按钮事件  url: `/pages/store/shoppingStore/index?storeId=${storeId}&businessId=${businessId}`
   * **/
  const actionBtnCase = (item) => {
    switch (item.type) {
      // 修改退款
      case 1:
        Taro.navigateTo({
          url: `/pages/person/applyRefund/index?refundOrderNo=${detailInfo.refundOrderNo}&update=true&orderId=${detailInfo.orderId}&type=${detailInfo.refundType}`
        });
        break;

      case 2:
        _recallAfterSaleOrdere();
        break;
      // 修改退款
      case 3:
        Taro.navigateTo({
          url: `/pages/person/applyRefund/index?refundOrderNo=${detailInfo.refundOrderNo}&update=true&orderId=${detailInfo.orderId}&type=${detailInfo.refundType}`
        });
        break;
      case 4:
        Taro.navigateTo({
          url: `/pages/person/fillLogis/index?refundOrderNo=${detailInfo.refundOrderNo}&update=true&orderId=${detailInfo.orderId}&type=${detailInfo.refundType}`
        });
        break;
      // 跟中物流
      case 5:
        // todo...
        let companyCode: any;
        let expressNo: any;
        for (let i = 0; i < midMsgKV.length; i++) {
          if (midMsgKV[i].key === '退款物流单号') {
            expressNo = midMsgKV[i].val;
          }
          if (midMsgKV[i].key === '物流公司') {
            companyCode = midMsgKV[i].val;
          }
        }
        Taro.navigateTo({
          url: `/pages/person/checkLogistics/index?companyCode=${companyCode}&expressNo=${expressNo}&orderId=${expressNo}`
        });
        break;
      case 6:
        Taro.navigateTo({
          url: `/pages/person/fillLogis/index?refundOrderNo=${detailInfo.refundOrderNo}&update=true&orderId=${detailInfo.orderId}&type=${detailInfo.refundType}`
        });
        setPopShow(true);
        break;
    }
  };

  const _saveExpress = async (params: any) => {
    let data = { ...params, ...{ refundOrderNo: router.params.refundOrderNo } };
    Glo.loading('正在保存');
    try {
      let res = await saveExpressCompanys(data);
      if (handleRequest(res as any)) {
        Glo.showToast('保存成功');
        setPopShow(false);
        getDetail();
        Glo.hideLoading();
      }
    } catch (e) {
      console.log(e);
      Glo.hideLoading();
    }
  };

  //eslint-disable-next-line react/no-multi-comp
  return (
    <Safe>
      <View className="refund-detail">
        <View className="refund-detail-head">
          <View className="refund-detail-head-title">
            {detailInfo && REFUND_STATUS_TEXT_HEAD[detailInfo.status]}
          </View>
          {/* <ViewPupShowMemo isShow={popShow}>
            <ViewportContentMemo
              close={() => {
                setPopShow(false);
              }}
              pickData={logisList}
              save={async (v: any) => {
                _saveExpress(v);
              }}
            />
          </ViewPupShowMemo> */}
          <View>
            {!!detailInfo && detailInfo.status === REFUND_DETAIL_STATUS.WAIT_VERIFY && (
              <View style={DATE_STYLE}>
                <View style={DATE_STYLE}>{timeDisparity}</View>
                后系统自动同意
              </View>
            )}
            {REFUND_DETAIL_STATUS.B_REJECT === detailInfo.status && (
              <View style={DATE_STYLE}>
                还剩
                <View style={DATE_STYLE}>{timeDisparity}</View>
                买家未处理自动关闭
              </View>
            )}
            {REFUND_DETAIL_STATUS.WAI_SEND_BACK === detailInfo.status && (
              <View style={DATE_STYLE}>
                <View style={DATE_STYLE}>{timeDisparity}</View>
                后逾期填写系统自动关闭
              </View>
            )}
            {REFUND_DETAIL_STATUS.WAIT_B_RECEIVED === detailInfo.status && (
              <View style={DATE_STYLE}>
                <View style={DATE_STYLE}>{timeDisparity}</View>
                后系统自动同意
              </View>
            )}
            {REFUND_DETAIL_STATUS.REJECT_RECEIVED === detailInfo.status && (
              <View style={DATE_STYLE}>
                <View style={DATE_STYLE}>{timeDisparity}</View>
                后系统自动关闭退款
              </View>
            )}
            {REFUND_DETAIL_STATUS.CLOSE === detailInfo.status && (
              <View style={DATE_STYLE}>
                {parseTime(Number(detailInfo.closeTime), '{y}年{m}月{d}日 {h}:{i}:{s}')}
              </View>
            )}
            {(REFUND_DETAIL_STATUS.REFUND_ING === detailInfo.status ||
              REFUND_DETAIL_STATUS.COMPLETE_REFUND === detailInfo.status) && (
              <View style={DATE_STYLE}>
                {parseTime(Number(detailInfo.refundTime), '{y}年{m}月{d}日 {h}:{i}:{s}')}
              </View>
            )}
          </View>
        </View>

        <View className="refund-detail-content">
          <View className="content-title">
            <Text>{REFUND_STATUS_TEXT[detailInfo.status] || '空'}</Text>
          </View>
          {(detailInfo.status === REFUND_DETAIL_STATUS.REFUND_ING ||
            detailInfo.status === REFUND_DETAIL_STATUS.COMPLETE_REFUND) && (
            <View style={{ marginTop: Taro.pxTransform(10) }}>
              <Wrap type={2}>
                <Text
                  style={{
                    color: '#333',
                    fontSize: Taro.pxTransform(28),
                    lineHeight: Taro.pxTransform(41)
                  }}
                >
                  退款总金额:
                </Text>
                <Text
                  style={{
                    color: '#FF2F7B',
                    fontSize: Taro.pxTransform(28),
                    paddingLeft: 5,
                    lineHeight: Taro.pxTransform(41)
                  }}
                >
                  ¥{PriceFormat(detailInfo.goodsRefundFee + detailInfo.postRefundFee, 2)}
                </Text>
              </Wrap>
            </View>
          )}

          <View style={{ margin: '15px 0 15px 0' }}>
            {!!midMsgKV &&
              midMsgKV.map((item) => {
                return (
                  <View className="reason-item">
                    <Wrap type={2}>
                      <Text>{item.key}：</Text>
                      <Text>{item.val}</Text>
                    </Wrap>
                  </View>
                );
              })}
          </View>
          <View>
            {SMALL_LIST[detailInfo.status] && (
              <View className="cont-list-text">
                {SMALL_LIST &&
                  SMALL_LIST[detailInfo.status].map((item, idx) => {
                    return (
                      <View className="text-item" style={{ marginTop: -2 }} key={idx}>
                        <View className="text">
                          <View className="text-tag">
                            <View className="tag"> </View>
                          </View>
                          {item.text}
                        </View>
                      </View>
                    );
                  })}
              </View>
            )}
          </View>
          <View
            className="refund-detail-content-btn"
            style={{ textAlign: 'right', marginRight: 10 }}
          >
            <View>
              {ACTION_BTN &&
                ACTION_BTN.map((item: any) => {
                  return (
                    item.showKey.includes(detailInfo.status) && (
                      <View
                        onClick={() => {
                          actionBtnCase(item);
                        }}
                        style={item.type ? BTN_STYLE : ('' as any)}
                        className="btn-item"
                      >
                        {item.text}
                      </View>
                    )
                  );
                })}
            </View>
          </View>
        </View>
        <View
          className="history"
          onClick={() => {
            Taro.navigateTo({
              url: '/pages/person/negotiationHistory/index?refundOrderId=' + detailInfo.id
            });
          }}
        >
          <Wrap justifyContent="space-between" flexDirection="row">
            <Text className="text" style={{ width: 120 }}>
              协商历史
            </Text>
            <View className="tag">
              <Img src={go_right} width={24}></Img>
            </View>
          </Wrap>
        </View>
        <View className="goods-info">
          <View>
            {!!goods &&
              goods.map((item, idx) => {
                if (item) {
                  return <RefundGoodsItem options={options} item={item} key={idx} />;
                }
              })}
          </View>
          <View className="refund-info">
            <View className="info-item">
              <Wrap type={1}>
                <View className="titles">退款类型：</View>
                <View className="words"> {detailInfo && REFUND_TYPE[detailInfo.refundType]}</View>
              </Wrap>
            </View>
            <View className="info-item">
              <Wrap type={1}>
                <View className="titles">退款原因：</View>
                <View className="words">{detailInfo && detailInfo.applyRefundReason}</View>
              </Wrap>
            </View>
            <View className="info-item">
              <Wrap type={1}>
                <View className="titles">退款描述：</View>
                <View className="words">{detailInfo && detailInfo.applyRefundDescription}</View>
              </Wrap>
            </View>
            <View className="info-item">
              <Wrap type={1}>
                <View className="titles">退款金额：</View>
                <View className="words" style={priceStyle}>
                  ￥{detailInfo && PriceFormat(detailInfo.goodsRefundFee)}
                </View>
              </Wrap>
            </View>
            <View className="info-item">
              <Wrap type={1}>
                <View className="titles">运费退款：</View>
                <View className="words" style={priceStyle}>
                  ￥ {detailInfo && PriceFormat(detailInfo.postRefundFee)}
                </View>
              </Wrap>
            </View>
            <View className="info-item">
              <Wrap type={1}>
                <View className="titles">退款单号：</View>
                <View className="words">{(detailInfo && detailInfo.refundOrderNo) || '无'}</View>
                <View
                  className="words"
                  onClick={() => {
                    Glo.copyContent(detailInfo.refundOrderNo);
                  }}
                  style={{ marginLeft: '10px', color: TEXT_COLOR }}
                >
                  复制
                </View>
              </Wrap>
            </View>
            <View className="info-item">
              <View className="titles">凭证图片：</View>
              <View className="img-list">
                {!!detailInfo &&
                  detailInfo.certificatePics.map((item) => {
                    return (
                      <View style={{ margin: 5, display: 'inline-block' }}>
                        <Img src={item.url} width={180} />
                      </View>
                    );
                  })}
              </View>
            </View>
          </View>
        </View>
        <View className="fixed-bom">
          <View
            onClick={() => {
              if (props.user.inApp) {
                let cmdParams = {
                  cmd: 'C0050708'
                };
                JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, (res) => {
                  console.log(res, 'JSBridge---联系客服');
                });
              } else {
                Taro.showToast({ icon: 'none', title: '请下载优托邦APP' });
              }
            }}
            style={BTN_STYLE as any}
            className="btn-item"
          >
            联系客服
          </View>
        </View>
      </View>
    </Safe>
  );
};

Index.config = {
  navigationBarTitleText: '退款详情',
  enablePullDownRefresh: true,
  disableScroll: false,
  navigationStyle: 'default'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    }
  };
};
export default connect(mapStateToProps as any)(Index as any);
// @ts-ignore
// export default Index;
