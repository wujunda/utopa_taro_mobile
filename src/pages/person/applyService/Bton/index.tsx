import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import Wrap from '../../../../components/Wrap';
import './index.scss';

interface IProps {
  item: any;
}
const Index: Taro.FC<IProps> = ({ item }) => {
  return (
    <View className="utp-applyService-ServiceBtn" style={{ margin: '20px' }}>
      {!!item && !!item.products && item.products.filter((it) => it.status === 10).length > 0 ? (
        <Wrap>
          <Text className="utp-applyService-tips">该订单已过售后期</Text>
          {/* <Text className="utp-applyService-Btn utp-applyService-disableBtn">
                                    退款
                                  </Text> */}
        </Wrap>
      ) : (
        <Text
          className="utp-applyService-Btn"
          onClick={() => {
            // let refundItem = JSON.stringify(item);
            Taro.setStorage({
              key: 'product',
              data: item
            });
            Taro.navigateTo({
              url: `/pages/person/selectRefundType/index?orderLineId=${
                (item as any).products[0].detailId
              }`
            });
          }}
        >
          退款
        </Text>
      )}
    </View>
  );
};
Index.defaultProps = {};

export default Index;
