import request from '../utils/request';

export interface Gprops {
  id: number;
  name: string;
  pic: string;
  price: number;
  skuId: number;
  norm1Value: string;
  norm2Value: string;
  num: number;
  limitNum: number;
  picIds: string;
  buyerMemo: string;
  orderNo: string;
  endShippingTime: string;
  originPrice:number;
  actuallyPrice: number;
  storeName?: string;
}
export interface Rprops {
  businessName: string;
  cancleReason: string;
  orderProducts: Gprops[];
  orderTotalMoney: number;
  goodsNum: number;
  creatTime: number;
  businessId: number;
  selfSupport:number;
  serviceStatus:number;
}
export interface AfterProps {
  closeReason: string;
  recalled: string;
  showRecallButton: number;
  orderNo: string;
  createTime: number;
  goodsTotalNum: number;
  skuId: number;
  orderId: number;
  products: AfterPro[];
}
interface AfterPro {
  skuInfo: string;
  productName: string;
  picUrl: string;
  price: number;
  num: number;
  status: number;
}
export interface Iprops {
  data: {
    records: Rprops[];
    serviceStatus: number;
    userAddress: any;
    businessName: string;
    expressDetail: string;
    orderNo: string;
    createTime: number;
    saleType: number;
    payWay: number;
    orderTotalMoney: number;
    products: any[];
    expressPrice: number;
    storeProductList: Rprops[];
    productPrice: number;
    usedPoint: number;
    exchangeType: number;
    validDuration:number
  };
}
// 订单列表
export const myProductOrders = (tab: any, pst: any) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.productorder.app.myProductOrders',
    ver: '1.0',
    params: JSON.stringify({
      // accessToken:Taro.getStorageSync('isLogin').accessToken,
      pageNo: pst.page,
      pageSize: pst.per_page,
      status: tab
    })
  });
};
// 订单详情
export const productOrderDetail = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.productorder.bin.app.productOrderDetail',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
//用户取消订单
export const cancleOrderDetail = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.productorder.app.cancleOrderDetail',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      sysId:1
    })
  });
};