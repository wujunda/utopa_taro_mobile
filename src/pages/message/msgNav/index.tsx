import Taro, { useRouter } from '@tarojs/taro';
import { View, Text, Image } from '@tarojs/components';

import Img from '../../../components/Img';
import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Txt from '../../../components/Txt';
import Tag from '../../../components/Tag';

import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';

import './index.scss';

import goods from '../../../assets/goods.png';
import { IMsg, IMsgInfo, getMsg } from '../../../services/msg';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const router = useRouter();
  let id = router.params.id;
  let name = router.params.name;
  Taro.setNavigationBarTitle({
    title: decodeURI(name)
  });

  const [state, hasMore, update, getMoreData, loading] = useRequestWIthMore<IMsg>(
    { msgGuideId: id },
    getMsg
  );
  if (false) {
    // console.log(hasMore);
    // console.log(update);
    // console.log(getMoreData);
    // console.log(loading);
  }
  var list: IMsgInfo[] = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.data.records.map((item) => {
        list.push(item);
      });
    });
  }
  const changeTime = (t) => {
    if (t && t !== null) {
      let time = new Date(t);
      let y = time.getFullYear();
      let m = time.getMonth() + 1 > 10 ? time.getMonth() + 1 : '0' + (time.getMonth() + 1);
      let d = time.getDate() > 10 ? time.getDate() : '0' + time.getDate();
      let h = time.getHours() > 10 ? time.getHours() : '0' + time.getHours();
      let min = time.getMinutes() > 10 ? time.getMinutes() : '0' + time.getMinutes();
      let sec = time.getSeconds() > 10 ? time.getSeconds() : '0' + time.getSeconds();
      return `${y}-${m}-${d} ${h}:${min}:${sec}`;
    }
    return '';
  };

  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <View className="utp-msgNav">
            {loading && goods.length === 0 && <Loading />}
            {!loading && goods.length === 0 && <LoadingNone />}
            {list.map((item) => {
              return (
                <View key={item.mId}>
                  {
                    // 文字消息
                    true && (
                      <View>
                        <View
                          style={{
                            marginTop: Taro.pxTransform(40),
                            marginBottom: Taro.pxTransform(40)
                          }}
                        >
                          <Wrap type={3}>
                            <Tag type={21}>{changeTime(item.createTime)}</Tag>
                          </Wrap>
                        </View>
                        <View className="utp-msgNav-content utp-msgNav-content-onlytext">
                          <Txt
                            size={40}
                            bold
                            title={item.title}
                            justifyContent="flex-start"
                            height={52}
                          ></Txt>
                          <Wrap type={2}>
                            <Text className="utp-msgNav-msgInfo-text utp-msgNav-msgInfo-text-1">
                              {item.content}
                            </Text>
                          </Wrap>
                        </View>
                      </View>
                    )
                  }
                  {
                    // 中图
                    false && (
                      <View>
                        <View
                          style={{
                            marginTop: Taro.pxTransform(40),
                            marginBottom: Taro.pxTransform(40)
                          }}
                        >
                          <Wrap type={3}>
                            <Tag type={21}>时间1</Tag>
                          </Wrap>
                        </View>
                        <View
                          className={
                            false ? 'utp-msgNav-content-bigImgContent' : 'utp-msgNav-content'
                          }
                        >
                          {true && (
                            <Txt
                              size={40}
                              bold
                              title="消息标题"
                              justifyContent="flex-start"
                              height={52}
                            ></Txt>
                          )}

                          <View className="utp-msgNav-msgInfo">
                            <Wrap type={2}>
                              {true ? (
                                <Img src={goods} width={160}></Img>
                              ) : (
                                <View className="utp-msgNav-msgInfo-hasBigImg">
                                  <Image className="utp-msgNav-msgInfo-bigImg" src={goods} />
                                  <Txt
                                    size={40}
                                    bold
                                    title="消息标题"
                                    justifyContent="flex-start"
                                    height={52}
                                  ></Txt>
                                </View>
                              )}

                              <Text className="utp-msgNav-msgInfo-text">内容</Text>
                            </Wrap>
                          </View>
                        </View>
                      </View>
                    )
                  }

                  {
                    // 大图
                    false && (
                      <View>
                        <View
                          style={{
                            marginTop: Taro.pxTransform(40),
                            marginBottom: Taro.pxTransform(40)
                          }}
                        >
                          <Wrap type={3}>
                            <Tag type={21}>时间1</Tag>
                          </Wrap>
                        </View>
                        <View
                          className={
                            false ? 'utp-msgNav-content-bigImgContent' : 'utp-msgNav-content'
                          }
                        >
                          {false && (
                            <Txt
                              size={40}
                              bold
                              title="消息标题"
                              justifyContent="flex-start"
                              height={52}
                            ></Txt>
                          )}

                          <View className="utp-msgNav-msgInfo">
                            <Wrap type={2}>
                              {false ? (
                                <Img src={goods} width={160}></Img>
                              ) : (
                                <View className="utp-msgNav-msgInfo-hasBigImg">
                                  <Image className="utp-msgNav-msgInfo-bigImg" src={goods} />
                                  <Txt
                                    size={40}
                                    bold
                                    title="消息标题"
                                    justifyContent="flex-start"
                                    height={52}
                                  ></Txt>
                                </View>
                              )}

                              <Text className="utp-msgNav-msgInfo-text">内容</Text>
                            </Wrap>
                          </View>
                        </View>
                      </View>
                    )
                  }
                </View>
              );
            })}
            {loading && goods.length !== 0 && <LoadingMore />}
            {!loading && !hasMore && goods.length > 0 && <LoadingNoneMore />}
          </View>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};

Index.config = {
  navigationBarTitleText: 'name',
  disableScroll: false
  //navigationStyle: 'custom',
  //usingComponents: {
  //Bar: '../../../components/bar' // 书写第三方组件的相对路径
  //}
};

export default Index;
