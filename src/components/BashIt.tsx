import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import './BashIt.scss';
import Txt from './Txt';

type PageStateProps = {
  // counter: {
  //   num: number;
  // };
};

type PageDispatchProps = {
  // add: () => void;
  // dec: () => void;
  // asyncAdd: () => any;
};

type PageOwnProps = {
  type: number;
  top: number;
  bottom: number;
  // 图片
  src?: any;
  srcWidth: number;
  srcHeight?: number;
  // 提示
  title?: string;
  titleHeight?: number;
  titleColor?: string;
  titleSize?: number;
  // 提示
  tips: string;
  tipsHeight: number;
  tipsColor: string;
  tipsSize: number;
  // 数量
  num?: number | string;
  numType?: 1;

  // 位置
  wrapTop?: number;
  //是否圆角
  isCircular?: boolean;
  onClick?: () => void;
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = (props) => {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */

  if (!props.wrapTop) {
    props.wrapTop = -4;
  }
  return (
    <View
      onClick={props.onClick}
      style={{
        paddingTop: Taro.pxTransform(props.top),
        paddingBottom: Taro.pxTransform(props.bottom)
      }}
    >
      {!!props.src && (
        <View
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <View
            style={{
              width: Taro.pxTransform(props.srcWidth),
              height: Taro.pxTransform(!!props.srcHeight ? props.srcHeight : props.srcWidth),
              position: 'relative'
            }}
          >
            <Image
              src={props.src}
              style={{
                width: '100%',
                height: '100%',
                borderRadius: props.isCircular ? '50%' : '0%'
              }}
            ></Image>
            {!!props.num && (
              <View
                style={{
                  position: 'absolute',
                  zIndex: 1,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  top: Taro.pxTransform(props.wrapTop),
                  right: Taro.pxTransform(-30),
                  left: Taro.pxTransform(30)
                }}
              >
                <View
                  className="utp-div utp-solid"
                  style={{
                    height: Taro.pxTransform(32),
                    minWidth: Taro.pxTransform(32),
                    borderWidth: Taro.pxTransform(2),
                    borderColor: '#ff2f7b',
                    backgroundColor: props.numType === 1 ? '#ff2f7b' : '#ffffff',
                    borderRadius: Taro.pxTransform(32),
                    paddingLeft: Taro.pxTransform(5),
                    paddingRight: Taro.pxTransform(5),
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <Txt
                    size={23}
                    color={props.numType === 1 ? 'white' : 'active'}
                    title={props.num}
                  ></Txt>
                </View>
              </View>
            )}
          </View>
        </View>
      )}
      {!!props.title && !!props.titleSize && !!props.titleHeight && !!props.titleColor && (
        <View
          style={{
            height: Taro.pxTransform(props.titleHeight),
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'wrap'
          }}
        >
          <Text
            style={{
              color: props.titleColor,
              fontSize: Taro.pxTransform(props.titleSize),
              position: 'relative'
            }}
          >
            {props.title}
          </Text>
        </View>
      )}

      {!!props.tips && (
        <View
          style={{
            marginTop: Taro.pxTransform(props.type === 1 ? -(props.tipsHeight - 8) : 0),
            height: Taro.pxTransform(props.tipsHeight),
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'wrap'
          }}
        >
          <Text
            style={{
              color: props.tipsColor,
              fontSize: Taro.pxTransform(props.tipsSize),
              position: 'relative'
            }}
          >
            {props.tips}
          </Text>
        </View>
      )}
    </View>
  );
};

Index.defaultProps = {};
export default Index;
