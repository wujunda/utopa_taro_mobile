import Taro, { Component, Config } from '@tarojs/taro';
import { Provider } from '@tarojs/redux';
import 'taro-ui/dist/style/index.scss';
import Index from './pages/index';
import './app.scss';
import dva from './utils/dva';
import JSBridge from './utils/jsbridge/index';
import models from './models';
import { Glo, UrlSearch } from './utils/utils';
import { getSession } from './services/user';
import { _fetchToken } from './services/login';

console.log('test,,,,');

if (process.env.TARO_ENV === 'rn') {
  // @ts-ignore
  console.ignoredYellowBox = [
    'Warning: BackAndroid is deprecated. Please use BackHandler instead.',
    'source.uri should not be an empty string',
    'Invalid props.style key'
  ];
  // @ts-ignore
  console.disableYellowBox = true; // 关闭全部黄色警告
}

// h5环境下开启调试工具
// if (process.env.SERVER_ENV !== 'prod' && process.env.TARO_ENV === 'h5') {
//   var script = document.createElement('script');
//   script.src = '//cdn.jsdelivr.net/npm/eruda';
//   document.body.appendChild(script);
//   script.onload = function () {
//     window.eruda.init();
//   };
// }

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

const dvaApp = dva.createApp({
  initialState: {},
  models: models
});
const store = dvaApp.getStore();
Glo.store = store;

class App extends Component {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  // eslint-disable-next-line react/sort-comp
  config: Config = {
    disableScroll: true,
    permission: {
      'scope.userLocation': {
        desc: '你的位置信息将用于小程序位置接口的效果展示' // 高速公路行驶持续后台定位
      }
    },
    pages: [
      'pages/home/index',
      'pages/cart/index',
      //'pages/home1/index',
      'pages/details/index',
      'pages/cate/index',
      'pages/use/index',
      //'pages/cart1/index',
      'pages/mine/index',
      //'pages/page/index',
      'pages/web/index',
      'pages/web/jump',
      //'pages/page1/index',
      'pages/page2/index',
      //'pages/page3/index',
      //'pages/page4/index',
      //'pages/page5/index',
      'pages/submit/index',
      'pages/login/index',
      'pages/payResult/result/index',
      'pages/payResult/cashier/index',
      //'pages/specialArea/index',
      'pages/bingMobile/index'
    ],
    //  分包路径
    subPackages: [
      // 专区
      {
        root: 'pages/specialArea/',
        pages: [
          // 'collage/index',
          // 'secondKill/index',
          // 'sameCity/index',
          // 'columns/index',
          'integral/index',
          'crossings/index'
        ]
      },
      // 订单
      {
        root: 'pages/packOrder/',
        pages: [
          'msg/index',
          'sendMsg/index',
          'canbonce/index',
          'search/index',
          'addAddress/index',
          'address/index',
          'config/index',
          'order/index',
          'orders/index',
          'location/index',
          'locationHome/index'
        ]
      },
      // 扫码购
      {
        root: 'pages/scan/',
        pages: ['scanCode/index', 'scanResult/index', 'scanCodeCart/index']
      },
      // 个人
      {
        root: 'pages/person/',
        pages: [
          'result/index',
          'payPassword/index',
          'setPassword/index',
          'password/index',
          'account/index',
          'setting/index',
          'evaluate/index',
          'customerService/index',
          'unsubscribe/index',
          'negotiationHistory/index',
          'applyService/index',
          'applyRefund/index',
          'selectRefundType/index',
          'refundDetail/index',
          'fillLogis/index', // mini需注释
          'checkLogistics/index' // mini需注释
        ]
      },
      //我的
      {
        root: 'pages/service/',
        pages: [
          'canUse/index', // mini需注释
          'browsingHistory/index',
          'likes/index',
          'shopConcern/index',
          'myCoupon/index',
          'goodsReviews/index',
          'integral/index',
          'Voucher/index',
          'VoucherOrder/index', // mini需注释
          'VoucherGet/index', // mini需注释
          'VoucherDetails/index'
        ]
      },
      //心愿单
      {
        root: 'pages/wishList/',
        pages: [
          'myWishList/index',
          'addWishList/index',
          'selectProduct/index',
          'myMember/index',
          'InviteFriends/index'
        ]
      },
      //电商
      {
        root: 'pages/store/',
        pages: [
          'clothingBusiness/index',
          'clothingSort/index',
          'productList/index',
          'storeInfo/index',
          'shoppingStore/index',
          'shoppingStore1/index',
          'storeHome/index',
          'liber/index',
          'liberBoth/index',
          'liberCate/index',
          'liberAd/index'
        ]
      },
      //儿童免费玩
      //{
      //root: 'pages/freeChildren/',
      //pages: ['freePlayHome/index', 'freeVoucher/index', 'freePlayList/index']
      //},
      //消息导航
      {
        root: 'pages/message/',
        pages: ['msgNav/index', 'management/index']
      },
      //拼团
      {
        root: 'pages/assemble/',
        pages: ['list/index', 'result/index']
      },
      //积分
      {
        root: 'pages/integral/',
        pages: [
          //'list/index',
          'newList/index'
          //'exchangeCouponDetail/index',
          //'exchangeVoucherDetail/index',
          //'rule/index',
          //'paySuccess/index',
          //'details/index'
        ]
      }
      // 鲁班可视化编辑项目
      //{
      //root: 'reviewPages',
      //pages: ['edit/index']
      //}
    ],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      color: '#666',
      selectedColor: '#ff2f7b',
      backgroundColor: '#fafafa',
      borderStyle: 'white',
      list: [
        {
          pagePath: 'pages/home/index',
          iconPath: './assets/tab-bar/home.png',
          selectedIconPath: './assets/tab-bar/home-active.png',
          text: '首页'
        },

        {
          pagePath: 'pages/cate/index',
          iconPath: './assets/tab-bar/cate.png',
          selectedIconPath: './assets/tab-bar/cate-active.png',
          text: '分类'
        },
        {
          pagePath: 'pages/cart/index',
          iconPath: './assets/tab-bar/cart.png',
          selectedIconPath: './assets/tab-bar/cart-active.png',
          text: '购物车'
        },
        {
          pagePath: 'pages/mine/index',
          iconPath: './assets/tab-bar/user.png',
          selectedIconPath: './assets/tab-bar/user-active.png',
          text: '个人'
        }
      ]
    }
  };

  componentWillMount() {
    console.log('生命周期回调—监听页面加载');
  }
  componentDidMount() {
    console.log('生命周期回调—监听页面初次渲染完成');
  }
  componentDidNotFound() {
    console.log('页面不存在监听函数');
  }
  componentWillUpdate() {
    console.log('页面即将更新');
  }
  componentDidUpdate() {
    console.log('页面更新完毕');
  }

  componentDidShow() {
    console.log('======componentDidShow=======');
    if (process.env.TARO_ENV === 'h5') {
      (function (history) {
        if (window.location.href.indexOf('isNeedMobile') > -1) {
          const isNeedMobile = window.location.href.split('isNeedMobile=')[1].split('#openId')[0];
          // @ts-ignore
          if (isNeedMobile == 1) {
            const grantCode = window.location.href.split('grantCode=')[1].split('#')[0];
            Taro.setStorage({ key: 'grantCode', data: grantCode });
            Taro.navigateTo({
              url: '/pages/bingMobile/index'
            });
          } else {
            if (window.location.href.indexOf('accessToken') > -1) {
              const accessToken = window.location.href.split('accessToken=')[1].split('#')[0];
              dvaApp.dispatch({
                type: 'cart/saveLogin',
                payload: {
                  data: {
                    accessToken: accessToken
                  }
                }
              });
              Taro.setStorage({
                key: 'isLogin',
                data: {
                  accessToken: accessToken
                }
              });
            }
          }
        }
        var pushState = history.pushState;
        history.pushState = function (state) {
          if (typeof history.onpushstate === 'function') {
            history.onpushstate({ state: state });
          }
          return pushState.apply(history, arguments);
        };
      })(window.history);
      // @ts-ignore

      window.history.onpushstate = () => {
        if (window.location.href.indexOf('crm_channel_id') > -1) {
          Taro.setStorage({
            key: 'channel_id',
            data: UrlSearch('crm_channel_id')
          });
        }
        Taro.setStorage({
          key: 'backRoute',
          data: {
            weChatUrl: window.location.href
          }
        });
      };
    }

    let session = Taro.getStorageSync('session');
    let openId = Taro.getStorageSync('openId');
    if (process.env.TARO_ENV === 'h5') {
      openId = true;
    }
    console.log(session, openId, 'openIdsession');
    if (openId && session) {
      //if (session) {
      dvaApp.dispatch({
        type: 'cart/saveSession',
        payload: {
          data: session
        }
      });
      let isLogin = Taro.getStorageSync('isLogin');
      console.log(isLogin, '====拿login====');
      if (isLogin) {
        dvaApp.dispatch({
          type: 'cart/saveLogin',
          payload: {
            data: isLogin
          }
        });

        //dvaApp.dispatch({
        //type: 'cart/getUnread',
        //payload: {}
        //});
      }
    } else if (false) {
      getSession({}).then((res) => {
        console.warn('全局session');
        console.warn(res);
        if (res.code === 0) {
          let session = res.data;
          Taro.setStorage({
            key: 'session',
            data: session
          });
          dvaApp.dispatch({
            type: 'cart/saveSession',
            payload: {
              data: session
            }
          });
        }
      });
    }
    Taro.getLocation({
      type: 'wgs84',
      success: function (res) {
        const latitude = res.latitude;
        const longitude = res.longitude;
        console.log('获取定位');
        console.log(latitude);
        console.log(longitude);
        dvaApp.dispatch({
          type: 'cart/saveXy',
          payload: {
            x: latitude,
            y: longitude
          }
        });
      }
    });
    setTimeout(async () => {
      console.log('获取定位地址');
      let x = dvaApp.getStore().getState().cart.x;
      let y = dvaApp.getStore().getState().cart.y;
      if (x) {
        let ads = await Glo.getXyAds(x, y);
        console.log(ads);
        dvaApp.dispatch({
          type: 'cart/saveAdsTitle',
          payload: {
            adsTitle: ads
          }
        });
      }
    }, 1000);
    // #ifdef weapp
    // Taro.checkSession({
    //   success() {
    //     console.log('权限未过期,直接进去首页');
    //     // return Taro.getStorage({ key: "session3rd" });
    //   },
    //   fail() {
    //     console.log('权限过期，先去登陆页面');
    //     Taro.navigateTo({ url: '/pages/login/index' });
    //   }
    // });
    // JSBridge.Common.Caller_Common_Base_getLoginStatus('', (data) => {
    //   if (data == '1' || data == 1) {
    //     console.log(data, '测试是否登陆已经登陆');
    //     // @ts-ignore
    //     JSBridge.Common.Caller_Common_Base_getOpenToken('', (responsed) => {
    //       console.log(responsed, 'responsed', 'app->res-responsed');
    //       if (responsed) {
    //         _fetchToken({
    //           openToken: responsed
    //         }).then((res) => {
    //           console.log(res, 'app->res');
    //           if (res.code === 0) {
    //             console.log(res, 'app->res->code==0');
    //             Taro.setStorage({
    //               key: 'isLogin',
    //               data: {
    //                 accessToken: res.data.accessToken
    //                 // uid: res.data.uid
    //               }
    //             });
    //             dvaApp.dispatch({
    //               type: 'cart/saveLogin',
    //               payload: {
    //                 data: { accessToken: res.data.accessToken }
    //               }
    //             });
    //           }
    //         });
    //       }
    //     });
    //   }
    // });
    console.log('测试是否登陆已经登陆--前1');
    JSBridge.Bridge.WebViewJavascriptBridge.setup(() => {
      // Taro.removeStorageSync('isLogin'); // 先删除，避免父token过期导致子token无效问题
      dvaApp.dispatch({
        type: 'user/judegInApp',
        falg: true
      });
      // console.log('测试是否登陆已经登陆--前');
      JSBridge.Common.Caller_Common_Base_getLoginStatus('', (data) => {
        // let status = Number(data);
        // eslint-disable-next-line no-restricted-globals
        if (data == '1' || data == 1) {
          console.log(typeof data);
          console.log(data, '测试是否登陆已经登陆1');
          // @ts-ignore
          JSBridge.Common.Caller_Common_Base_getOpenToken('', (responsed) => {
            console.log(responsed, 'responsed', 'app->res-responsed');
            if (responsed) {
              _fetchToken({
                openToken: responsed
              }).then((res) => {
                if (res.code == 0) {
                  Taro.setStorage({
                    key: 'isLogin',
                    data: {
                      accessToken: res.data.accessToken
                      // uid: res.data.uid
                    }
                  });
                  dvaApp.dispatch({
                    type: 'cart/saveLogin',
                    payload: {
                      data: { accessToken: res.data.accessToken }
                    }
                  });
                }
              });
            }
          });
        }
      });

      // @ts-ignore
      // JSBridge.Common.Caller_Common_Base_getOpenToken('', (responsed) => {
      //   console.log(responsed, 'responsed', 'app->res-responsed');
      //   if (responsed) {
      //     _fetchToken({
      //       openToken: responsed
      //     }).then((res) => {
      //       console.log(res, 'app->res');
      //       if (res.code === 0) {
      //         console.log(res, 'app->res->code==0');
      //         Taro.setStorage({
      //           key: 'isLogin',
      //           data: {
      //             accessToken: res.data.accessToken
      //             // uid: res.data.uid
      //           }
      //         });
      //         dvaApp.dispatch({
      //           type: 'cart/saveLogin',
      //           payload: {
      //             data: { accessToken: res.data.accessToken }
      //           }
      //         });
      //       }
      //     });
      //   }
      // });
    });

    const isWeiXin = () => {
      let ua = '';
      if (process.env.TARO_ENV === 'h5') {
        ua = navigator.userAgent.toLowerCase();
      }
      if (ua.indexOf('micromessenger') > -1) {
        console.log(' 是来自微信内置浏览器');
        return true;
      } else {
        // console.log('不是来自微信内置浏览器');
        return false;
      }
    };

    dvaApp.dispatch({
      type: 'user/judegIsWeiXin',
      falg: isWeiXin()
    });

    Taro.getSystemInfo({
      success: (res) => {
        // console.log('状态栏高度');
        // console.log(res.statusBarHeight);
        if (res.statusBarHeight) {
          dvaApp.dispatch({
            type: 'cart/saveStatusHeight',
            height: res.statusBarHeight
          });
        }
      }
    });
  }

  componentDidHide() {
    console.log('======componentDidHide======');
  }

  componentDidCatchError() {
    console.error('componentDidCatchError');
  }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    );
  }
}

Taro.render(<App />, document.getElementById('app'));
