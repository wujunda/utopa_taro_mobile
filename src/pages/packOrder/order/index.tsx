/* eslint-disable import/first */
import Taro, { useState, useRouter } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,//useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View, Text } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import Box from '../../../components/Box';
import Bton from '../../../components/Bton';
import Wrap from '../../../components/Wrap';
import Img from '../../../components/Img';
import Cnt from '../../../components/Cnt';
import Txt from '../../../components/Txt';
import SubGoods from '../../../components/SubGoods';
import Bar from '../../../components/bar';
import Yuan from '../../../components/Yuan';
import pay from '../../../assets/order/pay.png';
import waitPng from '../../../assets/order/wait.png';
import getPng from '../../../assets/order/get.png';
import Loading from '../../../components/Loading';
import donePng from '../../../assets/order/done.png';
import store from '../../../assets/submit/store.png';
import location from '../../../assets/positionAddress.png';
import status8 from '../../../assets/order/status8.png';
import close from '../../../assets/order/close.png';
import './index.scss';
import useRequest from '../../../hooks/useRequest';
import useSession from '../../../hooks/useSession';
import Setimeout from '../../../components/Setimeout';
import { productOrderDetail, cancleOrderDetail } from '../../../services/orders';
import Router from '../../../utils/router';
import { PAY_WAY, ORDER_DETAIL_STATUS, ORDER_DETAIL_DISTRIBUTETYPE } from '@/utils/common_enum';
import { Glo, goPayPage, successUrl } from '../../../utils/utils';

interface Props {
  isLogin: any;
  closeWidth: number;
  statusHeight: number;
}
type IProps = PageStateProps & Props;
const Index: Taro.FC<IProps> = (props) => {
  const [demo, setDemo] = useState(0);
  const Status = ORDER_DETAIL_STATUS;
  //支付方式
  const PayWay = PAY_WAY;
  const changeTime = (t) => {
    if (t && t !== null) {
      let time = new Date(t);
      let y = time.getFullYear();
      let m = time.getMonth() + 1 > 10 ? time.getMonth() + 1 : '0' + (time.getMonth() + 1);
      let d = time.getDate() > 10 ? time.getDate() : '0' + time.getDate();
      let h = time.getHours() > 10 ? time.getHours() : '0' + time.getHours();
      let min = time.getMinutes() > 10 ? time.getMinutes() : '0' + time.getMinutes();
      let sec = time.getSeconds() > 10 ? time.getSeconds() : '0' + time.getSeconds();
      return `${y}-${m}-${d} ${h}:${min}:${sec}`;
    }
    return '';
  };
  const router = useRouter();
  const refundBtn = {
    display: 'inline-block',
    fontSize: '14px',
    textAlign: 'center',
    lineHeight: '30px',
    width: '70px',
    height: '30px',
    border: '1px solid #eee',
    borderRadius: '5px'
  };
  const cancel = async () => {
    let payload = {
      orderId: router.params.orderId,
      clientType: 1
    };
    let res = await cancleOrderDetail(payload);
    if (res.code === 0) {
      //取消成功
      Taro.navigateTo({
        url: '/pages/packOrder/orders/index?status=110'
      });
    }
  };
  const statusStr = {
    0: '待商家审核',
    1: '拒绝退款,待买家处理 ',
    2: '待买家寄回',
    3: '待商家收货',
    4: '拒绝收货,待买家处理',
    5: '退款中',
    6: '退款成功',
    7: '关闭',
    8: '退款'
  };
  // 请求数据
  const params = {
    // accessToken: Taro.getStorageSync('login'),
    orderId: router.params.orderId,
    orderNo: router.params.orderNo,
    serviceStatus: router.params.serviceStatus
  };
  const [haveSession] = useSession();
  const [state, update, loading] = useRequest(params, productOrderDetail);
  if (haveSession && state === null && update && !loading) {
    update();
  }
  //待支付  待发货  待收货 已完成 已关闭 售后 退款中  已退款 待接单 待成团 待付尾款
  let statusList = [
    pay,
    waitPng,
    getPng,
    donePng,
    close,
    waitPng,
    waitPng,
    waitPng,
    status8,
    waitPng,
    waitPng,
    waitPng,
    waitPng
  ];
  return (
    <View className="utp-order" style={{ backgroundColor: '#f5f5f5' }}>
      <View className="utp-order-hd" style={{ paddingTop: props.statusHeight + 'px' }}>
        <Bar white goBack title="订单详情" bg className="utp-order-bd-bar" />
      </View>
      {loading && !state && <Loading />}
      {!!state && state.code === 0 && !!state.data && (
        <View
          className="utp-order-bd"
          style={{
            marginBottom: state && state.data.serviceStatus === 4 ? 0 : Taro.pxTransform(120)
          }}
        >
          <View className="utp-order-bd-top">
            <View className="utp-order-bd-top-status">
              <Wrap type={1} justifyContent="space-between">
                <Wrap>
                  <Img src={statusList[(state && state.data.serviceStatus) || 0]} width={100} />
                  <View className="utp-order-bd-top-status-tits">
                    <Txt
                      title={Status[(state && state.data.serviceStatus) || 0]}
                      size={36}
                      height={47}
                      color="white"
                      justifyContent="flex-start"
                    />
                    {!!state && state.data.serviceStatus === 0 && (
                      <View>
                        {state.data.validDuration && (
                          <Setimeout seconds={parseInt(state.data.validDuration / 1000)} type={3} />
                        )}
                      </View>
                    )}

                    {!!state && !!state.data.orderStatusDetail && (
                      <Txt
                        title={state.data.orderStatusDetail}
                        size={24}
                        height={38}
                        color="white"
                      />
                    )}
                  </View>
                </Wrap>
                <View
                  className="utp-order-bd-price"
                  style={{ display: state && state.data.serviceStatus === 0 ? 'block' : 'none' }}
                >
                  <Yuan price={state && state.data.orderTotalMoney} color="#fff" size={40}></Yuan>
                </View>
              </Wrap>
            </View>
          </View>

          <Box type={1}>
            {/*门店自提 */}
            <View
              className="utp-order-bd-infomation"
              style={{
                display: state && [1].includes(state.data.serviceStatus) ? 'block' : 'none'
              }}
            >
              {state &&
                state.data.serviceStatus === 1 &&
                state.data.storeProductList &&
                state.data.storeProductList[0] && (
                  <View>
                    <Wrap Myheight={56} top type={2}>
                      <Txt title="提货时间" size={28} />
                      <View className="utp-order-bd-infomation-value">
                        <Wrap type={2}>
                          <Txt title="2019-05-29 15:02:31" size={28} color="deep" />
                        </Wrap>
                      </View>
                    </Wrap>
                    <Wrap top type={2}>
                      <Txt title="提货地址" size={28} />
                      <View className="utp-order-bd-infomation-value utp-order-bd-infomation-value2">
                        <Wrap>
                          <Txt
                            title={state.data.storeProductList[0].receiveAddress}
                            size={28}
                            color="deep"
                            height={36}
                          />
                        </Wrap>
                      </View>
                    </Wrap>
                    <Wrap Myheight={56} top type={2}>
                      <Txt title="联系人" size={28} />
                      <View className="utp-order-bd-infomation-value utp-order-bd-infomation-value3">
                        <Wrap>
                          <Txt
                            title={
                              state.data.storeProductList[0].contactor +
                              '  ' +
                              state.data.storeProductList[0].contactPhone
                            }
                            size={28}
                            color="deep"
                          />
                        </Wrap>
                      </View>
                    </Wrap>
                  </View>
                )}
              {/* 5.6 代付款新增配送方式 */}
              {/* {state && state.data.serviceStatus === 0 && (
                <View>
                  <Wrap Myheight={56} top type={2}>
                    <Txt title="配送方式" size={28} />
                    <View className="utp-order-bd-infomation-value">
                      <Wrap type={2}>
                        <Txt title={} size={28} color="deep" />
                      </Wrap>
                    </View>
                  </Wrap>
                  <Wrap Myheight={56} top type={2}>
                    <Txt title="收货地址" size={28} />
                    <View className="utp-order-bd-infomation-value">
                      <Wrap type={2}>
                        <Txt title="广东 广州市天河区华夏路10号富力中" size={28} color="deep" />
                      </Wrap>
                    </View>
                  </Wrap>
                  <Wrap Myheight={56} top type={2}>
                    <Txt title="到店时间" size={28} />
                    <View className="utp-order-bd-infomation-value">
                      <Wrap type={2}>
                        <Txt title="2019-05-29 15:02:31" size={28} color="deep" />
                      </Wrap>
                    </View>
                  </Wrap>
                  <Wrap Myheight={56} top type={2}>
                    <Txt title="购买手机号" size={28} />
                    <View className="utp-order-bd-infomation-value">
                      <Wrap type={2}>
                        <Txt title="2019-05-29 15:02:31" size={28} color="deep" />
                      </Wrap>
                    </View>
                  </Wrap>
                </View>
              )} */}
            </View>

            {/* 正常  */}
            {/* {state && state.data.serviceStatus === 4 && (
              <View className="utp-order-bd-address">
                <Wrap type={2} top>
                  <Wrap>
                    <View className="utp-order-bd-address-icos">
                      <Cnt>
                        <Img src={location} width={48} />
                      </Cnt>
                    </View>

                    {!!state && !!state.data.userAddress && (
                      <View className="utp-order-bd-address-right">
                        <View>
                          <Wrap>
                            {!!state.data.userAddress.userName ? (
                              <Txt
                                title={state.data.userAddress.userName}
                                size={28}
                                color="deep"
                                height={48}
                                bold
                              />
                            ) : null}

                            <View style={{ width: Taro.pxTransform(20) }}></View>
                            {!!state.data.userAddress.phone ? (
                              <Txt
                                title={state.data.userAddress.phone}
                                size={28}
                                color="deep"
                                height={48}
                                bold
                              />
                            ) : null}
                          </Wrap>
                        </View>
                        <Wrap type={2} justifyContent="flex-end">
                          {!!state.data.userAddress.address && (
                            <Txt
                              title={`收货地址: ${state.data.userAddress.address}`}
                              size={28}
                              color="deep"
                              height={44}
                              dian
                            />
                          )}
                        </Wrap>
                      </View>
                    )}
                  </Wrap>
                </Wrap>
              </View>
            )} */}
          </Box>

          {/* 已支付 */}
          <Box type={1}>
            {state &&
              state.data.storeProductList &&
              state.data.storeProductList.map((i, ii) => {
                return (
                  <View className="utp-order-bd-box utp-order-bd-box-1" key={ii}>
                    <View className="utp-order-bd-store">
                      <Wrap type={1}>
                        <Wrap>
                          <Img src={store} width={48} />
                          <View className="utp-order-bd-store-title">
                            <Txt title={i.storeName} size={28} color="deep" />
                          </View>
                        </Wrap>
                      </Wrap>
                    </View>
                    {i &&
                      i.products &&
                      i.products.map((o) => {
                        return (
                          <View style={{ marginBottom: Taro.pxTransform(30) }} key={o.id}>
                            <SubGoods data={o} type={1} />
                            <View
                              style={{
                                textAlign: 'right',
                                display:
                                  state &&
                                  [1, 2, 3, 5, 6, 7, 8, 11, 12].includes(state.data.serviceStatus)
                                    ? 'block'
                                    : 'none'
                              }}
                              onClick={() => {
                                Taro.setStorage({
                                  key: 'product',
                                  data: {
                                    ...i,
                                    orderId: router.params.orderId
                                  }
                                });
                                if ([2, 3].includes(state.data.serviceStatus)) {
                                  //已发货和已完成选择退款类型
                                  Router.applyRefund();
                                } else if ([1, 8].includes(state.data.serviceStatus)) {
                                  //待发货和待接单 仅退款
                                  Router.selectRefundType();
                                } else if ([0, 1, 2, 3, 4, 5, 6, 7].includes(o.refundStatus)) {
                                  // 退款详情 参数applyRefund
                                  Router.refundDetail(o.refundOrderNo);
                                }
                              }}
                            >
                              <Text className="utp-order-bd-refund">
                                {statusStr[o.refundStatus] || '退款'}
                              </Text>
                            </View>
                          </View>
                        );
                      })}

                    {/* <View className="utp-order-bd-gang utp-solid"></View> */}
                    {/* <Wrap Myheight={88} justifyContent="space-between">
                      <Txt title="配送方式" size={28} color="deep" />
                      <Wrap>
                        <Txt title={i.expressDetail} size={28} />
                      </Wrap>
                    </Wrap> */}
                    {state && state.data.distributionType && (
                      <Wrap Myheight={88} justifyContent="space-between">
                        <Txt title="配送方式" size={28} color="deep" />
                        <Wrap>
                          <Txt title={ORDER_DETAIL_DISTRIBUTETYPE[i.distributionType]} size={28} />
                        </Wrap>
                      </Wrap>
                    )}
                    {state && state.data.serviceStatus !== 1 && (
                      <Wrap Myheight={88} justifyContent="space-between">
                        <Txt title="收货地址" size={28} color="deep" />
                        <View className="utp-order-bd-addres">
                          <Txt title={i.receiveAddress} size={28} dian justifyContent="flex-end" />
                        </View>
                      </Wrap>
                    )}
                    {i.shippingTime && i.expressDetail !== '到店享用' && (
                      <Wrap Myheight={88} justifyContent="space-between">
                        <Txt title="配送时间" size={28} color="deep" />
                        <Wrap>
                          <Txt title={i.shippingTime} size={28} />
                        </Wrap>
                      </Wrap>
                    )}
                    {!!i && !!i.lackHandleMessage && (
                      <Wrap Myheight={88} justifyContent="space-between">
                        <Txt title="如遇缺货" size={28} color="deep" />
                        <Wrap>
                          <Txt title={i.lackHandleMessage} size={28} />
                        </Wrap>
                      </Wrap>
                    )}
                    {i && i.buyerMemo && (
                      <Wrap type={2} Myheight={88}>
                        <Txt title="订单备注" size={28} color="deep" />
                        <View className="utp-order-bd-infomation-value">
                          <Wrap justifyContent="flex-end">
                            <Txt title={i.buyerMemo} size={28} dian />
                          </Wrap>
                        </View>
                      </Wrap>
                    )}
                  </View>
                );
              })}
          </Box>
          <View style={{ marginTop: Taro.pxTransform(-10) }}>
            <Box type={1}>
              <View className="utp-order-bd-box">
                <Wrap Myheight={88} justifyContent="space-between">
                  <Txt title="订单编号" size={28} color="deep" />
                  <Wrap>
                    <Txt title={state && state.data.orderNo} size={28} />
                    <View
                      style={{ marginLeft: Taro.pxTransform(5) }}
                      onClick={() => {
                        Taro.setClipboardData({
                          data: state && state.data.orderNo,
                          success: () => {
                            Taro.showToast({
                              title: '复制成功',
                              duration: 3000
                            });
                          }
                        });
                      }}
                    >
                      <Txt title="复制" size={24} color="active" />
                    </View>
                  </Wrap>
                </Wrap>
                <Wrap Myheight={88} justifyContent="space-between">
                  <Txt title="下单时间" size={28} color="deep" />
                  <Txt title={changeTime((state && state.data.createTime) || 0)} size={28} />
                </Wrap>
              </View>
            </Box>
          </View>
          <View style={{ marginTop: Taro.pxTransform(-10) }}>
            <Box type={1}>
              <View className="utp-order-bd-box">
                <View style={{ display: state && state.data.businessName ? 'block' : 'none' }}>
                  <Wrap Myheight={88} justifyContent="space-between">
                    <Txt title="商家" size={28} color="deep" />
                    <Txt title={state && state.data.businessName} size={28} />
                  </Wrap>
                </View>
                <Wrap Myheight={88} justifyContent="space-between">
                  <Txt title="销售方式" size={28} color="deep" />
                  <Txt title={state && state.data.saleType === 0 ? '现货' : '预售'} size={28} />
                </Wrap>
              </View>
            </Box>
          </View>
          <View style={{ marginTop: Taro.pxTransform(-10) }}>
            <Box type={1}>
              <View className="utp-order-bd-box">
                <Wrap Myheight={88} justifyContent="space-between">
                  <Txt title="类型" size={28} color="deep" />
                  <Txt
                    title={
                      state && state.data.exchangeType === 2
                        ? '优惠券'
                        : state && state.data.exchangeType === 3
                        ? '抵用券'
                        : '普通商品'
                    }
                    size={28}
                  />
                </Wrap>
              </View>
            </Box>
          </View>
          <View style={{ marginTop: Taro.pxTransform(-10) }}>
            <Box type={1}>
              <View className="utp-order-bd-box">
                <Wrap Myheight={88} justifyContent="space-between">
                  <Txt title="支付方式" size={28} color="deep" />
                  <Txt title={PayWay[state && state.data.payWay] || '在线支付'} size={28} />
                </Wrap>
              </View>
            </Box>
          </View>

          <View
            style={{
              marginTop: Taro.pxTransform(-10),
              marginBottom :Taro.pxTransform(60)
            }}
          >
            <Box type={1}>
              <View className="utp-order-bd-box">
                <Wrap justifyContent="space-between">
                  <Txt title="商品总额" size={28} color="deep" height={88} />
                  <View className="utp-order-bd-price1">
                    <Yuan price={state && state.data.productPrice} size={28}></Yuan>
                  </View>
                </Wrap>
                <Wrap justifyContent="space-between">
                  <Txt title="兑换积分" size={28} color="deep" height={88} />
                  <Wrap>
                    <Txt title="-" size={28} height={88} />
                    <View className="utp-order-bd-price1">
                      <Yuan price={state && state.data.usedPoint} size={28}></Yuan>
                    </View>
                  </Wrap>
                </Wrap>

                {/* <Wrap justifyContent="space-between">
                <Txt title="优惠活动1" size={28} color="deep" height={88} />
                <Txt title="2012-12-12 12:00:00" size={28} height={88} />
              </Wrap>
              <Wrap justifyContent="space-between">
                <Txt title="优惠活动2" size={28} color="deep" height={88} />
                <Txt title="2012-12-12 12:00:00" size={28} height={88} />
              </Wrap>
              <Wrap justifyContent="space-between">
                <Txt title="优惠券" size={28} color="deep" height={88} />
                <Txt title="2012-12-12 12:00:00" size={28} height={88} />
              </Wrap> */}
                <Wrap justifyContent="space-between">
                  <Txt title="运费" size={28} color="deep" height={88} />
                  <View className="utp-order-bd-price1">
                    <Yuan price={state && state.data.expressPrice} size={28}></Yuan>
                  </View>
                </Wrap>
                <View className="utp-order-bd-gang utp-solid"></View>
                <Wrap type={5} Myheight={100}>
                  <Wrap>
                    <Txt
                      title={
                        state && [1, 2, 3].includes(state.data.serviceStatus)
                          ? '实际支付：'
                          : '待支付 ：'
                      }
                      size={34}
                      color="black"
                      height={100}
                      bold
                    />
                    <Txt
                      title={state && state.data.usedPoint ? '160积分 +' : ''}
                      size={34}
                      color="active"
                      height={100}
                      bold
                    />
                    <View className="utp-order-bd-price2">
                      <Yuan
                        price={state && state.data.orderTotalMoney}
                        size={34}
                        color="#FF2F7B"
                      ></Yuan>
                    </View>
                  </Wrap>
                </Wrap>
              </View>
            </Box>
          </View>
        </View>
      )}
      {state && state.data.serviceStatus !== 4 && <View className="utp-order-bd-cl"></View>}
      {/* （-1:无效订单，0:待付款，1:待发货， 2:已发货，3:已完成, 4:已关闭，5:售后 6:退款中 7:已退款 8: 待接单 11:待成团 12:待付尾款 */}

      {/* 8: 1  */}
      {/* 关闭 无按钮  待支付   */}
      {state && state.data.serviceStatus !== 4 && (
        <View
          className="utp-order-ft"
          onClick={() => {
            setDemo(demo + 1);
          }}
        >
          <View className="utp-order-btns">
            <Wrap type={5}>
              {state &&
                [1, 2, 8].includes(state.data.serviceStatus) &&
                ['核销订单', '取消订单'].map((i, idn) => {
                  return (
                    <View
                      className="utp-order-btns-btn"
                      key={idn}
                      onClick={() => {
                        // 取消订单
                        if (i === '取消订单') {
                          Taro.showModal({
                            title: '提示',
                            content: '是否取消订单？',
                            success: function (res) {
                              if (res.confirm) {
                                cancel();
                              }
                            }
                          });
                        }
                        if (i === '核销订单') {
                          Router.goConfig(state.data.id);
                        }
                      }}
                    >
                      <Bton size="mini" type={1}>
                        {i}
                      </Bton>
                    </View>
                  );
                })}
              {state &&
                [3].includes(state.data.serviceStatus) &&
                ['核销订单', '申请售后'].map((i, idn) => {
                  return (
                    <View
                      className="utp-order-btns-btn"
                      key={idn}
                      onClick={() => {
                        if (i === '核销订单') {
                          Router.goConfig(state.data.id);
                        }
                        if (i === '申请售后') {
                          Router.goAfterSale();
                        }
                      }}
                    >
                      <Bton size="mini" type={1}>
                        {i}
                      </Bton>
                    </View>
                  );
                })}
              {state &&
                state.data.serviceStatus === 0 &&
                ['取消订单', '去支付'].map((i, idn) => {
                  return (
                    <View
                      className="utp-order-btns-btn"
                      key={idn}
                      onClick={() => {
                        if (i === '取消订单') {
                          Taro.showModal({
                            title: '提示',
                            content: '是否取消订单？',
                            success: function (res) {
                              if (res.confirm) {
                                cancel();
                              }
                            }
                          });
                        }
                        if (i === '去支付') {
                          if (process.env.TARO_ENV === 'weapp') {
                            Router.goPay(
                              state.data.id,
                              state.data.orderTotalMoney / 100,
                              state.data.orderNo,
                              state.data.payOrderNo
                            );
                          } else {
                            let channelId = 1001;
                            if (props.inApp) {
                              channelId = 1;
                            } else if (props.isWeiXinFalg) {
                              channelId = 1002;
                            }
                            goPayPage({
                              sysId: channelId,
                              payOrderNo: state.data.payOrderNo,
                              paySucceesHref: successUrl(),
                              payFailHref: successUrl()
                            });
                          }
                        }
                      }}
                    >
                      <Bton size="mini" type={i === '去支付' ? 'defalut' : 1}>
                        {i}
                      </Bton>
                    </View>
                  );
                })}
            </Wrap>
            {/* <View style={{ marginTop: Taro.pxTransform(-12) }}></View> */}
            {/* <Wrap type={5}> */}
            {/* <Wrap>
                <View className="utp-order-btns-btn">
                  <Bton size="mini" type={1}>
                    联系客服
                  </Bton>
                </View>
                <View style={{ width: Taro.pxTransform(20) }}></View>
                <View className="utp-order-btns-btn">
                  <Bton size="mini" type={1}>
                    取消订单
                  </Bton>
                </View>
                <View style={{ width: Taro.pxTransform(20) }}></View>
                <View className="utp-order-btns-btn">
                  <Bton size="mini">去支付</Bton>
                </View>
              </Wrap>
            </Wrap> */}
          </View>
        </View>
      )}
    </View>
  );
};

Index.config = {
  navigationBarTitleText: '订单详情',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5',
  navigationStyle: 'custom'
};

type PageStateProps = {
  loading: boolean;
  submit: any;
  inApp: boolean;
  isWeiXinFalg: boolean;
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight,
    inApp: state.user.inApp,
    isWeiXinFalg: state.user.isWeiXinFalg
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
