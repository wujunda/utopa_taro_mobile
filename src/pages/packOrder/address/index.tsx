import Taro, { useState, useEffect, useRouter, useDidShow } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
//import useStore from '../../../components/useNum';
import { connect } from '@tarojs/redux';

import Address from '../../../components/Address';
import Loading from '../../../components/Loading';
//import Bar from '../../../components/bar';
import Box1 from '../../../components/Box1';
import Bton from '../../../components/Bton';
import './index.scss';
import useRequest from '../../../hooks/useRequest';
import useSession from '../../../hooks/useSession';
import useLogin from '../../../hooks/useLogin';
import useResetCount from '../../../hooks/useResetCount';
import { IAddress, IAddressData, getAddress } from '../../../services/address';
import { IPost, IPosts } from '../../../services/submit';
import Router from '../../../utils/router';

type PageStateProps = {
  post: IPost;
  posts: IPosts[];
};

type PageDispatchProps = {
  savePostss: (any) => void;
  savePosts: (any) => void;
  getSubmit: (any) => void;
  savePost: (any) => void;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
const Index: Taro.FC<IProps> = (props) => {
  const router = useRouter();
  // console.log(router.params.id);
  let params = {
    id: router.params.id
  };
  // console.log('路由参数');
  // console.log(params);

  const [haveSession] = useSession();
  const [isLogin, updateLogin] = useLogin();
  if (haveSession && !isLogin) {
    if (updateLogin) {
      updateLogin();
    }
  }
  console.warn('是否登陆');
  console.warn(isLogin);
  // 购物车数据
  const [state, update, loading] = useRequest<IAddress>('name=a&age=1', getAddress);
  //const [state1, setState1] = useState<any>(state);
  const [can, update1] = useResetCount();
  if (
    haveSession &&
    isLogin &&
    (state === null || (state != null && state.code !== 0)) &&
    update &&
    !loading
  ) {
    if (can) {
      update();
      update1();
    }
  }

  //let num = useStore({ name: '' })[0];
  //if (
  //(!state1 && state) ||
  //(state1 && state1.data && state1.data.address === undefined) ||
  //(state1 && state1.data && state1.data.address && state1.data.address.length === 0)
  //) {
  //setState1(state);
  //}
  const [demo, setDemo] = useState(0);
  useEffect(() => {
    // console.log('demob变了');
  }, [demo]);
  let address!: IAddressData[];
  if (state && state.data && state.data.address) {
    address = state.data.address;
  }

  // 提交订单
  const onSubmit = (address: any) => {
    //obj.receiptAddress = {};
    console.warn('提交订单dz');
    console.warn(props.post);
    props.post.binComits.map((item) => {
      if (item.storeId.toString() === params.id.toString()) {
        item.receiptAddress = address;
      }
    });
    props.getSubmit({ post: props.post });
    props.savePost({
      data: props.post
    });
  };
  useDidShow(() => {
    //var str = Taro.getStorageSync('address');
    let newMsg = Taro.getStorageSync('addressLoading');
    //if (str) {
    //setState1(str);
    //}
    if (newMsg && update) {
      update();

      Taro.setStorage({
        key: 'addressLoading',
        data: ''
      });
    }
  });

  return (
    <View className="utp-page-address">
      {loading && <Loading />}
      <View className="utp-page-address-bd">
        <Box1 type={1}>
          {false && (
            <View>
              <View
                onClick={() => {
                  Taro.navigateTo({
                    url: '/pages/packOrder/addAddress/index'
                  });
                }}
              >
                <Address />
              </View>
              <Address />
              <Address />
            </View>
          )}
          {!!address &&
            address.map((item) => {
              return (
                <View
                  key={item.id}
                  onClick={() => {
                    if (params.id && params.id !== 'undefined') {
                      let obj: any = {};
                      props.posts.map((item) => {
                        if (!item) {
                          return;
                        }
                        console.log('数据数据');
                        console.log(item);
                        console.log(params.id);
                        if (item.storeId.toString() === params.id) {
                          obj = item;
                        }
                      });
                      let ads: any = Object.assign({}, item);
                      obj.storeId = Number(params.id);
                      obj.address = ads;
                      onSubmit(item);
                      props.savePostss({
                        data: obj
                      });
                      Router.goBack();

                      setTimeout(() => {
                        props.savePosts({
                          data: obj
                        });
                      }, 0);
                    }
                    //if (params.id && params.id !== 'undefined') {
                    //let obj: any = {};
                    //console.log('提交支付');
                    //console.log(props.posts);
                    //props.posts.map((item) => {
                    //if (Number(item.storeId) === Number(params.id)) {
                    //obj = item;
                    //}
                    //});
                    //let ads: any = Object.assign({}, item);
                    //obj.storeId = Number(params.id);
                    //obj.address = ads;

                    ////onSubmit(item);
                    ////props.savePostss({
                    ////data: obj
                    ////});

                    //Router.goBack();
                    //onSubmit(item);

                    //setTimeout(() => {
                    //props.savePosts({
                    //data: obj
                    //});
                    //}, 0);
                    //}
                  }}
                >
                  <Address
                    perfect
                    item={item}
                    onEdit={() => {
                      Taro.setStorage({
                        key: 'addressEdit',
                        data: item
                      });
                      Taro.setStorage({
                        key: 'LoctionAds',
                        data: ''
                      });

                      Router.goAddAddress();
                    }}
                  />
                </View>
              );
            })}
        </Box1>
      </View>
      <View
        className="utp-page-address-ft"
        onClick={() => {
          Taro.setStorage({
            key: 'LoctionObj',
            data: ''
          });

          Taro.setStorage({
            key: 'LoctionStr',
            data: ''
          });

          Taro.setStorage({
            key: 'LoctionAds',
            data: ''
          });

          Taro.setStorage({
            key: 'addressEdit',
            data: ''
          });
          Router.goAddAddress();
          setDemo(demo + 1);
        }}
      >
        <Bton>新增收货地址</Bton>
      </View>
    </View>
  );
};

Index.config = {
  navigationBarTitleText: '地址管理',
  disableScroll: false,
  enablePullDownRefresh: false,
  backgroundColor: '#000000'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    post: state.cart.post,
    posts: state.cart.posts
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    savePosts: (payload) => {
      dispatch({
        type: 'cart/savePosts',
        payload
      });
    },
    savePostss: (payload) => {
      dispatch({
        type: 'cart/savePostss',
        payload
      });
    },
    getSubmit: (payload) => {
      dispatch({
        type: 'cart/getSubmit',
        payload
      });
    },
    savePost: (payload) => {
      dispatch({
        type: 'cart/savePost',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
