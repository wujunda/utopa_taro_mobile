import { View, ScrollView, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';

import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Bar from '../../../components/bar';
import Bton from '../../../components/Bton';

import './index.scss';
interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <Bar title="账户注销"></Bar>
          <ScrollView
            refresherEnabled={true}
            refresherBackground={'#333'}
            className="utp-bd utp-bd-page utp-unsubscribe"
            scrollY
          >
            <View className="utp-unsubscribe-btn">
              <Text className="utp-unsubscribe-btnMiddle"></Text>
              <Text className="utp-unsubscribe-active"></Text>
            </View>
            <Text className="utp-unsubscribe-title">注销后，您将放弃以下权益：</Text>
            <View className="utp-unsubscribe-contentBox">
              <Text className="utp-unsubscribe-content">
                1.
                您的所有交易记录将被清空，请确保所有交易已完结且无纠纷，账户注销后因历史交易可能产生的退换货、维权相关的资金退回等权益将视作自动放弃。
                <br />
                2.
                您的优托邦账户的个人资料和历史信息（包括但不限于用户名、头像、购物记录、关注信息等）都将被清空且无法恢复找回。
                <br />
                3. 支持优托邦平台账户使用的多项产品/服务将无法使用此账户（如智能门禁、停车缴费等）。
                <br />
                您理解并同意，优托邦无法协助您重新恢复前述服务。
              </Text>
            </View>

            <Text className="utp-unsubscribe-title">注销账号需要满足以下条件：</Text>
            <View className="utp-unsubscribe-contentBox">
              <Text className="utp-unsubscribe-content">
                1.账号当前为有效状态 <br />
                2.账号内无未完成状态订单和服务 <br />
                3.账户无任何纠纷，包括投诉举报或被投诉举报
              </Text>
            </View>
            <View className="utp-unsubscribe-tipBox">
              <Wrap type={3}>
                <Text className="utp-unsubscribe-tips">
                  点击下方“同意注销”按钮，即表示您已阅读并同意
                </Text>
                <View style={{ width: 300 }}>
                  <Bton>同意注销</Bton>
                </View>
                <Text className="utp-unsubscribe-remind">阅读《账户注销重要提醒》</Text>
              </Wrap>
            </View>
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};
export default Index;
