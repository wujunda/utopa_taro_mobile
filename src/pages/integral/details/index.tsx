
import Taro, { useState } from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import DetailsCard from '@/components/integral/details'
import { userIntegralDetails } from '@/services/canbonce'
import useRequestWIthMore from '../../../hooks/useRequestWIthMore'
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import './index.scss';

interface IProps {}

const TAB = [
  {
    name: '积分发放',
    type: 1
  },
  {
    name: '积分消耗',
    type: 2
  }
]
function Details() {
  const [currentIndex, setCurrentIndex] = useState(1)
  const [state, hasMore, run, getMoreData, loading] = useRequestWIthMore(
    {},
    userIntegralDetails
  );
  

  var goods = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.result.records.map((items) => {
        goods.push(items);
      });
    });
  }
 
  const chooseTab = (type: number) => {
    setCurrentIndex(type)
  }
  return (
    <View className="utp-integral-details">
      <View className="utp-integral-details-tab">
        {
          TAB.map(item => {
            return (
              <View key={item.type} className="utp-integral-details-tab-box" onClick={() => chooseTab(item.type)}>
                <View className="utp-integral-details-tab-box-name" style={{color: item.type == currentIndex ? 'rgba(51,51,51,1)' : 'rgba(153,153,153,1)'}}>{item.name}</View>
                {
                  currentIndex == item.type &&
                  <View className="utp-integral-details-tab-box-line"></View>
                }
              </View> 
            ) 
          })
        }
      </View>
      {
        currentIndex == 1 ?
        <ScrollView>
          {loading && goods.length === 0 && <Loading />}
          {!loading && goods.length === 0 && <LoadingNone />}
          <View className="utp-integral-details-body">
            <View className="utp-integral-details-body-card">
              {
                goods.map(item => {
                  return (
                    <DetailsCard data={item} key={item.id}/>
                  )
                })
              }
            </View>
          </View>
          {loading && goods.length !== 0 && <LoadingMore />}
          {!loading && !hasMore && <LoadingNoneMore />}
        </ScrollView> : 
        <ScrollView>
          {loading && goods.length === 0 && <Loading />}
          {!loading && goods.length === 0 && <LoadingNone />}
          <View className="utp-integral-details-body">
            <View className="utp-integral-details-body-card" style={{display:'flex', justifyContent: 'center', color: '#cccccc', fontSize: '14px'}}>
              暂无数据~~
            </View>
          </View>
          {loading && goods.length !== 0 && <LoadingMore />}
          {!loading && !hasMore && <LoadingNoneMore />}
        </ScrollView>
      }
      
    </View>
  );
}

Details.config = {
  navigationBarTitleText: '积分明细',
  disableScroll: false,
  enablePullDownRefresh: true,
  onReachBottomDistance: 10,
  backgroundColor: '#f5f5f5'
};
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    }
  };
};
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    setToken: (params) => {
      dispatch({
        type: 'cart/saveLogin',
        payload: {
          data: params
        }
      })
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Details);