import request from '../utils/request';
import Taro from '@tarojs/taro';
import { ProductStoreInfo, ProductBaiscData } from '../interface/product';

export interface ISearchFindData {
  keyWords: string;
  used: number;
  url: string;
}
export interface ISearchFind {
  code: number;
  data: {
    total: number;
    records: ISearchFindData[];
  };
}

export interface ISearchHistoryData {
  content: string;
}
export interface ISearchHistory {
  code: number;
  data: {
    records: ISearchHistoryData[];
  };
}
export interface ISearch {
  code: number;
  data: {
    storeResult: {
      records: ProductStoreInfo[];
    };
    records: ProductBaiscData[];
    productVersion2Result: {
      records: ProductBaiscData[];
    };
  };
}

export const getSearch = (params: any, pst: any) => {
  // console.log(params);
  var obj = {
    accessToken: Taro.getStorageSync('login'),
    uid: Taro.getStorageSync('uid'),
    pageSize: pst.per_page,
    pageNo: pst.page
  };
  let url = 'neweshop.search.globalSearch';
  if (pst.categoryId) {
    url = 'eshop.product.getProductList';
    //delete pst.categoryId;
  }

  return request.post<any>('/app.do', {
    method: url,
    ver: '2.0',
    params: JSON.stringify(Object.assign(obj, pst))
  });
};

export const getSearchHistory1 = (params: any) => {
  if (Taro.getStorageSync('login')) {
    params.accessToken = Taro.getStorageSync('login');
    params.uid = Taro.getStorageSync('uid');
  }
  return request.post<any>('/app.do', {
    method: 'eshop.search.getSearchPage',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
export const getSearchHistory = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.search.getSearchPage',
    ver: '1.0',
    params: JSON.stringify({})
  });
};
export const getSearchDel = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.search.delByUid',
    ver: '1.0',
    params: JSON.stringify({})
  });
};

export const getSearchFind = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.search.getSearchKeywords',
    ver: '1.0',
    params: JSON.stringify({})
  });
};
