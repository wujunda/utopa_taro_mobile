import request from '../utils/request';
import { ProductBaiscData } from '../interface/product';

export interface IHistory {
  code: number;
  data: {
    activityId: number;
    activityName: string;
    activityDesc: string;
    label: string;
    activeTimeDesc: string;
    useTicket: number;
    page: {
      records: ProductBaiscData[];
    };
  };
}
export const getCanUse = (params: any, pst: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.activity.getActiveProductList',
    ver: '2.0',
    params: JSON.stringify({
      activeType: 2,
      activityId: '1239',
      clientType: 1,
      sysId: 1,
      pageSize: pst.per_page,
      pageNo: pst.page,
      ...params
    })
  });
};
