import { View, Text, Input, Image } from '@tarojs/components';
import Taro, { useState, useEffect, useRouter } from '@tarojs/taro';
import { connect } from '@tarojs/redux';
import cancelIcon from '@/assets/login/icon_search_delete.png';
import { sendThirdLoginMobileCheckCode, loginWithGrantCodeAndCheckCode } from '@/services/login';
import Router from '@/utils/router';
import './index.scss';

interface IProps {
  saveLogin: (any) => void;
}

let timeChange;
const Index: Taro.FC<IProps> = (props) => {
  const router = useRouter()
  const [mobile, setMobile] = useState('');
  const [authCode, setAuthCode] = useState('');
  const [seconde, setSeconde] = useState(0);

  useEffect(() => {
    if (seconde == 0) {
      clearInterval(timeChange);
    }
  }, [seconde]);

  const testPhone = (payload) => {
    const mobileReg = /^[1][3-9][0-9]{9}$/;
    if (mobileReg.test(payload)) {
      return true;
    }
    Taro.showToast({
      title: '请输入正确手机号',
      icon: 'none'
    });
    return false;
  };

  const sendCode = () => {
    if (seconde == 0 && testPhone(mobile)) {
      sendThirdLoginMobileCheckCode({ mobile: mobile }).then((res) => {
        if (res.code === 0) {
          Taro.showToast({
            title: '短信验证码已发送',
            icon: 'none'
          });
          setSeconde(60);
          timeChange = setInterval(() => {
            setSeconde((n) => --n);
          }, 1000);
        } else {
          Taro.showToast({
            title: res.msg,
            icon: 'none'
          });
        }
      });
    }
  };

  const next = () => {
    if (authCode == '') {
      return false;
    }
    if (mobile == '') {
      Taro.showToast({
        title:'请输入手机号',
        icon: 'none'
      });
      return false;
    }
    loginWithGrantCodeAndCheckCode({
      keepAccess: 0,
      checkCode: authCode,
      mobile: mobile,
      deviceId: '1',
      clientType: 1,
      grantCode: Taro.getStorageSync('grantCode')
    }).then((res) => {
      if (res.code == 0) {
        Taro.showToast({
          title: '绑定成功',
          icon: 'none'
        });
        props.saveLogin({
          data: res.data
        });
        Taro.setStorage({
          key: 'isLogin',
          data: res.data
        });
        Router.goBack();
      }
    });
  };
  return (
    <View className="utp-bingmoblie">
      <View className="utp-bingmoblie-title">绑定手机号</View>
      <View className="utp-bingmobile-input">
        <View className="utp-bingmobile-input-mobile utp-bingmobile-input-div">
          <Input
            type="text"
            placeholder="请输入手机号"
            maxlength="11"
            value={mobile}
            onInput={(e) => {
              setMobile(e.detail.value);
            }}
          />
          {mobile != '' && (
            <Image
              src={cancelIcon}
              className="utp-bingmobile-input-div-img"
              onClick={() => {
                setMobile('');
              }}
            />
          )}
        </View>
        <View className="utp-bingmobile-input-authCode utp-bingmobile-input-div">
          <Input
            type="tel"
            placeholder="请输入验证码"
            maxlength="8"
            value={authCode}
            onInput={(e) => {
              setAuthCode(e.detail.value);
            }}
          />
          <View className="utp-bingmobile-input-authCode-right">
            {authCode != '' && (
              <Image
                src={cancelIcon}
                className="utp-bingmobile-input-div-img"
                onClick={() => {
                  setAuthCode('');
                }}
              />
            )}
            <View
              className={
                mobile.length == 11 && seconde == 0
                  ? 'utp-bingmobile-input-authCode-right-getCode highColor'
                  : 'utp-bingmobile-input-authCode-right-getCode'
              }
              onClick={() => sendCode()}
            >
              获取验证码
              {seconde != 0 && <Text>({seconde})</Text>}
            </View>
          </View>
        </View>
      </View>

      <View
        className={authCode.length == 6 &&  mobile.length == 11 ? 'utp-bingmobile-button bottonHigh' : 'utp-bingmobile-button'}
        onClick={() => next()}
      >
        下一步
      </View>
    </View>
  );
};

Index.defaultProps = {};

Index.config = {
  navigationBarTitleText: '绑定手机号',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff'
};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    isLogin: state.cart.isLogin,
    isWeiXin: state.user.isWeiXinFalg
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    saveLogin: (payload) => {
      dispatch({
        type: 'cart/saveLogin',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
