import Taro, { useState, useEffect, useDidShow, useRouter } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { ScrollView, View, Text, Image, Input } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import Box from '../../components/Box';
import Bton from '../../components/Bton';
import Wrap from '../../components/Wrap';
import Img from '../../components/Img';
import Txt from '../../components/Txt';
import Cnt from '../../components/Cnt';
import Tab from '../../components/Tab';
import Card from './../../components/Card';
import Model from '../../components/Model';
import SubGoods from '../../components/SubGoods';
import Bar from '../../components/bar';
import JSBridge from '@/utils/jsbridge/index';
import { images } from '../../images';
import './index.scss';
import { ISubmitData, ISubmitStore, IPost, IPosts, getDone, getDone1 } from '../../services/submit';
import Router from '../../utils/router';

import { Glo, goPayPage, successUrl } from '../../utils/utils';

type PageStateProps = {
  post: IPost;
  loading: boolean;
  submit: any;
  inApp: boolean;
  isWeiXinFalg: boolean;
  posts: IPosts[];
};

type PageDispatchProps = {
  syncCart: () => void;
  getSubmit: (any) => void;
  savePost: (any) => void;
  savePosts: (any) => void;
  savePosts1: (any) => void;
  saveSubmit: (any) => void;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
const Index: Taro.FC<IProps> = (props) => {
  let once = [1];
  let isScan = useRouter().params.isScan === '1';
  const [yunfei, setYunfei] = useState(0);
  const [demo, setDemo] = useState(0);
  const [open, setopen] = useState(false);
  const [open1, setopen1] = useState(false);
  const [leftIndex, setLeftIndex] = useState(0);
  const [i, setI] = useState(0);
  const [type, settype] = useState(0);

  enum TYPE {
    TIMER = 0, // 选择时间
    MODE = 1, // 配送方式
    DISCOUNT = 2 // 优惠券
  }

  let State = props.submit;
  let sums!: ISubmitData;
  if (State && State.code === 0) {
    sums = State.data;
  }
  // 去除原生的分享按钮
  if (props.inApp) {
    useEffect(() => {
      let param = {
        show: '0'
      };
      JSBridge.Common.GTBridge_Common_Base_showShareBtn(param, (res) => {});
    }, []);
  }

  // 选中当前店铺
  const [crtStore, setCrtStore] = useState<ISubmitStore | null>(null);
  const [crtStoreMsg, setCrtStoreMsg] = useState<IPosts | null>(null);
  // 提交订单
  const onSubmit = (storeId: number, cardId: string) => {
    props.post.binComits.map((item) => {
      props.posts.map((item1) => {
        if (item1.storeId === item.storeId && item1.crtCard) {
          item.couponCode = item1.crtCard;
        }
      });
      if (storeId === item.storeId) {
        item.couponCode = cardId;
      }
    });
    props.getSubmit({ post: props.post });
    props.savePost({
      data: props.post
    });
  };
  // 计算运费
  const onYunfei = () => {
    let rst = 0;
    // 运费
    sums.products.map((item) => {
      item.logisticsModes.map((item1, index1) => {
        if (item.save.crt === index1) {
          rst += item1.carriage;
        }
      });
    });
    setYunfei(rst);
  };
  const onOrder1 = async () => {
    onOrderOn1();
  };
  const onOrderOn1 = async () => {
    Taro.showLoading({
      title: ''
    });
    let submit = props.submit;
    let store = submit.data.products[0];
    let goods: any[] = [];
    store.orderProducts.map((item) => {
      let obj: any = {};
      obj.skuId = item.skuId;
      obj.skuNum = item.num;
      obj.rfid = '';
      obj.sourceChannel = '';
      goods.push(obj);
    });
    let quan = '';
    props.posts.map((item1) => {
      if (item1.storeId === store.storeId && item1.crtCard) {
        quan = item1.crtCard;
      }
    });
    let post = Object.assign(
      {
        businessId: store.businessId.toString(),
        clientType: 1,
        products: goods,
        //shopcartIds: ['287261'],
        storeId: store.storeId.toString(),
        sysId: 1004
      },
      quan
        ? {
            couponCode: quan
          }
        : {}
    );
    //let data = await getDone1(props.post);
    let data = await getDone1(post);
    Taro.hideLoading();
    if (data.code === 0) {
      // 下单成功
      props.syncCart();
      Router.goPay(
        data.data.products[0].orderNo,
        money,
        data.data.products[0].orderNo,
        data.data.products[0].payOrderNo,
        true
      );
    } else {
      Taro.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
  };

  const onOrder = async () => {
    let canSubmit = true;
    if (!isXUNI && props.posts.length !== props.post.binComits.length) {
      canSubmit = false;
      // 未选择收货地址
      Taro.showToast({
        title: '请先完成配送/收货信息选择',
        icon: 'none',
        duration: 2000
      });
    }
    props.post.binComits.map((item) => {
      props.posts.map((item1) => {
        if (item1.storeId === item.storeId) {
          console.log('地址地址');
          console.log(item1.address);
          item.receiptAddress = item1.address;
          if (!isXUNI && item1.crtLabel !== 5 && item1.crtLabel !== 6 && !item1.address) {
            canSubmit = false;
            // 未选择收货地址
            Taro.showToast({
              title: '请先完成配送/收货信息选择',
              icon: 'none',
              duration: 2000
            });
          }
          // 配送
          if (!isXUNI) {
            item.logisticType = item1.crtLabel;
          }
          // 周边送和到店需要选时间
          if ((!isXUNI && item1.crtLabel === 1) || item1.crtLabel === 5 || item1.crtLabel === 6) {
            if (!item1.crtTime) {
              canSubmit = false;
              // 未选择收货时间
              Taro.showToast({
                title: '请先完成配送/收货信息选择',
                icon: 'none',
                duration: 2000
              });
            } else {
              if (item1.crtLabel === 1) {
                item.lackHandleMessage = item1.noneCrtObj.name;
                item.dictCode = item1.noneCrtObj.dictCode;
              }
              item.beginShippingTime =
                item1.crtTimeObj.dateTime + ' ' + item1.crtTimeObj.beginShippingTime;
              item.endShippingTime =
                item1.crtTimeObj.dateTime + ' ' + item1.crtTimeObj.endShippingTime;
            }
          }
          // 留言
          if (!!item1.msg) {
            // @ts-ignore
            item.buyerMemo = item1.msg;
          }
        }
      });
    });
    if (canSubmit) {
      if (isJifen && sums) {
        Taro.showModal({
          title: '积分使用',
          content: `本次消费将消耗积分账户中的${sums.orderExchangeTotalPoint}积分，是否继续提交订单？`,
          success: (res) => {
            if (res.confirm) {
              onOrderOn();
            } else if (res.cancel) {
            }
          }
        });
      } else {
        onOrderOn();
      }
    }
  };
  const onOrderOn = async () => {
    Taro.showLoading({
      title: ''
    });

    let channelId = 1001;
    if (props.inApp) {
      channelId = 1;
    } else if (props.isWeiXinFalg) {
      channelId = 1002;
    }
    let data = await getDone(props.post);
    Taro.hideLoading();
    if (data.code === 0) {
      // 下单成功
      if (isJifen && sums.orderTotalMoneyNew + yunfei1 == 0) {
        Taro.navigateTo({
          url: '/pages/integral/paySuccess/index?point=' + sums.orderExchangeTotalPoint
        });
        return;
      }
      props.syncCart();
      if (isJifen) {
        // 支付积分订单
        if (props.inApp) {
          const query = {
            payOrderNo: data.data.payOrderNo
          };
          let cmdParams = {
            cmd: 'C0060010000',
            params: JSON.stringify(query)
          };
          JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, (res) => {
            console.log(res);
          });
        } else {
          if (process.env.TARO_ENV === 'h5') {
            // 积分商品微信支付
            goPayPage({
              sysId: channelId,
              payOrderNo: data.data.payOrderNo,
              paySucceesHref: successUrl(),
              payFailHref: successUrl()
            });
          } else {
            // 积分商品小程序支付
            Router.goPay(data.data.id, money, data.data.parentNo, data.data.payOrderNo);
          }
        }
      } else {
        // 普通商品
        if (process.env.TARO_ENV === 'h5') {
          // 普通商品微信支付
          goPayPage({
            sysId: channelId,
            payOrderNo: data.data.payOrderNo,
            paySucceesHref: successUrl(),
            payFailHref: successUrl()
          });
        } else {
          // 普通商品小程序支付
          Router.goPay(data.data.id, money, data.data.parentNo, data.data.payOrderNo);
        }
      }
    } else {
      Taro.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
  };
  let money = '';
  let yunfei1 = 0;
  if (sums && sums.orderTotalMoneyNew) {
    money = Glo.formatPrice(sums.orderTotalMoneyNew + yunfei1);
  }

  let isJifen = false;
  if (sums && sums.orderExchangeTotalPoint && sums.orderExchangeTotalPoint > 0) {
    isJifen = true;
  }
  const [getOBJ, setGetOBJ] = useState<any>();

  if (sums) {
    sums.products.map((item) => {
      let storeItem: IPosts | any = { storeId: item.storeId };
      props.posts.map((item1) => {
        if (item1.storeId && item1.storeId === item.storeId) {
          storeItem = item1;
        }
      });
      // 配送方式
      if (storeItem.crt === undefined && !isScan) {
        item.logisticsModes.map((item1, index1) => {
          if (item1.label === 1) {
            // 缺货
            if (item.lackGoodsInfos && item.lackGoodsInfos.length) {
              storeItem.noneCrt = 0;
              storeItem.noneCrtObj = item.lackGoodsInfos[0];
            }
            storeItem.crt = index1;
            storeItem.crtLabel = item1.label;
            storeItem.saveCrt = index1;
            //getObj = item1;
            item.save = storeItem;
          }
        });
        if (storeItem.crt === undefined) {
          item.logisticsModes.map((item1, index1) => {
            if (index1 === 0) {
              storeItem.crt = index1;
              storeItem.crtLabel = item1.label;
              storeItem.saveCrt = index1;
              //getObj = item1;
              item.save = storeItem;
            }
          });
        }
      } else {
        props.posts.map((item6) => {
          if (item6.storeId === item.storeId) item.save = item6;
        });
      }
    });
  }

  let isXUNI;
  if (sums && !isScan) {
    sums.products.map((item) => {
      isXUNI = item.productType === 2 ? true : false;
      item.logisticsModes.map((item1, index1) => {
        if (item.save && item.save.crt === index1) {
          yunfei1 += item1.carriage;
        }
      });
    });
  }
  useDidShow(() => {
    let post = Taro.getStorageSync('post');
    let posts = Taro.getStorageSync('posts');
    let submit = Taro.getStorageSync('submit');
    if (post) {
      props.savePost({ data: post });
    }
    if (posts && posts.length) {
      //props.savePosts1({ data: posts });
    }
    if (submit) {
      props.saveSubmit({ data: submit });
    }
  });

  if (props.posts && props.posts.length) {
    Taro.setStorage({
      key: 'posts',
      data: props.posts
    });
  }
  if (props.post) {
    Taro.setStorage({
      key: 'post',
      data: props.post
    });
  }
  if (props.submit) {
    Taro.setStorage({
      key: 'submit',
      data: props.submit
    });
  }

  return (
    <View className="utp-submit" style={{ backgroundColor: '#f5f5f5' }}>
      {false && <Bar title="填写订单" />}
      <View className="utp-submit-bd">
        {false && (
          <Box>
            <View
              className="utp-submit-bd-address"
              onClick={() => {
                Taro.navigateTo({
                  url: '/pages/packOrder/address/index'
                });
              }}
            >
              <Wrap type={3}>
                <View className="utp-submit-bd-address-icos">
                  <Cnt>
                    <Img src={images.address} width={80} />
                  </Cnt>
                </View>
                <View style={{ flex: 1 }}>
                  <Txt
                    justifyContent="flex-start"
                    title="黄先生 13800138000"
                    size={32}
                    color="deep"
                    bold
                    height={44}
                  />
                  <Txt
                    justifyContent="flex-start"
                    title="广东省 广州市 天河区 华夏路10号富力中心10楼2023房 富力地产中心"
                    size={26}
                    color="deep"
                    height={36}
                  />
                </View>
                <View className="utp-submit-bd-address-rights">
                  <Img src={images.right} width={32} />
                </View>
              </Wrap>
            </View>
          </Box>
        )}
        {!!sums &&
          sums.products.map((item) => {
            let isXuni = item.productType === 2 ? true : false;
            let storeItem: IPosts | any = { storeId: item.storeId };
            props.posts.map((item1) => {
              if (item1.storeId && item1.storeId === item.storeId) {
                storeItem = item1;
              }
            });
            // 配送方式
            let getObj: any = '';
            if (storeItem.crt === undefined && !isScan) {
              item.logisticsModes.map((item1, index1) => {
                if (item1.label === 1) {
                  // 缺货
                  if (item.lackGoodsInfos && item.lackGoodsInfos.length) {
                    storeItem.noneCrt = 0;
                    storeItem.noneCrtObj = item.lackGoodsInfos[0];
                  }
                  storeItem.crt = index1;
                  storeItem.crtLabel = item1.label;
                  storeItem.saveCrt = index1;
                  getObj = item1;
                  item.save = storeItem;
                }
              });
              if (storeItem.crt === undefined) {
                item.logisticsModes.map((item1, index1) => {
                  if (index1 === 0) {
                    storeItem.crt = index1;
                    storeItem.crtLabel = item1.label;
                    storeItem.saveCrt = index1;
                    getObj = item1;
                    item.save = storeItem;
                  }
                });
              }
            } else {
              props.posts.map((item6) => {
                if (item6.storeId === item.storeId) item.save = item6;
              });
            }
            // 配送方式
            let title = '';
            if (!isScan) {
              item.logisticsModes.map((item1, index1) => {
                if (index1 === storeItem.crt) {
                  title = item1.name;
                  getObj = item1;
                }
              });
            }
            // 缺货提示
            let noneTitle = '';
            console.log('缺货提示');
            console.log(storeItem);
            if (!isScan && storeItem.crtLabel === 1) {
              item.lackGoodsInfos.map((item1, index1) => {
                if (index1 === storeItem.noneCrt) {
                  noneTitle = item1.name;
                }
              });
            }

            // 收货地址
            if (storeItem && storeItem.address) {
              item.address = storeItem.address;
            }
            // 配送时间
            let noGet = false;
            let crtTime =
              storeItem.crtLabel === 5 || storeItem.crtLabel === 6
                ? '请选择到店时间'
                : '请选择配送时间';
            if (!isScan) {
              item.logisticsModes.map((item1, index1) => {
                if (index1 === storeItem.crt && !item1.dateInfoList) {
                  noGet = true;
                }
                if (index1 === storeItem.crt && item1.dateInfoList) {
                  item1.dateInfoList.map((item2) => {
                    if (item2.shippingTimes) {
                      item2.shippingTimes.map((item3) => {
                        if (item3.endShippingTime === storeItem.crtTime) {
                          crtTime =
                            item3.dateTime +
                            ' ' +
                            item3.beginShippingTime +
                            '-' +
                            item3.endShippingTime;
                        }
                      });
                    }
                  });
                }
              });
            }
            // 优惠券
            let cardMsg = item.discountDetail;

            return (
              <Box color="transparent">
                <View className="utp-submit-bd-box utp-submit-bd-box-1">
                  <View className="utp-submit-bd-store">
                    <Wrap type={1}>
                      <Wrap>
                        <Img src={images.store} width={48} />
                        <View className="utp-submit-bd-store-title">
                          <Txt title={item.storeName} size={28} color="deep" />
                        </View>
                        {!isXuni && storeItem && storeItem.crtLabel === 1 && (
                          <Img src={images.order_jishida} width={110} height={36} />
                        )}
                      </Wrap>
                    </Wrap>
                  </View>
                  {item.orderProducts.map((item1) => {
                    return <SubGoods sums={sums} data={item1} key={item1.id} />;
                  })}
                  {false && (
                    <Wrap Myheight={74} top>
                      <Wrap top>
                        <Txt title="赠品" size={24} color="deep" bold />
                        <View
                          style={{
                            marginLeft: Taro.pxTransform(12),
                            // marginTop: Taro.pxTransform(20),
                            // marginBottom: Taro.pxTransform(20),
                            width: Taro.pxTransform(500)
                          }}
                        >
                          <Txt
                            dian
                            title="赠品的赠品的赠品的赠品的赠品的赠品的赠品的赠品的赠品的赠品的"
                            color="deep"
                          />
                        </View>
                      </Wrap>
                    </Wrap>
                  )}
                  {false && (
                    <Wrap
                      Myheight={72}
                      justifyContent="space-between"
                      click={() => {
                        setopen(false);
                      }}
                    >
                      <Txt title="限购商品，您已超过活动限额" size={24} color="red" />
                      <Img src={images.del} width={36} />
                    </Wrap>
                  )}
                  {!isScan && (
                    <View>
                      {!isXuni && (
                        <Wrap
                          click={() => {
                            //crtStore = item;
                            setCrtStore(item);
                            setCrtStoreMsg(storeItem);
                            settype(1);
                            setopen(true);
                          }}
                          justifyContent="space-between"
                          Myheight={88}
                        >
                          <Txt title="配送方式" size={28} color="deep" />
                          <Wrap>
                            <Txt title={title} size={26} color="deep" bold />
                            <Img src={images.right} width={32} />
                          </Wrap>
                        </Wrap>
                      )}
                      {!isXuni && !item.address && getObj.label !== 5 && getObj.label !== 6 && (
                        <Wrap
                          click={() => {
                            Router.goAddress(item.storeId);
                          }}
                          justifyContent="space-between"
                          Myheight={88}
                        >
                          <Txt title="收货地址" size={28} color="deep" />
                          <Wrap>
                            <Txt title="请选择收货人地址" size={26} color="deep" bold />
                            <Img src={images.right} width={32} />
                          </Wrap>
                        </Wrap>
                      )}
                      {!isXuni && getObj && (getObj.label === 5 || getObj.label === 6) && (
                        <Wrap
                          click={() => {
                            //Router.goAddress(item.storeId);
                          }}
                          justifyContent="flex-start"
                          Myheight={88}
                        >
                          <Txt title="店铺地址" size={28} color="deep" />
                          <View style={{ width: Taro.pxTransform(30) }}></View>
                          <Wrap>
                            <Txt title={getObj.selfAddress} size={26} color="deep" bold />
                          </Wrap>
                        </Wrap>
                      )}

                      {!isXuni && !!item.address && getObj.label !== 5 && getObj.label !== 6 && (
                        <View
                          className="utp-submit-address"
                          onClick={() => {
                            Router.goAddress(item.storeId);
                          }}
                        >
                          <View className="utp-submit-address-title">
                            <Txt title="收货地址" size={28} color="deep" />
                          </View>
                          <View className="utp-submit-address-cont">
                            <Wrap top>
                              <View className="utp-submit-address-cont1">
                                <Txt
                                  justifyContent="flex-start"
                                  title={item.address.address}
                                  size={28}
                                  color="deep"
                                  bold
                                  dian
                                />
                              </View>
                              <View
                                onClick={() => {
                                  Taro.navigateTo({
                                    url: '/pages/packOrder/address/index'
                                  });
                                }}
                              >
                                <Img src={images.right} width={32} />
                              </View>
                            </Wrap>
                            <Txt
                              justifyContent="flex-start"
                              title={item.address.userName + ' ' + item.address.phone}
                              size={28}
                              color="deep"
                              bold
                            />
                          </View>
                        </View>
                      )}
                      {
                        // 周边送超区
                      }
                      {!isXuni &&
                        storeItem.crtLabel === 1 &&
                        !!item.address &&
                        noGet &&
                        !props.loading && (
                          <View className="utp-submit-tip">
                            <Txt
                              justifyContent="flex-start"
                              title="收货地址不在商家配送范围，请重新选择地址"
                              color="red"
                              size={24}
                              height={68}
                            />
                          </View>
                        )}
                      {
                        // 到店时间
                      }
                      {!isXuni && getObj && (getObj.label === 5 || getObj.label === 6) && (
                        <Wrap
                          click={() => {
                            setGetOBJ(getObj);
                            setCrtStore(item);
                            setCrtStoreMsg(storeItem);
                            settype(0);
                            setopen(true);
                          }}
                          Myheight={88}
                          justifyContent="space-between"
                        >
                          <Txt title="到店时间" size={28} color="deep" />
                          <Wrap>
                            <Txt title={crtTime} size={28} color="deep" bold />
                            <Img src={images.right} width={32} />
                          </Wrap>
                        </Wrap>
                      )}

                      {
                        // 周边送配送时间
                      }
                      {!isXuni && storeItem.crtLabel === 1 && !!item.address && (
                        <Wrap
                          click={() => {
                            if (!noGet) {
                              setGetOBJ(getObj);
                              setCrtStore(item);
                              setCrtStoreMsg(storeItem);
                              settype(0);
                              setopen(true);
                            }
                          }}
                          Myheight={88}
                          justifyContent="space-between"
                        >
                          <Txt title="配送时间" size={28} color="deep" />
                          <Wrap>
                            <Txt title={crtTime} size={28} color="deep" bold />
                            <Img src={images.right} width={32} />
                          </Wrap>
                        </Wrap>
                      )}

                      <Wrap
                        click={() => {
                          setCrtStore(item);
                          setCrtStoreMsg(storeItem);
                          settype(2);
                          setopen(true);
                        }}
                        Myheight={88}
                        justifyContent="space-between"
                      >
                        <Txt title="优惠券" size={28} color="deep" />
                        <Wrap>
                          <Txt title={cardMsg} size={26} color="deep" bold />
                          <Img src={images.right} width={32} />
                        </Wrap>
                      </Wrap>
                      {!isXuni && storeItem.crtLabel === 1 && (
                        <Wrap
                          click={() => {
                            setCrtStore(item);
                            setCrtStoreMsg(storeItem);
                            setopen1(true);
                          }}
                          Myheight={88}
                          justifyContent="space-between"
                        >
                          <Txt title="如遇缺货" size={28} color="deep" />
                          <Wrap>
                            <Txt title={noneTitle} size={26} color="deep" bold />
                            <Img src={images.right} width={32} />
                          </Wrap>
                        </Wrap>
                      )}

                      <Wrap Myheight={88}>
                        <Txt title="订单备注" size={28} color="deep" />
                        <View className="utp-submit-remarks">
                          <Input
                            onInput={(e) => {
                              let value = e.detail.value;
                              storeItem.msg = value;
                              props.savePosts({
                                data: storeItem
                              });
                            }}
                            type="text"
                            placeholder=" 选填，输入对商品或配送的特殊要求"
                          />
                        </View>
                      </Wrap>
                    </View>
                  )}
                  {isScan && (
                    <View>
                      <View>
                        <Wrap Myheight={80} justifyContent="space-between">
                          <Txt title={isJifen ? '兑换金额' : '商品总额'} size={28} color="deep" />
                          {!!sums && (
                            <Txt
                              title={'¥' + Glo.formatPrice(sums.actuallyPrice)}
                              size={28}
                              color="deep"
                            />
                          )}
                        </Wrap>
                      </View>

                      {false && (
                        <View>
                          <Wrap Myheight={80} justifyContent="space-between">
                            <Txt title="优惠金额" size={28} color="deep" />
                            {!!sums && (
                              <Txt title={'¥' + sums.actuallyPrice} size={28} color="deep" />
                            )}
                          </Wrap>
                        </View>
                      )}
                      {once.map((itemo) => {
                        let card: any = '';
                        if (!!sums && !!sums.couponDiscountInfo) {
                          sums.couponDiscountInfo.map((item1) => {
                            card = item1;
                          });
                        }

                        return (
                          <View key={itemo}>
                            <Wrap Myheight={80} justifyContent="space-between">
                              {!card && <Txt title="优惠券" size={28} color="deep" />}
                              {!!card && (
                                <Txt
                                  title={'优惠券(' + card.couponName + ')'}
                                  size={28}
                                  color="deep"
                                />
                              )}
                              <Txt
                                title={'-¥' + (card ? Glo.formatPrice(card.discountValue) : 0)}
                                size={28}
                                color="deep"
                              />
                            </Wrap>
                          </View>
                        );
                      })}
                    </View>
                  )}
                </View>
              </Box>
            );
          })}
        {isScan &&
          !!sums &&
          sums.products.map((item) => {
            let storeItem: IPosts | any = { storeId: item.storeId };
            props.posts.map((item1) => {
              if (item1.storeId && item1.storeId === item.storeId) {
                storeItem = item1;
              }
            });
            // 配送方式
            let getObj: any = '';
            if (storeItem.crt === undefined && !isScan) {
              storeItem.crt = 0;
              storeItem.crtLabel = item.logisticsModes[0].label;
              storeItem.saveCrt = 0;
              getObj = item.logisticsModes[0];
              item.save = storeItem;
            } else {
              props.posts.map((item6) => {
                if (item6.storeId === item.storeId) item.save = item6;
              });
            }
            let title = '';
            if (!isScan) {
              item.logisticsModes.map((item1, index1) => {
                if (index1 === storeItem.crt) {
                  title = item1.name;
                  getObj = item1;
                }
              });
            }

            // 收货地址
            if (storeItem && storeItem.address) {
              item.address = storeItem.address;
            }
            // 配送时间
            let noGet = false;
            let crtTime =
              storeItem.crtLabel === 5 || storeItem.crtLabel === 6
                ? '请选择到店时间'
                : '请选择配送时间';
            if (!isScan) {
              item.logisticsModes.map((item1, index1) => {
                if (index1 === storeItem.crt && !item1.dateInfoList) {
                  noGet = true;
                }
                if (index1 === storeItem.crt && item1.dateInfoList) {
                  item1.dateInfoList.map((item2) => {
                    if (item2.shippingTimes) {
                      item2.shippingTimes.map((item3) => {
                        if (item3.endShippingTime === storeItem.crtTime) {
                          crtTime =
                            item3.dateTime +
                            ' ' +
                            item3.beginShippingTime +
                            '-' +
                            item3.endShippingTime;
                        }
                      });
                    }
                  });
                }
              });
            }
            // 优惠券
            let cardMsg = item.discountDetail;

            return (
              <Box color="transparent">
                <View
                  className="utp-submit-bd-box utp-submit-bd-box-1"
                  style={{ paddingBottom: Taro.pxTransform(10) }}
                >
                  <Wrap
                    click={() => {
                      setCrtStore(item);
                      setCrtStoreMsg(storeItem);
                      settype(2);
                      setopen(true);
                    }}
                    Myheight={88}
                    justifyContent="space-between"
                  >
                    <Txt title="优惠券" size={28} color="deep" />
                    <Wrap>
                      <Txt title={cardMsg} size={26} color="deep" bold />
                      <Img src={images.right} width={32} />
                    </Wrap>
                  </Wrap>

                  <Wrap Myheight={88} justifyContent="space-between">
                    <Txt title="配送方式" size={28} color="deep" />
                    <View style={{ paddingRight: Taro.pxTransform(10) }}>
                      <Wrap>
                        <Txt title="扫码自提" size={26} color="deep" bold />
                      </Wrap>
                    </View>
                  </Wrap>
                </View>
              </Box>
            );
          })}

        {!isScan && (
          <Box color="#F5f5f5">
            <View className="utp-submit-bd-box utp-submit-bd-box-2">
              <View>
                <Wrap Myheight={80} justifyContent="space-between">
                  <Txt title={isJifen ? '兑换金额' : '商品总额'} size={28} color="deep" />
                  {!!sums && (
                    <Txt title={'¥' + Glo.formatPrice(sums.actuallyPrice)} size={28} color="deep" />
                  )}
                </Wrap>
              </View>
              {isJifen && (
                <View>
                  <Wrap Myheight={80} justifyContent="space-between">
                    <Txt title="兑换积分" size={28} color="deep" />
                    {!!sums && (
                      <Txt title={'-' + sums.orderExchangeTotalPoint} size={28} color="deep" />
                    )}
                  </Wrap>
                </View>
              )}

              {false && (
                <View>
                  <Wrap Myheight={80} justifyContent="space-between">
                    <Txt title="优惠活动" size={28} color="deep" />
                    {!!sums && <Txt title={'¥' + sums.actuallyPrice} size={28} color="deep" />}
                  </Wrap>
                </View>
              )}
              <View>
                <Wrap Myheight={80} justifyContent="space-between">
                  <Txt title="运费" size={28} color="deep" />
                  {yunfei1 !== 0 && (
                    <Txt title={'+¥' + Glo.formatPrice(yunfei1)} size={28} color="deep" />
                  )}
                  {!!sums && yunfei1 == 0 && (
                    <Txt title={'+¥' + Glo.formatPrice(0)} size={28} color="deep" />
                  )}
                </Wrap>
              </View>
              {!!sums &&
                !!sums.couponDiscountInfo &&
                sums.couponDiscountInfo.map((item) => {
                  return (
                    <View>
                      <Wrap Myheight={80} justifyContent="space-between">
                        <Txt title={'优惠券(' + item.couponName + ')'} size={28} color="deep" />
                        <Txt
                          title={'-¥' + Glo.formatPrice(item.discountValue)}
                          size={28}
                          color="deep"
                        />
                      </Wrap>
                    </View>
                  );
                })}
            </View>
          </Box>
        )}
      </View>
      <View
        className="utp-submit-ft"
        onClick={() => {
          setDemo(demo + 1);
        }}
      >
        <View className="utp-submit-btns">
          <Wrap type={1} justifyContent="space-between">
            <Wrap>
              <Txt title="应付总额：" size={34} color="black" bold />
              {!!sums && (
                <Txt
                  title={'¥' + Glo.formatPrice(sums.orderTotalMoneyNew + yunfei1)}
                  size={34}
                  color="red"
                  bold
                />
              )}
            </Wrap>
            <View
              className="utp-submit-btns-btn"
              onClick={() => {
                if (isScan) {
                  onOrder1();
                } else {
                  onOrder();
                }
              }}
            >
              <Bton size="mini">提交订单</Bton>
            </View>
          </Wrap>
        </View>
      </View>
      {/*缺货 start  */}
      {open1 && (
        <Model
          show={open1}
          title={'如遇缺货'}
          height={460}
          onClose={() => {
            setopen1(false);
          }}
        >
          <View style={{ display: 'block' }}>
            <View className="utp-submit-model utp-submit-model-1">
              <View className="utp-submit-model-top">
                {!!crtStoreMsg &&
                  !!crtStore &&
                  !isScan &&
                  !!crtStore.lackGoodsInfos &&
                  crtStore.lackGoodsInfos.map((item, si) => {
                    return (
                      <View className="utp-submit-model-peisong">
                        {si !== crtStore.lackGoodsInfos.length - 1 && (
                          <View className="utp-submit-model-peisong-gang"></View>
                        )}
                        <View className="utp-submit-model-b1">
                          <Wrap
                            Myheight={100}
                            key={item.label}
                            click={() => {
                              crtStoreMsg.noneCrt = si;
                              crtStoreMsg.noneCrtObj = item;
                              props.savePosts({
                                data: crtStoreMsg
                              });

                              setopen1(false);
                              //setUpdate(update + 1);
                            }}
                            justifyContent="space-between"
                          >
                            <Txt title={item.name} size={32} color="deep" />
                            <Image
                              src={si === crtStoreMsg.noneCrt ? images.check1 : images.uncheck1}
                              className="img"
                            />
                          </Wrap>
                        </View>
                      </View>
                    );
                  })}
              </View>
            </View>
          </View>
        </Model>
      )}
      {/*缺货 end  */}

      <Model
        show={open}
        title={type === TYPE.TIMER ? '选择时间段' : type === TYPE.MODE ? '配送方式' : '优惠'}
        height={type === TYPE.TIMER ? 814 : type === TYPE.MODE ? 560 : 1064}
        onClose={() => {
          setopen(false);
        }}
      >
        {/*配送方式 start  */}
        <View style={{ display: type === TYPE.MODE ? 'block' : 'none' }}>
          <View className="utp-submit-model utp-submit-model-1">
            <View className="utp-submit-model-top">
              {!!crtStoreMsg &&
                !!crtStore &&
                !isScan &&
                !!crtStore.logisticsModes &&
                crtStore.logisticsModes.map((item, si) => {
                  return (
                    <View className="utp-submit-model-peisong">
                      {item.label !== 1 && si !== crtStore.logisticsModes.length - 1 && (
                        <View className="utp-submit-model-peisong-gang"></View>
                      )}
                      {item.label === 1 && (
                        <View className="utp-submit-model-peisong-b">
                          <Img src={images.order_jsdbk} width={750} height={100} />
                        </View>
                      )}

                      <View className="utp-submit-model-b1">
                        <Wrap
                          Myheight={100}
                          key={item.label}
                          click={() => {
                            if (item.label === 1 && !item.startingPriceOk) {
                              return;
                            }
                            crtStoreMsg.saveCrt = si;
                            crtStoreMsg.crtSaveLabel = item.label;
                            props.savePosts({
                              data: crtStoreMsg
                            });

                            setTimeout(() => {
                              if (crtStoreMsg) {
                                crtStoreMsg.crt = crtStoreMsg.saveCrt;
                                crtStoreMsg.crtLabel = crtStoreMsg.crtSaveLabel;
                                props.savePosts({
                                  data: crtStoreMsg
                                });
                              }
                              setopen(false);
                            }, 30);
                            //setUpdate(update + 1);
                          }}
                          justifyContent="space-between"
                        >
                          {item.label === 1 && (
                            <Wrap>
                              <Img src={images.order_jishida1} width={110} height={42.58} />
                              <View style={{ width: Taro.pxTransform(20) }}></View>
                              <Txt title={`起送价：¥${Glo.formatPrice(item.startingPrice)}`} />
                            </Wrap>
                          )}
                          {item.label !== 1 && <Txt title={item.name} size={32} color="deep" />}
                          <Image
                            src={
                              item.label === 1 && !item.startingPriceOk
                                ? images.order_none
                                : si === crtStoreMsg.saveCrt
                                ? images.check1
                                : images.uncheck1
                            }
                            className="img"
                          />
                        </Wrap>
                      </View>
                    </View>
                  );
                })}
            </View>
            {false && (
              <View
                className="utp-submit-model-btn"
                onClick={() => {
                  if (crtStoreMsg) {
                    crtStoreMsg.crt = crtStoreMsg.saveCrt;
                    crtStoreMsg.crtLabel = crtStoreMsg.crtSaveLabel;
                    props.savePosts({
                      data: crtStoreMsg
                    });
                  }
                  setopen(false);
                }}
              >
                <Bton>确定</Bton>                 
              </View>
            )}
          </View>
        </View>
        {/*配送方式 end  */}
        {/* 配送时间 start */}
        <View
          className="utp-submit-timer"
          style={{ display: type === TYPE.TIMER ? 'flex' : 'none' }}
        >
          <View className="utp-submit-timer-left">
            {getOBJ &&
              getOBJ.dateInfoList &&
              getOBJ.dateInfoList.map((item, inde) => {
                return (
                  <Text
                    onClick={() => {
                      setLeftIndex(inde);
                    }}
                    className={
                      leftIndex === inde
                        ? 'utp-submit-timer-left-item leftActived'
                        : 'utp-submit-timer-left-item'
                    }
                    key={inde}
                  >
                    {' '}
                    {item.dateStr}
                  </Text>
                );
              })}
          </View>
          <View className="utp-submit-timer-right">
            {getOBJ &&
              getOBJ.dateInfoList &&
              getOBJ.dateInfoList[leftIndex] &&
              getOBJ.dateInfoList[leftIndex].shippingTimes &&
              getOBJ.dateInfoList[leftIndex].shippingTimes.map((item, u) => {
                return (
                  <View
                    className="utp-submit-timer-right-item"
                    key={u}
                    onClick={() => {
                      if (crtStoreMsg) {
                        crtStoreMsg.crtTime = item.endShippingTime;
                        crtStoreMsg.crtTimeObj = item;
                      }
                      console.log(crtStoreMsg);
                      setCrtStoreMsg(crtStoreMsg);
                      setopen(false);

                      props.savePosts({
                        data: crtStoreMsg
                      });
                      //onYunfei();
                    }}
                  >
                    <Wrap Myheight={100} justifyContent="space-between">
                      <Txt
                        title={item.beginShippingTime + '-' + item.endShippingTime}
                        size={32}
                        color={
                          !!crtStoreMsg && item.endShippingTime === crtStoreMsg.crtTime
                            ? 'red'
                            : 'normal'
                        }
                      />
                    </Wrap>
                  </View>
                );
              })}
          </View>
        </View>
        {/* 配送时间 end */}
        {/* 优惠start */}
        {!!crtStoreMsg && (
          <View style={{ display: type === TYPE.DISCOUNT ? 'block' : 'none' }}>
            <Tab
              barList={['可用', '不可用']}
              onChange={(inn) => {
                setI(inn);
              }}
              defaultActiveKey={i}
              type={1}
              fixed={false}
            >
              {i === 0 && crtStore && crtStore.coupons && crtStore.coupons.alivableCoupons && (
                <View className="utp-submit-barBox">
                  <View className="utp-submit-barBox-top">
                    {!!crtStore &&
                      !!crtStore.coupons &&
                      !!crtStore.coupons.alivableCoupons &&
                      crtStore.coupons.alivableCoupons.map((item) => {
                        return (
                          <Card
                            right
                            key={item.couponId}
                            ischeck={
                              !!crtStore && crtStoreMsg.saveCrtCard === item.couponCode
                                ? true
                                : false
                            }
                            data={item}
                            choose={() => {
                              //setcard(tt);
                              if (crtStoreMsg.saveCrtCard === item.couponCode) {
                                crtStoreMsg.saveCrtCard = '';
                                props.savePosts({
                                  data: crtStoreMsg
                                });
                              } else {
                                crtStoreMsg.saveCrtCard = item.couponCode;
                                props.savePosts({
                                  data: crtStoreMsg
                                });
                              }
                            }}
                          />
                        );
                      })}
                  </View>
                  <View
                    className="utp-submit-barBox-btn"
                    onClick={() => {
                      if (crtStoreMsg) {
                        crtStoreMsg.crtCard = crtStoreMsg.saveCrtCard;
                        props.savePosts({
                          data: crtStoreMsg
                        });
                        onSubmit(crtStoreMsg.storeId, crtStoreMsg.crtCard);
                      }
                      setopen(false);
                    }}
                  >
                    <Bton>确定</Bton>
                  </View>
                </View>
              )}
              {i !== 0 && crtStore && crtStore.coupons && crtStore.coupons.unalivableCoupons && (
                <View className="utp-submit-barBox">
                  <ScrollView>
                    {!!crtStore &&
                      !!crtStore.coupons &&
                      !!crtStore.coupons.unalivableCoupons &&
                      crtStore.coupons.unalivableCoupons.map((item) => {
                        return <Card data={item} type={1} Title="已过期" key={item.couponId} />;
                      })}
                  </ScrollView>
                </View>
              )}
            </Tab>
          </View>
        )}
        {/* 优惠end */}
      </Model>
    </View>
  );
};

Index.config = {
  navigationBarTitleText: '填写订单',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    inApp: state.user.inApp,
    isWeiXinFalg: state.user.isWeiXinFalg,
    post: state.cart.post,
    submit: state.cart.submit,
    posts: state.cart.posts,
    loading:
      state.loading.effects['cart/changeGoods'] ||
      state.loading.effects['cart/syncCart'] ||
      state.loading.effects['cart/checkGoods'] ||
      state.loading.effects['cart/checkStore'] ||
      state.loading.effects['cart/checkCart']
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    getSubmit: (payload) => {
      dispatch({
        type: 'cart/getSubmit',
        payload
      });
    },
    savePost: (payload) => {
      dispatch({
        type: 'cart/savePost',
        payload
      });
    },

    syncCart: () => {
      dispatch({
        type: 'cart/syncCart'
      });
    },
    savePosts: (payload) => {
      dispatch({
        type: 'cart/savePosts',
        payload
      });
    },
    savePosts1: (payload) => {
      dispatch({
        type: 'cart/savePosts1',
        payload
      });
    },
    saveSubmit: (payload) => {
      dispatch({
        type: 'cart/saveSubmit',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
