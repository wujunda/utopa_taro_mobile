/* eslint-disable react-hooks/exhaustive-deps */
import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import './index.scss';
// #region 书写注意
// #endregion

// 底部sku
interface IProps {
  type?: number;
  title?: string;
  show?: boolean;
  width?:number;
  onClose?: () => void;
  onConfirm?:()=>void;
  closeOverlay?: () => void;
  leftBtn?: string;
  rightBtn?: string;
  leftBtnColor?: string;
  rightBtnColor?: string;
}
const Index: Taro.FC<IProps> = ({
  title,
  show,
  onClose,
  leftBtn = '取消',
  rightBtn = '确定',
  leftBtnColor,
  rightBtnColor,
  type = 3,
  onConfirm,
  width=670,
}) => {
  return (
    <View className={show ? 'utp-message active' : 'utp-message'}>
      <View className="utp-message__overlay" onClick={onClose}></View>
      <View className="utp-message__container layout" style={{width:Taro.pxTransform(width)}}>
        <View className="layout-header">{title}</View>
        <View className="utp-message-content">{this.props.children}</View>
        {// 用于支付
        type === 1 ? (
          <View className="utp-message-typebtn1">
            <Text
              className="utp-message-btn utp-message-btn1"
              style={{ color: leftBtnColor }}
              onClick={onClose}
            >
              {leftBtn}
            </Text>
            <Text className="utp-message-btn" style={{ color: rightBtnColor }} onClick={onConfirm}>
              {rightBtn}
            </Text>
          </View>
        ) : null}
        {// 用于左右结构有样式按钮
        type === 2 ? (
          <View className="utp-message-typebtn2">
            <Text
              className="utp-message-typebtn2-item utp-message-typebtn2-item1"
              style={{ color: leftBtnColor }}
              onClick={onClose}
            >
              {leftBtn}
            </Text>
            <Text
              className="utp-message-typebtn2-item utp-message-typebtn2-item2"
              style={{ color: rightBtnColor }}
              onClick={onConfirm}
            >
              {rightBtn}
            </Text>
          </View>
        ) : null}
        {// 用于上下结构有样式按钮
        type === 3 ? (
          <View className="utp-message-typebtn3">
            <Text
              className="utp-message-typebtn3-item utp-message-typebtn2-item2"
              style={{ color: leftBtnColor }}
              onClick={onConfirm}
            >
              {rightBtn}
            </Text>
            <Text
              className="utp-message-typebtn3-item utp-message-typebtn2-item1"
              style={{ color: rightBtnColor }}
              onClick={onClose}
            >
              {leftBtn}
            </Text>
          </View>
        ) : null}
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
