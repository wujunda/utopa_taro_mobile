import request from '../utils/request';

export interface Cate {
  name: string;
  id: number;
}

export interface ICate {
  data: {
    list: Cate[];
  };
}
// 登陆
export const getLogin = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'account.login.doLogin',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      clientType: '1'
    })
  });
};
// 第三方登陆
export const getLoginOther = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'account.login.loginByThirdAuth',
    ver: '1.0',
    params: JSON.stringify({
      ...params
    })
  });
};
// 临时登陆
export const getLoginNone = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'account.login.loginWithGrantCode',
    ver: '1.0',
    params: JSON.stringify({
      ...params
    })
  });
};
// 获取手机号
export const getPhone = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'account.wxMini.decryptedData',
    ver: '1.0',
    params: JSON.stringify({
      ...params
    })
  });
};

/**
 * 通过openToken换取accessToken
 * @openToken
 */
export const _fetchToken = (param) => {
  return request.post<any>('/app.do', {
    method: 'account.access.getChildToken',
    ver: '1.0',
    params: JSON.stringify(param)
  });
};

// 发送验证码
export const sendLoginCheckCode = (param) => {
  return request.post<any>('/app.do', {
    method: 'account.login.sendLoginCheckCode',
    ver: '1.0',
    params: JSON.stringify(param)
  });
};

// 发送绑定手机号验证码
export const sendThirdLoginMobileCheckCode = (param) => {
  return request.post<any>('/app.do', {
    method: 'account.login.sendThirdLoginMobileCheckCode',
    ver: '1.0',
    params: JSON.stringify(param)
  });
};

// 绑定手机号
export const loginWithGrantCodeAndCheckCode = (param) => {
  return request.post<any>('/app.do', {
    method: 'account.login.loginWithGrantCodeAndCheckCode',
    ver: '1.0',
    params: JSON.stringify(param)
  });
};

// 微信授权链接
export const getOauthUrl = (param) => {
  return request.post<any>('/app.do', {
    method: 'account.wechatApp.getOauthUrl',
    ver: '1.0',
    params: JSON.stringify(param)
  });
};
