import { useState, useEffect, useRef } from '@tarojs/taro';

function useEndTime(second) {
  const [start, setStart] = useState(false);
  const [time, setTime] = useState(second);
  const interval: any = useRef(); // interval 可以在这个作用域里任何地方清除和设置

  useEffect(() => {
    // effect 函数，不接受也不返回任何参数
    if (start) {
      interval.current = setInterval(() => {
        setTime((t) => t - 1);
      }, 1000);
    }
    return () => clearInterval(interval.current); // clean-up 函数，当前组件被注销时调用
  }, [start]); // 依赖数组，当数组中变量变化时会调用 effect 函数
  useEffect(() => {
    setStart(true);
  }, []);

  return [time];
}

export default useEndTime;
