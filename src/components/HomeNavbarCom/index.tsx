/* eslint-disable react/jsx-no-undef */
import Taro from '@tarojs/taro';
import ItemHomeIcon from '@/components/ItemHomeIcon';
// import { View } from '@tarojs/components';
import './index.scss';

import { Swiper, SwiperItem, Image, View, AtAvatar, Text } from '@tarojs/components';


// eslint-disable-next-line import/first

// eslint-disable-next-line import/first

interface ImageItem {
  id?: string;
  src?: any;
  label?: any;
  width?: number; // 百分比
  height?: number | string;
  isCircular: boolean;
}

interface Props {
  dataSource: ImageItem[];
}
const HomeNavBarCom: Taro.FC<Props> = ({ dataSource }) => {
  // console.log(dataSource, 'HomeNavBarCom');
  return (
    <View className="flex-box">
      {Array.isArray(dataSource) &&
        dataSource.map((item, idx) => {
          return (
            // eslint-disable-next-line react/jsx-key
            <View className="flex-item" key={idx}>
              <ItemHomeIcon
                tips={item.label}
                src={item.src}
                tipsSize={24}
                Height={100}
                Width={100}
                isCircular
              />
            </View>
          );
        })}
    </View>
  );
};

export default HomeNavBarCom;
