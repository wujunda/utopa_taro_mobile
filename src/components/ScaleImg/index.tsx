import Taro, { useState } from '@tarojs/taro';
import { View, Image } from '@tarojs/components';

import './index.scss';


interface IProps {
}

const Index: Taro.FC<IProps> = ({ data }) => {
  const [offsetX, setOffsetX] = useState(0)
  const [offsetY, setOffsetY] = useState(0)
  const [offsetLeftX, setOffsetLeftX] = useState(0)
  const [offsetLeftY, setOffsetLeftY] = useState(0)
  const [zoom, setZoom] = useState(false)
  const [distance, setDistance] = useState(0)
  const [scale, setScale] = useState(1)
  const [startX, setStartX] = useState(0)
  const [startY, setStartY] = useState(0)
  const touchStart = (e) => {
    if (e.touches.length == 1) {
      let {clientX, clientY} = e.touches[0];
      setStartX(clientX)
      setStartY(clientY)
    } else {
      let xMove = e.touches[1].clientX - e.touches[0].clientX;
      let yMove = e.touches[1].clientY - e.touches[0].clientY;
      let distance = Math.sqrt(xMove * xMove + yMove * yMove);
      setDistance(distance)
      setZoom(true)
    }
  }

  // // 触摸移动事件
  const touchMove = (e) => {
    if (e.touches.length == 1) {
      //单指移动,缩放状态，不处理单指
      if (zoom) {
        return;
      }
      let {clientX, clientY} = e.touches[0];
      let newoffsetX = clientX - startX;
      let newoffsetY = clientY - startY;
      setStartX(clientX)
      setStartY(clientY)
      let moveOffsetX = offsetX
      let moveOffsetY = offsetY
      let moveOffsetLeftX = offsetLeftX
      let moveOffsetLeftY = offsetLeftY
      moveOffsetX += newoffsetX;
      moveOffsetY += newoffsetY;
      moveOffsetLeftX = -moveOffsetX;
      moveOffsetLeftY = -moveOffsetY;
      setOffsetX(moveOffsetX)
      setOffsetY(moveOffsetY)
      setOffsetLeftX(moveOffsetLeftX)
      setOffsetLeftY(moveOffsetLeftY)
    } else {
      //双指缩放
      let xMove = e.touches[1].clientX - e.touches[0].clientX;
      let yMove = e.touches[1].clientY - e.touches[0].clientY;
      let distances = Math.sqrt(xMove * xMove + yMove * yMove);
      let distanceDiff = distances - distance;
      let newScale = scale + 0.005 * distanceDiff;
      // 缩放比例设置
      if (newScale <= 2.5 && newScale >= 1) {
        setDistance(distances)
        setScale(newScale)
      }
    }
  }

  // // 触摸结束事件,重置缩放状态
  const touchEnd = (e) => {
    if (e.touches.length == 0) {
      setZoom(false)
    }
  }
  return (
    <View className="utp-integral-rule-open-view">
      <Image
        src={data}
        mode="widthFix"
        className="utp-integral-rule-open-view-img"
        ontouchstart={(e) => touchStart(e)}
        ontouchmove={(e) => touchMove(e)}
        ontouchend={(e) => touchEnd(e)}
        style={{width: 100 * scale + '%', height: 100 * scale + '%'}}
      />
    </View>
    
  );
};

Index.defaultProps = {};

export default Index;
