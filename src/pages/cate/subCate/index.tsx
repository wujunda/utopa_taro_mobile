import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import ItemCateGoods from '../../../components/ItemCateGoods';
import ItemCateAd from '../../../components/ItemCateAd';
import Wrap from '../../../components/Wrap';
import Img from '../../../components/Img';
import useRequest from '../../../hooks/useRequest';
import { ISubCate, AdvertisementList, ISubCategoryList, getSubCate } from '../../../services/cate';
import './index.scss';
import Loading from '../../../components/Loading';
import Router from '../../../utils/router';

interface Props {
  id: number;
  id1: number;
}
const Index: Taro.FC<Props> = ({ id1 }) => {
  // @ts-ignore

  const [subState, update, loading] = useRequest<ISubCate>(!!id1 && id1.toString(), getSubCate);

  var ads: AdvertisementList[] = [];
  if (
    !!subState &&
    !!subState.data &&
    !!subState.data.bannerAdvertisement &&
    !!subState.data.bannerAdvertisement.advertisementList
  ) {
    ads = subState.data.bannerAdvertisement.advertisementList;
  }
  var pinpai: AdvertisementList[] = [];
  if (
    !!subState &&
    !!subState.data &&
    !!subState.data.recommendAdvertisement &&
    !!subState.data.recommendAdvertisement.advertisementList
  ) {
    pinpai = subState.data.recommendAdvertisement.advertisementList;
  }
  var nav: ISubCategoryList[] = [];
  if (!!subState && !!subState.data && !!subState.data.subCategoryList) {
    nav = subState.data.subCategoryList;
  }

  return (
    <View>
      {loading && <Loading />}
      {!!ads && (
        <View>
          <Wrap>
            {ads.map((item, index) => {
              if (index === 0) {
                return (
                  <View
                    key={item.image}
                    onClick={() => {
                      Router.goHome(item);
                    }}
                  >
                    <Img src={item.image} width={544} height={180} />
                  </View>
                );
              }
            })}
          </Wrap>
        </View>
      )}
      {!!subState && (
        <View>
          <View className="utp-cate-menu">
            <Text className="utp-txt-menu">推荐品牌</Text>
          </View>
          <View className="utp-cate-ads">
            <Wrap justifyContent="flex-start">
              {pinpai.map((item, index) => {
                return (
                  <View
                    style={{ marginRight: Taro.pxTransform(10) }}
                    key={index}
                    onClick={() => {
                      Router.goHome(item);
                    }}
                  >
                    <ItemCateAd
                      key={index}
                      src={item.image}
                      tips=""
                      onClick={() => {
                        let temp = JSON.parse(item.params);
                        if (!temp) {
                          return;
                        }
                        Router.goStore(temp.storeType, temp.storeId, temp.businessId);
                      }}
                    />
                  </View>
                );
              })}
            </Wrap>
          </View>
        </View>
      )}

      {nav.map((item) => {
        if (item.productCount === 0) {
          return;
        }
        return (
          <View key={item.id}>
            <View className="utp-cate-menu">
              <Text className="utp-txt-menu">{item.name}</Text>
            </View>
            <View className="utp-cate1-cates">
              <Wrap justifyContent="flex-start">
                {item.children.map((item1) => {
                  if (item1.productCount === 0) {
                    return;
                  }
                  return (
                    <View
                      key={item1.id}
                      style={{ marginRight: Taro.pxTransform(10) }}
                      onClick={() => {
                        Router.goSearch(item1.id, item1.name);
                      }}
                    >
                      <ItemCateGoods src={item1.pic} src1={item1.pic} tips={item1.name} />
                    </View>
                  );
                })}
              </Wrap>
            </View>
          </View>
        );
      })}
    </View>
  );
};
Index.defaultProps = {};
export default Index;
