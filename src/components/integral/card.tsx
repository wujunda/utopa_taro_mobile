import Taro from '@tarojs/taro';
import { View, Image } from '@tarojs/components';

import './card.scss';

interface DataProps {
  activityId: number;
  picUrl: string;
  price: number;
  exchangePoint: number;
  exchangePrice: number;
  name: string;
  skuId: number;
  storeId: number;
  productImage:string;
  productName:string;
  originPrice:number;
  exchangeType:number
}

interface IProps {
  data: DataProps;
}

const Index: Taro.FC<IProps> = ({ data }) => {
  const goDetail = (data) => {
    if (data && data.exchangeType === 1) {
      // 积分详情
      Taro.navigateTo({
        url: `/pages/details/index?id=${data.productId}&storeId=${data.storeId}&skuId=${data.skuId}&jifen=1`
      });
    } else if (data && data.exchangeType === 2) {
      // 优惠券详情
      if (data.exchangePrice) {
        Taro.navigateTo({
          url: `/pages/integral/exchangeCouponDetail/index?couponId=${data.productId}&activityId=${data.activityId}&storeId=${data.storeId}&businessId=${data.businessId}&exchangePoint=${data.exchangePoint}&exchangePrice=${data.exchangePrice}`
        });
      } else {
        Taro.navigateTo({
          url: `/pages/integral/exchangeCouponDetail/index?couponId=${data.productId}&activityId=${data.activityId}&storeId=${data.storeId}&businessId=${data.businessId}&exchangePoint=${data.exchangePoint}`
        });
      }
    } else if (data && data.exchangeType === 3) {
      // 抵用券详情
      if (data.exchangePrice) {
        Taro.navigateTo({
          url: `/pages/integral/exchangeVoucherDetail/index?goodsId=${data.productId}&shopId=${data.shopId}&shopGoodsId=${data.shopGoodId}&activityId=${data.activityId}&businessId=${data.businessId}&storeId=${data.storeId}&exchangePoint=${data.exchangePoint}&exchangePrice=${data.exchangePrice}`
        });
      } else {
        Taro.navigateTo({
          url: `/pages/integral/exchangeVoucherDetail/index?goodsId=${data.productId}&shopId=${data.shopId}&shopGoodsId=${data.shopGoodId}&activityId=${data.activityId}&businessId=${data.businessId}&storeId=${data.storeId}&exchangePoint=${data.exchangePoint}`
        });
      }
    }
  };
  return (
    <View className="utp-integral-card" onClick={() => goDetail(data)}>
      {
        data && data.productImage && 
        <Image className="utp-integral-card-img" src={data && data.productImage} />
      }
      {
        data && data.picUrl && 
        <Image className="utp-integral-card-img" src={data && data.picUrl} />
      }
      {
        data && data.productName && 
        <View className="utp-integral-card-name">{data && data.productName}</View>
      }
      {
        data && data.name && 
        <View className="utp-integral-card-name">{data && data.name}</View>
      }
      {
        data && data.exchangeType === 1 && data.exchangePrice != 0 && data.exchangePrice  &&
        <View>
          <View className="utp-integral-card-pointAndPrice">
            {data.exchangePoint}积分+{data.exchangePrice / 100}元
          </View>
          {
            data.originPrice && <View className="utp-integral-card-price">市场价:￥{data.originPrice/ 100}</View>
          }
          {
            data.price && <View className="utp-integral-card-price">市场价:￥{data.price/ 100}</View>
          }          
        </View>
      }
      {
        data && data.exchangeType === 1 && (data.exchangePrice == 0 || !data.exchangePrice) &&
        <View>
          <View className="utp-integral-card-pointAndPrice">{data.exchangePoint}积分</View>
          {
            data.originPrice && <View className="utp-integral-card-price">市场价:￥{data.originPrice/ 100}</View>
          }
          {
            data.price && <View className="utp-integral-card-price">市场价:￥{data.price/ 100}</View>
          }
        </View>
      }
      {
        data && data.exchangeType === 2 && data.exchangePrice != 0 && data.exchangePrice &&
        <View>
          <View className="utp-integral-card-lable">优惠券</View>
          <View className="utp-integral-card-point">
            {data.exchangePoint}积分+{data.exchangePrice / 100}元
          </View>
        </View>
      }
      {
        data && data.exchangeType === 2 && (data.exchangePrice == 0 || !data.exchangePrice) &&
        <View>
          <View className="utp-integral-card-lable">优惠券</View>
          <View className="utp-integral-card-point">{data.exchangePoint}积分</View>
        </View>
      }
      {
        data && data.exchangeType === 3 && data.exchangePrice != 0 && data.exchangePrice  &&
        <View>
          <View className="utp-integral-card-lable">抵用券</View>
          <View className="utp-integral-card-point">
            {data.exchangePoint}积分+{data.exchangePrice / 100}元
          </View>
        </View>
      }
      {
        data && data.exchangeType === 3 && (data.exchangePrice == 0 || !data.exchangePrice) &&
        <View>
          <View className="utp-integral-card-lable">抵用券</View>
          <View className="utp-integral-card-point">{data.exchangePoint}积分</View>
        </View>
      }
    </View>
  );
};

Index.defaultProps = {};
export default Index;
