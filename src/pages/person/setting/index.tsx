import Taro from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
import useStore from '../../../components/useNum';
import { connect } from '@tarojs/redux';
// import useRequest from '../../../hooks/useRequest';
// import { IUse, getItem } from '../../../services/user';

import Box1 from '../../../components/Box1';
import More from '../../../components/More';
import Cnt from '../../../components/Cnt';
import Txt from '../../../components/Txt';
import Router from '../../../utils/router';
import './index.scss';

function Index(props) {
  let num = useStore({ name: '' })[0];
  console.log('jjjjj');
  console.log(num);
  console.log('props');
  console.log(props);
  // const [state, update, loading] = useRequest<IUse>('name=a&age=1', getItem);
  // console.log('更多');
  // console.log(state);
  // console.log(loading);
  // console.log(update);

  return (
    <View>
      <View className="utp-setting-box">
        <Box1 type={1}>
          <View
            onClick={() => {
              Taro.navigateTo({
                url: '/pages/person/account/index'
              });
            }}
          >
            <More title="账号与密码设置" />
          </View>
          {false && <More title="支付设置" />}
        </Box1>
      </View>
      {false && (
        <View className="utp-setting-box">
          <Box1 type={1}>
            <More title="关于优托邦" />
            <More title="清除缓存" type={1} />
          </Box1>
        </View>
      )}
      <View
        className="utp-setting-btns"
        onClick={() => {
          props.logout();
          Router.goBack();
        }}
      >
        <Cnt>
          <Txt title="退出登陆" size={30} color="deep" />
        </Cnt>
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '设置',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};
Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {};
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    logout: (payload) => {
      Taro.setStorage({
        key: 'isLogin',
        data: ''
      });

      dispatch({
        type: 'cart/logout',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
