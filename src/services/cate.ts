import request from '../utils/request';

// 一级分类
export interface Cate {
  name: string;
  id: number;
}

export interface ICate {
  code: number;
  data: {
    list: Cate[];
  };
}
// 二级分类
export interface ISubCate {
  code: number;
  data: {
    // 子分类
    subCategoryList: ISubCategoryList[];
    recommendAdvertisement: ISubCategoryList;
    bannerAdvertisement: BannerAdvertisement;
  };
}
export interface BannerAdvertisement {
  advertisementList: AdvertisementList[];
}
// 子分类
export interface ISubCategoryListItem {
  id: number;
  name: string;
  pic: string;
}
export interface ISubCategoryList {
  name: string;
  id: number;
  HTML: any;
  children: ISubCategoryListItem[];
}
// 推荐品牌
export interface ISubCategoryList {
  name: string;
  title: string;
  code: string;
  advertisementList: AdvertisementList[];
}
export interface AdvertisementList {
  image: string;
  params: string;
}

// 一级分类
export const getCate = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.platformForeCategory.getFirstCategory',
    ver: '1.0',
    params: JSON.stringify({ productId: '6925123', storeId: '681169' })
  });
};
// 二级级分类
export const getSubCate = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.platformForeCategory.getFirstSubCategory',
    ver: '1.0',
    params: JSON.stringify({ clientType: '1', foreCategoryId: params })
  });
};
