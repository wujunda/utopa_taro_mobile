const env = require('./getEnv');

module.exports = {
  env: {
    ...env,
    NODE_ENV: '"development"',
  },
  defineConstants: {},
  mini: {},
  h5: {}
};
