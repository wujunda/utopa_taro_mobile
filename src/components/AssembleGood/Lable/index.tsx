// import { ComponentClass } from "react";
import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import Txt from '../../Txt';
// import Safe from '../../safe'
// import Bton from '../../Bton'
import Settimeout from '../../Setimeout';
import Cnt from '../../Cnt';
import Img from '../../Img';
import Wrap from '../../Wrap';
import logo from '../../../assets/assemble/logo.png';
import './index.scss';

// #region 书写注意
//
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

/**
 * 指定config的类型声明为: Taro.Config
 *
 * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
 * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
 * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
 */
// pages/details/index 商品详情拼团入口
interface IProps {
  show?: boolean;
}
const Index: Taro.FC<IProps> = ({ show = true }) => {
  return (
    <View className="utp-div utp-asslable">
      <Wrap type={2}>
        <Txt title="395" color="red" size={28} />
        <Txt title="人正在拼团，赶紧参与吧" color="deep" size={28} />
      </Wrap>
      {[1, 2, 3].map(i => {
        return (
          <View className="utp-asslable-item" key={i}>
            <Img src={logo} width={88} />
            <View className="utp-asslable-center">
              <Wrap type={2}>
                <Txt title="还差" size={28} color="deep" bold />
                <Txt title="1人" size={28} color="red" bold />
                <Txt title="成团" size={28} color="deep" bold />
              </Wrap>
              <Wrap type={2}>
                <Settimeout seconds={89888} type={1} />
              </Wrap>
            </View>
            <View className="utp-asslable-btn">
              <Cnt>
                <Txt title="去参团" size={28} color="white" />
              </Cnt>
            </View>
          </View>
        );
      })}
    </View>
  );
};

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion
Index.defaultProps = {};

export default Index;
