import Taro from '@tarojs/taro';
import { View, ScrollView, Text } from '@tarojs/components';

import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Bar from '../../../components/bar';
import Tag from '../../../components/Tag';
import Img from '../../../components/Img';
import Score from '../../../components/Score';

import goods from '../../../assets/goods.png';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <Bar title="店铺分类" />
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-freePlayList"
            scrollY
          >
            <View className="utp-freePlayList-goodsInfo-Wrap">
              <View className="utp-freePlayList-goodsInfo">
                <Wrap type={2} Myheight={220}>
                  <View>
                    <Img src={goods} width={180}></Img>
                  </View>
                  <View className="utp-freePlayList-titlesBox">
                    <Wrap type={3} top flexDirection="column">
                      <View>
                        <Text className="utp-freePlayList-titles">
                          muji 衬衫男士长袖正装修身衣服灰色衬衣商务休闲寸衫
                        </Text>
                      </View>
                      <View>
                        <Wrap>
                          <Text className="utp-freePlayList-renqi">人气值：</Text>
                          <Score num={4} width={24} />
                        </Wrap>
                      </View>
                      <View className="utp-freePlayList-positionTitle-Box">
                        <Text className="utp-freePlayList-positionTitle">
                          广州市天河区高德置地夏广场2203室进…
                        </Text>
                      </View>
                    </Wrap>
                  </View>
                </Wrap>
                <View className="utp-freePlayList-goodsInfo-freeCard">
                  <View className="utp-freePlayList-goodsInfo-freeCard-tagBox">
                    <View className="utp-freePlayList-goodsInfo-freeCard-tag">
                      <Tag>免费</Tag>
                    </View>
                    <Text className="utp-freePlayList-goodsInfo-freeCard-title taro-text">
                      {'\t'}
                      分享10人可享0元免费体验造型1次
                    </Text>
                  </View>
                  <View className="utp-freePlayList-goodsInfo-freeCard-tagBox">
                    <View className="utp-freePlayList-goodsInfo-freeCard-tag">
                      <Tag>免费</Tag>
                    </View>
                    <Text className="utp-freePlayList-goodsInfo-freeCard-title taro-text">
                      {'\t'}
                      分享10人可享0元免费体验造型1次
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};
export default Index;
