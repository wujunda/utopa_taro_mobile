import request from '../utils/request';
// 广告
export interface IAd {
  groupName: string;
  groupDetails: {
    name: string;
    image: string;
    width: number;
    height: number;
    command: any;
  }[];
}
export interface IAdData {
  code: number;
  data: {
    list: IAd[];
  };
}

// 子分类
export interface ICateSub {
  name: string;
  id: number;
  image: string;
}
export interface ICateSubData {
  code: number;
  data: {
    list: ICateSub[];
  };
}
// 分类
export interface ICate {
  name: string;
  id: number;
}
export interface ICateData {
  code: number;
  data: {
    list: ICate[];
  };
}

export interface ILiberBothData {
  code: number;
  data: {
    list: ILiberBoth[];
  };
}
export interface ILiberBoth {
  firstLetter: string;
  title: string;
  key: string;
  items: any[];
  brandList: {
    name: string;
  }[];
}

export interface ILiberData {
  code: number;
  data: {
    banners: ILiber[];
    classes: IBar[];
  };
}
export interface IBar {
  name: string;
  image: string;
  command: any;
  index: number;
}
export interface ILiber {
  image: string;
  command: any;
  height: number;
  width: number;
}
export const getLiberAd = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.special.getThemeAdvert',
    ver: '1.0',
    params: JSON.stringify({ clientType: '1', id: params })
  });
};

export const getCate = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.special.getCategory',
    ver: '1.0',
    params: JSON.stringify({ clientType: '1', themeId: params })
  });
};
export const getCateSub = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.special.getBrand',
    ver: '1.0',
    params: JSON.stringify({ clientType: '1', categoryId: params })
  });
};
// 一级分类
export const getLiberBoth = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.special.getThemeBrandIndex',
    ver: '1.0',
    params: JSON.stringify({ clientType: '1', id: params })
  });
};
// 二级级分类
export const getLiber = (id: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.special.getBaseInfo',
    ver: '1.0',
    params: JSON.stringify({ clientType: '1', id: id })
  });
};
