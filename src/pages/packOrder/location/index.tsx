import Taro, { useRouter, useState, useEffect, useDidShow } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { Picker, View, Input } from '@tarojs/components';
import useStore from '../../../components/useNum';
import { connect } from '@tarojs/redux';
import Wrap from '../../../components/Wrap';
import Img from '../../../components/Img';
import Box1 from '../../../components/Box1';
import Txt from '../../../components/Txt';
import Cnt from '../../../components/Cnt';
import more from '../../../assets/location/more.png';
import './index.scss';
import { IAds, ISsq, getSsq } from '../../../services/address';
import useRequest from '../../../hooks/useRequest';
import useSession from '../../../hooks/useSession';
import useLogin from '../../../hooks/useLogin';
import useResetCount from '../../../hooks/useResetCount';
import Router from '../../../utils/router';

declare let BMap: any, BMAP_STATUS_SUCCESS: any;

var debounce = require('lodash/debounce');
var QQMapWX = require('../../../utils/qqmap-wx-jssdk.min.js');

var qqmapsdk;

function Index(props) {
  let num = useStore({ name: '' })[0];
  let id = useRouter().params.id;
  console.log('jjjjj');
  console.log(num);
  console.log('props');
  console.log(props);
  const [search, setSearch] = useState('');
  useEffect(() => {
    if (search) {
      moreOne(() => {
        onSearch();
      });
    }
  }, [search]);
  const [demo, setDemo] = useState(0);
  useEffect(() => {
    console.log('demob变了');
  }, [demo]);
  const [state, setState] = useState<any>({
    selector: [],
    selectorChecked: []
  });
  const [haveSession] = useSession();
  const [isLogin, updateLogin, lodingLoading] = useLogin();
  const [canL, updateL] = useResetCount();
  if (haveSession && !isLogin && !lodingLoading) {
    if (updateLogin) {
      if (canL) {
        updateLogin();
        updateL();
      }
    }
  }
  console.warn('是否登陆');
  console.warn(isLogin);
  // 用户数据
  const [ssq, update, loading] = useRequest<ISsq>('name=a&age=1', getSsq);
  const [can, update1] = useResetCount();
  if (
    haveSession &&
    isLogin &&
    (ssq === null || (ssq != null && ssq.code !== 0)) &&
    update &&
    !loading
  ) {
    if (can) {
      update();
      update1();
      console.log(ssq);
    }
  }

  // 选择省市区
  const [crtSsq, setCrtSsq] = useState('');
  console.log(crtSsq);
  const [objs, setObjs] = useState<any>([]);
  let [crt, setCrt] = useState(-1);
  let [address, setAddress] = useState<IAds[]>([]);
  const onColumnChange = (e) => {
    console.log(e);
    let obj = Object.assign({}, state);
    if (ssq) {
      ssq.data.areas[0].children.map((item, index) => {
        // 改变第二列
        if (e.detail.column === 0 && index === e.detail.value) {
          obj.selector[1] = item.children;
          obj.selector[2] = item.children[0].children;
          setCrt(index);
        }
        // 改变第三列
        console.log(333);
        console.log(crt);
        if (e.detail.column === 1 && crt === index) {
          console.log(obj.selectorChecked);
          item.children.map((item1, index1) => {
            if (index1 === e.detail.value) {
              obj.selector[2] = item1.children;
            }
          });
        }
      });
      setState(obj);
    }
  };
  if (state.selector.length === 0 && ssq != null && ssq.code === 0) {
    let obj = Object.assign({}, state);
    obj.selector[0] = ssq.data.areas[0].children;
    obj.selector[1] = ssq.data.areas[0].children[0].children;
    obj.selector[2] = ssq.data.areas[0].children[0].children[0].children;
  }
  const [tips, setTips] = useState('');

  const onChange = (e) => {
    console.log(e);
    let obj = Object.assign({}, state);
    obj.selectorChecked = e.detail.value;
    setState(obj);
    let arr: any = [];
    arr.push(state.selector[0][e.detail.value[0]]);
    arr.push(state.selector[1][e.detail.value[1]]);
    arr.push(state.selector[2][e.detail.value[2]]);
    let str =
      state.selector[0][e.detail.value[0]].name +
      ' ' +
      state.selector[1][e.detail.value[1]].name +
      ' ' +
      state.selector[2][e.detail.value[2]].name;

    setCrtSsq(str);
    setObjs(arr);
    setTips(arr[2].name);
    Taro.setStorage({
      key: 'LoctionObj',
      data: arr
    });

    Taro.setStorage({
      key: 'LoctionStr',
      data: str
    });
  };
  useDidShow(() => {
    var str = Taro.getStorageSync('LoctionStr');
    if (str) {
      var arr = Taro.getStorageSync('LoctionObj');
      setCrtSsq(str);
      setObjs(arr);
      setTips(arr[2].name);
    }
  });
  const onSearch = () => {
    let arr = [objs[1].name, objs[2].name, search];
    if (process.env.TARO_ENV === 'weapp') {
      qqmapsdk = new QQMapWX({
        key: 'AUYBZ-3XDKD-YYF4H-HM3CM-7BBTO-FHBZY'
      });
      qqmapsdk.search({
        keyword: search,
        region: [objs[1].name, objs[2].name].join(''),
        success: function (res) {
          console.log('成功');
          console.log(res);
          if (res.message === 'query ok') {
            const s: any = [];
            res.data.map((item) => {
              item.point = item.location;
              s.push(item);
            });
            console.log(s);
            setAddress(s);
          }
        },
        fail: function (res) {
          console.log('失败');
          console.log(res);
        },
        complete: function (res) {
          console.log('完成');
          console.log(res);
        }
      });
      return;
    }

    let local = new BMap.LocalSearch(objs[1].name, {
      // 智能搜索
      renderOptions: { autoViewport: true }
    });
    local.search(arr.join(''));
    local.setSearchCompleteCallback((respone) => {
      console.log(respone);
      if (local.getStatus() === BMAP_STATUS_SUCCESS) {
        const s: any = [];
        for (let i = 0; i < respone.getCurrentNumPois(); i++) {
          s.push(respone.getPoi(i));
        }
        console.log(s);
        setAddress(s);
      }
    });
  };
  console.log('列表数据');
  console.log(address);

  return (
    <View className="utp-location">
      <View className="utp-location-hd">
        <View className="utp-location-sch">
          <Wrap type={1}>
            <Picker
              mode="multiSelector"
              value={state.selectorChecked}
              range={state.selector}
              rangeKey="name"
              onChange={onChange}
              onColumnChange={onColumnChange}
            >
              <Wrap>
                <Txt title={tips} size={26} />
                <View className="utp-location-sch-icos">
                  <Cnt>
                    <Img src={more} width={24} />
                  </Cnt>
                </View>
              </Wrap>
            </Picker>
            <View className="utp-location-sch-ipts">
              <Wrap type={1}>
                <Input
                  value={search}
                  className="utp-location-sch-ipt"
                  type="text"
                  placeholder="搜索写字楼、小区、学校"
                  focus
                  onInput={(e) => {
                    setSearch(e.detail.value);
                  }}
                  onConfirm={onSearch}
                />
              </Wrap>
            </View>
          </Wrap>
        </View>
      </View>
      <View className="utp-location-bd">
        <Box1>
          {address.map((item) => {
            return (
              <View
                key={item.title}
                className="utp-location-item utp-solid"
                onClick={() => {
                  if (id === '1') {
                    props.saveAdsTitle({
                      adsTitle: item.title
                    });
                  } else {
                    Taro.setStorage({
                      key: 'LoctionAds',
                      data: item
                    });
                  }
                  Router.goBack();
                }}
              >
                <View className="utp-location-item-txts">
                  <Wrap type={1}>
                    <Txt title={item.title} size={28} color="deep" dian />
                  </Wrap>
                </View>

                <View className="utp-location-item-txts">
                  <Wrap type={1}>
                    <Txt title={item.address} size={26} dian color="low" />
                  </Wrap>
                </View>
              </View>
            );
          })}
        </Box1>
      </View>
      <View
        className="utp-location-ft"
        onClick={() => {
          setDemo(demo + 1);
        }}
      ></View>
    </View>
  );
}

let moreOne = debounce((cb: any) => {
  cb();
}, 300);

Index.config = {
  navigationBarTitleText: '定位管理',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};
// @ts-ignore

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    post: state.cart.post
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    getSubmit: (payload) => {
      dispatch({
        type: 'cart/getSubmit',
        payload
      });
    },
    saveAdsTitle: (payload) => {
      dispatch({
        type: 'cart/saveAdsTitle',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
