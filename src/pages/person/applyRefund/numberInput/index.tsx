/* eslint-disable import/no-duplicates */
import Taro, { useState, useEffect } from '@tarojs/taro';
import { View, Image, Text, Input } from '@tarojs/components';
import './index.scss';
import { images } from '../../../../images';
import icon_add from '../../../../assets/assemble/add.png';
import icon_minus from '../../../../assets/assemble/minus.png';

interface IProps {
  onInput?: (qty: number) => void;
  add?: (qty: number) => void;
  reduce?: (qty: number) => void;
  qty?: number;
  min?: number;
  max?: number;
  isInput?: boolean;
  pushData?: (d: number) => void;
}

const Index: Taro.FC<IProps> = ({ onInput, add, reduce, qty = 0, max, min, isInput = true }) => {
  const [num, setNum] = useState<any>(qty);
  useEffect(() => {
    setNum(qty);
  }, [qty]);
  return (
    <View className="input-num">
      <View className="flex utp-step utp-div">
        <View
          className="flex utp-step-minus"
          onClick={() => {
            let o = num <= 0 ? 0 : num - 1;
            let minNum = min && o < min ? min : o;
            minNum = Number(minNum.toFixed(2));
            let minZero = minNum < 0 ? 0 : minNum;
            if (minNum >= 0) {
              setNum(minNum);
            }
            reduce && reduce(minZero);
            onInput && onInput(minZero);
          }}
        >
          <Image src={icon_minus} className="utp-step-img" />
        </View>
        <Text className="flex utp-step-num" style={{ width: Taro.pxTransform(135) }}>
          <Input
            style={{ margin: '0 10px 0 10px' }}
            disabled={!isInput}
            value={num as any}
            onBlur={(v) => {
              console.log(v);
            }}
            onInput={(e) => {
              let o: any = e.detail.value;
              if (max && Number(e.detail.value) > max) {
                o = max;
              }
              setNum(o);
              onInput && onInput(o as any);
            }}
            className="input"
            type="number"
          ></Input>
        </Text>
        <View className="flex  utp-step-add">
          <Image
            src={icon_add}
            onClick={() => {
              let o = num + 1;
              let maxNum = max === 0 || (max && max < o) ? max : o;
              setNum(maxNum);
              add && add(maxNum);
              onInput && onInput(maxNum);
            }}
            className="utp-step-img"
          />
        </View>
      </View>
    </View>
  );

  // const del = qty;
  // console.log(qty, del);
};
Index.defaultProps = {};
// @ts-ignore
export default Index;
