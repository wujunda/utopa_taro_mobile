import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import './index.scss';
import Tag from '../Tag';
import Yuan from '../Yuan';
import add1 from '../../assets/cart/add1.png';

interface dataProps {
  productImage: string;
  activeLabel: number;
  productName: string;
  storeId: number;
  salePrice: number;
  originPrice: number;
  productId: number;
  skuId: number;
  businessId: number;
}
type PageStateProps = {
  cart: any;
  item: ProductBaiscData;
};
type PageDispatchProps = {
  data: dataProps;
  addToCart: (goodsInfo) => void;
};
type IProps = PageStateProps & PageDispatchProps;
const Index: Taro.FC<IProps> = (props) => {
  const { data } = props;
  return (
    <View className="utp-shopGoods utp-div">
      <View className="utp-shopGoods-i utp-cnt">
        <Image className="utp-shopGoods-img" src={data && data.productImage} />
      </View>
      <View className="utp-shopGoods-item">
        {/* <Wrap type={1} flexDirection="column"> */}
        <View className="utp-shopGoods-titles">
          <Text className="utp-shopGoods-title taro-text">{data && data.productName}</Text>
        </View>
        <View
          className="utp-shopGoods-miaosha"
          style={{ display: data && data.activeLabel ? 'block' : 'none' }}
        >
          <Tag type={1}>{data && data.activeLabel}</Tag>
        </View>
        <View className="utp-shopGoods-ft">
          <View className="utp-shopGoods-moneys">
            <View className="utp-shopGoods-money">
              <Yuan price={data && data.salePrice} size={20} color="#FF2F7B"></Yuan>
            </View>
            {data && data.originPrice ? (
              <View className="utp-shopGoods-money-1">
                <Yuan size={20} price={data && data.originPrice} color='#999'></Yuan>
              </View>
            ):null}
          </View>

          <View
            className="utp-shopGoods-add utp-cnt"
            onClick={(e) => {
              e.stopPropagation();
              props.addToCart({
                need: true,
                productId: data.productId,
                number: 1,
                skuId: data.skuId,
                storeId: data.storeId,
                businessId: data.businessId
              });
            }}
          >
            <Image className="utp-shopGoods-add-ico" src={add1} />
          </View>
        </View>

        {/* <View className="utp-shopGoods-stores">
          <Wrap type={2}>
            <Text className="utp-shopGoods-store">优品特卖店优品特卖店</Text>
            <Text className="utp-shopGoods-toStore">进店 ></Text>
          </Wrap>
        </View> */}
        {/* </Wrap> */}
      </View>
    </View>
  );
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    cart: state.cart
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    addToCart: (payload) => {
      dispatch({
        type: 'cart/addCart',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
