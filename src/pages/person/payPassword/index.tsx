import Taro, { useState } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View, Input } from '@tarojs/components';
import { connect } from '@tarojs/redux';
// import useRequest from '../../../hooks/useRequest';
// import { IUse, getItem } from '../../../services/user';

import Img from '../../../components/Img';
import Bton from '../../../components/Bton';
import { images } from '../../../images';
import Txt from '../../../components/Txt';
import eye from '@/assets/eye.png';
import eye1 from '@/assets/eye2.png';
import Wrap from '../../../components/Wrap';
import './index.scss';
import { getResetPay } from '../../../services/user';
import Router from '../../../utils/router';

function Index({}) {
  const [focus, setFocus] = useState(false);
  const [focus1, setFocus1] = useState(false);
  const [focus2, setFocus2] = useState(false);
  const [password, setPassword] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  console.log(focus);
  const onNext = async () => {
    let data = await getResetPay({
      loginPassword: password,
      newPayPassword: password1
    });
    if (data.code === 0) {
      Taro.showToast({
        title: '修改成功'
      });
      setTimeout(() => {
        Router.goBack();
      }, 2000);
      //Router.goSetPassword(code, phone);
    } else {
      Taro.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
  };
  const [see, setSee] = useState(false);
  const [see1, setSee1] = useState(false);
  const [see2, setSee2] = useState(false);
  let next = false;
  if (password.length > 5 && password1.length > 5 && password1 === password2) {
    next = true;
  }

  return (
    <View className="utp-password">
      <View className="utp-password-box">
        <View
          className="utp-password-ipts"
          onClick={() => {
            setTimeout(() => {
              setFocus(true);
            }, 30);
          }}
        >
          <View className="utp-password-ipts-tips">
            <Txt title="登陆密码" size={26} color="deep" />
          </View>
          <View className="utp-password-ipts-ipt">
            <Input
              onInput={(e) => {
                let value = e.detail.value;
                setPassword(value);
              }}
              value={password}
              focus={focus}
              onFocus={() => {
                setFocus(true);
              }}
              password={!see}
              placeholder="请输入登陆密码"
            />
          </View>
          {focus && password && (
            <View
              className="utp-password-ipts-clo utp-cnt"
              onClick={() => {
                setPassword('');
              }}
            >
              <Img src={images.set_clo} width={28} />
            </View>
          )}

          <View
            className="utp-password-ipts-ico utp-cnt"
            onClick={() => {
              setSee(!see);
            }}
          >
            <Img src={see ? eye : eye1} width={48} />
          </View>
        </View>

        <View
          className="utp-password-ipts"
          onClick={() => {
            setTimeout(() => {
              setFocus1(true);
            }, 30);
          }}
        >
          <View className="utp-password-ipts-tips">
            <Txt title="新支付密码" size={26} color="deep" />
          </View>
          <View className="utp-password-ipts-ipt">
            <Input
              onInput={(e) => {
                let value = e.detail.value;
                setPassword1(value);
              }}
              value={password1}
              focus={focus1}
              onFocus={() => {
                setFocus1(true);
              }}
              password={!see1}
              placeholder="请输入新密码"
            />
          </View>
          {focus1 && password1 && (
            <View
              className="utp-password-ipts-clo utp-cnt"
              onClick={() => {
                setPassword1('');
              }}
            >
              <Img src={images.set_clo} width={28} />
            </View>
          )}

          <View
            className="utp-password-ipts-ico utp-cnt"
            onClick={() => {
              setSee1(!see1);
            }}
          >
            <Img src={see1 ? eye : eye1} width={48} />
          </View>
        </View>

        <View
          className="utp-password-ipts"
          onClick={() => {
            setTimeout(() => {
              setFocus2(true);
            }, 30);
          }}
        >
          <View className="utp-password-ipts-tips">
            <Txt title="确认新支付密码" size={26} color="deep" />
          </View>
          <View className="utp-password-ipts-ipt">
            <Input
              onInput={(e) => {
                let value = e.detail.value;
                setPassword2(value);
              }}
              value={password2}
              focus={focus2}
              onFocus={() => {
                setFocus2(true);
              }}
              password={!see2}
              placeholder="请重复输入新密码"
            />
          </View>
          {focus2 && password2 && (
            <View
              className="utp-password-ipts-clo utp-cnt"
              onClick={() => {
                setPassword2('');
              }}
            >
              <Img src={images.set_clo} width={28} />
            </View>
          )}

          <View
            className="utp-password-ipts-ico utp-cnt"
            onClick={() => {
              setSee2(!see2);
            }}
          >
            <Img src={see2 ? eye : eye1} width={48} />
          </View>
        </View>
      </View>
      <View className="utp-password-box">
        {!next && (
          <Bton type={'none'} size="mini">
            确定
          </Bton>
        )}
        {next && (
          <View onClick={onNext}>
            <Bton size="mini">确定</Bton>
          </View>
        )}
        <View
          style={{ marginTop: Taro.pxTransform(30) }}
          onClick={() => {
            Router.goPassword();
          }}
        >
          <Wrap justifyContent="space-between">
            <View></View>
            <Txt title="忘记密码？" size={24} />
          </Wrap>
        </View>
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '设置支付密码',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff'
};
// @ts-ignore
export default connect((state) => state)(Index);
