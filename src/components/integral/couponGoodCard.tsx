
import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import icon from '../../assets/cart_icon_forward_gray@2x.png'
import sellOut from '../../assets/sell_out.png'
import cartIcon from '../../assets/cart/add1.png'
import JSBridge from '../../utils/jsbridge/index';
import { goAddCart } from '../../services/submit'
import './couponGoodCard.scss';

interface DateProps {
  businessId: number;
  storeId:number;
  saleStatus: number;
  selfSupport: number;
  productName: string;
  salePrice: number;
  originPrice: number;
  activeLabel: string;
  storeName: string;
}

interface IProps {
  data: DataProps;
  isApp: boolean;
}

const Index: Taro.FC<IProps> = ({data, isApp}) => {
  // eslint-disable-next-line no-shadow
  const goDetail = (e, data) => {
    e.stopPropagation()
    if (isApp) {
      const query = {
        productId: data.productId,
        storeId: data.storeId,
        skuId: data.skuId,
      };
      const cmdParams = {
        cmd: 'C0020030000',
        params: JSON.stringify(query)
      };
      JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, res => {
        // console.log(res)
      });
    } else {
       Taro.navigateTo({
        url: `/pages/details/index?id=${data.productId}&storeId=${data.storeId}&skuId=${data.skuId}`
      });
    }
  }
  // eslint-disable-next-line no-shadow
  const goStore = (e, data) => {
    e.stopPropagation()
    if(isApp) {
      const cmdParam = {
        cmd: data.storeCmd,
        params: data.storeCommand.params
      };
      JSBridge.Common.GTBridge_Common_Base_CMD(cmdParam, res => {
        // console.log(res)
      });
    } else {
      Taro.navigateTo({
        url: `/pages/store/shoppingStore/index?businessId=${data.businessId}&storeId=${data.storeId}`
      });
    }
  }

  const addCart = (e, data) => {
    e.stopPropagation()
    const params = {
      businessId: data.businessId,
      productId: data.productId,
      storeId: data.storeId,
      skuId: data.skuId,
      number: 1,
    }
    // console.log(11111111111111, '====isApp====')
    if (!Taro.getStorageSync('isLogin')) {
      if (isApp) {
        JSBridge.Common.GTBridge_Common_Base_goLogin();
      } else {
        Taro.navigateTo({
          url: '/pages/login/index'
        });
      }
    } else {
      goAddCart(params).then(res => {
        if (res.code == 0) {
          Taro.showToast({
            title: '添加到购物车成功',
            icon: 'none'
          })
        } else {
           Taro.showToast({
            title: res.msg || '添加到购物车失败',
            icon: 'none'
          })
        }
      })
    }
    
  } 
  return (
    <View className="utp-integral-couponCard" onClick={(e) => goDetail(e, data)}>
      {
        data && data.saleStatus === 4 ? <Image src={sellOut} className="utp-integral-couponCard-sellOut"/> : ''
      }
      <Image src={data && data.productImage} className="utp-integral-couponCard-img"/>
      <View className="utp-integral-couponCard-right">
        <View className="utp-integral-couponCard-right-name">
        { data && data.selfSupport == 1 ? <View className="utp-integral-couponCard-right-name-lable">自营</View> : '' }&nbsp;
        <View className="utp-integral-couponCard-right-name-message">{data && data.productName}</View>
        </View>
        <View className="utp-integral-couponCard-right-bottom">
          <View className="utp-integral-couponCard-right-bottom-price">
            <View className="utp-integral-couponCard-right-bottom-price-num">
              <View className="utp-integral-couponCard-right-price-num-icon">￥</View>
              <View>{data && data.salePrice / 100}</View>
            </View>
            { data && data.originPrice ? <View className="utp-integral-couponCard-right-bottom-price-originalNum">￥{data.originPrice / 100}</View> : '' }
            { data && data.activeLabel ? <View className="utp-integral-couponCard-right-bottom-price-activeLabel">{data.activeLabel}</View> : '' }
          </View>
          
          <View className="utp-integral-couponCard-right-bottom-store">
            <View className="utp-integral-couponCard-right-bottom-store-name">{data && data.storeName}</View>
            {
              isApp && 
              <View className="utp-integral-couponCard-right-bottom-store-open" onClick={(e) => goStore(e, data)}>进店 
                <Image src={icon} className="utp-integral-couponCard-right-bottom-store-open-img"/>
              </View>
            }
          </View>
        </View>
        <View className="utp-integral-couponCard-right-cartIcon" onClick={(e) => addCart(e, data)}>
          <Image src={cartIcon}  className="utp-integral-couponCard-right-cartIcon-img"/>
        </View>
      </View>
    </View>
  );
};

Index.defaultProps = {};
export default Index;