import Taro, { useState, useRouter } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View, Textarea } from '@tarojs/components';
import { connect } from '@tarojs/redux';

import Wrap from '../../../components/Wrap';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import Bton from '../../../components/Bton';
import './index.scss';
import Router from '../../../utils/router';
import icon_addphoto from '../../../assets/mine/icon_addphoto.png';
import { getToken, getSendMsg, getImgUrl } from '../../../services/upload';
import { images } from '../../../images';
import Qiniu from '../../../utils/qiniu';

function Index({}) {
  const router = useRouter();
  let id = router.params.id;
  const [ipt, setIpt] = useState('');
  const [imgs, setImgs] = useState<any>([]);
  const [imgsh5, setImgsh5] = useState<any>([]);
  const select = () => {
    Taro.chooseImage({
      success: (info) => {
        let temp = [...imgs];
        let temph5 = [...imgsh5];
        let arr = temp.concat(info.tempFilePaths);
        let arrh5 = temph5.concat([info.tempFiles[0].originalFileObj]);
        setImgs(arr);
        setImgsh5(arrh5);
      }
    });
  };
  const onSubmit = async () => {
    if (!ipt) {
      Taro.showToast({
        title: '你还没写问题呢~',
        icon: 'none'
      });
    }
    if (imgs.length === 0) {
      onDone();
      return;
    }
    const data = await getToken({});
    if (data.code === 0 && data.data && data.data.upToken) {
      Taro.showLoading({
        title: ''
      });
      let arr = imgs;
      if (process.env.TARO_ENV === 'h5') {
        arr = imgsh5;
      }
      arr.map((item) => {
        console.log('开始上传');
        console.log(data.data.upToken);
        console.log(item);
        Qiniu(data.data.upToken, item).then(
          async (hash) => {
            Taro.hideLoading();
            feedbackKeys.push(hash);

            if (feedbackKeys.length === imgs.length) {
              onDone();
            }
            let url = await getImgUrl(hash);
            console.log('图片');
            console.log(url);
          },
          () => {
            Taro.hideLoading();
            feedbackKeys = [];
          }
        );
      });
    } else {
      Taro.showToast({
        title: '获取token失败!',
        icon: 'none'
      });
    }
  };
  let feedbackKeys: any = [];
  const onDone = async () => {
    Taro.showLoading({
      title: ''
    });
    let data = await getSendMsg({
      feedbackKeys: feedbackKeys.join(','),
      adviceId: id,
      content: ipt,
      clientType: 1,
      sysId: 1
    });
    Taro.hideLoading();
    if (data.code === 0) {
      Taro.showToast({
        title: '你的反馈我们已经收到!',
        icon: 'none'
      });
      setTimeout(() => {
        Router.goBack();
      }, 2000);
    }
  };
  const onDel = (index) => {
    if (imgs.length > 0) {
      let arr: any = [].concat(imgs);
      arr.splice(index, 1);
      let arrh5: any = [].concat(imgsh5);
      arrh5.splice(index, 1);
      setImgs(arr);
      setImgsh5(arrh5);
    }
  };

  return (
    <View className="utp-sendMsg">
      <View className="utp-sendMsg-body">
        <View className="utp-sendMsg-ipts">
          <Textarea
            value={ipt}
            className="utp-sendMsg-ipts-ipt"
            maxlength={100}
            placeholder="写下优托邦的问题和需要解决的问题，优托邦将及时为你解决"
            onInput={(e) => {
              setIpt(e.detail.value);
            }}
          />
        </View>
        <View className="utp-sendMsg-tips">
          <Wrap justifyContent={'space-between'}>
            <View />
            <Txt title={`${ipt.length}/100`} />
          </Wrap>
        </View>
        <View className="utp-sendMsg-box">
          <Wrap justifyContent={'flex-start'}>
            {imgs.map((item, index) => {
              return (
                <View className="utp-sendMsg-imgs1" key={item}>
                  <View
                    className="utp-sendMsg-imgs1-del"
                    onClick={() => {
                      onDel(index);
                    }}
                  >
                    <Img src={images.sendMsg_clo} width={38} />
                  </View>
                  <View className="utp-sendMsg-imgs">
                    <Img src={item} width={201} />
                  </View>
                </View>
              );
            })}
            {imgs.length !== 6 && (
              <View className="utp-sendMsg-imgs1 utp-sendMsg-imgs1-1" onClick={select}>
                <View className="utp-sendMsg-imgs">
                  <Img src={icon_addphoto} width={201} />
                </View>
              </View>
            )}
          </Wrap>
        </View>
      </View>
      <View className="utp-sendMsg-ft" onClick={onSubmit}>
        <Bton>提交</Bton>
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '帮助反馈',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff'
};
// @ts-ignore
export default connect((state) => state)(Index);
