/* eslint-disable import/first */
import Taro from '@tarojs/taro';
import SameCity from './sameCity/index';
import Collage from './collage/index';
import { ZONE_TYPES } from './common/config';
import { View } from '@tarojs/components';

interface Props {
  uid: string;
}

export const Index: Taro.FC<Props> = (props) => {
  const type = 3;
  let components;
  switch (type) {
    case ZONE_TYPES.SAME_CITY: {
      components = <SameCity title="同城好货" context={props} />;
      break;
    }
    case ZONE_TYPES.COLLAGE: {
      components = <Collage title="拼团专区" context={props} />;
      break;
    }
    default: {
      components = <View>页面丢失</View>;
    }
  }
  return components;
};

export default Index;
