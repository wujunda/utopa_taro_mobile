import Taro, { useEffect, useState, useRouter } from '@tarojs/taro';
import { ScrollView, View } from '@tarojs/components';

import Wrap from '../../../components/Wrap';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import { connect } from '@tarojs/redux';
import useRequest from '../../../hooks/useRequest';
import Router from '../../../utils/router';
import {
  ICateSub,
  ICateSubData,
  ICate,
  ICateData,
  getCate,
  getCateSub
} from '../../../services/liber';
import './index.scss';

type PageStateProps = {};

type PageDispatchProps = {
  // addCart: (any) => void;
};
type IProps = PageStateProps & PageDispatchProps;
const Index: Taro.FC<IProps> = () => {
  const id = useRouter().params.id;

  const [state] = useRequest<ICateData>(id, getCate);

  // 分类
  let cate: ICate[] = [];
  if (state && state.code === 0) {
    cate = state.data.list;
  }

  // 获取子分类
  const [cateId, setCateId] = useState<number>(0);
  useEffect(() => {
    if (state && state.code === 0) {
      setCateId(state.data.list[0].id);
    }
  }, [state]);
  const [state1, update] = useRequest<ICateSubData>(cateId, getCateSub);
  useEffect(() => {
    if (cateId && update) {
      update();
    }
  }, [cateId]);
  let sub: ICateSub[] = [];
  if (state1 && state1.code === 0) {
    sub = state1.data.list;
  }

  return (
    <View className="uto-liberCate">
      <View className="uto-liberCate-sch utp-cnt">
        <View
          className="uto-liberCate-sch-box utp-cnt"
          onClick={() => {
            Router.goHome({
              cmd: 'C0040020203',
              events: null,
              h5Params: null,
              params: `{"zoneId":"20200427011","titleTxt":"品牌索引"}`,
              url: ''
            });
          }}
        >
          <Txt title="按索引查看全部" size={26} color="deep" />
        </View>
      </View>
      <View className="uto-liberCate-left">
        <ScrollView scrollY className="uto-liberCate-lefts">
          {cate.map((item) => {
            return (
              <View
                key={item.id}
                className="uto-liberCate-left-cates utp-cnt"
                onClick={() => {
                  setCateId(item.id);
                }}
              >
                <View
                  className={`uto-liberCate-left-cate utp-cnt ${
                    cateId === item.id ? 'uto-liberCate-left-cate-crt' : ''
                  }`}
                >
                  <Txt title={item.name} size={24} color={cateId === item.id ? 'deep' : 'normal'} />
                </View>
              </View>
            );
          })}
        </ScrollView>
      </View>
      <View className="uto-liberCate-right">
        <ScrollView scrollY className="uto-liberCate-lefts">
          <Wrap>
            {sub.map((item) => {
              return (
                <View
                  key={item.id}
                  className="uto-liberCate-right-sub"
                  onClick={() => {
                    Router.goSearch(item.id, item.name, true);
                  }}
                >
                  <Img src={item.image} width={160} />
                </View>
              );
            })}
          </Wrap>
        </ScrollView>
      </View>
    </View>
  );
};
Index.config = {
  navigationBarTitleText: '品牌分类',
  disableScroll: true,
  enablePullDownRefresh: false,
  backgroundColor: '#ffffff'
  // 不显示标题
  //navigationStyle: 'custom'
};

const mapStateToProps: PageStateProps = (state) => {
  return {
    number: state.cart.number,
    post: state.cart.post,
    cart: state.cart,
    loading: state.loading.effects['cart/addCart']
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};
// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
