import Taro, { useEffect } from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import './index.scss';

declare let QRCode: any;

interface Props {
  size: number;
  value: string;
}
const Index: Taro.FC<Props> = ({ size = 200, value }) => {
  useEffect(() => {
    var qrcode = new QRCode(document.getElementById('qrcode'), {
      text: value,
      width: size,
      height: size,
      colorDark: '#000000',
      colorLight: '#ffffff',
      correctLevel: QRCode.CorrectLevel.H
    });
  }, []);
  return <View id="qrcode"></View>;
};
Index.defaultProps = {};

export default Index;
