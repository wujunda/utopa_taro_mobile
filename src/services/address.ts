import request from '../utils/request';

export interface IAds {
  title: string;
  address: string;
  point: {
    lat: number;
    lng: number;
  };
}
export interface IAddressData {
  addrDetail: string;
  address: string;
  addressType: string;
  cityId: string;
  cityName: string;
  countryName: string;
  detailAddress: string;
  districtId: string;
  districtName: string;
  latitude: string;
  longitude: string;
  mapAddress: string;
  phone: string;
  prefecture: string;
  provinceId: string;
  provinceName: string;
  userName: string;
  id: number;
  isDefault: number;
  isInRange: number;
  isInScope: number;
  optimistic: number;
  state: number;
  perfected: boolean;
  storeId: number;
}
export interface IAddress {
  code: number;
  data: {
    address: IAddressData[];
  };
}
interface IChildren {
  code: string;
  name: string;
  children: IChildren[];
}

export interface ISsq {
  code: number;
  data: {
    areas: {
      name: string;
      children: IChildren[];
    }[];
  };
}
export const getSsq = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'base.area.getAreaTreeByParent',
    ver: '1.0',
    params: JSON.stringify({})
  });
};
export const getAddAddress = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'utopa.address.addAddress',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
export const getEditAddress = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'utopa.address.updateAddress',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
export const getDelAddress = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'utopa.address.deleteAddress',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};

export const getAddress = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'utopa.address.getAddressListByStoreId',
    ver: '1.0',
    params: JSON.stringify({})
  });
};
