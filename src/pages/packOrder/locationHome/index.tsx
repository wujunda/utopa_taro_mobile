import Taro, { useState, useEffect, useDidShow } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { Text, Picker, View, Input } from '@tarojs/components';
import useStore from '../../../components/useNum';
import { connect } from '@tarojs/redux';
import Wrap from '../../../components/Wrap';
import Img from '../../../components/Img';
import Box1 from '../../../components/Box1';
import Txt from '../../../components/Txt';
import Cnt from '../../../components/Cnt';
import more from '../../../assets/location/more.png';
import './index.scss';
import useRequest from '../../../hooks/useRequest';
import useSession from '../../../hooks/useSession';
import useLogin from '../../../hooks/useLogin';
import useResetCount from '../../../hooks/useResetCount';
import Router from '../../../utils/router';
import { IAds, ISsq, getSsq, IAddress, IAddressData, getAddress } from '../../../services/address';
import { images } from '../../../images';

declare let BMap: any, BMAP_STATUS_SUCCESS: any;

var debounce = require('lodash/debounce');
var QQMapWX = require('../../../utils/qqmap-wx-jssdk.min.js');

var qqmapsdk;

type PageStateProps = {
  loading: boolean;
  isLoagin: boolean;
  adsTitle: string;
  x: string;
  y: string;
};
type PageDispatchProps = {
  syncCart: () => void;
  saveAdsTitle: (any) => void;
  saveXy: (any) => void;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
const Index: Taro.FC<IProps> = ({ isLogin, saveXy, adsTitle, x, y, saveAdsTitle }) => {
  const [show, setShow] = useState(true);
  const [index1, setIndex] = useState(-1);
  let num = useStore({ name: '' })[0];
  console.log('jjjjj');
  console.log(num);
  const [search, setSearch] = useState('');
  useEffect(() => {
    if (search) {
      moreOne(() => {
        onSearch();
      });
    }
  }, [search]);
  const [ads, update3, loadingi3] = useRequest<IAddress>('name=a&age=1needLogin', getAddress);
  const [demo, setDemo] = useState(0);
  useEffect(() => {
    console.log('demob变了');
  }, [demo]);
  const [state, setState] = useState<any>({
    selector: [],
    selectorChecked: []
  });
  console.warn('是否登陆');
  console.warn(isLogin);
  // 用户数据
  const [ssq, update, loading] = useRequest<ISsq>('name=a&age=1', getSsq);
  const [can, update1] = useResetCount();
  if (isLogin && (ssq === null || (ssq != null && ssq.code !== 0)) && update && !loading) {
    if (can) {
      update();
      update1();
      console.log(ssq);
    }
  }

  // 选择省市区
  const [crtSsq, setCrtSsq] = useState('');
  console.log(crtSsq);
  const [objs, setObjs] = useState<any>([]);
  let [crt, setCrt] = useState(-1);
  let [address, setAddress] = useState<IAds[]>([]);
  const onColumnChange = (e) => {
    console.log(e);
    let obj = Object.assign({}, state);
    if (ssq) {
      ssq.data.areas[0].children.map((item, index) => {
        // 改变第二列
        if (e.detail.column === 0 && index === e.detail.value) {
          obj.selector[1] = item.children;
          obj.selector[2] = item.children[0].children;
          setCrt(index);
        }
        // 改变第三列
        console.log(333);
        console.log(crt);
        if (e.detail.column === 1 && crt === index) {
          console.log(obj.selectorChecked);
          item.children.map((item1, index1) => {
            if (index1 === e.detail.value) {
              obj.selector[2] = item1.children;
            }
          });
        }
      });
      setState(obj);
    }
  };
  if (state.selector.length === 0 && ssq != null && ssq.code === 0) {
    let obj = Object.assign({}, state);
    obj.selector[0] = ssq.data.areas[0].children;
    obj.selector[1] = ssq.data.areas[0].children[0].children;
    obj.selector[2] = ssq.data.areas[0].children[0].children[0].children;
  }
  const [tips, setTips] = useState('广州市');

  const onChange = (e) => {
    console.log(e);
    let obj = Object.assign({}, state);
    obj.selectorChecked = e.detail.value;
    setState(obj);
    let arr: any = [];
    arr.push(state.selector[0][e.detail.value[0]]);
    arr.push(state.selector[1][e.detail.value[1]]);
    arr.push(state.selector[2][e.detail.value[2]]);
    let str =
      state.selector[0][e.detail.value[0]].name +
      ' ' +
      state.selector[1][e.detail.value[1]].name +
      ' ' +
      state.selector[2][e.detail.value[2]].name;

    setCrtSsq(str);
    setObjs(arr);
    setTips(arr[2].name);
    Taro.setStorage({
      key: 'LoctionObj',
      data: arr
    });

    Taro.setStorage({
      key: 'LoctionStr',
      data: str
    });
  };
  const [init, setInit] = useState(false);
  useDidShow(() => {
    if (update3) {
      update3();
    }

    var str = Taro.getStorageSync('LoctionStr');
    if (str) {
      var arr = Taro.getStorageSync('LoctionObj');
      setCrtSsq(str);
      setObjs(arr);
      setTips(arr[2].name);
    }
    if (!init) {
      setInit(true);
      setTimeout(() => {
        if (x) {
          getXyAds();
        }
      }, 200);
    }
  });
  const onSearch = () => {
    let arr = [objs[1].name, objs[2].name, search];
    if (process.env.TARO_ENV === 'weapp') {
      qqmapsdk = new QQMapWX({
        key: 'AUYBZ-3XDKD-YYF4H-HM3CM-7BBTO-FHBZY'
      });
      qqmapsdk.search({
        keyword: search,
        region: [objs[1].name, objs[2].name].join(''),
        success: function (res) {
          console.log('成功');
          console.log(res);
          if (res.message === 'query ok') {
            const s: any = [];
            res.data.map((item) => {
              item.point = item.location;
              s.push(item);
            });
            console.log(s);
            setAddress(s);
          }
        },
        fail: function (res) {
          console.log('失败');
          console.log(res);
        },
        complete: function (res) {
          console.log('完成');
          console.log(res);
        }
      });
      return;
    }

    let local = new BMap.LocalSearch(objs[1].name, {
      // 智能搜索
      renderOptions: { autoViewport: true }
    });
    local.search(arr.join(''));
    local.setSearchCompleteCallback((respone) => {
      console.log(respone);
      if (local.getStatus() === BMAP_STATUS_SUCCESS) {
        const s: any = [];
        for (let i = 0; i < respone.getCurrentNumPois(); i++) {
          s.push(respone.getPoi(i));
        }
        console.log(s);
        setAddress(s);
      }
    });
  };
  console.log('列表数据');
  console.log(address);
  const [crtAds, setCrtAds] = useState<any>('');

  let adss: IAddressData[] = [];
  if (ads && ads.data && ads.data.address) {
    adss = ads.data.address;
  }
  const getXyAds = () => {
    console.log('坐标坐标');
    console.log(x);
    console.log(y);
    if (x) {
      if (process.env.TARO_ENV === 'weapp') {
        console.log('坐标坐标1');
        qqmapsdk = new QQMapWX({
          key: 'AUYBZ-3XDKD-YYF4H-HM3CM-7BBTO-FHBZY'
        });
        qqmapsdk.search({
          keyword: '餐饮',
          location: {
            latitude: x,
            longitude: y
          },
          success: function (res) {
            console.log('成功');
            console.log(res);
            if (res.message === 'query ok') {
              const s: any = [];
              res.data.map((item, index) => {
                if (index === 0) {
                  setCrtAds(item);
                  setTips(item.city);
                  saveAdsTitle({
                    adsTitle: item.address
                  });
                }
                item.point = item.location;
                s.push(item);
              });
              console.log(s);
              setAddress(s);
            }
          },
          fail: function (res) {
            console.log('失败');
            console.log(res);
          },
          complete: function (res) {
            console.log('完成');
            console.log(res);
          }
        });
        return;
      }
      let local = new BMap.LocalSearch('广州', {
        // 智能搜索
        renderOptions: { autoViewport: true }
      });
      local.search('酒店');
      console.log('搜索');
      local.setSearchCompleteCallback((respone) => {
        console.log(respone);
        if (local.getStatus() === BMAP_STATUS_SUCCESS) {
          const s: any = [];
          for (let i = 0; i < respone.getCurrentNumPois(); i++) {
            if (i === 0) {
              setCrtAds(respone.getPoi(i));
              setTips(respone.getPoi(i).city);

              saveAdsTitle({
                adsTitle: respone.getPoi(i).address
              });
            }
            s.push(respone.getPoi(i));
          }
          console.log(s);
          setAddress(s);
        }
      });
    }
  };

  return (
    <View className="utp-location">
      <View className="utp-location-hd">
        <View className="utp-location-sch">
          <Wrap type={1}>
            <Picker
              mode="multiSelector"
              value={state.selectorChecked}
              range={state.selector}
              rangeKey="name"
              onChange={onChange}
              onColumnChange={onColumnChange}
            >
              <Wrap>
                <Txt title={tips} size={26} />
                <View className="utp-location-sch-icos">
                  <Cnt>
                    <Img src={more} width={24} />
                  </Cnt>
                </View>
              </Wrap>
            </Picker>
            <View
              className="utp-location-sch-ipts"
              onClick={() => {
                Router.goAddressLocation(1);
              }}
            >
              <Wrap type={1}>
                <Input
                  disabled
                  value={search}
                  className="utp-location-sch-ipt"
                  type="text"
                  placeholder="搜索写字楼、小区、学校"
                  focus
                  onInput={(e) => {
                    setSearch(e.detail.value);
                  }}
                  onConfirm={onSearch}
                />
              </Wrap>
            </View>
          </Wrap>
        </View>
      </View>
      <View className="utp-location-bd">
        {show && (
          <View className="utp-location-bd-tips">
            <Wrap type={1} justifyContent="space-between">
              <Txt title="不同收货地址存在库存及配送差异，请选择准确收货地址" color="red" />
              <View
                onClick={() => {
                  setShow(false);
                }}
                style={{ marginLeft: Taro.pxTransform(0) }}
                className="utp-cnt"
              >
                <Img src={images.ads_close} width={20} />
              </View>
            </Wrap>
          </View>
        )}
        <View className="utp-location-bd-box">
          <View className="utp-location-bd-tits">
            <Wrap type={1} justifyContent="space-between">
              <Txt height={74} title="配送至" />
              <Wrap type={1}>
                <View style={{ marginRight: Taro.pxTransform(8) }}>
                  <Img src={images.update} width={34} />
                </View>
                {!x && (
                  <View
                    onClick={() => {
                      Taro.openSetting({
                        success: function (res) {
                          if (res.authSetting['scope.userLocation'] === true) {
                            Taro.getLocation({
                              type: 'wgs84',
                              success: function (res) {
                                const latitude = res.latitude;
                                const longitude = res.longitude;
                                console.log('获取定位');
                                console.log(latitude);
                                console.log(longitude);
                                saveXy({
                                  x: latitude,
                                  y: longitude
                                });
                              }
                            });
                            setTimeout(() => {
                              getXyAds();
                            }, 30);
                          }
                        }
                      });
                    }}
                  >
                    <Text className="utp-location-bd-txt">开启定位</Text>
                  </View>
                )}

                {!!x && (
                  <View
                    onClick={() => {
                      getXyAds();
                      setIndex(-1);
                    }}
                  >
                    <Text className="utp-location-bd-txt">重新定位</Text>
                  </View>
                )}
              </Wrap>
            </Wrap>
          </View>
          <View className="utp-location-bd-tits">
            <Wrap type={1}>
              <Txt
                title={x ? adsTitle : '获取位置失败，请开启定位后重试'}
                height={80}
                size={28}
                color="deep"
              />
            </Wrap>
          </View>
        </View>
        <View className="utp-location-bd-box utp-location-bd-box-1">
          <View className="utp-location-bd-tits">
            <Wrap type={1} justifyContent="space-between">
              <Txt title="我的收货地址" height={74} />
              <Wrap type={1}>
                <View style={{ marginRight: Taro.pxTransform(8) }}>
                  <Img src={images.add_ads} width={32} />
                </View>
                <View
                  onClick={() => {
                    if (!isLogin) {
                      Router.goLogin();
                      return;
                    }
                    Router.goAddress();
                  }}
                >
                  <Text className="utp-location-bd-txt">新增地址</Text>
                </View>
              </Wrap>
            </Wrap>
          </View>
          {!isLogin && (
            <View className="utp-location-bd-ads">
              <View className="utp-location-bd-tits">
                <View
                  onClick={() => {
                    Router.goLogin();
                  }}
                >
                  <Txt height={64} title="请先登陆" size={26} color="low" />
                </View>
              </View>
            </View>
          )}

          {adss.map((item) => {
            return (
              <View key={item.id} className="utp-location-bd-ads">
                <View className="utp-location-bd-tits">
                  <Wrap type={1} justifyContent="space-between">
                    <View
                      onClick={() => {
                        saveAdsTitle({
                          adsTitle: item.addrDetail
                        });
                      }}
                    >
                      <Txt height={64} title={item.addrDetail} size={28} color="deep" />
                    </View>
                    <View
                      style={{ marginRight: Taro.pxTransform(0) }}
                      onClick={() => {
                        Taro.setStorage({
                          key: 'addressEdit',
                          data: item
                        });
                        Taro.setStorage({
                          key: 'LoctionAds',
                          data: ''
                        });

                        Router.goAddAddress();
                      }}
                    >
                      <Img src={images.ads_edit} width={44} />
                    </View>
                  </Wrap>
                </View>
                <View className="utp-location-bd-tits utp-location-bd-tits-1">
                  <Wrap type={1}>
                    <View
                      onClick={() => {
                        saveAdsTitle({
                          adsTitle: item.addrDetail
                        });
                      }}
                    >
                      <Txt
                        height={60}
                        title={`${item.userName} ${item.phone}`}
                        size={24}
                        color="low"
                      />
                    </View>
                  </Wrap>
                </View>
              </View>
            );
          })}
        </View>

        <View className="utp-location-bd-box utp-location-bd-box-1 utp-location-bd-box-2">
          <View className="utp-location-bd-tits utp-location-bd-tits-2">
            <Wrap type={1} justifyContent="space-between">
              <Txt title="附近地址" height={74} />
            </Wrap>
          </View>
          {address.map((item, index) => {
            if (index !== 0) {
              return (
                <View
                  key={item.title}
                  className="utp-location-bd-ads"
                  onClick={() => {
                    setCrtAds(item);
                    setIndex(index);
                    saveAdsTitle({
                      adsTitle: item.title
                    });
                  }}
                >
                  <View className="utp-location-bd-tits utp-location-bd-tits-2">
                    <Wrap type={1} justifyContent="space-between">
                      <Txt height={100} title={item.title} size={28} color="deep" />
                    </Wrap>
                  </View>
                </View>
              );
            }
          })}
        </View>

        <Box1>
          {false &&
            address.map((item) => {
              return (
                <View
                  key={item.title}
                  className="utp-location-item utp-solid"
                  onClick={() => {
                    Taro.setStorage({
                      key: 'LoctionAds',
                      data: item
                    });
                    Router.goBack();
                  }}
                >
                  <View className="utp-location-item-txts">
                    <Wrap type={1}>
                      <Txt title={item.title} size={28} color="deep" dian />
                    </Wrap>
                  </View>

                  <View className="utp-location-item-txts">
                    <Wrap type={1}>
                      <Txt title={item.address} size={26} dian color="low" />
                    </Wrap>
                  </View>
                </View>
              );
            })}
        </Box1>
      </View>
      <View
        className="utp-location-ft"
        onClick={() => {
          setDemo(demo + 1);
        }}
      ></View>
    </View>
  );
};

let moreOne = debounce((cb: any) => {
  cb();
}, 300);

Index.config = {
  navigationBarTitleText: '定位管理',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    post: state.cart.post,
    isLogin: state.cart.isLogin,
    adsTitle: state.cart.adsTitle,
    x: state.cart.x,
    y: state.cart.y
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    getSubmit: (payload) => {
      dispatch({
        type: 'cart/getSubmit',
        payload
      });
    },
    saveAdsTitle: (payload) => {
      dispatch({
        type: 'cart/saveAdsTitle',
        payload
      });
    },
    saveXy: (payload) => {
      dispatch({
        type: 'cart/saveXy',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
