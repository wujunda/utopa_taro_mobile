import Taro, { useState, useRouter } from '@tarojs/taro';
import { View } from '@tarojs/components';

import Wrap from '../../../components/Wrap';
import Bton from '../../../components/Bton';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
//import Qrcode from '../../../components/Qrcode';
import Loading from '../../../components/Loading';
import LoadingNone from '../../../components/LoadingNone';
import { getPay, IVoucher, getInfo1 } from '../../../services/voucher';
// import { images } from '../../../images';
import useRequest from '../../../hooks/useRequest';
import { Glo } from '../../../utils/utils';
// import Router from '../../../utils/router';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const id = useRouter().params.id;
  // const [show, setShow] = useState(false);
  // const [outing, setOuting] = useState(false);
  const [state, update, loading] = useRequest<IVoucher>(id, getInfo1);
  if (false) {
    console.log(update);
  }
  // let t = [1];
  // let second = 0;
  const onPay = async () => {
    let data = await getPay({
      goodsCode: state.data.goodsCode,
      goodsNum: 1,
      goodsPrice: state.data.orderAmount,
      shopGoodsId: state.data.shopGoodsId,
      clientType: 1
    });
    if (data.code === 0) {
    } else {
      Taro.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
  };
  return (
    <View className="utp-voucherOrder">
      {loading && !state && <Loading />}
      {!loading && !state && <LoadingNone />}
      {!!state && state.data && (
        <View>
          <View
            className="utp-voucherOrder-goods"
            onClick={() => {
              Taro.navigateTo({
                url: '/pages/service/VoucherDetails/index?id=' + state.data.shopGoodsId
              });
            }}
          >
            <Wrap type={1}>
              <View className="utp-voucherOrder-goods-imgs">
                <Img src={state.data.picUrl} width={140} />
              </View>
              <View className="utp-voucherOrder-goods-box">
                <View className="utp-voucherOrder-goods-box-title">
                  <Wrap justifyContent="space-between">
                    {!!state && state.data && (
                      <Txt title={state.data.name} color="deep" size={30} />
                    )}
                    {!!state && state.data && (
                      <Txt
                        title={`￥${Glo.formatPrice(state.data.goodsPrice)}`}
                        color="low"
                        size={28}
                      />
                    )}
                  </Wrap>
                </View>
              </View>
            </Wrap>
            <View className="utp-voucherOrder-tips1">
              <Wrap type={1} justifyContent="space-between">
                <Txt title="数量" color="deep" size={28} />
                <Txt title="1" color="deep" size={28} />
              </Wrap>
            </View>
            <View className="utp-voucherOrder-tips1">
              <Wrap type={1} justifyContent="space-between">
                <Txt title="总计" color="deep" size={28} />
                <Txt color="red" size={28} title={`￥${Glo.formatPrice(state.data.goodsPrice)}`} />
              </Wrap>
            </View>
          </View>
          <View style={{ padding: Taro.pxTransform(20) }} onClick={onPay}>
            <Bton> 提交订单</Bton>
          </View>
        </View>
      )}
    </View>
  );
};

Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '订单详情',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};
export default Index;
