import Taro from '@tarojs/taro';
import JSBridge from './jsbridge/index';
import models from '../models';
import { _fetchToken } from '../services/login';
import Router from '../utils/router';
import Qiniu from '../utils/qiniu';
import { getToken } from '../services/upload';
// eslint-disable-next-line import/first
import { getImgUrl } from '@/services/upload';

declare let BMap: any, BMAP_STATUS_SUCCESS: any;
import { getCodeAddGoods, ICodeGoods } from '../services/scan';
import { getStoreId } from '../services/submit';

var QQMapWX = require('./qqmap-wx-jssdk.min.js');

var qqmapsdk;

interface Request {
  code: number;
  msg: string;
  status: number;
}
// old
export const handleRequest = (data: Request) => {
  if (data.code === 0) return true;
  else {
    Taro.showToast({
      title: data.msg || '请求异常',
      icon: 'none'
    });
    return false;
  }
};
//
export const handleNewRequest = (data: Request) => {
  if (data.status === 1) return true;
  else {
    Taro.showToast({
      title: data.msg || '请求异常',
      icon: 'none'
    });
    return false;
  }
};
/**
 * 保留小数 todo....
 * **/
export const priceToFixedZore = (d: number, retain: number = 2) => {
  try {
    let num: any = d / 100;
    return Number(num.toFixed(retain)) * 1;
  } catch (e) {
    return null;
  }
};

// 千位数分割
export const thousandSeparator = (num) => {
  return (
    num &&
    (num.toString().indexOf('.') != -1
      ? num.toString().replace(/(\d)(?=(\d{3})+\.)/g, function ($1, $2) {
          return $2 + ',';
        })
      : num.toString().replace(/(\d)(?=(\d{3})+\b)/g, function ($1, $2) {
          return $2 + ',';
        }))
  );
};

/**
 * 保留小数
 * **/
export const priceToFixed = (d: number, retain: number = 2) => {
  try {
    return d.toFixed(retain);
  } catch (e) {
    return null;
  }
};
/**
 * price *100 or /100
 * ago 1=*100  2=/100
 * 有精确度问题
 * to do...
 *  **/
export const PriceFormat = (m: number, ago: number = 2) => {
  try {
    let priceM: any;
    switch (ago) {
      case 1:
        priceM = priceToFixed(m * 100);
        break;
      case 2:
        priceM = priceToFixed(m / 100);
        break;
      default:
        return m;
    }
    return priceM;
  } catch (e) {
    return null;
  }
};

export const urlToObject = (url = window.location.href) => {
  var urlObject = {};
  if (/\?/.test(url)) {
    var urlString = url.substring(url.indexOf('?') + 1);
    var urlArray = urlString.split('&');
    for (var i = 0, len = urlArray.length; i < len; i++) {
      var urlItem = urlArray[i];
      var item = urlItem.split('=');
      urlObject[item[0]] = item[1];
    }
    return urlObject;
  }
};

export function parseTime(time: any, cFormat?: any) {
  if (arguments.length === 0 || !time) {
    return false;
  }
  try {
    const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}';
    let date;
    if (typeof time === 'object') {
      date = time;
    } else {
      if (('' + time).length === 10) time = parseInt(time) * 1000;
      date = new Date(time);
    }
    const formatObj = {
      y: date.getFullYear(),
      m: date.getMonth() + 1,
      d: date.getDate(),
      h: date.getHours(),
      i: date.getMinutes(),
      s: date.getSeconds(),
      a: date.getDay()
    };
    const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
      let value = formatObj[key];
      if (key === 'a') return ['一', '二', '三', '四', '五', '六', '日'][value - 1];
      if (result.length > 0 && value < 10) {
        value = '0' + value;
      }
      return value || 0;
    });
    return time_str;
  } catch (e) {
    console.log(e);
  }
}

export const timeDiffStr = (t: any) => {
  let time = Number(t);
  let d = Math.floor(t / 1000 / 60 / 60 / 24);
  let hour = Math.floor((time / 1000 / 60 / 60) % 24);
  let min = Math.floor((time / 1000 / 60) % 60);
  let sec = Math.floor((time / 1000) % 60);
  if (d) {
    return d + '天' + hour + '小时' + min + '分钟';
  }
  if (hour) {
    return hour + '小时' + min + '分钟';
  }
  if (min) {
    return min + '分钟';
  }
};

export const InitEndTime = (endwise: any, format = 'S') => {
  let dd: string | number,
    hh: string | number,
    mm: string | number,
    // eslint-disable-next-line no-unused-vars
    ss: any = null;
  try {
    var time = parseInt(endwise) - new Date().getTime() / 1000;
    if (time <= 0) {
      return false;
    } else {
      dd = Math.floor(time / 60 / 60 / 24);
      hh = Math.floor((time / 60 / 60) % 24);
      mm = Math.floor((time / 60) % 60);
      ss = Math.floor(time % 60);
      if (format == 'S') {
        return dd + '天' + hh + '小时' + mm + '分' + ss + '秒';
      }
      if (format == 'M') {
        return dd + '天' + hh + '小时' + mm + '分';
      }
      return dd + '天' + hh + '小时';
    }
  } catch (e) {
    return null;
  }
};

// 时间戳转字符串
export const formatTime = (number, format) => {
  var formateArr = ['Y', 'M', 'D', 'h', 'm', 's'];
  var returnArr = [];

  var date = new Date(number);
  returnArr.push(date.getFullYear());
  returnArr.push(formatNumber(date.getMonth() + 1));
  returnArr.push(formatNumber(date.getDate()));

  returnArr.push(formatNumber(date.getHours()));
  returnArr.push(formatNumber(date.getMinutes()));
  returnArr.push(formatNumber(date.getSeconds()));

  for (var i in returnArr) {
    format = format.replace(formateArr[i], returnArr[i]);
  }
  return format;
};

//数据转化
export const formatNumber = (n) => {
  n = n.toString();
  return n[1] ? n : '0' + n;
};

// const getUploadToken = async () => {
//   var upToken = await getToken({});
// };
// const data = await getToken({});
// if (data.code === 0 && data.data && data.data.upToken) {
//   imgs.map((item) => {
//     Qiniu(data.data.upToken, item).then(
//       (hash) => {
//         feedbackKeys.push(hash);
//         if (feedbackKeys.length === imgs.length) {
//           onDone();
//         }
//       },
//       () => {
//         feedbackKeys = [];
//       }
//     );
//   });
// }

//图片上传
export const UploadImgCallBack = (call: any) => {
  Taro.chooseImage({
    success: async (info) => {
      try {
        var upToken = await getToken({});
        // [info.tempFiles[0].originalFileObj];
        // 先处理
        var imgData;
        if (process.env.TARO_ENV === 'weapp') {
          imgData = await Qiniu(upToken.data.upToken, info.tempFilePaths[0]);
        } else {
          imgData = await Qiniu(upToken.data.upToken, info.tempFiles[0].originalFileObj);
        }
        // let url = await getImgUrl(imgData);
        // console.log(upToken, imgData);
        //info.tempFilePaths, 1, imgData
        call && call({ type: 1, path: info.tempFilePaths, key: imgData });
      } catch (e) {
        console.log(e, '欧里给异常了老表');
      }
    },
    complete: (info) => {
      call && call({ info, type: 3 });
    },
    /** 接口调用失败的回调函数 */
    fail: (info) => {
      call && call({ info, type: 2 });
    }
  });
};

export class Glo {
  static store: any;
  static formatPrice(i: number) {
    if (i == null) {
      return '0.00';
    } else {
      let money: string = (i / 100).toFixed(2);
      return money;
      if (money.split('.')[1][1] == '0' && money.split('.')[1][0] == '0') {
        money = money.split('.')[0];
      } else if (money.split('.')[1][1] == '0') {
        //money = money.split('.')[0] + '.' + money.split('.')[1][0];
      }
      return money;
    }
  }
  static copyContent(data: any, t = 3000) {
    Taro.setClipboardData({
      data: data,
      success: () => {
        Taro.showToast({
          title: '复制成功',
          icon: 'none',
          duration: t
        });
      }
    });
  }
  static loading(title: any) {
    Taro.showLoading({
      title: title
    });
  }
  static hideLoading() {
    Taro.hideLoading();
  }
  static showToast(title, type = 'none') {
    Taro.showToast({ icon: type as any, title: title });
  }
  static isPhone(str: string, num?: number) {
    if (num === 86) {
      return /^1\d{10}$/.test(str);
    } else {
      if (String(str).length > 20) {
        return false;
      } else {
        return true;
      }
    }
  }
  static async getCode(code: string, type) {
    let vm = this;
    if (code.indexOf('_') > -1) {
      let arr = code.split('_');
      let skuId = '';
      let storeId = '';
      let busId = '';
      if (arr[1]) {
        let code1 = arr[1];
        busId = code1.split('-')[0];
      }
      if (arr[2]) {
        skuId = arr[2];
      }
      if (arr[3]) {
        storeId = arr[3];
      }
      let data = await getStoreId({
        clientType: 1,
        pageNo: 1,
        pageSize: 20,
        qrCode: code,
        sysId: 1004
      });
      if (data.code === 0) {
        storeId = data.data.records[0].storeId;
        console.log('这里这里');
        vm.onAddGoods(skuId, storeId, busId, type);
      }
      return;
    }
    if (code.indexOf('http') > -1) {
      Router.goWeb(code);
    } else {
      vm.scanSus(code, type);
    }
  }
  static scan(type) {
    let vm = this;
    if (!Glo.store.getState().cart.isLogin) {
      Router.goLogin();
      return;
    }
    //this.getCode('6908946286905', type);
    //this.getCode('TT_945-2110713827665857-01_22709701_208551_0', type);
    //return;
    Taro.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        let code = res.result;
        vm.getCode(code);
      },
      fail: () => {
        console.log('扫码失败');
      }
    });
  }
  static onAddGoods = async (skuId, storeId, businessId, type) => {
    let data = await getCodeAddGoods({
      businessId: businessId,
      clientType: 1,
      number: 1,
      skuId: skuId,
      sourceChannel: '',
      storeId: storeId
    });
    if (data.code === 0) {
      console.log('跳购物车');
      if (type === 1) {
        Taro.redirectTo({ url: '/pages/scan/scanCodeCart/index' });
      } else {
        Taro.navigateTo({ url: '/pages/scan/scanCodeCart/index' });
      }
    }
  };

  static scanSus(code: string, type) {
    if (type === 1) {
      Taro.redirectTo({ url: '/pages/scan/scanResult/index?id=' + code });
    } else {
      Taro.navigateTo({ url: '/pages/scan/scanResult/index?id=' + code });
    }
  }
  static getXyAds(x, y) {
    return new Promise((r) => {
      if (process.env.TARO_ENV === 'weapp') {
        qqmapsdk = new QQMapWX({
          key: 'AUYBZ-3XDKD-YYF4H-HM3CM-7BBTO-FHBZY'
        });
        qqmapsdk.search({
          keyword: '餐饮',
          location: {
            latitude: x,
            longitude: y
          },
          success: function (res) {
            console.log('成功');
            console.log(res);
            if (res.message === 'query ok') {
              res.data.map((item, index) => {
                if (index === 0) {
                  r(item.address);
                }
              });
            }
          },
          fail: function (res) {},
          complete: function (res) {}
        });
        return;
      }
      let local = new BMap.LocalSearch('广州', {
        // 智能搜索
        renderOptions: { autoViewport: true }
      });
      local.search('酒店');
      local.setSearchCompleteCallback((respone) => {
        if (local.getStatus() === BMAP_STATUS_SUCCESS) {
          const s: any = [];
          for (let i = 0; i < respone.getCurrentNumPois(); i++) {
            if (i === 0) {
              r(respone.getPoi(i).address);
            }
          }
        }
      });
    });
  }
}

/**
 * 去支付平台发起支付
 * @param {Object} params 支付平台参数
 */
export const goPayPage = (params = {}) => {
  const config = {
    dev: {
      payUrl: 'http://h5-pay-dev.myutopa.com'
    },
    test: {
      payUrl: 'http://h5-pay-test.myutopa.com'
    },
    pred: {
      payUrl: 'https://h5-pay-pred.myutopa.com'
    },
    prod: {
      payUrl: 'https://h5-pay.myutopa.com'
    }
  };

  const getEnv = () => {
    const hostname = window.location.hostname;
    let env = 'prod';
    if (hostname.indexOf('dev') > -1) {
      env = 'dev';
    } else if (hostname.indexOf('test') > -1) {
      env = 'test';
    } else if (hostname.indexOf('pre') > -1) {
      env = 'pred';
    }
    return env;
  };

  const { payOrderNo, paySucceesHref = '', payFailHref = '', sysId = 1001 } = params;

  if (!payOrderNo) {
    console.error('支付失败，缺少payOrderNo');
    return;
  }

  const envStr = getEnv();
  const payHostname = config[envStr]['payUrl'];

  const href = `${payHostname}/#/pay?payOrderNo=${payOrderNo}&sysId=${sysId}&paySucceesHref=${encodeURIComponent(
    paySucceesHref
  )}&payFailHref=${encodeURIComponent(payFailHref)}`;

  // console.log('go-pay-url:', href);
  window.location.href = href;
};

export const getAppToken = () => {
  return new Promise((resolve, reject) => {
    (JSBridge as any).Common.Caller_Common_Base_getOpenToken('', (responsed) => {
      if (responsed) {
        _fetchToken({
          openToken: responsed
        }).then((res) => {
          console.log(res, 'app->res');
          if (res.code === 0) {
            console.log(res, 'app->res->code==0');
            Taro.setStorage({
              key: 'isLogin',
              data: {
                accessToken: res.data.accessToken
              }
            });
            resolve(res);
          } else {
            reject();
          }
        });
      }
    });
  });
};

export const inAppIsLogin = () => {
  return new Promise((resolve, reject) => {
    JSBridge.Bridge.WebViewJavascriptBridge.setup(() => {
      // 判断是否登录
      JSBridge.Common.Caller_Common_Base_getLoginStatus('', (response) => {
        console.log('getLoginStatus(11', response);
        if (response == 0) {
          console.log('getLoginStatus(', response);
          JSBridge.Common.GTBridge_Common_Base_goLogin();
        } else {
          // 拿token
          (JSBridge as any).Common.Caller_Common_Base_getOpenToken('', (responsed) => {
            console.log('Caller_Common_Base_getOpenToken(', responsed);
            if (responsed) {
              _fetchToken({
                openToken: responsed
              }).then((res) => {
                console.log('openToken(', res);
                if (res.code === 0) {
                  Taro.setStorage({
                    key: 'isLogin',
                    data: {
                      accessToken: res.data.accessToken
                    }
                  });
                  Glo.store.dispatch({
                    type: 'cart/saveLogin',
                    payload: {
                      data: { accessToken: res.data.accessToken }
                    }
                  });
                  setTimeout(() => {
                    resolve(true);
                  }, 200);
                } else {
                  Taro.showToast({
                    title: res.msg,
                    icon: 'none'
                  });
                  reject(false);
                }
              });
            } else {
              console.log('eroor-->');
              reject(false);
            }
          });
        }
      });
    });
  });
};

/**
 * 针对对象的Key值进行一个深排序
 * @param target 排序目标对象
 */
export const objKeySort = (target) => {
  function deepSort(obj, cache = []) {
    if (obj === null || typeof obj !== 'object') {
      return obj;
    }
    // @ts-ignore
    var hit = cache.filter((c) => c.original === obj)[0];
    if (hit) {
      // @ts-ignore
      return hit.copy;
    }

    var copy = Array.isArray(obj) ? [] : {};
    cache.push({
      // @ts-ignore
      original: obj,
      // @ts-ignore
      copy
    });
    Object.keys(obj)
      .sort()
      .forEach((key) => {
        copy[key] = deepSort(obj[key], cache);
      });

    return copy;
  }

  return deepSort(target);
};

/**
 * 传入一个对象返回queryString后的字符串
 * @param reqData 处理数据
 */
export const queryStringEncode = (reqData: object) => {
  let ret = '';
  // tslint:disable-next-line:forin
  for (const it in reqData) {
    ret += encodeURIComponent(it) + '=' + encodeURIComponent(reqData[it]) + '&';
  }
  return ret;
};
export const successUrl = () => {
  const hostname = window.location.hostname;
  let url = 'https://www.myutopa.com/m/mall/#/pages/integral/list/index';
  if (hostname.indexOf('dev') > -1) {
    url = 'https://www-dev.myutopa.com/m/mall/#/pages/integral/list/index';
  } else if (hostname.indexOf('pre') > -1) {
    url = 'https://www-pre.myutopa.com/m/mall/#/pages/integral/list/index';
  } else if (hostname.indexOf('test') > -1) {
    url = 'https://www-test.myutopa.com/m/mall/#/pages/integral/list/index';
  }
  return url;
};

/**
 * 拿到地址具体参数
 * @param name
 */

export const UrlSearch = (name: string) => {
  var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i'); // 匹配目标参数
  var result = window.location.href.split('?')[1].match(reg); // 对匹配目标参数
  if (result != null) {
    return decodeURIComponent(result[2]);
  } else {
    return null;
  }
};
