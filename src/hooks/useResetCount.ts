import { useState, useEffect } from '@tarojs/taro';

function useResetCount(): [boolean, () => void] {
  const [can, setCan] = useState(true);
  const [count, setCount] = useState(0);

  useEffect(() => {
    if (count > 10) {
      setCan(false);
    } else {
      setCan(true);
    }
  }, [count]);

  useEffect(() => {}, []);

  const refresh = () => {
    setCan(false);
    setTimeout(() => {
      setCount(count + 1);
    }, 1000);
  };

  return [can, refresh];
}

export default useResetCount;
