import Taro, { useRouter } from '@tarojs/taro';
import { View } from '@tarojs/components';
import { AtTabBar } from 'taro-ui';

import Img from '../../../components/Img';
import { connect } from '@tarojs/redux';
import useRequest from '../../../hooks/useRequest';
import Router from '../../../utils/router';
import { IBar, ILiber, ILiberData, getLiber } from '../../../services/liber';
import './index.scss';
import { images } from '../../../images';

type PageStateProps = {
  statusHeight: number;
};

type PageDispatchProps = {
  // addCart: (any) => void;
};
type IProps = PageStateProps & PageDispatchProps;
const Index: Taro.FC<IProps> = ({ statusHeight }) => {
  const id = useRouter().params.id;

  const [state] = useRequest<ILiberData>(id, getLiber);

  // 图片
  let list: ILiber[] = [];
  if (state && state.code === 0) {
    list = state.data.banners;
  }

  // 底部tabbar
  let tabs: any[] = [];
  let tabsTrue: IBar[] = [];
  if (state && state.code === 0) {
    state.data.classes.map((item, index) => {
      tabsTrue.push(item);
      let obj: any = {};
      obj.title = item.name;
      if (index !== 0) {
        obj.image = images[`liber_${index}`];
      } else {
        obj.image = item.image;
      }
      obj.selectedImage = item.image;
      tabs.push(obj);
    });
  }

  return (
    <View className="uto-liber">
      <View
        className="uto-liber-hd utp-cnt"
        style={{ paddingTop: Taro.pxTransform(statusHeight * 2) }}
      >
        <View
          className="uto-liber-hd-back utp-cnt"
          style={{ paddingTop: Taro.pxTransform(statusHeight * 2) }}
          onClick={() => {
            Router.goBack();
          }}
        >
          <Img src={images.liber_back} width={48} height={48} />
        </View>
        <Img src={images.liber_top} width={256} height={112} />
        {false && (
          <View
            className="uto-liber-hd-back utp-cnt uto-liber-hd-back-1"
            style={{ paddingTop: Taro.pxTransform(statusHeight * 2) }}
            onClick={() => {
              Taro.switchTab({
                url: '/pages/cart/index'
              });
            }}
          >
            <Img src={images.liber_cart} width={48} height={48} />
          </View>
        )}
      </View>
      <View
        className="uto-liber-bd"
        style={{ marginTop: Taro.pxTransform(statusHeight * 2 + 120) }}
      >
        {list.map((item) => {
          return (
            <View
              className="uto-liber-imgs"
              onClick={() => {
                Router.goHome(item.command);
              }}
            >
              <Img src={item.image} width={710} height={(710 * item.height) / item.width} />
            </View>
          );
        })}
        <AtTabBar
          color="#b5b3b4"
          selectedColor="#040001"
          fixed
          tabList={tabs}
          onClick={(index) => {
            console.log(index);
            if (index !== 0) {
              tabsTrue.map((item, index1) => {
                if (index1 === index) {
                  Router.goHome(item.command);
                }
              });
            }
          }}
          current={0}
        />
      </View>
    </View>
  );
};
Index.config = {
  navigationBarTitleText: 'liber',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f6f6f6',
  // 不显示标题
  navigationStyle: 'custom'
};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    number: state.cart.number,
    post: state.cart.post,
    statusHeight: state.cart.statusHeight,
    cart: state.cart,
    loading: state.loading.effects['cart/addCart']
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};
// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
