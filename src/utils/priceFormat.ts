export function formatPrice(i: number) {
  if (i == null) {
    return '0.00';
  } else {
    let money: string = (i / 100).toFixed(2);
    if (money.split('.')[1][1] == '0' && money.split('.')[1][0] == '0') {
      money = money.split('.')[0];
    } else if (money.split('.')[1][1] == '0') {
      money = money.split('.')[0] + '.' + money.split('.')[1][0];
    }
    return money;
  }
}
