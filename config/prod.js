const env = require('./getEnv');
const chalk = require('chalk');

// console.log(chalk.yellow('构建环境：'));
// console.log(JSON.stringify(env, null, 2));

module.exports = {
  env: {
    ...env,
    NODE_ENV: '"production"',
  },
  defineConstants: {},
  mini: {},
  h5: {
    publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
    output: {
      filename: 'js/[name].[hash:8].js',
      chunkFilename: 'js/[name].[chunkhash:8].js'
    },
    miniCssExtractPluginOption: {
      filename: 'css/[name].[hash:8].css',
      chunkFilename: 'css/[name].[chunkhash:8].css',
    },
    /**
     * 如果h5端编译后体积过大，可以使用webpack-bundle-analyzer插件对打包体积进行分析。
     * 参考代码如下：
     * webpackChain (chain) {
     *   chain.plugin('analyzer')
     *     .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, [])
     * }
     */
    webpackChain(chain) {
      if (process.env.BUILD_ANALYZER === 'true') {
        chain.plugin('analyzer').use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, []);
      }
    }
  }
};
