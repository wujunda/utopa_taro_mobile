import Taro, { useState, useReachBottom, useRouter } from '@tarojs/taro';
import { View } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
// import JSBridge from '../../../utils/jsbridge/index';
import { couponGoods, binConfirmOrder } from '../../../services/integral';
// import { _fetchToken } from '../../../services/login';
import { goPayPage, inAppIsLogin, successUrl } from '../../../utils/utils';
import Message from '../../../components/Message/index';

import CouponGoodsCard from '../../../components/integral/couponGoodCard';
import './index.scss';

type PageStateProps = {
  user: {
    inApp: boolean;
    isWeiXinFalg: boolean;
  };
}

type PageDispatchProps = {
  setToken: () => void;
};

function CouponDetail(props) {
  const router = useRouter();
  const [messageShow, setMessageShow] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [param, setParam] = useState({
    activityId: router.params.couponId,
    pointActivityId: router.params.activityId,
    activeType: 2
  });
  // eslint-disable-next-line no-unused-vars
  const [state, hasMore, run, getMoreData, loading] = useRequestWIthMore<IStore>(
    param,
    couponGoods
  );

  var goods = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.data.page.records.map((items) => {
        goods.push(items);
      });
    });
  }
  useReachBottom(() => {
    console.warn('onReachBottom');
  });
  const onSubmit = async () => {
    if (state && state.length > 0 && state[0].data.operateStatus == 1) {
      return false;
    }
    if (props.user.inApp) {
      inAppIsLogin().then(res => {
        setMessageShow(res);
      })
      // JSBridge.Bridge.WebViewJavascriptBridge.setup(() => {
      //   // 判断是否登录
      //   JSBridge.Common.Caller_Common_Base_getLoginStatus('', (response) => {
      //     if (response == 0) {
      //       JSBridge.Common.GTBridge_Common_Base_goLogin();
      //     } else {
      //       // 拿token
      //       JSBridge.Common.Caller_Common_Base_getOpenToken('', (responsed) => {
      //         if (responsed) {
      //           _fetchToken({
      //             openToken: responsed
      //           }).then((res) => {
      //             if (res.code === 0) {
      //               Taro.setStorage({
      //                 key: 'isLogin',
      //                 data: {
      //                   accessToken: res.data.accessToken
      //                 }
      //               });
      //               props.setToken({accessToken: res.data.accessToken})
      //               setMessageShow(true)
      //             } else {
      //               Taro.showToast({
      //                 title: res.msg,
      //                 icon: 'none'
      //               });
      //             }
      //           });
      //         }
      //       });
      //     }
      //   });
      // });
    } else {
      if (!Taro.getStorageSync('isLogin')) {
        Taro.navigateTo({
          url: '/pages/login/index'
        });
      } else {
        setMessageShow(true);
      }
    }
  };
  const onCancel = () => {
    setMessageShow(false);
  };
  const submit = async () => {
    setMessageShow(false);
    Taro.showLoading({
      title: ''
    });
    let channelId = 1001;
    let ordersource = 2;
    if (props.user.inApp) {
      channelId = 1;
      ordersource = 1;
    } else if (props.user.isWeiXinFalg) {
      channelId = 1002;
    }
    let paramData = {
      binComits: [
        {
          ordersource: ordersource,
          businessId: (router.params.businessId != 'null' && router.params.businessId != 'undefined') ? router.params.businessId : '',
          storeId: (router.params.storeId != 'null' && router.params.storeId != 'undefined') ? router.params.storeId : '',
          sysId: channelId,
          pointExchangeCouponParam: {
            activityId: router.params.activityId,
            couponId: router.params.couponId,
          }
        }
      ],
      comitType: 6
    };
    let resData = await binConfirmOrder(paramData);
    Taro.hideLoading();
    if (resData && resData.code === 0) {
      if (router.params.exchangePrice && router.params.exchangePrice > 0) {
        let channelId = 1001;
        if (props.user.inApp) {
          channelId = 1
        } else if (props.user.isWeiXinFalg) {
          channelId = 1002
        }
        let dataParam = {
          payOrderNo: resData.data.payOrderNo,
          sysId: channelId,
          paySucceesHref: successUrl(),
          payFailHref: successUrl()
        };
        goPayPage(dataParam);
      } else {
        Taro.navigateTo({
          url: `/pages/integral/paySuccess/index?point=${router.params.exchangePoint}`
        });
      }
    } else {
      Taro.showToast({
        title: resData.msg,
        icon: 'none'
      });
    }
  };

  return (
    <View className="utp-integral-coupon">
      <View className="utp-integral-coupon-message">
        <View className="utp-integral-coupon-message-lable">
          <View className="utp-integral-coupon-message-lable-title">
            {state && state.length > 0 && state[0].data.activityName}
          </View>
          <View className="utp-integral-coupon-message-lable-time">
            {state && state.length > 0 && state[0].data.activeTimeDesc}
          </View>
          <View className="utp-integral-coupon-message-lable-rule">
            {state && state.length > 0 && state[0].data.activityDesc}
          </View>
        </View>
      </View>
      <View className="utp-integral-coupon-goods">
        {goods && 
        goods.map((item, index) => {
          return <CouponGoodsCard data={item} isApp={props.user.inApp} key={index}/>;
        })}
      </View>
      {!router.params.exchangePrice ? (
        <View className="utp-integral-coupon-bottom" onClick={() => onSubmit()} style={state && state.length > 0 && state[0].data.operateStatus == 1 ? {backgroundColor: '#ccc'} : ''}>
          <View className="utp-integral-coupon-bottom-lable">立即兑换（{router.params.exchangePoint}积分）</View>
        </View>
      ) : (
        <View className="utp-integral-coupon-bottom" onClick={() => onSubmit()} style={state && state.length > 0 && state[0].data.operateStatus == 1 ? {backgroundColor: '#ccc'} : ''}>
          <View className="utp-integral-coupon-bottom-lable">立即兑换（{router.params.exchangePoint}积分+¥{router.params.exchangePrice / 100}）</View>
        </View>
      )}
      <Message
        show={messageShow}
        type={1}
        title="积分使用"
        onClose={() => onCancel()}
        onConfirm={() => submit()}
      >
        本次消费将消耗积分账户中{router.params.exchangePoint}积分， 是否继续提交订单？
      </Message>
    </View>
  );
}

CouponDetail.config = {
  navigationBarTitleText: '积分兑换优惠券',
  disableScroll: false
};
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    }
  };
};
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    setToken: (params) => {
      dispatch({
        type: 'cart/saveLogin',
        payload: {
          data: params
        }
      })
    }
  };
};
// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(CouponDetail);
