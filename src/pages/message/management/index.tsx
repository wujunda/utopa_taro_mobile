import { View, ScrollView, Text } from '@tarojs/components';
import Taro, { useState } from '@tarojs/taro';

import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Bar from '../../../components/bar';
import Img from '../../../components/Img';
import goods from '../../../assets/goods.png';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const [managementTabIndex, setmanagementTabIndex] = useState(0);

  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <Bar title="购物车" />
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-management"
            scrollY
          >
            <Wrap type={2} flexDirection="column" top>
              <View className="utp-management-follow">
                <Wrap type={3}>
                  <Text className="utp-management-follow-left">已关注</Text>
                  <View className="utp-management-follow-center">
                    <View style={{ marginRight: 10 }}>
                      <Img src={goods} width={64} round></Img>
                    </View>
                    <View style={{ marginRight: 10 }}>
                      <Img src={goods} width={64} round></Img>
                    </View>
                    <View style={{ marginRight: 10 }}>
                      <Img src={goods} width={64} round></Img>
                    </View>
                  </View>
                  <Text className="utp-management-follow-right">编辑</Text>
                </Wrap>
              </View>
              <Text className="utp-management-Recommend">专属推荐</Text>
              <View className="utp-management-ImgBox">
                <View className="utp-management-ImgBox-info">
                  <Wrap type={4} flexDirection="column">
                    <View>
                      <Img src={goods} width={100} round></Img>
                    </View>
                    <View className="utp-management-ImgBox-text">
                      <Text className="utp-management-ImgBox-text1">童年时光</Text>
                      <Text className="utp-management-ImgBox-text2">推荐文案推荐童年时光</Text>
                    </View>
                  </Wrap>
                </View>
                <View className="utp-management-ImgBox-info">
                  <Wrap type={4} flexDirection="column">
                    <View>
                      <Img src={goods} width={100} round></Img>
                    </View>
                    <View className="utp-management-ImgBox-text">
                      <Text className="utp-management-ImgBox-text1">童年时光</Text>
                      <Text className="utp-management-ImgBox-text2">推荐文案推荐童年时光</Text>
                    </View>
                  </Wrap>
                </View>
                <View className="utp-management-ImgBox-info">
                  <Wrap type={4} flexDirection="column">
                    <View>
                      <Img src={goods} width={100} round></Img>
                    </View>
                    <View className="utp-management-ImgBox-text">
                      <Text className="utp-management-ImgBox-text1">童年时光</Text>
                      <Text className="utp-management-ImgBox-text2">推荐文案推荐童年时光</Text>
                    </View>
                  </Wrap>
                </View>
                <View className="utp-management-ImgBox-info">
                  <Wrap type={4} flexDirection="column">
                    <View>
                      <Img src={goods} width={100} round></Img>
                    </View>
                    <View className="utp-management-ImgBox-text">
                      <Text className="utp-management-ImgBox-text1">童年时光</Text>
                      <Text className="utp-management-ImgBox-text2">推荐文案推荐童年时光</Text>
                    </View>
                  </Wrap>
                </View>
              </View>
              <View className="utp-management-businessSort">
                {['商家分类', '商家分类', '商家分类', '商家分类', '商家分类', '商家分类'].map(
                  (item, index) => {
                    return (
                      <View
                        key={index}
                        className={
                          index === managementTabIndex
                            ? 'utp-management-businessSort-storeName-active'
                            : 'utp-management-businessSort-storeName'
                        }
                        onClick={() => {
                          // console.log('我切换', index);
                          setmanagementTabIndex(index);
                        }}
                      >
                        {item}
                      </View>
                    );
                  }
                )}
              </View>
              <View className="utp-management-businessInfoBox">
                <ScrollView
                  refresherEnabled
                  refresherBackground="#333"
                  className="utp-bd utp-bd-page"
                  scrollY
                >
                  <View className="utp-management-businessInfo">
                    <Wrap type={4}>
                      <View style={{ marginRight: 10 }}>
                        <Img src={goods} width={90} round></Img>
                      </View>
                      <View className="utp-management-businessInfo-textBox">
                        <Wrap type={4} flexDirection="column" top>
                          <View className="utp-management-businessInfo-text">
                            <Text className="utp-management-businessInfo-text1">童年时光</Text>
                            <Text className="utp-management-businessInfo-text2">主营类目</Text>
                          </View>
                        </Wrap>
                      </View>
                      <View className="utp-management-businessInfo-BtnBox">
                        <Text className="utp-management-businessInfo-Btn" onClick={() => {}}>
                          关注
                        </Text>
                      </View>
                    </Wrap>
                  </View>
                  <View className="utp-management-businessInfo">
                    <Wrap type={4}>
                      <View style={{ marginRight: 10 }}>
                        <Img src={goods} width={90} round></Img>
                      </View>
                      <View className="utp-management-businessInfo-textBox">
                        <Wrap type={4} flexDirection="column" top>
                          <View className="utp-management-businessInfo-text">
                            <Text className="utp-management-businessInfo-text1">童年时光</Text>
                            <Text className="utp-management-businessInfo-text2">主营类目</Text>
                          </View>
                        </Wrap>
                      </View>
                      <View className="utp-management-businessInfo-BtnBox">
                        <Text className="utp-management-businessInfo-Btn" onClick={() => {}}>
                          关注
                        </Text>
                      </View>
                    </Wrap>
                  </View>
                  <View className="utp-management-businessInfo">
                    <Wrap type={4}>
                      <View style={{ marginRight: 10 }}>
                        <Img src={goods} width={90} round></Img>
                      </View>
                      <View className="utp-management-businessInfo-textBox">
                        <Wrap type={4} flexDirection="column" top>
                          <View className="utp-management-businessInfo-text">
                            <Text className="utp-management-businessInfo-text1">童年时光</Text>
                            <Text className="utp-management-businessInfo-text2">主营类目</Text>
                          </View>
                        </Wrap>
                      </View>
                      <View className="utp-management-businessInfo-BtnBox">
                        <Text className="utp-management-businessInfo-Btn" onClick={() => {}}>
                          关注
                        </Text>
                      </View>
                    </Wrap>
                  </View>
                  <View className="utp-management-businessInfo">
                    <Wrap type={4}>
                      <View style={{ marginRight: 10 }}>
                        <Img src={goods} width={90} round></Img>
                      </View>
                      <View className="utp-management-businessInfo-textBox">
                        <Wrap type={4} flexDirection="column" top>
                          <View className="utp-management-businessInfo-text">
                            <Text className="utp-management-businessInfo-text1">童年时光</Text>
                            <Text className="utp-management-businessInfo-text2">主营类目</Text>
                          </View>
                        </Wrap>
                      </View>
                      <View className="utp-management-businessInfo-BtnBox">
                        <Text className="utp-management-businessInfo-Btn" onClick={() => {}}>
                          关注
                        </Text>
                      </View>
                    </Wrap>
                  </View>
                  <View className="utp-management-businessInfo">
                    <Wrap type={4}>
                      <View style={{ marginRight: 10 }}>
                        <Img src={goods} width={90} round></Img>
                      </View>
                      <View className="utp-management-businessInfo-textBox">
                        <Wrap type={4} flexDirection="column" top>
                          <View className="utp-management-businessInfo-text">
                            <Text className="utp-management-businessInfo-text1">童年时光</Text>
                            <Text className="utp-management-businessInfo-text2">主营类目</Text>
                          </View>
                        </Wrap>
                      </View>
                      <View className="utp-management-businessInfo-BtnBox">
                        <Text className="utp-management-businessInfo-Btn" onClick={() => {}}>
                          关注
                        </Text>
                      </View>
                    </Wrap>
                  </View>
                </ScrollView>
              </View>
            </Wrap>
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};
export default Index;
