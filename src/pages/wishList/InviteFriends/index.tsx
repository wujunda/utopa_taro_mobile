import Taro from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';

import Img from '../../../components/Img';
import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Txt from '../../../components/Txt';
import Bton from '../../../components/Bton';

import bgCode from '../../../assets/mine/bg_wode.png';
import erWeiMa from '../../../assets/mine/erweima.jpg';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          {/* <Bar /> */}
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page"
            scrollY
          >
            <View
              className="utp-InviteFriends-bgWrap"
              style={{
                background: `url(${bgCode})`
              }}
            ></View>
            <View className="utp-InviteFriends-content">
              <Wrap type={3} flexDirection="column">
                <View>
                  <Img src={erWeiMa} width={500}></Img>
                </View>

                <View className="utp-InviteFriends-inviteText">
                  <Txt size={32} title="我的邀请码:fww311" color="deep"></Txt>
                </View>

                <View className="utp-InviteFriends-toinvite">
                  <Bton type={7}>我要邀请</Bton>
                </View>
              </Wrap>
            </View>
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};

export default Index;
