import Taro, { useReachBottom } from '@tarojs/taro';
import { View } from '@tarojs/components';

import Goods1 from '../../../components/Goods1';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import Safe from '../../../components/safe';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import useSession from '../../../hooks/useSession';
import useLogin from '../../../hooks/useLogin';
import useResetCount from '../../../hooks/useResetCount';
import { IHistory, getHistory } from '../../../services/history';
import { ProductBaiscData } from '../../../interface/product';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  // console.log('注册注册');
  const [haveSession] = useSession();
  const [isLogin, updateLogin] = useLogin();
  if (haveSession && !isLogin) {
    if (updateLogin) {
      updateLogin();
    }
  }
  // 购物车数据
  // @ts-ignore
  const [state, hasMore, update, getMoreData, loading] = useRequestWIthMore<IHistory>(
    'name=a&age=1',
    getHistory
  );
  const [can, update1] = useResetCount();
  (state, 'steate');
  if (
    haveSession &&
    isLogin &&
    (state === null || (state != null && state.length == 0)) &&
    update &&
    !loading
  ) {
    if (can) {
      update({});
      update1();
    }
  }
  var goods: ProductBaiscData[] = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.data.records.map((item) => {
        goods.push(item);
      });
    });
  }
  useReachBottom(() => {
    console.warn('onReachBottom');
  });

  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <View className="utp-100"></View>
          <View style={{ flex: 1,background:'#f5f5f5' }}>
            {loading && goods.length === 0 && <Loading />}
            {!loading && goods.length === 0 && <LoadingNone />}
            {goods.map((item) => {
              return (
                <View key={item.id}>
                  <Goods1 item={item}  isAdd={false}/>
                </View>
              );
            })}
            {loading && goods.length !== 0 && <LoadingMore />}
            {!loading && !hasMore && goods.length > 0 && <LoadingNoneMore />}
          </View>
        </View>
      </View>
    </Safe>
  );
};

Index.config = {
  navigationBarTitleText: '浏览历史',
  disableScroll: false
  //navigationStyle: 'custom',
  //usingComponents: {
  //Bar: '../../../components/bar' // 书写第三方组件的相对路径
  //}
};
Index.defaultProps = {};
export default Index;
