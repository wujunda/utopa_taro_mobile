import Taro from '@tarojs/taro';
import { ScrollView, View } from '@tarojs/components';
import Img from '../../../../components/Img';

interface IProps {
  item: any;
}
const Index: Taro.FC<IProps> = ({ item }) => {
  return (
    item && (
      <ScrollView
        className="more-item"
        scrollX
        scrollWithAnimation
        scrollTop={0}
        style={{ height: 105 }}
        lowerThreshold={20}
        upperThreshold={20}
      >
        {item.products &&
          item.products.map((o, idx) => {
            return (
              <View
                style={{ marginRight: 10, borderRadius: '5px' }}
                className="utp-applyService-goodsInfo-imgs"
                key={idx}
              >
                <Img src={o.pics} width={150} />
                <View
                  style={{
                    fontSize: '12px',
                    textAlign: 'center',
                    color: '#333',
                    marginTop: '3px'
                  }}
                >
                  退{o.refundnums || 0}件
                </View>
              </View>
            );
          })}
      </ScrollView>
    )
  );
};
Index.defaultProps = {};

export default Index;
