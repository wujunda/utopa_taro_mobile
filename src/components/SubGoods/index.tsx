import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';

import './index.scss';
import Wrap from '../../components/Wrap';
import Txt from '../../components/Txt';
import { Gprops } from '../../services/orders';
import { ISubmitData, ISubmitGoods } from '../../services/submit';
import Yuan from '../../components/Yuan';
import { Glo } from '../../utils/utils';

interface IProps {
  data: Gprops | ISubmitGoods;
  sums?: ISubmitData;
  type?: number;
}
const Index: Taro.FC<IProps> = ({ sums, data, type = 0 }) => {
  let isJifen = false;
  if (sums && sums.orderExchangeTotalPoint && sums.orderExchangeTotalPoint > 0) {
    isJifen = true;
  }
  return (
    <View className="utp-sub-goods utp-div">
      <View className="utp-sub-goods-i utp-cnt">
        <Image className="utp-sub-goods-img" src={data && data.pic} />
      </View>
      <View className="utp-sub-goods-item">
        <View className="utp-sub-goods-titles">
          <Text className="utp-sub-goods-title taro-text">
            {'\t'}
            {data && data.name}
          </Text>
        </View>
        <View className="utp-sub-goods-moneys">
          {isJifen && (
            <Wrap justifyContent="space-between">
              <Wrap>
                <View style={{ marginLeft: Taro.pxTransform(12) }}>
                  <Txt
                    title={`市场价：¥${Glo.formatPrice(data.actuallyPrice)}`}
                    size={26}
                    color="deep"
                  />
                </View>
              </Wrap>
              {sums && (
                <Txt
                  title={`${sums.orderExchangeTotalPoint}积分 +¥${Glo.formatPrice(
                    sums.actuallyPrice
                  )}`}
                  size={26}
                  color="red"
                />
              )}
            </Wrap>
          )}

          {!isJifen && (
            <Wrap justifyContent="space-between">
              {type === 0 ? (
                <Wrap>
                  <View>
                    <View className="utp-sub-goods-money">
                      <Yuan price={data && data.price} color="#FF2F7B" size={28}></Yuan>
                    </View>
                  </View>
                  <View style={{ marginLeft: Taro.pxTransform(12) }}>
                    <Txt title={`x${data && data.num}`} size={28} color="deep" />
                  </View>
                </Wrap>
              ) : (
                <Wrap>
                  <View className="utp-sub-goods-money">
                    <Yuan price={data && data.price} color="#FF2F7B" size={28}></Yuan>
                  </View>
                  {!!data &&  (
                    <View className="utp-sub-goods-moneyor">¥{data.originPrice / 100}</View>
                  )}
                </Wrap>
              )}
              {type === 1 ? (
                <Txt title={`x${data && data.num}`} size={28} color="deep" />
              ) : (
                <View className="utp-sub-goods-num">
                  <Yuan price={data && data.num * data.price} color="#FF2F7B" size={28}></Yuan>
                </View>
              )}
            </Wrap>
          )}
        </View>
        <View className="utp-sub-goods-more">
          <Text className="utp-sub-goods-more-t">
            {data && data.norm2Value != null
              ? data && data.norm2Value + ';' + data.norm1Value
              : data && data.norm1Value}
          </Text>
        </View>
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
