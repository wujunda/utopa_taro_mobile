import Taro, { useState, useEffect } from '@tarojs/taro';
import request from '../utils/request';

function useSession() {
  const [haveSession, setHaveSession] = useState(false);
  const [count, setCount] = useState(0);

  useEffect(() => {
    return;
    var session = Taro.getStorageSync('session');
    if (!session) {
      request
        .get<any>('/app.do', {
          method: 'account.session.gen'
        })
        .then((data) => {
          console.warn('hooks Session');
          console.warn(data);

          if (data) {
            console.warn('设置session');
            //Taro.setStorage({
            //key: 'session',
            //data: JSON.stringify(data)
            //});
            var session = Taro.getStorageSync('session');
            console.warn('缓存session');
            console.warn(session);
            setHaveSession(true);
          }
        });
    } else {
      setHaveSession(true);
    }
  }, [count]);

  useEffect(() => {
    return () => {};
  }, []);

  const refresh = () => {
    setCount(count + 1);
  };

  return [haveSession, refresh];
}

export default useSession;
