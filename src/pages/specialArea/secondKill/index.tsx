/* eslint-disable no-unused-vars */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable import/first */
import Taro, {
  usePageScroll,
  useState,
  useMemo,
  useEffect,
  useDidShow,
  useRouter,
  pxTransform
} from '@tarojs/taro';
import { View, Text, ScrollView, Image } from '@tarojs/components';
import Wrap from '@/components/Wrap';
import { ViewCountDownCom } from '../components/smallCom';
import Img from '@/components/Img';
import { SPECIAL_IMAGES } from '../common/images';
import './index.scss';
import {
  seckillActiveList,
  getBaseInfo,
  getSeckillActiveListProducts,
  subscribe
} from '../../../services/specialArea';
import { handleRequest, Glo, priceToFixedZore, thousandSeparator } from '@/utils/utils';
import { handleScroll, PriceFormat } from '../common/tool';
import { goGoodsDetail } from '../common/config';
import { wxRegister, shareCallbackList } from '@/utils/wxApi';
import { connect } from '@tarojs/redux';

interface Props {
  uid: string;
}
type PageStateProps = {
  user: {
    inApp: boolean;
    isWeiXinFalg: boolean;
  };
};
// 抢购状态
/*
1=马上抢 2=提醒我 3=取消预约 4=结束 5=超时结束
*/
const SECOND_STATUS = {
  SECOND_NOW: 1,
  REMIND: 2,
  CANCEL: 3,
  BY_OUT: 4,
  OVER_TIME: 5
};
// 状态按钮图片 以及 status desc
const SECOND_BTN_STATUS = {
  [SECOND_STATUS.SECOND_NOW]: {
    text: '马上抢',
    image: SPECIAL_IMAGES.second_buy_now
  },
  [SECOND_STATUS.REMIND]: {
    text: '提醒我',
    image: SPECIAL_IMAGES.second_buy_yellow
  },
  [SECOND_STATUS.CANCEL]: {
    text: '取消预约',
    image: SPECIAL_IMAGES.second_buy_grey
  },
  [SECOND_STATUS.BY_OUT]: {
    text: '已抢完',
    image: SPECIAL_IMAGES.second_buy_grey
  },
  5: {
    text: '已结束',
    image: SPECIAL_IMAGES.second_buy_grey
  }
};
const ViewLineProgressMemo = ({ totalNum, sellNum, maxWidth }) => {
  const lineWidth = 100 - (sellNum / totalNum) * 100;
  return Taro.useMemo(() => {
    return (
      maxWidth && (
        <View className="line-box" style={{ width: (maxWidth && maxWidth) || '' }}>
          <View className="line-box-child" style={{ width: sellNum + '%' }}></View>
        </View>
      )
    );
  }, [totalNum, sellNum, maxWidth]);
};

// 商品item
// eslint-disable-next-line react/no-multi-comp
const ViewSecondGoodsItem = (props: any) => {
  const { item, onClickBtn, killStatus, itemClick } = props;
  const images = 'url(' + SECOND_BTN_STATUS[item.remindState].image + ')';
  const btnText = SECOND_BTN_STATUS[item.remindState].text;
  const [status, setStatus] = useState<any>(killStatus);
  useEffect(() => {
    setStatus(status);
  }, []);
  return Taro.useMemo(() => {
    return (
      item && (
        // eslint-disable-next-line no-shadow
        <View className="goods-item" onClick={(item: any) => itemClick && itemClick(item)}>
          <Wrap type={1} flexDirection="row" top>
            <View className="goods-item-img">
              {item.remindState === SECOND_STATUS.BY_OUT && (
                <View className="goods-item-img-done">已抢完</View>
              )}
              <Img
                src={item.imageUrl}
                width={Taro.pxTransform(280)}
                height={Taro.pxTransform(280)}
              ></Img>
            </View>
            <View className="goods-item-con">
              <View className="goods-item-con-name">{item.productName}</View>
              <View className="goods-item-con-tag">
                {item.tags &&
                  item.tags.map((tag, idx) => {
                    return (
                      <Text key={idx} className="tag-item">
                        {tag}
                      </Text>
                    );
                  })}
              </View>
              <View className="goods-item-con-btn" style={{ backgroundImage: images }}>
                <View className="btn-tag">补贴{'¥' + priceToFixedZore(item.subsidyPrice, 2)}</View>
                <View className="btn-text">
                  <Wrap type={1} flexDirection="row" top justifyContent="space-between">
                    <View className="btn-left">
                      <View className="left-top">
                        <Text
                          style={{
                            display: 'inline-block',
                            fontSize: Taro.pxTransform(24),
                            marginRight: Taro.pxTransform(4)
                          }}
                        >
                          ¥
                        </Text>
                        {priceToFixedZore(item.seckillPrice, 2)}
                      </View>
                      <View className="left-bom">¥{priceToFixedZore(item.originalPrice, 1)}</View>
                    </View>
                    <View
                      className="right-text"
                      onClick={(e) => {
                        e.stopPropagation();

                        onClickBtn && onClickBtn(item);
                      }}
                    >
                      {btnText}
                    </View>
                  </Wrap>
                </View>
              </View>
              <View className="goods-item-con-line" style={{ marginTop: -5, marginLeft: -3 }}>
                <Wrap>
                  <View style={{ marginRight: Taro.pxTransform(20) }}>
                    <ViewLineProgressMemo
                      totalNum={item.virtualSalesNums > 0 && item.virtualSalesNums}
                      sellNum={item.progressBar}
                      maxWidth={100}
                    />
                  </View>
                  <Text
                    style={{
                      fontSize: Taro.pxTransform(24),
                      color: '#FF2F7B',
                      paddingTop: Taro.pxTransform(20)
                    }}
                  >
                    {killStatus === 2 ? '未开抢' : '已售' + item.virtualSalesNums + '+'}
                    {/* {item.virtualSalesNums > 0 ? '已售' + item.virtualSalesNums + '+' : '未开抢'} */}
                  </Text>
                </Wrap>
              </View>
            </View>
          </Wrap>
        </View>
      )
    );
  }, [item, status, killStatus]);
};
/**
 * 时间列表
 * **/
const ViewTimeList = (props: any) => {
  const { onChange, list, acIndex } = props;
  const [activeIndex, setActiveIndex] = useState<number>(acIndex);

  useEffect(() => {
    setActiveIndex(acIndex);
  }, [acIndex]);
  const STATUS_ACTIVE_STY = useMemo(
    () => ({
      background: '#FF2F7B',
      color: '#fff'
    }),
    [acIndex]
  );
  return Taro.useMemo(() => {
    return (
      list && (
        <View className="time-list">
          <ScrollView
            className="time-list"
            scrollLeft={list.length > 5 ? 30 * activeIndex : 0}
            scrollX
            scrollWithAnimation
            lowerThreshold={10}
            upperThreshold={10}
          >
            {list &&
              list.map((item, idx: number) => {
                return (
                  // eslint-disable-next-line react/jsx-key
                  <View key={idx}>
                    <View
                      className="time-list-item"
                      onClick={() => {
                        setActiveIndex(idx);
                        onChange && onChange(item, idx);
                      }}
                    >
                      <View
                        className="item-date"
                        style={{ color: activeIndex === idx ? STATUS_ACTIVE_STY.background : '' }}
                      >
                        {/* 描述 */}
                        {item.labelTime}
                      </View>
                      <View
                        className="item-status"
                        style={idx === activeIndex ? STATUS_ACTIVE_STY : ''}
                      >
                        {item.startValue}
                      </View>
                    </View>
                  </View>
                );
              })}
          </ScrollView>
        </View>
      )
    );
  }, [list, activeIndex, STATUS_ACTIVE_STY]);
};

// 倒计时
const ViewCountDownComMemo = ({ endTime }) => {
  return useMemo(() => {
    return endTime && <ViewCountDownCom endTime={endTime} />;
  }, [endTime]);
};

const PAGE_SIZE = 100;
export const Index: Taro.FC<Props> = (props: any) => {
  const router = useRouter();
  const THEME_ID: any = router.params.zoneId || '20200630023';
  const [topOpa, setTopOpa] = useState<any>(0);
  // 西东操作
  const [isFixed, setFixed] = useState<boolean>(false);
  //商品列表
  const [goodsList, setGoodsList] = useState<any>([]);
  // 活动结束时间
  const [endTime, setEndTime] = useState<any>({});
  //分享参数
  const [shareData, setShareData] = useState<any>({});
  const [dataInfo, setDataInfo] = useState<any>({});
  // 活动时间列表
  const [timeList, setTimeList] = useState<any>([]);
  // 请求活动商品 参数

  const [killStatus, setkillStatus] = useState<any>(null);

  const KILL_TEXT_1 = '距离结束还有';
  const KILL_TEXT_2 = '距离活动开始';
  // 文案
  const [killText, setKillText] = useState<any>(KILL_TEXT_1);

  const [params, setParams] = useState<any>({
    pageNo: 1,
    pageSize: PAGE_SIZE
  });
  // 当前选中高亮活动
  const [defualtActiveIndex, setDefualtActiveIndex] = useState<any>(0);
  // 全部活动是否已经结束
  const [isAllOver, setIsAllOver] = useState<boolean>(false);
  const TOP_STYLE = useMemo(
    () => ({
      backgroundColor: 'rgba(255,91,143,' + topOpa + ')'
    }),
    [topOpa]
  );
  const FIXED_STYLE = useMemo(
    () => ({
      position: isFixed ? 'fixed' : '',
      top: isFixed ? Taro.pxTransform(20) : '',
      zIndex: 100
    }),
    [isFixed]
  );

  /* 
   滚动条滑动 触发
  */
  usePageScroll((e) => {
    let size = e.scrollTop;
    console.log(e);
    if (size >= 150 && !isFixed) {
      setFixed(true);
    }
    if (size < 135 && isFixed) {
      setFixed(false);
    }
    let opa = size / 100;
    console.log(opa);
    // to do
    if (size < 180) {
      setTopOpa(opa);
    }
  });
  /*
   初始化执行
  */
  // useEffect(() => {
  //   // handleScroll();
  // }, []);
  // useEffect(() => {
  //   if (props.user.isWeiXinFalg) {
  //     wxRegister(
  //       shareCallbackList({
  //         title: shareData.shareTitle || 'UTOPA',
  //         desc: shareData.image || '描述',
  //         link: shareData.shareUrl || window.location.href,
  //         imgUrl:
  //           shareData.image ||
  //           'https://image.myutopa.com/Fr0Vlg2LNy2owURS5bDu1sNxfrHl?e=1614217121&token=WNtUYaMiCdpTDzanlE03SjKXLinbmTzkLGXEBYC_:RSZxw6kzc_1z1ua7yQifGs1YLkg='
  //       })
  //     );
  //   }
  // }, [shareData, props.user.isWeiXinFalg]);

  useDidShow(() => {
    _seckillActiveList();
  });

  // 数组排序
  // const ArrFliter = () => {
  //   [activeTimeLis[3], activeTimeLis[4]] = [activeTimeLis[4], activeTimeLis[3]];
  // };

  const _seckillActiveList = async () => {
    Glo.loading('正在加载...');
    // eslint-disable-next-line no-shadow
    let params: any = {
      themeId: THEME_ID
    };
    try {
      let res = await seckillActiveList(params);
      if (handleRequest(res as any)) {
        let data = res.data;
        //分享数据
        setShareData({
          title: data.shareTitle,
          image: data.image,
          desc: data.shareContent,
          shareUrl: data.shareUrl
        });
        wxRegister(
          shareCallbackList({
            title: data.shareTitle || '秒杀活动',
            desc: data.shareContent || '秒杀分享内容',
            link: data.shareUrl || window.location.href,
            imgUrl:
              data.image ||
              'https://image.myutopa.com/Fr0Vlg2LNy2owURS5bDu1sNxfrHl?e=1614217121&token=WNtUYaMiCdpTDzanlE03SjKXLinbmTzkLGXEBYC_:RSZxw6kzc_1z1ua7yQifGs1YLkg='
          })
        );

        setDataInfo(data);
        // 默认初始化活动商品请求参数
        setParams((state) => ({
          ...state,
          themeId: data.themeId,
          columnId: data.columnList[0].id
        }));
        // 活动时间
        let activeTimeLis = data.columnList[0].seckillActivities;

        // //数组位置调换 抢购中 在中间
        // if (activeTimeLis.length > 3) {
        //   let idx: number = activeTimeLis.findIndex((item) => item.start === 1);
        //   if (idx > 0) {
        //     [activeTimeLis[idx], activeTimeLis[2]] = [activeTimeLis[2], activeTimeLis[idx]];
        //   }
        // }

        // //数组位置调换 抢购中 在中间
        // let idx: number = activeTimeLis.findIndex((item) => item.start === 1);
        // if (activeTimeLis.length > 3 && idx > 0) {
        //   [activeTimeLis[idx], activeTimeLis[2]] = [activeTimeLis[2], activeTimeLis[idx]];
        // }
        // 秒杀活动 列表 默认取第一选项活动列表 todo...
        let default_first_activity = activeTimeLis;
        // default_first_activity[index1] = arr.splice(index2, 1, arr[index1])[0];

        //设置活动循环列表
        setTimeList(default_first_activity);

        //  初始化活动默认选中数据
        let activetyIndexItem = await toolActive(default_first_activity);

        //选中初始化设置 to do : data.columnList[0].seckillActivities[activetyIndex].activityId,
        setDefualtActiveIndex(activetyIndexItem.idx);

        setTimeout(() => {
          setDefualtActiveIndex(activetyIndexItem.idx);
        }, 0);

        console.log(activetyIndexItem.idx, 'activetyIndexItem.idx');
        let obj = {
          ...params,
          themeId: data.themeId,
          columnId: data.columnList[0].id,
          activityId: activetyIndexItem.activityId,
          activityGroupId: activetyIndexItem.activityGroupId
        };
        await _getActiveGoodsList(obj);

        // 模式
        setkillStatus(activetyIndexItem.start);

        // // 是否有活动开始
        // let isOver = default_first_activity.every((item) => {
        //   return item.start === 2;
        // });
        // //是否有活动进行中
        // let isIng = default_first_activity.every((item) => {
        //   return item.start === 1;
        // });
        // if (isOver && !isIng) {
        //   setEndTime(activetyIndexItem.activityStartTime / 1000);
        // } else {
        //   setEndTime(activetyIndexItem.activityEndTime / 1000);
        // }
        //  to do 倒计时
      }
    } catch (e) {
    } finally {
      Glo.hideLoading();
    }
  };

  // 活动初始化选中规则
  const toolActive = (arr) => {
    let data: any;
    arr.map((item, index) => {
      item['idx'] = index;
    });

    // 判断是否有正在活动中
    const isActiveIng = () => {
      return arr.some((item) => {
        return item.start === 1;
      });
    };
    // 判断活动列表状态 是否全部已结束
    const bolAllOver = () => {
      return arr.every((item) => {
        return item.start === 0;
      });
    };

    // 所有未开始
    const allNostart = () => {
      return arr.every((item) => {
        return item.start === 2;
      });
    };
    //

    // 有正在活动中
    if (isActiveIng()) {
      data = arr.find((item) => {
        return item.start === 1;
      });
      setEndTime(data.activityEndTime / 1000);
    }
    // 没有正在进行的活动 并且所有未开始
    else if (!isActiveIng() && allNostart()) {
      let toDay = new Date().getTime();
      data = arr.find((item: any) => {
        return item.activityStartTime >= toDay;
      });
      setEndTime(data.activityStartTime / 1000);
      setKillText(KILL_TEXT_2);
    }
    // all over所有活动结束
    else if (bolAllOver()) {
      setIsAllOver(true);
      data = arr[arr.length - 1];
    }
    //没有开始
    else {
      setKillText(KILL_TEXT_2);
      let toDay = new Date().getTime();
      data = arr.find((item: any) => {
        return item.activityStartTime >= toDay;
      });
      setEndTime(data.activityStartTime / 1000);
    }
    return data;
  };

  /* 
   活动状态
    0 结束
    1 开始
    2 未开始
  */
  // 判断活动列表状态 是否全部已结束
  // const bolAllOver = (arr: any) => {
  //   return arr.every((item, index, array) => {
  //     return item.start === 0;
  //   });
  // };

  // // 是否有正在活动中
  // const isActiveIng = (arr) => {
  //   return arr.some((item, index) => {
  //     return item.start === 1;
  //   });
  // };

  // // 查找比当前时间大于的时值 todo..
  // const findFirst = (arr, getItem = true) => {
  //   let toDay = new Date().getTime();
  //   //item.activityStartTime >= toDay;
  //   if (getItem) {
  //     return arr.find((item: any) => {
  //       return item.start === 1;
  //     });
  //   } else {
  //     return arr.findIndex((item: any) => {
  //       return item.start === 1;
  //     });
  //   }
  // };

  // 订阅秒杀 推送 -->app
  const _subscribe = async (item: any) => {
    Glo.loading('...');
    try {
      let res = await subscribe(item);
      if (handleRequest(res as any)) {
        _getActiveGoodsList(params);
        // _seckillActiveList();
      }
    } catch (e) {
    } finally {
    }
  };

  /*
商品按钮 不同状态执行函数
*/
  const goodsItemBtnAction = async (item: any) => {
    let text = SECOND_BTN_STATUS[item.remindState].text;

    switch (item.remindState) {
      case SECOND_STATUS.SECOND_NOW:
        goGoodsDetail(item.productId, item.storeId);

        // window.location.href =
        //   getShopDetailUrl() + `shopDetail?id=${item.productId}&storeId=${item.storeId}`;
        // Taro.navigateTo({
        //   url: `/pages/details/index?id=${item.productId}&businessId=${item.businessId}&storeId=${item.storeId}`
        // });
        break;
      case SECOND_STATUS.REMIND:
        console.log(item.remindState, '----');
        // 提醒我
        _subscribe(item);
        break;
      case SECOND_STATUS.CANCEL:
        break;
      case SECOND_STATUS.BY_OUT:
        break;
      default:
    }
  };
  //获取秒杀商品
  const _getActiveGoodsList = async (obj, isReload: boolean = true) => {
    Glo.loading('数据加载中...');
    let data = {
      ...params,
      ...obj
    };
    let res = await getSeckillActiveListProducts(data);
    try {
      if (handleRequest(res as any)) {
        // eslint-disable-next-line no-shadow
        let data: any = res.data;
        if (isReload) {
          setGoodsList(data.records);
        }
      } else {
      }
      console.log(res);
    } catch (e) {
    } finally {
      Glo.hideLoading();
    }
  };
  const TitleBox = () => {
    return (
      <Image
        style={{ height: Taro.pxTransform(88), width: pxTransform(256) }}
        src={dataInfo.image}
      ></Image>
    );
  };

  return (
    <View className="second-box" style={{ background: dataInfo.backgroupColor, height: '100%' }}>
      {/* {props.user.inApp && (
        <AppBar style={TOP_STYLE} isApp={props.user.inApp} titleBox={TitleBox()} />
      )} */}

      <View
        className="second-box-head"
        style={{ backgroundImage: 'url(' + dataInfo.backgroundImage + ')' }}
      >
        {dataInfo.activityJoinPeopleNum && (
          <View className="total-box">
            <Wrap type={2}>
              <Text className="total-box-num">
                {thousandSeparator(dataInfo.activityJoinPeopleNum)}
              </Text>
              <Text className="total-box-words">人正在抢购!</Text>
            </Wrap>
          </View>
        )}

        <View className="count-down-box" style={{ marginTop: 20, textAlign: 'center' }}>
          {isAllOver === true ? (
            <View>全部活动已结束</View>
          ) : (
            <View>
              <Text style={{ display: 'inline-block' }}>{killText}</Text>
              <ViewCountDownComMemo endTime={endTime} />
            </View>
          )}

          {/* {!isAllOver && (
            <View>
              <Text style={{ display: 'inline-block' }}>距离结束还有</Text>
              <ViewCountDownComMemo endTime={endTime} />
            </View>
          )} */}
        </View>
      </View>
      <View className="content-box">
        <View className="times-box" style={FIXED_STYLE as any}>
          <ViewTimeList
            list={timeList}
            acIndex={defualtActiveIndex}
            onChange={async (item: any, idx) => {
              setkillStatus(item.start);
              setParams((state) => ({
                ...state,
                activityId: item.activityId,
                activityGroupId: item.activityGroupId
              }));
              let data = await {
                ...params,
                activityId: item.activityId,
                activityGroupId: item.activityGroupId
              };
              await _getActiveGoodsList(data);
            }}
          />
        </View>
      </View>
      <View
        className="second-box-goods"
        style={{ marginTop: isFixed ? Taro.pxTransform(110) : '' }}
      >
        {goodsList &&
          goodsList.map((item, idx) => {
            return (
              <View key={item.id}>
                <ViewSecondGoodsItem
                  killStatus={killStatus}
                  itemClick={() => {
                    goGoodsDetail(item.productId, item.storeId);
                    // Taro.navigateTo({
                    //   url: `/pages/details/index?id=${item.productId}&businessId=${item.businessId}&storeId=${item.storeId}`
                    // });
                  }}
                  list={goodsList}
                  item={item}
                  onClickBtn={() => {
                    goodsItemBtnAction(item);
                  }}
                />
              </View>
            );
          })}
      </View>
      <View style={{ textAlign: 'center', color: '#999', fontSize: 12 }}>---我也是有底线的---</View>
    </View>
  );
};

Index.config = {
  navigationBarTitleText: '秒杀',
  disableScroll: true,
  enablePullDownRefresh: true,
  navigationStyle: 'default'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    }
  };
};
export default connect(mapStateToProps as any)(Index as any);
// Index.defaultProps = {};
// export default Index;
