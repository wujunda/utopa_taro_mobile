/**
 * 字符串长度截取
 *  **/
export const spliceStr = (d: string, max: number) => {
  try {
    if (d && d.length > max) {
      d.slice(0, max);
      return d + '..';
    }
    return d;
  } catch (e) {
    return null;
  }
};
/**
 * 保留小数
 * **/
export const priceToFixed = (d: number, retain: number = 2) => {
  try {
    return d.toFixed(retain);
  } catch (e) {
    return null;
  }
};
/**
 * price *100 or /100
 * ago 1=*100  2=/100
 * 有精确度问题
 * to do...
 *  **/
export const PriceFormat = (m: number, ago: number = 2) => {
  try {
    let priceM: any;
    switch (ago) {
      case 1:
        priceM = priceToFixed(m * 100);
        break;
      case 2:
        priceM = priceToFixed(m / 100);
        break;
      default:
        return m;
    }
    return priceM;
  } catch (e) {
    return null;
  }
};

// 倒计时

export const InitEndTime = (endwise: any) => {
  let dd,
    hh,
    mm,
    // eslint-disable-next-line no-unused-vars
    ss: any = null;
  try {
    var time = parseInt(endwise) - new Date().getTime() / 1000;
    if (time <= 0) {
      return false;
    } else {
      dd = Math.floor(time / 60 / 60 / 24);
      hh = Math.floor((time / 60 / 60) % 24)
        .toString()
        .padStart(2, '0');
      mm = Math.floor((time / 60) % 60)
        .toString()
        .padStart(2, '0');
      ss = Math.floor(time % 60)
        .toString()
        .padStart(2, '0');
      return {
        dd: dd,
        hh: hh,
        mm: mm,
        ss: ss
      };
    }
  } catch (e) {
    return null;
  }
};

export const handleScroll = () => {
  // eslint-disable-next-line no-unused-vars
  var startX: Number = 0;
  var startY: Number = 0;
  function touchStart(e: any) {
    try {
      var touch = e.touches[0],
        x = Number(touch.pageX),
        y = Number(touch.pageY);
      startX = x;
      startY = y;
      // eslint-disable-next-line no-shadow
    } catch (e) {
      console.log(e);
    }
  }
  document.addEventListener('touchstart', touchStart);
  var ele: any = document.querySelector('.scrollEle');
  ele.ontouchmove = function (e: any) {
    var point = e.touches[0],
      eleTop = ele.scrollTop,
      eleScrollHeight = ele.scrollHeight,
      eleOffsetHeight = ele.offsetHeight,
      eleTouchBottom = eleScrollHeight - eleOffsetHeight;
    if (eleTop === 0) {
      if (point.clientY > startY) {
        e.preventDefault();
      }
    } else if (eleTop === eleTouchBottom) {
      if (point.clientY < startY) {
        e.preventDefault();
      }
    }
  };
};
