import Taro from '@tarojs/taro';
import { Text, View } from '@tarojs/components';
import './index.scss';

interface IProps {
  price?: number;
  autoDecimal?: boolean;
  sign?: string;
  className?: string;
  style?: any;
  size?: number;
  color?: string;
}

const toDecimal = (num) => {
  if (num) {
    return Number(num) / 100;
  }
  return 0;
};

const Yuan: Taro.FC<IProps> = ({ style, sign, children, price, size = 24, color = '#666' }) => {
  return (
    <View style={style} className="utp-yuan">
      <Text
        className="utp-yuan-unit"
        style={{
          display: 'inline-block;',
          fontSize: Taro.pxTransform(size),
          color: color,
          fontWeight: ['$999', '#666', '#fff'].includes(color) ? 400 : 500
        }}
      >
        {sign}
      </Text>
      {toDecimal(price || children)}
    </View>
  );
};

Yuan.defaultProps = {
  autoDecimal: true,
  sign: '￥'
};

export default Yuan;
