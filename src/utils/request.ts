import Taro from '@tarojs/taro';
//import qs from 'query-string';
import { Glo } from '../utils/utils';
// eslint-disable-next-line no-unused-vars
import { bossRequest, BossApiResponseData } from './request.boss';
import { getSession, renewSession } from '../services/user';

// eslint-disable-next-line import/no-commonjs
var md5 = require('js-md5');

//let BASE_URL = '/testApi';
let BASE_URL = '/api';
//let BASE_URL = 'https://www.nanshig.com';

// 不同环境请求不同地址
const getServerUrl = () => {
  //return 'https://api-test.myutopa.com';
  if (process.env.SERVER_URL) return process.env.SERVER_URL;
  let serverUrl;
  let hostname;
  if (process.env.TARO_ENV === 'h5') {
    hostname = window.location.hostname;
  }
  const domain = {
    prod: 'api',
    dev: 'api-dev',
    test: 'api-test',
    pre: 'api-pre'
  };
  let env = 'dev';
  if (hostname.indexOf('dev') > -1) {
    env = 'dev';
  } else if (hostname.indexOf('test') > -1) {
    env = 'test';
  } else if (hostname.indexOf('pre') > -1) {
    env = 'pre';
  }
  serverUrl = 'https://' + domain[env] + '.myutopa.com';

  if (process.env.TARO_ENV === 'weapp') {
    serverUrl = 'https://api.myutopa.com';
  }
  serverUrl = '/api';

  return serverUrl;
};
// 有 crmcloud 域名的接口
const CRM_CLOUD_URL = () => {
  let serverUrl;
  const hostname = window.location.hostname;
  const domain = {
    prod: 'api-crmcloud',
    dev: 'api-crmcloud-dev',
    test: 'api-crmcloud-test',
    pre: 'api-crmcloud'
  };
  let env = 'pre';
  if (hostname.indexOf('dev') > -1) {
    env = 'dev';
  } else if (hostname.indexOf('test') > -1) {
    env = 'test';
  } else if (hostname.indexOf('pre') > -1) {
    env = 'pre';
  }
  serverUrl = 'https://' + domain[env] + '.myutopa.com';

  if (process.env.TARO_ENV === 'weapp') {
    serverUrl = 'https://api-crmcloud.myutopa.com';
  }

  return serverUrl;
};

//BASE_URL = 'https://api-test.myutopa.com';
BASE_URL = getServerUrl();

// TODO 目前不做缓存
const isDev = process.env.NODE_ENV === 'development' && false;

const SESSION_KEY = 'session';

// let token = Taro.getStorageSync("authorization");

type Method = 'OPTIONS' | 'GET' | 'HEAD' | 'POST' | 'PUT' | 'DELETE' | 'TRACE' | 'CONNECT';

interface ISession {
  createTime: number;
  expireTime: number;
  extendData: any;
  renewTime: number;
  sessionKey: string;
  sessionSecret: string;
  timestamp: number;
}

// @ts-ignore
var objKeySort = (target) => {
  function deepSort(obj, cache = []) {
    if (obj === null || typeof obj !== 'object') {
      return obj;
    }
    // @ts-ignore
    var hit = cache.filter((c) => c.original === obj)[0];
    if (hit) {
      // @ts-ignore
      return hit.copy;
    }

    var copy = Array.isArray(obj) ? [] : {};
    cache.push({
      // @ts-ignore
      original: obj,
      // @ts-ignore
      copy
    });
    Object.keys(obj)
      .sort()
      .forEach((key) => {
        // console.log('生成签名1');
        // console.log(key);
        // console.log(obj[key]);
        //console.log(obj[key].replace(/\//g, '\\/'));
        //if (key && obj[key] && deepSort(obj[key])) {
        //console.log('生成签名2');
        //console.log(obj[key].toString().replace(/\//g, '\\/'));
        //console.log(obj[key].toString().replace(/(\/)/g, '\\$1'));

        //copy[key] = deepSort(obj[key].toString().replace(/(\/)/g, '\\$1'), cache);
        //copy[key] = deepSort(obj[key].toString().replace(/\//g, '\\/'), cache);
        //} else {
        copy[key] = deepSort(obj[key], cache);
      });

    return copy;
  }

  return deepSort(target);
};

let loadingSession = false;
const getWait = () => {
  return new Promise((r) => {
    let time = setInterval(() => {
      let haveSession = Taro.getStorageSync('session');
      if (haveSession) {
        clearInterval(time);
        r();
      }
    }, 100);
    setTimeout(() => {
      clearInterval(time);
    }, 20000);
  });
};

// 保存会话信息
const saveSession = (session: ISession) => {
  Taro.setStorageSync(SESSION_KEY, session);

  Glo.store.dispatch({
    type: 'cart/saveSession',
    payload: {
      data: session
    }
  });
};

// 获取本地存储的会话
const getLocalSession = (): ISession | null => {
  // 从内存
  const stateSession = Glo.store.getState().cart.session;
  if (stateSession) return stateSession;

  // 从缓存
  const storageSession = Taro.getStorageSync(SESSION_KEY);
  if (storageSession) return storageSession;

  return null;
};

export const request = async (url: string, data?: any, method: Method = 'GET', headers = {}) => {
  let sess = getLocalSession();
  let isSession = false;
  if (data && data.method && data.method.indexOf('account.session.gen') > -1) {
    isSession = true;
  }

  // 会话即将过期，手动去续约
  if (
    sess &&
    sess.renewTime &&
    Date.now() > sess.renewTime &&
    data.method.indexOf('account.session.renew') === -1
  ) {
    const newSession = await renewSession({ sessionKey: sess.sessionKey });
    if (newSession && newSession.code === 0) {
      sess = newSession.data as ISession;
      saveSession(sess);
    }
  }

  if (!sess && !isSession && !loadingSession) {
    loadingSession = true;
    const session = await getSession({});
    loadingSession = false;
    saveSession(session.data);
  } else if (loadingSession && !isSession) {
    await getWait();
  }
  if (isDev) {
    const cacheData = Taro.getStorageSync(url);
    if (data) {
      return Promise.resolve(cacheData);
    }
  }
  //var session = Taro.getStorageSync('session');
  let session = getLocalSession();
  // console.log('获取的session');
  // console.log(session);
  let isLogin = Glo.store.getState().cart.isLogin;
  // console.log(session, isLogin, '=======ISlogin=========');
  var getSign = (postData, _session) => {
    // console.log('生成签名');
    // console.log(isLogin);
    //console.log(postData.params);
    //console.log(objKeySort(postData.params));
    if (typeof postData.params === 'string') {
      postData.params = JSON.parse(postData.params);
    }
    if (isLogin) {
      postData.params.accessToken = isLogin.accessToken;
      postData.params.uid = isLogin.uid;
    } else {
      if (!postData.params.uid) {
        postData.params.accessToken = '';
        postData.params.uid = '';
      }
    }
    postData.params = objKeySort(postData.params);
    let paramsStr = JSON.stringify(postData.params);
    paramsStr = paramsStr.replace(/(\/)/g, '\\$1');

    var mixData = `method=${postData.method}&params=${paramsStr}&sessionKey=${postData.sessionKey}&timestamp=${postData.timestamp}&ver=${postData.ver}${_session.sessionSecret}`;

    return md5(mixData);
  };
  if (url.indexOf('api-crmcloud') > -1) {
    data.accessToken = isLogin.accessToken;
    //url = url + '?accessToken=' + isLogin.accessToken;
    // @ts-ignore
    headers['Authorization-App'] = isLogin.accessToken;
  }
  if (session && url.indexOf('api-crmcloud') === -1) {
    var timestamp = Math.floor(Date.now() / 1000);
    data.timestamp = timestamp;
    data.sessionKey = session.sessionKey;
    data.ver = data.ver || '1.0';
    data.sign = getSign(data, session);
    data.params = JSON.stringify(data.params);
  }

  const option = {
    url,
    data,
    method,
    header: {
      Accept: 'application/json',
      //"Content-Type": "application/json",
      'Content-Type': 'application/x-www-form-urlencoded',
      // Authorization: token || (token = Taro.getStorageSync("authorization")),
      ...headers
    }
  };

  if ((data && data.page > 1) || url.endsWith('/readme') || url.includes('/user/starred')) {
    // TODO
  } else {
    //Taro.showLoading({ title: "loading.." });
  }
  const CKECK_LOGINCODE = [99950005, 99950006, 99950007, 100010003]; // 99950005 未登录用户, 99950006 令牌过期, 99950007 令牌续约   100010003  token已过期（crmcloud域名的接口）
  const CKECK_SESSIONCODE = [99950001, 99950002, 99950003, 99950004]; // 99950001 会话过期  99950002 会话续约 99950003 无效会话 99950004 签名失败
  return Taro.request(option)
    .then(async ({ statusCode, data: resData }) => {
      // console.log(statusCode, '---statusCode--', data);
      // 错误码
      if (
        (statusCode === 404 && url.includes('/user/following')) ||
        url.includes('/user/starred')
      ) {
        return null;
      }
      if (statusCode === 401) {
        // TODO
        if (url.includes('/user/starred') || (url.includes('/user') && method === 'GET')) {
          return null;
        }
        throw new Error('Error 401: Required Login!');
      }
      if (statusCode === 403) {
        throw new Error('Error 403: API rate limit exceeded, required login!');
      }

      // 需要登录的接口
      console.warn('是否未登陆');
      if (CKECK_LOGINCODE.includes(resData.code)) {
        Taro.setStorage({
          key: 'isLogin',
          data: ''
        });
        Glo.store.dispatch({
          type: 'cart/logout',
          payload: {}
        });
        Taro.navigateTo({
          url: '/pages/login/index'
        });
        return {};
      } else if (resData.code === 99950002) {
        // 会话续约
        await Taro.removeStorage({ key: SESSION_KEY });
        const oldSession = getLocalSession();
        if (oldSession) {
          const res = await renewSession({ sessionKey: oldSession.sessionKey });

          if (res && res.code === 0 && res.data) {
            saveSession(res.data);
          }
        }
      } else if (CKECK_SESSIONCODE.includes(resData.code)) {
        // H5的话 会话问题重新获取 session 并且刷新当前页
        Taro.removeStorageSync('session');
        if (process.env.TARO_ENV === 'h5') {
          setTimeout(() => {
            window.location.reload();
          }, 200);
        }
      }
      // 错误异常抛出
      if (resData.code !== 0) {
        throw new Error(resData.msg || '系统错误，请稍后重试~~');
      }
      // TODO refactor
      const msg = `Error: code ${statusCode}`;
      if (statusCode >= 200 && statusCode < 300 && resData) {
        if (isDev) {
          Taro.setStorageSync(url, resData);
        }
        return resData;
      }
      throw new Error(msg);
    })
    .catch((error) => {
      console.error('接口出错', error);
      Taro.showToast({
        title: error.message || 'error(─‿─)',
        icon: 'none',
        duration: 1500,
        mask: true
      });
      return {};
    });
  //.finally(() => {
  ////Taro.stopPullDownRefresh();
  ////Taro.hideLoading();
  //});
};

interface ApiResData<T = any> {
  data: T;
  code: number;
  msg: string;
  error_code?: string; // 新电商的错误code编码
}

const transformResult = <T>(res: BossApiResponseData): ApiResData<T> => {
  return {
    data: res.data,
    msg: res.message,
    error_code: res.code,
    code: res.status === 1 ? 0 : -1
  };
};

export enum API_TYPE {
  // eslint-disable-next-line no-unused-vars
  OLD_ESHOP, // 旧电商接口
  // eslint-disable-next-line no-unused-vars
  BOSS_ESHOP, // 新电商接口
  // eslint-disable-next-line no-unused-vars
  CRM_CLOUD // CRM积分接口
}

export default {
  // 通用路径
  APP_DO: '/app.do',
  // 新旧架构类型
  API_TYPE,
  /**
   *
   * @param url 路径 旧架构接口 /app.do； 新架构的正常path
   * @param data 携带在query上面的参数
   * @param {API_TYPE} apiType 默认API_TYPE.OLD_ESHOP为旧电商接口
   * @example
   * // 旧架构请求方法
   * request.get<any>('/app.do', {
   *   method: 'eshop.point.getPointMall',
   *   ver: '1.0',
   *   params: JSON.stringify({})
   * });
   *
   * // 新架构请求使用方法
   * request.get<{uid: string}>(
   *   '/service/xxx',
   *   { businessId: 1, categoryId: 2, storeId: 2 },
   *   request.API_TYPE.BOSS_ESHOP
   * );
   */
  get<T = any>(
    url = '/',
    data = {},
    apiType: API_TYPE = API_TYPE.OLD_ESHOP
  ): Promise<ApiResData<T>> {
    if (apiType === API_TYPE.BOSS_ESHOP) {
      return bossRequest<T>(url, data, 'GET').then((res) => transformResult<T>(res));
    }
    if (apiType === API_TYPE.CRM_CLOUD) {
      return request(CRM_CLOUD_URL() + url, data, 'GET');
    }
    return request(url.indexOf('http') === -1 ? BASE_URL + url : url, data, 'GET');
  },

  /**
   *
   * @param url 路径 旧架构接口 /app.do； 新架构为正常path
   * @param data 携带在query上面的参数
   * @param {API_TYPE} apiType 默认API_TYPE.OLD_ESHOP为旧电商接口
   * @example
   * // 旧架构请求方法
   * request.post<any>('/app.do', {
   *   method: 'eshop.point.getPointMall',
   *   ver: '1.0',
   *   params: JSON.stringify({})
   * });
   *
   * // 新架构请求使用方法
   * request.post<{uid: string}>(
   *   '/service/xxx',
   *   { businessId: 1, categoryId: 2, storeId: 2 },
   *   request.API_TYPE.BOSS_ESHOP
   * );
   */
  post<T = any>(
    url = '/',
    data = {},
    apiType: API_TYPE = API_TYPE.OLD_ESHOP
  ): Promise<ApiResData<T>> {
    if (apiType === API_TYPE.BOSS_ESHOP) {
      return bossRequest<T>(url, data, 'POST').then((res) => transformResult<T>(res));
    }
    if (apiType === API_TYPE.CRM_CLOUD) {
      return request(CRM_CLOUD_URL() + url, data, 'POST');
    }
    return request(url.indexOf('http') === -1 ? BASE_URL + url : url, data, 'POST');
  },

  put<T>(url = '/', data = {}, apiType: API_TYPE = API_TYPE.OLD_ESHOP): Promise<ApiResData<T>> {
    if (apiType === API_TYPE.BOSS_ESHOP) {
      return bossRequest<T>(url, data, 'PUT').then((res) => transformResult<T>(res));
    }
    if (apiType === API_TYPE.CRM_CLOUD) {
      return request(CRM_CLOUD_URL() + url, data, 'PUT');
    }
    return request(url.indexOf('http') === -1 ? BASE_URL + url : url, data, 'PUT');
  },

  delete<T>(url = '/', data = {}, apiType: API_TYPE = API_TYPE.OLD_ESHOP): Promise<ApiResData<T>> {
    if (apiType === API_TYPE.BOSS_ESHOP) {
      return bossRequest<T>(url, data, 'DELETE').then((res) => transformResult<T>(res));
    }
    if (apiType === API_TYPE.CRM_CLOUD) {
      return request(CRM_CLOUD_URL() + url, data, 'DELETE');
    }
    return request(BASE_URL + url, data, 'DELETE');
  }
};
