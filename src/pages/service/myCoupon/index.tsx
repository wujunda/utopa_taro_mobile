import { View, ScrollView } from '@tarojs/components';
import Taro, { useState } from '@tarojs/taro';
import useRequest from '../../../hooks/useRequest';
import useSession from '../../../hooks/useSession';
import { myCouponList } from '../../../services/coupons';
import Safe from '../../../components/safe';
import Card from '../../../components/Card';
import Bar from '../../../components/bar';
import Tab from '../../../components/Tab';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  // const router = useRouter();
  // console.log(router.params.id);
  // 请求数据
  const params = {
    // accessToken: Taro.getStorageSync('login'),
    // orderId: router.params.orderId,
    // orderNo: router.params.orderNo,
    // serviceStatus: router.params.serviceStatus
  };
  const [haveSession] = useSession();
  const [state, update, loading] = useRequest('', myCouponList);
  if (haveSession && state === null && update && !loading) {
    update();
  }
  const [myCouponTabIndex, setmyCouponTabIndex] = useState(0);
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <View className="utp-myCoupon">
            <Bar title="优惠券" />
            <Tab
              type={1}
              barList={['未使用', '已使用', '已过期']}
              defaultActiveKey={myCouponTabIndex}
              onChange={(i) => {
                setmyCouponTabIndex(i);
              }}
            >
              <ScrollView
                refresherEnabled
                refresherBackground="#333"
                className="utp-myCoupon-content"
                scrollY
              >
                {myCouponTabIndex === 0 &&
                  state &&
                  state.data.notUseData &&
                  state.data.notUseData.data.map((im, mn) => {
                    return <Card Title="去使用" key={mn} data={im} />;
                  })}
                {myCouponTabIndex === 1 &&
                  state &&
                  state.data.usedData &&
                  state.data.usedData.data.map((im, mn) => {
                    return <Card Title="已使用" key={mn} data={im} type={1} />;
                  })}
                {myCouponTabIndex === 2 &&
                  state &&
                  state.data.outTimeData &&
                  state.data.outTimeData.data.map((im, mn) => {
                    return <Card Title="已过期" key={mn} data={im} type={1} />;
                  })}
              </ScrollView>
            </Tab>
          </View>
        </View>
      </View>
    </Safe>
  );
};

Index.config = {
  navigationBarTitleText: '优惠券',
  disableScroll: false
  // enablePullDownRefresh: true,
  // backgroundColor: '#f5f5f5'
};

Index.defaultProps = {};
export default Index;
