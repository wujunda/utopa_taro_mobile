import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import './index.scss';
import Wrap from '../../components/Wrap';
import Txt from '../../components/Txt';

interface Props {}
const Index: Taro.FC<Props> = ({}) => {
  return (
    <View
      className="utp-loading utp-cnt"
      style={{ height: Taro.pxTransform(70) }}
    >
      <Wrap>
        <View style={{ width: Taro.pxTransform(12) }}></View>
        <Txt size={24} title="没有更多数据了" />
      </Wrap>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
