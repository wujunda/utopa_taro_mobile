import { AnyAction, Reducer } from 'redux';
import { UserState } from './user';

export interface ConnectState {
  user: UserState;
  cart: any;
}

export interface EffectsCommandMap {
  put: <A extends AnyAction>(action: A) => any;
  call: Function;
  select: <T>(func: (state: ConnectState) => T) => T;
  take: Function;
  cancel: Function;
  [key: string]: any;
}

export type Effect = (action: AnyAction, effects: EffectsCommandMap) => void;

/**
 * T State的接口
 */
export interface CommonModelType<S> {
  namespace: string;
  state: S;
  effects: {
    [key: string]: Effect;
  };
  reducers: {
    [key: string]: Reducer<S, AnyAction>;
  };
  subscriptions: {
    [key: string]: Function;
  };
}
