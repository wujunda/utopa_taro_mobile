import request from '../utils/request';
// eslint-disable-next-line import/first
import Taro from '@tarojs/taro';

// 提交评价
export const addComment = (params) => {
  return request.post<any>('/app.do', {
    method: 'eshop.productComment.app.addComment',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      uid: Taro.getStorageSync('uid'),
      accessToken: Taro.getStorageSync('login'),
      clientType: 1,
      sysId: 1
    })
  });
};

// 获取 退款协商历史记录
export const getHistoryList = (data) => {
  return request.get(
    '/service/app/user/refundOrderConsult/getConsultHistory',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};

// 获取专题首页基础信息
export const getBaseInfo = (params) => {
  return request.post<any>('/app.do', {
    method: 'eshop.special.getBaseInfo',
    ver: '1.0',
    params: JSON.stringify({
      ...params
    })
  });
};

//专区栏目拼团商品列表
export const getcollageShopList = (data) => {
  return request.get(
    'eshop.special.getSeckillActiveListProducts',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};
// 拼团专区首页 商品列表
export const getGroupThemeIndexProduct = (params) => {
  return request.post<any>('/app.do', {
    method: 'neweshop.special.getGroupThemeIndexProduct',
    ver: '1.0',
    params: JSON.stringify({
      ...params
    })
  });
};

//2.25.8	秒杀专区活动展示
export const seckillActiveList = (params) => {
  return request.post<any>('/app.do', {
    method: 'neweshop.special.seckillActiveList',
    ver: '1.0',
    params: JSON.stringify({
      ...params
    })
  });
};

//秒杀活动商品列表
export const getSeckillActiveListProducts = (params) => {
  return request.post<any>('/app.do', {
    method: 'neweshop.special.seckillActiveProductList',
    ver: '1.0',
    params: JSON.stringify({
      ...params
    })
  });
};
/*
 *秒杀商品取消订阅
 */
export const cancelSubscribe = (data) => {
  return request.post(
    '/service/app/seckillProduct/cancleSubscribe',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};
/*
 *秒杀商品订阅
 */
export const subscribe = (data) => {
  return request.post(
    '/service/app/user/seckillProduct/subscribe',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};

/*
栏目查看更多->  
*/

//秒杀活动商品列表
export const getColumnProduct = (params) => {
  return request.post<any>('/app.do', {
    method: 'eshop.special.getColumnProduct',
    ver: '1.0',
    params: JSON.stringify({
      ...params
    })
  });
};

export const getColumnStore = (params) => {
  return request.post<any>('/app.do', {
    method: 'eshop.special.getColumnStore',
    ver: '1.0',
    params: JSON.stringify({
      ...params
    })
  });
};

// 获取积分专区栏目商品列表
export const getPointColumnProductList = (params) => {
  return request.post<any>('/app.do', {
    method: 'neweshop.special.pointColumnProductList',
    ver: '1.0',
    params: JSON.stringify({
      ...params
    })
  });
};
