import request from '../utils/request';
import { IGoods } from '../../../interface/home';
export interface Store {
  name: string;
  id: number;
}

export interface storeData {
  id: number;
  skuId: number;
  bgStoreUrl: string;
  logoUrl: string;
  address: string;
  name: string;
  shareUrl: string;
  logisticsModel: string[];
  star: number;
  labels: any[];
  productImage: string;
  productName: string;
  picUrls: string[];
  salePrice: number;
  originPrice: number;
  couponBatchDtoDataName: string[];
  productId: number;
  businessId: number;
  storeId: number;
  price: number;
  oldPrice: number;
  activeLabel: number;
  description: string;
  selfSupport: number;
  coupons: any[];
  storeLogisticsList: any[];
  storeTypeDesc: string;
  summary: string;
  servicePhone: string;
  longitude: number;
  latitude: number;
  storeMainCategory?: string;
}
export interface ItemType {
  activityId: string;
  productItems: any[];
  picUrls: any[];
  activityName: string;
}
export interface ListType {
  name: string;
  pic: string;
  foreCategoryId: string;
}
export interface IStore {
  data: {
    store: storeData;
    items: any[];
    page: any;
    list: any[];
    records: storeData[] | IGoods[];
    number: number;
  };
}
interface getAccess {
  storeId: number | string;
  categoryId?: number | string;
  businessId?: number | string;
  pageNo?: number;
  pageSize?: number;
  activityId?: number | string;
  activeType?: number | string;
  favoriteId?: number | string;
}
// 商品详情
export const getStoreDetail = (params: getAccess) => {
  return request.post<any>('/app.do', {
    method: 'eshop.newStore.getStoreDetail',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
export const getStoreActivityList = (params: getAccess) => {
  return request.post<any>('/app.do', {
    method: 'eshop.newStore.getStoreActivityList',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};

export const getStoreList = (params: getAccess) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.foreCategory.listRootAndLeavesForeCategoryById',
    ver: '1.0',
    params: JSON.stringify({
      businessId: params.businessId,
      storeId: params.storeId
    })
  });
};
export const getsig = (params: getAccess, pst: any) => {
  // console.log(params,'--pa')
  return request.post<any>('/app.do', {
    method: 'eshop.activity.getActiveProductList',
    ver: '2.0',
    params: JSON.stringify({
      ...params,
      pageNo: pst,
      pageSize: 20
    })
  });
};
// 点击店铺类目
export const getProductsByBusinessId = (params: getAccess, pst?: any) => {
  console.log('获取店铺商品');
  console.log(pst);
  console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.product.getProductsByBusinessId',
    ver: '2.0',
    params: JSON.stringify({
      ...params,
      pageNo: pst.page,
      pageSize: pst.per_page,
      sysId: 1,
      clientType: '1'
    })
  });
};

export const getAdvertisementByStoreId = (params: getAccess) => {
  return request.post<any>('/app.do', {
    method: 'base.advertisement.getAdvertisementByStoreId',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};

// 时尚类店铺展示类目列表
export const getStoreCustomerList = (params: getAccess) => {
  return request.post<any>('/app.do', {
    method: 'eshop.newStore.getStoreCustomerList',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
// 时尚类店铺新品
export const getStoreNewProduct = (params: getAccess) => {
  return request.post<any>('/app.do', {
    method: 'eshop.newStore.getStoreNewProduct',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
//我的店铺收藏列表
export const getFavStoreList = (params: getAccess) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.newStore.getFavStoreList',
    ver: '1.0',
    params: JSON.stringify({
      pageNo: 1,
      pageSize: 20,
      clientType: '1'
    })
  });
};
// 我的店铺收藏删除
export const unfavorite = (params: getAccess) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.unfavorite',
    ver: '1.0',
    params: JSON.stringify({
      clientType: '1',
      favoriteId: params.favoriteId,
      sysId: '1',
      type: '1',
      storeId: params.storeId
    })
  });
};
export const favorite = (params: getAccess) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.favorite',
    ver: '1.0',
    params: JSON.stringify({
      clientType: '1',
      favoriteId: params.favoriteId,
      sysId: '1',
      type: '1',
      storeId: params.storeId
    })
  });
};

export const userGetCoupon = (params: getAccess) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.coupon.userGetCoupon',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      clientType: '1',
      favoriteId: params.favoriteId,
      sysId: '1'
    })
  });
};
// {"accessToken":"HUmDHtdvNlaUZzBzMcUIEfJJHNlGmzbHIksAlkSwVorSKwZPPkziTPZINNcShfLc",
// "clientType":1,"latitude":"23.137551","longitude":"113.422616",
// "storeId":"13520","sysId":1,"uid":"071cddc9b41a49e5ae8edcaef00458dc"}
export const getDetail = (params: getAccess) => {
  return request.post<any>('/app.do', {
    method: 'neweshop.store.app.getStoreDetail',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      clientType: '1',
      sysId: '1'
    })
  });
};
