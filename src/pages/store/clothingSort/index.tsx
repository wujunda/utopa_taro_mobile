import { View, ScrollView, Text, Image } from '@tarojs/components';
import Taro, { useState } from '@tarojs/taro';

import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Tag from '../../../components/Tag';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import Card from '../../../components/Card';
import Score from '../../../components/Score';

import mengban from '../../../assets/clothingBusiness/mengban.png';
import store_icons_down from '../../../assets/shoppingStore/store_icons_down.png';
import icon_phone_orange from '../../../assets/shoppingStore/icon_phone_orange.png';
import store_tag1 from '../../../assets/shoppingStore/store-tag1.png';
import store_tag3 from '../../../assets/shoppingStore/store-tag3@2x.png';
import iconUp from '../../../assets/shoppingStore/store_icons_up.png';
import more from '../../../assets/more.png';
import { images } from '../../../images';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const [Coupon, setCoupon] = useState(false);

  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          {/* <Bar></Bar> */}
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-clothingSort"
            scrollY
          >
            <View
              className="utp-clothingSort-bgWrap"
              style={{
                background: `url(${images.big_bg})`
              }}
            >
              <Image
                src={mengban}
                style={{
                  width: '100%',
                  height: 200
                }}
              ></Image>
            </View>
            <View className="utp-clothingSort-storeInfo">
              <Wrap type={3} top>
                <View className="utp-clothingSort-storeInfo-card">
                  <Wrap type={2} flexDirection="column" top>
                    <View className="utp-clothingSort-titles">
                      <View className="utp-clothingSort-titles-tag">
                        <Tag>自营</Tag>
                      </View>
                      <Text className="utp-clothingSort-title taro-text">
                        {'\t'}
                        店铺名称店铺名称店铺名称店铺名称店铺名称
                      </Text>
                    </View>
                    <View className="utp-clothingSort-storeInfo-score">
                      <Wrap type={2}>
                        <Score num={3} />
                        <Img src={store_tag3} width={24}></Img>
                        <Txt title="扫码自取"></Txt>
                        <Img src={store_tag1} width={24}></Img>
                        <Txt title="即时达"></Txt>
                      </Wrap>
                    </View>
                    <View
                      style={{
                        marginTop: 10,
                        marginBottom: 10
                      }}
                    >
                      <Txt
                        size={20}
                        color="low"
                        title="这里是大概100字左右的商家简介，可以用作店铺公告来使用。在这里展示，可以用作店铺公告来使用。在这里展示可以用作店铺公告来使用。在这里展示有多少字显示多少字……"
                        justifyContent="flex-start"
                      ></Txt>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        position: 'relative'
                      }}
                    >
                      {/* 关闭优惠券时 */}
                      <View
                        className={Coupon ? 'utp-clothingSort-hidden' : ''}
                        onClick={() => {
                          setCoupon(true);
                          // console.log('展开优惠券', Coupon);
                        }}
                      >
                        <Wrap type={1}>
                          <View
                            style={{
                              width: '90%'
                            }}
                          >
                            <Wrap type={2}>
                              <View className="utp-clothingSort-storeInfo-Discount">
                                <Wrap type={2}>
                                  <Text className="utp-clothingSort-storeInfo-Coupon">券</Text>
                                  <Text className="utp-clothingSort-storeInfo-reduction">
                                    99减50
                                  </Text>
                                </Wrap>
                              </View>
                              <View className="utp-clothingSort-storeInfo-Discount">
                                <Wrap type={2}>
                                  <Text className="utp-clothingSort-storeInfo-Coupon">券</Text>
                                  <Text className="utp-clothingSort-storeInfo-reduction">
                                    99减50
                                  </Text>
                                </Wrap>
                              </View>
                            </Wrap>
                          </View>

                          <Img src={store_icons_down}></Img>
                        </Wrap>
                      </View>
                      {/* 打开优惠券时 */}
                      <View
                        className={
                          Coupon ? 'utp-clothingSort-openCoupon' : 'utp-clothingSort-hidden'
                        }
                      >
                        {/* <ScrollView
                          refresherEnabled
                          refresherBackground="#333"
                          className="utp-clothingSort-openCouponScroll"
                          scrollY
                        > */}
                        <View className="utp-clothingSort-scroll">
                          <View>
                            <View
                              style={{
                                marginTop: 10,
                                marginBottom: 10
                              }}
                            >
                              <Txt size={30} bold title="领券" justifyContent="flex-start"></Txt>
                            </View>
                            <Card />
                            <Card />
                            <Card />
                            <Card />
                            <View className="utp-clothingSort-storeInfo-address">
                              <View
                                style={{
                                  marginTop: 10,
                                  marginBottom: 10
                                }}
                              >
                                <Txt size={30} bold title="地址" justifyContent="flex-start"></Txt>
                              </View>
                              <Wrap type={1} justifyContent="space-between">
                                <Text className="utp-clothingSort-storeInfo-addressInfo">
                                  我是个地址我是个地址我是个地址我是个地址我是个地址
                                </Text>
                                <View onClick={() => {}}>
                                  <Img src={icon_phone_orange} width={32}></Img>
                                </View>
                              </Wrap>
                            </View>
                          </View>
                        </View>
                        {/* </ScrollView> */}
                        <View
                          className="utp-clothingSort-iconUp"
                          onClick={() => {
                            setCoupon(false);
                            // console.log('合上优惠券', Coupon);
                          }}
                        >
                          <Img src={iconUp} width={60}></Img>
                        </View>
                      </View>
                    </View>
                  </Wrap>
                  <View onClick={() => {}} className="utp-clothingSort-storeIcon">
                    <Img
                      src="https://dss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3256100974,305075936&fm=26&gp=0.jpg"
                      width={120}
                    ></Img>
                  </View>
                </View>
              </Wrap>
            </View>
            <View
              className="utp-clothingSort-sortInfo"
              style={{ display: Coupon ? 'none' : 'block' }}
            >
              <View className="utp-clothingSort-sortInfo-card">
                <View
                  style={{
                    marginTop: 10,
                    marginBottom: 14
                  }}
                >
                  <Wrap justifyContent="space-between">
                    <Txt size={32} bold title="男装" justifyContent="flex-start"></Txt>
                    <Img src={more} width={24}></Img>
                  </Wrap>
                </View>
                <Wrap justifyContent="space-between">
                  {['男上衣', '男下装', '内衣', '男上装', '男上装'].map((item) => {
                    return <Text className="utp-clothingSort-sortInfo-card-tag">{item}</Text>;
                  })}
                </Wrap>
              </View>
              <View className="utp-clothingSort-sortInfo-card">
                <View
                  style={{
                    marginTop: 10,
                    marginBottom: 14
                  }}
                >
                  <Wrap justifyContent="space-between">
                    <Txt size={32} bold title="女装" justifyContent="flex-start"></Txt>
                    <Img src={more} width={24}></Img>
                  </Wrap>
                </View>
                <Wrap justifyContent="space-between">
                  {['女上衣', '女下装', '内衣', '女上装', '女上装'].map((item) => {
                    return <Text className="utp-clothingSort-sortInfo-card-tag">{item}</Text>;
                  })}
                </Wrap>
              </View>
              <View className="utp-clothingSort-sortInfo-card">
                <View
                  style={{
                    marginTop: 10,
                    marginBottom: 14
                  }}
                >
                  <Wrap justifyContent="space-between">
                    <Txt size={32} bold title="童装" justifyContent="flex-start"></Txt>
                    <Img src={more} width={24}></Img>
                  </Wrap>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};
Index.config = {
  navigationBarTitleText: '店铺',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};

Index.defaultProps = {};
export default Index;
