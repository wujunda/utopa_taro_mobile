import { View, Text } from '@tarojs/components';
import Taro, { useReachBottom, useEffect, useState, useRouter } from '@tarojs/taro';
import Bar from '../../../components/bar';
import Goods1 from '../../../components/Goods1';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import Safe from '../../../components/safe';
import Wrap from '../../../components/Wrap';
import Tag from '../../../components/Tag';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import useSession from '../../../hooks/useSession';
import useLogin from '../../../hooks/useLogin';
import useResetCount from '../../../hooks/useResetCount';
import { getDetail, storeData } from '../../../services/store';
import { ProductBaiscData } from '../../../interface/product';
import Score from '../../../components/Score';
import './index.scss';
import store_tag2 from '../../../assets/shoppingStore/store-tag2.png';
import ads from '../../../assets/ads.png';
import store_tag1 from '../../../assets/shoppingStore/store-tag1.png';
import store_tag3 from '../../../assets/shoppingStore/store-tag3@2x.png';
import store_tag4 from '../../../assets/shoppingStore/store-tag4.png';
import right from '../../../assets/shoppingStore/right.png';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const [info, setInfo] = useState({} as storeData);
  const [haveSession] = useSession();
  const [isLogin, updateLogin] = useLogin();
  const router = useRouter();
  let payload = {
    storeId: router.params.storeId,
    latitude: router.params.latitude,
    longitude: router.params.longitude
  };
  // console.log(payload, '---');
  // 购物车数据
  // @ts-ignore
  useEffect(() => {
    const getInfo = async () => {
      let res = await getDetail(payload);
      if (res.code === 0) {
        setInfo(res.data);
      }
    };
    getInfo();
  }, []);

  return (
    <Safe>
      <View className="utp-html">
        {/* <View className="utp-body"> */}
        {/* <Bar title="店铺信息" /> */}
        <View className="utp-storeinfo">
          <View className="utp-storeinfo-top">
            <Wrap alignItems="center" Myheight={180}>
              <View className="utp-storeinfo-top-logo">
                <Img src={info.logoUrl} width={120} />
              </View>
              <View>
                <Txt title={info.name} bold size={30} color="deep" height={44} />
                <View style={{ height: 4 }}></View>
                <Wrap>
                  <Score num={info.star} />
                  {info.selfSupport && <Tag>自营</Tag>}
                </Wrap>
              </View>
            </Wrap>
          </View>
          <View className="utp-storeinfo-gray"></View>
          <View className="utp-storeinfo-bottom">
            <View className="utp-storeinfo-bottom-item">
              <Txt title="基本信息" color="low" size={28} justifyContent="flex-start" height={62}  />

              <Txt
                title={
                  info.summary && info.summary != null
                    ? `简介： ${info.summary}`
                    : '简介：暂无店铺介绍'
                }
                size={26}
                color="black"
                justifyContent='flex-start'
              />

              <View className="utp-storeinfo-bottom-server">
                <Txt title="服务：" size={26} color="black" />
                {Array.isArray(info.storeLogisticsList) &&
                  info.storeLogisticsList.map((i, index) => {
                    return (
                      <View key={index} className="utp-storeinfo-bottom-labels">
                        <Wrap>
                          <Img
                            src={
                              i.logisticsModeDesc === '即时达' || i.logisticsModeDesc === '周边送'
                                ? store_tag1
                                : i.logisticsModeDesc === '扫码自取'
                                ? store_tag3
                                : i.logisticsModeDesc === '门店自提'
                                ? store_tag4
                                : i.logisticsModeDesc === '全国送'
                                ? store_tag2
                                : store_tag2
                            }
                            width={24}
                          />
                          <Txt
                            title={
                              i.logisticsModeDesc === '周边送' ? '即时达' : i.logisticsModeDesc
                            }
                          />
                        </Wrap>
                      </View>
                    );
                  })}
              </View>
            </View>
            <View className="utp-storeinfo-bottom-item">
              <Txt title="认证信息" color="low" size={28} justifyContent="flex-start" height={62} />
              <Wrap justifyContent="space-between">
                <Wrap Myheight={36}>
                  <Txt title="认证类型 ：" size={26} color="black" />
                  <Txt title={info.storeTypeDesc} size={26} color="black" />
                </Wrap>

                <Wrap>
                  <Text className="utp-storeinfo-bottom-blue">查看认证资质</Text>
                  <Img src={right} width={20} />
                </Wrap>
              </Wrap>
              {/* <View style={{ height: 10 }}></View> */}
              {/* <Txt title="实体认证：已通过" size={26} color="black" justifyContent="flex-start" /> */}
            </View>
            {/* <View className="utp-storeinfo-bottom-item">
                <Txt
                  title="门店信息"
                  color="low"
                  size={28}
                  justifyContent="flex-start"
                  height={62}
                />
                <Wrap justifyContent="space-between">
                  <Wrap alignItems="flex-start">
                    <Txt title="门店地址：" size={26} color="black" />
                    <Text className="utp-storeinfo-bottom-address">
                      这里是展示店完全显示，有多少完全显示，有多少完全显示，有多少铺地址，显示多少，行数不固定
                    </Text>
                  </Wrap>
                  <Img src={ads} width={48} />
                </Wrap>
                <View className="utp-storeinfo-bottom-crevice"></View>
                <Wrap alignItems="flex-start">
                  <Txt title="联系电话：" size={26} color="black" />
                  <Text className="utp-storeinfo-bottom-address">
                    020-3258456、020-3258456 020-3258456 020-3258456
                  </Text>
                </Wrap>
                <View className="utp-storeinfo-bottom-crevice"></View>
                <Wrap alignItems="flex-start">
                  <Txt title="营业时间：" size={26} color="black" />
                  <Text className="utp-storeinfo-bottom-address">
                    周一：9：00-12：00、14：00-18：00 周二：9：00-12：00、14：00-18：00
                  </Text>
                </Wrap>
              </View> */}
          </View>
        </View>
        {/* </View> */}
      </View>
    </Safe>
  );
};

Index.config = {
  navigationBarTitleText: '店铺信息',
  disableScroll: false
  //navigationStyle: 'custom',
  //usingComponents: {
  //Bar: '../../../components/bar' // 书写第三方组件的相对路径
  //}
};
Index.defaultProps = {};
export default Index;
