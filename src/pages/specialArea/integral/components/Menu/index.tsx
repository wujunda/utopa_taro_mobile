import Taro from '@tarojs/taro';
import { View, Image } from '@tarojs/components';
import JSBridge from '@/utils/jsbridge/index';

import './index.scss';

const Menu = (props) => {
  const { classes } = props;

  const toCmd = (cmdParams) => {
    JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, () => {
      console.log('执行', cmdParams);
    });
  };

  return (
    <View className="integral-mall-menu">
      <View className="menu-line-one">
        {!!classes &&
          classes.map((item, index) => {
            if (index < 3) {
              return (
                <Image src={item.image} className="item" onClick={() => toCmd(item.command)} />
              );
            }
          })}
      </View>
      <View className="menu-line-two">
        {!!classes &&
          classes.map((item, index) => {
            if (index >= 3) {
              return (
                <Image src={item.image} className="item" onClick={() => toCmd(item.command)} />
              );
            }
          })}
      </View>
    </View>
  );
};

export default Menu;
