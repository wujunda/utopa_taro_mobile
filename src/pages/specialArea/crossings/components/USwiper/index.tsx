import Taro, { useState } from '@tarojs/taro';
import { Swiper, SwiperItem, Image, View } from '@tarojs/components';
import JSBridge from '@/utils/jsbridge/index';

import './index.scss';

const USwiper = (props) => {
  const { banners, inApp } = props;
  const [active, setActive] = useState(0);

  function swiperChange(e) {
    setActive(e.detail.current);
  }

  const toDetails = (item) => {
    if (inApp) {
      JSBridge.Common.GTBridge_Common_Base_CMD(item.command, () => {
        console.log('执行', item.command);
      });
    } else {
      item.url && (window.location.href = item.url);
    }
  };

  return (
    <View>
      <Swiper
        className="uSwiper"
        indicatorDots={false}
        current={active}
        circular
        autoplay={false}
        duration={1000}
        onChange={swiperChange}
      >
        {!!banners &&
          banners.map((item, index) => (
            <SwiperItem key={item.id} className="uSwiper-item" onClick={() => toDetails(item)}>
              <View>
                <Image
                  src={item.image}
                  className={`uSwiper-item-image ${active === index ? 'uSwiper-item-active' : ''}`}
                />
              </View>
            </SwiperItem>
          ))}
      </Swiper>
      <View className="uSwiper-dots">
        {!!banners &&
          banners.map((item, index) => (
            <View
              key={item.id}
              className={`uSwiper-dots-item ${active === index ? 'uSwiper-dots-active' : ''}`}
            />
          ))}
      </View>
    </View>
  );
};

export default USwiper;
