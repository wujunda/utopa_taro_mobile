import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';

import './index.scss';
import Img from '../Img';
import Wrap from '../Wrap';
import { Glo } from '@/utils/utils';

interface GoodsItem {
  num?: number;
  picIds: String;
  productName: String;
  norm1Name: String;
  price: number;
  image: String;
}
interface IProps {
  item: GoodsItem;
  options?: any; // 修改显示字段
}
const RefundGoodsItem: Taro.FC<IProps> = ({ item, options }) => {
  return (
    <View className="good-item">
      <Wrap type={2} flexDirection="row">
        <View className="items-img">
          {
            !!item && !!options  && !!options.image && (
              <Img src={options.image ? item[options.image] : item.image} width={180}></Img>
            ) 
          }
        </View>
        <View className="items-content">
          <Text className="goods-name">
            {options && options.name ? item[options.name] : item.productName}
          </Text>
          <View className="goods-spec">
            {options && options.norm1Name ? item[options.norm1Name] : item.norm1Name}
          </View>
          <View className="goods-by">
            <Wrap type={2} flexDirection="row">
              <View className="goods-price">
                <Wrap type={2} flexDirection="row">
                  <Text className="p-text">单价：</Text>
                  <Text className="p-num">
                    { !!item && !!options && !!options.price  &&  Glo.formatPrice(!!item[options.price] ? item[options.price] : item.price)}
                  </Text>
                </Wrap>
              </View>
              <View className="goods-num">
                <Wrap type={2} flexDirection="row">
                  <Text className="n-text">数量：</Text>
                  <Text className="n-num">
                    {options && options.num ? item[options.num] : item.num}
                  </Text>
                </Wrap>
              </View>
            </Wrap>
          </View>
        </View>
      </Wrap>
    </View>
  );
};
RefundGoodsItem.defaultProps = {};

export default RefundGoodsItem;
