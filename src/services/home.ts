import request from '../utils/request';
import { ISwipe, IIcons, IGoods } from '../interface/home';
export interface CustomCategoryList {
  foreCategoryId: number;
  name: string;
  subTitle: string;
  items: IGoods[];
}
export interface ISec {
  code: number;
  data: {
    customCategoryList: CustomCategoryList[];
  };
}
// 轮播
export interface AdvertisementList extends ISwipe {
  image: string;
  url: string;
  params: string;
  id: string;
}
export interface ILunbo {
  code: string;
  advertisementList: AdvertisementList[];
}
// 小图标
export interface IMiniApp extends IIcons {
  iconUrl: string;
  title: string;
  id: number;
}
export interface IMini {
  groupCode: string;
  items: IMiniApp[];
}
// 大图广告
export interface IBig {
  code: string;
  advertisementList: AdvertisementList[];
}

export interface IHome {
  code: number;
  data: {
    advertisementSpaceList: ILunbo[] | IBig[];
    memberConfList: IMini[];
  };
}

export const getHome = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.homepage.getFirstPageData',
    ver: '2.0',
    params: JSON.stringify(params)
  });
};
export const getSec = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.homepage.getSecondPageData',
    ver: '2.0',
    params: JSON.stringify(params)
  });
};
export const getSecMore = (params: any, pst: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.product.getCustomCategoryList',
    ver: '2.0',
    params: JSON.stringify({
      foreCategoryId: params.split('%%')[1],
      pageSize: pst.per_page,
      pageNo: pst.page
    })
  });
};
