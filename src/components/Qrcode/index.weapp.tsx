import { Canvas } from '@tarojs/components';
import './index.scss';
import drawQrcode from '../../utils/weapp.qrcode.esm';

import Taro, { Component } from '@tarojs/taro';

export default class Index extends Component {
  componentDidMount() {
    drawQrcode({
      width: this.props.size,
      height: this.props.size,
      canvasId: 'myQrcode',
      text: this.props.value,
      _this: this.$scope
    });
  }

  render() {
    return (
      <Canvas
        style={{ width: this.props.size + 'px', height: this.props.size + 'px' }}
        canvasId="myQrcode"
      />
    );
  }
}
