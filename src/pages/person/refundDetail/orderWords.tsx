import { REFUND_DETAIL_STATUS } from '@/utils/common_enum';
// const REFUND_DETAIL_STATUS = {
//   WAIT_VERIFY: 0, //待商家审核
//   B_REJECT: 1, //商家拒绝
//   WAI_SEND_BACK: 2, // 等待买家退回
//   WAIT_B_RECEIVED: 3, // 商家收货
//   REJECT_RECEIVED: 4, //拒绝收货
//   REFUND_ING: 5, //退款中
//   COMPLETE_REFUND: 6, // 已完成退款
//   CLOSE: 7 //关闭
// };
// eslint-disable-next-line react/react-in-jsx-scope

// 头部状态 标题文字
// eslint-disable-next-line import/prefer-default-export
export const REFUND_STATUS_TEXT: any = {
  [REFUND_DETAIL_STATUS.WAIT_VERIFY]: '您已发起退货申请，请耐心等待商家处理',
  [REFUND_DETAIL_STATUS.B_REJECT]: '商家拒绝退款申请，如果问题还未解决，可以修改后再次发起',
  [REFUND_DETAIL_STATUS.WAI_SEND_BACK]: '商家已同意退款申请，请尽快填写物流单号',
  [REFUND_DETAIL_STATUS.WAIT_B_RECEIVED]: '您已提交退货物流信息，请耐心等待商家处理',
  [REFUND_DETAIL_STATUS.REJECT_RECEIVED]: '商家拒绝退款，请修改物流信息或撤销申请',
  [REFUND_DETAIL_STATUS.REFUND_ING]: '系统正在处理退款，如超过24小时状态未更新，请联系优托邦客服',
  [REFUND_DETAIL_STATUS.COMPLETE_REFUND]:
    '系统已退款至您付款账户，若您24小时未收到，请联系优托邦客服',
  [REFUND_DETAIL_STATUS.CLOSE]: '您撤销申请退款,如问题仍未解决，可重新发起退款'
};
export const REFUND_STATUS_TEXT_HEAD = {
  [REFUND_DETAIL_STATUS.WAIT_VERIFY]: '待商家审核',
  [REFUND_DETAIL_STATUS.B_REJECT]: '已拒绝退款申请，待买家处理',
  [REFUND_DETAIL_STATUS.WAI_SEND_BACK]: '请买家退回商品，并填写物流单号',
  [REFUND_DETAIL_STATUS.WAIT_B_RECEIVED]: '待商家收货处理',
  [REFUND_DETAIL_STATUS.REJECT_RECEIVED]: '商家拒绝退款，待买家处理',
  [REFUND_DETAIL_STATUS.REFUND_ING]: '商家同意退款，系统处理中',
  [REFUND_DETAIL_STATUS.COMPLETE_REFUND]: '已完成退款',
  [REFUND_DETAIL_STATUS.CLOSE]: '退款关闭'
};

export const WAIT_B_RECEIVED_TEXT_LIST = [
  {
    text: '商家同意退货或超时未处理，系统将退款给您'
  },
  {
    text: '如果商家拒绝,您可以修改物流信息，商家会重新处理'
  }
];
export const LIST_TEXT = [
  {
    text: '商家同意退款或超时未处理,请您及时退货货物'
  },
  {
    text: '如果商家超时未处理,且设置退货地址,系统将退款给您。'
  },
  {
    text: '如果商家拒绝,您可以修改退款信息，商家会重新处理'
  }
];
