
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

import './index.scss';

function Index(props) {
  return (
    <View className="utp-integral-coupon"/>
  );
}

Index.config = {
  navigationBarTitleText: '积分规格',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};
// @ts-ignore
export default Index;