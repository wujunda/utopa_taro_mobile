import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

import './index.scss';

type PageStateProps = {
  // counter: {
  //   num: number;
  // };
};

type PageDispatchProps = {
  // add: () => void;
  // dec: () => void;
  // asyncAdd: () => any;
  children: any;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = () => {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */

  return (
    <View className="utp-safe">
      <View className="utp-safe">{this.props.children}</View>
    </View>
  );
};

Index.defaultProps = {};
export default Index;
