import Taro from '@tarojs/taro';
import { connect } from '@tarojs/redux';

import { View } from '@tarojs/components';
import Router from '../../../utils/router';
import Wrap from '../../../components/Wrap';

import ItemHomeIcon1 from '../../../components/ItemHomeIcon1';

type PageStateProps = {
  item: any[];
};

type PageDispatchProps = {};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

//let cishu: number = 0;
const Index: Taro.FC<IProps> = (props) => {
  let arr: any[] = [];
  !!props && !!props.item && props.item.map((item1) => {
    if (item1.command.cmd === 'C0080100') {
      return;
    } else {
      arr.push(item1);
    }
  });
  return (
    <Wrap type={1} justifyContent="space-between">
      {!!arr &&arr.map((item1) => {
        //if (item1.command.cmd === 'C0080100') {
        //return;
        //}
        item1.source = item1.iconUrl;
        return (
          <View className="utp-home-fours-ico">
            <ItemHomeIcon1
              tips={item1.title}
              src={item1.source}
              tipColor="#333"
              onClick={() => {
                // @ts-ignore
                Router.goHome(item1.command);
              }}
            />
          </View>
        );
      })}
    </Wrap>
  );
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {};
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
