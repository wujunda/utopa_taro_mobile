import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import Wrap from '../../../../components/Wrap';

interface IProps {
  item: any;
  key: any;
}
const TEXT_COLOR = '#FF2F7B';
const priceStyle = {
  color: TEXT_COLOR
};
const AWAIT_INFO: any = [
  {
    text: '维权类型',
    field: 'refundType',
    textColor: {},
    isMoney: false
  },
  {
    text: '商品退款',
    field: 'goodsRefundFee',
    textColor: priceStyle,
    isMoney: true
  },
  {
    text: '运费退款',
    field: 'postFefundFee',
    textColor: priceStyle,
    isMoney: true
  },
  {
    text: '退款原因',
    field: 'refundReason',
    textColor: {}
  },
  {
    text: '拒绝原因',
    field: 'refuseRefundReason'
  },
  {
    text: '补充说明',
    field: 'refundInstructions',
    textColor: ''
  },
  {
    text: '快递公司名称',
    field: 'companyName'
  },
  {
    text: '物流单号',
    field: 'expressNo'
  },
  {
    text: '退款说明',
    field: 'refundInstructions'
  },
  {
    text: '退款描述',
    field: 'refuseRefundDescription'
  },
  {
    text: '退货地址',
    field: 'returnGoodsAddress'
  }
];

const Index: Taro.FC<IProps> = ({ item, key }) => {
  return (
    <Wrap type={2} flexDirection="column">
      <View className="history-item">
        <Wrap type={2} flexDirection="row">
          <View className="history-item-icon">
            {/* <Img src="" ></Img> */}
            <View
              className="history-item-icon-spot"
              style={{ background: key === 0 ? TEXT_COLOR : '#DDDDDD' }}
            ></View>
          </View>
          <View className="history-item-title">{item.title}</View>
        </Wrap>
        {item.remark && <View>{item.remark}</View>}
        {!!item &&
          !!item.contentDTO &&
          !!item.contentDTO.companyName &&
          !!item.contentDTO.expressNo && (
            <View className="history-item-logis" onClick={() => {}}>
              <Text>查看物流</Text>
            </View>
          )}
        {!!item && !!item.contentDTO && (
          <View>
            {AWAIT_INFO.map((o, idx) => {
              return (
                item[o.field] && (
                  <View className="history-item-info" key={idx}>
                    <Wrap type={2} flexDirection="row">
                      <Text>{o.text}：</Text>
                      <Text style={o.textColor}>
                        {item[o.field]}
                        {/* {o.isMoney && '￥'}
                    {o.isMoney ? handleToFixed(item[o.field], 2) : item[o.field]} */}
                      </Text>
                    </Wrap>
                  </View>
                )
              );
            })}
          </View>
        )}
        <View className="history-item-time">{item.createTime}</View>
      </View>
    </Wrap>
  );
};
Index.defaultProps = {};

export default Index;
