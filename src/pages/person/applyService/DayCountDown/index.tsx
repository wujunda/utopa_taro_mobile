import Taro, { useState } from '@tarojs/taro';
import { View } from '@tarojs/components';
import { InitEndTime } from '@/utils/utils';

interface IProps {
  status: any;
  timeDisparity: any;
}
const statusStr = {
  0: '待商家审核',
  1: '商家已拒绝，您需在',
  2: '待买家退回商品，您需在',
  3: '待商家收货处理退款，商家需在',
  4: '商家已拒绝，您需在',
  5: '商家已同意，系统退款中',
  6: '退款成功',
  7: '退款关闭'
};

const Index: Taro.FC<IProps> = ({ status, timeDisparity }) => {
  console.log(timeDisparity);
  let times = timeDisparity;
  const [timeEnd, setTimeEnd] = useState(InitEndTime(times));
  setTimeout(() => {
    setTimeEnd(InitEndTime(times));
  }, 1000);
  return (
    status && (
      <View style={{ display: 'inline-block', marginLeft: '5px', fontSize: '13px', color: '#333' }}>
        {statusStr[status]}
        <View style={{ display: 'inline-block', color: '#FF2F7B' }}>{timeEnd}</View>内处理
      </View>
    )
  );
};
Index.defaultProps = {};

export default Index;
