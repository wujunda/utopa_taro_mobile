import request from '../utils/request';

export interface ProductsType{
    businessId:number;
    creator:string;
    id:number;
    productId:number;
    qty:number;
    skuId:number;
    skuPic:string;
    storeId:number;
    wishId:number;
    activeLabel:string;
    skuPrice:number;
    price:number;
    check:boolean;
    storeName:string;
    productName:string;
    skuImgs:string[];
    picUrl:string;
    name:string;
    productSkuNorms:any[];
}
export interface ThemeType{
  dataType:number;
  id:number;
  imageUrl:string;
  subTitle:string;
  themeStatement:string;
  title:string;
  sort:number;
}
export interface IMsgInfo {
    id:number;
    productList:ProductsType[];
    products:ProductsType[];
    title:string;
    totalCount:number;
    createTime:string;
    shareStatement:string;
    wiseTheme:string;
    wiseThemeId:number;
}
export interface IMsg {
  code: number;
  data: {
    wiseThemeList:any[];
    records: IMsgInfo[];
  };
}
// 心愿单列表
export const findPageUserWishe = (params: any,pst:any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.wish.findPageUserWish',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      pageNo: pst.page,
      pageSize: pst.per_page,
    })
  });
};
// 心愿单列表
export const getDetail = (params: any) => {
    // console.log(params);
    return request.post<any>('/app.do', {
      method: 'eshop.wish.getDetail',
      ver: '1.0',
      params: JSON.stringify({
        ...params,
        clientType:1,
      })
    });
  };
    // 心愿单主题列表
export const getWiseThemeList = (params?: any) => {
    return request.post<any>('/app.do', {
      method: 'eshop.special.getWiseThemeList',
      ver: '1.0',
      params: JSON.stringify({
        ...params,
        clientType:1,
      })
    });
  };
  export const update = (params?: any) => {
    return request.post<any>('/app.do', {
      method: 'eshop.wish.update',
      ver: '1.0',
      params:JSON.stringify(params)
    });
  };
  export const save = (params?: any) => {
    return request.post<any>('/app.do', {
      method: 'eshop.wish.save',
      ver: '1.0',
      params:JSON.stringify(params) 
    });
  };
   export const getWiseThemeProduct = (params: any) => {
    return request.post<any>('/app.do', {
      method: 'eshop.special.getWiseThemeProduct',
      ver: '1.0',
      params: JSON.stringify({
        ...params,
        clientType:1,
        pageNo: 1,
        pageSize: 20,
      }) 
    });
  };
  // {"accessToken":"fgtJCmbzvteLcjgKGlufwTUatNLRoZFDlRGAXEcqqGVnqasATOznNLXZsOkkownK",
  // "clientType":1,
  // "nickName":"用户1782217",
  // "sysId":1,
  // "uid":"e3b41a82adb0401dafeccc059a7648b4",
  // "userAddressId":"17146",
  // "wishId":"5519"}
  // {"accessToken":"URQDboPyjRNrEwAzcWCuMutNebKzKvWFlmigJoWmMGYjpCAqhhJnAiLAFAztvgPa",
  // "clientType":1,"nickName":"786","sysId":1,"uid":"e3b41a82adb0401dafeccc059a7648b4","userAddressId":"17146","wishId":"5519"}
  //设置分享
export const saveShare = (params: any) => {
    return request.post<any>('/app.do', {
      method: 'eshop.wishShare.saveShare',
      ver: '1.0',
      params: JSON.stringify({
        ...params,
        clientType:1,
        sysId:1,
      }) 
    });
  };