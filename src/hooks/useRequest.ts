import { useState, useEffect, useRef, usePullDownRefresh } from '@tarojs/taro';
import events from '@/utils/event_bus';
import { PULL_DOWN_REFRESH_EVENT } from '@/constants';
import { Glo } from '../utils/utils';

function useRequest<T>(
  params: any,
  request: (params: any) => Promise<T | null>,
  option: { isFetch?: boolean } = {}
): [T | null, () => void, boolean] | [] {
  const { isFetch = true } = option;
  const [currData, setData] = useState<T | null>(null);
  const [count, setCount] = useState(0);
  const pagePullDownRef = useRef('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (params.toString().indexOf('needLogin') > -1 && !Glo.store.getState().cart.isLogin) {
      return;
    }
    if (isFetch) {
      setLoading(true);
      request(params).then((data) => {
        setLoading(false);
        if (data) {
          setData(data);
        }
      });
    }
  }, [count]);

  const refresh = () => {
    setCount(count + 1);
  };

  usePullDownRefresh(() => {
    // console.log('触发了下拉');
    if (isFetch) {
      refresh();
    }
  });

  useEffect(() => {
    events.on(PULL_DOWN_REFRESH_EVENT, (page: string) => {
      if (!pagePullDownRef.current) {
        pagePullDownRef.current = page;
      } else if (pagePullDownRef.current !== page) {
        return;
      }
      if (isFetch) {
        refresh();
      }
    });
    return () => {
      events.off(PULL_DOWN_REFRESH_EVENT);
    };
  }, []);


  return [currData, refresh, loading];
}

export default useRequest;
