import Taro, { useEffect ,useState} from '@tarojs/taro';
import { View ,Camera,CoverView} from '@tarojs/components';
import './index.scss';

interface IProps {
}

const Index: Taro.FC<IProps> = () => {
  const [position,setPosition] = useState([100,100,260,260])
  useEffect(()=>{
    Taro.getSystemInfo({
      success:(rr)=>{
        let w = rr.screenWidth/2-130;
        setPosition ([w,150,260,260]);
      }
    })
  },[])
    const result = (res) =>{
        Taro.showToast({title:res.detail.result})
    }
  return (
      <View className="utp-scan">
          <Camera scanArea={position} className="utp-scan-box" mode="scanCode" flash="off" onScanCode={result}></Camera>
          <CoverView className="utp-scan-area"></CoverView>
          <CoverView className="utp-scan-comontip utp-scan-top">
              扫一扫
          </CoverView>
          <CoverView className="utp-scan-comontip utp-scan-mask">
          </CoverView>
          <CoverView className="utp-scan-tip1 utp-scan-comontip">将商品条码/二维码对准框内，即可自动扫描</CoverView>
          <CoverView className="utp-scan-tip2 utp-scan-comontip ">暂不支持此类型扫码</CoverView>
      </View>
  );
};
Index.defaultProps = {};
export default Index;
