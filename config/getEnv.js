// eslint-disable-next-line import/no-commonjs
module.exports = {
  SERVER_ENV: JSON.stringify(process.env.SERVER_ENV), // 当前构建的环境
  SERVER_URL: JSON.stringify(process.env.SERVER_URL), // 就架构接口地址
  BOSS_SERVER_URL: JSON.stringify(process.env.BOSS_SERVER_URL), // 新架构接口
  APP_KEY: JSON.stringify(process.env.APP_KEY), // appkey
}