import { View, ScrollView, Text, Image, SwiperItem, Swiper } from '@tarojs/components';
import Taro, {
  useState,
  useEffect,
  useRouter,
  useDidMount,
  useRef,
  pxTransform
} from '@tarojs/taro';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import ItemHomeIcon from '../../../components/ItemHomeIcon';
import ItemMineIcon from '../../../components/ItemMineIcon';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Tag from '../../../components/Tag';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import Card from '../../../components/Card';
import Score from '../../../components/Score';
import { IGoods } from '../../../interface/home';
import Goods3 from '../../../components/Goods3';
import Goods2s from '../../../components/Goods2s';
import store_tag3 from '../../../assets/shoppingStore/store-tag3@2x.png';
import store_tag4 from '../../../assets/shoppingStore/store-tag4.png';
import mengban from '../../../assets/shoppingStore/newBg.png';
import store_icons_down from '../../../assets/shoppingStore/store_icons_down.png';
import white from '../../../assets/shoppingStore/white.png';
import store_tag1 from '../../../assets/shoppingStore/store-tag1.png';
import useSession from '../../../hooks/useSession';
import iconUp from '../../../assets/shoppingStore/store_icons_up.png';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import cate from '../../../assets/shoppingStore/cate2.png';
import Acate from '../../../assets/tab-bar/cate-active.png';
import pro from '../../../assets/tab-bar/pro.png';
import APro from '../../../assets/tab-bar/pro-active.png';
import cart from '../../../assets/shoppingStore/cart.png';
import Acart from '../../../assets/tab-bar/cart-active.png';
import store_tag2 from '../../../assets/shoppingStore/store-tag2.png';
import Router from '../../../utils/router';
import {
  ItemType,
  ListType,
  IStore,
  storeData,
  getStoreCustomerList,
  getStoreActivityList,
  getStoreNewProduct,
  getStoreList,
  getStoreDetail,
  getAdvertisementByStoreId,
  getProductsByBusinessId,
  userGetCoupon
} from '../../../services/store';
import './index.scss';
import useResetCount from '../../../hooks/useResetCount';

interface AdvTypa {
  image: string;
  adverItems: any[];
}
interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const router = useRouter();
  const [top, setTop] = useState(0);
  const [Coupon, setCoupon] = useState(false);
  const [TabIndex, setTabIndex] = useState(0);
  const [Fixed, setFixed] = useState(false);
  const [data, setData] = useState({} as storeData);
  const [list, setList] = useState([] as ListType[]);
  const [advs, setAdvs] = useState([] as AdvTypa[]);
  const [categorys, setCategorys] = useState([] as ItemType[]);
  const [discount, setDiscount] = useState([] as ItemType[]);
  const [newProduct, setnewProduct] = useState([]);
  const [active, setActive] = useState(0);
  const [tabs, setTabs] = useState([
    { active: Acart, unActive: cart, title: '' },
    { active: APro, unActive: pro, title: '全部宝贝' },
    { active: Acate, unActive: cate, title: '宝贝分类' },
    { active: Acart, unActive: cart, title: '购物车' }
  ]);
  function onScroll(e) {
    if (active === 0) {
      const query = Taro.createSelectorQuery();
      query.select('.utp-clothingBusiness-clothingTabList').boundingClientRect();
      query.selectViewport().scrollOffset();
      query.exec(function (res) {
        res[0].top; //节点的上边界坐标
        res[1].scrollTop; // 显示区域的竖直滚动位置
        // console.log('我的高度是', e.detail.scrollTop, res[0].top);
        var size = res[0].top;
        if (size <= 48 && Fixed == false && !Coupon) {
          setFixed(true);
        } else if (size > 48 && Fixed == true) {
          setFixed(false);
        } else {
          return;
        }
      });
    }
  }
  const [haveSession] = useSession();
  const [state, hasMore, update, , loading] = useRequestWIthMore<IStore>(
    {
      storeId: router.params.storeId,
      businessId: router.params.businessId
    },
    getProductsByBusinessId
  );
  const [can, update1] = useResetCount();
  if (
    haveSession &&
    (state === null || (state != null && state.length == 0)) &&
    update &&
    !loading
  ) {
    if (can) {
      update({});
      update1();
    }
  }
  var goods: IGoods[] = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.data.records.map((it) => {
        goods.push(it);
      });
    });
  }

  //初始化数据
  const init = async () => {
    let payload = {
      storeId: router.params.storeId,
      businessId: router.params.businessId
      // storeId: 13520,
      // businessId: '1002'
    };
    let res0 = await getStoreDetail(payload);
    let res1 = await getStoreList(payload);
    let res2 = await getAdvertisementByStoreId(payload);
    let res3 = await getStoreCustomerList(payload);
    let res4 = await getStoreActivityList(payload);
    let res5 = await getStoreNewProduct(payload);
    if (res0 && res0.data && res0.data != null && res0.data.store != null) {
      let temp = [...tabs];
      temp[0].unActive = res0.data.store.logoUrl;
      temp[0].active = res0.data.store.logoUrl;
      setTabs(temp);
      setData(res0.data.store);
    }
    if (res1.code == 0 && res1.data && res1.data != null) {
      setList(res1.data.list);
    }
    if (
      res2.code == 0 &&
      res2.data &&
      res2.data != null &&
      res2.data.items &&
      res2.data.items.length > 0
    ) {
      setAdvs(res2.data.items);
    }
    if (res3.code == 0 && res3.data && res3.data != null && res3.data.items) {
      setCategorys(res3.data.items);
    }
    if (res4.code == 0 && res4.data && res4.data != null && res4.data.items) {
      setDiscount(res4.data.items);
    }
    if (res5.code == 0 && res5.data && res5.data != null && res5.data.records) {
      setnewProduct(res5.data.records);
    }
    setTimeout(() => {
      const query = Taro.createSelectorQuery();
      query
        .select('.utp-clothingBusiness-storeInfo')
        .boundingClientRect((rec) => {
          let temp = 430 - rec.height;
          setTop(temp);
        })
        .exec();
      Taro.hideLoading();
    }, 500);
  };
  const getCoupon = async (payload) => {
    let pay = {
      activityId: payload.activityId,
      businessId: router.params.businessId,
      storeId: router.params.storeId,
      couponCode: payload.couponCode
    };
    let res = await userGetCoupon(pay);
    if (res.code === 0) {
      Taro.showToast({ title: '领取成功', icon: 'none' });
    } else {
      Taro.showToast({ title: res.msg, icon: 'none' });
    }
  };
  useEffect(() => {
    Taro.showLoading({ title: '加载中' });
    init();
  }, []);
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-clothingBusiness"
            scrollY
            onScroll={onScroll}
          >
            <View
              className="utp-clothingBusiness-topcon"
              style={{ background: `url(${data.bgStoreUrl})` }}
            >
              <Image src={mengban} className="utp-clothingBusiness-bgtop"></Image>
              <View className="utp-clothingBusiness-storeInfo">
                <Wrap type={3} top>
                  <View className="utp-clothingBusiness-storeInfo-card">
                    <Wrap type={2} flexDirection="column" top>
                      <View
                        className="utp-clothingBusiness-titles"
                        onClick={() => {
                          Router.goStoreInfo(router.params.storeId, data.latitude, data.longitude);
                        }}
                      >
                        <Text className="utp-clothingBusiness-title taro-text">{data.name}</Text>
                        <Img src={white} width={32} />
                      </View>
                      <View className="utp-clothingBusiness-storeInfo-score">
                        <Wrap type={2}>
                          {!!data.selfSupport && (
                            <View className="utp-clothingBusiness-tag">
                              <Tag>自营</Tag>
                            </View>
                          )}
                          <Score num={data.star} />

                          {data.logisticsModel &&
                            data.logisticsModel.map((i, ii) => {
                              return (
                                <View className="utp-clothingBusiness-lables" key={ii}>
                                  <View className="utp-clothingBusiness-before"></View>
                                  <Wrap>
                                    <Img
                                      src={
                                        i === '即时达' || i === '周边送'
                                          ? store_tag1
                                          : i === '扫码自取'
                                          ? store_tag3
                                          : i === '门店自提'
                                          ? store_tag4
                                          : i === '全国送'
                                          ? store_tag2
                                          : store_icons_down
                                      }
                                      width={24}
                                    />
                                    <Txt
                                      title={i === '周边送' ? '即时达' : i}
                                      color="white"
                                      size={22}
                                      height={30}
                                    />
                                  </Wrap>
                                </View>
                              );
                            })}
                        </Wrap>
                      </View>

                      <View
                        style={{
                          width: '100%',
                          position: 'relative',
                          display: data.coupons && data.coupons.length === 0 ? 'none' : 'block'
                        }}
                      >
                        <View>
                          <Wrap type={1}>
                            <View>
                              <Wrap type={2}>
                                {data.coupons &&
                                  data.coupons.map((m, mm) => {
                                    return (
                                      <View
                                        className="utp-clothingBusiness-storeInfo-Discount"
                                        key={mm}
                                      >
                                        <Wrap type={2}>
                                          <Text className="utp-clothingBusiness-storeInfo-Coupon">
                                            {m.couponName}
                                          </Text>
                                          <Text
                                            className="utp-clothingBusiness-storeInfo-reduction"
                                            onClick={() => {
                                              getCoupon(m);
                                            }}
                                          >
                                            领券
                                          </Text>
                                        </Wrap>
                                      </View>
                                    );
                                  })}
                              </Wrap>
                            </View>
                          </Wrap>
                        </View>
                      </View>
                    </Wrap>
                  </View>
                </Wrap>
              </View>
            </View>

            {active == 0 && (
              <View className="utp-clothingBusiness-home" style={{ marginTop: pxTransform(-top) }}>
                <View
                  className={
                    Fixed
                      ? 'utp-clothingBusiness-clothingTab utp-clothingBusiness-top'
                      : 'utp-clothingBusiness-clothingTab'
                  }
                  style={{
                    position: Fixed ? 'fixed' : 'static',
                    top: 0,
                    zIndex: 100
                  }}
                >
                  <Wrap type={2}>
                    {!Fixed &&
                      ['首页', '优惠', '新品'].map((item, index) => {
                        return (
                          <View
                            key={index}
                            className={
                              index === TabIndex
                                ? 'utp-clothingBusiness-clothingTab-1-active'
                                : 'utp-clothingBusiness-clothingTab-1'
                            }
                            onClick={() => {
                              setTabIndex(index);
                            }}
                          >
                            {item}
                          </View>
                        );
                      })}
                    {Fixed &&
                      ['首页', '优惠', '新品'].map((item, index) => {
                        return (
                          <View
                            key={index}
                            className={
                              index === TabIndex
                                ? 'utp-clothingBusiness-topfix-active'
                                : 'utp-clothingBusiness-topfix'
                            }
                            onClick={() => {
                              setTabIndex(index);
                            }}
                          >
                            {item}
                          </View>
                        );
                      })}
                  </Wrap>
                </View>
                {TabIndex === 0 && (
                  <View className="utp-clothingBusiness-clothingTabList">
                    <View className="utp-clothingBusiness-TabListScroll">
                      <ScrollView refresherEnabled refresherBackground="#333" scrollX>
                        <Wrap type={2} flexWrap="nowrap">
                          {list &&
                            list.map((item, index) => {
                              return (
                                <View
                                  className="utp-clothingBusiness-TabListScroll-box"
                                  key={index}
                                  onClick={() => {
                                    Taro.navigateTo({
                                      url: `/pages/store/productList/index?categoryId=${item.foreCategoryId}&storeId=${router.params.storeId}&businessId=${router.params.businessId}`
                                    });
                                  }}
                                >
                                  <ItemHomeIcon
                                    tips={item.name}
                                    src={item.pic}
                                    Width={108}
                                    Height={108}
                                    tipColor="#333"
                                    tipsSize={26}
                                  />
                                </View>
                              );
                            })}
                        </Wrap>
                      </ScrollView>
                    </View>
                    {advs && (
                      <View>
                        {advs.length > 0 &&
                          advs.map((o, oo) => {
                            return (
                              <Swiper
                                key={oo}
                                className="utp-clothingBusiness-adv"
                                style={{
                                  height: Taro.pxTransform(advs[oo].adverItems[0].height)
                                }}
                              >
                                {o.adverItems.map((y, yy) => {
                                  return (
                                    <SwiperItem key={yy} className="utp-clothingBusiness-advitem">
                                      <Image
                                        className="utp-clothingBusiness-advitem-img"
                                        src={y.image}
                                        onClick={() => {
                                          console.log(y.url);
                                        }}
                                      />
                                    </SwiperItem>
                                  );
                                })}
                              </Swiper>
                            );
                          })}
                      </View>
                    )}
                    {categorys &&
                      categorys.map((im, inn) => {
                        return (
                          <View className="utp-clothingBusiness-homeImg" key={inn}>
                            <Wrap justifyContent="center">
                              <Image
                                src={im.picUrls[0]}
                                className="utp-clothingBusiness-homeImg-item"
                              />
                            </Wrap>
                            <View style={{ height: 10 }}></View>
                            <Wrap>
                              {im.productItems.map((n, nn) => {
                                return <Goods3 key={nn} data={n} add />;
                              })}
                            </Wrap>
                          </View>
                        );
                      })}
                  </View>
                )}

                {TabIndex === 1 && (
                  <View className="utp-clothingBusiness-clothingTabList utp-clothingBusiness-clothingTabList-2">
                    <View>
                      {data.coupons && data.coupons.length > 0 ? (
                        <View className="utp-clothingBusiness-cardbox">
                          {data.coupons.map((u, uu) => {
                            return <Card data={u} key={uu} />;
                          })}
                        </View>
                      ) : null}

                      {discount &&
                        discount.map((u, uu) => {
                          return (
                            <View key={uu} className="utp-clothingBusiness-clothingTabList-2-title">
                              <Wrap justifyContent="space-between" Myheight={90}>
                                <Txt title={u.activityName} size={30} bold color="deep"></Txt>
                                <View
                                  onClick={() => {
                                    Taro.navigateTo({
                                      url: `/pages/store/productList/index?storeId=${router.params.storeId}&businessId=${router.params.businessId}&activeType=1&activityId=${u.activityId}`
                                    });
                                  }}
                                >
                                  <Txt title="查看更多" size={26}></Txt>
                                </View>
                              </Wrap>
                              <View className="utp-clothingBusiness-clothingTabList-2-goods">
                                <Wrap flexWrap="nowrap">
                                  {u.productItems.map((m, mm) => {
                                    return <Goods3 data={m} key={mm} />;
                                  })}
                                </Wrap>
                              </View>
                            </View>
                          );
                        })}
                    </View>
                  </View>
                )}
                {TabIndex === 2 && (
                  <View className="utp-clothingBusiness-clothingTabList utp-clothingBusiness-clothingTabList-3">
                    <View>
                      {newProduct ? (
                        <Goods2s data={newProduct as IGoods[]} border={0} bg="" />
                      ) : null}
                    </View>
                  </View>
                )}
              </View>
            )}
            {active === 2 && (
              <View
                className="utp-clothingBusiness-class"
                style={{
                  marginTop: Taro.pxTransform(-top),
                  position: 'relative'
                }}
              >
                {list &&
                  list.map((e, ee) => {
                    return (
                      <View className="utp-clothingBusiness-class-item" key={ee}>
                        <Wrap
                          Myheight={100}
                          justifyContent="space-between"
                          click={() => {
                            Taro.navigateTo({
                              url: `/pages/store/productList/index?categoryId=${e.foreCategoryId}&storeId=${router.params.storeId}&businessId=${router.params.businessId}`
                            });
                          }}
                        >
                          <Txt title={e.name} color="deep" size={32} />
                          <Img src={iconUp} width={46} />
                        </Wrap>
                        <Wrap justifyContent="space-between">
                          {e.children.map((i) => {
                            return (
                              <View
                                className="utp-clothingBusiness-class-lable"
                                key={i}
                                onClick={() => {
                                  Taro.navigateTo({
                                    url: `/pages/store/productList/index?categoryId=${i.foreCategoryId}&storeId=${router.params.storeId}&businessId=${router.params.businessId}`
                                  });
                                }}
                              >
                                <Txt
                                  title={i.name}
                                  size={26}
                                  color="deep"
                                  justifyContent="flex-start"
                                  height={70}
                                  dian
                                />
                              </View>
                            );
                          })}
                        </Wrap>
                      </View>
                    );
                  })}
              </View>
            )}
            {active === 1 && (
              <View style={{ marginTop: Taro.pxTransform(-top) }}>
                {goods ? <Goods2s data={goods as IGoods[]} bg="" border={0} /> : null}
                {loading && goods.length === 0 && active !== 1 && <LoadingMore />}
                {!loading && !hasMore && active === 1 && <LoadingNoneMore />}
              </View>
            )}
          </ScrollView>
          <View className="utp-clothingBusiness-ft">
            <Wrap>
              {tabs &&
                tabs.map((p, pp) => {
                  return (
                    <View
                      key={pp}
                      style={{ width: '25%' }}
                      onClick={() => {
                        setActive(pp);
                        if (pp === 3) {
                          setCoupon(false);
                          Taro.switchTab({
                            url: '/pages/cart/index'
                          });
                        }
                      }}
                    >
                      <ItemMineIcon
                        tips={p.title}
                        tipsSize={22}
                        src={active === pp ? p.active : p.unActive}
                        tipsColor={active !== pp ? '#333' : '#FF2F7B'}
                        srcWidth={60}
                      />
                    </View>
                  );
                })}
            </Wrap>
          </View>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {
  navigationBarTitleText: '店铺',
  disableScroll: true
  //navigationStyle: 'custom'
};
Index.config = {
  navigationBarTitleText: '店铺',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};
export default Index;
