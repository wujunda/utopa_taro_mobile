import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

import './index.scss';
import Wrap from '../Wrap';
import BashIt from '../BashIt';
import Txt from '../Txt';
import Cnt from '../Cnt';
import { images } from '../../images';

interface IProps {
  isXuni?: boolean;
  storeId: number;
  businessId: number;
  type?: 'none';
  addCart?: () => void;
  buyGoods?: () => void;
}
const Index: Taro.FC<IProps> = ({
  isXuni = false,
  type,
  addCart,
  buyGoods,
  storeId,
  businessId
}) => {
  return (
    <View className="utp-addCart">
      <Wrap type={1} justifyContent="space-between">
        <Wrap>
          {false && (
            <View className="utp-addCart-icos">
              <BashIt
                top={0}
                bottom={0}
                src={images.help}
                srcWidth={60}
                tips="客服"
                tipsHeight={20}
                tipsColor="#333333"
                tipsSize={18}
              ></BashIt>
            </View>
          )}

          <View
            className="utp-addCart-icos"
            onClick={() => {
              Taro.navigateTo({
                url: `/pages/store/shoppingStore/index?storeId=${storeId}&businessId=${businessId}`
              });
            }}
          >
            <BashIt
              top={0}
              bottom={0}
              src={images.home}
              srcWidth={60}
              tips="店铺"
              tipsHeight={20}
              tipsColor="#333333"
              tipsSize={18}
            ></BashIt>
          </View>

          <View
            className="utp-addCart-icos"
            onClick={() => {
              Taro.switchTab({
                url: '/pages/cart/index'
              });
            }}
          >
            <BashIt
              top={0}
              bottom={0}
              src={images.cart}
              srcWidth={60}
              tips="购物车"
              tipsHeight={20}
              tipsColor="#333333"
              tipsSize={18}
              num={0}
              numType={1}
            ></BashIt>
          </View>
        </Wrap>
        <View className="utp-addCart-btns">
          {!isXuni && type !== 'none' && (
            <Wrap>
              <View className="utp-addCart-btn utp-addCart-btn-1" onClick={addCart}>
                <Cnt>
                  <Txt title="加入购物车" size={28} color="white" />
                </Cnt>
              </View>
              <View className="utp-addCart-btn" onClick={buyGoods}>
                <Cnt>
                  <Txt title="立即购买" size={28} color="white" />
                </Cnt>
              </View>
            </Wrap>
          )}
          {isXuni && type !== 'none' && (
            <Wrap>
              <View
                className="utp-addCart-btn utp-addCart-btn-2 utp-addCart-btn-2-1"
                onClick={buyGoods}
              >
                <Cnt>
                  <Txt title="立即购买" size={28} color="white" />
                </Cnt>
              </View>
            </Wrap>
          )}
          {type === 'none' && (
            <Wrap>
              <View className="utp-addCart-btn utp-addCart-btn-2">
                <Cnt>
                  <Txt title="已售罄" size={28} color="white" />
                </Cnt>
              </View>
            </Wrap>
          )}
        </View>
      </Wrap>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
