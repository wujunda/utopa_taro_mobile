import { View } from '@tarojs/components';
import Taro, { useReachBottom, useRouter } from '@tarojs/taro';

import Goods1 from '../../../components/Goods1';
import Txt from '../../../components/Txt';
import Card from '../../../components/Card';
import Wrap from '../../../components/Wrap';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import Safe from '../../../components/safe';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import useSession from '../../../hooks/useSession';
import useLogin from '../../../hooks/useLogin';
import useResetCount from '../../../hooks/useResetCount';
import { IHistory, getCanUse } from '../../../services/canUse';
import { ProductBaiscData } from '../../../interface/product';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const activeType = useRouter().params.activeType;
  const activityId = useRouter().params.activityId;
  // console.log('注册注册');
  const [haveSession] = useSession();
  const [isLogin, updateLogin] = useLogin();
  if (haveSession && !isLogin) {
    if (updateLogin) {
      updateLogin();
    }
  }
  // 购物车数据
  // @ts-ignore
  const [state, hasMore, update, getMoreData, loading] = useRequestWIthMore<IHistory>(
    {
      activityId,
      activeType
    },
    getCanUse
  );
  const [can, update1] = useResetCount();
  if (
    haveSession &&
    isLogin &&
    (state === null || (state != null && state.length == 0)) &&
    update &&
    !loading
  ) {
    if (can) {
      update({});
      update1();
    }
  }
  var goods: ProductBaiscData[] = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.data.page.records.map((item) => {
        goods.push(item);
      });
    });
  }
  useReachBottom(() => {});

  return (
    <Safe>
      <View className="utp-html utp-canUse">
        <View className="utp-body">
          <View className="utp-100"></View>
          {state &&
            state.length > 0 &&
            activeType === '2' &&
            state.map((item, index) => {
              if (index !== 0) {
                return;
              }
              return (
                <View className="utp-canUse-tips">
                  <Card source="details" canUse={true} data={item.data} />
                </View>
              );
            })}

          {state &&
            state.length > 0 &&
            activeType === '1' &&
            state.map((item, index) => {
              if (index !== 0) {
                return;
              }
              return (
                <View className="utp-canUse-tips">
                  <Wrap>
                    <Txt height={48} title={item.data.activityName} size={30} color="deep" />
                  </Wrap>
                  <Wrap>
                    <Txt
                      height={40}
                      title={`${
                        (item.data.useTicket === 1 ? '可叠加优惠券 ｜ ' : '') +
                        item.data.activeTimeDesc
                      }`}
                      size={24}
                      color="low"
                    />
                  </Wrap>
                  <Wrap>
                    <Txt height={40} title={item.data.activityDesc} size={24} color="low" />
                  </Wrap>
                </View>
              );
            })}
          <View style={{ flex: 1 }}>
            {loading && goods.length === 0 && <Loading />}
            {!loading && goods.length === 0 && <LoadingNone />}
            {goods.map((item) => {
              return (
                <View key={item.id}>
                  <Goods1 item={item} isAdd={false} />
                </View>
              );
            })}
            {loading && goods.length !== 0 && <LoadingMore />}
            {!loading && !hasMore && goods.length > 0 && <LoadingNoneMore />}
          </View>
        </View>
      </View>
    </Safe>
  );
};

Index.config = {
  navigationBarTitleText: '可用商品',
  disableScroll: false
  //navigationStyle: 'custom',
  //usingComponents: {
  //Bar: '../../../components/bar' // 书写第三方组件的相对路径
  //}
};
Index.defaultProps = {};
export default Index;
