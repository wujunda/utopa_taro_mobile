// import { ComponentClass } from "react";
import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import './index.scss';

import goods from '../../../assets/goods.png';
import Yuan from '../../Yuan';
import success from '../../../assets/assemble/icon_success@2x.png';
import fail from '../../../assets/assemble/icon_fail@2x.png';

// #region 书写注意
//
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

/**
 * 指定config的类型声明为: Taro.Config
 *
 * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
 * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
 * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
 */
// 单个拼团商品 用于拼团列表  pages/assemble/list/index
interface DataIprops {
  goodsLogo: string;
  goodsName: string;
  activityId: number;
  activityCode: string;
  activityCreateTime: number;
  activityEndTime: number;
  goodsGroupPrice: number;
  goodsDiscountPrice: number;
  groupStatus: number;
  activityUserCount: number;
}
interface IProps {
  data: DataIprops;
  status?: 0 | 1;
  goStatus?: () => void;
  goOrder?: () => void;
}
const Index: Taro.FC<IProps> = ({ data, status = 0, goStatus, goOrder }) => {
  return (
    <View className="utp-div utp-product ">
      <Image src={status === 0 ? fail : success} className="utp-product-status" />
      <View className="utp-product-item">
        <View className="utp-product-i utp-cnt">
          <Image className="utp-product-img" src={data.goodsLogo} />
        </View>
        <View className="utp-product-titles">
          <Text className="utp-product-title taro-text">{data.goodsName}</Text>
          <Text className="utp-product-people">{data.activityUserCount}人拼</Text>
        </View>
        <View className="utp-flex1 utp-product-price">
          <Yuan price={(data && data.goodsGroupPrice) || 0} color="#FF2F7B"></Yuan>
        </View>
      </View>
      <View className="utp-flex1 utp-product-btn">
        <Text className="utp-flex1 utp-product-btnitem utp-solid" onClick={goStatus}>
          拼团详情
        </Text>
        <Text className="utp-flex1 utp-product-btnitem utp-solid" onClick={goOrder}>
          订单详情
        </Text>
      </View>
    </View>
  );
};

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion
Index.defaultProps = {};

export default Index;
