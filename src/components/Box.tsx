import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

interface Props {
  children: any;
  type?: number;
  color?: string;
  noBottom?: boolean;
}
const Index: Taro.FC<Props> = ({ noBottom = false, children, type, color = '#fff' }) => {
  let styles: any = {
    backgroundColor: color,
    position: 'relative',
    borderRadius: Taro.pxTransform(8),
    marginBottom: noBottom ? 0 : Taro.pxTransform(20),
    paddingBottom: Taro.pxTransform(1),
    marginLeft: Taro.pxTransform(20),
    marginRight: Taro.pxTransform(20)
  };
  if (type == 1) {
    styles = {
      backgroundColor: color,
      position: 'relative',
      marginBottom: noBottom ? 0 : Taro.pxTransform(20),
      paddingBottom: Taro.pxTransform(1),
      paddingLeft: Taro.pxTransform(20),
      paddingRight: Taro.pxTransform(20)
    };
  }

  return (
    <View style={styles} className="utp-div">
      {children}
    </View>
  );
};
Index.defaultProps = {};

export default Index;
