/* eslint-disable react-hooks/exhaustive-deps */
import Taro from '@tarojs/taro';
import { View, Image } from '@tarojs/components';
import close from './../../assets/close6.png';
import './index.scss';
// #region 书写注意
// #endregion

// 底部sku
interface IProps {
  title?: string;
  show?: boolean;
  onClose?: () => void;
  closeOverlay?: () => void;
  height?: number;
}
const Index: Taro.FC<IProps> = ({ title, show, onClose, height = 1000 }) => {
  return (
    <View className={show ? 'utp-model active' : 'utp-model'}>
      <View className="utp-model__overlay" onClick={onClose}></View>
      <View className="utp-model__container layout">
        <View className="layout-header">
          {title}
          <Image src={close} className="close-img" onClick={onClose} />
        </View>
        <View className="layout-body" style={{ height: Taro.pxTransform(height) }}>
          {this.props.children}
        </View>
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
