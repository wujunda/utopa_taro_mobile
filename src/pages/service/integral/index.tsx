import Taro, { useState, useEffect } from '@tarojs/taro';
import { View, ScrollView, Text } from '@tarojs/components';
import { connect } from '@tarojs/redux';

import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import ItemHomeIcon from '../../../components/ItemHomeIcon';
import Bar from '../../../components/bar';
import icon_duihuandingdan from '../../../assets/integral/icon_duihuandingdan.png';
// import icon_jifentingche from '../../../assets/integral/icon_jifentingche.png';
import icon_jifenmingxi from '../../../assets/integral/icon_jifenmingxi.png';
import icon_jifenshangcheng from '../../../assets/integral/icon_jifenshangcheng.png';
import { integralList } from '../../../services/integral'
import { userIntegralMsg } from '../../../services/canbonce'
import IntegralCard from '../../../components/integral/card';
// import useRequest from '../../../hooks/useRequest';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore'
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';

import useSession from '../../../hooks/useSession';
import useLogin from '../../../hooks/useLogin';
import './index.scss';

interface Props {
  statusHeight: number;
}

const Index: Taro.FC<IProps> = (props) => {
  const [haveSession] = useSession();
  const [isLogin, updateLogin, lodingLoading] = useLogin();
  const [integralMsg, setIntegralMsg] = useState({
    freeBalance: 0,
    frozenBalance: 0
  })
  if (haveSession && !isLogin && !lodingLoading) {
    if (updateLogin) {
      updateLogin();
    }
  }
  useEffect(() => {
    userIntegralMsg().then(res => {
      if (res.code == 0) {
        setIntegralMsg(res.result)
      } else {
        Taro.showToast({
          title: res.message,
          icon: 'none'
        })
      }
    })
  }, [])
  
  const [states, hasMore, run, getMoreData, loadings] = useRequestWIthMore<IStore>(
    { type:0 },
    integralList
  );

  var goodsData = [];
  if (states && states.length > 0) {
    states.map((item) => {
      item.data.items.map((items) => {
        goodsData.push(items);
      });
    });
  }
  const [integralInfo] = useState([
    {
      title: '积分明细',
      src: icon_jifenmingxi,
      url: '/pages/integral/details/index'
    },
    // {
    //   title: '积分停车',
    //   src: icon_jifentingche,
    //   url: ''
    // },
    {
      title: '兑换订单',
      src: icon_duihuandingdan,
      url: ''
    },
    {
      title: '积分商城',
      src: icon_jifenshangcheng,
      url: '/pages/integral/list/index'
    }
  ]);

  return (
    <ScrollView>
      <Safe>
        <View
          refresherEnabled
          refresherBackground="#333"
          className="utp-bd utp-bd-page utp-integral"
          scrollY
        >
          {loadings && goodsData.length === 0 && <Loading />}
          {!loadings && goodsData.length === 0 && <LoadingNone />}
          <View className="utp-integral-part1">
            <View className="utp-integral-hd" style={{ paddingTop: props.statusHeight + 'px' }}>
              <Bar white goBack title="我的积分" bg className="utp-integral-bd-bar" />
            </View>
            <View className="utp-integral-part1-myIntegral">
              <View className="utp-integral-part1-myIntegral-now">
                <View className="utp-integral-part1-myIntegral-now-text">可用</View>
                <Text className="utp-integral-part1-myIntegral-now-num">
                  {integralMsg.freeBalance}
                </Text>
              </View>
              <View className="utp-integral-part1-myIntegral-now">
                <View className="utp-integral-part1-myIntegral-now-text">待生效</View>
                <Text className="utp-integral-part1-myIntegral-now-num">
                 {integralMsg.frozenBalance}
                </Text>
              </View>
            </View>
            <View
              className="utp-integral-part1-rule"
            >
              <View className="utp-integral-part1-rule-bg" onClick={() => {
                Taro.navigateTo({
                  url: '/pages/integral/rule/index'
                })
              }}>积分规则说明</View>
            </View>
          </View>

          <View className="utp-integral-part2">
            <Wrap type={3}>
              {integralInfo.map((item, index) => {
                return (
                  <View
                    className="utp-ico1-box"
                    key={index}
                    onClick={() => {
                      Taro.navigateTo({
                        url: item.url
                      });
                    }}
                  >
                    <ItemHomeIcon
                      tipColor="rgba(255,239,230,1)"
                      tips={item.title}
                      src={item.src}
                      tipsSize={24}
                    />
                  </View>
                );
              })}
            </Wrap>
          </View>
          <Text className="utp-integral-part3-title">商城精选</Text>

          <View className="utp-integral-part3">
            {goodsData && goodsData.length > 0 && goodsData.map((item, index) => {
              return (
                <IntegralCard data={item} key={index}/>
              )
            })}
          </View>
          {loadings && goodsData.length !== 0 && <LoadingMore />}
          {!loadings && !hasMore && <LoadingNoneMore/>}
        </View>
      </Safe>
    </ScrollView>
  );
};

Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '我的积分',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
