import Taro from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
import { connect } from '@tarojs/redux';
// import useRequest from '../../../hooks/useRequest';
// import { IUse, getItem } from '../../../services/user';
import useEndTime from '../../../hooks/useEndTime';

function Index(props) {
  const [time] = useEndTime(60);
  if (props && props.callBack) {
    props.callBack(time);
  }

  return <View></View>;
}

// @ts-ignore
export default connect((state) => state)(Index);
