import request from '../utils/request';
import { bossRequest, BossApiResponseData } from '../utils/request.boss';
import Taro from '@tarojs/taro';
// eslint-disable-next-line import/first
import { Glo } from '@/utils/utils';

export interface ProductType {
  name: string;
  billId: number | null;
  products: any[];
  billType: null;
  cancleReason: null;
  closeReasonType: null;
  createTime: number;
  lo;
  goodsNum: number;
  orderId: number;
  orderNo: string;
  orderTotalMoney: number;
  overTime: number;
  payOrderNo: string;
  saleType: number;
  serviceStatus: number;
  note: string;
  certificatePicsArray: any[];
  expressNo: number;
  payWay: number;
  afterSaleNo: string;
  reason: string;
}
export interface AfterProps {
  closeReason: string;
  statusDetail: string;
  recalled: string;
  showRecallButton: number;
  orderNo: string;
  createTime: number;
  goodsTotalNum: number;
  skuId: number;
  orderId: number;
  goodsNum: number;
  products: AfterPro[];
  afterSaleNo: number;
}
interface AfterPro {
  productName: string;
  norm1Value: string;
  skuInfo: string;
  name: string;
  price: number;
  picUrl: string;
  num: number;
  picIds: string;
  goodsTotalNum: number;
  status: number;
}
export interface Iprops {
  data: {
    records: any[];
  };
}
export interface DataType {
  name: string;
  orderDetailId: number;
  orderId: number;
  order_no: string;
  payTime: number;
  picUrl: string;
  skuId: number;
  score?: number;
  comment?: string;
  productUrl?: string;
  commentId?: number;
  commentResponses?: any[];
  commentUrls?: any[];
}
interface newProps {
  records: any[];
}
// 申请售后列表
export const myAfterSaleProductOrders = (payload, pst: any) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.productorder.app.myAfterSaleProductOrders',
    ver: '1.0',
    params: JSON.stringify({
      accessToken: Taro.getStorageSync('login'),
      pageNo: pst.page,
      pageSize: pst.per_page
    })
  });
};

// 售后记录列表
export const myAfterSaleAndSalingPage = (params, pst: any) => {
  return request.post<newProps>(
    '/service/app/user/refundOrder/getAppRefundOrderList',
    { ...params, pageNo: pst.page, pageSize: pst.per_page },
    request.API_TYPE.BOSS_ESHOP
  );
};
// 售后提交
export const commitAfterSale = (params) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.aftersale.commitAfterSale',
    ver: '1.0',
    params: JSON.stringify({
      uid: Taro.getStorageSync('uid'),
      accessToken: Taro.getStorageSync('login'),
      ...params
    })
  });
};
// 撤回售后申请
export const recallAfterSaleOrdere = (params) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.aftersale.recallAfterSaleOrdere',
    ver: '1.0',
    params: JSON.stringify({
      accessToken: Taro.getStorageSync('login'),
      ...params
    })
  });
};
// 售后寄回接口
export const rebackProduct = (params) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.aftersale.rebackProduct',
    ver: '1.0',
    params: JSON.stringify({
      accessToken: Taro.getStorageSync('login'),
      ...params
    })
  });
};
//售后申请进度
export const afterSaleDetail = (params) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.aftersale.afterSaleDetail',
    ver: '1.0',
    params: JSON.stringify({
      accessToken: Taro.getStorageSync('login'),
      ...params
    })
  });
};
//退货原因获取
export const returnsReasons = (params) => {
  return request.post<any>('/app.do', {
    method: 'eshop.returnsreason.returnsReasons',
    ver: '1.0',
    params: JSON.stringify({
      accessToken: Taro.getStorageSync('login')
    })
  });
};
//待评价列表
export const myUnCommentList = (params) => {
  return request.post<any>('/app.do', {
    method: 'eshop.productComment.app.myUnCommentList',
    ver: '1.0',
    params: JSON.stringify({
      uid: Taro.getStorageSync('uid'),
      accessToken: Taro.getStorageSync('login'),
      pageNo: 1,
      pageSize: 20,
      clientType: 1,
      sysId: 1
    })
  });
};
export const myCommentList = (params) => {
  return request.post<any>('/app.do', {
    method: 'eshop.productComment.app.myCommentList',
    ver: '1.0',
    params: JSON.stringify({
      uid: Taro.getStorageSync('uid'),
      accessToken: Taro.getStorageSync('login'),
      pageNo: 1,
      pageSize: 20,
      clientType: 1,
      sysId: 1
    })
  });
};
// 提交评价
export const addComment = (params) => {
  return request.post<any>('/app.do', {
    method: 'eshop.productComment.app.addComment',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      clientType: 1,
      sysId: 1
    })
  });
};

// path: string,
// data: object = {},
// method: 'GET' | 'POST' | 'PUT' | 'DELETE' = 'GET',
// //协商历史

// 获取 退款协商历史记录
export const getHistoryList = (data) => {
  return request.get(
    '/service/app/user/refundOrderConsult/getConsultHistory',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};
// //退款详情
export const getRefundDetailInfo = (data) => {
  return request.post(
    '/service/app/user/refundOrderDetail/getAppRefundOrderDetail',
    // { refundOrderNo: '3022020010600001580808670' },
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};

// 获取退款 可退邮费
export const getPostFeeBalance = (data: any) => {
  return request.get(
    '/service/app/user/refundOrder/getPostFeebalance',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};

// 申请退款，仅退款
export const applyRefundMoney = (data: any) => {
  return request.post(
    '/service/app/user/refundOrder/applyRefundMoney',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};

// 申请退款，退货退款
export const applyRefundMonAndPro = (data: any) => {
  return request.post(
    '/service/app/user/refundOrder/applyRefundMonAndPro',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};

// 获取退款协商历史列表 /service/app/user/refundOrderConsult/getConsultHistory
export const getConsultHistory = (data: any) => {
  return request.get(
    '/service/app/user/refundOrderConsult/getConsultHistory',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};

// 售后申请撤销
export const quitRefundOrder = (data: any) => {
  return request.get(
    '/service/app/user/refundOrder/quitRefundOrder',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};

// 修改退款
export const updateRefundApply = (data: any) => {
  return request.post(
    '/service/app/user/refundOrder/updateRefundApply',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};

// 订单详情信息。 /service/app/user/productOrderDetail/getByOrderDetailId
export const getByOrderDetailId = (data: any) => {
  return request.get(
    '/service/app/user/productOrderDetail/getByOrderDetailId',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};
// 退款信息 商品可退金额

export const getProductDetailBalance = (data: any) => {
  return request.get(
    '/service/app/user/refundOrder/getProductDetailBalance',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};
