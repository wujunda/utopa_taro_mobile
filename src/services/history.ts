import request from '../utils/request';
import { ProductBaiscData } from '../interface/product';

export interface IHistory {
  code: number;
  data: {
    current: number;
    pages: number;
    size: number;
    total: number;
    records: ProductBaiscData[];
  };
}
export const getHistory = (params: any, pst: any) => {
  // console.log(params);
  console.warn('查询参数');
  console.warn(pst);
  return request.post<any>('/app.do', {
    method: 'eshop.product.getProductHistory',
    ver: '2.0',
    params: JSON.stringify({
      pageSize: pst.per_page,
      pageNo: pst.page
    })
  });
};
