import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import './index.scss';
import check from '../../assets/cart/check.png';
import uncheck from '../../assets/submit/uncheck.png';
import Img from '../Img';
import Txt from '../Txt';
import Bton from '../Bton';
import Wrap from '../Wrap';
import Cnt from '../Cnt';
import Yuan from '../Yuan';

interface IProps {
  count?: number;
  totalPrice?: number;
  ischeck?: boolean;
  selectAll?: () => void;
  onSubmit: () => void;
}
const Index: Taro.FC<IProps> = ({ onSubmit, ischeck = false, selectAll, count, totalPrice }) => {
  // console.log(count, '--count');
  const handleClick = (e) => {
    e.stopPropagation();
    if (selectAll) {
      selectAll();
    }
  };
  const handleSubmit = (e) => {
    e.stopPropagation();
    onSubmit();
  };
  const handleStop = (e) => {
    e.stopPropagation();
  };

  return (
    <View className="utp-cart-sub" onClick={handleStop}>
      <View onClick={handleClick}>
        <Wrap type={2}>
          <View className="utp-cart-sub-icos">
            <Cnt>
              <Img src={ischeck ? check : uncheck} width={36} />
            </Cnt>
          </View>
          <Txt title="全选" />
        </Wrap>
      </View>
      <Wrap>
        <Wrap type={2}>
          <Txt title="合计：" size={26} color="deep" />
          <View className="utp-cart-sub-price">
            <Yuan price={totalPrice} color="FF2F7B" size={28}></Yuan>
          </View>
        </Wrap>
        <View className="utp-cart-sub-box" onClick={handleSubmit}>
          {!count && (
            <Bton type={7} size="mini">
              去结算
            </Bton>
          )}
          {!!count && count > 0 && (
            <Bton type={7} size="mini">
              去结算({count})
            </Bton>
          )}
        </View>
      </Wrap>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
