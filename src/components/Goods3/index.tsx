import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import './index.scss';
import add1 from '../../assets/cart/add1.png';
import Tag from '../Tag';
import Wrap from '../Wrap';
import Yuan from '../Yuan';
// import { images } from '../../images';
import { ProductBaiscdata } from '../../interface';

type PageStateProps = {
  cart: any;
  data: ProductBaiscdata;
};
type PageDispatchProps = {
  addToCart: (goodsInfo) => void;
};
interface dataProps {
  id: number;
  picUrl: string;
  price: number;
  priceOrigin: number;
  name: string;
  skuId: number;
  storeId: number;
  businessId: number;
  productId: number;
  productName: string;
  activeLabel: string;
  productImage: string;
  originPrice: number;
  salePrice: number;
}
interface Props {
  data: dataProps;
  add?: boolean;
}
type IProps = PageStateProps & PageDispatchProps & Props;
const Index: Taro.FC<IProps> = (props) => {
  const { data, add=true } = props;
  // console.log(add, 'add');
  return (
    <View
      className="utp-goods3s"
      onClick={() => {
        Taro.navigateTo({
          url: `/pages/details/index?id=${data.id}&storeId=${data.storeId}&skuId=${data.skuId}&&businessId=${data.businessId}`
        });
      }}
    >
      <View className="utp-goods3 utp-div">
        <View className="utp-goods3-i utp-cnt">
          <Image className="utp-goods3-img" src={data && (data.picUrl || data.productImage)} />
        </View>
        <View className="utp-goods3-data">
          <View className="utp-goods3-titles">
            {!!data && !!data.selfSupport && (
              <View className="utp-goods3-titles-tag">
                <Tag>自营</Tag>
              </View>
            )}
            <Text
              className={
                data && data.selfSupport
                  ? 'utp-goods3-title taro-text utp-goods3-indent'
                  : 'utp-goods3-title taro-text'
              }
            >
              {data && (data.name || data.productName)}
            </Text>
          </View>
          <Wrap>
            <View style={{ display: data && data.label ? 'block' : 'none' }}>
              <Tag type={1}>{data && (data.label || data.activeLabel)}</Tag>
            </View>
          </Wrap>
          <View className="utp-goods3-ft">
            <Wrap type={1} justifyContent="space-between">
              <Wrap type={1}>
                <View className="utp-goods3-moneys">
                  <Yuan price={data && (data.price || data.salePrice)} color="#FF2F7B" size={20}></Yuan>
                </View>
              </Wrap>
              <View
                className={
                  add ? 'utp-goods3-add utp-cnt' : 'utp-goods3-add utp-cnt utp-goods3-hidden'
                }
                onClick={(e) => {
                  e.stopPropagation();
                  props.addToCart({
                    productId: data.productId,
                    number: 1,
                    skuId: data.skuId,
                    storeId: data.storeId,
                    businessId: data.businessId
                  });
                }}
              >
                <Image className="utp-goods3-add-ico" src={add1} />
              </View>
            </Wrap>
          </View>
          <View
            className="utp-goods3-ft-1"
            style={{
              visibility:
                data &&
                data.priceOrigin !== null &&
                (data.priceOrigin > data.price || data.originPrice > data.salePrice)
                  ? 'visible'
                  : 'hidden'
            }}
          >
            <Wrap type={1}>
              <View className="utp-goods3-ft-money">
                <Yuan price={data && (data.priceOrigin || data.originPrice)} color="#999" size={22}></Yuan>
              </View>
            </Wrap>
          </View>
        </View>
      </View>
    </View>
  );
};
Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    cart: state.cart
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    addToCart: (payload) => {
      dispatch({
        type: 'cart/addCart',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
