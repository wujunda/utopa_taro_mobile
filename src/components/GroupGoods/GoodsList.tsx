/*
 * @Description: 平铺类型商品组件
 * @Version: 1.0
 * @Autor: gaotongjian
 * @Date: 2020-04-24 21:00:17
 * @LastEditors: gaotongjian
 * @LastEditTime: 2020-05-06 17:25:59
 */

import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import Wrap from '../Wrap';
import Goods1 from '../Goods1';
import Goods2 from '../Goods2';
import Goods3 from '../Goods3';
import './GoodsList.scss';

// eslint-disable-next-line no-unused-vars
import { ProductBaiscData, ProductStoreInfo } from '../../interface';

type dataSource = ProductBaiscData & ProductStoreInfo;

type GoodsTypes = '1x1' | '1x2' | '1x3';

interface IProps {
  // 数据
  dataSource: dataSource[];

  // 展示行数，0 代表不限制
  showRowCount?: number | false;

  // 商品组件类型
  type?: GoodsTypes;
}

const rowCount: Record<GoodsTypes, number> = {
  '1x1': 1,
  '1x2': 2,
  '1x3': 3
};

const GoodsList: Taro.FC<IProps> = ({ dataSource, showRowCount, type = '1x1' }) => {
  let showCount: number | undefined;
  if (showRowCount) {
    showCount = rowCount[type] * showRowCount;
  }

  // 超过长度进行截取
  const list = Array.isArray(dataSource)
    ? showCount
      ? dataSource.slice(0, showCount)
      : dataSource
    : [];

  // console.log({ list });

  return (
    <Wrap flexWrap="wrap">
      {list.map((goodsData) => {
        if (type === '1x1') {
          return (
            <View className="utp-goods-list1">
              <Goods1 key={goodsData.id} {...goodsData} />
            </View>
          );
        }

        if (type === '1x2') {
          return (
            <View className="flex flex-center utp-goods-list2">
              <Goods2 key={goodsData.id} {...goodsData} />
            </View>
          );
        }

        if (type === '1x3') {
          return (
            <View className="flex flex-center utp-goods-list3">
              <Goods3 key={goodsData.id} {...goodsData} />
            </View>
          );
        }

        return null;
      })}
    </Wrap>
  );
};

GoodsList.defaultProps = {
  showRowCount: false,
  type: '1x1',
  dataSource: []
};

export default GoodsList;
