/*
 * @Description: 平铺类型商品组件
 * @Version: 1.0
 * @Autor: gaotongjian
 * @Date: 2020-04-24 21:00:17
 * @LastEditors: gaotongjian
 * @LastEditTime: 2020-05-06 17:25:59
 */

import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import './GoodsList.scss';

// eslint-disable-next-line no-unused-vars
import { ProductBaiscData, ProductStoreInfo } from '../../interface';

type dataSource = ProductBaiscData & ProductStoreInfo;

type GoodsTypes = '1x1' | '1x2' | '1x3';

interface IProps {
  // 数据
  dataSource: dataSource[];

  // 展示行数，0 代表不限制
  showRowCount?: number | false;

  // 商品组件类型
  type?: GoodsTypes;
}

const GoodsList: Taro.FC<IProps> = ({}) => {
  return <View></View>;
};

GoodsList.defaultProps = {
  showRowCount: false,
  type: '1x1',
  dataSource: []
};

export default GoodsList;
