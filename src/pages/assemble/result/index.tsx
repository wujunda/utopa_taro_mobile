import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
// import Bar from '../../../components/bar';
import Txt from '../../../components/Txt';
import Safe from '../../../components/safe';
import Bton from '../../../components/Bton';
import Cnt from '../../../components/Cnt';
import Img from '../../../components/Img';
import Assemble from '../../../components/AssembleGood/Result';
import SetTimerOut from '../../../components/Setimeout';
import Goods2s from '../../../components/Goods2s';
import useSession from '../../../hooks/useSession';
import useRequest from '../../../hooks/useRequest';
import success from './../../../assets/assemble/success.png';
import { IGoods } from '../../../interface/home';
import fail from './../../../assets/assemble/fail.png';
import { Iprops, findGroupDetailById, findRecommendedGoods } from '../../../services/assemble';
import './index.scss';

// 拼团详情状态页面
interface IProps {}
const Index: Taro.FC<IProps> = () => {
  const [haveSession] = useSession();
  const [state, run, loading] = useRequest<Iprops>(2729, findGroupDetailById);
  const [state1, run1, loading1] = useRequest<Iprops>(1360, findRecommendedGoods);
  if (haveSession && state === null && run && !loading) {
    run();
  }
  // setStatus(state && state.data.activityStatus)
  if (haveSession && state1 === null && run1 && !loading1) {
    run1();
  }
  let a = 88888;
  if (state) {
    a = (state.data.groupEndTime - state.data.groupStartTime) / 1000;
  }
  return (
    <Safe>
      <View className="utp-pint">
        {/* <Bar title="拼团详情" /> */}
        <View className="utp-pint-box">
          {/* 拼团单个商品组件 loading状态*/}
          <View style={{ display: state && state.data.activityStatus === 2 ? 'block' : 'none' }}>
            <Assemble data={state && state.data} />
            <View className="utp-pint-tip">
              <Txt title=" 还差" color="deep" size={32} />
              <Txt
                title={state && state.data.restParticipantCount.toString() || ''}
                color="red"
                size={32}
              />
              <Txt title="拼团成功" color="deep" size={32} />
            </View>
            {/* 倒计时组件 */}
            <View className="utp-pint-timer">
              <SetTimerOut seconds={a} background="#FFF5F9" type={2} />
            </View>
          </View>

          {/* 拼团未开始 */}
          <View
            style={{ display: state && state.data.activityStatus === 1 ? 'block' : 'none' }}
            className="utp-pint-show"
          >
            <Cnt>
              <Img src={fail} width={128} />
            </Cnt>
            <View className="utp-pint-info">
              <Txt title="该团还没有开始" size={32} />
            </View>
          </View>
          {/* 失败 */}
          <View
            style={{
              display:
                (state && state.data.groupStatus == 3 && state.data.activityStatus == 2) ||
                (state && state.data.groupStatus == 3 && state.data.activityStatus == 3)
                  ? 'block'
                  : 'none'
            }}
            className="utp-pint-show"
          >
            <Cnt>
              <Img src={fail} width={128} />
            </Cnt>
            <View className="utp-pint-info">
              <Txt title="该团未能成团，拼团失败" size={32} />
            </View>
          </View>
          {/* 成功 */}
          <View
            style={{
              display:
                state && !(state.data.currentUserMemberType === 3) && state.data.groupStatus === 2
                  ? 'block'
                  : 'none'
            }}
            className="utp-pint-show"
          >
            <Cnt>
              <Img src={success} width={128} />
            </Cnt>
            <View className="utp-pint-info">
              <Txt title="恭喜你拼团成功" size={32} />
            </View>
          </View>

          {/* 按钮 */}
          <Bton type={7} onClick={()=>{
            Taro.switchTab({url:'/pages/home/index'})
          }}>
              继续购物
          </Bton>
        </View>

        <View className="utp-pint-more">
          <Txt title="- 更多优惠推送，一省到底 -" size={32} color="low" />
        </View>
        <View
          style={{
            backgroundColor: '#F5F5F5'
          }}
        >
          {state1 && state1.data.records ? (
            <Goods2s data={state1.data.records as IGoods[]} />
          ) : null}
        </View>
      </View>
    </Safe>
  );
};
Index.defaultProps = {};
export default Index;
