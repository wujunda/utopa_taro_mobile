import request from '../utils/request';
import Taro from '@tarojs/taro';
export interface IDetails {
  code: number;
  data: {
    id: number;
    isFavorite: number;
    picUrls: string[];
    name: string;
    items: any[];
    storeName: string;
    businessLogoUrl: string;
    returnPolicyValue: string;
    star: number;
    hotProducts: any[];
    recommendProducts: any[];
    freightTemplateName: string;
    productCommentList: any;
    selfSupport: number;
    originalPrice: number;
    price: number;
    discountCardDetails: any[];
    coupons: any[];
    storeId: number;
    productCommentLis;
    businessId: number;
    productSkus: any[];
    skus: any[];
    params: string;
    skuImgDefault: string;
    wantPersonNum?: number;
    goodsDescImgUrls: any[];
  };
}

export const getItem = (params: any = {}) => {
  return request.get<any>('/item', params);
};
export const getQuan = (params: any = {}) => {
  return request.get<any>('/app.do', {
    method: 'utopa.eshop.coupon.userGetCoupon',
    params: JSON.stringify({
      activityId: '875',
      businessId: '1695',
      clientType: 1,
      couponCode: '201202006255883037032',
      storeId: '538247',
      ...params
    })
  });
};

export const getSession = (params: any = {}) => {
  return request.get<any>('/app.do', {
    method: 'account.session.gen',
    params: JSON.stringify(params)
  });
};
// 商品详情
export const getGoodsDetails = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.product.getProduct',
    ver: '1.0',
    params: JSON.stringify(
      Object.assign(params, {
        accessToken: Taro.getStorageSync('login'),
        uid: Taro.getStorageSync('uid')
      })
    )
  });
};
export const discountCardDetailPage = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.product.discountCardDetailPage',
    ver: '1.0',
    params: JSON.stringify({ ...params })
  });
};
// 商品sku
export const getGoodsSku = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'neweshop.product.getProductSkuApp',
    //method: 'utopa.eshop.product.getProductSkuApp',
    ver: '1.0',
    params: JSON.stringify({ clientType: 1, sysId: 1, ...params })
  });
};
// 店铺推荐商品
export const getStoreRecommend = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.product.getStoreRecommendProduct',
    ver: '2.0',
    params: JSON.stringify(params)
  });
};
//收藏
export const favorite = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.favorite',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
export const unfavorite = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.unfavorite',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
// 秒杀预定
export const subscribe = (data: any) => {
  return request.post(
    '/service/app/user/seckillProduct/subscribe',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};
// 取消预定
export const cancleSubscribe = (data: any) => {
  return request.post(
    '/service/app/user/seckillProduct/cancleSubscribe',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};
