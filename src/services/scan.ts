import request from '../utils/request';

export interface ICodeGoods {
  price: number;
  productName: string;
  storeName: string;
  storeId: string;
  businessId: string;
  skuId: string;
  productSkuViewDto: {
    imgKey: string;
  };
}

export interface ICodeRst {
  code: number;
  data: {
    records: ICodeGoods[];
  };
}

export const getCodesNumber = (params: any) => {
  console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.getOfflineTotalNumber',
    ver: '2.0',
    params: JSON.stringify({})
  });
};

export const getCodes = (params: any) => {
  console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.getOfflineShopCartDetail',
    ver: '2.0',
    params: JSON.stringify({})
  });
};
export const getCodeCheckStore = (params: any) => {
  console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.selectPartOrNot',
    ver: '2.0',
    params: JSON.stringify({
      ids: ['245797'],
      isSelect: 1,
      storeId: '330674'
    })
  });
};

export const getCodeCheckGoods = (params: any) => {
  console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.updateShopCart',
    ver: '2.0',
    params: JSON.stringify({
      id: 123123,
      isSelect: 0 //1
    })
  });
};
export const getCodeDel = (params: any) => {
  console.log(params);
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.deleteShopCartPro',
    ver: '2.0',
    params: JSON.stringify({
      ids: '245797'
    })
  });
};

export const getCodeAddGoods = (params: any) => {
  console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.addOfflineShopCart',
    ver: '2.0',
    params: JSON.stringify(params)
  });
};
export const getAddCode = (params: any) => {
  console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.product.getByBarcodeNew',
    ver: '1.0',
    params: JSON.stringify({
      barcode: params.id
    })
  });
};
