import Taro, { useState } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
import { connect } from '@tarojs/redux';

import Wrap from '../../../components/Wrap';
import Img from '../../../components/Img';
import Txt from '../../../components/Txt';
import Bton from '../../../components/Bton';
import './index.scss';
import Router from '../../../utils/router';
import { getToken } from '../../../services/upload';
import { images } from '../../../images';
import Qiniu from '../../../utils/qiniu';
import { getCanBonce } from '../../../services/canbonce';

function Index({}) {
  const [imgs, setImgs] = useState<any>(['', '']);
  const [imgsh5, setImgsh5] = useState<any>(['', '']);
  const select = (index) => {
    Taro.chooseImage({
      success: (info) => {
        let temp = [...imgs];
        temp[index] = info.tempFilePaths[0];
        setImgs(temp);
        let temph5 = [...imgsh5];
        temph5[index] = info.tempFiles[0].originalFileObj;
        setImgsh5(temph5);

        //let temp = [...imgs];
        //let arr = temp.concat(info.tempFilePaths);
        //setImgs(arr);
      }
    });
  };
  const onSubmit = async () => {
    if (!imgs[0] || !imgs[1]) {
      Taro.showToast({
        title: '请选择照片',
        icon: 'none'
      });
      return;
    }
    const data = await getToken({});
    console.log('获取token');
    if (data.code === 0 && data.data && data.data.upToken) {
      Taro.showLoading({
        title: ''
      });
      let arr = imgs;
      if (process.env.TARO_ENV === 'h5') {
        arr = imgsh5;
      }
      arr.map((item) => {
        console.log('开始上传');
        console.log(data.data.upToken);
        console.log(item);
        Qiniu(data.data.upToken, item).then(
          (hash) => {
            Taro.hideLoading();
            feedbackKeys.push(hash);
            if (feedbackKeys.length === imgs.length) {
              onDone();
            }
          },
          () => {
            Taro.hideLoading();
            console.log('上传失败');
            feedbackKeys = [];
          }
        );
      });
    } else {
      Taro.showToast({
        title: '获取token失败!',
        icon: 'none'
      });
    }
  };
  let feedbackKeys: any = [];
  const onDone = async () => {
    console.log('上传成功');
    Taro.showLoading({
      title: ''
    });
    let data = await getCanBonce({
      paymentPicKey: feedbackKeys[0],
      voucherPicKey: feedbackKeys[1]
    });
    Taro.hideLoading();
    if (data.code === 0) {
      Taro.showToast({
        title: '提交成功',
        icon: 'none'
      });
      setTimeout(() => {
        Router.goBack();
      }, 2000);
    } else if (data.code === 100010003) {
      Router.goLogin();
    }
  };

  return (
    <View className="utp-canbonce">
      <View className="utp-canbonce-body">
        <View className="utp-canbonce-body-tips">
          <Wrap justifyContent="space-between">
            <View className="utp-canbonce-body-tips-box">
              <View className="utp-cnt">
                <Img src={images.jifen_one} width={90} />
              </View>
              <View>
                <Txt title="拍摄店铺小票" color="#816958" size={20} height={22} />
              </View>
              <View>
                <Txt title="与相应支付凭证" color="#816958" size={20} height={22} />
              </View>
            </View>
            <Img src={images.jifen_next} width={34} height={12} />
            <View>
              <View className="utp-cnt">
                <Img src={images.jifen_two} width={90} />
              </View>
              <View>
                <Txt title="上传店铺小票" color="#816958" size={20} height={22} />
              </View>
              <View>
                <Txt title="与相应支付凭证" color="#816958" size={20} height={22} />
              </View>
            </View>
            <Img src={images.jifen_next} width={34} height={12} />
            <View>
              <View className="utp-cnt">
                <Img src={images.jifen_three} width={90} />
              </View>
              <View>
                <Txt title="平台审核后" color="#816958" size={20} height={22} />
              </View>
              <View>
                <Txt title="增加相应积分" color="#816958" size={20} height={22} />
              </View>
            </View>
          </Wrap>
        </View>
        <View className="utp-canbonce-body-wrap">
          <Wrap type={1} justifyContent="space-between">
            <View className="utp-canbonce-body-box">
              <Img src={images.jifen_img1} width={344} height={400} />
            </View>
            <View className="utp-canbonce-body-box">
              <Img src={images.jifen_img2} width={344} height={400} />
            </View>
          </Wrap>
        </View>
        <View className="utp-canbonce-body-wrap">
          <Wrap type={1} justifyContent="space-between">
            <View
              className="utp-canbonce-body-box"
              onClick={() => {
                select(0);
              }}
            >
              {!imgs[0] && (
                <View className="utp-canbonce-body-box-tips">
                  <Txt title="上传店铺小票" color="#816958" size={26} />
                </View>
              )}
              {!imgs[0] && <Img src={images.jifen_add} width={344} height={400} />}
              {!!imgs[0] && <Img src={imgs[0]} width={344} height={400} />}
            </View>
            <View
              className="utp-canbonce-body-box"
              onClick={() => {
                select(1);
              }}
            >
              {!imgs[1] && (
                <View className="utp-canbonce-body-box-tips">
                  <Txt title="上传支付凭证" color="#816958" size={26} />
                </View>
              )}
              {!imgs[1] && <Img src={images.jifen_add} width={344} height={400} />}
              {!!imgs[1] && <Img src={imgs[1]} width={344} height={400} />}
            </View>
          </Wrap>
        </View>
      </View>
      <View className="utp-canbonce-ft" onClick={onSubmit}>
        <Bton>提交</Bton>
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '拍照积分',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff'
};
// @ts-ignore
export default connect((state) => state)(Index);
