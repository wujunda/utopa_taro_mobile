import Taro, { useState, useRouter } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import Safe from '../../../components/safe';
//import Bar from '../../../components/bar';
import Img from '../../../components/Img';
import Bton from '../../../components/Bton';
import Wrap from '../../../components/Wrap';
import Txt from '../../../components/Txt';
import fail from './../../../assets/pay/fail.png';
import success from './../../../assets/pay/success.png';
import loading from './../../../assets/pay/loading.png';
import './index.scss';

function Index() {
  const [status] = useState(1);

  const router = useRouter();
  const id = router.params.id;
  const orderNo = router.params.orderNo;
  console.log(id);
  return (
    <Safe>
      <View className="utp-pay">
        <Wrap flexDirection="column" type={3}>
          <View className="utp-pay-img">
            <Img width={140} src={status === 0 ? fail : status === 1 ? success : loading} />
          </View>
          <Txt
            title={status === 0 ? '支付失败' : status === 1 ? '支付成功' : '支付结果确认中'}
            size={36}
            color="deep"
          />

          {[0, 1].includes(status) ? (
            <View
              className="utp-pay-btn"
              onClick={() => {
                Taro.navigateTo({
                  url: `/pages/packOrder/orders/index?status=1`
                  //url: `/pages/packOrder/order/index?orderId=${id}`
                });
              }}
            >
              <Bton type={1}>查看订单详情</Bton>
            </View>
          ) : (
            <View className="utp-pay-btnbox">
              <View className="utp-pay-btn2">
                <Bton type={1}>暂不支付</Bton>
              </View>
              <View className="utp-pay-btn2">
                <Bton>已支付</Bton>
              </View>
            </View>
          )}
        </Wrap>
      </View>
    </Safe>
  );
}

Index.config = {
  navigationBarTitleText: '支付结果',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff'
};
// @ts-ignore
export default connect((state) => state)(Index);
