import Taro, { useState, useEffect, useRouter, useDidShow } from '@tarojs/taro';
import { View, Text, Textarea } from '@tarojs/components';
import Safe from '@/components/safe';
import Wrap from '../../../components/Wrap';
import RefundGoodsItem from '@/components/RefundGoodsItem';
import './index.scss';
import Img from '@/components/Img';
import ViewPickReason from './selectPick';
import { UploadImgCallBack, handleRequest, Glo, PriceFormat } from '@/utils/utils';
import cameraIcon from '../../../assets/mine/icon_addphoto.png';
import {
  returnsReasons,
  getPostFeeBalance,
  applyRefundMoney,
  applyRefundMonAndPro,
  getRefundDetailInfo,
  updateRefundApply,
  getByOrderDetailId,
  getProductDetailBalance
} from '../../../services/afterSale';
import NumberInput from './numberInput/index';
import { connect } from '@tarojs/redux';
import { _fetchToken } from '@/services/login';
import JSBridge from '@/utils/jsbridge/index';

interface Params {
  goodsNum: Number; //商品退款数量
  applyRefundReason?: String;
  applyRefundReasonCode: String; // 退款原因 code
  goodsRefundFee: Number; // 退款金额 单位 分
  postRefundFee: Number; // 运费退款金额，单位为分
  applyRefundDescription?: String;
  refundOrderNo: String;
  refundMoney: Number;
  refundRole: Number; // 退款角色
  refundType: String; // 退款类型
  applyRefundCertificatePics: any; //图片 key
}
type PageStateProps = {
  user: {
    inApp: boolean;
    isWeiXinFalg: boolean;
  };
};
const goodsOptions = {
  name: 'name'
};
const upStyle = {
  margin: '10px 10px 10px 0'
};
const Index: Taro.FC<Params> = (props: any) => {
  //const maxRefundMoney = Number(Glo.formatPrice(product.orderTotalMoney));
  const router = useRouter();
  // let orders = Taro.getStorageSync('product');
  const [getUploadImg, setUploadImg] = useState([] as any[]);
  const [dataReason, setDataReason] = useState<any>([]);
  const [imgKey, setImgKey] = useState<any>([]);
  const [goodsItem, setGoodsItem] = useState<any>({});
  // 最多可退
  const [maxRefund, setMaxRefundMoney] = useState<any>({
    maxGoodsRefundFee: 0,
    maxPostFee: 0,
    hasReBackPostFee: 0,
    maxGoodsNum: 0
  });
  useEffect(() => {
    Glo.loading('正在加载..');
    try {
      if (props.user.inApp) {
        (JSBridge as any).Common.Caller_Common_Base_getOpenToken('', (responsed) => {
          console.log(responsed, 'responsed');
          if (responsed) {
            _fetchToken({
              openToken: responsed
            }).then((res) => {
              if (res.code === 0) {
                console.log('代码测试');
                Taro.setStorage({
                  key: 'isLogin',
                  data: {
                    accessToken: res.data.accessToken
                  }
                });
                setTimeout(() => {
                  initDta();
                }, 2000);
              }
            });
          }
        });
      } else {
        initDta();
      }
    } catch (e) {
    } finally {
    }
    // try {
    //   console.log('登陆app');
    //   if (props.user.inApp) {
    //     console.log('登陆app');
    //     inAppIsLogin().then((res) => {
    //       if (res == true || res) {
    //         setTimeout(() => {
    //           initDta();
    //         }, 1000);
    //       } else {
    //         console.log('获取app token异常');
    //       }
    //     });
    //   } else {
    //     console.log('h5');
    //     initDta();
    //   }
    // } catch (e) {
    // } finally {
    // }
  }, []);
  const initDta = async () => {
    //let isAppLogin = await inAppIsLogin();
    // await getAppToken();
    getReasonList();
    getUpdataDetailInfo();
    _getByOrderDetailId();
  };
  useDidShow(() => {});
  //const maxRefundMoney = Number(Glo.formatPrice(product.orderTotalMoney));
  //售后类型
  const refundType = router.params.type;
  const [params, setParams] = useState(({
    orderId: '',
    goodsNum: 0,
    refundRole: 1,
    applyRefundReason: '',
    goodsRefundFee: maxRefund.maxGoodsRefundFee,
    refundType: refundType,
    postRefundFee: 0,
    applyRefundDescription: '',
    applyRefundReasonCode: ''
  } as unknown) as Params);
  const UPLOAD_MAX_SIZE: number = 4;
  //const HEAD_TITLE = (router.params.type as any) === '1' ? '仅退款' : '退货退款';

  // 获取商品可退款信息
  const _getProductDetailBalance = async (data) => {
    // eslint-disable-next-line no-shadow
    let params = {
      refundOrderId: data.refundOrderId || undefined,
      orderDetailId: data.orderDetailId
    };
    if (!params.refundOrderId) {
      delete params.refundOrderId;
    }
    let res: any = await getProductDetailBalance(params);
    if (handleRequest(res as any)) {
      // eslint-disable-next-line no-shadow
      let data = res.data;
      setMaxRefundMoney((state) => ({
        ...state,
        maxGoodsRefundFee: Number(PriceFormat(data.balance))
      }));
      setParams((state) => ({
        ...state,
        goodsRefundFee: Number(PriceFormat(data.balance))
      }));
    }
  };
  // 申请退款商品信息
  const _getByOrderDetailId = async () => {
    if (router.params.update) return;
    Glo.loading('正在加载');
    try {
      let detailId = router.params.orderLineId;
      // fix 兼容 安卓 旧版APP 拼?参导致BUG
      if (detailId.includes('?')) {
        let fixId = detailId.split('?');
        detailId = fixId[0];
      }
      let res = await getByOrderDetailId({ orderdetail: detailId });
      if (handleRequest(res as any)) {
        let data = res.data;
        getFeeBalance({ orderId: data.orderId, refundOrderId: null });
        await _getProductDetailBalance({
          orderDetailId: detailId,
          refundOrderId: null
        });
        setParams((state) => ({
          ...state,
          // goodsRefundFee: Number(PriceFormat(data.payPrice) * data.num),
          orderId: data.orderId,
          orderDetailId: data.id,
          skuId: data.skuId,
          goodsNum: Number(data.num - data.cancelNum)
        }));

        setMaxRefundMoney((state) => ({
          ...state,
          maxGoodsNum: Number(data.num - data.cancelNum)
        }));
        setGoodsItem({
          name: data.name,
          image: data.pics[0] || '',
          norm1Name: data.skuProperties,
          num: Number(data.num - data.cancelNum),
          price: data.payPrice,
          picIds: data.pics[0] || ''
        });
      }
    } catch (e) {
      Glo.showToast(e);
    } finally {
      Glo.hideLoading();
    }
  };

  /**
   * 获取可退款运费
   * **/
  const getFeeBalance = async (data) => {
    //balance 可退运费 //hasReBackPostFee 已退运费
    // if (router.params.update && router.params.orderId) return;
    if (!data) return;
    // eslint-disable-next-line no-shadow
    let params = {
      refundOrderId: data.refundOrderId && data.refundOrderId,
      orderId: data.orderId
    };
    if (!params.refundOrderId) {
      delete params.refundOrderId;
    }
    let res = await getPostFeeBalance(params);
    if (handleRequest(res as any)) {
      setMaxRefundMoney((state) => ({
        ...state,
        hasReBackPostFee: Number(PriceFormat(res.data.hasReBackPostFee)),
        maxPostFee: Number(PriceFormat(res.data.balance))
      }));
      setParams((state) => ({
        ...state,
        postRefundFee: Number(PriceFormat(res.data.balance))
      }));
    }
  };
  /**
   * 申请原因
   * **/
  const getReasonList = async () => {
    var res = await returnsReasons({});
    if (handleRequest(res as any)) {
      setDataReason(res.data.reasons);
    }
  };

  //   to do....
  const formatGoodsField = (
    obj,
    options: any = {
      name: 'productName',
      norm1Name: 'skuInfo',
      image: 'pics'
    }
  ) => {
    try {
      return {
        name: options && options.name ? obj[options.name] : obj.name,
        num: options && options.num ? obj[options.num] : obj.num,
        image: options && options.image ? obj[options.image] : obj.image,
        norm1Name: options && options.norm1Name ? obj[options.norm1Name] : obj.skuInfo,
        price: options && options.price ? Number(obj[options.price]) : Number(obj.price)
      };
    } catch (e) {
      console.log(e);
    }
  };

  // 从退款详情过来修改
  const getUpdataDetailInfo = async () => {
    if (!router.params.update || !router.params.refundOrderNo) return;
    try {
      Glo.loading('正在加载...');
      let res = await getRefundDetailInfo({ refundOrderNo: router.params.refundOrderNo });
      if (handleRequest(res as any)) {
        let data = res.data.refundDetailMasterBody;
        setParams((state) => ({
          ...state,
          applyRefundReason: data.applyRefundReason,
          applyRefundReasonCode: data.applyRefundReasonCode,
          applyRefundDescription: data.applyRefundDescription,
          orderDetailId: data.refundSkuDetails[0].orderDetailId,
          orderId: data.orderId,
          // 可退数量
          goodsNum: data.refundSkuDetails[0].canReturnNum,
          refundOrderDetailId: data.refundSkuDetails[0].refundOrderDetailId,
          skuId: data.refundSkuDetails[0].skuId || '',
          refundOrderId: data.id
        }));
        // 限制商品 最多数量
        setMaxRefundMoney((state) => ({
          ...state,
          maxGoodsNum: data.refundSkuDetails[0].canReturnNum + data.refundSkuDetails[0].num
        }));
        // 订单  id  退款
        getFeeBalance({ orderId: data.orderId, refundOrderId: data.id });
        // data.refundSkuDetails[0].orderDetailId
        _getProductDetailBalance({
          orderDetailId: data.refundSkuDetails[0].orderDetailId,
          refundOrderId: data.id
        });
        // 可退
        // setMaxRefundMoney((state) => ({
        //   ...state,
        //   maxGoodsRefundFee: Number(PriceFormat(data.goodsRefundFee))
        // }));
        // 编辑回显 set key img
        if (Array.isArray(data.certificatePics) && data.certificatePics.length > 0) {
          // eslint-disable-next-line no-shadow
          let imgKey: any = [];
          let imgUrl: any = [];
          data.certificatePics.map((item: any) => {
            imgKey.push(item.key);
            imgUrl.push(item.url);
          });
          setUploadImg(imgUrl);
          setImgKey(imgKey);
        }
        // eslint-disable-next-line no-shadow
        let goodsItem = data.refundSkuDetails[0];
        goodsItem.num = data.refundSkuDetails[0].canReturnNum + data.refundSkuDetails[0].num;
        setGoodsItem(formatGoodsField(goodsItem));
      }
    } catch (e) {
    } finally {
      Glo.hideLoading();
    }
  };

  /***
   * 保存
   * products[0].detailId ，skuId 目前定的是 只有一个。so默认取第一个。
   * ***/
  const commit = async () => {
    // eslint-disable-next-line no-use-before-define
    Glo.loading('正在提交..');
    let data: any = {
      ...params,
      ...{ goodsNum: Number(params.goodsNum) },
      ...{ goodsRefundFee: Number(PriceFormat(params.goodsRefundFee as any, 1)) },
      ...{ postRefundFee: Number(PriceFormat(params.postRefundFee as any, 1)) },
      ...{ applyRefundCertificatePics: imgKey }
    };
    let METHODS: any = null;
    // 退款
    if (!router.params.update && router.params.type === '1') {
      METHODS = applyRefundMoney(data);
    }
    //退货退款
    if (!router.params.update && router.params.type === '2') {
      METHODS = applyRefundMonAndPro(data);
    }
    // 修改
    if (router.params.update) {
      METHODS = METHODS = updateRefundApply(data);
    }
    let res = await METHODS;
    if (handleRequest(res as any)) {
      Glo.showToast('提交成功..');
      let refundOrderNo = router.params.update
        ? router.params.refundOrderNo
        : res.data.refundOrderNo;
      setTimeout(() => {
        Taro.navigateTo({
          url: '/pages/person/refundDetail/index?refundOrderNo=' + refundOrderNo
        });
      }, 1000);
    }
  };

  // eslint-disable-next-line react/no-multi-comp
  const renderViewUploadImg = () => {
    return (
      <View
        className="upload-item"
        onClick={() =>
          UploadImgCallBack((item: any) => {
            console.log(item);
            if (item.type === 1) {
              let cloneImg = [...getUploadImg];
              let list = cloneImg.concat(item.path);
              imgKey.push(item.key);
              setImgKey(imgKey);
              setUploadImg(list);
            }
          })
        }
      >
        <View>
          <Img src={cameraIcon} width={210} />
        </View>
      </View>
    );
  };
  return (
    <Safe>
      {/* <Bar title={HEAD_TITLE} /> */}
      <View className="apply-refund">
        <View>{!!goodsItem && <RefundGoodsItem options={goodsOptions} item={goodsItem} />}</View>
        <View className="apply-refund-shop" style={{ background: '#fff' }}>
          {refundType === '2' && (
            <View style={{ height: 35, margin: '10px 0' }}>
              <Wrap type={1} justifyContent="space-between" flexWrap="nowrap">
                <View style={{ width: 400 }}>
                  <Text className="apply-refund-shop-text">申请数量</Text>
                </View>
                <View>
                  <NumberInput
                    onInput={(v) => {
                      setParams((state) => ({
                        ...state,
                        goodsNum: v
                      }));
                    }}
                    qty={params.goodsNum as any}
                    max={maxRefund.maxGoodsNum}
                  />
                </View>
              </Wrap>
            </View>
          )}
          <View className="apply-refund-shop-reason" style={{ height: 40 }}>
            <ViewPickReason
              leftText="申请原因"
              defaultValue={params.applyRefundReason}
              data={dataReason}
              onChange={(v) => {
                console.log(v, '选择原因');
                setParams((state) => ({
                  ...state,
                  applyRefundReason: v.reason,
                  applyRefundReasonCode: v.reasonCode
                }));
              }}
            />
          </View>
          <View>
            <View style={{ height: 35 }}>
              <Wrap type={2} justifyContent="space-between" flexWrap="nowrap">
                <View style={{ width: 400 }}>
                  <Text className="apply-refund-shop-text">商品退款</Text>
                  <Text className="apply-refund-shop-words">
                    最多可退￥{maxRefund && maxRefund.maxGoodsRefundFee}
                  </Text>
                </View>
                <View>
                  <NumberInput
                    onInput={(v) => {
                      setParams((state) => ({
                        ...state,
                        goodsRefundFee: v
                      }));
                    }}
                    qty={params.goodsRefundFee as any}
                    max={maxRefund.maxGoodsRefundFee}
                  />
                </View>
              </Wrap>
            </View>
            <View style={{ height: 35, margin: '20px 0' }}>
              <Wrap type={1} justifyContent="space-between" flexWrap="nowrap">
                <View style={{ width: 400 }}>
                  <Text className="apply-refund-shop-text">运费退款</Text>
                  <Text className="apply-refund-shop-words">
                    累计已退￥{maxRefund && maxRefund.hasReBackPostFee}，还可退￥
                    {maxRefund && maxRefund.maxPostFee}
                  </Text>
                </View>
                <View>
                  <NumberInput
                    onInput={(v) => {
                      setParams((state) => ({
                        ...state,
                        postRefundFee: v
                      }));
                    }}
                    max={maxRefund && maxRefund.maxPostFee}
                    qty={params.postRefundFee as any}
                  />
                </View>
              </Wrap>
            </View>
          </View>
        </View>
        <View style={{ background: '#fff' }} className="apply-refund-desc">
          <Text>补充说明(选填)</Text>
          <Textarea
            autoHeight
            className="apply-refund-desc-area"
            value={params.applyRefundDescription as any}
            style={{ height: 100, width: '100%', border: 'none', outline: 'none', resize: 'none' }}
            maxlength={500}
            placeholder="请输入您的补充说明"
            onInput={(e) => {
              setParams((state) => ({
                ...state,
                applyRefundDescription: e.detail.value
              }));
            }}
          />
        </View>
        <View style={{ background: '#fff' }} className="apply-refund-upload">
          <Text>上传照片(选填)</Text>
          <Wrap type={2} flexDirection="row">
            {getUploadImg &&
              getUploadImg.map((item) => {
                return (
                  // eslint-disable-next-line react/jsx-key
                  <View style={{ display: 'inline-block', margin: upStyle.margin }}>
                    <Img src={item} width={210} />
                  </View>
                );
              })}
            {getUploadImg.length < UPLOAD_MAX_SIZE && (
              <View style={upStyle}>{renderViewUploadImg()}</View>
            )}
          </Wrap>
          <Text className="apply-refund-upload-limit">
            最多{UPLOAD_MAX_SIZE}张，支持jpg,png,bmg格式
          </Text>
        </View>
        <View className="apply-refund-commit">
          <View className="apply-refund-commit-btn" onClick={() => commit()}>
            提交并审核
          </View>
        </View>
      </View>
    </Safe>
  );
};

Index.config = {
  navigationBarTitleText: '退款申请',
  enablePullDownRefresh: true,
  disableScroll: false,
  navigationStyle: 'default'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    }
  };
};
export default connect(mapStateToProps as any)(Index as any);
// @ts-ignore
// eslint-disable-next-line no-undef
//export default Index;
