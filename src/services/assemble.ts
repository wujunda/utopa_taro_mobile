import request from '../utils/request';

export interface Iprops {
  code: number;
  data: {
    activityStatus: number;
    groupStatus: number;
    restParticipantCount: number;
    groupStartTime: number;
    groupEndTime: number;
    currentUserMemberType: number;
    records: any[];
  };
}
// 拼团列表
export const findGroupDetailByUid = (tab: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'groupbooking.group.findGroupDetailByUid',
    ver: '1.0',
    params: JSON.stringify({
      groupStatus: tab,
      pageNo: 1
    })
  });
};
// 拼团商品详情
export const findGroupDetailById = (groupId: any) => {
  // console.log(groupId);
  return request.post<any>('/app.do', {
    method: 'groupbooking.group.findGroupDetailById',
    ver: '1.0',
    params: JSON.stringify({
      groupId: 2729
    })
  });
};
// 商品推荐
export const findRecommendedGoods = (id: any) => {
  return request.post<any>('/app.do', {
    method: 'groupbooking.goods.findRecommendedGoods',
    ver: '1.0',
    params: JSON.stringify({
      id,
      pageNo: 1,
      pageSize: 10
    })
  });
};
