import Taro from '@tarojs/taro';
import { View, Image } from '@tarojs/components';
import Wrap from '@/components/Wrap';
import './index.scss';

interface PropsFlow {
  name?: string;
  image?: string;
  title?: string;
  imgHeight?: number;
  dotImg?: string;
  goods: any;
  type?: any;
  imgClick?: () => void;
  onClick?: () => void;
}
const FlowGoodsItem: Taro.FC<PropsFlow> = (props: any) => {
  const { imgHeight, imgClick, goods, onClick } = props;
  return (
    goods && (
      <View
        className="float-item"
        onClick={() => {
          onClick && onClick();
        }}
      >
        <View
          onClick={() => {
            imgClick && imgClick();
          }}
          className="float-item-img"
          style={{ height: imgHeight }}
        >
          <Image
            className="goods-img"
            src={goods.productImage as any}
            style={{ height: imgHeight, width: '100%' }}
          ></Image>
        </View>
        <View className="float-item-content">
          <View className="float-item-content-title"> {goods.productName}</View>
          <View className="dot-box">
            <Wrap type={2}>
              <View className="dots">
                <Image
                  src={goods.productImage as any}
                  style={{
                    height: Taro.pxTransform(40),
                    width: Taro.pxTransform(40),
                    borderRadius: '50%'
                  }}
                ></Image>
              </View>
              <View className="dot-name"> {goods.storeName}</View>
            </Wrap>
          </View>
        </View>
      </View>
    )
  );
};
export default FlowGoodsItem;
// // to do
// export const FlowGoodsBox = (props: any) => {
//   const { children, count, width } = props;
//   return Taro.useMemo(() => {
//     return (
//       children && (
//         <View style={{ width: width, columnCount: count }} className="flow-goods-box">
//           {children}
//         </View>
//       )
//     );
//   }, [children, count, width]);
// };
