import { View, Text, Image } from '@tarojs/components';
import Taro, { useState, useRouter, useEffect } from '@tarojs/taro';
import Wrap from '../../../components/Wrap';
// import Goods1 from '../../../components/Goods1';
import check from '../../../assets/cart/check.png';
import uncheck from '../../../assets/submit/uncheck.png';
// import Bar from '../../../components/bar';
import Yuan from '../../../components/Yuan';
// import Loading from '../../../components/Loading';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
// import LoadingMore from '../../../components/LoadingMore';
// import LoadingNone from '../../../components/LoadingNone';
// import LoadingNoneMore from '../../../components/LoadingNoneMore';
import Safe from '../../../components/safe';
// import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
// import useSession from '../../../hooks/useSession';
// import useLogin from '../../../hooks/useLogin';
// import useResetCount from '../../../hooks/useResetCount';
// import { ProductBaiscData } from '../../../interface/product';
import down from '../../../assets/wishList/down.png';
import del from '../../../assets/search/delete.png';
import { getWiseThemeProduct, ProductsType } from '../../../services/wishList';
import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const router = useRouter();
  const [show, setShow] = useState(false);
  const [list, setList] = useState([] as ProductsType[]);
  const [all, setAll] = useState([] as ProductsType[]);
  // const [haveSession] = useSession();
  // const [isLogin, updateLogin] = useLogin();
  // if (haveSession && !isLogin) {
  //   if (updateLogin) {
  //     updateLogin();
  //   }
  // }
  // 购物车数据
  // @ts-ignore
  let payload = {
    wiseThemeId: router.params.wiseThemeId
  };
  const title = router.params.title;

  // const [state, hasMore, update, getMoreData, loading] = useRequestWIthMore(
  //   payload,
  //   getWiseThemeProduct
  // );
  // const [can, update1] = useResetCount();
  // if (
  //   haveSession &&
  //   isLogin &&
  //   (state === null || (state != null && state.length == 0)) &&
  //   update &&
  //   !loading
  // ) {
  //   if (can) {
  //     update({});
  //     update1();
  //   }
  // }
  // var goods = [];
  // if (state && state.length > 0) {
  //   state.map((item) => {
  //     item.data.records.map((it) => {
  //       it.check=false
  //       goods.push(it);
  //     });
  //   });
  // }
  const getNorms = (arr) => {
    let str = '';
    arr.forEach((element) => {
      str += element.normValue + ';';
    });
    return str;
  };
  useEffect(() => {
    const getPro = async () => {
      let temp;
      if (Taro.getStorageSync('infoBox')) {
        temp = Taro.getStorageSync('infoBox').productList;
        if (temp) {
          setList(temp);
          // setCount(temp.length);
        } else {
          setList([]);
          // setCount(0);
        }
      }
      let res = await getWiseThemeProduct(payload);
      if (res.code === 0) {
        let pay = res.data.records;
        pay.forEach((ele) => {
          temp.forEach((ele1) => {
            if (ele.productId == ele1.productId && ele.businessId == ele1.businessId) {
              ele.check = true;
            }
          });
        });
        setAll(pay);
      }
    };
    getPro();
  }, []);
  return (
    <Safe>
      <View className="utp-selectPro">
        <Wrap Myheight={88} className="utp-selectPro-bar" justifyContent="flex-end">
          <Txt
            title="确定"
            size={28}
            color="active"
            onClick={() => {
              let temp = Taro.getStorageSync('infoBox');
              temp.productList = list;
              Taro.setStorage({
                key: 'infoBox',
                data: temp
              });
              Taro.navigateTo({
                url: !router.params.id
                  ? '/pages/wishList/addWishList/index?back=true'
                  : `/pages/wishList/addWishList/index?back=true&id=${router.params.id}`
              });
            }}
          />
        </Wrap>
        <View className="utp-selectPro-top">
          <Wrap Myheight={120} justifyContent="space-between" flexWrap="nowrap">
            <Txt title="已选商品：" size={28} color="low" />
            <Wrap className="utp-selectPro-topBox">
              {list.length <= 4 &&
                list.map((i, ii) => {
                  return (
                    <Image
                      src={i.picUrl || i.skuImgs[0]}
                      className="utp-selectPro-top-img"
                      key={ii}
                    />
                  );
                })}
              {list.length > 4 &&
                [0, 1, 2, 3].map((p) => {
                  return (
                    <Image
                      src={list[p].picUrl || list[p].skuImgs[0]}
                      className="utp-selectPro-top-img"
                      key={p}
                    />
                  );
                })}
            </Wrap>
            <Wrap
              click={() => {
                setShow(!show);
              }}
            >
              <Txt size={28} title={`共${list.length}件`} />
              <Img src={down} width={28} />
            </Wrap>
          </Wrap>
        </View>
        {show && (
          <View
            className="utp-selectPro-mask"
            onClick={() => {
              setShow(false);
            }}
          ></View>
        )}
        {show && (
          <View className="utp-selectPro-selectBox">
            {list.length > 0 &&
              list.map((o, oo) => {
                return (
                  <Wrap justifyContent="space-between" Myheight={120} key={oo}>
                    <Image src={o.picUrl || o.skuImgs[0]} className="utp-selectPro-selectBox-Img" />
                    <Text className="utp-selectPro-selectBox-title">{o.name || o.productName}</Text>
                    <Txt title="*1" size={28} color="deep" />
                    <Img
                      src={del}
                      width={36}
                      onClick={() => {
                        let temp = [...list];
                        let re = [...all];
                        let index = re.findIndex((i) => i.productId === o.productId);
                        if (index > -1) {
                          re[index].check = false;
                          setAll(re);
                        }
                        temp.splice(oo, 1);
                        setList(temp);
                      }}
                    />
                  </Wrap>
                );
              })}
            {list.length === 0 && (
              <Wrap Myheight={470} justifyContent="center">
                <Txt color="low" title="还没有添加商品哦~" size={32} />
              </Wrap>
            )}
          </View>
        )}
        <View className="utp-selectPro-productsBox">
          {all &&
            all.map((item, index) => {
              return (
                <View className="utp-selectPro-productBox" key={index}>
                  <Wrap justifyContent="space-between">
                    <Image src={item.skuImgs[0]} className="utp-selectPro-productBox-img" />
                    <Wrap flexDirection="column" top Myheight={250} justifyContent="space-between">
                      <Text className="utp-selectPro-productBox-title">{item.productName}</Text>
                      {item.productSkuNorms != null && (
                        <Text className="utp-selectPro-productBox-norms">
                          {getNorms(item.productSkuNorms)}
                        </Text>
                      )}
                      <Wrap Myheight={38}>
                        <View className="utp-selectPro-productBox-price">
                          <Yuan price={item.price} color="#FF2F7B" size={40}></Yuan>
                        </View>
                        <View className="utp-selectPro-productBox-orprice">
                          <Yuan price={item.skuPrice}></Yuan>
                        </View>

                        {item.activeLabel && (
                          <View className="utp-selectPro-productBox-label">{item.activeLabel}</View>
                        )}
                      </Wrap>
                      <Txt title={item.storeName} color="low" size={24} />
                    </Wrap>
                    <Img
                      src={item.check ? check : uncheck}
                      width={36}
                      onClick={() => {
                        let pay = [...list];
                        let temp = [...all];
                        temp[index].check = !temp[index].check;
                        if (temp[index].check) {
                          pay.push(item);
                          setList(pay);
                        } else {
                          let res = pay.filter((i) => i.productId !== item.productId);
                          setList(res);
                        }
                        setAll(temp);
                      }}
                    />
                  </Wrap>
                </View>
              );
            })}
        </View>
      </View>
    </Safe>
  );
};

Index.config = {
  navigationBarTitleText: '浏览历史',
  disableScroll: false
  //navigationStyle: 'custom',
  //usingComponents: {
  //Bar: '../../../components/bar' // 书写第三方组件的相对路径
  //}
};
Index.defaultProps = {};
export default Index;
