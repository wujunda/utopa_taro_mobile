/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/*
 后端新架构请求方法
*/
import Taro from '@tarojs/taro';
import qs from 'query-string';
import { objKeySort } from './utils';
import { TOAST_MESSAGE, ERROR_CODE } from './common_enum';
import { Glo } from '../utils/utils';
import { version } from '../../package.json';

// eslint-disable-next-line import/first
import JSBridge from '@/utils/jsbridge/index';
// eslint-disable-next-line import/first
import { _fetchToken } from '@/services/login';

// eslint-disable-next-line import/no-commonjs
const md5 = require('js-md5');

export interface BossApiResponseData<T = any> {
  status: number;
  code: string;
  message: string;
  data: T;
}

interface SessionApiRes {
  appKey: string;
  sessionKey: string;
  sessionSecret: string;
  expireTime: number;
  renewTime: number;
  extendData: any;
}

// 新架构会话缓存key
const BOSS_SESSION_KEY = 'BOSS_SESSION_KEY';
// 新架构会话秘钥缓存key
const BOSS_SESSION_SECRET = 'BOSS_SESSION_SECRET';

// 获取会话的路径
const BOSS_SESSION_API_PATH = '/core/account/session/genSession';

// const BOSS_SERVER_URL = 'https://boss-api-test.myutopa.com/eshop';
// const APP_KEY = 'e42ee29bdbba499aa08930e9bcc2343d';

const BOSS_SERVER_URL = process.env.BOSS_SERVER_URL;
const APP_KEY = process.env.APP_KEY;

const CONTENT_TYPE_FORM = 'application/x-www-form-urlencoded;charset=utf-8';
const CONTENT_TYPE_JSON = 'application/json;charset=utf-8';

/* eslint-disable no-undef */
// 设备信息
const deviceInfo = (() => {
  const res = Taro.getSystemInfoSync();
  return {
    appInfo: version,
    osInfo: res.system,
    deviceType: `${res.brand}_${res.model}`,
    platform: res.platform,
    windowWidth: res.windowWidth,
    source: Taro.getEnv() === Taro.ENV_TYPE.WEAPP ? 'wx' : 'h5'
  };
})();

// actiontBrigToken();
const getLoginInfo = () => {
  let isLogin = Taro.getStorageSync('isLogin');
  let isLoginStore = Glo.store.getState().cart.isLogin;
  console.log(isLogin, isLoginStore, '请求---信息');
  try {
    return isLogin
      ? {
          accessToken: isLogin && isLogin.accessToken
          // uid: isLogin && isLogin.uid ? isLogin.uid : undefined
        }
      : {};
  } catch (e) {
    console.error('getLoginInfo--error', e);
  }
};

// 获取session
export const getSession = async (reset = true) => {
  if (!reset) {
    let { data: sessionKey } = await Taro.getStorage({ key: BOSS_SESSION_KEY });
    let { data: sessionSecret } = await Taro.getStorage({ key: BOSS_SESSION_SECRET });
    if (sessionKey && sessionSecret) {
      return {
        sessionKey,
        sessionSecret
      };
    }
  }

  try {
    // 需要重新获取
    const res = await Taro.request<BossApiResponseData<SessionApiRes>>({
      method: 'POST',
      url: BOSS_SERVER_URL + BOSS_SESSION_API_PATH + `?appKey=${APP_KEY}`,
      retryTimes: 2
    });

    if (res.data.status === 1 && res.data.data) {
      const { sessionKey: key, sessionSecret: secret } = res.data.data;
      await Taro.setStorage({ key: BOSS_SESSION_KEY, data: key });
      await Taro.setStorage({ key: BOSS_SESSION_SECRET, data: secret });

      return { sessionKey: key, sessionSecret: secret };
    }
    // 返回异常
    Taro.showToast({ title: res.data.message || res.data.code });
  } catch (error) {
    console.error('获取会话异常', error);
    Taro.showToast({ title: TOAST_MESSAGE.API_ERROR });
  }

  return {};
};

const getSignByQuery = async (query: object, { sessionSecret }: any) => {
  // 把加密数据对象拼接起来
  try {
    // const handlerDataStr = (obj) => {
    //   console.log(obj, 'obj--->');
    //   return Object.keys(obj).reduce((preVal, k) => preVal + k + obj[k].toString(), '');
    // };
    const handlerDataStr = (obj: any) => {
      let str: any = '';
      if (obj) {
        for (let key in obj) {
          str += key + obj[key];
        }
      }
      return str;
    };
    // console.log(handlerDataStr(query), ' handlerDataStr(query)-------------------');
    const signStr = sessionSecret + handlerDataStr(query) + sessionSecret;
    return md5(signStr);
  } catch (e) {
    console.error('getSignByQuery异常', e);
  }
};

/**
 * 新架构电商请求方法
 * @param path 请求路径
 * @param data 业务数据
 * @param method 请求方法
 * @param headers 额外请求头
 */
export async function bossRequest<T = any>(
  path: string,
  data: object = {},
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' = 'GET',
  headers = {}
): Promise<BossApiResponseData<T>> {
  let query = {};
  let body = {};
  let url = BOSS_SERVER_URL + path;
  console.log('bossRequest');
  if (method === 'GET') {
    Object.assign(query, data);
  } else {
    body = data;
  }
  // 处理path上本身带有query的情况
  const parsedUrl = qs.parseUrl(url);
  url = parsedUrl.url;
  const session = await getSession();

  console.log(getLoginInfo(), '-getLoginInfo()用户登陆问题-3');
  // query要根据首字母排序
  query = objKeySort(
    Object.assign({}, deviceInfo, parsedUrl.query, query, getLoginInfo(), {
      sessionKey: session.sessionKey,
      appKey: APP_KEY,
      timestamp: Math.floor(Date.now() / 1000)
    })
  );
  const sign = await getSignByQuery(query, session);

  Object.assign(query, { sign });

  url = qs.stringifyUrl({ url, query }); // url加上参数

  const options = {
    url,
    method,
    data: method === 'GET' ? {} : body,
    header: {
      'Content-Type': method === 'GET' ? CONTENT_TYPE_FORM : CONTENT_TYPE_JSON,
      ...headers
    }
  };
  return Taro.request<BossApiResponseData<T>>(options)
    .then(handlerStatusCode) // 处理http status code
    .then(handlerResCode()) // 处理后端返回的status code
    .then((res) => res.data)
    .catch(handlerCatchError);
}

function handlerStatusCode<T extends Taro.request.SuccessCallbackResult<any>>(res: T) {
  const { statusCode } = res;
  console.log(res, 'handlerStatusCode');
  if (statusCode === 403) {
    throw new Error('Error 403: API rate limit exceeded, required login!');
  }

  return res;
}

const handlerResCode = () => async <
  T extends Taro.request.SuccessCallbackResult<BossApiResponseData>
>(
  res: T
) => {
  // const RETRY_AGAIN = 'RETRY_AGAIN';
  if (res.data.code === ERROR_CODE.SYS_OK && res.data.status === 1) {
    return res;
  } else if (res.data.code === ERROR_CODE.USER_NOT_LOGIN) {
    // 用户未登录
    Taro.setStorage({
      key: 'isLogin',
      data: ''
    });
    Glo.store.dispatch({
      type: 'cart/logout',
      payload: {}
    });
    Taro.navigateTo({
      url: '/pages/login/index'
    });
    throw new Error(ERROR_CODE.USER_NOT_LOGIN);
  }
  // else if (res.data.code === ERROR_CODE.EXPIRE_ERROR && args[3] && args[3][RETRY_AGAIN] === undefined) { // 会话过期需要重新请求一次
  //   await Taro.setStorage({ key: BOSS_SESSION_KEY, data: null });
  //   await Taro.setStorage({ key: BOSS_SESSION_SECRET, data: null });
  //   const result = await bossRequest<T>(args[0], args[1], args[2], {
  //     [RETRY_AGAIN]: 1,
  //     ...args[3]
  //   });
  //   return result.data
  // }

  return res;
};

function handlerCatchError(error): BossApiResponseData {
  const message = error.message;
  console.error({
    title: `${TOAST_MESSAGE.API_ERROR}.【${message}】`
  });

  return {
    status: 0,
    message,
    data: null,
    code: ''
  };
}
