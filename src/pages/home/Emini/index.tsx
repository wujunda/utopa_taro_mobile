import Taro from '@tarojs/taro';
import { connect } from '@tarojs/redux';

import { View } from '@tarojs/components';
import Router from '../../../utils/router';
import Img from '../../../components/Img';
import Wrap from '../../../components/Wrap';

type PageStateProps = {
  arr: any[];
};

type PageDispatchProps = {};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

//let cishu: number = 0;
const Index: Taro.FC<IProps> = (props) => {
  return (
    <View className="utp-cnt" style={{ background: '#f8f8f8', borderRadius: Taro.pxTransform(12) }}>
      <Wrap justifyContent="center">
        {props.arr &&
          props.arr.map((item1, index1) => {
            let width = 356;
            let height = 254;
            if (index1 > 2 && index1 < 7) {
              width = 168;
            }
            if (index1 === 1 || index1 === 7) {
              width = 354;
            }
            if (index1 === 4) {
              width = 186;
            }
            if (index1 === 5) {
              width = 168;
            }
            if (index1 === 6) {
              width = 188;
            }
            if (index1 === 2 || index1 === 3 || index1 === 4) {
              height: 274;
            }
            if (index1 === 5 || index1 === 6 || index1 === 7) {
              height: 280;
            }
            return (
              <View
                onClick={() => {
                  Router.goHome(item1);
                  //let params = item1.params && JSON.parse(item1.params);
                  //if (params && params.businessId && params.storeId && params.storeType) {
                  //Router.goStore(params.storeType, params.storeId, params.businessId);
                  //}
                }}
                key={item1.id}
              >
                <Img src={item1.image} width={width} height={height} />
              </View>
            );
          })}
      </Wrap>
    </View>
  );
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {};
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
