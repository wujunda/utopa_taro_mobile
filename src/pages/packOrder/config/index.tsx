import Taro, { useRouter } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
import { connect } from '@tarojs/redux';

import Txt from '../../../components/Txt';
import './index.scss';

import Qrcode from '../../../components/Qrcode';

interface IProps {}
const Index: Taro.FC<IProps> = ({}) => {
  let id = useRouter().params.id;
  return (
    <View className="utp-config">
      <View className="utp-config-box" style={{ width: '250px' }}>
        <View className="utp-config-box-codes utp-cnt" style={{ height: '250px' }}>
          <Qrcode value={id} size={225} />
        </View>
        <View className="utp-config-box-btn utp-cnt">
          <Txt title="未核销" size={32} color="white" />
        </View>
      </View>
      <View className="utp-config-tips">
        <Txt title="核销码用于到店提货时出示" size={26} color="white" height={36} />
        <Txt title="以便商家进行订单核销" size={26} color="white" height={36} />
      </View>
    </View>
  );
};
Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '核销订单',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#323951'
  // 不显示标题
  //navigationStyle: 'custom'
};
// @ts-ignore
export default connect((state) => state)(Index);
