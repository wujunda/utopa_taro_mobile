import Taro from '@tarojs/taro';
import { View, Image } from '@tarojs/components';

interface Props {
  src: any;
  width?: number | string;
  height?: number | string;
  mode?: 'scaleToFill' | 'aspectFit' | 'aspectFill' | 'aspectFill';
  round?: boolean;
  onClick?: () => void;
}

const getWidthOrHeight = (value: number | string) => {
  if (value === 1000) return '100%';
  if (typeof value === 'string') return value;
  // console.log(value,'--value')
  return Taro.pxTransform(value);
};

const Index: Taro.FC<Props> = ({
  src,
  width = 40,
  height = width,
  mode = 'scaleToFill',
  round = false,
  onClick
}) => {
  return (
    <View
      style={{
        width: getWidthOrHeight(width),
        height: getWidthOrHeight(height),
        position: 'relative'
      }}
      onClick={onClick}
    >
      <Image
        mode={mode}
        src={src}
        style={{
          width: '100%',
          height: '100%',
          display: 'block',
          borderRadius: round ? '50%' : 0
        }}
      ></Image>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
