import request from '../utils/request';
export interface ICate {
  code: number;
  data: {
    list: any[];
  };
}
export const getUseCate = (params) => {
  return request.post<any>('/app.do', {
    method: 'eshop.businessCategory.getFirstCategoryList',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      clientType: '1',
      sysId: '1'
    })
  });
};
export const getUseStore = (params) => {
  console.log(params,'--params')
  return request.post<any>('/app.do', {
    method: 'neweshop.newStore.getStoreListBycolumnNo',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      columnNo: '12',
      clientType: '1',
      sysId: '1'
    })
  });
};
