import Api from './api';
import { getGoodsList } from '../services/goods';

export default {
  namespace: 'example',

  // 数据
  state: {
    name: 'dengyin',
    goodsList: []
  },

  // 订阅
  subscriptions: {},

  // 异步
  effects: {
    *wait(_, { call, put }) {
      // console.log('我是example的wait', call);
      // console.log('我是example的wait', 44);
      // console.log('我是example的wait', Api);
      let str = yield call(Api.api, { name: '我是异步数据' });
      //console.log(str);

      yield put({
        type: 'save',
        payload: {
          name: str
        }
      });
      // console.log('end');
    },

    *getGoods(_, { call, put }) {
      // console.log('我是example的getGoods', call);
      // console.log('我是example的getGoods', 44);
      // console.log('我是example的getGoods', getGoodsList);

      let List = yield call(getGoodsList, { act: 'index' });
      console.log('我是example的getGoods', List);

      if (List.code === 200) {
        yield put({
          type: 'goods',
          payload: {
            goodsList: List.datas
          }
        });
        // console.log('end');
      }
    }
  },

  // 同步
  reducers: {
    save(state, action) {
      // console.log(333);
      // console.log(action);
      return { ...state, ...action.payload };
    },
    goods(state, action) {
      // console.log('goods');
      // console.log(action);
      return { ...state, goodsList: action.payload.goodsList };
    }
  }
};
