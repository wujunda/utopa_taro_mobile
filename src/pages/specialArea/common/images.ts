// 专区 拼团头部背景
import topBg from '../../../assets/special/top_bg.png';
import collage_list_1 from '../../../assets/special/collage_list_1.png';
import collage_list_2 from '../../../assets/special/collage_list_2.png';
import collage_list_3 from '../../../assets/special/collage_list_3.png';
import collage_list_4 from '../../../assets/special/collage_list_4.png';
import today_text from '../../../assets/special/today_text.png';
import more_right from '../../../assets/special/right.png';
import bar_back_arrow from '../../../assets/order/back.png';
import second_top_bg from '../../../assets/special/second_top_bg.png';
import second_buy_now from '../../../assets/special/second_buy_now.png';
import second_buy_yellow from '../../../assets/special/second_buy_yellow.png';
import second_buy_grey from '../../../assets/special/second_buy_grey.png';
import add_card from '../../../assets/cart/add1.png';
import navigation_bar_icon_back from '../../../assets/special/navigation_bar_icon_back.png';
import navigation_bar_icon_back_b from '../../../assets/special/navigation_bar_icon_back_b.png';
import right_more_666 from '../../../assets/special/right_more_666@2x.png';
import back_white from '../../../assets/special/back_white.png';
import all_right from '../../../assets/special/all_right.png';


// eslint-disable-next-line import/prefer-default-export
export const SPECIAL_IMAGES = {
  collage_top_bg: topBg,
  collage_list_1: collage_list_1,
  collage_list_2: collage_list_2,
  collage_list_3: collage_list_3,
  collage_list_4: collage_list_4,
  today_text: today_text,
  more_right: more_right,
  second_top_bg: second_top_bg,
  second_buy_now: second_buy_now,
  second_buy_yellow: second_buy_yellow,
  second_buy_grey: second_buy_grey
};

export const COMMON_IMAGES = {
  app_bar_back_arrow: bar_back_arrow,
  add_card: add_card,
  navigation_bar_icon_back: navigation_bar_icon_back,
  navigation_bar_icon_back_b: navigation_bar_icon_back_b,
  right_more_666: right_more_666,
  back_white: back_white,
  all_right: all_right
};
