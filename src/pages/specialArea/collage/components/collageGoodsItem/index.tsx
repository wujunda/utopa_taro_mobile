/* eslint-disable import/first */
/* eslint-disable no-shadow */
import Taro, { useRouter } from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import { COMMON_IMAGES } from '../../../common/images';
import Wrap from '@/components/Wrap';
// eslint-disable-next-line import/first
import './index.scss';
import Img from '@/components/Img';
import { PriceFormat } from '@/utils/utils';

//商品项
const ViewCollageGoodsItem = (props) => {
  const { item, onGoCollage } = props;
  return (
    item && (
      <View onClick={() => onGoCollage && onGoCollage()}>
        <View className="collage-goods-item">
          <Wrap type={1} flexDirection="row" top>
            <View className="item-image">
              <Img
                src={item.productImage}
                height={Taro.pxTransform(280)}
                width={Taro.pxTransform(280)}
              ></Img>
            </View>
            <View className="item-content">
              <Text className="item-name">{item.productName}</Text>
              <View className="spec" style={{ minHeight: Taro.pxTransform(60) }}>
                {item.tags &&
                  item.tags.map((item) => {
                    // eslint-disable-next-line react/jsx-key
                    return <View className="spec-tag">{item.tagText}</View>;
                  })}
              </View>
              <View
                className="item-tag"
                style={{ color: '#999', display: 'inline-block', marginLeft: -5 }}
              >
                已拼
                <Text style={{ display: 'inline-block', color: '#FF2A48' }}>
                  {item.alreadyGroupCount || 0}
                </Text>
                件
              </View>
              <View className="item-btm">
                <View className="btm-item left">
                  <Text className="big-text">
                    <Text
                      style={{
                        display: 'inline-block',
                        fontSize: Taro.pxTransform(28),
                        paddingRight: Taro.pxTransform(3)
                      }}
                    >
                      ¥
                    </Text>
                    {PriceFormat(item.salePrice)}
                  </Text>
                  <Text className="small-text" style={{ color: '#919192' }}>
                    ¥{PriceFormat(item.originPrice)}
                  </Text>
                </View>
                <View className="btm-item right">
                  <Wrap type={1} flexDirection="row" top justifyContent="space-between">
                    <View>
                      <View className="words">去拼团</View>
                      <View className="sm-words">{item.activityUserCount}人团</View>
                    </View>
                    <View className="right-arrow">
                      <Image
                        src={COMMON_IMAGES.all_right}
                        style={{ height: Taro.pxTransform(24), width: Taro.pxTransform(24) }}
                      ></Image>
                    </View>
                  </Wrap>
                </View>
              </View>
            </View>
          </Wrap>
        </View>
      </View>
    )
  );
};
export default ViewCollageGoodsItem;
