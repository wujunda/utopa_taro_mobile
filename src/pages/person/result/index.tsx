import Taro, { useRouter, useDidShow } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
import { connect } from '@tarojs/redux';
// import useRequest from '../../../hooks/useRequest';
// import { IUse, getItem } from '../../../services/user';

import Img from '../../../components/Img';
import Txt from '../../../components/Txt';
import { images } from '../../../images';
import './index.scss';
import Router from '../../../utils/router';

function Index({}) {
  const router = useRouter();
  let id = router.params.type;
  useDidShow(() => {
    Taro.setNavigationBarTitle({
      title: id === '1' ? '设置已完成' : '设置失败'
    });
  });

  return (
    <View className="utp-result1">
      <View className="utp-cnt" style={{ marginTop: Taro.pxTransform(200) }}>
        <Img
          src={id === '1' ? images.set_reset_sus : images.set_reset_error}
          width={180}
          height={206}
        />
      </View>
      <View
        className="utp-cnt"
        style={{ marginTop: Taro.pxTransform(50), marginBottom: Taro.pxTransform(50) }}
      >
        <Txt
          title={
            id === '1'
              ? '新密码设置已成功，请妥善保管您的新密码'
              : '新密码设置未成功，不要气馁再试一次'
          }
          color={'low'}
        />
      </View>
      <View className="utp-cnt">
        <View
          className={'utp-result1-btn utp-cnt '}
          onClick={(e) => {
            e.stopPropagation;
            //getCode1();
            if (id === '1') {
              Router.goLogin();
            } else {
              Router.goBack();
            }
          }}
        >
          <Txt size={28} title={id === '1' ? '去登陆' : '再试一次'} color={'orange'} />
        </View>
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '设置新密码',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff'
};
// @ts-ignore
export default connect((state) => state)(Index);
