import Taro from '@tarojs/taro';
import { connect } from '@tarojs/redux';
import { View } from '@tarojs/components';

import Img from '../../../components/Img';
import Router from '../../../utils/router';

type PageStateProps = {
  arr: any[];
};

type PageDispatchProps = {};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

//let cishu: number = 0;
const Index: Taro.FC<IProps> = (props) => {
  return (
    <View>
      {props.arr &&
        props.arr.map((item, index) => {
          return (
            <View
              onClick={() => {
                Router.goHome(item);
              }}
              key={index}
              className="utp-cnt"
              style={{ background: '#f8f8f8', padding: Taro.pxTransform(20) }}
            >
              <Img src={item.image} width={710} height={266} />
            </View>
          );
        })}
    </View>
  );
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {};
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
