/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-key */
/* eslint-disable import/first */
import Taro, { useState, useEffect, useRouter, useDidShow } from '@tarojs/taro';
import { View, Text, Picker, Input } from '@tarojs/components';
import './index.scss';
import Wrap from '@/components/Wrap';
import { getExpressCompanys, saveExpressCompanys } from '../../../services/common';
import { handleRequest, Glo } from '@/utils/utils';
import { REFUND_DETAIL_STATUS, REFUND_TYPE } from '@/utils/common_enum';
import useSession from '@/hooks/useSession';
import useLogin from '@/hooks/useLogin';
import ViewPickReason from '../applyRefund/selectPick';
import Router from '@/utils/router';

const Index: Taro.FC<any> = (props: any) => {
  const [haveSession] = useSession();
  const router = useRouter();
  const [isLogin, updateLogin] = useLogin();
  const [logisList, setLogisList] = useState<any>([]);
  const [saveParams, setSaveParams] = useState<any>({
    expressCompanyNo: '',
    expressCompanyName: '',
    expressCompanyCode: ''
  });
  // eslint-disable-next-line no-shadow
  const BTN_STYLE = {
    background: '#eee'
  };
  useEffect(() => {
    Glo.loading('正在加载');
    setTimeout(() => {
      _getExpressCompanys();
    }, 1200);
  }, []);
  useDidShow(() => {});

  /***
   * 获取物流信息
   * ***/
  const _getExpressCompanys = async () => {
    let res = await getExpressCompanys();
    if (handleRequest(res as any)) {
      setLogisList(res.data);
    }
    Glo.hideLoading();
  };

  const _saveExpress = async (params: any) => {
    let data = { ...params, ...{ refundOrderNo: router.params.refundOrderNo } };
    Glo.loading('正在保存');
    try {
      let res = await saveExpressCompanys(data);
      if (handleRequest(res as any)) {
        Glo.showToast('保存成功');
        setTimeout(() => {
          Taro.navigateTo({
            url: `/pages/person/refundDetail/index?refundOrderNo=${router.params.refundOrderNo}`
          });
        }, 500);
        // setPopShow(false);
        // getDetail();
      }
    } catch (e) {
      console.log(e);
    } finally {
      Glo.hideLoading();
    }
  };

  //eslint-disable-next-line react/no-multi-comp
  return (
    <View>
      <View className="sel-pup">
        <View>
          <View style={{ backgroundColor: '#fff', padding: '25px 15px' }}>
            <Wrap type={1} flexWrap="nowrap">
              <Text className="text-words">快递单号</Text>
              <View className="text-words" style={{ color: '#000', marginLeft: 15 }}>
                <Input
                  onInput={(v) => {
                    setSaveParams((state) => ({
                      ...state,
                      expressCompanyNo: v.detail.value
                    }));
                  }}
                  placeholder="请填写物流单号"
                />
              </View>
            </Wrap>
          </View>
          <View className="text-words" style={{ backgroundColor: '#fff', padding: '15px 15px' }}>
            <ViewPickReason
              leftText="快递公司"
              binKey="name"
              data={logisList}
              onChange={(v) => {
                setSaveParams((state) => ({
                  ...state,
                  expressCompanyName: v.name,
                  expressCompanyCode: v.code
                }));
              }}
            />
          </View>
        </View>
        <View className="btn-box">
          <View
            style={
              saveParams.expressCompanyNo === '' || saveParams.expressCompanyName === ''
                ? BTN_STYLE
                : ''
            }
            className="pup-btn"
            onClick={() => {
              if (saveParams.expressCompanyNo != '' && saveParams.expressCompanyName !== '') {
                _saveExpress(saveParams);
              }
            }}
          >
            保存
          </View>
        </View>
      </View>
    </View>
  );
};

Index.config = {
  navigationBarTitleText: '填写物流',
  enablePullDownRefresh: true,
  disableScroll: false,
  navigationStyle: 'default'
};

export default Index;
// @ts-ignore
// export default Index;
