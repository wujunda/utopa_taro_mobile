import Taro, { useState } from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';
import Safe from '../../../components/safe';
import Products from '../../../components/AssembleGood/Good';
import Tab from '../../../components/Tab';
import Loading from '../../../components/Loading';
import LoadingNone from '../../../components/LoadingNone';
import useSession from '../../../hooks/useSession';
import useRequest from '../../../hooks/useRequest';
import useLogin from '../../../hooks/useLogin';
import { Iprops, findGroupDetailByUid } from '../../../services/assemble';
import './index.scss';

function Index(props) {
  const [i, setI] = useState(0);
  const [haveSession] = useSession();
  const [isLogin, updateLogin] = useLogin();
  if (haveSession && !isLogin) {
    if (updateLogin) {
      updateLogin();
    }
  }
  const [state, run, loading] = useRequest<Iprops>(i, findGroupDetailByUid);
  if (
    haveSession &&
    isLogin &&
    (state === null || (state != null && state.code !== 0)) &&
    run &&
    !loading
  ) {
    run();
  }
  return (
    <Safe>
      <View className="utp-assemlist">
        <Tab
          onChange={(inn) => {
            setI(inn);
            run && run();
          }}
          defaultActiveKey={i}
        >
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page"
            scrollY
          >
            {loading && <Loading />}
            {state &&
              state.data &&
              state.data.records &&
              state.data.records.length === 0 &&
              !loading && <LoadingNone />}
            {state &&
              state.data.records &&
              state.data.records.map((item, index) => {
                return (
                  <Products
                    data={item}
                    goStatus={() => {
                      Taro.navigateTo({
                        url: `/pages/assemble/result/index?groupId=${item.groupId}`
                      });
                    }}
                    goOrder={() => {
                      Taro.navigateTo({
                        url:`/pages/packOrder/order/index?orderId=${item.orderId}&orderNo=${item.orderNo}&serviceStatus=${item.orderStatus}` 
                      });
                    }}
                    key={index}
                  />
                );
              })}
          </ScrollView>
        </Tab>
      </View>
    </Safe>
  );
}
Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '我的拼团',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};
export default Index;
