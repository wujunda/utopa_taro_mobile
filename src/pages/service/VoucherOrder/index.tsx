import Taro, { useState, useRouter } from '@tarojs/taro';
import { View } from '@tarojs/components';

import Wrap from '../../../components/Wrap';
import Bton from '../../../components/Bton';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
//import Qrcode from '../../../components/Qrcode';
import Loading from '../../../components/Loading';
import Qrcode from '../../../components/Qrcode';
import LoadingNone from '../../../components/LoadingNone';
import { IVoucher, getInfo1 } from '../../../services/voucher';
import { images } from '../../../images';
import useRequest from '../../../hooks/useRequest';
import { Glo, parseTime, goPayPage, successUrl } from '../../../utils/utils';
import Time from './Time';
import Router from '../../../utils/router';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const id = useRouter().params.id;
  const [show, setShow] = useState(false);
  const [outing, setOuting] = useState(false);
  const [state, update, loading] = useRequest<IVoucher>(id, getInfo1);
  if (false) {
    console.log(update);
  }
  let t = [1];
  let second = 0;
  return (
    <View className="utp-voucherOrder">
      {state && state.data && state.data.statusCode === 0 && (
        <View className="utp-voucherOrder-time utp-cnt">
          <Wrap>
            {t.map((item) => {
              let time = state.data.remainTime;
              let end = Number((state.data.payTime / 1000).toFixed(0)) + time;
              let start = Number((new Date().getTime() / 1000).toFixed(0));
              console.log(start);
              console.log(end);
              if (start > end) {
                console.log('超时');
              } else {
                second = end - start;
              }
              return <Time sec={second} />;
            })}
          </Wrap>
        </View>
      )}
      {show && (
        <View
          className={`utp-voucherOrder-modal animate__fadeIn utp-cnt ${
            outing ? 'animate__fadeOut' : ''
          }`}
          onClick={() => {
            setOuting(true);
            setTimeout(() => {
              setShow(false);
              setOuting(false);
            }, 250);
          }}
        >
          <View
            className="utp-cnt"
            style={{ width: '200px', height: '200px', backgroundColor: '#fff' }}
          >
            <Qrcode size={180} value={state.data.couponInstances[0].couponCode} />
          </View>
        </View>
      )}
      {loading && !state && <Loading />}
      {!loading && !state && <LoadingNone />}
      {!!state && state.data && (
        <View>
          <View
            className="utp-voucherOrder-goods"
            onClick={() => {
              Taro.navigateTo({
                url:
                  '/pages/service/VoucherDetails/index?id=' + state.data.shopGoodsId + '&id1=' + id
              });
            }}
          >
            <Wrap type={1}>
              <View className="utp-voucherOrder-goods-imgs">
                <Img src={state.data.picUrl} width={140} />
              </View>
              <View className="utp-voucherOrder-goods-box">
                <View className="utp-voucherOrder-goods-box-title">
                  <Wrap>
                    {!!state && state.data && (
                      <Txt title={state.data.name} color="deep" size={30} />
                    )}
                  </Wrap>
                </View>
                <View className="utp-voucherOrder-goods-box-tips">
                  <Wrap>
                    {!!state && state.data && (
                      <Txt
                        title={`￥${Glo.formatPrice(state.data.goodsPrice)}`}
                        color="red"
                        size={28}
                      />
                    )}
                  </Wrap>
                </View>
              </View>
              <View className="utp-voucherOrder-goods-btn">
                <Img src={images.right} width={36} />
              </View>
            </Wrap>
          </View>
          {!!state.data.couponInstances && !!state.data.couponInstances.length && (
            <View className="utp-voucherOrder-code">
              <Wrap justifyContent="space-between">
                <Wrap>
                  <Txt
                    title={`券码：${state.data.couponInstances[0].couponCode}`}
                    color="deep"
                    height={44}
                    size={28}
                  />
                  <View
                    className="utp-voucherOrder-codes"
                    onClick={() => {
                      setShow(!show);
                    }}
                  >
                    <Qrcode size={14} value={state.data.couponInstances[0].couponCode} />
                  </View>
                </Wrap>
                <Txt title={state.data.statusDesc} color="red" size={28} />
              </Wrap>
            </View>
          )}
          {state.data.statusCode === 0 && (
            <View className="utp-voucherOrder-box">
              <Wrap>
                <Txt title="暂无详情" color="low" size={28} height={80} />
              </Wrap>
            </View>
          )}
          <View className="utp-voucherOrder-box">
            <View className="utp-voucherOrder-title">
              <Wrap>
                <Txt title="详情" color="low" size={28} height={80} />
              </Wrap>
            </View>
            <View className="utp-voucherOrder-box1">
              <Wrap>
                <Txt title={`订单编号：${state.data.orderNo}`} color="deep" size={28} height={58} />
              </Wrap>
              {!(!!state.data.couponInstances && !!state.data.couponInstances.length) && (
                <Wrap>
                  <Txt
                    title={`下单时间：${parseTime(state.data.placeOrderTime)}`}
                    color="deep"
                    size={28}
                    height={58}
                  />
                </Wrap>
              )}

              {!!state.data.couponInstances && !!state.data.couponInstances.length && (
                <Wrap>
                  <Txt title="付款方式：支付" color="deep" size={28} height={58} />
                </Wrap>
              )}
              {!!state.data.couponInstances && !!state.data.couponInstances.length && (
                <Wrap>
                  <Txt
                    title={`领取时间：${parseTime(state.data.placeOrderTime)}`}
                    color="deep"
                    size={28}
                    height={58}
                  />
                </Wrap>
              )}
              <Wrap>
                <Txt title={`数量：${state.data.goodsNum}`} color="deep" size={28} height={58} />
              </Wrap>
              <Wrap>
                <Txt
                  title={`总价：￥${Glo.formatPrice(state.data.goodsPrice)}`}
                  color="deep"
                  size={28}
                  height={58}
                />
              </Wrap>
              {!!state.data.couponInstances && !!state.data.couponInstances.length && (
                <Wrap>
                  <Txt
                    title={`实付：￥${Glo.formatPrice(state.data.orderAmount)}`}
                    color="deep"
                    size={28}
                    height={58}
                  />
                </Wrap>
              )}
            </View>
          </View>
          {state.data.statusCode !== 0 && (
            <View className="utp-voucherOrder-box">
              <Wrap>
                <Txt title="暂无详情" color="low" size={28} height={80} />
              </Wrap>
            </View>
          )}
          <View className="utp-voucherOrder-box">
            <View className="utp-voucherOrder-title">
              <Wrap>
                <Txt title="使用须知" color="low" size={28} height={80} />
              </Wrap>
            </View>
            <View className="utp-voucherOrder-box1 utp-voucherOrder-box1-1">
              <Wrap>
                <Txt title="有效期" color="deep" size={28} height={58} />
              </Wrap>
              <View className="utp-voucherOrder-tips">
                <Wrap>
                  <Txt
                    title={`${parseTime(state.data.validStartTime)} 至 ${parseTime(
                      state.data.validEndTime
                    )}`}
                    color="low"
                    size={28}
                    height={58}
                  />
                </Wrap>
              </View>
              <Wrap>
                <Txt title="使用时间" color="deep" size={28} height={58} />
              </Wrap>
              <View className="utp-voucherOrder-tips">
                <Wrap>
                  <Txt
                    title={`${state.data.useStartTime ? parseTime(state.data.useStartTime) : '-'}`}
                    color="low"
                    size={28}
                    height={58}
                  />
                </Wrap>
              </View>
              <Wrap>
                <Txt title="使用说明" color="deep" size={28} height={58} />
              </Wrap>
            </View>
          </View>

          {state.data.statusCode === 0 && (
            <View className="utp-voucherOrder-ft1">
              <Wrap type={1} justifyContent="space-between">
                <Txt title={`¥${Glo.formatPrice(state.data.totalAmount)}`} size={30} color="red" />
                <View style={{ width: '90px' }}>
                  {second > 0 && (
                    <View
                      onClick={() => {
                        if (process.env.TARO_ENV !== 'weapp') {
                          goPayPage({
                            sysId: 1001,
                            payOrderNo: state.data.payOrderNo,
                            paySucceesHref: successUrl(),
                            payFailHref: successUrl()
                          });
                        } else {
                          Router.goPay(
                            state.data.orderId,
                            Glo.formatPrice(state.data.totalAmount),
                            state.data.orderNo,
                            state.data.payOrderNo
                          );
                        }
                      }}
                    >
                      <Bton size="mini">立即付款</Bton>
                    </View>
                  )}
                  {second == 0 && (
                    <Bton size="mini" type="none">
                      立即付款
                    </Bton>
                  )}
                </View>
              </Wrap>
            </View>
          )}
          {state.data.statusCode === 1 && (
            <View className="utp-voucherOrder-ft">
              <Bton>申请退款</Bton>
            </View>
          )}
        </View>
      )}
    </View>
  );
};

Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '订单详情',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};
export default Index;
