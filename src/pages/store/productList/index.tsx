import { View } from '@tarojs/components';
import Taro, { useReachBottom,useRouter } from '@tarojs/taro';
import Goods2s from '../../../components/Goods2s';
import Goods1 from '../../../components/Goods1';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import Safe from '../../../components/safe';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import useSession from '../../../hooks/useSession';
import useLogin from '../../../hooks/useLogin';
import { IGoods } from '../../../interface/home';
import useResetCount from '../../../hooks/useResetCount';
import {
  getProductsByBusinessId,
  getsig
} from '../../../services/store';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const router = useRouter();
  const [haveSession] = useSession();
  const [isLogin, updateLogin] = useLogin();
  if (haveSession && !isLogin) {
    if (updateLogin) {
      updateLogin();
    }
  }
  // 购物车数据
  // @ts-ignore
  let payload= {
    businessId: router.params.businessId,
    storeId: router.params.storeId,
    categoryId: router.params.categoryId || '',
    activityId: router.params.activityId || '',
    activeType:router.params.activeType || '',
  };
  let method = payload.activityId? getsig : getProductsByBusinessId;
  const [state, hasMore, update, getMoreData, loading] = useRequestWIthMore(
    payload,
    method,
  );
  const [can, update1] = useResetCount();
  if (
    haveSession &&
    isLogin &&
    (state === null || (state != null && state.length == 0)) &&
    update &&
    !loading
  ) {
    if (can) {
      update({});
      update1();
    }
  }
  var goods: IGoods[] = [];
  if (state && state.length > 0) {
    if(router.params.categoryId){
      state.map((item) => {
        item.data.records.map((it) => {
          goods.push(it);
        });
      });
    }else{
      state.map((item) => {
        item.data.page.records.map((it) => {
          goods.push(it);
        });
      });
    }
  }


  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <View className="utp-products">
            {loading && goods.length === 0 && <Loading />}
            {!loading && goods.length === 0 && <LoadingNone />}
            {
              goods ? <Goods2s data={ goods as IGoods[]} /> : null  
            }
            {loading && goods.length !== 0 && <LoadingMore />}
            {!loading && !hasMore && goods.length > 0 && <LoadingNoneMore />}
          </View>
        </View>
      </View>
    </Safe>
  );
};

Index.config = {
  navigationBarTitleText: '浏览历史',
  disableScroll: false
  //navigationStyle: 'custom',
  //usingComponents: {
  //Bar: '../../../components/bar' // 书写第三方组件的相对路径
  //}
};
Index.defaultProps = {};
export default Index;
