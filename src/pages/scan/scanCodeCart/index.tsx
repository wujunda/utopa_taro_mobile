import Taro, { useState, useDidShow } from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import Safe from '../../../components/safe';
import Box from '../../../components/Box';
import Txt from '../../../components/Txt';
import Wrap from '../../../components/Wrap';
import Goods from '../../../components/Goods';
import Img from '../../../components/Img';
import Bar from '../../../components/bar';
import Bton from '../../../components/Bton';
import Cnt from '../../../components/Cnt';
import Tag from '../../../components/Tag';
import Card from '../../../components/Card';
import Model from '../../../components/Model';
import right from './../../../assets/cart/right.png';
import check from '../../../assets/cart/check.png';
import uncheck from '../../../assets/submit/uncheck.png';
import del from './../../../assets/cart/del.png';
import scan from './../../../assets/scan.png';
import Yuan from '../../../components/Yuan';
import { getDelCart, ShopCartDtos } from '../../../services/cart';
import { Glo } from '../../../utils/utils';

//import useRequest from '../../../hooks/useRequest';

import './index.scss';

type PageStateProps = {
  loading: boolean;
  cart: any;
  session: any;
  isLogin: boolean;
  statusHeight: number;
  closeWidth: number;
};

type PageDispatchProps = {
  syncCart1: () => void;
  getSubmit: (any) => void;
  checkStore: (any) => void;
  checkCart: (any) => void;
  savePost: (any) => void;
  getCategoryList: () => void;
  saveSession: (any) => void;
  submitBefore: () => void;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
const Index: Taro.FC<IProps> = (props) => {
  useDidShow(() => {
    props.syncCart1();
  });
  const [show, setShow] = useState(false);
  const [card, setCard] = useState([
    { status: 0, id: 12 },
    { status: 0, id: 67 },
    { status: 0, id: 98 }
  ]);
  const [hasCou, setCou] = useState(false);

  let cartList: ShopCartDtos[] = props.cart.cartList1;
  // 扫码购提交订单
  const onSubmit1 = async (item: ShopCartDtos, isScan) => {
    let arr = [item];
    props.submitBefore();
    // console.log(Router);
    let goods: any = {};
    let goodsArr: any = [];
    arr.map((item) => {
      if (item.businessId) {
        let obj: any = {};
        obj.businessId = item.businessId;
        obj.isUseOriginPrice = 0;
        obj.storeId = item.storeId;
        obj.sysId = 1004;
        item.productDetails.map((item1) => {
          if (item1.isSelect === 1) {
            let obj: any = {};
            obj.skuId = item1.productId;
            obj.skuNum = item1.number;
            goodsArr.push(obj);
          }
        });
        if (goodsArr.length > 0) {
          obj.products = goodsArr;
          //obj.receiptAddress = {};
          goods = Object.assign({}, obj);
        }
      }
    });
    let post: any;
    if (goodsArr.length > 0) {
      post = Object.assign(
        {
          clientType: 1,
          sysId: 1
        },
        { binComits: [goods] }
      );
      Taro.showLoading({
        title: ''
      });
      props.getSubmit({ post, type: 1, isScan });
      props.savePost({
        data: post
      });
    } else {
      // 没有选中商品
      Taro.showToast({
        title: '请选择商品',
        icon: 'none'
      });
    }
  };

  // 提交订单
  const onSubmit = async (item: ShopCartDtos, isScan) => {
    let arr = [item];
    props.submitBefore();
    // console.log(Router);
    let goods: any = [];
    arr.map((item) => {
      if (item.businessId) {
        let obj: any = {};
        obj.businessId = item.businessId;
        obj.isUseOriginPrice = 0;
        obj.storeId = item.storeId;
        obj.sysId = 1004;
        let goodsArr: any = [];
        item.productDetails.map((item1) => {
          if (item1.isSelect === 1) {
            let obj: any = {};
            obj.skuId = item1.productId;
            obj.skuNum = item1.number;
            goodsArr.push(obj);
          }
        });
        if (goodsArr.length > 0) {
          obj.products = goodsArr;
          //obj.receiptAddress = {};
          goods.push(obj);
        }
      }
    });
    let post: any;
    if (goods.length > 0) {
      post = {
        binComits: goods,
        clientType: 1,
        sysId: 1
      };
      Taro.showLoading({
        title: ''
      });
      props.getSubmit({ post, type: 1, isScan });
      props.savePost({
        data: post
      });
    } else {
      // 没有选中商品
      Taro.showToast({
        title: '请选择商品',
        icon: 'none'
      });
    }
  };
  if (props.loading) {
    Taro.showLoading({
      title: ''
    });
  } else {
    Taro.hideLoading();
  }
  const onDel = () => {
    let ids: any = [];
    cartList.map((item) => {
      item.productDetails.map((item1) => {
        if (item1.isSelect == 1) {
          ids.push(item1.id);
        }
      });
    });
    if (ids.length === 0) {
      Taro.showToast({
        title: '请先选择商品',
        icon: 'none'
      });
      return;
    }
    Taro.showModal({
      title: '提示',
      content: '确定删除所选商品吗？',
      success: async (res) => {
        if (res.confirm) {
          Taro.showLoading({
            title: ''
          });
          let rst = await getDelCart({
            ids: ids.join(',')
          });
          Taro.hideLoading();
          if (rst.code === 0) {
            props.syncCart1();
          }
        } else if (res.cancel) {
        }
      }
    });
  };

  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body utp-scan">
          <View
            className="utp-scan-hd"
            style={{ paddingTop: Taro.pxTransform(props.statusHeight * 2) }}
          >
            <Bar goBack title="扫码购" rightIcon={del} onClick={onDel} />
          </View>
          <View
            className="utp-scan-bd"
            style={{
              marginTop: Taro.pxTransform(props.statusHeight * 2 + 88),
              marginBottom: Taro.pxTransform(170)
            }}
          >
            <View style={{ width: Taro.pxTransform(750) }}></View>
            {cartList.map((item, ins) => {
              let isCheck = true;
              let goods: number[] = [];
              item.productDetails.map((item1) => {
                goods.push(item1.id);
                if (item1.isSelect === 0) {
                  isCheck = false;
                }
              });
              if (item.category === 'uneffectProduct') {
                return;
              }
              return (
                <Box key={ins}>
                  <View className="utp-scan-box">
                    <Wrap Myheight={100}>
                      <View className="utp-scan-imgs">
                        <View
                          className="utp-scan-title"
                          onClick={() => {
                            props.checkStore({
                              storeId: item.storeId,
                              isSelect: isCheck ? 0 : 1,
                              ids: goods
                            });
                          }}
                        >
                          <Img src={isCheck ? check : uncheck} width={36} />
                        </View>
                        <Txt title={item.category} color="deep" size={30} bold />
                      </View>
                      <View
                        className="utp-scan-getDiscount"
                        onClick={() => {
                          if (item.haveCoupon) {
                            setCou(true);
                          }
                          setShow(true);
                        }}
                      >
                        <Cnt>{false && <Txt title="领券" size={24} color="red" />}</Cnt>
                      </View>
                    </Wrap>
                    {false && (
                      <Wrap>
                        <Wrap type={2} Myheight={80}>
                          <Tag margin={10}>促销</Tag>
                          <Txt title="   满1000送赠品" size={26} color="deep" />
                        </Wrap>
                        <Wrap type={2}>
                          <Txt title="去凑单 " size={26} color="blue" />
                          <Img src={right} width={20} />
                        </Wrap>
                      </Wrap>
                    )}
                    {item.productDetails.map((item1, index) => {
                      return <Goods isScan data={item1} key={index} />;
                    })}
                    <Wrap Myheight={160} justifyContent="space-between">
                      <Wrap type={2}>
                        <Txt title="合计：" size={34} color="deep" />
                        <View className="utp-scan-yuan">
                          <Yuan price={item.totalAmount} size={34} color="#FF2F7B" />
                        </View>
                      </Wrap>
                      <Wrap>
                        <View
                          className="utp-scan-btn"
                          onClick={() => {
                            onSubmit(item, false);
                          }}
                        >
                          <Bton type={1} size="mini">
                            直邮配送
                          </Bton>
                        </View>
                        <View
                          className="utp-scan-btn"
                          onClick={() => {
                            onSubmit1(item, true);
                          }}
                        >
                          <Bton type={7} size="mini">
                            去结算
                          </Bton>
                        </View>
                      </Wrap>
                    </Wrap>
                  </View>
                </Box>
              );
            })}
          </View>
          <Model
            show={show}
            title="优惠"
            onClose={() => {
              setShow(false);
            }}
          >
            <View style={{ padding: 10, display: hasCou ? 'block' : 'none' }}>
              <Txt title="领券" size={28} color="deep" justifyContent="flex-start" />
              {card.map((tt, nn) => {
                return (
                  <Card
                    key={nn}
                    Title={tt.status == 0 ? '领取' : '去使用'}
                    choose={() => {
                      if (tt.status === 0) {
                        Taro.showToast({
                          title: '领取成功',
                          success: () => {
                            card[nn].status = 1;
                            setCard(card);
                          }
                        });
                      } else {
                        //去到活动页使用
                      }
                    }}
                  />
                );
              })}
            </View>
            <View style={{ padding: 10 }}>
              <Txt title="暂无优惠券" size={28} color="deep" justifyContent="flex-start" />
            </View>
          </Model>
          {false && (
            <View
              className="utp-scan-tip"
              onClick={() => {
                if (process.env.TARO_ENV === 'weapp') {
                  Taro.navigateTo({ url: '/pages/scan/scanCode/index' });
                }
              }}
            >
              <Wrap type={1}>
                <Img src={scan} width={48} />
                <Txt color="white" title="继续扫码" size={30} />
              </Wrap>
            </View>
          )}
        </View>

        <View
          className="utp-scan-ft"
          onClick={() => {
            console.log('saom');
            Glo.scan(1);
          }}
        >
          <Bton>继续扫码添加商品</Bton>
        </View>
      </View>
    </Safe>
  );
};
Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '填写订单',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff',
  // 不显示标题
  navigationStyle: 'custom'
};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight,
    closeWidth: state.cart.closeWidth,
    post: state.cart.post,
    cart: state.cart,
    session: state.cart.session,
    isLogin: state.cart.isLogin,
    loading:
      state.loading.effects['cart/changeGoods'] ||
      state.loading.effects['cart/changeGoodsSku'] ||
      state.loading.effects['cart/syncCart1'] ||
      state.loading.effects['cart/checkGoods'] ||
      state.loading.effects['cart/checkStore'] ||
      state.loading.effects['cart/checkCart'] ||
      state.loading.effects['cart/getSubmit']
    //state.loading.effects['cart/getCategoryList']
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    getSubmit: (payload) => {
      dispatch({
        type: 'cart/getSubmit',
        payload
      });
    },
    syncCart1: () => {
      dispatch({
        type: 'cart/syncCart1'
      });
    },
    checkStore: (payload) => {
      console.warn('切换店铺1');
      dispatch({
        type: 'cart/checkStore',
        payload,
        isScan: true
      });
    },
    checkCart: (payload) => {
      dispatch({
        type: 'cart/checkCart',
        payload
      });
    },
    savePost: (payload) => {
      dispatch({
        type: 'cart/savePost',
        payload
      });
    },
    saveSession: (payload) => {
      dispatch({
        type: 'cart/saveSession',
        payload
      });
    },
    submitBefore: () => {
      dispatch({
        type: 'cart/submitBefore'
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
