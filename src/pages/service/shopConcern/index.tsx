import Taro from '@tarojs/taro';
import { View, ScrollView, Text } from '@tarojs/components';
import useLogin from '../../../hooks/useLogin';
import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Img from '../../../components/Img';
import Cnt from '../../../components/Cnt';
// import Txt from '../../../components/Txt';
// import Tag from '../../../components/Tag';
// import song from '../../../assets/shoppingStore/store-tag1.png';
// import quan from '../../../assets/shoppingStore/store-tag4.png';
// import saoma from '../../../assets/shoppingStore/store-tag3@3x.png';
import useRequest from '../../../hooks/useRequest';
import useSession from '../../../hooks/useSession';
import { getFavStoreList, unfavorite } from '../../../services/store';
import Score from '../../../components/Score';
import Router from '../../../utils/router';
import Loading from '../../../components/Loading';
import LoadingNone from '../../../components/LoadingNone';
import store_tag2 from '../../../assets/shoppingStore/store-tag2.png';
import store_tag1 from '../../../assets/shoppingStore/store-tag1.png';
import store_tag3 from '../../../assets/shoppingStore/store-tag3@2x.png';
import store_tag4 from '../../../assets/shoppingStore/store-tag4.png';
import './index.scss';

interface IProps {}
const Index: Taro.FC<IProps> = ({}) => {
  const [haveSession] = useSession();
  const [isLogin, updateLogin] = useLogin();
  if (haveSession && !isLogin) {
    if (updateLogin) {
      updateLogin();
    }
  }
  console.warn('是否登陆');
  console.warn(isLogin);
  // 购物车数据
  const [state, update, loading] = useRequest('', getFavStoreList);
  if (haveSession && isLogin && state === null && update && !loading) {
    update();
  }
  const dounfavorite = async (payload) => {
    // console.log(payload);
    let res = await unfavorite(payload);
    if (res.code === 0) {
      update && update();
    }
  };
  let list: any[] = [];
  if (state && state.data && state.data.records) {
    list = state.data.records;
  }
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-shopConcern"
            scrollY
          >
            {loading && list.length === 0 && <Loading />}
            {!loading && list.length === 0 && <LoadingNone />}
            {state &&
              state.data.records &&
              state.data.records.map((i, io) => {
                return (
                  <View
                    className="utp-shopConcern-businessInfo"
                    key={io}
                    onClick={() => {
                      Router.goStore(i.storeTemplateCode, i.storeId, i.businessId);
                    }}
                  >
                    <Wrap type={4} top>
                      <Wrap className="utp-shopConcern-businessInfo-imgbox">
                        <Img src={i.logoUrl} width={120} round></Img>
                      </Wrap>
                      <View className="utp-shopConcern-businessInfo-textBox">
                        <Wrap flexDirection="column" top justifyContent="flex-start">
                          <View className="utp-shopConcern-businessInfo-text">
                            <Text className="utp-shopConcern-businessInfo-text1">{i.name}</Text>
                            <Score num={i.star} />
                            <View className="utp-shopConcern-businessInfo-text2box">
                              <Wrap>
                                <Text className="utp-shopConcern-businessInfo-text2">
                                  {i.storeMainCategory}
                                </Text>
                                {i.logisticsModesList.map((o, oo) => {
                                  return (
                                    <View
                                      key={oo}
                                      className="utp-shopConcern-businessInfo-labelsbox"
                                    >
                                      <View className="utp-shopConcern-businessInfo-before"></View>
                                      <Cnt>
                                        <Img
                                          src={
                                            o === '即时达' || o === '周边送'
                                              ? store_tag1
                                              : o === '扫码自取'
                                              ? store_tag3
                                              : o === '门店自提'
                                              ? store_tag4
                                              : o === '全国送'
                                              ? store_tag2
                                              : store_tag4
                                          }
                                          width={24}
                                        />
                                        <Text className="utp-shopConcern-businessInfo-content">
                                          {o}
                                        </Text>
                                      </Cnt>
                                    </View>
                                  );
                                })}
                              </Wrap>
                            </View>
                            <Wrap>
                              {i.labels &&
                                i.labels.length > 0 &&
                                i.labels.map((r, rr) => {
                                  return (
                                    <View className="utp-shopConcern-businessInfo-text3" key={rr}>
                                      <Text>{r.labelName}</Text>
                                    </View>
                                  );
                                })}
                            </Wrap>
                            {i.couponBatchDtoDataName && (
                              <View className="utp-shopConcern-businessInfo-text4Box">
                                {i.couponBatchDtoDataName.map((u, uu) => {
                                  return (
                                    <Text
                                      className="utp-shopConcern-businessInfo-text4 utp-shopConcern-flex"
                                      key={uu}
                                    >
                                      {u}
                                    </Text>
                                  );
                                })}
                              </View>
                            )}
                          </View>
                        </Wrap>
                      </View>
                      {/* <View className="utp-shopConcern-businessInfo-Icon">
                        <Img src={shop_move_icon} width={28}></Img>
                      </View> */}
                    </Wrap>
                    <View
                      className="utp-shopConcern-businessInfo-status"
                      onClick={(e) => {
                        //设置取消关注
                        e.stopPropagation();
                        // setPa({
                        //   favoriteId:i.storeId,
                        //   storeId:i.storeId,
                        // })
                        dounfavorite({ favoriteId: i.storeId, storeId: i.storeId });
                      }}
                    >
                      已关注
                    </View>
                  </View>
                );
              })}
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};

Index.config = {
  navigationBarTitleText: '关注店铺',
  disableScroll: false
  // enablePullDownRefresh: true,
  // backgroundColor: '#f5f5f5'
};

Index.defaultProps = {};
export default Index;
