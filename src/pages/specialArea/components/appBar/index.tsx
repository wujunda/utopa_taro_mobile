/* eslint-disable react/jsx-key */
/* eslint-disable no-unused-vars */

import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import Img from '@/components/Img';
import Wrap from '@/components/Wrap';
import { COMMON_IMAGES } from '../../common/images';
import './index.scss';
// eslint-disable-next-line import/first
// import { useSelector } from '@tarojs/redux';

interface Props {
  style?: any;
  rightBox?: any;
  rightIcon?: any;
  rightText?: any;
  title?: any;
  titleStyle?: any;
  titleBox?: any;
  background?: any;
  rightIconClick?: (e?: any) => void;
  isApp?: boolean;
}

export const AppBar: Taro.FC<Props> = ({
  title,
  titleStyle,
  rightBox,
  background = 'none',
  rightIconClick,
  titleBox,
  style,
  isApp = true
}) => {
  return (
    <View>
      <View
        className="app-bar"
        style={{ ...style, overflow: 'hidden', height: isApp ? Taro.pxTransform(176) : null }}
      >
        <View style={{ paddingTop: isApp ? Taro.pxTransform(70) : '' }}>
          <Wrap type={1} flexDirection="row" top justifyContent="space-between">
            <View
              className="app-bar-back"
              onClick={() => {
                Taro.navigateBack();
              }}
            >
              <Img src={COMMON_IMAGES.app_bar_back_arrow} height={48} width={48}></Img>
            </View>
            <View className="app-bar-title" style={titleStyle || ''}>
              {titleBox && <View>{titleBox}</View>}
            </View>

            <View className="app-bar-right" onClick={() => rightIconClick && rightIconClick()}>
              {rightBox && <View>{rightBox}</View>}
            </View>
          </Wrap>
        </View>
      </View>
    </View>
  );
};
AppBar.defaultProps = {};
export default AppBar;
