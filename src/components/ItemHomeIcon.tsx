import Taro from '@tarojs/taro';
import BashIt from './BashIt';

type PageStateProps = {
  // counter: {
  //   num: number;
  // };
};

type PageDispatchProps = {
  // add: () => void;
  // dec: () => void;
  // asyncAdd: () => any;
};

type PageOwnProps = {
  src: any;
  tips: string;
  num?: number | string;
  tipsSize?: number;
  Width?: number;
  Height?: number;
  isCircular?: boolean;
  onClick?: () => void;
  tipColor?: string;
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = (props) => {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */

  return (
    <BashIt
      top={20}
      bottom={0}
      src={props.src}
      srcWidth={props.Width ? props.Width : 96}
      srcHeight={props.Width ? props.Height : 96}
      tips={props.tips}
      isCircular={props.isCircular ? true : false}
      tipsHeight={54}
      tipsColor={props.tipColor ? props.tipColor : '#fff'}
      tipsSize={props.tipsSize ? props.tipsSize : 22}
      num={props.num}
      onClick={props.onClick}
    ></BashIt>
  );
};

Index.defaultProps = {};
export default Index;
