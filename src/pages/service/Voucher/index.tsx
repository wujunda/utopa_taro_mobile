import Taro, { useState, useEffect } from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';

import Safe from '../../../components/safe';
import Card1 from '../../../components/Card1';
import Tab from '../../../components/Tab';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import { IQuan, IVoucher, getVoucher } from '../../../services/voucher';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const [query, setQuery] = useState(-2);
  useEffect(() => {
    if (update) {
      update({});
    }
  }, [query]);
  const [innd, setInnd] = useState(0);
  // @ts-ignore
  const [state, hasMore, update, getMoreData, loading] = useRequestWIthMore<IVoucher>(
    'name=a&age=1%%' + query,
    getVoucher
  );
  var lists: IQuan[] = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.data.orders.map((item) => {
        lists.push(item);
      });
    });
  }

  return (
    <Safe>
      <View className="utp-html">
        <View>
          <Tab
            barList={['全部', '待付款', '待使用', '退款/售后']}
            defaultActiveKey={innd}
            onChange={(i) => {
              if (loading) {
                return;
              }
              setInnd(i);
              let q;
              if (i === 0) {
                q = -2;
              }
              if (i === 1) {
                q = 0;
              }
              if (i === 2) {
                q = 1;
              }
              if (i === 3) {
                q = 3;
              }
              setQuery(q);
            }}
          >
            <ScrollView
              refresherEnabled
              refresherBackground="#333"
              className="utp-bd utp-bd-page utp-VoucherDetails"
              scrollY
            >
              {loading && lists.length === 0 && <Loading />}
              {!loading && lists.length === 0 && <LoadingNone />}
              {innd === 0 ? (
                <View>
                  {lists.map((item) => {
                    return <Card1 item={item} />;
                  })}
                </View>
              ) : null}
              {innd === 1 ? (
                <View>
                  {lists.map((item) => {
                    return <Card1 item={item} />;
                  })}
                </View>
              ) : null}
              {innd === 2 ? (
                <View>
                  {lists.map((item) => {
                    return <Card1 item={item} />;
                  })}
                </View>
              ) : null}

              {innd === 3 ? (
                <View>
                  {lists.map((item) => {
                    return <Card1 item={item} />;
                  })}
                </View>
              ) : null}

              {loading && lists.length !== 0 && <LoadingMore />}
              {!loading && !hasMore && lists.length > 0 && <LoadingNoneMore />}
            </ScrollView>
          </Tab>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '抵用券',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};
export default Index;
