/* eslint-disable react/no-multi-comp */
/* eslint-disable import/first */
/* eslint-disable no-shadow */
import Taro, { useState, useMemo, useDidShow, useRouter, useEffect } from '@tarojs/taro';
import { View, Text, WebView } from '@tarojs/components';
import Wrap from '@/components/Wrap';
import Img from '@/components/Img';
import SwiperCard from '../components/swiperCard/index';
import './index.scss';
// eslint-disable-next-line import/first
import { COMMON_IMAGES } from '../common/images';
import { getBaseInfo, getGroupThemeIndexProduct } from '../../../services/specialArea';
import { handleRequest, Glo } from '../../..//utils/utils';
import ListView from 'taro-listview';
import JSBridge from '@/utils/jsbridge/index';
import { connect } from '@tarojs/redux';
import Header from '../components/Header';
// import { shareAppMessage, commonNavigateTo } from '../../../hooks/useShareAppMessage';
import { goGoodsDetail, shareAppMessage, commonNavigateTo } from '../common/config';
import ViewCollageGoodsItem from './components/collageGoodsItem/index';
import ViewGoodsSwiperItem from './components/goodsSwiperItem';
import ViewCollageItem from './components/CollageItem';

interface Props {
  type?: any;
  title?: string;
  context?: any;
}
type PageStateProps = {
  user: {
    inApp: boolean;
    isWeiXinFalg: boolean;
  };
};

const CollagePage: Taro.FC<Props> = (props: any) => {
  const router = useRouter();
  const [topOpa, setTopOpa] = useState<any>(1);
  const [banners, setBanners] = useState<any>([]);
  const [basic, setBasic] = useState<any>({});
  // const [endTime, setEndTime] = useState<any>();
  const [shareData, setShareData] = useState<any>({});
  const [toDayData, setTodayData] = useState<any>({});
  const [cloumns, setCloumns] = useState<any>([]);
  const [moreState, setMoreState] = useState<any>({
    isLoaded: false,
    error: false,
    hasMore: false,
    isEmpty: false
  });
  const tips = { release: '加载中', activate: '下拉刷新', deactivate: '释放刷新' };
  const tempId = router.params.zoneId || '20200702117';

  useEffect(() => {
    Glo.loading('...');
  }, []);
  useDidShow(() => {
    // refList.fetchInit();
    pullDownRefresh();
  });

  // 拼团专区首页商品列表
  const _getGroupThemeIndexProduct = async (shopId = 11111) => {
    let params = {
      id: shopId
    };
    let res: any = await getGroupThemeIndexProduct(params);
    if (handleRequest(res)) {
      let data: any = res.data;
      // 今日数据默认第一条
      setTodayData(data.columnList[0]);
      setCloumns(data.columnList);
    }
  };
  const onScroll = (e) => {
    let size = e.detail.scrollTop;
    let opa = size / 100;
    // to do
    // 1 默认透明，2白底黑字
    if (size > 20) {
      setTopOpa(2);
    } else {
      setTopOpa(1);
    }
  };
  var refList: any = {};
  const pullDownRefresh = async () => {
    Glo.loading('正在加载...');
    try {
      const params = {
        id: tempId
      };
      // 基础主题
      let baseRes = await getBaseInfo(params);
      if (handleRequest(baseRes as any)) {
        setBanners(baseRes.data && baseRes.data.banners);
        setBasic(baseRes.data);
        if (props.user.inApp && process.env.TARO_ENV === 'h5') {
          setShareData({
            title: baseRes.data.shareTitle || '标题',
            imgUrl: baseRes.data.image,
            desc: baseRes.data.shareContent || '描述',
            link: baseRes.data.shareUrl || window.location.href
          });
        }

        if (!props.user.inApp && process.env.TARO_ENV === 'h5') {
          //分享
          shareAppMessage({
            title: baseRes.data.shareTitle || '标题',
            imgUrl: baseRes.data.image,
            desc: baseRes.data.shareContent || '描述',
            link: baseRes.data.shareUrl || window.location.href
          });
        }
        await _getGroupThemeIndexProduct(tempId as any);
        Glo.hideLoading();
        setMoreState((state) => ({
          ...state,
          isLoaded: true
        }));
      }
    } catch (e) {
      Glo.hideLoading();
      setMoreState((state) => ({
        ...state,
        isLoaded: true
      }));
    }
    // finally {
    //   Glo.hideLoading();
    //   setMoreState((state) => ({
    //     ...state,
    //     isLoaded: true
    //   }));
    // }
  };
  let insRef = (node) => {
    refList = node;
  };

  // check more 栏目通用 CMD
  const goJsBrigCollageCloumn = (item) => {
    const data = {
      type: '5', // 拼团类 //specialThemeBrandId: tempId,
      columnId: item.id.toString(),
      columnTitle: item.name.toString()
    };
    if (router.params.crm_channel_id) {
      data['crm_channel_id'] = router.params.crm_channel_id;
    }
    let cmdParams = {
      cmd: 'C0040010101',
      params: JSON.stringify(data)
    };
    JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, () => {
      // 查看栏目
      console.log('查看栏目');
    });
  };

  //  通用类
  const goJsBrigCollageItem = (cmdParams) => {
    JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, () => {
      console.log('执行', cmdParams);
    });
  };

  // 跳转类型
  const goJumType = (item) => {
    console.log(item);
    switch (item.jumpType) {
      case 1:
        window.location.href = item.url;
        break;
      case 2:
        if (props.user.inApp) {
          goJsBrigCollageItem(item.command);
        }
        break;
      case 3:
      default:
        break;
    }
  };

  return (
    // backgroundColor: (basic && basic.backgroundBottomColor) || ''
    <View
      className="skeleton lazy-view"
      style={{ backgroundColor: (basic && basic.backgroundBottomColor) || '' }}
    >
      {props.user.inApp && process.env.TARO_ENV == 'h5' && (
        <Header
          title={basic.name}
          isApp={props.user.inApp}
          topOpa={topOpa}
          hasShare
          color={basic.fontColor}
          rightClick={() => {
            if (props.user.inApp && process.env.TARO_ENV === 'h5') {
              shareAppMessage(shareData, 2);
            }
          }}
        />
      )}

      <ListView
        lazy
        indicator={tips}
        autoHeight
        ref={(node) => insRef(node)}
        isLoaded={moreState.isLoaded}
        isError={moreState.error}
        hasMore={moreState.hasMore}
        style={{ height: '100vh' }}
        isEmpty={moreState.isEmpty}
        onScroll={(e) => {
          onScroll(e);
        }}
        onPullDownRefresh={() => pullDownRefresh()}
        lazyStorage="lazyView"
      >
        <View>
          <View
            className="collage"
            style={{ backgroundColor: (basic && basic.backgroundBottomColor) || '' }}
          >
            <View
              className="collage-top"
              style={{ backgroundImage: 'url(' + basic.backgroundImage + ')' }}
            ></View>
            <View className="collage-pad">
              <View className="collage-card">
                <SwiperCard
                  height={280}
                  onClick={(e) => {
                    // 跳转类型 1:H5 2:原生 3:不跳转
                    switch (e.jumpType) {
                      case 1:
                        window.location.href = e.url;
                        break;
                      case 2:
                        if (props.user.inApp && process.env.TARO_ENV == 'h5') {
                          goJsBrigCollageItem(e.command);
                        }
                        break;
                      case 3:
                      default:
                        break;
                    }
                  }}
                  sourceData={banners}
                />
              </View>
              <View className="collage-items">
                {!!basic && !!basic.classes && (
                  <ViewCollageItem
                    onClick={(o) => {
                      goJumType(o);
                    }}
                    classes={basic.classes}
                  />
                )}
              </View>

              <ViewGoodsSwiperItem
                onMore={() => {
                  if (process.env.TARO_ENV == 'h5') {
                    if (props.user.inApp) {
                      goJsBrigCollageCloumn(!!toDayData && toDayData);
                    } else {
                      if (router.params.crm_channel_id) {
                        Taro.navigateTo({
                          url: `/pages/specialArea/columns/index?id=${toDayData.id}&crm_channel_id=${router.params.crm_channel_id}`
                        });
                      } else {
                        Taro.navigateTo({
                          url: `/pages/specialArea/columns/index?id=${toDayData.id}`
                        });
                      }
                    }
                  } else {
                    // 小程序跳转
                  }
                }}
                onClick={(item) => {
                  if (process.env.TARO_ENV === 'h5') {
                    if (props.user.inApp) {
                      let params = item.productCommand;
                      let params_str = JSON.parse(item.productCommand.params);
                      if (router.params.crm_channel_id) {
                        params_str['crm_channel_id'] = Number(router.params.crm_channel_id);
                      }
                      params.params = JSON.stringify(params_str);
                      goJsBrigCollageItem(params);
                    } else {
                      goGoodsDetail(item.id, item.storeId);
                    }
                  } else {
                    console.log('小程序');
                    Taro.navigateTo({
                      url: `/pages/details/index?id=${item.id}&storeId=${item.storeId}&businessId=${item.businessId}`
                    });
                  }
                }}
                title={!!toDayData && toDayData.name}
                list={!!toDayData && !!toDayData.groupProductList && toDayData.groupProductList}
              />
              <View className="collage-goods">
                {!!cloumns &&
                  cloumns.map((item: any, idx: number) => {
                    return (
                      idx > 0 && (
                        // eslint-disable-next-line react/jsx-key
                        <View key={item.id} style={{ marginTop: Taro.pxTransform(45) }}>
                          <View style={{ margin: Taro.pxTransform(20) }}>
                            <Wrap type={1} flexDirection="row" top justifyContent="space-between">
                              <View className="more-title">{item.name}</View>
                              <View
                                className="more-text"
                                onClick={() => {
                                  if (process.env.TARO_ENV === 'h5') {
                                    if (props.user.inApp) {
                                      goJsBrigCollageCloumn(item);
                                    } else {
                                      let go_url = `/pages/specialArea/columns/index?id=${item.id}`;
                                      commonNavigateTo(go_url);
                                    }
                                  } else {
                                    // Taro.navigateTo({
                                    //   url: `/pages/specialArea/columns/index?id=${item.id}`
                                    // });
                                  }
                                }}
                              >
                                <Wrap type={2}>
                                  <Text style={{ marginRight: Taro.pxTransform(5) }}>更多</Text>
                                  <Img
                                    src={COMMON_IMAGES.right_more_666}
                                    width={Taro.pxTransform(24)}
                                    height={Taro.pxTransform(24)}
                                  ></Img>
                                </Wrap>
                              </View>
                            </Wrap>
                          </View>
                          {!!item.groupProductList &&
                            item.groupProductList.map((item_a: any, idx_a: number) => (
                              // eslint-disable-next-line react/jsx-key
                              <View style={{ marginTop: Taro.pxTransform(10) }}>
                                <ViewCollageGoodsItem
                                  // 跳原生
                                  // 拼团
                                  onGoCollage={() => {
                                    if (process.env.TARO_ENV === 'h5') {
                                      if (props.user.inApp) {
                                        let params = item_a.productCommand;
                                        let params_str = JSON.parse(item_a.productCommand.params);
                                        if (router.params.crm_channel_id) {
                                          params_str['crm_channel_id'] = Number(
                                            router.params.crm_channel_id
                                          );
                                        }
                                        params.params = JSON.stringify(params_str);
                                        goJsBrigCollageItem(params);
                                      } else {
                                        goGoodsDetail(item_a.id, item_a.storeId);
                                      }
                                    } else {
                                      console.log('小程序');
                                      Taro.navigateTo({
                                        url: `/pages/details/index?id=${item.id}&storeId=${item.storeId}&businessId=${item.businessId}`
                                      });
                                    }
                                  }}
                                  item={item_a}
                                  key={item_a.id}
                                />
                              </View>
                            ))}
                        </View>
                      )
                    );
                  })}
              </View>
            </View>
          </View>
        </View>
        {!moreState.hasMore && (
          <View style={{ textAlign: 'center', color: '#999', fontSize: 12, marginTop: 30 }}>
            ---没有更多了---
          </View>
        )}
      </ListView>
    </View>
  );
};

CollagePage.config = {
  navigationBarTitleText: '拼团',
  disableScroll: true,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5',
  navigationStyle: 'default'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    }
  };
};
export default connect(mapStateToProps as any)(CollagePage as any);
