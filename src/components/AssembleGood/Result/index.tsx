// import { ComponentClass } from "react";
import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';

import './index.scss';
import Yuan from '../../Yuan';
import { IAssembel } from '../../../interface/home';

// #region 书写注意
//
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

/**
 * 指定config的类型声明为: Taro.Config
 *
 * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
 * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
 * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
 */
// 单个拼团商品 用于拼团详情loading状态 pages/assemble/result/index

interface IProps {
  data: IAssembel;
}
const Index: Taro.FC<IProps> = ({ data }) => {
  return (
    <View
      className="utp-flex utp-assemble utp-div"
      onClick={() => {
        Taro.navigateTo({
          url: '/pages/details/index'
        });
      }}
    >
      <View className="utp-assemble-i utp-cnt">
        <Image className="utp-assemble-img" src={(data && data.goodsLogo) || ''} />
      </View>
      <View className="utp-assemble-item">
        <View className="utp-assemble-titles">
          <Text className="utp-assemble-title taro-text">{data && data.goodsName}</Text>
        </View>
        <View className="utp-flex utp-assemble-ft">
          <View className="utp-assemble-moneys utp-assemble-price">
            <Text>单买价：</Text>
            <View className="utp-assemble-orprice">
              <Yuan price={data && data.goodsGroupPrice} color="#999" size={26}></Yuan>
            </View>
          </View>
        </View>
        <View className="utp-flex utp-assemble-ft">
          <View className="utp-assemble-moneys">
            <Text>拼团价：</Text>
            <View className="utp-assemble-money">
              <Yuan price={data && data.goodsDiscountPrice} color="#FF2F7B" size={30}></Yuan>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion
Index.defaultProps = {};

export default Index;
