import Taro, { useEffect, useState, useDidShow } from '@tarojs/taro';
import { connect } from '@tarojs/redux';
import { ScrollView, View } from '@tarojs/components';
import Wrap from '../../components/Wrap';
import Txt from '../../components/Txt';
import Img from '../../components/Img';
import ItemHomeIcon from '../../components/ItemHomeIcon';
import './index.scss';
import { IIcons } from '../../interface/home';
import Router from '../../utils/router';
import { images } from '../../images';

type PageStateProps = {
  loading: boolean;
  data: IIcons[];
  icons: IIcons[];
  type?: number;
};

type PageDispatchProps = {
  saveIcons: (any) => void;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
const Index: Taro.FC<IProps> = ({ saveIcons, icons, data, type = 0 }) => {
  useDidShow(() => {});
  useEffect(() => {
    console.warn('保存icons数据1');
    if (icons.length === 0) {
      console.warn('保存icons数据2');
      saveIcons({
        icons: data
      });
    }
  }, []);

  const [num, setNum] = useState(0);
  let haveMiniQ = false;
  !!data && data.map((item, index) => {
    if (item.command && item.command.cmd === 'C0010010000') {
      haveMiniQ = true;
    }
  });
  let arr: any[] = [];

  !!data && data.map((item, index) => {
    if (item.command && item.command.cmd === 'C0010010000') {
      return;
    } else {
      arr.push(item);
    }
  });
  return (
    <View className="utp-ico1s">
      {type === 0 && (
        <View className="utp-ico1s-icos">
          <View className="utp-ico1s-icos-ico">
            <View
              className="utp-ico1s-icos-ico-crt"
              style={{ left: Taro.pxTransform(num < 0 ? 0 : num > 66 ? 66 : num) }}
            ></View>
          </View>
        </View>
      )}
      <ScrollView
        scrollX
        lowerThreshold={1}
        onScrollToLower={() => {
          Router.goEditUse();
        }}
        onScroll={(e) => {
          console.log(e);
          // @ts-ignore
          let width = e.detail.scrollWidth;
          // @ts-ignore
          let left = e.detail.scrollLeft;
          console.log(width);
          console.log(left);
          setNum((66 * left) / 85);
        }}
      >
        <View className={'utp-ico1 ' + (type === 0 ? '' : 'utp-ico1-1')}>
          {type === 0 && (
            <View className="utp-ico1-mores">
              <View className="utp-ico1-mores-more">
                <Img src={images.homeMore} width={28} />
              </View>
              <View className="utp-ico1-mores-txts">
                <View className="utp-ico1-mores-txt">
                  <Txt title="继" color="low" />
                </View>
                <View className="utp-ico1-mores-txt">
                  <Txt title="续" color="low" />
                </View>
                <View className="utp-ico1-mores-txt">
                  <Txt title="滑" color="low" />
                </View>
                <View className="utp-ico1-mores-txt">
                  <Txt title="动" color="low" />
                </View>
                <View className="utp-ico1-mores-txt">
                  <Txt title="查" color="low" />
                </View>
                <View className="utp-ico1-mores-txt">
                  <Txt title="看" color="low" />
                </View>
                <View className="utp-ico1-mores-txt">
                  <Txt title="更" color="low" />
                </View>
                <View className="utp-ico1-mores-txt">
                  <Txt title="多" color="low" />
                </View>
              </View>
            </View>
          )}
          <Wrap>
            {!!data && !!arr && 
              arr.map((item, index) => {
                //if (item.command && item.command.cmd === 'C0010010000') {
                //return;
                //}
                if (index < 10)
                  return (
                    <View
                      className={'utp-ico1-box ' + (type === 0 ? '' : 'utp-ico1-box-1')}
                      key={index}
                    >
                      <ItemHomeIcon
                        tips={item.title}
                        src={item.source}
                        tipColor="#333"
                        onClick={() => {
                          // @ts-ignore
                          Router.goHome(item.command);
                        }}
                      />
                    </View>
                  );
              })}
          </Wrap>
        </View>
      </ScrollView>
    </View>
  );
};
Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    icons: state.cart.icons
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    saveIcons: (payload) => {
      dispatch({
        type: 'cart/saveIcons',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
