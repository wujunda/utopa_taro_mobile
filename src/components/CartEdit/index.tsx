import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import './index.scss';
import Img from '../Img';
import Txt from '../Txt';
import Bton from '../Bton';
import Wrap from '../Wrap';
import Cnt from '../Cnt';
import { images } from '../../images';

interface IProps {
  ischeck?: boolean;
  chooseAll?: () => void;
  onDel: () => void;
  onMove: () => void;
}
const Index: Taro.FC<IProps> = ({ ischeck = false, chooseAll, onDel, onMove }) => {
  const handleClick = (e) => {
    e.stopPropagation();
    if (chooseAll) {
      chooseAll();
    }
  };
  return (
    <View className="utp-cart-sub">
      <View onClick={handleClick}>
        <Wrap type={2}>
          <View className="utp-cart-sub-icos">
            <Cnt>
              <Img src={ischeck ? images.check : images.uncheck} width={36} />
            </Cnt>
          </View>
          <Txt title="全选" />
        </Wrap>
      </View>
      <Wrap>
        <View className="utp-cart-sub-box" onClick={onDel}>
          <Bton type={2} size="mini">
            删除
          </Bton>
        </View>
        <View className="utp-cart-sub-box" onClick={onMove}>
          <Bton type={1} size="mini">
            移入收藏
          </Bton>
        </View>
      </Wrap>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
