import Taro, { useState, useEffect, useRouter, pxTransform } from '@tarojs/taro';
import { connect } from '@tarojs/redux';
import { ScrollView, Image, View } from '@tarojs/components';
import { getBaseInfo, getPointColumnProductList } from '@/services/specialArea';
import { wxRegister, shareCallbackList } from '@/utils/wxApi';
import JSBridge from '@/utils/jsbridge/index';
import Header from '../components/Header';
import './index.scss';
import USwiper from '../components/Swiper';
import Menu from './components/Menu';
import List from './components/List';

// TODO fix any type
const IntegralMall = (props) => {
  const router = useRouter();
  const tempId = router.params.zoneId;
  const [banners, setBanners] = useState<any>([]);
  const [classes, setClasses] = useState<any>([]);
  const [columns, setColumns] = useState<any>([]);
  const [config, setConfig] = useState<any>({});
  const [topOpa, setTopOpa] = useState<any>(1);

  async function getConfig() {
    const { data } = await getBaseInfo({ id: tempId });
    //window.document.title = data.name;
    setBanners(data.banners || []);
    setClasses(data.classes || []);
    setConfig(data);
    registWechatShare(data);
  }

  async function getColumns() {
    const { data } = await getPointColumnProductList({ themeId: tempId });
    setColumns(data.list);
  }
  const registWechatShare = (shareConfig) => {
    if (props.user.isWeiXinFalg && process.env.TARO_ENV === 'h5') {
      wxRegister(
        shareCallbackList({
          title: shareConfig.name,
          desc: '',
          link: window.location.href,
          imgUrl:
            shareConfig.image ||
            'https://image.myutopa.com/Fr0Vlg2LNy2owURS5bDu1sNxfrHl?e=1614217121&token=WNtUYaMiCdpTDzanlE03SjKXLinbmTzkLGXEBYC_:RSZxw6kzc_1z1ua7yQifGs1YLkg='
        })
      );
    }
  };

  useEffect(() => {
    getConfig();
    getColumns();
  }, []);

  const onScroll = (e) => {
    let size = e.detail.scrollTop;
    // 1 默认透明，2白底黑字
    if (size > 20) {
      setTopOpa(2);
    } else {
      setTopOpa(1);
    }
  };

  return (
    <ScrollView
      scrollY
      className="integral-mall"
      onScroll={onScroll}
      style={!props.user.inApp ? { paddingTop: pxTransform(0) } : {}}
    >
      {props.user.inApp && (
        <Header
          title={config.name}
          color={config.fontColor}
          topOpa={topOpa}
          hasShare
          isApp
          rightClick={() => {
            if (props.user.inApp && process.env.TARO_ENV === 'h5') {
              let share = {
                shareTitle: config.name,
                subTitle: '',
                shareIcon:
                  'https://image.myutopa.com/Fr0Vlg2LNy2owURS5bDu1sNxfrHl?e=1614217121&token=WNtUYaMiCdpTDzanlE03SjKXLinbmTzkLGXEBYC_:RSZxw6kzc_1z1ua7yQifGs1YLkg=',
                shareUrl: window.location.href
              };
              console.log('在app内');
              console.log(share, 'share');
              JSBridge.Common.GTBridge_Common_Base_showShare(share, (res) => {
                if (res) {
                  console.log('分享成功');
                }
              });
            }
          }}
        />
      )}
      {config && (
        <Image
          className="integral-mall-top"
          src={config.backgroundImage}
          style={!props.user.inApp ? { top: 0 } : {}}
        />
      )}
      <View className="integral-content">
        {!!banners && <USwiper banners={banners} />}
        {!!classes && <Menu classes={classes} />}
        {!!columns && <List inApp={props.user.inApp} columns={columns} />}
      </View>
    </ScrollView>
  );
};

IntegralMall.config = {
  navigationBarTitleText: '积分商城',
  disableScroll: true,
  enablePullDownRefresh: true,
  navigationStyle: 'default'
};

const mapStateToProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    }
  };
};

//@ts-ignore
export default connect(mapStateToProps)(IntegralMall);
