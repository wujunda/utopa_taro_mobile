// eslint-disable-next-line import/prefer-default-export
/**
 * 支付方式
 * **/
export const PAY_WAY = {
  1: '余额',
  2: '微信',
  3: '支付宝',
  4: '微信扫码',
  5: '微信公众号',
  6: '支付宝扫码',
  7: '微信刷卡',
  8: '支付宝刷卡',
  9: '支付宝生活号',
  21: '工行微信',
  22: '工行支付宝',
  23: '工行微信公众号',
  24: '工行支付宝生活号',
  25: '工行微信刷卡',
  26: '工行支付宝刷卡',
  27: '工行二维码',
  31: '优星币支付',
  32: '优星币刷卡支付'
};
/**
 * 订单状态
 * **/
export const ORDER_DETAIL_STATUS = {
  0: '待支付',
  1: '待发货',
  2: '已发货',
  3: '已完成',
  4: '已关闭',
  5: '售后',
  6: '退款中',
  7: '已退款',
  8: '待接单',
  11:'待成团',
  12:'待付尾款',
};

//退款类型
export enum REFUND_TYPE {
  '仅退款' = 1,
  '退货退款' = 2
}
// 退款状态
export const REFUND_DETAIL_STATUS = {
  WAIT_VERIFY: 0, //待商家审核
  B_REJECT: 1, //商家拒绝
  WAI_SEND_BACK: 2, // 等待买家寄给回
  WAIT_B_RECEIVED: 3, // 等待商家收货
  REJECT_RECEIVED: 4, //拒绝收货
  REFUND_ING: 5, //退款中
  COMPLETE_REFUND: 6, // 已完成退款
  CLOSE: 7 //关闭
};

// 后端错误码
export const ERROR_CODE = {
  SYS_OK: 'SYS_OK', // 成功
  SYS_ERROR: 'SYS_ERROR', // 应用程序发生异常，请稍后再试
  SYS_BUSY: 'SYS_BUSY', // 系统繁忙，请稍后再试
  EXPIRE_ERROR: 'EXPIRE_ERROR', // 会话已过期
  SESSION_RENEW: 'SESSION_RENEW', // 会话需续约
  INVALID_SESSION: 'INVALID_SESSION', // 无效会话
  INVALID_APPLICATION: 'INVALID_APPLICATION', // 无效应用
  SIGN_FAILED: 'SIGN_FAILED', // 签名失败
  SIGN_INCORRECT: 'SIGN_INCORRECT', // 签名不正确
  USER_NOT_LOGIN: 'USER_NOT_LOGIN', // 用户未登录
  UNAUTHORIZED: 'UNAUTHORIZED', // 未授权
  TOKEN_EXPIRE: 'TOKEN_EXPIRE', // 令牌过期
  TOKEN_RENEW: 'TOKEN_RENEW', // 令牌需要续约
  PARENT_TOKEN_INVALID: 'PARENT_TOKEN_INVALID', // 父令牌无效
  NO_INTERFACE_AUTH: 'NO_INTERFACE_AUTH', // 没有访问该接口的权限
  SYS_OPT_FREQUENTLY_ERROR: 'SYS_OPT_FREQUENTLY_ERROR' // 操作太频繁
};

// 弹出的提示信息
export const TOAST_MESSAGE = {
  API_ERROR: '请求异常'
};

export const ORDER_DETAIL_DISTRIBUTETYPE = {
  // API_ERROR: '请求异常'
  0:'物流配送',
  1:'门店自提',
  2:'扫码自提',
  3:'即时达',
  4:'同城送',
  5:'全国送',
  6:'到店享用'

};

// ，0:物流配送，1:门店自提，2:扫码自提，3：周边送 4：同城送 5：全国送 6: 到店享用',
// { value: "", label: "全部" },
// // { value: 0, label: "物流配送" },
// { value: 1, label: "门店自提" },
// { value: 2, label: "扫码购" },
// { value: 3, label: "周边送" },
// //{ value: 4, label: "同城送" },
// { value: 5, label: "快递" },
// { value: 6, label: "到店享用" }

// distributionType: [
//   "物流配送",
//   "门店自提",
//   "扫码购",
//   "周边送",
//   "同城送",
//   "快递",
//   "到店享用"
// ],
