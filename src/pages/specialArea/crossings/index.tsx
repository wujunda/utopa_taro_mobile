import Taro, { useState, useEffect, useRouter, pxTransform } from '@tarojs/taro';
import { connect } from '@tarojs/redux';
import { View, Image, ScrollView } from '@tarojs/components';
import JSBridge from '@/utils/jsbridge/index';
import { getBaseInfo, getColumnProduct, getColumnStore } from '@/services/specialArea';
import { wxRegister, shareCallbackList } from '@/utils/wxApi';
import { handleRequest, Glo } from '@/utils/utils';
import './index.scss';
import GoodsCard from './components/GoodsCard';
import USwiper from './components/USwiper';
import Header from '../components/Header';
import FlowGoodsItem from '../components/FlowGoodsItem/index';
import Goods from '@/components/Goods';
import { commonNavigateTo } from '@/hooks/useShareAppMessage';

const useLocation = (props) => {
  const [location, setLocation] = useState({
    longitude: 0,
    latitude: 0
  });
  useEffect(() => {
    getLocation();
  }, [location]);

  function getLocation() {
    if (props.user.inApp) {
      JSBridge.Common.GTBridge_Common_Base_location({}, (resp) => {
        let ua = '';
        if (process.env.TARO_ENV === 'h5') {
          ua = navigator.userAgent;
        }
        const isIos = !!ua.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
        const isAndroid = ua.indexOf('Android') > -1 || ua.indexOf('Linux') > -1;
        if (isIos) {
          if (location.longitude === 0) {
            setLocation(resp);
          }
        } else if (isAndroid) {
          if (location.longitude === 0) {
            setLocation(JSON.parse(resp));
          }
        }
      });
    }
  }
  return [location];
};

const Crossings = (props) => {
  const router = useRouter();
  const tempId = router.params.zoneId;
  const [active, setActive] = useState(0);
  const [config, setConfig] = useState<any>({});
  const [classes, setClasses] = useState<any>([]);
  const [adGroup, setAdGroup] = useState<any>([]);
  const [banners, setBanners] = useState<any>([]);
  const [tabs, setTabs] = useState<any>([]);
  // 切换栏目类型数据
  const [columnType, setColumnType] = useState<any>([]);
  const [location] = useLocation(props);

  const [loading, setLoading] = useState<any>({});
  const [limit, setLimit] = useState({
    limit: 0,
    total: 0
  });
  const [page, setPage] = useState({
    pageNo: 1,
    pageSize: 10
  });
  const [column, setColumn] = useState<any>({});
  const [list, setList] = useState([]);
  const LOAD_DATA_TEXT = '数据加载中...';
  // 图文视频类型 商品列表
  const VIDEO_IMG_TYPE = 7;
  useEffect(() => {
    const params = {
      id: tempId
    };
    getConfig(params).then((resp) => {
      const { id, dataType, sortRule } = resp.columns[0];
      setColumn({
        id,
        dataType,
        sortRule
      });
      setColumnType(resp.columns[0]);
      handleTab(0, resp.columns[0]);
    });
  }, [location]);

  async function getConfig(params) {
    Glo.loading(LOAD_DATA_TEXT);
    try {
      const resp = await getBaseInfo(params);
      if (handleRequest(resp as any)) {
        const { data } = resp;
        // window.document.title = data.name;
        setConfig(data);
        setBanners(data.banners || []);
        setClasses(data.classes || []);
        setAdGroup(data.adverGroups || []);
        setTabs(data.columns || []);
        if (process.env.TARO_ENV === 'h5' && !props.user.inApp) {
          wxRegister(
            shareCallbackList({
              title: data.shareTitle || data.name,
              desc: data.shareContent || 'CROSSING分享',
              link: window.location.href,
              imgUrl:
                data.image ||
                'https://image.myutopa.com/Fr0Vlg2LNy2owURS5bDu1sNxfrHl?e=1614217121&token=WNtUYaMiCdpTDzanlE03SjKXLinbmTzkLGXEBYC_:RSZxw6kzc_1z1ua7yQifGs1YLkg='
            })
          );
        }
        Glo.hideLoading();
        return data;
      }
    } catch (e) {
      Glo.hideLoading();
    }
  }

  async function getColumns(params) {
    Glo.loading(LOAD_DATA_TEXT);
    try {
      const resp =
        params.dataType === 3 ? await getColumnStore(params) : await getColumnProduct(params);
      if (handleRequest(resp as any)) {
        const { data } = resp;
        setList(data.records || []);
        setLimit({
          limit: data.limit,
          total: data.total
        });
      }
      Glo.hideLoading();
    } catch (e) {
      Glo.hideLoading();
    }
  }

  const toDetails = (item) => {
    if (props.user.inApp) {
      JSBridge.Common.GTBridge_Common_Base_CMD(item.command, () => {
        console.log('执行', item.command);
      });
    } else {
      item.url && (window.location.href = item.url);
    }
  };

  async function handleTab(index, item) {
    const data = {
      id: item.id,
      dataType: item.dataType,
      sortRule: item.sortRule
    };
    setColumnType(data);
    const params = {
      pageNo: 1,
      pageSize: 10,
      ...location,
      ...data
    };
    await getColumns(params);
    setColumn(data);
    setPage({
      pageNo: 1,
      pageSize: 10
    });
    setActive(index);
  }

  const Threshold = 80;
  const onScrollToLower = async () => {
    if (limit.limit < limit.total) {
      setLoading({ status: true, text: '正在加载' });
      setPage({ pageNo: 1, pageSize: page.pageSize += 10 });
      await getColumns({ ...column, ...page, ...location });
      setLoading({ status: true, text: '加载成功' });
    } else {
      setLoading({ status: true, text: '加载完毕' });
    }
  };

  return (
    <ScrollView
      className="crossings"
      scrollY
      style={!props.user.inApp ? { paddingTop: pxTransform(20) } : {}}
      lowerThreshold={Threshold}
      onScrollToLower={onScrollToLower}
    >
      {props.user.inApp && process.env.TARO_ENV === 'h5' && (
        <Header
          title={config.name}
          topOpa={2}
          hasShare
          isApp={props.user.inApp}
          rightClick={() => {
            if (props.user.inApp && process.env.TARO_ENV === 'h5') {
              let share = {
                shareTitle: config.name,
                subTitle: config.shareTitle,
                shareIcon:
                  config.shareImage ||
                  'https://image.myutopa.com/Fr0Vlg2LNy2owURS5bDu1sNxfrHl?e=1614217121&token=WNtUYaMiCdpTDzanlE03SjKXLinbmTzkLGXEBYC_:RSZxw6kzc_1z1ua7yQifGs1YLkg=',
                shareUrl: config.shareUrl || window.location.href
              };
              console.log('在app内');
              console.log(share, 'share');
              JSBridge.Common.GTBridge_Common_Base_showShare(share, (res) => {
                if (res) {
                  console.log('分享成功');
                }
              });
            }
          }}
        />
      )}
      {config.backgroundImage && (
        <View style={{ padding: `0 ${pxTransform(20)}` }}>
          <Image className="crossings-top" src={config.backgroundImage} />
        </View>
      )}
      {!!banners && <USwiper banners={banners} inApp={props.user.inApp} />}
      {!!classes && (
        <View className="crossings-first">
          {classes.map((item, idx) => (
            <Image
              onClick={() => toDetails(item)}
              key={`idx+${idx}`}
              className="crossings-first-item"
              src={item.image}
            />
          ))}
        </View>
      )}
      {!!adGroup && (
        <View className="crossings-second">
          {!!adGroup[0] && !!adGroup[0].groupDetails[0] && (
            <Image className="crossings-second-bg" src={adGroup[0].groupDetails[0].image} />
          )}

          {!!adGroup[0] && !!adGroup[0].groupDetails[1] && (
            <Image
              className="crossings-second-top"
              onClick={() => toDetails(adGroup[0].groupDetails[1])}
              src={adGroup[0].groupDetails[1].image}
            />
          )}
          <View className="crossings-second-content">
            {!!adGroup &&
              !!adGroup[0] &&
              adGroup[0].groupDetails.length > 0 &&
              adGroup[0].groupDetails.map((item, index) => {
                return (
                  index > 1 && (
                    <Image
                      key={`idx+${index}`}
                      onClick={() => toDetails(item)}
                      className="crossings-second-content-item"
                      src={item.image}
                    />
                  )
                );
              })}
          </View>
        </View>
      )}
      {adGroup &&
        adGroup.length > 1 &&
        adGroup.map((item, index) => {
          return (
            index > 0 && (
              <View key={item.id + index} className="crossings-third">
                {!!item.groupDetails[0] && (
                  <Image
                    className="crossings-third-bg"
                    src={item.groupDetails[0].image}
                    onClick={() => toDetails(item.groupDetails[0])}
                  />
                )}
                {!!item.groupDetails && item.groupDetails.length > 1 && (
                  <View className="crossings-third-content">
                    {item.groupDetails.map((req, idx) => {
                      // eslint-disable-next-line react/jsx-key
                      return (
                        idx > 0 &&
                        idx < 4 && (
                          <Image
                            onClick={() => toDetails(req)}
                            className="crossings-third-content-item"
                            src={req.image}
                          />
                        )
                      );
                    })}
                  </View>
                )}
              </View>
            )
          );
        })}

      {!!tabs && (
        <ScrollView scrollX scrollWithAnimation className="crossings-tabs">
          {tabs.map((item, index) => (
            <View key={item.id} style={{ display: 'inline-block' }}>
              <View className="crossings-tabs-item" onClick={() => handleTab(index, item)}>
                <View
                  className="crossings-tabs-item-top"
                  style={active === index ? { color: '#51B44C' } : {}}
                >
                  {item.name}
                </View>
                {item.title && (
                  <View
                    className="crossings-tabs-item-bottom"
                    style={active === index ? { color: '#fff', background: '#51B44C' } : {}}
                  >
                    {item.title}
                  </View>
                )}
              </View>
              {index + 1 !== tabs.length && <View className="crossings-tabs-line" />}
            </View>
          ))}
        </ScrollView>
      )}

      <View className="crossings-list">
        {!!list &&
          columnType.dataType != VIDEO_IMG_TYPE &&
          list.map((item: any) => <GoodsCard key={item.id} goods={item} type={column.dataType} />)}
      </View>
      {/* 图文视频类型 */}
      <View className="flow-box">
        {!!list &&
          columnType.dataType == VIDEO_IMG_TYPE &&
          list.map((item: any, idx) => (
            <FlowGoodsItem
              onClick={() => {
                if (!props.user.inApp && process.env.TARO_ENV === 'h5') {
                  let pages: any = `/pages/details/index?id=${item.id}&businessId=${item.businessId}&storeId=${item.storeId}`;
                  commonNavigateTo(pages);
                } else {
                  let cmdParams = item.productCommand;
                  let params_str = JSON.parse(item.productCommand.params);
                  if (router.params.crm_channel_id) {
                    params_str['crm_channel_id'] = Number(router.params.crm_channel_id);
                  }
                  cmdParams.params = JSON.stringify(params_str);
                  JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, () => {});
                }
              }}
              imgHeight={idx % 2 == 0 ? 300 + idx * 2 : 150 + idx * 3}
              key={item.id}
              goods={item}
              type={column.dataType}
            />
          ))}
      </View>
      {loading.status && <View className="crossings-loading">--{loading.text}--</View>}
    </ScrollView>
  );
};

Crossings.config = {
  navigationBarTitleText: 'Crossings专区',
  disableScroll: true,
  enablePullDownRefresh: true,
  navigationStyle: 'default'
};

const mapStateToProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    },
    cart: {
      latitude: state.cart.x,
      longitude: state.cart.y
    }
  };
};

//@ts-ignore
export default connect(mapStateToProps)(Crossings);
