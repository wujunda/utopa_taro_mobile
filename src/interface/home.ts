import request from '../utils/request';

// 轮播
export interface ISwipe {
  // 路由
  url: string;
  // 图片地址
  source: string;
}

// 图标
export interface IIcons {
  // 路由
  url: string;
  // 图片地址
  source: string;
  // 文字
  title: string;
}

// 商品
export interface IGoods {
  // 商品id
  id: number;
  // 商品图片
  productImage: string;
  goodsLogo:string;
  // 商品标题
  productName: string;
  // 商品价格
  salePrice: number;
  // 促销价
  originPrice: number;
  // 是否自营
  selfSupport: number;
  // 
  productId:number;
  skuId:number;
  storeId:number;
  businessId:number;
  activeLabel:string;
  //部分商品用的一下字段
  originalPrice?:number;
  discountPrice?:number;
  goodsId?:number;
  goodsName?:string;
}
export interface IAssembel {
  goodsLogo: string;
  goodsName: string;
  activityId: number;
  activityCode: string;
  activityCreateTime: number;
  activityEndTime: number;
  goodsGroupPrice: number;
  goodsDiscountPrice: number;
  groupStatus: number;
  activityUserCount: number;
}
export const getGoodsList = (params: any) => {
  return request.get<any>('/mobile/index.php', params);
};
