import Taro, { usePageScroll, useRouter, useState, useDidShow, useEffect } from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';
import Txt from '../../../components/Txt';
import Wrap from '../../../components/Wrap';
import Swipe from '../../../components/Swipe';
import Goods1 from '../../../components/Goods1';
import Icons from '../../../components/Icons';
import useRequest from '../../../hooks/useRequest';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import { getStoreHome, getStoreGoods, IHome } from '../../../services/storeHome';
import './index.scss';
import { connect } from '@tarojs/redux';
import { ISwipe, IIcons } from '../../../interface/home';
import SearchStore from '../../../components/SearchStore';

interface Props {
  statusHeight: number;
  closeWidth: number;
  unread: number;
}
let url = 'neweshop.special.getColumnStore';
const Index: Taro.FC<Props> = ({}) => {
  const [fix, setFix] = useState(false);
  const [top, setTop] = useState(0);
  usePageScroll((e) => {
    let scrollTop = e.scrollTop;
    console.log('SB');
    console.log(scrollTop);
    console.log(top);
    if (scrollTop > top) {
      setFix(true);
    } else {
      setFix(false);
    }
  });

  useDidShow(() => {});
  // 首页数据
  // @ts-ignore
  const [state, update, loading] = useRequest<IHome>({ id: useRouter().params.id }, getStoreHome);

  const [start, setStart] = useState('start');
  // @ts-ignore
  const [state1, hasMore1, update1, getMoreData1, loading1] = useRequestWIthMore<IHistory>(
    start,
    getStoreGoods
  );
  //
  const [id, setId] = useState('');
  const [init, setInit] = useState(false);
  useEffect(() => {
    console.log('数据');
    console.log(state);
    if (state && state.code === 0) {
      console.log('初始化');
      if (!init) {
        setInit(true);
        Taro.setNavigationBarTitle({
          // @ts-ignore
          title: state.data.name
        });

        // @ts-ignore
        setId(state.data.columns[0].id);
        // @ts-ignore
        url =
          state.data.columns[0].dataType === 3
            ? 'neweshop.special.getColumnStore'
            : 'eshop.special.getColumnProduct';
        setStart('start1' + '%%' + state.data.columns[0].id);
        setTimeout(() => {
          Taro.createSelectorQuery()
            .select('#utp-storeHome-menus')
            .fields(
              {
                dataset: true,
                size: true,
                scrollOffset: true,
                properties: ['scrollX', 'scrollY']
              },
              function (res) {
                res.height; // 节点的高度
                setTop(res.height);
              }
            )
            .exec();
        }, 30);
      }
    }
  }, [state]);
  useEffect(() => {
    if (id) {
      setStart(url + '%%start1' + '%%' + id);
      if (update1) {
        // @ts-ignore
        update1();
      }
    }
  }, [id]);

  let swipes: ISwipe[] = [];
  if (state && state.code === 0 && state.data && state.data.banners) {
    // @ts-ignore
    swipes = state.data.banners;
    swipes.map((item) => {
      // @ts-ignore
      item.source = item.image;
    });
  }
  let icos: IIcons[] = [];
  if (state && state.code === 0 && state.data && state.data.classes) {
    // @ts-ignore
    icos = state.data.classes;
    icos.map((item) => {
      // @ts-ignore
      item.source = item.image;
      // @ts-ignore
      item.title = item.name;
    });
  }
  let txts: any[] = [];
  if (state && state.code === 0 && state.data && state.data.columns) {
    txts = state.data.columns;
  }
  //  menu index
  const [crt, setCrt] = useState(0);
  // @ts-ignore
  var goods: ProductBaiscData[] = [];
  let isStore = url === 'neweshop.special.getColumnStore';
  if (!isStore && state1 && state1.length > 0) {
    state1.map((item) => {
      if (item && item.data && item.data.records) {
        item.data.records.map((item) => {
          if (item.productId) {
            goods.push(item);
          }
        });
      }
    });
  }

  var stores: any[] = [];
  if (isStore && state1 && state1.length > 0) {
    state1.map((item) => {
      if (item && item.data && item.data.records) {
        item.data.records.map((item) => {
          if (!item.productId) {
            stores.push(item);
          }
        });
      }
    });
  }
  console.log(goods);
  console.log(stores);
  console.log(isStore);

  return (
    <View className="utp-storeHome">
      <View id="utp-storeHome-menus">
        <View className="utp-storeHome-swipes">
          <Swipe data={swipes} height={280} />
        </View>
        <View className="utp-storeHome-icos">
          <Icons data={icos} type={1} />
        </View>
        <View className="utp-storeHome-menusTop"></View>
      </View>
      {false && (
        <View className="utp-storeHome-ad2">
          <View className="utp-cnt">
            <View className="utp-storeHome-ad2-box-item6"></View>
          </View>
          <View className="utp-cnt">
            <View className="utp-storeHome-ad2-box">
              <Wrap justifyContent="space-between">
                <View className="utp-storeHome-ad2-box-item"></View>
                <View className="utp-storeHome-ad2-box-item"></View>
                <View className="utp-storeHome-ad2-box-item"></View>
              </Wrap>
            </View>
          </View>
        </View>
      )}

      {false && (
        <View className="utp-storeHome-ad3">
          <Wrap type={1} justifyContent="space-between">
            <View></View>
            <View className="utp-storeHome-ad3-box">
              <Wrap justifyContent="space-between">
                <View className="utp-storeHome-ad3-box-item"></View>
                <View className="utp-storeHome-ad3-box-item"></View>
                <View className="utp-storeHome-ad3-box-item"></View>
              </Wrap>
            </View>
          </Wrap>
        </View>
      )}
      <View className="utp-storeHome-menus">
        <View
          className="utp-storeHome-menus-1"
          style={{ position: fix ? 'static' : 'absolute' }}
        ></View>
        <ScrollView
          className="utp-storeHome-menus-2"
          scrollX
          style={{ position: fix ? 'fixed' : 'static' }}
        >
          {txts.map((item, index) => {
            return (
              <View
                className="utp-storeHome-menus-item"
                onClick={() => {
                  if (loading) {
                    return;
                  }
                  url =
                    item.dataType === 3
                      ? 'neweshop.special.getColumnStore'
                      : 'eshop.special.getColumnProduct';
                  setCrt(index);
                  setId(item.id);
                }}
              >
                <Txt title={item.name} size={30} color={index === crt ? 'black' : 'low'} />
              </View>
            );
          })}
        </ScrollView>
      </View>
      <View className="utp-storeHome-box">
        {!isStore && (
          <View>
            {!isStore && loading1 && goods.length === 0 && <Loading />}
            {!isStore && !loading1 && goods.length === 0 && <LoadingNone />}
          </View>
        )}
        {isStore && (
          <View>
            {isStore && loading1 && stores.length === 0 && <Loading />}
            {isStore && !loading1 && stores.length === 0 && <LoadingNone />}
          </View>
        )}
        {!isStore &&
          goods.map((item) => {
            return <Goods1 item={item} />;
          })}
        {isStore &&
          stores.map((item) => {
            return (
              <SearchStore
                item={item}
                key={item.storeId}
                like={() => {
                  //dolike(item);
                }}
              />
            );
          })}

        {!isStore && loading1 && goods.length !== 0 && <LoadingMore />}
        {!isStore && !loading1 && !hasMore1 && goods.length > 0 && <LoadingNoneMore />}
        {isStore && loading1 && stores.length !== 0 && <LoadingMore />}
        {isStore && !loading1 && !hasMore1 && stores.length > 0 && <LoadingNoneMore />}
      </View>
    </View>
  );
};
Index.config = {
  navigationBarTitleText: '',
  disableScroll: false,
  backgroundColor: '#f6f6f6'
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight,
    closeWidth: state.cart.closeWidth,
    unread: state.cart.unread
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
