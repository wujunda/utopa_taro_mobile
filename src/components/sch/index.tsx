import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import Wrap from '../../components/Wrap';
import Img from '../../components/Img';
import Txt from '../../components/Txt';
import { images } from '../../images';

import './index.scss';
import { Glo } from '../../utils/utils';
import Router from '../../utils/router';

interface IProps {
  type?: 1 | 2;
  // width?: number; //百分比的数字
  opacity?: number;
  onScan?: () => void;
  scrollTop?: number;
  closeWidth?: number;
  unread: number;
  isLogin: any;
  adsTitle: string;
}

const Index: Taro.FC<IProps> = ({
  unread,
  type = 1,
  opacity = 0,
  onScan,
  scrollTop = 0,
  closeWidth,
  isLogin,
  adsTitle
}) => {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  let transX;
  if (scrollTop != 0) {
    transX = scrollTop * (5 / 6);
    if (transX > 30) {
      transX = 30;
    }
  }
  let width = 355;
  if (opacity) {
    //width = (1 - opacity) * 375;
  }
  // console.log('我是搜索元素————----', (1 - opacity) * 88, opacity);

  return (
    <View className="utp-schBox">
      <View
        className="utp-sch-box"
        style={{ width: Taro.pxTransform(750 - (closeWidth ? closeWidth * 2 : 0)) }}
      >
        {type === 1 && (
          <View className="utp-sch">
            <View
              className="utp-sch-ads1 utp-cnt"
              onClick={() => {
                Taro.navigateTo({
                  url: '/pages/packOrder/locationHome/index'
                });
              }}
            >
              <Image mode="aspectFit" className="utp-sch-i1" src={images.ads} />
            </View>

            <View
              className="utp-sch-ads utp-cnt"
              onClick={() => {
                Taro.navigateTo({
                  url: '/pages/packOrder/locationHome/index'
                });
              }}
            >
              <Image mode="aspectFit" className="utp-sch-i" src={images.jishida} />
            </View>
            <View
              className="utp-schs utp-cnt"
              onClick={() => {
                Taro.navigateTo({
                  url: '/pages/packOrder/search/index'
                });
              }}
            >
              <View className="utp-sch1 utp-solid">
                <View className="utp-sch2">
                  <Wrap type={2} Myheight={60}>
                    <Image
                      mode="aspectFit"
                      className="utp-sch1-ico"
                      src={images.home_search_icon_black}
                    />
                    <Txt title="搜索" size={28} color="low" height={25} />
                  </Wrap>
                </View>
              </View>
            </View>

            <View
              className="utp-sch-scan utp-cnt"
              onClick={() => {
                Glo.scan(0);
              }}
            >
              <Image mode="aspectFit" className="utp-sch-i" src={images.scan} />
            </View>
            <View
              className="utp-sch-msg utp-cnt"
              style={{ position: 'relative' }}
              onClick={() => {
                if (!isLogin) {
                  Router.goLogin();
                } else {
                  Taro.navigateTo({
                    url: '/pages/packOrder/msg/index'
                  });
                }
              }}
            >
              <Image mode="aspectFit" className="utp-sch-i" src={images.msg} />
              {!!unread && (
                <View
                  style={{
                    position: 'absolute',
                    zIndex: 1,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    top: Taro.pxTransform(10),
                    right: Taro.pxTransform(6),
                    left: Taro.pxTransform(10)
                  }}
                >
                  <View
                    className="utp-div utp-solid"
                    style={{
                      height: Taro.pxTransform(32),
                      minWidth: Taro.pxTransform(32),
                      borderWidth: Taro.pxTransform(2),
                      borderColor: '#ff2f7b',
                      backgroundColor: '#ff2f7b',
                      borderRadius: Taro.pxTransform(32),
                      paddingLeft: Taro.pxTransform(5),
                      paddingRight: Taro.pxTransform(5),
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}
                  >
                    <Txt size={23} color="white" title={unread}></Txt>
                  </View>
                </View>
              )}
            </View>
          </View>
        )}

        {type === 2 && (
          <View style={{ position: 'relative', height: '100%' }}>
            <View className="utp-sch">
              <View
                className="utp-sch-ads1 utp-cnt"
                onClick={() => {
                  Taro.navigateTo({
                    url: '/pages/packOrder/locationHome/index'
                  });
                }}
              >
                <Image mode="aspectFit" className="utp-sch-i1" src={images.ads_1} />
              </View>

              <View className="utp-sch-ads utp-cnt">
                {false && !!adsTitle && (
                  <View className="utp-sch-ads-tips">
                    <View className="utp-sch-ads-tips-sanjiao">
                      <Img src={images.sanjiao} width={32} height={12} />
                    </View>
                    <Wrap type={1}>
                      <Txt title={`送至：${adsTitle}`} color="white" dian size={24} />
                    </Wrap>
                  </View>
                )}

                <Image
                  mode="aspectFit"
                  className="utp-sch-i"
                  src={opacity <= 0.3 ? images.jishida1 : images.jishida}
                />
              </View>
              {false && opacity < 0.3 && (
                <View className="utp-schs-distant">
                  {opacity < 0.3 && (
                    <View>
                      <Wrap type={2}>
                        <Text className="utp-schs-distant-text">
                          中国广东省广州市天河区黄村立交……
                        </Text>
                      </Wrap>
                    </View>
                  )}
                </View>
              )}
              <View
                className="utp-schs utp-cnt"
                onClick={() => {
                  Taro.navigateTo({
                    url: '/pages/packOrder/search/index'
                  });
                }}
              >
                <View className="utp-sch1 utp-solid utp-sch1-1">
                  <View className="utp-sch2">
                    <Wrap type={2} Myheight={60}>
                      <Image
                        mode="aspectFit"
                        className="utp-sch1-ico"
                        src={images.home_search_icon_black}
                      />
                      <Txt title="搜索" size={28} color="low" height={25} />
                    </Wrap>
                  </View>
                </View>
              </View>

              {opacity > 0.3 && (
                <View
                  className="utp-schs utp-cnt"
                  onClick={() => {
                    Taro.navigateTo({
                      url: '/pages/packOrder/search/index'
                    });
                  }}
                >
                  <View className="utp-sch1 utp-solid">
                    <View className="utp-sch2">
                      <Wrap type={2} Myheight={68}>
                        <Image
                          mode="aspectFit"
                          className="utp-sch1-ico"
                          src={images.home_search_icon_black}
                        />
                        <Txt title="搜索" size={28} color="low" height={25} />
                      </Wrap>
                    </View>
                  </View>
                </View>
              )}

              <View className="utp-sch-scan utp-cnt" onClick={onScan}>
                <Image
                  mode="aspectFit"
                  className="utp-sch-i"
                  src={opacity > 0.3 ? images.scan : images.home_icon_scan_white}
                />
              </View>
              <View className="utp-sch-msg utp-cnt" style={{ position: 'relative' }}>
                <Image
                  mode="aspectFit"
                  className="utp-sch-i"
                  src={opacity > 0.3 ? images.msg : images.home_message_white}
                />
                {!!unread && (
                  <View
                    style={{
                      position: 'absolute',
                      zIndex: 1,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                      top: Taro.pxTransform(10),
                      right: Taro.pxTransform(6),
                      left: Taro.pxTransform(10)
                    }}
                  >
                    <View
                      className="utp-div utp-solid"
                      style={{
                        height: Taro.pxTransform(32),
                        minWidth: Taro.pxTransform(32),
                        borderWidth: Taro.pxTransform(2),
                        borderColor: '#ffffff',
                        backgroundColor: '#ffffff',
                        borderRadius: Taro.pxTransform(32),
                        paddingLeft: Taro.pxTransform(5),
                        paddingRight: Taro.pxTransform(5),
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}
                    >
                      <Txt size={23} color="red" title={unread}></Txt>
                    </View>
                  </View>
                )}
              </View>
            </View>

            <View
              className="utp-sch-longSch utp-cnt"
              style={{
                width: '100%',
                opacity: 1 - opacity
              }}
              onClick={() => {
                Taro.navigateTo({
                  url: '/pages/packOrder/search/index'
                });
              }}
            >
              {false && (
                <View
                  className={
                    opacity >= 0.3 ? 'utp-sch-longSch-sch1 utp-solid' : 'utp-sch-longSch-sch1'
                  }
                  style={{
                    width: width > 248 ? width : Taro.pxTransform(496),
                    transform: scrollTop > 0.01 ? `translate3D(${-transX}px,0,0)` : ''
                  }}
                >
                  <View className="utp-sch2">
                    <Wrap type={2} Myheight={68}>
                      <Image
                        // mode="aspectFit"
                        className="utp-sch1-ico"
                        src={
                          opacity > 0.3
                            ? images.home_search_icon_black
                            : images.home_search_icon_white
                        }
                      />
                      <Txt
                        title="请搜索商户或者商品"
                        size={28}
                        color={opacity > 0.3 ? 'low' : 'white'}
                      />
                    </Wrap>
                  </View>
                  {/* </Wrap> */}
                  {false && opacity >= 0.3 && (
                    <View className="utp-sch1-btn utp-cnt utp-div">
                      <Text className="utp-sch1-txt">搜索</Text>
                    </View>
                  )}
                </View>
              )}
            </View>
          </View>
        )}
      </View>
    </View>
  );
};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    isLogin: state.cart.isLogin
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    getSubmit: (payload) => {
      dispatch({
        type: 'cart/getSubmit',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
