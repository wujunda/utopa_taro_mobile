import request from '../utils/request';

export interface IUserData {
  avatarUrl: string;
  nickname: string;
  mobile: string;
  utopaId: string;
  cardCount: number;
  collection: number;
  couponCount: number;
  discountCouponCount: number;
  instanceCount: number;
  memberPoints: number;
  newCardCount: number;
  point: number;
  productCollectCount: number;
  productHistoryCount: number;
  storeCollectCount: number;
  ucoinAmount: number;
  voucherCount: number;
  hasUcoinPayPwd: boolean;
  ucoinAvailable: boolean;
  items: Igoods[];
  completedCount: number;
  orderSalingRefund: number;
  refund: number;
  toBeCommentedOnCount: number;
  unPay: number;
  unReceive: number;
  unSend: number;
}
interface Igoods {
  endTime: number;
  exchangePoint: number;
  exchangePrice: number;
  marketingStock: number;
  name: string;
  picUrl: string;
  price: number;
  productId: number;
  skuId: number;
}
export interface IUser {
  code: number;
  data: IUserData;
}
export interface IUse {
  name: string;
  age: number;
}
export const getItem = (params: any) => {
  return request.get<any>('/item', params);
};
export const getResetPay = (params: any) => {
  return request.get<any>('/app.do', {
    method: 'utopa.account.resetPassword',
    params: JSON.stringify(params)
  });
};

export const getResetPassword = (params: any) => {
  return request.get<any>('/app.do', {
    method: 'account.reg.resetPassword',
    params: JSON.stringify(params)
  });
};

export const getCheckCode = (params: any) => {
  return request.get<any>('/app.do', {
    method: 'account.reg.verifyResetPswCheckCode',
    params: JSON.stringify(params)
  });
};
export const getCode = (params: any) => {
  return request.get<any>('/app.do', {
    method: 'account.reg.sendResetPswCheckCode',
    params: JSON.stringify(params)
  });
};

export const getSession = (params: any) => {
  // console.log(params);
  return request.get<any>('/app.do', {
    method: 'account.session.gen',
    params: JSON.stringify({})
  });
};

// 会话续约
export const renewSession = (params = {}) => {
  return request.post('/app.do', {
    method: 'account.session.renew',
    params
  });
};

export const getUser = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.useraction.getIndexUserDate',
    ver: '1.0',
    params: JSON.stringify({})
  });
};
export const getUserOrderCount = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.useraction.getUserOrderCount',
    ver: '1.0',
    params: JSON.stringify({})
  });
};
export const getArr = (url: string, params: any) => {
  // console.log(url);
  // console.log(params);
  return request.post<any>('/arr', params);
};
// 获取积分商城信息
export const getPointMall = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.point.getPointMall',
    ver: '1.0',
    params: JSON.stringify({})
  });
};

export const getOpenToken = () => {
  return request.post('/app.do', {
    method: 'account.access.getOpenToken',
    params: JSON.stringify({})
  })
}