import Taro, { useState, useEffect, useRouter } from '@tarojs/taro';
import { View, ScrollView, Text, Image, Textarea, Input } from '@tarojs/components';

import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Yuan from '../../../components/Yuan';
import Bar from '../../../components/bar';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import Bton from '../../../components/Bton';
import right from '../../../assets/right.png';
import icon_addphoto from '../../../assets/mine/icon_addphoto.png';
import Stepper from '../../../components/Stepper';
import {
  commitAfterSale,
  rebackProduct,
  returnsReasons,
  afterSaleDetail,
  recallAfterSaleOrdere,
  ProductType
} from '../../../services/afterSale';
import Qiniu from '../../../utils/qiniu';
import { getToken } from '../../../services/upload';
import './index.scss';

interface IProps {}

interface ReasonInfoType {
  id: number;
  reason: string;
}
const Index: Taro.FC<IProps> = ({}) => {
  const [steps, setsteps] = useState(0);
  const [openReason, setopenReason] = useState(false);
  const [reason, setReason] = useState('');
  const [address, setAddress] = useState('');
  const [reasonActive, setreasonActive] = useState(0);
  const [reaStr, setreaStr] = useState('');
  const [product, setProduct] = useState({} as ProductType);
  const [imgs, setImgs] = useState<any[]>([]);
  const [expressCompanyId, setExpressCompanyId] = useState('');
  const [expressNo, setExpressNo] = useState('');
  const router = useRouter();
  const [reasonInfo, setReasonInfo] = useState([] as ReasonInfoType[]);
  // 提交售后
  let feedbackKeys: any = [];
  const submit = async () => {
    const data = await getToken({});
    if (data.code === 0 && data.data && data.data.upToken && imgs) {
      imgs.map((item) => {
        Qiniu(data.data.upToken, item).then(
          (hash) => {
            feedbackKeys.push(hash);
            if (feedbackKeys.length === imgs.length) {
              onDone();
            }
          },
          () => {
            feedbackKeys = [];
          }
        );
      });
    } else {
      Taro.showToast({
        title: '获取token失败!',
        icon: 'none'
      });
    }
  };
  const onDone = async () => {
    let res = await commitAfterSale({
      skuId: product.products[0].skuId,
      orderId: product.orderId,
      goodsNum: product.goodsNum,
      totalGoodsPrice: product.orderTotalMoney,
      note: reason,
      certificatePics: feedbackKeys.join('.'),
      returnsReasonId: reasonActive
    });
    if (res.code === 0) {
      Taro.showToast({
        title: '提交成功',
        icon: 'success'
      });
      Taro.navigateTo({ url: '/pages/person/applyService/index' });
    } else {
      Taro.showToast({
        title: res.msg,
        icon: 'none'
      });
    }
  };
  //撤回申请
  const withdraw = () => {
    Taro.showModal({
      content: '确定撤回申请吗？',
      success: async (res) => {
        if (res.confirm) {
          //确定
          let resp = await recallAfterSaleOrdere({ afterSaleNo: router.params.afterSaleNo });
          if (resp.code === 0) {
            Taro.showToast({ icon: 'none', title: '撤销成功' });
          }
        }
      }
    });
  };

  // 寄回接口
  const sendBack = async () => {
    //售后单号   物流公司id  物流单号
    let payload = {
      afterSaleNo: router.params.afterSaleNo,
      expressCompanyId: expressCompanyId,
      expressNo: expressNo
    };
    let res = await rebackProduct(payload);
    if (res.code === 0) {
      console.log('成功');
    }
  };

  //售后单状态（-1:无效订单，0:待付款，1:待发货， 2:已发货，3:已完成, 4:已关闭，5:售后 6:退款中 7:已退款，
  //8:异常关闭, 9:该商品已过售后期， 61：平台审核， 62：寄回商品， 63：仓库收货, 64: 待退款审核）
  //申请进度
  const selectImg = () => {
    Taro.chooseImage({
      success: (info) => {
        let temp = [...imgs];
        let arr = temp.concat(info.tempFilePaths);
        setImgs(arr);
      }
    });
  };
  useEffect(() => {
    // orderI
    // 初始化调用 useRouter接受参数
    const getSteps = async () => {
      const res = await afterSaleDetail({ afterSaleNo: router.params.afterSaleNo });
      if (res.code === 0) {
        setProduct(res.data);
        let s = res.data.status;
        if (s === 7) {
          setsteps(4);
        } else if (s === 62) {
          setsteps(2);
        } else if (s === 63) {
          setsteps(3);
        } else if (s === 61) {
          setreaStr(res.data.reason);
          setReason(res.data.note);
          setImgs(res.data.certificatePicsArray);
          setsteps(1);
        } else {
          setsteps(0);
          fetchData();
        }
      }
    };
    //原因选择
    const fetchData = async () => {
      const result = await returnsReasons('');
      if (result.code === 0) {
        setReasonInfo(result.data.reasons);
      }
    };

    if (router.params.afterSaleNo) {
      // 从申请记录过来
      getSteps();
    } else {
      //从申请售后过来  获取商品信息
      setProduct(Taro.getStorageSync('product'));
      fetchData();
    }
  }, []);
  let list: any[] = [];
  if (product && product.products) {
    product.products.map((item) => {
      list.push(item);
    });
  }
  let list1: any[] = [];
  if (product && product.certificatePicsArray) {
    product.certificatePicsArray.map((item) => {
      list1.push(item);
    });
  }
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <Bar title="提交申请" />
          <View className="utp-customerService-steps">
            <Wrap type={4}>
              {['提交', '审核', '寄回', '收货', '完成'].map((item, index) => {
                return (
                  <View key={index} className="utp-customerService-steps-tips">
                    <Wrap type={2}>
                      <Text
                        className={
                          steps === index
                            ? 'utp-customerService-steps-title-active'
                            : 'utp-customerService-steps-title'
                        }
                      >
                        {item}
                      </Text>
                      <Img src={right} width={44} />
                    </Wrap>
                  </View>
                );
              })}
            </Wrap>
          </View>

          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page"
            scrollY
          >
            {steps === 0 ? (
              <View className="utp-customerService-infoBox">
                <Wrap type={2} flexDirection="column">
                  <View className="utp-customerService-goodsInfos">
                    {list.map((i, ii) => {
                      return (
                        <View className="utp-customerService-goodsInfo" key={ii}>
                          <Wrap>
                            <Img src={i.picIds || i.picUrl} width={170} />
                            <View className="utp-customerService-goodsInfo-box">
                              <Text className="utp-customerService-goodsInfo-names">{i.name}</Text>
                              <Text className="utp-customerService-goodsInfo-norms">
                                {i.skuInfo || i.norm1Value}
                              </Text>
                              <Wrap justifyContent="space-between" Myheight={40}>
                                <View className="utp-customerService-goodsInfo-price">
                                  <Yuan price={i.price} size={28} color="#FF2F7B"></Yuan>
                                </View>
                                <Text className="utp-customerService-goodsInfo-num">
                                  数量：{i.num}
                                </Text>
                              </Wrap>
                            </View>
                          </Wrap>
                        </View>
                      );
                    })}
                  </View>
                  <View className="utp-customerService-number">
                    <Wrap type={1} justifyContent="space-between" flexWrap="nowrap">
                      <Text className="utp-customerService-number-serviceNum">申请数量</Text>
                      <Stepper num={product.goodsNum} />
                    </Wrap>
                  </View>
                  <View className="utp-customerService-reason">
                    <Wrap type={1} justifyContent="space-between">
                      <View className="utp-customerService-reason-serviceReason">退货原因</View>
                      <View
                        className="utp-customerService-reason-tips"
                        onClick={() => {
                          if (steps === 0) {
                            setopenReason(true);
                          }
                        }}
                      >
                        <Wrap>
                          {steps === 0 ? (
                            <Txt title={reasonActive ? reaStr : '请选择退货原因'} />
                          ) : (
                            <Txt title={reaStr} />
                          )}
                          <Img src={right} width={44} />
                        </Wrap>
                      </View>
                    </Wrap>
                  </View>
                  <View className="utp-customerService-explain">
                    <View className="utp-customerService-explain-explainContent">
                      <Text style={{ fontSize: 14, color: '#333' }}>补充说明</Text>
                      {steps === 0 ? (
                        <Textarea
                          className="utp-customerService-explain-explainContent-text"
                          value={reason}
                          maxlength={500}
                          placeholder="请输入您的补充说明"
                          onInput={(e) => {
                            setReason(e.detail.value);
                          }}
                        />
                      ) : (
                        <Text className="utp-customerService-explain-explainContent-text">
                          {reason}
                        </Text>
                      )}
                      <Text className="utp-customerService-explain-explainContent-numLength">
                        {reason.length}/500
                      </Text>
                    </View>
                  </View>
                  <View className="utp-customerService-voucher">
                    <Text style={{ fontSize: 14, color: '#333' }}>上传凭证</Text>
                    <View className="utp-customerService-voucher-voucherImg">
                      <Wrap type={2} top>
                        {imgs.length > 0 &&
                          imgs.length < 5 &&
                          imgs.map((o, oo) => {
                            return (
                              <View key={oo} className="utp-customerService-voucher-img">
                                <Img src={o} width={220} />
                              </View>
                            );
                          })}

                        {steps === 0 && imgs.length < 4 && (
                          <Img src={icon_addphoto} width={220} onClick={selectImg}></Img>
                        )}
                      </Wrap>
                    </View>
                    <Txt title="支持jpg png bmp格式，最多上传4张" justifyContent="flex-start" />
                  </View>
                  {steps === 0 && (
                    <View className="utp-customerService-submit" onClick={submit}>
                      <Bton>提交审核</Bton>
                    </View>
                  )}
                </Wrap>
              </View>
            ) : (
              <View className="utp-customerService-infoBox2">
                <Wrap type={2} flexDirection="column">
                  {/* 3  可以填写 4 5 */}
                  {[2, 3].includes(steps) && (
                    <View className="utp-customerService-logisticsBox">
                      <View className="utp-customerService-logistics">
                        <Wrap type={1} justifyContent="flex-start">
                          <View className="utp-customerService-logistics-text">物流公司</View>

                          {steps === 2 ? (
                            <View className="utp-customerService-logistics-text-1">
                              <Input
                                placeholder="请输入物流公司"
                                value={expressCompanyId}
                                onInput={(e) => {
                                  setExpressCompanyId(e.detail.value);
                                }}
                              />
                            </View>
                          ) : (
                            <View className="utp-customerService-logistics-text-1">
                              {product && product.name}
                            </View>
                          )}
                        </Wrap>
                      </View>
                      <View className="utp-customerService-logistics">
                        <Wrap type={1} justifyContent="flex-start">
                          <View className="utp-customerService-logistics-text">物流单号</View>
                          {steps === 2 ? (
                            <View className="utp-customerService-logistics-text-1">
                              <Input
                                placeholder="请输入物流单号"
                                value={expressNo}
                                onInput={(e) => {
                                  setExpressNo(e.detail.value);
                                }}
                              />
                            </View>
                          ) : (
                            <View className="utp-customerService-logistics-text-1">
                              {product && product.expressNo}
                            </View>
                          )}
                        </Wrap>
                      </View>
                      {steps === 2 && (
                        <View className="utp-customerService-logistics utp-customerService-address">
                          <Input
                            className="utp-customerService-logistics-text"
                            placeholder="退货地址:请联系店铺客服"
                            value={address}
                            onInput={(e) => {
                              setAddress(e.detail.value);
                            }}
                          />
                        </View>
                      )}
                    </View>
                  )}
                  {list.map((o, oo) => {
                    return (
                      <View className="utp-customerService-goodsInfo" key={oo}>
                        <Wrap>
                          <Img src={o.picIds || o.picUrl} width={170} />
                          <View className="utp-customerService-goodsInfo-box">
                            <Text className="utp-customerService-goodsInfo-names">{o.name}</Text>
                            <Text className="utp-customerService-goodsInfo-norms">
                              {o.skuInfo || o.norm1Value}
                            </Text>
                            <Wrap justifyContent="space-between" Myheight={40}>
                              <View className="utp-customerService-goodsInfo-price">
                                <Yuan price={o.price} color="#FF2F7B" size={28}></Yuan>
                              </View>
                              <Text className="utp-customerService-goodsInfo-num">
                                数量：{o.num}
                              </Text>
                            </Wrap>
                          </View>
                        </Wrap>
                      </View>
                    );
                  })}
                  {[
                    {
                      title: '售后编号',
                      text: product && product.afterSaleNo
                    },
                    {
                      title: '申请时间',
                      text: product && product.createTime
                    },
                    {
                      title: '支付方式',
                      text: product.payWay
                    },
                    {
                      title: '退货原因',
                      text: product && product.reason
                    }
                  ].map((item, oo) => {
                    return (
                      <View className="utp-customerService-logistics" key={oo}>
                        <Wrap type={1} justifyContent="space-between">
                          <View className="utp-customerService-logistics-text">{item.title}</View>
                          <View className="utp-customerService-logistics-text-1">{item.text}</View>
                        </Wrap>
                      </View>
                    );
                  })}

                  <View className="utp-customerService-explain">
                    <View className="utp-customerService-explain-explainContent">
                      <Text style={{ fontSize: 14, color: '#333' }}>补充说明</Text>
                      <Text className="utp-customerService-explain-explainContent-text">
                        {product && product.note}
                      </Text>
                      <Text className="utp-customerService-explain-explainContent-numLength">
                        {product.note.length}/500
                      </Text>
                    </View>
                  </View>
                  <View className="utp-customerService-voucher">
                    <Text style={{ fontSize: 14, color: '#333' }}>上传凭证</Text>
                    <View className="utp-customerService-voucher-voucherImg">
                      <Wrap type={2} top>
                        {list1.map((i, ii) => {
                          return (
                            <View key={ii} className="utp-customerService-voucher-img">
                              <Img src={i} width={220} />
                            </View>
                          );
                        })}
                      </Wrap>
                    </View>
                    <Txt title="支持jpg png bmp格式，最多上传4张" justifyContent="flex-start" />
                  </View>
                  <View
                    className="utp-customerService-bottomBtnGroup"
                    style={{
                      display: [1, 2].includes(steps) ? 'block' : 'none'
                    }}
                  >
                    {steps === 1 && (
                      <Wrap justifyContent="center" Myheight={200}>
                        <View className="utp-customerService-bottomBtn" onClick={withdraw}>
                          <Txt title="撤回申请" color="deep" size={28} height={100} />
                        </View>
                      </Wrap>
                    )}
                    {steps === 2 && (
                      <Wrap justifyContent="center" Myheight={200}>
                        <View className="utp-customerService-bottomBtn" onClick={withdraw}>
                          <Txt title="撤回申请" color="deep" size={28} height={100} />
                        </View>
                        <View
                          className="utp-customerService-bottomBtn utp-customerService-active"
                          onClick={sendBack}
                        >
                          <Txt title="提交" color="white" size={28} height={100} />
                        </View>
                      </Wrap>
                    )}
                  </View>
                </Wrap>
              </View>
            )}
          </ScrollView>
          <View
            className={
              openReason
                ? 'utp-customerService-hideReason'
                : 'utp-customerService-hideReason-hidden'
            }
            onClick={() => {
              setopenReason(false);
            }}
          >
            <View className="utp-customerService-hideReason-chooseReason">
              <ScrollView
                refresherEnabled
                refresherBackground="#333"
                className="utp-bd utp-bd-page"
                scrollY
              >
                <Wrap type={4} flexDirection="column">
                  {reasonInfo &&
                    reasonInfo.map((item, inm) => {
                      return (
                        <Text
                          className={
                            reasonActive === item.id
                              ? 'utp-customerService-hideReason-chooseItem-active'
                              : 'utp-customerService-hideReason-chooseItem'
                          }
                          onClick={() => {
                            setreasonActive(item.id);
                            setreaStr(item.reason);
                            setopenReason(false);
                          }}
                          key={inm}
                        >
                          {item && item.reason}
                        </Text>
                      );
                    })}
                </Wrap>
              </ScrollView>
            </View>
          </View>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};
export default Index;
