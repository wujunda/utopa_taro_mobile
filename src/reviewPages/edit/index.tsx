import Taro, { useEffect, useState } from '@tarojs/taro';
import { useParentIFrame } from './useParentIFrame';
import { MatchComponent } from '../components';
import BaseContainer from '../components/baseContainer';

import { ComponentData, ComStyle } from '../interface';

interface Props {}

const sendMsgActions = {
  changeCurrent: (uid: string) => ({ type: 'changeCurrent', uid }),
  init: () => ({ type: 'init' })
};

const Edit: Taro.FC<Props> = (props) => {
  const [componentList, setComponentList] = useState<ComponentData<object>[]>([]);
  const [currentUid, setCurrentUid] = useState<string>('');
  const [isEditor, setEditor] = useState(true);
  const [, reRender] = useState();
  const parentIFrame = useParentIFrame({
    onMessage(msg: any) {
      // console.log('onMessage: ', msg);
      if (msg.type === 'changeComList') {
        // 改变组件列表
        setComponentList(Array.isArray(msg.data) ? msg.data : []);
      } else if (msg.type === 'changeCurrentCom') {
        // 改变当前编辑的组件
        setCurrentUid(msg.data.uid || null);
      } else if (msg.type === 'render') {
        // 强制渲染
        reRender(null);
      } else if (msg.type === 'changeEditor') {
        // 改变编辑状态
        setEditor(msg.data.editor);
      } else {
        // console.log('收到消息：', msg);
      }
    },
    onInit(parent) {
      if (parent) {
        // console.log('初始化iframe');
        parent.sendMessage(sendMsgActions.init());
      }
    }
  });

  return (
    <view style={{ backgroundColor: '#f8f8f8' }}>
      {componentList.map((item) => {
        const comStyle: ComStyle = item.style || {};
        return (
          <BaseContainer
            key={item.uid}
            style={comStyle}
            isEditor={isEditor && currentUid === item.uid}
          >
            <MatchComponent uid={item.uid} type={item.code} data={item.data} childs={item.childs} />
          </BaseContainer>
        );
      })}
    </view>
  );
};

export default Edit;
