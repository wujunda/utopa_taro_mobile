/* eslint-disable import/first */
/* eslint-disable no-shadow */
import Taro, { useRouter, useState } from '@tarojs/taro';
import { View, Image, Text, ScrollView } from '@tarojs/components';
import { COMMON_IMAGES } from '../../common/images';
import Wrap from '@/components/Wrap';
// eslint-disable-next-line import/first
import '../index.scss';
import Img from '@/components/Img';
import { priceToFixedZore } from '@/utils/utils';

const renderViewLineScroll = (props) => {
  const { left, getWidth } = props;
  return Taro.useMemo(() => {
    return (
      getWidth && (
        <View className="scroll-tag-box" style={{ marginTop: Taro.pxTransform(20) }}>
          <View
            className="bar"
            style={
              {
                left: Taro.pxTransform(Number(left)),
                width: Number(getWidth) + '%'
              } as any
            }
          ></View>
        </View>
      )
    );
  }, [left, getWidth]);
};
//商品项
const ViewGoodsSwiperItem = (props) => {
  // eslint-disable-next-line no-unused-vars
  const { list, onClick, onMore, title } = props;
  const [scrollLeft, setScrollLeft] = useState<Number>(0);
  const getWidth = ((160 * 5) / (list && list.length * 160)) * 100;
  return (
    list && (
      <View className="goods-swiper" style={{ marginTop: Taro.pxTransform(30) }}>
        <View>
          <View className="goods-swiper-head">
            <Wrap type={1} flexDirection="row" top justifyContent="space-between">
              <View>
                <Wrap type={2} flexDirection="row" top>
                  <View className="more-title">{title}</View>
                  <View
                    style={{ marginLeft: Taro.pxTransform(15), marginTop: Taro.pxTransform(-8) }}
                  ></View>
                </Wrap>
              </View>
              <View
                className="more-text"
                onClick={() => {
                  onMore && onMore();
                }}
              >
                <Wrap type={2}>
                  <Text style={{ marginRight: Taro.pxTransform(5) }}>更多 </Text>
                  <Img
                    src={COMMON_IMAGES.right_more_666}
                    width={Taro.pxTransform(24)}
                    height={Taro.pxTransform(24)}
                  ></Img>
                </Wrap>
              </View>
            </Wrap>
          </View>
          <View>
            <ScrollView
              className="scroll-tab"
              scrollX
              scrollWithAnimation
              lowerThreshold={10}
              upperThreshold={10}
              onScroll={(e: any) => {
                setScrollLeft(e.detail.scrollLeft / list.length);
              }}
            >
              {list &&
                list.map((d, idx) => {
                  return (
                    <View key={d.productId} onClick={() => onClick && onClick(d)}>
                      <View
                        className="scroll-tab-item"
                        style={{ marginLeft: idx === 0 ? Taro.pxTransform(20) : '' }}
                      >
                        <View className="item-img">
                          <Img src={d.productImage} width={156}></Img>
                        </View>
                        <View className="item-price">
                          <Wrap type={2} flexDirection="row" top>
                            <View>
                              <Text
                                className="new-price"
                                style={{ fontSize: Taro.pxTransform(20) }}
                              >
                                ¥
                              </Text>
                              <Text className="new-price">{priceToFixedZore(d.salePrice, 2)}</Text>
                            </View>
                            <Text className="old-price">¥{priceToFixedZore(d.originPrice, 2)}</Text>
                          </Wrap>
                        </View>
                      </View>
                    </View>
                  );
                })}
            </ScrollView>
          </View>
          {renderViewLineScroll({ left: scrollLeft, getWidth: Math.ceil(getWidth + list.length) })}
        </View>
      </View>
    )
  );
};

export default ViewGoodsSwiperItem;
