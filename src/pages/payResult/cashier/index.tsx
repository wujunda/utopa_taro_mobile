import Taro, { useState, useRouter } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View, Text, Image } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import Safe from '../../../components/safe';
import Img from '../../../components/Img';
import Bton from '../../../components/Bton';
import Wrap from '../../../components/Wrap';
import Txt from '../../../components/Txt';
import Message from '../../../components/Message';
//import apipay from '../../../assets/pay/appPay.png';
//import applepay from '../../../assets/pay/applepay.png';
//import cloudpay from '../../../assets/pay/cloudpay.png';
import wepay from '../../../assets/pay/wepay.png';
import check from '../../../assets/cart/check.png';
import uncheck from '../../../assets/submit/uncheck.png';
import './index.scss';
import Router from '../../../utils/router';
import { getMiniPay } from '../../../services/submit';
import { images } from '../../../images';

function Index() {
  const [inn, setInn] = useState(0);
  const [show, setShow] = useState(false);

  const router = useRouter();
  const id = router.params.id;
  const money = router.params.money;
  const isScan = router.params.isScan === '1';
  const parentNo = router.params.parentNo;
  const payOrderNo = router.params.payOrderNo;
  console.log('订单id');
  console.log(id);
  const goScan = () => {
    Taro.redirectTo({
      url: '/pages/packOrder/config/index?id=' + id
    });
  };
  const onMiniPay = async () => {
    let data = await getMiniPay({
      openId: Taro.getStorageSync('openId'),
      payOrderNo: payOrderNo
    });
    if (data.code === 0) {
      Taro.requestPayment({
        timeStamp: data.data.payInfo.timeStamp,
        nonceStr: data.data.payInfo.nonceStr,
        package: data.data.payInfo.extension,
        signType: 'MD5',
        paySign: data.data.payInfo.sign,
        success: (res) => {
          if (isScan) {
            goScan();
          } else {
            Router.goPayRstReplace(id, parentNo);
          }
        },
        fail: (res) => {}
      });
    } else {
      Taro.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
  };
  return (
    <Safe>
      <View className="utp-cashier">
        <View className="utp-cashier-price">
          <Txt title="待支付金额" size={30} color="low" height={42} />
          <Wrap type={4}>
            <Txt title="¥" size={40} color="deep" height={56} />
            <Txt title={money} size={70} color="deep" height={98} />
          </Wrap>
        </View>
        {[
          { url: wepay, title: '微信支付' }
          //{ url: apipay, title: '支付宝支付' },
          //{ url: cloudpay, title: '云闪付' },
          //{ url: applepay, title: 'Apple Pay' }
        ].map((item, index) => {
          return (
            <View
              className="utp-cashier-method"
              key={index}
              onClick={() => {
                setInn(index);
              }}
            >
              <Wrap type={2}>
                <Img src={item.url} width={100} />
                <Txt size={32} bold color="deep" title={item.title} />
              </Wrap>
              <Img src={index === inn ? check : uncheck} width={36} />
            </View>
          );
        })}
        {false && <Image src={images.big_text_banner} className="utp-cashier-adv" />}
        <View
          className="utp-cashier-btn"
          onClick={() => {
            //Router.goPayRstReplace(id, parentNo);
            //return;
            onMiniPay();
          }}
        >
          <Bton> 确认支付</Bton>
        </View>
        <Message
          title="标签"
          show={show}
          type={1}
          onClose={() => {
            setShow(false);
            Taro.navigateTo({ url: '/pages/packOrder/order/index' });
          }}
          onConfirm={() => {
            Taro.navigateTo({ url: '/pages/payResult/result/index' });
          }}
        >
          <Text>678</Text>
        </Message>
      </View>
    </Safe>
  );
}

Index.config = {
  navigationBarTitleText: '收银台',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};
// @ts-ignore
export default connect((state) => state)(Index);
