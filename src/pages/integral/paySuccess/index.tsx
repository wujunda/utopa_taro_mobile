
import { View, Image } from '@tarojs/components';
import Taro, { useRouter, useEffect } from '@tarojs/taro';
import { connect } from '@tarojs/redux';
import JSBridge from '../../../utils/jsbridge/index';
import successIcon from '../../../assets/pay/success.png'

import './index.scss';

// interface IProps {}
function Success(props) {
  // 去除原生的分享按钮
  // if (props.user.inApp) {
    useEffect(() => {
      if (props.user.inApp) {
        let param = {
          show: '0'
        }
        JSBridge.Common.GTBridge_Common_Base_showShareBtn(param, res => {})
      }
    }, [])
  // }
  
  const router = useRouter()
  const orderDetail = () => {
    if (props && props.user.inApp) {
      const query = {
        showType: '0',
        isScoreOrder: 1,
        isPoint: true
      };
      const cmdParams = {
        cmd: 'C0020050001',
        params: JSON.stringify(query)
      };
      JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, res => {});
    }
  }
  const continueExchange = () => {
    if (props && props.user.inApp) {
      const cmdParams = {
        cmd: 'C0050303'
      };
      JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, res => {});
    } else {
      Taro.navigateTo({
        url: '/pages/integral/list/index'
      })
    }
  }
  return (
    <View className="utp-integral-success">
      <Image src={successIcon} className="utp-integral-success-icon"/>
      <View>兑换成功</View>
      <View className="utp-integral-success-deduct">系统已自动扣除{router.params.point}积分</View>
      {
        props && props.user && props.user.inApp ?
        <View className="utp-integral-success-button"> 
          <View className="utp-integral-success-button-left" onClick={() => orderDetail()}>查看订单</View>
          <View className="utp-integral-success-button-right" onClick={
            () => continueExchange()
          }>继续兑换</View>
        </View> :
        <View className="utp-integral-success-outApp">
          <View className="utp-integral-success-outApp-right" onClick={
              () => continueExchange()
            }>继续兑换</View>
        </View>
      }
    </View>
  );
}

Success.config = {
  navigationBarTitleText: '支付结果',
  disableScroll: false
};
// @ts-ignore
export default connect((state) => state)(Success);