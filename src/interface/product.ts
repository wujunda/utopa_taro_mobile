type ID = number | string;

// 商品的基础信息
export interface ProductBaiscData {
  id: ID;

  // skuid
  skuId?: ID;

  // sku编码
  skuCode?: string;

  // 商品编码
  productCode?: string;
  productId: string;

  // 店铺id
  storeId: ID;
  storeName: string;

  // 商家id
  businessId: ID;

  // 商品名称
  productName: string;

  // 商品图片
  
  productImage: string;
  storeTemplateCode:string;
  // 原价
  selfSupport:number;
  activeLabel:string;
  saleStatus:number; 
  // { status: '', text: '全部' },
  // { status: 0, text: '待提交' },
  // { status: 1, text: '销售中' },
  // { status: 2, text: '待审核' },
  // { status: 3, text: '审核不通过' },
  // { status: 4, text: '已售罄' },
  // { status: 5, text: '已下架' },
  // { status: 6, text: '已结束' }
  // 销售价格
  originPrice?: number;
  salePrice?: number;
  picUrl?:string;
  name?:string;
  price?: number;
  originalPrice?: number;
  skuPrice?: number;
  skuImgs?:string[];
}

// 商品的店铺信息
export interface ProductStoreInfo {
  storeId: ID;

  storeName: string;

  storeCode: string;
  logoUrl: string;
  productList: ProductBaiscData[];
  storeMainCategory: string;
  telephone: string;
  businessLogoUrl: string;
  selfSupport:number;
  logisticsModel:string[];
  couponBatchDtoDataName:string[];
  businessId:number;
  storeTemplateCode: string;
  isFavorite:number;
  coupons:any[];
  labels:any[];
}
