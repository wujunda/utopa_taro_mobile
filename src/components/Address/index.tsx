import './index.scss';
import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import Img from '../Img';
import Txt from '../Txt';
import Wrap from '../Wrap';
import Cnt from '../Cnt';
import Tag from '../Tag';
import { IAddressData } from '../../services/address';
import { images } from '../../images';

interface IProps {
  type?: 1;
  perfect?: boolean;
  item?: IAddressData;
  onEdit: () => void;
}
const Index: Taro.FC<IProps> = ({ onEdit, item, type, perfect = false }) => {
  return (
    <View className={'utp-address utp-solid' + (type === 1 ? ' utp-address-1' : '')}>
      {!perfect ? (
        <View className="utp-address-hd">
          <Wrap type={1} Myheight={176}>
            <View>
              <View className="utp-address-hd-name">
                <Txt title="kkkkkkkkk某" size={30} color="deep" bold dian />
              </View>
              <View className="utp-address-hd-status">
                <Cnt>
                  <Txt title="待完善" size={22} color="red" />
                </Cnt>
              </View>
            </View>
            <View className="utp-address-hd-content">
              <Wrap flexDirection="column" top>
                <Wrap>
                  <View className="utp-address-hd-phones">
                    <Txt title="138****9898" size={30} color="deep" bold />
                  </View>
                  {type !== 1 && (
                    <View className="utp-address-hd-tags">
                      <Tag type={3}>默认</Tag>
                    </View>
                  )}
                  {type !== 1 && (
                    <View className="utp-address-hd-tags">
                      <Tag type={31}>公司</Tag>
                    </View>
                  )}
                </Wrap>
                <Wrap>
                  <Text className="utp-address-tip">
                    里哪里哪里里哪里哪哪哪哪里哪里里哪里哪哪哪里哪里里哪里哪哪哪里哪里里哪里哪哪里
                  </Text>
                  <View className="utp-address-icos">
                    <Img src={images.edit} width={44} />
                  </View>
                </Wrap>
              </Wrap>
            </View>
          </Wrap>
        </View>
      ) : (
        <View className="utp-address-hd">
          {!!item && (
            <View>
              <Wrap flexDirection="column" top>
                <Wrap>
                  <View style={{ width: Taro.pxTransform(120) }}>
                    <Wrap>
                      <Txt title={item.userName} dian={true} size={30} color="deep" bold />
                    </Wrap>
                  </View>
                  <View className="utp-address-hd-phones" style={{ marginLeft: 18 }}>
                    <Txt title={item.phone} size={30} color="deep" bold />
                  </View>
                  {item.isDefault === 1 && (
                    <View className="utp-address-hd-tags">
                      <Tag type={3}>默认</Tag>
                    </View>
                  )}
                  {!!item.addressType && (
                    <View className="utp-address-hd-tags">
                      <Tag type={31}>{item.addressType}</Tag>
                    </View>
                  )}
                </Wrap>
                <Wrap>
                  <Text className="utp-address-tip utp-address-tipPre">{item.address}</Text>
                  <View
                    className="utp-address-icos"
                    onClick={(e) => {
                      e.stopPropagation;
                      if (onEdit) {
                        onEdit();
                      }
                    }}
                  >
                    <Img src={images.edit} width={44} />
                  </View>
                </Wrap>
              </Wrap>
            </View>
          )}
        </View>
      )}
    </View>
  );
};
Index.defaultProps = {};

export default Index;
