import Taro from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import useStore from '../../../components/useNum';

import Wrap from '../../../components/Wrap';
import Tag from '../../../components/Tag';
import Txt from '../../../components/Txt';
import Img from '../../../components/Img';
import Cnt from '../../../components/Cnt';
import Box1 from '../../../components/Box1';
import MsgCenter from '../../../components/MsgCenter';
import './index.scss';
import ItemMsg from '../../../components/ItemMsg';
import close from '../../../assets/msg/close.png';
import { IMenu, getMsgMenu, IMsgMenu } from '../../../services/msg';
import useRequest from '../../../hooks/useRequest';
import Router from '../../../utils/router';

function Index(props) {
  let num = useStore({ name: '' })[0];
  console.log('jjjjj');
  console.log(num);
  console.log('props');
  console.log(props);
  const [state, update, loading] = useRequest<IMsgMenu>('name=a&age=1', getMsgMenu);
  console.log('更多');
  console.log(state);
  console.log(loading);
  console.log(update);
  let list: IMenu[] = [];
  if (state && state.code === 0) {
    list = state.data.records;
  }

  return (
    <View className="utp-msg">
      {false && (
        <View className="utp-msg-msg">
          <Wrap type={1}>
            <Txt title="开启推送通知，掌握最新消息。" color="active" />
            <Wrap>
              <Tag type={2}>去开启</Tag>
              <View className="utp-msg-close">
                <Img src={close} width={20} />
              </View>
            </Wrap>
          </Wrap>
        </View>
      )}
      <View className="utp-msg-icos">
        <Wrap>
          {list.map((item) => {
            return (
              <View
                key={item.id}
                style={{ flex: 1 }}
                onClick={() => {
                  //Taro.navigateTo({ url: '/pages/message/msgNav/index' });
                  Router.goMsgInfo(item.name, item.id);
                }}
              >
                <Cnt>
                  <ItemMsg tips={item.name} src={item.imgUri} num={item.unreadCount} />
                </Cnt>
              </View>
            );
          })}
        </Wrap>
      </View>
      {/* <Box1>
        <MsgCenter></MsgCenter>
        <MsgCenter></MsgCenter>
        <MsgCenter></MsgCenter>
      </Box1> */}
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '消息',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};
// @ts-ignore
export default connect((state) => state)(Index);
