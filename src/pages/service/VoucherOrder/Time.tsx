import Taro from '@tarojs/taro';
import Txt from '../../../components/Txt';
import useEndTime from '../../../hooks/useEndTime';

interface IProps {
  sec: number;
}
const Index: Taro.FC<IProps> = ({ sec }) => {
  function getMinutes(s: number) {
    let tmp = (Number(Math.floor(s / 60)) % 60).toString();
    return getZero(tmp);
  }

  function getSecond(s: number) {
    let tmp = (s % 60).toString();
    return getZero(tmp);
  }
  function getZero(i: number | string) {
    if (Number(i) < 10) {
      return '0' + i;
    } else {
      return i;
    }
  }

  const [s] = useEndTime(sec);
  return (
    <Txt
      title={`剩余支付时间：${sec == 0 ? '00' : getMinutes(s)}:${sec == 0 ? '00' : getSecond(s)}`}
      color="red"
    />
  );
};
Index.defaultProps = {};

export default Index;
