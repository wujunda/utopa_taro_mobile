import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

interface Props {
  type?: 0 | 1 | 2;
  children: any;
}
const Index: Taro.FC<Props> = ({ children, type = 0 }) => {
  return (
    <View
      style={{
        paddingLeft: Taro.pxTransform(type === 0 ? 24 : type === 2 ? 20 : 0),
        paddingRight: Taro.pxTransform(type === 2 ? 20 : 0),
        backgroundColor: '#ffffff',
        overflow: 'hidden'
      }}
      className="utp-div"
    >
      <View
        style={{
          marginBottom: Taro.pxTransform(-2)
        }}
      >
        {children}
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
