import Taro from '@tarojs/taro';
import { View, Image } from '@tarojs/components';

import './index.scss';
import Img from '../Img';
import Txt from '../Txt';
import Cnt from '../Cnt';
import Wrap from '../Wrap';
import Tag from '../Tag';
import { ICouponItem } from '../../services/coupons';
import { images } from '../../images';
import { IQuan, IVoucher, getVoucher } from '../../services/voucher';
import { Glo, parseTime } from '../../utils/utils';

interface IProps {
  item: IQuan;
}
const Index: Taro.FC<IProps> = ({ item }) => {
  return (
    <View
      className="utp-card1"
      onClick={(e) => {
        e.stopPropagation;
        Taro.navigateTo({
          url: '/pages/service/VoucherOrder/index?id=' + item.orderNo
        });
      }}
    >
      <View className="utp-card1-bds">
        <View className="utp-card1-bd">
          <View className="utp-card1-bd-imgs">
            <Img src={item && item.logo} width={144} />
          </View>
          <View className="utp-card1-bd-box">
            <View>
              <Wrap>
                <Txt title={item && item.name} color="black" size={30} height={58} />
              </Wrap>
            </View>
            <View>
              <Wrap>
                <Txt title={`数量：${item && item.goodsNum}`} color="low" size={26} height={50} />
              </Wrap>
            </View>
            <View>
              <Wrap>
                <Txt
                  title={`总价：¥ ${item && item.orderAmount ? item.orderAmount : 0}`}
                  color="low"
                  size={26}
                  height={50}
                />
              </Wrap>
            </View>
            <View className="utp-card1-bd-box-tips">
              <Txt title={ item && item.statusDesc} color="red" />
            </View>
          </View>
        </View>
      </View>
      <View className="utp-card1-ft">
        <Wrap type={1} justifyContent="space-between">
          <Txt title={parseTime(item && item.placeOrderTime)} size={26} color="low" height={86} />
          {item && item.statusCode !== -3 && (
            <View
              className="utp-card1-ft-btn utp-cnt"
              onClick={() => {
                if (item && item.statusCode === 0) {
                  // 去付款
                } else {
                  // 去使用
                }
              }}
            >
              <Txt title={item && item.statusCode === 0 ? '立即付款' : '去使用'} color="red" />
            </View>
          )}
        </Wrap>
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
