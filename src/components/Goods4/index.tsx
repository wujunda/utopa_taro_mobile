import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';

import './index.scss';
import Tag from '../Tag';
import Img from '../Img';
import Wrap from '../Wrap';
import Txt from '../Txt';
import add from '../../assets/cart/add1.png';
import { getCodeAddGoods, ICodeGoods } from '../../services/scan';
import { Glo } from '../../utils/utils';

type PageStateProps = {
  // counter: {
  //   num: number;
  // };
  item: ICodeGoods;
};

type PageDispatchProps = {
  // add: () => void;
  // dec: () => void;
  // asyncAdd: () => any;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = (props) => {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  const onAddGoods = async () => {
    let data = await getCodeAddGoods({
      businessId: props.item.businessId,
      clientType: 1,
      number: 1,
      skuId: props.item.skuId,
      sourceChannel: '',
      storeId: props.item.storeId
    });
    if (data.code === 0) {
      Taro.redirectTo({
        url: '/pages/scan/scanCodeCart/index'
      });
    }
  };
  return (
    <View className="utp-goodscan">
      <Wrap Myheight={100} justifyContent="space-between">
        <Wrap>
          <Tag margin={8}>扫码自取</Tag>
          {
            !!props.item &&  !!props.item.storeName &&(
              <Txt title={props.item.storeName} color="deep" size={30} bold />
            )
          }
        </Wrap>
        <Txt title="7.9km" color="low" size={24} />
      </Wrap>
      <View
        className="utp-goodscan-box utp-div"
        onClick={(e) => {
          e.stopPropagation;
          //Taro.navigateTo({
          //url: '/pages/details/index'
          //});
        }}
      >
        <View className="utp-goodscan-i utp-cnt">
          <Image className="utp-goodscan-img" src={props.item.productSkuViewDto.imgKey} />
        </View>
        <View style={{ flex: 1, height: '100%' }}>
          <Wrap flexDirection="column" type={3} justifyContent="space-between">
            <View className="utp-goodscan-titles">
              <Text className="utp-goodscan-title taro-text">
                {'\t'}
                {props.item.productName}
              </Text>
            </View>
            <View
              className="utp-goodscan-pricebox"
              onClick={(e) => {
                e.stopPropagation;
                onAddGoods();
              }}
            >
              <Wrap flexDirection="row" justifyContent="space-between">
                {
                  !!props.item &&!!props.item.price &&  (
                    <Txt title={'¥' + Glo.formatPrice(props.item.price)} color="red" size={30} />
                  )
                }
                <Img src={add} />
              </Wrap>
            </View>
          </Wrap>
        </View>
      </View>
    </View>
  );
};

Index.defaultProps = {};
export default Index;
