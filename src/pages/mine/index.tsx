import Taro, { useDidShow } from '@tarojs/taro';
import { Text, View, Image } from '@tarojs/components';
import { connect } from '@tarojs/redux';

import './index.scss';
import more from '../../assets/more.png';
import msg from '../../assets/msg.png';
import Wrap from '../../components/Wrap';
import Box from '../../components/Box';
import Img from '../../components/Img';
import Txt from '../../components/Txt';
import ItemMineIcon from '../../components/ItemMineIcon';
import ItemMineNum from '../../components/ItemMineNum';
import useRequest from '../../hooks/useRequest';
import personal_pic_portrait from '../../assets/mine/personal_pic_portrait.png';
import Router from '../../utils/router';
import { IUser, IUserData, getUser, getUserOrderCount } from '../../services/user';
import { images } from '../../images';

interface Props {
  isLogin: any;
  closeWidth: number;
  statusHeight: number;
  unread: number;
}
const Index: Taro.FC<Props> = (props) => {
  const [state, update] = useRequest<IUser>('needLogin', getUser);
  const [state2, update2] = useRequest<IUser>('needLogin', getUserOrderCount);
  useDidShow(() => {
    if (props.isLogin && update) {
      update();
    }
    if (props.isLogin && update2) {
      update2();
    }
  });

  let user!: IUserData;
  if (!!state && !!state.data) {
    user = state.data;
  }
  let count!: IUserData;
  if (!!state2 && !!state2.data) {
    count = state2.data;
  }
  return (
    <View className="utp-index4">
      <View className="utp-users">
        <View className="utp-users-a">
          <Image mode="widthFix" className="utp-users-i" src={images.newmine_bg} />
        </View>
        <View className="utp-user">
          <Wrap type={1}>
            <View
              className="utp-user-i utp-cnt"
              onClick={() => {
                if (!props.isLogin) {
                  Router.goLogin();
                } else {
                }
              }}
            >
              <Image className="utp-user-i-b" src={images.newmine_head} />
              {props.isLogin && !!user && (
                <Image
                  className="utp-user-ii"
                  src={user.avatarUrl === '' ? personal_pic_portrait : user.avatarUrl}
                />
              )}
              {!props.isLogin && <Image className="utp-user-ii" src={personal_pic_portrait} />}
              {false && (
                <View className="utp-user-i-1 utp-cnt">
                  <Img src={images.newmine_lan} width={128} height={62} />
                </View>
              )}
            </View>
            <View
              className="utp-names"
              onClick={() => {
                if (!props.isLogin) {
                  Router.goLogin();
                } else {
                }
              }}
            >
              {props.isLogin && <Text className="utp-name">{!!user ? user.nickname : ''}</Text>}
              {!props.isLogin && <Text className="utp-name">请登陆</Text>}

              {props.isLogin && (
                <View className="utp-flex">
                  <Text className="utp-phone">{!!user ? user.mobile : ''}</Text>
                </View>
              )}
            </View>

            <View
              className="utp-index4-set utp-flex utp-flex-db"
              style={{
                right: props.closeWidth + 'px'
              }}
            >
              <View
                className="utp-index4-set-ico utp-cnt"
                style={{ position: 'relative' }}
                onClick={() => {
                  if (!props.isLogin) {
                    Router.goLogin();
                  } else {
                    Taro.navigateTo({
                      url: '/pages/packOrder/msg/index'
                    });
                  }
                }}
              >
                <Image className="utp-index4-set-set1" src={images.newmine_msg} />
                {!!props.unread && (
                  <View
                    style={{
                      position: 'absolute',
                      zIndex: 1,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                      top: Taro.pxTransform(-4),
                      right: Taro.pxTransform(6),
                      left: Taro.pxTransform(30)
                    }}
                  >
                    <View
                      className="utp-div utp-solid"
                      style={{
                        height: Taro.pxTransform(32),
                        minWidth: Taro.pxTransform(32),
                        borderWidth: Taro.pxTransform(2),
                        borderColor: '#ff2f7b',
                        backgroundColor: '#ff2f7b',
                        borderRadius: Taro.pxTransform(32),
                        paddingLeft: Taro.pxTransform(5),
                        paddingRight: Taro.pxTransform(5),
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}
                    >
                      <Txt size={23} color="white" title={props.unread}></Txt>
                    </View>
                  </View>
                )}
              </View>
              <View
                className="utp-index4-set-ico utp-cnt"
                onClick={() => {
                  if (!props.isLogin) {
                    Router.goLogin();
                  } else {
                    Taro.navigateTo({
                      url: '/pages/person/setting/index'
                    });
                  }
                }}
              >
                <Image className="utp-index4-set-set" src={images.newmine_setting} />
              </View>
            </View>

            {false && props.isLogin && (
              <View className="utp-int">
                <View className="utp-int-a">
                  <Image className="utp-int-i" src={images.int} />
                </View>

                <View
                  className="utp-int-box"
                  onClick={() => {
                    Taro.navigateTo({ url: '/pages/service/integral/index' });
                  }}
                >
                  <Wrap type={1}>
                    <View className="utp-int-icos utp-cnt">
                      <Image className="utp-int-ico" src={images.icon_personal_jifen} />
                    </View>
                    <View className="utp-int-tips utp-cnt">
                      <View>
                        <Txt
                          size={28}
                          height={30}
                          color="active"
                          title={!!user ? user.memberPoints : 0}
                        ></Txt>
                        <Txt size={20} height={22} color="active" title="积分"></Txt>
                      </View>
                    </View>
                  </Wrap>
                </View>
              </View>
            )}
          </Wrap>
        </View>
        {
          // 收藏start
        }
        <View className="utp-index4-likes">
          <Wrap justifyContent="space-between">
            {[
              '商品收藏',
              '关注店铺',
              '浏览历史'
              //'优惠券'
            ].map((item, index) => {
              let num: number | string = '';
              if (item == '商品收藏' && !!user) {
                num = user.productCollectCount;
              }
              if (item == '关注店铺' && !!user) {
                num = user.storeCollectCount;
              }
              if (item == '浏览历史' && !!user) {
                num = user.productHistoryCount;
              }
              if (item == '优惠券' && !!user) {
                num = user.discountCouponCount;
              }
              return (
                <View
                  className="utp-ico utp-cnt utp-ico-1"
                  key={index}
                  onClick={() => {
                    if (!props.isLogin) {
                      Router.goLogin();
                    } else {
                      if (item === '优惠券') {
                        Taro.navigateTo({
                          url: '/pages/service/myCoupon/index'
                        });
                      } else if (item === '浏览历史') {
                        Router.goHistory();
                      } else if (item === '关注店铺') {
                        Taro.navigateTo({
                          url: '/pages/service/shopConcern/index'
                        });
                      } else if (item === '商品收藏') {
                        Router.goLikes();
                      }
                    }
                  }}
                >
                  <ItemMineNum title={(num && num.toString()) || ''} tips={item} />
                </View>
              );
            })}
          </Wrap>
        </View>

        {
          // 收藏end
        }
        {
          // 积分start
        }
        <View className="utp-index4-jifen">
          <View className="utp-index4-jifen-1">
            <Img src={images.newmine_bg1} width={710} height={140} />
          </View>
          <View className="utp-index4-jifen-2">
            <View
              className="utp-index4-jifen-box"
              onClick={() => {
                if (!props.isLogin) {
                  Router.goLogin();
                  return;
                }
                Taro.navigateTo({
                  url: '/pages/service/integral/index'
                });
              }}
            >
              <View className="utp-index4-jifen-box-gang"></View>
              <Wrap type={1}>
                <View>
                  <Img src={images.newmine_jifen} width={88} />
                </View>
                <View className="utp-index4-jifen-titles">
                  <View>
                    <Text className="utp-index4-jifen-titles-txt">我的积分</Text>
                  </View>
                  <View>
                    {!user && (
                      <Text className="utp-index4-jifen-titles-txt utp-index4-jifen-titles-txt-1">
                        0
                      </Text>
                    )}
                    {!!user && (
                      <Text className="utp-index4-jifen-titles-txt utp-index4-jifen-titles-txt-1">
                        {user.memberPoints}
                      </Text>
                    )}
                  </View>
                </View>
              </Wrap>
            </View>

            <View
              className="utp-index4-jifen-box"
              onClick={() => {
                if (!props.isLogin) {
                  Router.goLogin();
                  return;
                }
                Taro.navigateTo({
                  url: '/pages/service/myCoupon/index'
                });
              }}
            >
              <View className="utp-index4-jifen-box-gang"></View>
              <Wrap type={1}>
                <View>
                  <Img src={images.newmine_youhui} width={88} />
                </View>
                <View className="utp-index4-jifen-titles">
                  <View>
                    <Text className="utp-index4-jifen-titles-txt">优惠券</Text>
                  </View>
                  <View>
                    {!user && (
                      <Text className="utp-index4-jifen-titles-txt utp-index4-jifen-titles-txt-1">
                        0
                      </Text>
                    )}
                    {!!user && (
                      <Text className="utp-index4-jifen-titles-txt utp-index4-jifen-titles-txt-1">
                        {user.discountCouponCount}张
                      </Text>
                    )}
                  </View>
                </View>
              </Wrap>
            </View>

            <View
              className="utp-index4-jifen-box"
              onClick={() => {
                if (!props.isLogin) {
                  Router.goLogin();
                  return;
                }
                Taro.navigateTo({
                  url: '/pages/service/Voucher/index'
                });
              }}
            >
              <Wrap type={1}>
                <View>
                  <Img src={images.newmine_diyong} width={88} />
                </View>
                <View className="utp-index4-jifen-titles">
                  <View>
                    <Text className="utp-index4-jifen-titles-txt">抵用券</Text>
                  </View>
                  <View>
                    {!user && (
                      <Text className="utp-index4-jifen-titles-txt utp-index4-jifen-titles-txt-1">
                        0
                      </Text>
                    )}
                    {!!user && (
                      <Text className="utp-index4-jifen-titles-txt utp-index4-jifen-titles-txt-1">
                        {user.couponCount}张
                      </Text>
                    )}
                  </View>
                </View>
              </Wrap>
            </View>
          </View>
        </View>
        {
          // 积分end
        }
      </View>
      <View style={{ marginTop: Taro.pxTransform(20) }}></View>
      <Box>
        <View className="utp-th utp-flex utp-flex-b">
          <View className="utp-cnt">
            <Text className="utp-txt-th">我的订单</Text>
          </View>
          <View
            className="utp-flex utp-flex-b"
            onClick={() => {
              if (!props.isLogin) {
                Router.goLogin();
              } else {
                Taro.navigateTo({
                  url: '/pages/packOrder/orders/index?status=110'
                });
              }
            }}
          >
            <View className="utp-cnt">
              <Text className="utp-txt-th utp-txt-th-1">全部订单</Text>
            </View>
            <Image className="utp-more1" src={more} />
          </View>
        </View>

        <Wrap>
          {['待付款', '待发货', '待收货', '待评价', '售后/退款'].map((item, index) => {
            let src = msg;
            if (item == '待付款') {
              src = images.newmine_pay;
            }
            if (item == '待发货') {
              src = images.newmine_give;
            }
            if (item == '待收货') {
              src = images.newmine_get;
            }
            if (item == '待评价') {
              src = images.newmine_pingjia;
            }
            if (item == '售后/退款') {
              src = images.newmine_shouhou;
            }
            let num: number = 0;
            if (item == '待付款' && !!user) {
              num = count && count.unPay;
            }
            if (item == '待发货' && !!user) {
              num = count && count.unSend;
            }
            if (item == '待收货' && !!user) {
              num = count && count.unReceive;
            }
            if (item == '待评价' && !!user) {
              num = count && count.toBeCommentedOnCount;
            }
            if (item == '售后/退款' && !!user) {
              num = count && count.refund;
            }

            return (
              <View
                style={{ width: '20%' }}
                key={index}
                onClick={() => {
                  if (!props.isLogin) {
                    Router.goLogin();
                  } else {
                    if (item === '待评价') {
                      Taro.navigateTo({
                        url: '/pages/service/goodsReviews/index'
                      });
                    } else if (item === '售后/退款') {
                      Taro.navigateTo({
                        url: '/pages/person/applyService/index'
                      });
                    } else {
                      //全部  110   待付款 0  待发货 1 待收货2
                      let status = item === '待付款' ? 0 : item === '待发货' ? 1 : 2;
                      Taro.navigateTo({
                        url: `/pages/packOrder/orders/index?status=${status}`
                      });
                    }
                  }
                }}
              >
                <ItemMineIcon tips={item} src={src} num={num} />
              </View>
            );
          })}
        </Wrap>
      </Box>

      <Box>
        <View className="utp-th utp-flex">
          <Text className="utp-txt-th">常用服务</Text>
        </View>
        <Wrap>
          {[
            '地址管理',
            '帮助反馈',
            '心愿单',
            //'手机开门',
            //'投诉建议',
            //'邀请好友',
            '我的拼团'
            //'停车缴费'
            //'会员码',
            //'抵用券'
          ].map((item, index) => {
            let src = msg;
            if (item == '地址管理') {
              src = images.newmine_address;
            }
            if (item == '帮助反馈') {
              src = images.newmine_help;
            }
            if (item == '抵用券') {
              src = images.personal_icon_exchange;
            }
            if (item == '手机开门') {
              src = images.newmine_door;
            }
            if (item == '投诉建议') {
              src = images.newmine_jianyi;
            }
            if (item == '邀请好友') {
              src = images.newmine_friend;
            }
            if (item == '我的拼团') {
              src = images.newmine_tuan;
            }
            if (item == '会员码') {
              src = images.personal_icon_huiyuanma;
            }
            if (item == '心愿单') {
              src = images.newmine_wish;
            }
            if (item == '停车缴费') {
              src = images.newmine_chefei;
            }

            return (
              <View
                onClick={() => {
                  if (!props.isLogin) {
                    Router.goLogin();
                  } else {
                    if (item === '地址管理') {
                      Router.goAddress();
                    } else if (item === '邀请好友') {
                      Taro.navigateTo({
                        url: '/pages/wishList/InviteFriends/index'
                      });
                    } else if (item === '会员码') {
                      Taro.navigateTo({
                        url: '/pages/wishList/myMember/index'
                      });
                    } else if (item === '我的拼团') {
                      Taro.navigateTo({
                        url: '/pages/assemble/list/index'
                      });
                    } else if (item === '心愿单') {
                      Taro.navigateTo({
                        url: '/pages/wishList/myWishList/index'
                      });
                    } else if (item === '抵用券') {
                      Taro.navigateTo({
                        url: '/pages/service/Voucher/index'
                      });
                    } else {
                      Taro.navigateTo({
                        url: '/pages/page2/index'
                      });
                    }
                  }
                }}
                style={{ width: '25%' }}
                key={index}
              >
                <ItemMineIcon tips={item} src={src} />
              </View>
            );
          })}
        </Wrap>
      </Box>
    </View>
  );
};
Index.defaultProps = {};

Index.config = {
  navigationBarTitleText: '',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5',
  navigationStyle: 'custom'
};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    unread: state.cart.unread,
    isLogin: state.cart.isLogin,
    closeWidth: state.cart.closeWidth,
    statusHeight: state.cart.statusHeight
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
