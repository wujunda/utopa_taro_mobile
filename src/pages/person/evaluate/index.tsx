import { View, Text, Textarea,Image } from '@tarojs/components';
import Taro, { useState, useRouter, useEffect } from '@tarojs/taro';
import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Bar from '../../../components/bar';
import Bton from '../../../components/Bton';
import Img from '../../../components/Img';
import startActive from '@/assets/info/startActive.png';
import start from '@/assets/info/start.png';
import { getToken,getImgUrl } from '../../../services/upload';
import icon_addphoto from '../../../assets/mine/icon_addphoto.png';
import { addComment, DataType } from '../../../services/afterSale';
import Qiniu from '../../../utils/qiniu';
import { images } from '../../../images';
import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const [comment, setComment] = useState('');
  const [imgs, setImgs] = useState([] as any[]);
  const [newImgs, setNewImgs] = useState([] as any[]);
  const [data, setData] = useState({} as DataType);
  const [num, setNum] = useState(5);
  const router = useRouter();

  const select = () => {
    Taro.chooseImage({
      count: 6,
      success: (info) => {
        let temp = [...imgs];
        let temp2 =[...newImgs];
        let arr = temp.concat(info.tempFilePaths);
        let arrh5 = temp2.concat([info.tempFiles[0].originalFileObj]);
        setImgs(arr);
        setNewImgs(arrh5);
      }
    });
  };
  useEffect(() => {
    setData(Taro.getStorageSync('list'));
  }, []);
  let feedbackKeys: any = [];
  const submit = async () => {
    if (imgs.length === 0) {
      onDone();
      return;
    }
    const data = await getToken({});
    if (data.code === 0 && data.data && data.data.upToken) {
      let arr = imgs;
      if (process.env.TARO_ENV === 'h5') {
        arr = newImgs;
      }
      arr.map((item) => {
        Qiniu(data.data.upToken, item).then(
          async  (hash) => {
            Taro.hideLoading();
            feedbackKeys.push(hash);
            if (feedbackKeys.length === imgs.length) {
              onDone();
            }
            let url = await getImgUrl(hash);
          },
          () => {
            Taro.showToast({
              title: '上传图片失败，请重试',
              icon: 'none'
            });
            feedbackKeys = [];
          }
        );
      });
    } else {
      Taro.showToast({
        title: '获取token失败!',
        icon: 'none'
      });
    }
  };
  const onDone = async () => {
    Taro.showLoading({
      title: ''
    });
    let payload = {
      comment: comment,
      orderDetailId: router.params.orderDetailId,
      score: num,
      picIds: feedbackKeys.join(',')
    };
    let res = await addComment(payload);
    if (res.code === 0) {
      Taro.showToast({
        icon: 'success',
        title: '评价成功'
      });
      Taro.navigateTo({
        url: '/pages/service/goodsReviews/index'
      });
    }else{
      Taro.showToast({
        icon: 'none',
        title: res.msg
      });
    }
  };
  const onDel = (index) => {
    if (imgs.length > 0) {
      let arr: any = [].concat(imgs);
      arr.splice(index, 1);
      let arrh5: any = [].concat(newImgs);
      arrh5.splice(index, 1);
      setImgs(arr);
      setNewImgs(arrh5);
    }
  };
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <View style={{ height: 'calc(100% - 45px)' }}>
            <View style={{ width: Taro.pxTransform(750) }}></View>
            {false && <Bar title="评价晒单" />}
            <View className="utp-evaluate-infoBox">
              <Wrap type={2} flexDirection="column">
                <View className="utp-evaluate-goodsInfo">
                  <Wrap type={2} Myheight={220}>
                    <View>
                      <Img src={data.picUrl} width={180}></Img>
                    </View>
                    <View className="utp-evaluate-titlesBox">
                      <Wrap type={3} top flexDirection="column">
                        <Text className="utp-evaluate-titles">{data.name}</Text>
                        <Wrap>
                          {[1, 2, 3, 4, 5].map((p, pp) => {
                            return (
                              <Img
                                src={p <= num ? startActive : start}
                                key={pp}
                                width={40}
                                onClick={() => {
                                  setNum(p);
                                }}
                              />
                            );
                          })}
                        </Wrap>
                      </Wrap>
                    </View>
                  </Wrap>
                </View>
                <View className="utp-evaluate-evaluateContent">
                  <Textarea
                    value={comment}
                    className="utp-evaluate-evaluateContent-text"
                    maxlength={100}
                    placeholder="请输入你评价的内容，不超过100字哦~"
                    onInput={(e) => {
                      setComment(e.detail.value);
                    }}
                  />
                </View>
                <View className="utp-evaluate-evaluateImg">
                  <Wrap type={2} top>
                    {imgs.map((i, ii) => {
                      return (
                        <View key={ii} className='utp-evaluate-evaluateImgBox' style={{marginRight:(ii+1)%3==0?0:20,marginBottom:20}} >
                          <Img src={i} width={200} ></Img>
                          <Image src={images.sendMsg_clo} className='utp-evaluate-del' onClick={()=>{
                            onDel(ii)
                          }}/>
                        </View>
                      );
                    })}
                    {imgs.length < 6 && (
                      <Img src={icon_addphoto} width={220} onClick={select}></Img>
                    )}
                  </Wrap>
                </View>

                {imgs.length > 0 && comment && num ? (
                  <View onClick={submit} className="utp-evaluate-btn">
                    <Bton>提交评价</Bton>
                  </View>
                ) : (
                  <View className="utp-evaluate-btn">
                    <Bton type="none">提交评价</Bton>
                  </View>
                )}
              </Wrap>
            </View>
          </View>
        </View>
      </View>
    </Safe>
  );
};
Index.config = {
  navigationBarTitleText: '评价晒单',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};

Index.defaultProps = {};
export default Index;
