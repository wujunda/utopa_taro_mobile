import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import './index.scss';

type PageStateProps = {
  top?: number;
  bottom?: number;
  children: any[];
};

type PageDispatchProps = {};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
const Index: Taro.FC<IProps> = ({ children, top = 0, bottom = 0 }) => {
  let fixed = false;
  if (children && children.length > 1) {
    fixed = true;
  }
  let hd, bd, ft;
  if (children) {
    hd = !!children[0];
    bd = !!children[0];
    ft = !!children[0];
  }
  // console.log(children);
  return (
    <View className={fixed ? 'utp-wrap' : ''}>
      {hd && <View className={fixed ? 'utp-wrap-hd' : ''}>{children[0]}</View>}
      {bd && (
        <View
          className={fixed ? 'utp-wrap-bd' : ''}
          style={{ top: Taro.pxTransform(top), bottom: Taro.pxTransform(bottom) }}
        >
          {children[1]}
        </View>
      )}
      {ft && <View className={fixed ? 'utp-wrap-ft' : ''}>{children[2]}</View>}
    </View>
  );
};

// @ts-ignore
export default Index;
