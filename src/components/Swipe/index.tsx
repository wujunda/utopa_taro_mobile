import Taro, { useState } from '@tarojs/taro';
import { Image, View, SwiperItem, Swiper } from '@tarojs/components';
import './index.scss';
import { ISwipe } from '../../interface/home';
import Router from '../../utils/router';

interface Props {
  data: ISwipe[];
  height?: number;
}
const Index: Taro.FC<Props> = ({ data, height = 520 }) => {
  const [crt, setCrt] = useState(0);
  return (
    <View className="utp-swipe" style={{ height: Taro.pxTransform(height) }}>
      <View className="utp-swipe-icos">
        {!!data && data.map((item, index) => {
          if (!item) {
            return;
          }
          return (
            <View
              key={item.url}
              className={'utp-swipe-icos-ico ' + (crt === index ? 'utp-swipe-icos-ico-crt' : '')}
            ></View>
          );
        })}
      </View>
      <Swiper
        onChange={(e) => {
          setCrt(e.detail.current);
        }}
        className="utp-swipe"
        indicatorColor="#999"
        indicatorActiveColor="#333"
        circular
        autoplay
        style={{ height: Taro.pxTransform(height) }}
        indicatorDots={false}
      >
        {!!data && data.map((item, index) => {
          if (!item) {
            return;
          }
          return (
            <SwiperItem key={index}>
              <View
                className="utp-swipe-item utp-cnt utp-swipe-item-1"
                style={{ height: Taro.pxTransform(height) }}
                onClick={() => {
                  Router.goHome(item);
                }}
              >
                <Image style="width:100%;height:100%" src={item.source} />
              </View>
            </SwiperItem>
          );
        })}
      </Swiper>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
