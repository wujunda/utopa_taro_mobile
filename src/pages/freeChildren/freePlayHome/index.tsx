import Taro from '@tarojs/taro';
import { View, ScrollView, Text, Image } from '@tarojs/components';

import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
// import Goods1 from '../../../components/Goods1';
import Goods1 from '../../../components/Goods1';
import Img from '../../../components/Img';
import Card from '../../../components/Card';
import Score from '../../../components/Score';
import goods from '../../../assets/goods.png';
import positionAddress from '../../../assets/positionAddress.png';
import icon_phone_orange from '../../../assets/shoppingStore/icon_phone_orange.png';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-freePlayHome"
            scrollY
          >
            <View className="utp-freePlayHome-part1">
              <View className="utp-freePlayHome-titlesBox">
                <Wrap type={3} top flexDirection="column">
                  <View>
                    <Text className="utp-freePlayHome-titles">韩国非凡宝贝专业儿童摄影机构</Text>
                  </View>
                  <View>
                    <Wrap>
                      <Text className="utp-freePlayHome-renqi">人气值：</Text>
                      <Score num={4} width={24} />
                    </Wrap>
                  </View>
                  <View className="utp-freePlayHome-ImgBox">
                    <Image src={goods} className="utp-freePlayHome-Img"></Image>
                    <Image src={goods} className="utp-freePlayHome-Img"></Image>
                    <Image src={goods} className="utp-freePlayHome-Img"></Image>
                    <Image src={goods} className="utp-freePlayHome-Img"></Image>
                    <Image src={goods} className="utp-freePlayHome-Img"></Image>
                  </View>
                  <View className="utp-freePlayHome-address">
                    <Wrap type={1}>
                      <View onClick={() => {}}>
                        <Img src={positionAddress} width={36}></Img>
                      </View>
                      <Text className="utp-freePlayHome-addressContent">
                        我是个地址我是个地址我是个地址我是个地址我是个地址
                      </Text>

                      <View onClick={() => {}}>
                        <Img src={icon_phone_orange} width={32}></Img>
                      </View>
                    </Wrap>
                  </View>
                </Wrap>
              </View>
            </View>
            <View className="utp-freePlayHome-part2">
              <Text className="utp-freePlayHome-part2-title">优惠</Text>
              <Card />
              <Card />
            </View>
            <View className="utp-freePlayHome-part3">
              <Text className="utp-freePlayHome-part3-title">在售商品</Text>
              <Goods1 />
              <Goods1 />
            </View>
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};
export default Index;
