import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

interface Props {
  children: any;
}
const Index: Taro.FC<Props> = ({ children }) => {
  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      {children}
    </View>
  );
};
Index.defaultProps = {};

export default Index;
