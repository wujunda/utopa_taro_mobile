import { getOpenToken } from '@/services/user';
// eslint-disable-next-line no-unused-vars
import { CommonModelType } from './connect';
import Api from './api';

export interface UserState {
  name: string;
  isWeiXinFalg: boolean;
  inApp: boolean;
  openToken?: string;
}

const model: CommonModelType<Partial<UserState>> = {
  namespace: 'user',

  // 数据
  state: {
    name: 'user',
    isWeiXinFalg: false,
    inApp: false,
    openToken: ''
  },

  // 订阅
  subscriptions: {},

  // 异步
  effects: {
    *wait(_, { call, put }) {
      // console.log(call);
      // console.log(44);
      // console.log(Api);
      let str = yield call(Api.api, { name: 'uuiuiu' });
      //console.log(str);

      yield put({
        type: 'save',
        payload: {
          name: str
        }
      });
      // console.log('end');
    },
    *judegIsWeiXin(param, { put }) {
      yield put({
        type: 'isWeiXin',
        payload: param.falg
      });
    },
    *judegInApp(param, { put }) {
      yield put({
        type: 'isApp',
        payload: param.falg
      });
    },
    // 获取openToken,传给h5获取用户信息使用
    *getOpenToken(_, { call, put, select }) {
      const token = yield select(state => state.user.openToken);
      if (token) return token;
      const res = yield call(getOpenToken);
      console.log('getOpenToken', { res });
      if (res.code === 0 && res.data) {
        const { openToken } = res.data
        yield put({ type: 'saveOpenToken', payload: { openToken } });
        return openToken
      }
    }
  },

  // 同步
  reducers: {
    save(state, action) {
      // console.log(333);
      // console.log(action);
      return { ...state, ...action.payload };
    },
    isWeiXin(state = {}, action) {
      state.isWeiXinFalg = action.payload;
      return { ...state, ...action.payload };
    },
    isApp(state = {}, action) {
      state.inApp = action.payload;
      return { ...state, ...action.payload };
    },
    saveOpenToken(state = {}, { payload }) {
      return { ...state, openToken: payload.openToken };
    }
  }
};

export default model;
