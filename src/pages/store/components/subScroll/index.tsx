import Taro, { useState, useEffect, useReachBottom, usePageScroll } from '@tarojs/taro';
import { View, Text, ScrollView } from '@tarojs/components';
import ShoppingGoods from '../../../../components/shopGoods';
import ItemCateGoods from '../../../../components/ItemCateGoods';
import ItemCateAd from '../../../../components/ItemCateAd';
import Wrap from '../../../../components/Wrap';
import useRequest from '../../../../hooks/useRequest';
import LoadingMore from '../../../../components/LoadingMore';
import LoadingNone from '../../../../components/LoadingNone';
import LoadingNoneMore from '../../../../components/LoadingNoneMore';
import {} from '../../../../services/cate';
import useSession from '../../../../hooks/useSession';
import useResetCount from '../../../../hooks/useResetCount';
import useLogin from '../../../../hooks/useLogin';
import './index.scss';
import useRequestWIthMore from '../../../../hooks/useRequestWIthMore';
import Loading from '../../../../components/Loading';
import { IStore, getProductsByBusinessId } from '../../../../services/store';

interface Props {
  data?: any;
  defaultData?: any;
  index?: number;
  callBottom: any;
}
const Index: Taro.FC<Props> = (props) => {
  const [haveSession] = useSession();
  let { data, defaultData, index } = props;
  const [state, hasMore, run, getMoreData, loading] = useRequestWIthMore(
    data,
    getProductsByBusinessId
  );
  if (props.callBottom) {
    console.log('更新下拉刷新');
    props.callBottom.fn = getMoreData;
  }
  const [can, update1] = useResetCount();
  if (haveSession && (state === null || (state != null && state.length == 0)) && run && !loading) {
    if (can) {
      run(data);
      update1();
    }
  }
  let goods = [];
  if (state && state.length > 0 && state[0].data.records) {
    state.map((item) => {
      item.data.records.map((it) => {
        goods.push(it);
      });
    });
  }
  console.log('renderGoods');
  console.log(goods);

  return (
    <View>
      {!defaultData && loading && goods.length === 0 && <Loading />}
      {!defaultData && !loading && goods.length === 0 && <LoadingNone />}
      {
        //默认渲染
        defaultData &&
          defaultData[index || 0] &&
          defaultData[index || 0].productItems.map((item1) => {
            return (
              <View
                key={item1.id}
                className="utp-shoppingStore-mytabsList-goods"
                id={item1.id ? item1.id : ''}
                onClick={() => {
                  Taro.navigateTo({
                    url: `/pages/details/index?id=${item1.productId}&businessId=${item1.businessId}&storeId=${item1.storeId}`
                  });
                }}
              >
                <ShoppingGoods data={item1} />
              </View>
            );
          })
      }
      {/* 切换 */}
      {goods.map((item1) => {
        return (
          <View
            key={item1.id}
            className="utp-shoppingStore-mytabsList-goods"
            onClick={() => {
              Taro.navigateTo({
                url: `/pages/details/index?id=${item1.productId}&businessId=${item1.businessId}&storeId=${item1.storeId}`
              });
            }}
          >
            <ShoppingGoods data={item1} />
          </View>
        );
      })}
      {loading && goods.length !== 0 && <LoadingMore />}
      {!loading && !hasMore && <LoadingNoneMore />}
    </View>
  );
};
Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '店铺',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};
export default Index;
