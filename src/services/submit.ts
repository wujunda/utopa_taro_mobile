import request from '../utils/request';
import Taro from '@tarojs/taro';
import { IAddressData } from './address';

export interface IPost1Address {
  businessId: number;
  isUseOriginPrice: number;
  storeId: number;
  products: IPostGoods[];
  receiptAddress: any;
  couponCode: string;
}
export interface IPost1Time {
  skuId: number;
  skuNum: number;
}
export interface IPost1Card {
  skuId: number;
  skuNum: number;
}
export interface IPosts {
  // 配送方式
  crt: number;
  saveCrt: number;
  crtLabel: number;
  crtSaveLabel: number;
  // 配送地址
  address: IAddressData;
  // 配送时间
  crtTime: string;
  crtTimeObj: ShippingTimes;
  // 优惠券
  crtCard: string;
  saveCrtCard: string;
  // 备注
  msg: String;
  storeId: number;
}

export interface IPostGoods {
  skuId: number;
  skuNum: number;
}
export interface IPostStore {
  businessId: number;
  isUseOriginPrice: number;
  storeId: number;
  products: IPostGoods[];
  receiptAddress: any;
  couponCode: string;
  beginShippingTime: string;
  endShippingTime: string;
  logisticType: number;
  buyerMemo: string;
}
export interface IPost {
  clientType: any;
  sysId: any;
  binComits: IPostStore[];
}

export interface ISubmitGoods {
  name: string;
  norm1Name: string;
  norm1Value: string;
  skuCode: string;
  pic: string;
  realSkuCode: string;
  id: number;
  price: number;
  productsSubtotalPrice: number;
  skuId: number;
  num: number;
  norm2Value: string;
  actuallyPrice: number;
  originPrice: number;
}

//// 配送时间日
//export interface ShippingTimesMapItem {
//beginShippingTime: string;
//dateTime: string;
//endShippingTime: string;
//shippingFee: number;
//}
// 配送时间日
export interface ShippingTimes {
  beginShippingTime: string;
  dateTime: string;
  endShippingTime: string;
  shippingFee: number;
}
// 配送时间天
export interface DateInfoList {
  dateStr: string;
  shippingTimes: ShippingTimes[];
}

// 配送方式
export interface LogisticsModes {
  carriage: number;
  name: string;
  label: number;
  //shippingTimesMap: ShippingTimesMap[];
  dateInfoList: DateInfoList[];
}
// 优惠券
export interface Coupon {
  couponName: string;
  couponCode: string;
  conditionInfo: string;
  preferentialInfo: string;
  couponValue: number;
  couponId: number;
}
// 优惠券
export interface Coupons {
  alivableCoupons: Coupon[];
  unalivableCoupons: Coupon[];
}
export interface ISubmitStore {
  save: IPosts;
  storeName: string;
  storeId: number;
  businessId: number;
  businessType: number;
  actuallyAmount: number;
  discountDetail: string;
  // 商品
  orderProducts: ISubmitGoods[];
  // 配送方式
  logisticsModes: LogisticsModes[];
  // 默认配送方式
  crt: number;
  saveCrt: number;
  // 收货地址
  address: IAddressData;
  // 配送时间
  crtTime: string;
  // 优惠券
  crtCard: string;
  saveCrtCard: string;
  coupons: Coupons;
  beginShippingTime: string;
  endShippingTime: string;
  logisticType: number;
}

export interface CouponDiscountInfo {
  couponName: string;
  discountValue: number;
}
export interface ISubmitData {
  actuallyPrice: number;
  arrivalDay: number;
  orderTotalMoneyNew: number;
  orderTotalMoney: number;
  orderExchangeTotalPoint: number;
  givenUcoin: number;
  category: string;
  commitButton: boolean;
  products: ISubmitStore[];
  couponDiscountInfo: CouponDiscountInfo[];
}

export interface ISubmit {
  code: number;
  data: ISubmitData;
}

export const getWaitSubmit = (params: any) => {
  params.accessToken = Taro.getStorageSync('login');
  params.uid = Taro.getStorageSync('uid');
  return request.post<any>('/app.do', {
    method: 'eshop.productorder.binConfirmOrder',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};

export const getWaitSubmit1 = (params: any) => {
  params.accessToken = Taro.getStorageSync('login');
  params.uid = Taro.getStorageSync('uid');
  return request.post<any>('/app.do', {
    method: 'eshop.productorder.binConfirmOrder',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
export const getDone = (params: any) => {
  params.accessToken = Taro.getStorageSync('login');
  params.uid = Taro.getStorageSync('uid');
  return request.post<any>('/app.do', {
    method: 'eshop.productorder.bin.app.bincommitOrder',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
export const getDone1 = (params: any) => {
  params.accessToken = Taro.getStorageSync('login');
  params.uid = Taro.getStorageSync('uid');
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.productorder.app.confirmSelfObtainOrder',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};

export const getCustomCategoryList = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.product.getCustomCategoryList',
    ver: '2.0',
    params: JSON.stringify(params)
  });
};

export const getMiniPay = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'neweshop.productorder.payOrder',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};
// 加入购物车
export const goAddCart = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.shopcart.addShopCart',
    ver: '2.0',
    params: JSON.stringify({
      ...params,
      clientType: 1,
      sysId: 1
    })
  });
};
// 加入购物车
export const getStoreId = (params: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.product.analysisScanCodeShopping',
    ver: '2.0',
    params: JSON.stringify({
      ...params
    })
  });
};

//店铺购物车 数量
export const totalNumber = (params) => {
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.getTotalNumber',
    ver: '1.0',
    params: JSON.stringify({
      storeId: params,
      sysId: '1'
    })
  });
};
