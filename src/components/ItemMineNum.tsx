// import { ComponentClass } from 'react';
import Taro from '@tarojs/taro';
import BashIt from './BashIt';

type PageStateProps = {
  // counter: {
  //   num: number;
  // };
};

type PageDispatchProps = {
  // add: () => void;
  // dec: () => void;
  // asyncAdd: () => any;
};

type PageOwnProps = {
  tips: string;
  title: string;
  num?: number | string;
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = (props) => {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  return (
    <BashIt
      top={0}
      bottom={0}
      src=""
      srcWidth={62}
      title={props.title || '0'}
      titleHeight={62}
      titleColor="#333333"
      titleSize={40}
      tips={props.tips}
      tipsHeight={32}
      tipsColor="#6a6a6a"
      tipsSize={22}
      num={props.num}
    ></BashIt>
  );
};

Index.defaultProps = {};
export default Index;
