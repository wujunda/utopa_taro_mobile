import request from '../utils/request';
import { ProductBaiscData } from '../interface/product';

export interface IHistory {
  code: number;
  data: {
    current: number;
    pages: number;
    size: number;
    total: number;
    records: ProductBaiscData[];
  };
}
export const getLikes = (params: any, pst: any) => {
  // console.log(params);
  console.warn('查询参数');
  console.warn(pst);
  return request.post<any>('/app.do', {
    method: 'eshop.product.getFavProductList',
    ver: '2.0',
    params: JSON.stringify({
      pageSize: pst.per_page,
      pageNo: pst.page
    })
  });
};
export const getAddLike = (params: any, pst: any) => {
  // console.log(params);
  console.warn('查询参数');
  console.warn(pst);
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.favorite',
    ver: '1.0',
    params: JSON.stringify({
      favoriteId: pst.favoriteId,
      storeId: pst.storeId,
      type: 0
    })
  });
};
export const getUnLike = (params: any, pst: any) => {
  // console.log(params);
  console.warn('查询参数');
  console.warn(pst);
  return request.post<any>('/app.do', {
    method: 'utopa.eshop.shopcart.unfavorite',
    ver: '1.0',
    params: JSON.stringify({
      favoriteId: pst.favoriteId,
      storeId: pst.storeId,
      type: 0
    })
  });
};
