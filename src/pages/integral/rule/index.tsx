
import { View, RichText } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import Taro, { useState, useEffect, useDidShow, useRef } from '@tarojs/taro';
import Scale from '../../../components/ScaleImg/index'
import JSBridge from '../../../utils/jsbridge/index';
import { _fetchToken } from '../../../services/login';
import { getUserPointRule } from '../../../services/integral';
import './index.scss';

type PageStateProps = {
  user: {
    inApp: boolean;
  };
}

type PageDispatchProps = {
  setToken: () => void;
};

function Rule(props) {
  const [imgUrl, setImgUrl] = useState('')
  const [openImgFalg, setOpenImgFalg] = useState(false)
  const [richModel, setRichModel] = useState('')
  const couterRef = useRef();
  useDidShow(() => {
    if (props.user.inApp) {
      JSBridge.Bridge.WebViewJavascriptBridge.setup(() => {
        // 判断是否登录
        JSBridge.Common.Caller_Common_Base_getLoginStatus('', (response) => {
          if (response == 0) {
            JSBridge.Common.GTBridge_Common_Base_goLogin();
          } else {
            // 拿token
            JSBridge.Common.Caller_Common_Base_getOpenToken('', (response) => {
              if (response) {
                _fetchToken({
                  openToken: response
                }).then((res) => {
                  if (res.code === 0) {
                    Taro.setStorage({
                      key: 'isLogin',
                      data: {
                        accessToken: res.data.accessToken
                      }
                    });
                    props.setToken({accessToken: res.data.accessToken})
                    setTimeout(() => {
                      getUserPointRule().then(res => {
                        if (res.code == 0) {
                          setRichModel(res.data.noteDetail)
                        } else {
                          Taro.showToast({
                            title: res.msg,
                            icon: 'none'
                          })
                        }
                      })
                    }, 300);
                  } else {
                    Taro.showToast({
                      title: res.msg,
                      icon: 'none'
                    });
                  }
                });
              }
            });
          }
        });
      });
    } else {
      getUserPointRule().then(res => {
        if (res.code == 0) {
          setRichModel(res.data.noteDetail)
        } else if (res.code == 99950005) {
          Taro.navigateTo({
            url: '/pages/login/index'
          });
        } else {
          Taro.showToast({
            title: res.msg,
            icon: 'none'
          })
        }
      })
    }
  })
  useEffect(() => {
    if (richModel) {
      const imgScr = richModel.split('src="')[1].split('">')[0].replace('amp;', '')
      setImgUrl(imgScr)
    }
  }, [richModel])
  const openImg = () => {
    setOpenImgFalg(true)
    // if (imgUrl != '') {
    //   setOpenImgFalg(true)
    // } else {
    //   setImgUrl(document.getElementsByTagName('img')[0].src)
    //   setTimeout(() => {
    //     setOpenImgFalg(true)
    //   }, 100);
    // }
  }
  const closeImg = () => {
    setOpenImgFalg(false)
  }
  return (
    <View className="utp-integral-rule">
      <View className="utp-integral-rule-text" onClick={() => openImg()}>
        <RichText nodes={richModel} ref={couterRef}/>
      </View>
      {
        openImgFalg && 
        <View className="utp-integral-rule-open" onclick={() => closeImg()} style="overflow:auto;">
          <Scale data={imgUrl} />
        </View>
      }
    </View>
  );
}

Rule.config = {
  navigationBarTitleText: '积分规则',
  disableScroll: false
};
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp
    }
  };
};
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    setToken: (params) => {
      dispatch({
        type: 'cart/saveLogin',
        payload: {
          data: params
        }
      })
    }
  };
};
export default connect(mapStateToProps,mapDispatchToProps)(Rule);