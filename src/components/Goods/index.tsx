import Taro, { useState, useEffect } from '@tarojs/taro';
import { connect } from '@tarojs/redux';
import { View, Image, Text } from '@tarojs/components';
import Yuan from './../Yuan';
import './index.scss';
import check from '../../assets/cart/check.png';
import uncheck from '../../assets/submit/uncheck.png';
import down from '../../assets/location/more.png';
import Stepper from './../Stepper';
import Wrap from './../Wrap';
import Txt from './../Txt';
import Sku from '../../components/Sku';
//import { images } from '../../images';

import { getGoodsSku } from '../../services/details';

type PageStateProps = {
  isScan: boolean;
  isDone: boolean;
  // counter: {
  //   num: number;
  // };
};

type PageDispatchProps = {};

type PageOwnProps = {
  edit?: boolean;
  data?: any;
  onCheck?: () => void;
  onMinus?: (qty) => void;
  onAdd?: (qty) => void;
  changeGoods: (any, boolean) => void;
  changeGoodsSku: (any) => void;
  checkGoods: (any, boolean) => void;
  onToggleSku: (boolean) => void;
  onEditChange?: () => void;
  isCrt?: boolean;
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = ({
  isScan = false,
  isCrt,
  edit = false,
  data,
  changeGoods,
  changeGoodsSku,
  checkGoods,
  onEditChange,
  isDone = false,
  onToggleSku
}) => {
  const [skuState, setSkuState] = useState<any>('');
  const [show, setShow] = useState(false);
  useEffect(() => {
    if (onToggleSku) {
      onToggleSku(show);
    }
  }, [show]);
  const [update, setUpdate] = useState(1);
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */

  const addGoods = async () => {
    changeGoods(
      {
        id: data.id,
        number: data.number + 1
      },
      isScan
    );
  };
  const decGoods = async () => {
    if (data.number !== 1) {
      changeGoods(
        {
          id: data.id,
          number: data.number - 1
        },
        isScan
      );
    }
  };

  const toggleCheckGoods = async () => {
    checkGoods(
      {
        id: data.id,
        isSelect: data.isSelect === 0 ? 1 : 0
      },
      isScan
    );
  };
  const handleClick = (e) => {
    e.stopPropagation();
    if (!edit) {
      if (toggleCheckGoods) {
        toggleCheckGoods();
      }
    } else {
      // console.log('取消选中');
      data.isCrt = !data.isCrt;
      setUpdate(update + 1);
      if (onEditChange) {
        onEditChange();
      }
    }
  };
  // console.log('更新');
  const getSku = async (e) => {
    e.stopPropagation();
    Taro.showLoading({
      title: ''
    });
    let rst = await getGoodsSku({
      productId: data.proId,
      storeId: data.storeId
    });
    Taro.hideLoading();
    if (rst.code === 0) {
      setSkuState(rst);
      setShow(true);
    }
  };

  return (
    <View
      className="utp-goods utp-div"
      onClick={() => {
        Taro.navigateTo({
          //url: `/pages/details/index?id=237089&storeId=14801&skuId=539733&jifen=1`
          url: `/pages/details/index?id=${data.proId}&storeId=${data.storeId}&businessId=${data.businessId}&type=${data.businessType}`
        });
      }}
    >
      {isDone && <View style={{ width: Taro.pxTransform(20) }}></View>}

      {!isDone && (
        <View className="utp-goods-check utp-cnt" onClick={handleClick}>
          {!edit && (
            <Image className="utp-goods-check-img" src={data.isSelect === 1 ? check : uncheck} />
          )}

          {update && edit && (
            <Image className="utp-goods-check-img" src={isCrt || data.isCrt ? check : uncheck} />
          )}
        </View>
      )}

      <View className="utp-goods-i utp-cnt">
        <Image className="utp-goods-img" src={data && data.productPic} />
        {isDone && <View className="utp-goods-ii"></View>}
      </View>
      <View className="utp-goods-item">
        <View className="utp-goods-titles">
          <Text
            className={
              !isDone ? 'utp-goods-title taro-text' : 'utp-goods-title taro-text utp-goods-utitle'
            }
          >
            {'\t'}
            {/* 标题标题标题标题标题标标题标题标题标题标题标标题标题 */}
            {data && data.productName}
          </Text>
        </View>
        {!isDone ? (
          <View>
            <View className="utp-goods-moneys">
              <Wrap Myheight={40} flexWrap="nowrap">
                <View className="utp-goods-money">
                  <Yuan price={data && data.price} size={30} color="#FF2F7B" />
                </View>
                {data && data.price < data.originalPrice && (
                  <View className="utp-goods-ormoney">
                    <Yuan price={data && data.originalPrice} size={24} color="#999" />
                  </View>
                )}
              </Wrap>
              <Stepper
                qty={data && data.number}
                onMinus={() => {
                  decGoods();
                }}
                onAdd={() => {
                  addGoods();
                }}
              />
            </View>

            <View className="utp-goods-more" onClick={getSku}>
              <Text className="utp-goods-more-t">
                {data && data.norm2Value === null
                  ? data && data.norm1Value
                  : data && data.norm2Value + ' ;  ' + data.norm1Value}{' '}
              </Text>
              <Image src={down} className="utp-goods-more-img" />
            </View>
          </View>
        ) : (
          <View className="utp-goods-tip">
            <Txt title="宝贝已不能购买，请联系客服" size={20} color="deep" />
          </View>
        )}
      </View>

      {show && (
        <Sku
          isCart
          id1={data.productId}
          type={skuState && skuState.data.productSkus[0].skuType}
          show={show}
          data={skuState && skuState.data}
          cartImage={data.productPic}
          cartNumber={data.number}
          onClose={() => {
            setShow(false);
          }}
          closeOverlay={() => {
            setShow(false);
          }}
          changeCartSku={(post) => {
            changeGoodsSku(
              Object.assign(post, {
                ids: data.id,
                storeId: data.storeId,
                businessId: data.businessId
              })
            );
          }}
        />
      )}
    </View>
  );
};

Index.defaultProps = {};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {};
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    changeGoods: (payload, isScan) => {
      console.warn('触发添加购物车1');
      dispatch({
        type: 'cart/changeGoods',
        payload,
        isScan
      });
    },
    changeGoodsSku: (payload) => {
      console.warn('触发添加购物车1');
      dispatch({
        type: 'cart/changeGoodsSku',
        payload
      });
    },

    checkGoods: (payload, isScan) => {
      console.warn('触发选中反选');
      console.warn(payload);
      console.warn(isScan);
      dispatch({
        type: 'cart/checkGoods',
        payload,
        isScan
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
