import request from '../utils/request';
export interface IHome {
  code: number;
  data: {
    banners: { image: string }[];
    classes: { image: string; name: string }[];
    columns: { id: number; name: string }[];
  };
}
export const getStoreHome = (params) => {
  return request.post<any>('/app.do', {
    method: 'eshop.special.getBaseInfo',
    ver: '1.0',
    params: JSON.stringify({
      ...params,
      clientType: '1',
      sysId: '1'
    })
  });
};
export const getStoreStore = (params, pst: any) => {
  return request.post<any>('/app.do', {
    method: params.split('%%')[0],
    ver: '1.0',
    params: JSON.stringify({
      id: params.split('%%')[params.split('%%').length - 1],
      sortRule: 2,
      pageSize: pst.per_page,
      pageNo: pst.page
    })
  });
};
export const getStoreGoods = (params, pst: any) => {
  return request.post<any>('/app.do', {
    method: params.split('%%')[0],
    ver: '1.0',
    params: JSON.stringify(
      Object.assign(
        {},
        params.split('%%')[0] === 'neweshop.special.getColumnStore'
          ? {
              id: params.split('%%')[params.split('%%').length - 1],
              sortRule: 2,
              pageSize: pst.per_page,
              pageNo: pst.page
            }
          : {
              id: params.split('%%')[params.split('%%').length - 1],
              pageSize: pst.per_page,
              pageNo: pst.page,
              dataType: 1,
              clientType: 1,
              sortRule: 1
            }
      )
    )
  });
};
