import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import Wrap from '../Wrap';
import Txt from '../Txt';
import Cnt from '../Cnt';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = () => {
  return (
    <View className="utp-OperateCart">
      <Wrap type={3}>
        <View className="utp-OperateCart-collection">
          <Cnt>
            <View style={{ width: Taro.pxTransform(55) }}>
              <Txt color="white" title="移入" size={26} />
            </View>
            <View style={{ width: Taro.pxTransform(55) }}>
              <Txt color="white" title="收藏" size={26} />
            </View>
          </Cnt>
        </View>
        <View className="utp-OperateCart-remove">
          <Cnt>
            <Txt color="white" title="删除" size={26} />
          </Cnt>
        </View>
        <View className="utp-OperateCart-cancel">
          <Cnt>
            <Txt color="deep" title="取消" size={26} />
          </Cnt>
        </View>
      </Wrap>
    </View>
  );
};

Index.defaultProps = {};

export default Index;
