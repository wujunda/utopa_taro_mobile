import request from '../utils/request';
// 积分商品列表
export const integralList = (param: any, pst: any) => {
  return request.post<any>('/app.do', {
    method: 'neweshop.point.getPointMallNew',
    ver: '1.0',
    params: JSON.stringify({
      ...param,
      pageSize: pst.per_page,
      pageNo: pst.page
    })
  });
};

// 优惠券可用商品列表
export const couponGoods = (param: any, pst: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.activity.getActiveProductList',
    ver: '2.0',
    params: JSON.stringify({
      ...param,
      pageSize: pst.per_page,
      pageNo: pst.page
    })
  });
};

// 折扣券详情
export const voucherDetail = (param) => {
  return request.post<any>('/app.do', {
    method: 'eshop.goodsSaleWay.getGoods',
    ver: '1.0',
    params: JSON.stringify({
      ...param
    })
  });
};

export const getPointExchangeGoodsDetail = (param) => {
  return request.post<any>('/app.do', {
    method: 'neweshop.goods.getPointExchangeGoodsDetail',
    ver: '1.0',
    params: JSON.stringify({
      ...param
    })
  });
};

// 积分订单提交
export const binConfirmOrder = (param) => {
  return request.post<any>('/app.do', {
    method: 'eshop.productorder.bin.app.bincommitOrder',
    ver: '1.0',
    params: JSON.stringify(param)
  });
};

// 积分规则
export const getUserPointRule = () => {
  return request.post<any>('/app.do', {
    method: 'eshop.useraction.getUserPointRule',
    ver: '1.0',
    params: JSON.stringify({})
  });
};
// 积分兑换数据列表
export const getColumnProduct = (param, pst: any) => {
  return request.post<any>('/app.do', {
    method: 'eshop.special.getColumnProduct',
    ver: '1.0',
    params: JSON.stringify({
      ...param,
      pageSize: pst.per_page,
      pageNo: pst.page
    })
  });
};
