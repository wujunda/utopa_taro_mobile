import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import goods from '../../assets/goods.png';
import add1 from '../../assets/cart/add1.png';
import Tag from '../Tag';
import Wrap from '../Wrap';
import Yuan from '../Yuan';

// import { images } from '../../images';

// eslint-disable-next-line no-unused-vars
import { ProductBaiscData, ProductStoreInfo } from '../../interface';
import { IGoods } from '../../interface/home';
// import Router from '../../utils/router';

import './index.scss';

type PageStateProps = {
  cart: any;
  item: ProductBaiscData;
};

type PageDispatchProps = {
  addToCart: (any) => void;
};

type PageOwnProps = {
  item: IGoods;
};

// interface Props extends ProductBaiscData, ProductStoreInfo {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = (props) => {
  const { item } = props;
  return (
    <View
      className="utp-goods2s"
      onClick={() => {
        let id = item.productId ? item.productId : item.id;
        let businessId = item.businessId ? item.businessId : item.goodsId;
        Taro.navigateTo({
          url: `/pages/details/index?id=${id}&businessId=${businessId}&storeId=${item.storeId}`
        });
      }}
    >
      <View className="utp-goods2 utp-div">
        <View className="utp-goods2-i utp-cnt">
          <Image className="utp-goods2-img" src={item.productImage || item.goodsLogo || goods} />
        </View>
        <View className="utp-goods2-item">
          <View className="utp-goods2-titles">
            <View
              className="utp-goods2-titles-tag"
              style={{ display: item && item.selfSupport ? 'block' : 'none' }}
            >
              <Tag>自营</Tag>
            </View>
            <Text
              className={
                item && item.selfSupport
                  ? 'utp-goods2-title taro-text utp-goods2-titleindex'
                  : 'utp-goods2-title taro-text'
              }
            >
              {item.productName ||
                item.goodsName ||
                '标题标题标题标题标题标标题标题标题标题标题标标题标题'}
            </Text>
          </View>
          <Wrap Myheight={30}>
            {item && item.activeLabel && <Tag type={1}>{item.activeLabel}</Tag>}
            {
              item && item.tags && item.tags.length > 0 && item.tags.map((ite, index) => {
                return (
                  <Tag type={4} key={index}>{ite}</Tag>
                )
              })
            }
          </Wrap>
          
          <View className="utp-goods2-ft">
            <Wrap type={1} justifyContent="space-between">
              <Wrap type={1}>
                <View className="utp-goods2-moneys">
                  <Yuan
                    price={item && (item.discountPrice || item.salePrice)}
                    size={22}
                    color="#FF2F7B"
                  ></Yuan>
                </View>
                <View
                  className="utp-goods2-money-1"
                  style={{
                    display: item && (item.originPrice || item.originalPrice) ? 'block' : 'none'
                  }}
                >
                  <Yuan
                    price={item && (item.originPrice || item.originalPrice)}
                    color="#999"
                  ></Yuan>
                </View>
              </Wrap>
              <View
                className="utp-goods2-add utp-cnt"
                onClick={(e) => {
                  e.stopPropagation();
                  props.addToCart({
                    productId: item.productId,
                    number: 1,
                    skuId: item.skuId,
                    storeId: item.storeId,
                    businessId: item.businessId
                  });
                }}
              >
                <Image className="utp-goods2-add-ico" src={add1} />
              </View>
            </Wrap>
          </View>
        </View>
      </View>
    </View>
  );
};
Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    cart: state.cart
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    addToCart: (payload) => {
      dispatch({
        type: 'cart/addCart',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
