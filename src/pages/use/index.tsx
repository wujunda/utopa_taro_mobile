import Taro, { usePageScroll, useRouter, useState, useDidShow, useEffect } from '@tarojs/taro';
import { Text, View, ScrollView } from '@tarojs/components';
import { connect } from '@tarojs/redux';

import Txt from '../../components/Txt';
import Wrap from '../../components/Wrap';
import Img from '../../components/Img';
import useRequest from '../../hooks/useRequest';
import Loading from '../../components/Loading';
import LoadingMore from '../../components/LoadingMore';
import LoadingNone from '../../components/LoadingNone';
import LoadingNoneMore from '../../components/LoadingNoneMore';
import useRequestWIthMore from '../../hooks/useRequestWIthMore';
import { getUseCate, getUseStore, ICate } from '../../services/use';
import { getFavStoreList } from '../../services/store';
import './index.scss';
import { IIcons } from '../../interface/home';
import SearchStore from '../../components/SearchStore';
import ItemHomeIcon from '../../components/ItemHomeIcon';

interface Props {
  statusHeight: number;
  closeWidth: number;
  unread: number;
  icons: IIcons[];
}
const Index: Taro.FC<Props> = ({ icons }) => {
  const [fix, setFix] = useState(false);
  const [top, setTop] = useState(0);
  usePageScroll((e) => {
    let scrollTop = e.scrollTop;
    if (scrollTop > top) {
      setFix(true);
    } else {
      setFix(false);
    }
  });

  useDidShow(() => {});

  // @ts-ignore
  const [cate, cateUpdate, cateLoading] = useRequest<ICate>(
    { id: useRouter().params.id },
    getUseCate
  );

  const [start, setStart] = useState('start');
  // @ts-ignore
  const [store, hasMore1, update1, getMoreData1, loading1] = useRequestWIthMore<IHistory>(
    start,
    getUseStore
  );
  const [like] = useRequest<any>({}, getFavStoreList);
  //
  const [id, setId] = useState('');
  const [init, setInit] = useState(false);
  useEffect(() => {
    console.log('数据');
    console.log(cate);
    if (cate && cate.code === 0) {
      console.log('初始化');
      if (!init) {
        setInit(true);
        setId(cate.data.list[0].columnNo);
      }
    }
  }, [cate]);

  // 获取菜单距离顶部高度
  useEffect(() => {
    if (cate && cate.code === 0) {
      setTimeout(() => {
        Taro.createSelectorQuery()
          .select('#utp-storeHome1-menus')
          .fields(
            {
              dataset: true,
              size: true,
              scrollOffset: true,
              properties: ['scrollX', 'scrollY']
            },
            function (res) {
              res.height; // 节点的高度
              setTop(res.height);
            }
          )
          .exec();
      }, 30);
    }
  }, [cate]);

  // id变化获取店铺信息
  useEffect(() => {
    console.log('id变化');
    if (id) {
      setStart('start1' + '%%' + id);
      console.log('id变化1');
      if (update1) {
        // @ts-ignore
        update1();
      }
    }
  }, [id]);

  let txts: any[] = [];
  if (cate && cate.code === 0 && cate.data && cate.data.list) {
    txts = cate.data.list;
  }

  var stores: any[] = [];
  if (store && store.length > 0) {
    store.map((item) => {
      if (item && item.data && item.data.records) {
        item.data.records.map((item) => {
          if (!item.productId) {
            stores.push(item);
          }
        });
      }
    });
  }
  var likes: any[] = [];
  if (like && like.code === 0) {
    like.data.records.map((item) => {
      likes.push(item);
    });
  }

  console.log(stores);

  return (
    <View className="utp-storeHome1">
      <View id="utp-storeHome1-menus">
        <View className="utp-storeHome1-likes">
          <Wrap type={1} justifyContent="space-between">
            <Wrap justifyContent="flex-start">
              <Txt title="已关注" size={28} color="deep" />
              {likes.map((item, index) => {
                if (index < 4) {
                  return (
                    <View style={{ marginLeft: Taro.pxTransform(24) }}>
                      <Img src={item.logoUrl} width={64} />
                    </View>
                  );
                }
              })}
            </Wrap>
            <View
              onClick={() => {
                Taro.navigateTo({
                  url: '/pages/service/shopConcern/index'
                });
              }}
            >
              <Text className="utp-storeHome1-likes-txt">查看</Text>
            </View>
          </Wrap>
        </View>
        <View>
          <View className="utp-storeHome1-give">
            <View className="utp-storeHome1-give-title">
              <Wrap type={1}>
                <Txt title="专属推荐" size={32} color="deep" />
              </Wrap>
            </View>
            <ScrollView className="utp-storeHome1-give-items" scrollX>
              {icons.map((item, index) => {
                if (index < 28) {
                  return (
                    <View className="utp-storeHome1-give-item0">
                      <View className="utp-storeHome1-give-item">
                        <ItemHomeIcon
                          tips={item.title}
                          src={item.source}
                          tipColor="#333"
                          onClick={() => {
                            // @ts-ignore
                            Router.goHome(item.command);
                          }}
                        />
                      </View>
                    </View>
                  );
                }
              })}
            </ScrollView>
          </View>
        </View>
      </View>
      <View className="utp-storeHome1-menus">
        <View
          className="utp-storeHome1-menus-1"
          style={{ position: fix ? 'static' : 'absolute' }}
        ></View>
        <ScrollView
          className="utp-storeHome1-menus-2"
          scrollX
          style={{ position: fix ? 'fixed' : 'static' }}
        >
          {txts.map((item) => {
            return (
              <View
                className="utp-storeHome1-menus-item"
                onClick={() => {
                  if (loading1) {
                    return;
                  }
                  setId(item.columnNo);
                }}
              >
                <Txt title={item.name} size={30} color={item.columnNo === id ? 'black' : 'low'} />
              </View>
            );
          })}
        </ScrollView>
      </View>
      <View>
        <View className="utp-storeHome1-box">
          {stores.length === 0 && (
            <View style={{ height: '1000px' }}>
              {loading1 && stores.length === 0 && <Loading />}
              {!loading1 && stores.length === 0 && <LoadingNone />}
            </View>
          )}
          {stores.map((item) => {
            return (
              <SearchStore
                item={item}
                key={item.storeId}
                like={() => {
                  //dolike(item);
                }}
              />
            );
          })}

          {loading1 && stores.length !== 0 && <LoadingMore />}
          {!loading1 && !hasMore1 && stores.length > 0 && <LoadingNoneMore />}
        </View>
      </View>
    </View>
  );
};
Index.config = {
  navigationBarTitleText: '编辑常用',
  disableScroll: false,
  backgroundColor: '#f6f6f6'
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight,
    closeWidth: state.cart.closeWidth,
    unread: state.cart.unread,
    icons: state.cart.icons
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
