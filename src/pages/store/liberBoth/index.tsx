import Taro, { useRouter } from '@tarojs/taro';
import { View } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import useRequest from '../../../hooks/useRequest';
import Router from '../../../utils/router';
import { ILiberBothData, getLiberBoth } from '../../../services/liber';
import { AtIndexes } from 'taro-ui';

import './index.scss';

type PageStateProps = {};

type PageDispatchProps = {
  // addCart: (any) => void;
};
type IProps = PageStateProps & PageDispatchProps;
const Index: Taro.FC<IProps> = () => {
  const id = useRouter().params.id;

  const [state] = useRequest<ILiberBothData>(id, getLiberBoth);

  let list: any[] = [];
  if (state && state.code === 0) {
    //list = state.data.banners;
    state.data.list.map((item) => {
      item.key = item.firstLetter;
      item.title = item.firstLetter;
      item.items = item.brandList;
      list.push(item);
    });
  }
  //const list = [
  //{
  //title: 'A',
  //key: 'A',
  //items: [
  //{
  //name: '阿坝'
  //// 此处可加其他业务字段
  //},
  //{
  //name: '阿拉善'
  //}
  //]
  //},
  //{
  //title: 'B',
  //key: 'B',
  //items: [
  //{
  //name: '北京'
  //},
  //{
  //name: '保定'
  //}
  //]
  //}
  //];

  return (
    <View className="uto-liberBoth">
      <View style="height:100vh">
        <AtIndexes
          list={list}
          onClick={(e) => {
            console.log(e);

            Router.goSearch(e.id, e.name, true);
          }}
        ></AtIndexes>
      </View>
    </View>
  );
};
Index.config = {
  navigationBarTitleText: '品牌索引',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f6f6f6'
  // 不显示标题
  //navigationStyle: 'custom'
};

const mapStateToProps: PageStateProps = (state) => {
  return {
    number: state.cart.number,
    post: state.cart.post,
    cart: state.cart,
    loading: state.loading.effects['cart/addCart']
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};
// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
