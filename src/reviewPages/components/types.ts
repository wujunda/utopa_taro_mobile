/*
 * @Description:
 * @Version: 1.0
 * @Autor: gaotongjian
 * @Date: 2020-04-26 11:29:09
 * @LastEditors: gaotongjian
 * @LastEditTime: 2020-04-29 19:13:19
 */

const goods = {
  goods_sku_goods1: 'goods-sku-goods1x1',
  goods_sku_goods2: 'goods-sku-goods1x2',
  goods_sku_goods3: 'goods-sku-goods1x3'
};

const imageText = {
  iamge_single: 'iamge_single',
  iamge_row_multiple: 'iamge_row_multiple'
};

const swiperType = {
  nav_swiper: 'nav_swiper'
};
const navBar = {
  nav_navbar: 'nav_navbar'
};
export const COM_TYPES = {
  ...goods,
  ...imageText,
  ...swiperType,
  ...navBar
};

export default COM_TYPES;
