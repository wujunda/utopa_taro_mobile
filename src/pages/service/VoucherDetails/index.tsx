import Taro, { useRouter } from '@tarojs/taro';
import { View, ScrollView, Text, Swiper, SwiperItem } from '@tarojs/components';

import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Img from '../../../components/Img';

import positionAddress from '../../../assets/positionAddress.png';
import { images } from '../../../images';
import useRequest from '../../../hooks/useRequest';
import { getFree, getInfo } from '../../../services/voucher';
import { Glo, parseTime } from '../../../utils/utils';
import Loading from '../../../components/Loading';
import LoadingNone from '../../../components/LoadingNone';
import Router from '../../../utils/router';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const id = useRouter().params.id;
  const id1 = useRouter().params.id1;
  // @ts-ignore
  const [state, update, loading] = useRequest<any>(id, getInfo);
  const onFree = async () => {
    let data = await getFree({
      goodsCode: state.data.goodsCode,
      shopGoodsId: id
    });
    if (data.code === 0) {
      Taro.showToast({
        title: '领取成功',
        icon: 'none'
      });
      setTimeout(() => {
        Router.goBack();
      }, 2000);
    } else {
      Taro.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
  };
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-Voucher"
            scrollY
          >
            {loading && !state && <Loading />}
            {!loading && !state && <LoadingNone />}
            {!!state && state.data && (
              <View>
                <View className="utp-Voucher-swipeBox">
                  <Swiper
                    className="utp-Voucher-swipe"
                    indicatorColor="#999"
                    indicatorActiveColor="#333"
                    circular
                    indicatorDots
                    autoplay
                  >
                    {state.data.goods.picUrls.map((item) => {
                      return (
                        <SwiperItem key={item}>
                          <Img src={item} width={1000}></Img>
                        </SwiperItem>
                      );
                    })}
                  </Swiper>
                </View>
                <View className="utp-Voucher-voucherTitleBox">
                  <Wrap justifyContent="space-between">
                    <View className="utp-Voucher-voucherTitles">
                      <Text className="utp-Voucher-voucherTitle">{state.data.goods.name}</Text>
                    </View>
                    <View className="utp-Voucher-gang"></View>
                    <View className="utp-Voucher-gang1">
                      <Wrap flexDirection="column" type={1}>
                        <Text className="utp-Voucher-voucherNum">{state.data.goods.qtyLimit}</Text>
                        <Text className="utp-Voucher-voucherSale">已售</Text>
                      </Wrap>
                    </View>
                  </Wrap>
                </View>
                <View className="utp-Voucher-addressBox">
                  <View>
                    <Text className="utp-Voucher-addressBox-titles">
                      {state.data.goods.shopName}
                    </Text>
                  </View>
                  <View className="utp-Voucher-addressBox-address">
                    <Wrap>
                      <View style={{ flex: 1 }}>
                        <Wrap type={1}>
                          <View onClick={() => {}}>
                            <Img src={positionAddress} width={36}></Img>
                          </View>
                          <Text className="utp-Voucher-addressBox-addressContent">
                            {state.data.goods.address}
                          </Text>
                        </Wrap>
                      </View>
                      <View className="utp-Voucher-gang"></View>
                      <View
                        className="utp-Voucher-gang1 utp-cnt"
                        onClick={() => {
                          Taro.makePhoneCall({
                            phoneNumber: state.data.tel //仅为示例，并非真实的电话号码
                          });
                        }}
                      >
                        <Img src={images.phone} width={48} />
                      </View>
                    </Wrap>
                  </View>
                </View>
                <View className="utp-Voucher-explain">
                  <Wrap flexDirection="column" top>
                    <View className="utp-Voucher-explain-part1">
                      <Text className="utp-Voucher-explain-titles">有效期</Text>
                      <Text className="utp-Voucher-explain-content">
                        {`${parseTime(state.data.goods.validStartTime)} 至 ${parseTime(
                          state.data.goods.validEndTime
                        )}`}
                      </Text>
                    </View>
                    <View className="utp-Voucher-explain-part2">
                      <Text className="utp-Voucher-explain-titles">使用时间</Text>
                      <Text className="utp-Voucher-explain-content">
                        {state.data.goods.useStartTime
                          ? state.data.goods.useStartTime
                          : '暂无相关信息'}
                      </Text>
                    </View>
                    <View className="utp-Voucher-explain-part3">
                      <Text className="utp-Voucher-explain-titles">使用说明</Text>
                      <Text className="utp-Voucher-explain-content">暂无相关信息</Text>
                    </View>
                    <View className="utp-Voucher-explain-part3">
                      <Text className="utp-Voucher-explain-titles">购物需知</Text>
                      <Text className="utp-Voucher-explain-content">暂无相关信息</Text>
                    </View>
                    <View className="utp-Voucher-explain-part3">
                      <Text className="utp-Voucher-explain-titles">服务介绍</Text>
                      <Text className="utp-Voucher-explain-content">暂无相关信息</Text>
                    </View>
                  </Wrap>
                </View>
                <View className="utp-Voucher-Receive">
                  <Wrap type={1} justifyContent="space-between">
                    <View className="utp-Voucher-Receive-left">
                      <Wrap type={3} flexDirection="column" top>
                        <View style={{ height: 30 }}>
                          <Wrap>
                            <Text className="utp-Voucher-Receive-priceIcon">￥</Text>
                            <Text className="utp-Voucher-Receive-price">
                              {Glo.formatPrice(state.data.goods.price)}
                            </Text>
                            <Text className="utp-Voucher-Receive-oldPrice">
                              ￥{Glo.formatPrice(state.data.goods.priceOrigin)}
                            </Text>
                          </Wrap>
                        </View>
                        <Text className="utp-Voucher-Receive-text">
                          领取成功后，可在我的-抵用券查看
                        </Text>
                      </Wrap>
                    </View>
                    <View
                      onClick={() => {
                        if (state.data.goods.price == 0) {
                          // 领取
                          onFree();
                        } else {
                          // 抢购
                          Taro.navigateTo({
                            url: `/pages/service/VoucherGet/index?id=${id1}`
                          });
                        }
                      }}
                    >
                      <Text className="utp-Voucher-Receive-right">
                        {state.data.goods.price == 0 ? '领取' : '立即抢购'}
                      </Text>
                    </View>
                  </Wrap>
                </View>
              </View>
            )}
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};
Index.config = {
  navigationBarTitleText: '抵用券详情',
  disableScroll: false,
  //enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
  // 不显示标题
  //navigationStyle: 'custom'
};
export default Index;
