/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from '@tarojs/taro';

// 初始化
if (process.env.TARO_ENV === 'h5') {
  require('../../lib/iframeResizer.contentWindow.min.js');
  global['iFrameResizer'] = {
    // targetOrigin: 'http://mydomain.com',
    onReady: function() {
      // console.log('[iFrameResizer]:onReadey');
    },
    onMessage(msg) {
      // console.log('收到消息', msg);
    }
  };
}
type GetPageInfoCallback = (properties: any) => void;

export interface ParentIFrame {
  autoResize: (auto: boolean) => void;
  close: () => void;
  getId: () => string;
  getPageInfo: (callback: GetPageInfoCallback | false) => void;
  scrollTo: (x, y) => void;
  scrollToOffset: (x, y) => void;
  sendMessage: (message: any, targetOrigin?: string) => void;
  setHeightCalculationMethod: () => void;
  size: (customHeight?: number, customWidth?: number) => void;
}

interface IFrameResizeOptions {
  onReady?: () => void;
  onMessage?: (msg: any) => void;
  onInit?: (parentIFrame: ParentIFrame) => void;
}

export const useParentIFrame = (options: IFrameResizeOptions = {}) => {
  const [parentIFrame, setParentIFrame] = useState<ParentIFrame | undefined>(
    global['parentIFrame']
  );

  useEffect(() => {
    const iFrameResizer = global['iFrameResizer'];
    const tempOnReady = iFrameResizer.onReady;
    const tempOnMessage = iFrameResizer.onMessage;

    if ('onReady' in options) {
      global['iFrameResizer'].onReady = options.onReady;
    }

    if ('onMessage' in options) {
      global['iFrameResizer'].onMessage = options.onMessage;
    }

    return () => {
      global['iFrameResizer'].onReady = tempOnReady;
      global['iFrameResizer'].onMessage = tempOnMessage;
    };
  }, []);

  useEffect(() => {
    let interval;
    interval = setInterval(() => {
      if ('parentIFrame' in global) {
        setParentIFrame(global['parentIFrame']);
        if (options.onInit) {
          options.onInit(global['parentIFrame']);
        }
        clearInterval(interval);
      }
    }, 200);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return parentIFrame;
};
