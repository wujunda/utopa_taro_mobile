import Taro, { useRouter, useEffect } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
import Router from '@/utils/router';
// import { connect } from '@tarojs/redux';

// type PageStateProps = {
//   loading: boolean;
// };

// type PageDispatchProps = {
//   // getOpenToken: () => void;
// };

// type PageOwnProps = {};

// type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
interface IProps {}

const Index: Taro.FC<IProps> = (props) => {
  const { params } = useRouter();

  useEffect(() => {
    if (typeof params === 'object' && params.cmd) {
      let data = {};
      try {
        if (params && typeof params.params === 'string') {
          data = JSON.parse(decodeURIComponent(params.params)) || {};
        }
      } catch (e) {
        console.error('解析参数失败', e)
      }
      console.log({ cmd: params.cmd, params: data, redirect: true })
      Router.goPage({ cmd: params.cmd, params: data, redirect: true }).catch((error) => {
        console.warn('跳转失败：', error);
        Taro.navigateBack();
      });
    }
  }, [params, props]);

  return <View />;
};

Index.config = {
  navigationBarTitleText: '',
  disableScroll: false,
  enablePullDownRefresh: false,
  backgroundColor: '#f5f5f5'
};

// @ts-ignore
export default Index;
