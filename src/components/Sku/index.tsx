/* eslint-disable react-hooks/exhaustive-deps */
import Taro, { useState, useEffect } from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import Img from './../Img';
//import add from './../../assets/assemble/add.png';
//import minus from './../../assets/assemble/minus.png';
import './index.scss';
//import goods from '../../assets/goods.png';
import Wrap from '../../components/Wrap';
import Txt from '../../components/Txt';
import Stepper from '../../components/Stepper';
import Bton from './../Bton';
import Yuan from './../Yuan';

import { connect } from '@tarojs/redux';
import { images } from '../../images';
import { Glo } from '../../utils/utils';

// #region 书写注意
// #endregion

type PageStateProps = {
  cart: any;
  goods: any;
  isXuni: boolean;
};

type PageDispatchProps = {
  addToCart: (goodsInfo) => void;
};

interface dataProps {
  productSides: any;
  productSkus: any[];
  skuImgDefault: string;
  skus: any[];
  stockSkus?: any;
}
type PageOwnProps = {
  id1: string;
  cartImage: string;
  data: dataProps;
  type?: number;
  show?: boolean;
  onClose?: () => void;
  closeOverlay?: () => void;
  goCart?: () => void;
  buy?: () => void;
  onJifen?: (any?) => void;
  cartNumber?: number;
  isCart?: boolean;
  changeCartSku?: (any) => void;
  onChangeSku?: (any) => void;
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

// 底部sku
const Index: Taro.FC<IProps> = ({
  goods,
  data,
  show = false,
  onClose,
  closeOverlay,
  type = 3,
  id1 = '',
  cartImage = '',
  cartNumber,
  changeCartSku,
  isCart = false,
  onJifen,
  onChangeSku,
  addToCart,
  isXuni = false
  //addToCart
}) => {
  let obj = Object.assign({}, data);
  const [initCart, setInitCart] = useState(false);
  const [state, setState] = useState(obj);
  //const [index, setIndex] = useState(0);
  const [qty, setQty] = useState(id1 ? cartNumber : 1);
  const [img, setImg] = useState(data && data.skuImgDefault);
  const [skuType, setSkuType] = useState(type);
  const [stock, setStock] = useState(data && data.productSkus[0].stock);
  const [price, setPrice] = useState(data && data.productSkus[0].price);
  const [oldprice, setoldPrice] = useState(data && data.productSkus[0].actuallyPrice);
  const [tip, setTip] = useState(data && data.productSkus[0].discountDesc);
  let skuId = '';
  const [skuObj, setSkuObj] = useState<any>('');
  useEffect(() => {
    console.log('初始化sku');
  }, []);
  const [one, setOne] = useState(false);
  const getNow = (i, ite) => {
    let now;
    let arr: any = [];
    arr.push(ite.normValueCode);
    state.skus.map((item, index) => {
      item.normValues.map((item1) => {
        if (item1.isCrt && index != i) {
          arr.push(item1.normValueCode);
        }
      });
    });
    let crt = arr.join('#');
    for (var x = 0; x < state.productSkus.length; x++) {
      let obj = state.productSkus[x];
      obj.crts = [];
      arr.map((item) => {
        if (obj.productConfirm.indexOf(item) > -1) {
          obj.productConfirm.replace(item, '');
          obj.crts.push(item);
        }
      });
      // console.log('开始匹配');
      if (obj.crts.length > 0 && obj.crts.length === arr.length && obj.crts.join('#') === crt) {
        // console.log('匹配成功');
        now = obj;
      }
    }
    return now;
  };
  // type=>2秒杀    type =>1  折扣  type=>3 拼团商品  type => 0售罄 库存为0
  useEffect(() => {
    if (state && state.skus && state.skus.length > 0) {
      let arr: any = [];
      state.skus.map((item) => {
        item.normValues.map((item1) => {
          if (item1.isCrt) {
            arr.push(item1.normValueCode);
          }
        });
      });
      let crt = arr.join('#');
      for (var x = 0; x < state.productSkus.length; x++) {
        let obj = state.productSkus[x];
        obj.crts = [];
        arr.map((item) => {
          if (obj.productConfirm.indexOf(item) > -1) {
            obj.productConfirm.replace(item, '');
            obj.crts.push(item);
          }
        });
        // console.log('开始匹配');
        if (obj.crts.length > 0 && obj.crts.length === arr.length && obj.crts.join('#') === crt) {
          // console.log('匹配成功');
          setSkuObj(obj);
          skuId = obj.id;
          setSkuType(obj.skuType);
          setStock(obj.stock);

          setSkuType(obj.skuType);
          if (obj.skuType === 3) {
            setPrice(obj.groupPrice);
            setoldPrice(obj.discountPrice);
          } else {
            setPrice(obj.price);
            setoldPrice(obj.originalPrice);
          }
          setTip(obj.discountDesc);

          if (!one) {
            if (onChangeSku) {
              setOne(true);
              onChangeSku(obj);
            }
          }
          break;
        }
      }
    }
    // console.log('skuID');
    // console.log(skuId);
  }, [state]);
  // 默认选中第一个
  let haveDef = false;
  if (!haveDef && state && state.skus) {
    state.skus.map((item) => {
      item.normValues.map((item1) => {
        if (item1.isCrt) {
          haveDef = true;
        }
      });
    });
  }
  if (!id1 && !haveDef && state && state.skus) {
    // console.log('默认选择第一个1');
    state.skus.map((item) => {
      // console.log('默认选择第一个2');
      item.normValues.map((item1, index1) => {
        // console.log('默认选择第一个3');
        if (index1 === 0) {
          // console.log('默认选择第一个4');
          item1.isCrt = true;
        }
      });
    });
    let obj = Object.assign({}, state);
    setState(obj);
  }
  // 购物车选sku
  if (id1 && !initCart && state && state.skus) {
    // console.log(id1);
    let crtStr = '';
    for (var x = 0; x < state.productSkus.length; x++) {
      if (state.productSkus[x].id === id1) {
        crtStr = state.productSkus[x].productConfirm;
      }
    }

    state.skus.map((item) => {
      item.normValues.map((item1) => {
        if (crtStr.indexOf(item1.normValue) > -1) {
          item1.isCrt = true;
        }
      });
    });
    let obj = Object.assign({}, state);
    setState(obj);
    setInitCart(true);
    setImg(cartImage);
  }
  // console.log('sku数据');
  // console.log(data);
  return (
    <View
      className={show ? 'utp-sku utp-sku-active' : 'utp-sku'}
      onClick={(e) => {
        e.stopPropagation();

        if (onChangeSku) {
          onChangeSku(skuObj);
        }
        if (onClose) {
          onClose();
        }
      }}
    >
      <View className="utp-skuoverlay" onClick={closeOverlay}></View>
      <View
        className="utp-sku-container layout"
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <View
          className="layout-header "
          onClick={(e) => {
            e.stopPropagation;
            console.log('关闭');
            if (onChangeSku) {
              console.log('关闭1');
              onChangeSku(skuObj);
            }
            if (onClose) {
              console.log('关闭');
              onClose();
            }
          }}
        >
          <Image src={images.close} className="close-img" />
        </View>
        <View className="utp-skuflex">
          <Img src={img || ''} width={180} />
          {type === 4 ? (
            <View className="utp-info">
              <View className="utp-info-aprice">
                <Text>
                  {' '}
                  {`${skuObj.exchangePoint}积分+¥${Glo.formatPrice(skuObj.exchangePrice)}`}
                </Text>
              </View>
              <Wrap>
                <Text className="utp-info-price utp-info-price2"></Text>
              </Wrap>
            </View>
          ) : null}

          {skuType === 3 ? (
            <View className="utp-info">
              <View className="utp-info-aprice">
                <Text> 拼团价: </Text>
                <View className="utp-info-unit">
                  <Yuan price={price} color="#FF2F7B" size={20}></Yuan>
                </View>
              </View>
              <Wrap>
                <Text className="utp-info-price utp-info-price2">原价:</Text>
                <View className="utp-info-price">
                  <Yuan price={oldprice} size={28}></Yuan>
                </View>
              </Wrap>
            </View>
          ) : null}
          {skuType === 1 ? (
            <View className="utp-info">
              <View className="utp-info-aprice">
                <Text> 折扣价:</Text>
                <View className="utp-info-unit">
                  <Yuan price={price} color="#FF2F7B" size={20}></Yuan>
                </View>
              </View>
              <Wrap>
                <Text className="utp-info-price utp-info-price2">原价:</Text>
                <View className="utp-info-price utp-info-price2">
                  <Yuan price={oldprice} size={28}></Yuan>
                </View>
              </Wrap>
            </View>
          ) : null}
          {skuType === 2 ? (
            <View className="utp-info">
              <View className="utp-info-aprice">
                <Text> 秒杀价: </Text>
                <View className="utp-info-unit">
                  <Yuan price={price} color="#FF2F7B" size={20}></Yuan>
                </View>
              </View>
              <Wrap>
                <Text className="utp-info-price utp-info-price2">原价:</Text>
                <View className="utp-info-price utp-info-price2">
                  <Yuan price={oldprice} size={28}></Yuan>
                </View>
              </Wrap>
            </View>
          ) : null}
          {skuType === 0 ? (
            <View className="utp-info">
              <View className="utp-info-aprice">
                <Yuan price={oldprice} color="#FF2F7B" size={20}></Yuan>
              </View>
            </View>
          ) : null}
        </View>
        {data &&
          data.skus.map((item, index) => {
            return (
              <View
                className="utp-props"
                key={index}
                onClick={(e) => {
                  e.stopPropagation();
                }}
              >
                <Wrap type={2}>
                  <Txt title={item && item.normName} color="deep" size={26} bold />
                </Wrap>
                <View className="utp-norms">
                  {item &&
                    item.normValues.map((item1, index1) => {
                      let now = getNow(index, item1);
                      let none = false;
                      if (now.stock === 0) {
                        none = true;
                      }
                      return (
                        <Text
                          key={index1}
                          onClick={(e) => {
                            e.stopPropagation();
                            //setIndex(index1);
                            if (!none) {
                              item.normValues.map((ite) => {
                                ite.isCrt = false;
                              });
                              item1.isCrt = true;
                              let obj = Object.assign({}, state);
                              setState(obj);

                              item1.url && item1.url != null && setImg(item1.url);
                            }
                          }}
                          className={
                            none
                              ? 'utp-norms-item utp-norms-unuseable'
                              : item1.isCrt
                              ? 'utp-norms-item utp-norms-active tp-solid'
                              : 'utp-norms-item'
                          }
                        >
                          {item1.normValue}
                        </Text>
                      );
                    })}
                </View>
              </View>
            );
          })}
        {type !== 4 && (
          <View className="utp-props utp-props-num">
            <Txt title="数量" color="deep" size={26} bold />
            <View className="utp-props-step">
              <Stepper
                qty={qty}
                onAdd={() => {
                  if (skuObj) {
                    if ((qty ? qty + 1 : 0) <= skuObj.stock) {
                      let a = qty ? qty + 1 : 1;
                      setQty(a);
                    }
                  }
                }}
                onMinus={() => {
                  let a = qty ? qty - 1 : 1;
                  setQty(a);
                }}
              />
            </View>
          </View>
        )}
        <View className="utp-props utp-props-limitTip">
          {tip && tip != null && <Txt title={tip} color="red" justifyContent="flex-end" />}
        </View>
        <View className="utp-props-leave">
          <Txt title={`商品库存剩余${stock}件`} color="low" />
        </View>
        {skuType === 3 && !isCart ? (
          <View className="utp-skubtn">
            <View
              className="utp-skubtn-item utp-skubtn-alone"
              onClick={() => {
                if (onJifen) {
                  if (qty) {
                    onJifen(qty, skuObj);
                  }
                }
              }}
            >
              <Txt title={oldprice / 100} size={32} color="white" bold />
              <Txt title="单独购买" size={22} color="white" />
            </View>
            <View
              className="utp-skubtn-item utp-skubtn-assemble"
              onClick={() => {
                if (onJifen) {
                  if (qty) {
                    onJifen(qty, skuObj);
                  }
                }
              }}
            >
              <Txt title={price / 100} size={32} color="white" bold />
              <Txt title="我要开团" size={22} color="white" />
            </View>
          </View>
        ) : null}
        {!id1 && skuObj && skuObj.stock != 0 && skuType === 4 && !isCart ? (
          <View className="utp-skubtn">
            <View
              className="utp-skubtn-disabled"
              onClick={() => {
                if (onChangeSku) {
                  console.log('修改sku');
                  console.log(skuObj);
                  onChangeSku(skuObj);
                }
                if (onJifen) {
                  setTimeout(() => {
                    onJifen(qty, skuObj);
                  }, 30);
                }
              }}
            >
              <Bton>确定</Bton>
            </View>
          </View>
        ) : null}

        {isXuni && (skuType === 2 || skuType === 1 || skuType === 0) && !isCart ? (
          <View className="utp-skubtn">
            <View
              className="utp-skubtn-item utp-skubtn-assemble utp-skubtn-disabled"
              onClick={() => {
                if (onJifen) {
                  if (qty) {
                    onJifen(qty, skuObj);
                  }
                }
              }}
            >
              <Txt title="立即购买" size={22} color="white" />
            </View>
          </View>
        ) : null}

        {!isXuni && (skuType === 2 || skuType === 1 || skuType === 0) && !isCart ? (
          <View className="utp-skubtn">
            <View
              className="utp-skubtn-item utp-skubtn-alone"
              onClick={() => {
                // console.log('触发更改数量');
                addToCart({
                  productId: goods.productId,
                  number: qty,
                  skuId: skuObj.id,
                  storeId: goods.storeId,
                  businessId: goods.businessId
                });
                if (onClose) {
                  if (onChangeSku) {
                    onChangeSku(skuObj);
                  }
                  onClose();
                }
              }}
            >
              <Txt title="加入购物车" size={22} color="white" />
            </View>
            <View
              className="utp-skubtn-item utp-skubtn-assemble"
              onClick={() => {
                if (onJifen) {
                  if (qty) {
                    onJifen(qty, skuObj);
                  }
                }
              }}
            >
              <Txt title="立即购买" size={22} color="white" />
            </View>
          </View>
        ) : null}
        {!id1 && skuObj && skuObj.stock == 0 ? (
          <View className="utp-skubtn">
            <View className="utp-skubtn-disabled">
              <Bton type="none"> 已售罄 </Bton>
            </View>
          </View>
        ) : null}
        {id1 && isCart ? (
          <View className="utp-skubtn">
            <View
              className="utp-skubtn-disabled"
              onClick={() => {
                if (changeCartSku) {
                  changeCartSku({
                    skuId: skuObj.id,
                    number: qty
                  });
                }
                if (onClose) {
                  if (onChangeSku) {
                    onChangeSku(skuObj);
                  }
                  onClose();
                }
              }}
            >
              <Bton> 确定 </Bton>
            </View>
          </View>
        ) : null}
      </View>
    </View>
  );
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    cart: state.cart
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    addToCart: (payload) => {
      dispatch({
        type: 'cart/addCart',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
