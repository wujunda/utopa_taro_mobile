import Taro, { pxTransform } from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import black from '@/assets/special/back_black.png';
import { COMMON_IMAGES } from '../../common/images';
// eslint-disable-next-line import/first
import JSBridge from '@/utils/jsbridge/index';
import './index.scss';

const Header = (props) => {
  const {
    style,
    title,
    color,
    bgColor,
    rightBox,
    rightClick,
    hasShare,
    isApp,
    topOpa,
    titleColor = '#333333'
  } = props;

  let appStyle = isApp && {
    position: 'fixed',
    top: 0,
    zIndex: 11111,
    width: '100%',
    left: 0,
    height: pxTransform(176),
    ...style
  };
  const defaultBcolor = '#fff';
  // const backgroundColor = 'rgba(255,91,143,' + 0 + ')';
  return (
    <View style={{ ...appStyle, background: topOpa && topOpa === 2 ? defaultBcolor : '' }}>
      <View
        className="app-header"
        style={{
          color: color,
          marginTop: isApp ? Taro.pxTransform(88) : '',
          bgColor: bgColor || defaultBcolor,
          ...style,
          background: topOpa && topOpa === 2 ? defaultBcolor : ''
        }}
      >
        <Image
          className="app-header-back"
          src={topOpa && topOpa === 2 ? black : COMMON_IMAGES.back_white}
          onClick={() => {
            if (isApp) {
              // 返回上一页 app
              console.log('app 返回');
              JSBridge.Common.GTBridge_Common_Base_back({}, () => {
                console.log('app 成功');
              });
            } else {
              Taro.navigateBack();
            }
          }}
        />
        <Text
          className="app-header-title"
          style={{ color: topOpa && topOpa === 2 ? titleColor : color }}
        >
          {title}
        </Text>
        <View className="app-header-right" onClick={() => rightClick && rightClick()}>
          {hasShare && (
            <Image
              style={{ height: pxTransform(48), width: pxTransform(48) }}
              src={
                topOpa && topOpa === 2
                  ? COMMON_IMAGES.navigation_bar_icon_back
                  : COMMON_IMAGES.navigation_bar_icon_back_b
              }
            ></Image>
          )}
          {rightBox && rightBox}
        </View>
      </View>
    </View>
  );
};

export default Header;
