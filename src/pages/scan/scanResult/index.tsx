import Taro, { useRouter, useState, useDidShow } from '@tarojs/taro';
import { connect } from '@tarojs/redux';
import { View, Text } from '@tarojs/components';
import Safe from '../../../components/safe';
import Box from '../../../components/Box';
import Good from '../../../components/Goods4';
import Bar from '../../../components/bar';
import cart from './../../../assets/cart/bCart.png';
import Loading from '../../../components/Loading';

import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import useRequest from '../../../hooks/useRequest';
import { ICodeGoods, ICodeRst, getAddCode, getCodesNumber } from '../../../services/scan';

import './index.scss';

type PageStateProps = {
  loading: boolean;
  statusHeight: number;
};

type PageDispatchProps = {
  syncCart: () => void;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
const Index: Taro.FC<IProps> = ({ statusHeight }) => {
  const router = useRouter();
  let id = router.params.id;
  // @ts-ignore
  const [state, hasMore, update, getMoreData, loading] = useRequestWIthMore<ICodeRst>(
    { id },
    getAddCode
  );
  const [state1, update1] = useRequest<any>('', getCodesNumber);
  const [num, setNum] = useState(0);
  if (state1 && state1.code === 0) {
    setNum(state1.data.number);
  }
  useDidShow(() => {
    if (update1) {
      update1();
    }
  });

  var goods: ICodeGoods[] = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.data.records.map((item) => {
        goods.push(item);
      });
    });
  }
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body utp-scanResult">
          <View
            className="utp-scanResult-hd"
            style={{ paddingTop: Taro.pxTransform(statusHeight * 2) }}
          >
            <Bar
              num={num}
              goBack
              title="扫码结果"
              rightIcon={cart}
              onClick={() => {
                if (num) {
                  console.log('跳购物车1');
                  Taro.navigateTo({ url: '/pages/scan/scanCodeCart/index' });
                }
              }}
            />
          </View>
          <View
            style={{
              marginTop: Taro.pxTransform(statusHeight * 2 + 88)
            }}
          >
            <View style={{ width: Taro.pxTransform(750) }}></View>

            {loading && !state && <Loading />}
            {false && (
              <Text className="utp-scanResult-topTip">开启定位后可以更精确地获取与店铺的距离</Text>
            )}
            {!!state && <Text className="utp-scanResult-want">你是不是想买</Text>}
            <Box>
              <View className="utp-body utp-scanResult-box">
                {goods.map((item) => {
                  return <Good item={item} key={item.productName} />;
                })}
              </View>
            </Box>
          </View>
        </View>
      </View>
    </Safe>
  );
};
Index.config = {
  navigationBarTitleText: '扫码结果',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5',
  // 不显示标题
  navigationStyle: 'custom'
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    post: state.cart.post,
    statusHeight: state.cart.statusHeight
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    getSubmit: (payload) => {
      dispatch({
        type: 'cart/getSubmit',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
