import Taro from '@tarojs/taro';
// import { View } from '@tarojs/components';
import Wrap from '../Wrap';
import UtpImg from '../Img';

interface ImageItem {
  id?: string;
  src: string;
  width?: number; // 百分比
  height?: number | string;
}

interface Props{
  dataSource: ImageItem[];
}

const RowMultipleImage: Taro.FC<Props> = ({ dataSource }) => {
  return (
    <Wrap justifyContent="flex-start" alignItems="flex-start">
      {Array.isArray(dataSource) && dataSource.map((item, idx) => (
        <UtpImg key={item.id || idx} src={item.src} width={item.width} height={item.height} />
      ))}
    </Wrap>
  )
}

export default RowMultipleImage;