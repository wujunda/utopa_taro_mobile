import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

import './index.scss';
import Txt from '../Txt';
import Img from '../Img';
import Wrap from '../Wrap';
import right from '../../assets/right.png';

interface IProps {
  type?: 0 | 1;
  title: string;
}
const Index: Taro.FC<IProps> = ({ title, type = 0 }) => {
  return (
    <View className="utp-more utp-solid">
      <Wrap type={1} justifyContent="space-between">
        <Txt title={title} size={30} color="deep" />
        {type === 0 && <Img src={right} width={46} />}
        {type === 1 && (
          <View style={{ paddingRight: Taro.pxTransform(8) }}>
            <Txt title="131M" size={30} color="low" />
          </View>
        )}
      </Wrap>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
