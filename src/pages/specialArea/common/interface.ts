export interface GoodsItem {
  productImage: string;
  productName: string;
  salePrice: number;
  saleStatus: number;
  storeName: string;
  productId: number;
  originPrice: number;
}
export interface swiperItem {
  image: string;
  id: string;
}
