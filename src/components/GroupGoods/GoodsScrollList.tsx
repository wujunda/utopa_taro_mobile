import Taro from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';
// import Goods1 from '../Goods1';
import Goods2 from '../Goods2';
import Goods3 from '../Goods3';

// eslint-disable-next-line no-unused-vars
import { ProductBaiscData, ProductStoreInfo } from '../../interface';

type dataSource = ProductBaiscData & ProductStoreInfo;

type GoodsTypes = '1x2' | '1x3';

interface IProps {
  // 数据
  dataSource: dataSource[];

  // 展示行数，0 代表不限制
  showRowCount?: number | false;

  // 商品组件类型
  type?: GoodsTypes;
}

const rowCount: Record<GoodsTypes, number> = {
  '1x2': 2,
  '1x3': 3
};

const GoodsScrollList: Taro.FC<IProps> = ({ dataSource, showRowCount, type = '1x1' }) => {
  let showCount: number | undefined;
  if (showRowCount) {
    showCount = rowCount[type] * showRowCount;
  }

  // 超过长度进行截取
  const list = Array.isArray(dataSource)
    ? showCount
      ? dataSource.slice(0, showCount)
      : dataSource
    : [];

  // console.log({ list });

  return (
    <ScrollView scrollX>
      {list.map((goodsData) => {
        if (type === '1x2') {
          return <Goods2 key={goodsData.id} {...goodsData} />;
        }

        if (type === '1x3') {
          return <Goods3 key={goodsData.id} {...goodsData} />;
        }

        return null;
      })}
    </ScrollView>
  );
};

GoodsScrollList.defaultProps = {
  showRowCount: false,
  type: '1x2',
  dataSource: []
};

export default GoodsScrollList;

