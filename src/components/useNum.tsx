import { useState, useEffect } from '@tarojs/taro';

interface Props {
  name: string;
}

const useStore = (props: Props) => {
  // console.log(props);
  const [state] = useState(333);

  useEffect(() => {
    // console.log('变化');
  });

  return [state];
};

export default useStore;
