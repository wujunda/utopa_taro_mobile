import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import Wrap from '../../components/Wrap';
import Goods2 from '../../components/Goods2';
import './index.scss';
import { IGoods } from '../../interface/home';

interface Props {
  data: IGoods[];
  bg?: string;
  border?: number;
}
const Index: Taro.FC<Props> = ({ data, bg = '#f4f4f4', border = 1 }) => {
  return (
    <View
      style={{
        borderWidth: Taro.pxTransform(border),
        borderColor: bg,
        position: 'relative'
      }}
      className="utp-solid"
    >
      <View>
        <View
          style={{
            backgroundColor: bg,
            paddingLeft: Taro.pxTransform(15),
            paddingTop: Taro.pxTransform(15),
            paddingBottom: Taro.pxTransform(15)
          }}
        >
          <Wrap type={6}>
            {data &&
              data.map((item) => {
                return (
                  <View
                    key={item.id}
                    style={{
                      marginBottom: Taro.pxTransform(15),
                      marginRight: Taro.pxTransform(15)
                    }}
                  >
                    <Goods2 item={item} />
                  </View>
                );
              })}
          </Wrap>
        </View>
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
