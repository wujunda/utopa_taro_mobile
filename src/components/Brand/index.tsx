import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

import './index.scss';

interface IProps {}
const Index: Taro.FC<IProps> = ({}) => {
  return (
    <View
      className="utp-brand"
      onClick={() => {
        Taro.navigateTo({
          url: '/pages/details/index'
        });
      }}
    ></View>
  );
};
Index.defaultProps = {};

export default Index;
