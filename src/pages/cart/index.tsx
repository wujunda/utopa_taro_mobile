import Taro, { useState, useDidShow, useEffect } from '@tarojs/taro';
import { View, ScrollView, Image } from '@tarojs/components';
import { connect } from '@tarojs/redux';

import Safe from '../../components/safe';
import Goods from '../../components/Goods';
import Bar from '../../components/bar';
import CartEdit from '../../components/CartEdit';
import CartSub from '../../components/CartSub';
import Wrap from '../../components/Wrap';
import Img from '../../components/Img';
import Cnt from '../../components/Cnt';
import Txt from '../../components/Txt';
import Tag from '../../components/Tag';
import Yuan from '../../components/Yuan';
import Goods2s from '../../components/Goods2s';
import { IGoods } from '../../interface/home';
import Bton from '../../components/Bton';
import Model from '../../components/Model';
import './index.scss';
// import ItemMsg from '@/components/ItemMsg';
//import useRequest from '../../hooks/useRequest';
//import useSession from '../../hooks/useSession';
//import useLogin from '../../hooks/useLogin';
//import useResetCount from '../../hooks/useResetCount';
import {
  getMoveCart,
  getDelCart,
  ShopCartDtos,
  bussinessCouponList,
  userGetCoupon,
  deleteShopCartPro
} from '../../services/cart';
import Router from '../../utils/router';
import { images } from '../../images';
import Card from '../../components/Card';

type PageStateProps = {
  isWeiXinFalg: boolean;
  loading: boolean;
  cart: any;
  session: any;
  isLogin: boolean;
  statusHeight: number;
  closeWidth: number;
};

type PageDispatchProps = {
  syncCart: () => void;
  getSubmit: (any) => void;
  checkStore: (any) => void;
  checkCart: (any) => void;
  savePost: (any) => void;
  getCategoryList: () => void;
  saveSession: (any) => void;
  submitBefore: () => void;
  deleteShopCartPro: () => void;
};

type PageOwnProps = {};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
const Index: Taro.FC<IProps> = (props) => {
  const [show, setShow] = useState<boolean>(false);
  // console.log(props);
  useDidShow(() => {
    // console.log('购物车shuj ');
    if (props.isLogin) {
      props.syncCart();
    }
    props.getCategoryList();
  });
  useEffect(() => {
    //if (props.isLogin) {
    //props.syncCart();
    //}
    //props.getCategoryList();
  }, []);
  const [update, setUpdate] = useState(1);
  //const [haveSession] = useSession();
  const [edit, setEdit] = useState(false);
  const [open, setOpen] = useState(false);
  const [storeId, setStoreId] = useState('');
  // 购物车数据
  //const [state, update, loading] = useRequest<ICart>('name=a&age=1', getCart);
  const [one, setOne] = useState(1);
  if (
    props.cart.carts.code !== 0 &&
    //haveSession &&
    props.isLogin &&
    one === 1
    //(state === null || (state != null && state.code !== 0)) &&
    //update &&
    //!loading &&
  ) {
    setOne(2);
    props.syncCart();
  }
  // 数据
  let cartList: ShopCartDtos[] = props.cart.cartList;
  let categoryList: IGoods[] = props.cart.categoryList;
  let carts = props.cart.carts;
  let isAllSelect = false;
  let all = true;
  cartList.map((item) => {
    if (item.category !== 'uneffectProduct') {
      item.productDetails.map((item1) => {
        // console.log('全选循环');
        if (item1.isSelect !== 1) {
          all = false;
        }
      });
      // console.log(all);
    }
  });
  isAllSelect = all;
  // 提交订单
  const onSubmit = async () => {
    props.submitBefore();
    // console.log(Router);
    let goods: any = [];
    cartList.map((item) => {
      if (item.businessId) {
        let obj: any = {};
        obj.businessId = item.businessId;
        obj.isUseOriginPrice = 0;
        obj.sysId = props.isWeiXinFalg ? 1002 : 1001;
        obj.storeId = item.storeId;
        obj.sysId = 1004;
        let goodsArr: any = [];
        item.productDetails.map((item1) => {
          if (item1.isSelect === 1) {
            let obj: any = {};
            obj.skuId = item1.productId;
            obj.skuNum = item1.number;
            goodsArr.push(obj);
          }
        });
        if (goodsArr.length > 0) {
          obj.products = goodsArr;
          //obj.receiptAddress = {};
          goods.push(obj);
        }
      }
    });
    let post: any;
    if (goods.length > 0) {
      post = {
        binComits: goods,
        clientType: 1,
        sysId: 1
      };
      Taro.showLoading({
        title: ''
      });
      props.getSubmit({ post, type: 1 });
      props.savePost({
        data: post
      });
    } else {
      // 没有选中商品
    }
  };

  if (props.loading) {
    Taro.showLoading({
      title: ''
    });
  } else {
    Taro.hideLoading();
  }
  const [editAll, setEditAll] = useState(false);
  const [delIds, setDelIds] = useState('');
  const [moveStores, setMoveStores] = useState('');
  const [moveIds, setMoveIds] = useState('');
  const onEditChange = (rst?: boolean) => {
    let all = true;
    let delIds: any = [];
    let moveStores: any = [];
    let moveIds: any = [];
    cartList.map((item) => {
      item.isCrt = true;
      item.productDetails.map((item1) => {
        if (rst === true) {
          item1.isCrt = true;
        } else if (rst === false) {
          item1.isCrt = false;
        }
        if (!item1.isCrt) {
          item.isCrt = false;
          all = false;
        }
        if (item1.isCrt) {
          delIds.push(item1.id);
          moveStores.push(item1.storeId);
          moveIds.push(item1.proId);
        }
      });
    });
    setEditAll(all);
    setDelIds(delIds.join(','));
    setMoveStores(moveStores.join(','));
    setMoveIds(moveIds.join(','));
  };
  const onDel = async (type?) => {
    console.log('删除商品');
    console.log(type);
    console.log(delIds);
    let ids: any = [];
    if (type) {
      cartList.map((item) => {
        if (item.category === 'uneffectProduct') {
          item.productDetails.map((item1) => {
            ids.push(item1.id);
          });
        }
      });
    } else {
      if (!delIds) {
        Taro.showToast({
          title: '请选择需要删除的商品',
          icon: 'none'
        });
        return;
      }
    }
    Taro.showLoading({
      title: ''
    });
    let rst = await getDelCart({
      ids: type ? ids.join(',') : delIds
    });
    Taro.hideLoading();
    if (rst.code === 0) {
      props.syncCart();
      onEditChange();
      if (!type) {
        setEdit(false);
        setTimeout(() => {
          Taro.showToast({
            title: '删除商品成功',
            icon: 'none'
          });
        }, 300);
      }
    }
  };
  const onMove = async () => {
    // console.log('移入收藏');
    if (!moveIds) {
      Taro.showToast({
        title: '请选择商品',
        icon: 'none'
      });
      return;
    }
    Taro.showLoading({
      title: ''
    });
    let rst = await getMoveCart({
      favoriteIds: moveIds,
      storeIds: moveStores
    });
    Taro.hideLoading();
    if (rst.code === 0) {
      props.syncCart();
      onEditChange();
      setEdit(false);
      setTimeout(() => {
        Taro.showToast({
          title: '移入收藏成功',
          icon: 'none'
        });
      }, 300);
    }
  };
  let haveGoods = false;
  if (cartList.length > 0 && cartList[0].category !== 'uneffectProduct') {
    haveGoods = true;
  }
  console.log('商品');
  console.log(props.cart.cartList);

  const [couponList, setCouponList] = useState([]);
  const getBussinessCouponList = async (payload) => {
    let res = await bussinessCouponList(payload);
    if (res.code === 0) {
      setCouponList(res.data.coupons);
    }
  };
  return (
    <Safe>
      <View className="utp-html">
        <View
          className="utp-body"
          style={{
            background: '#F5F5F5',
            top: props.statusHeight + 'px',
            paddingTop: process.env.TARO_ENV === 'weapp' ? 0 : props.statusHeight + 'px'
          }}
        >
          <Bar
            title="购物车"
            rightTitle={props.isLogin ? (haveGoods ? (!edit ? '编辑' : '完成') : '') : ''}
            onClick={() => {
              setEdit(!edit);
            }}
          />
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-cart-page"
            scrollY
          >
            <View>
              {!!props.isLogin && haveGoods && (
                <View>
                  {props.cart.cartList.map((item, inn) => {
                    let isCheck = true;
                    let goods: number[] = [];
                    item.productDetails.map((item1) => {
                      goods.push(item1.id);
                      if (item1.isSelect === 0) {
                        isCheck = false;
                      }
                    });
                    if (item.category === 'uneffectProduct' && item.productDetails.length === 0) {
                      return;
                    }
                    return (
                      <View key={item.category}>
                        {item.category === 'uneffectProduct' && item.productDetails.length > 0 && (
                          <View style={{ marginBottom: Taro.pxTransform(-20) }}>
                            <Wrap Myheight={80} justifyContent="center">
                              <Txt title="- 以下商品因配送范围或下架导致失效 -" />
                            </Wrap>
                          </View>
                        )}
                        <View
                          className="utp-cart-page-item"
                          style={{ paddingBottom: item.category === 'uneffectProduct' ? '6px' : 0 }}
                        >
                          <Wrap Myheight={100} justifyContent="space-between">
                            <Wrap>
                              <View
                                style={{
                                  width:
                                    item.category !== 'uneffectProduct'
                                      ? Taro.pxTransform(76)
                                      : Taro.pxTransform(20)
                                }}
                                onClick={() => {
                                  if (!edit) {
                                    props.checkStore({
                                      storeId: item.storeId,
                                      isSelect: isCheck ? 0 : 1,
                                      ids: goods
                                    });
                                  } else {
                                    item.isCrt = !item.isCrt;
                                    item.productDetails.map((item1) => {
                                      if (item.isCrt) {
                                        item1.isCrt = true;
                                      } else {
                                        item1.isCrt = false;
                                      }
                                      // console.log('修改之后的值0');
                                      // console.log(item1);
                                    });
                                    // console.log('修改之后的值');
                                    // console.log(item.productDetails);
                                    setUpdate(update + 1);
                                    onEditChange();
                                  }
                                }}
                              >
                                {item.category !== 'uneffectProduct' && !edit ? (
                                  <Cnt>
                                    <Img src={isCheck ? images.check : images.uncheck} width={36} />
                                  </Cnt>
                                ) : null}
                                {item.category !== 'uneffectProduct' && edit ? (
                                  <Cnt>
                                    <Img
                                      src={item.isCrt ? images.check : images.uncheck}
                                      width={36}
                                    />
                                  </Cnt>
                                ) : null}
                              </View>
                              <Txt
                                title={
                                  item.category === 'uneffectProduct' ? '失效商品' : item.category
                                }
                                color="deep"
                                size={30}
                                bold
                              />
                            </Wrap>
                            {item.category === 'uneffectProduct' ? (
                              <View
                                onClick={() => {
                                  onDel(true);
                                  let arr = item.shopCartDtos.map((i) => i.id);
                                  let payload = arr.join();
                                  deleteShopCartPro(payload);
                                }}
                              >
                                <Txt title="清空失效商品" size={24} color="red" />
                              </View>
                            ) : (
                              <View
                              // onClick={() => {
                              //   getBussinessCouponList({
                              //     businessId: item.businessId,
                              //     storeId: item.storeId
                              //   });
                              //   setOpen(true);
                              //   setStoreId(item.storeId);
                              // }}
                              >
                                {/* {!!item.activityRegularList && <Tag type={2}>领券</Tag>} */}

                                {false && <Tag type={2}>领券</Tag>}
                              </View>
                            )}
                          </Wrap>
                          {item.productDetails.map((item1) => {
                            return (
                              <Goods
                                onToggleSku={(e) => {
                                  setShow(e);
                                }}
                                onEditChange={onEditChange}
                                isCrt={item1.isCrt}
                                data={item1}
                                edit={edit}
                                key={item1.id}
                                isDone={item.category === 'uneffectProduct'}
                              />
                            );
                          })}
                          {item.category !== 'uneffectProduct' && (
                            <Wrap Myheight={122} type={5}>
                              <Txt title="小计：" size={30} />
                              <View className="utp-cart-page-smallCount">
                                <Yuan price={item.totalAmount} color="#FF2F7B" size={30}></Yuan>
                              </View>
                            </Wrap>
                          )}
                        </View>
                      </View>
                    );
                  })}
                </View>
              )}
              {!!props.isLogin && !haveGoods && (
                <View>
                  <Wrap type={3}>
                    <Image src={images.cart_none}></Image>
                  </Wrap>
                </View>
              )}
              {!props.isLogin && (
                <View>
                  <Wrap type={3}>
                    <Image src={images.cart_none}></Image>
                  </Wrap>
                  <View
                    style={{
                      marginTop: Taro.pxTransform(50)
                    }}
                  >
                    <Txt title="还没登陆哦" color="low" size={20} />
                    <View
                      style={{
                        marginTop: Taro.pxTransform(50),
                        paddingLeft: Taro.pxTransform(200),
                        paddingRight: Taro.pxTransform(200),
                        marginBottom: Taro.pxTransform(60)
                      }}
                      onClick={() => {
                        Router.goLogin();
                      }}
                    >
                      <Bton type={7} size="mini">
                        去登陆
                      </Bton>
                    </View>
                  </View>
                </View>
              )}
            </View>
            <View style={{ display: categoryList.length > 0 ? 'block' : 'none' }}>
              <Wrap Myheight={80} justifyContent="center">
                <Txt title="——————  " color="low" size={32} />
                <Txt title=" 为你推荐 " color="deep" bold size={32} />
                <Txt title="  ——————" color="low" size={32} />
              </Wrap>
              <Goods2s data={categoryList as IGoods[]} />
            </View>
          </ScrollView>
          <View className="m-ft">
            {props.isLogin && edit && (
              <CartEdit
                chooseAll={() => {
                  if (editAll) {
                    onEditChange(false);
                  } else {
                    onEditChange(true);
                  }
                }}
                ischeck={editAll}
                onDel={() => {
                  onDel(false);
                }}
                onMove={onMove}
              />
            )}
            {!show && haveGoods && props.isLogin && !edit && (
              <View
                onClick={() => {
                  Taro.navigateTo({
                    url: '/pages/submit/index'
                  });
                }}
              >
                <CartSub
                  count={carts.selectTotalNumber}
                  totalPrice={carts.totalAmount}
                  selectAll={() => {
                    props.checkCart({
                      isSelectAll: isAllSelect ? 0 : 1
                    });
                  }}
                  ischeck={isAllSelect}
                  onSubmit={onSubmit}
                />
              </View>
            )}
          </View>
        </View>
      </View>
      <Model
        title="优惠券"
        show={open}
        onClose={() => {
          setOpen(false);
        }}
        height={1200}
      >
        <View className="utp-cart-page-couponsBox">
          {/* {","activityId":"916","businessId":"945","clientType":1,"couponCode":"201202007055922976178","storeId":"330674","sysId":1,"uid":"071cddc9b41a49e5ae8edcaef00458dc"} */}
          {couponList.map((ite, index) => {
            return (
              <Card
                key={ite.couponCode}
                data={ite}
                onQuan={async () => {
                  let data = await userGetCoupon({
                    activityId: ite.activityId,
                    businessId: ite.businessId,
                    couponCode: ite.couponCode,
                    storeId: storeId
                  });
                  if (data.code === 0) {
                    Taro.showToast({ title: '领取成功', icon: 'none' });
                    getBussinessCouponList({
                      businessId: ite.businessId,
                      storeId: storeId
                    });
                  }
                }}
              />
            );
          })}
        </View>
      </Model>
    </Safe>
  );
};
Index.config = {
  navigationBarTitleText: '购物车',
  disableScroll: true,
  navigationStyle: 'custom'
  //usingComponents: {
  //Bar: "../../components/bar" // 书写第三方组件的相对路径
  //}
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight,
    isWeiXinFalg: state.user.isWeiXinFalg,
    closeWidth: state.cart.closeWidth,
    post: state.cart.post,
    cart: state.cart,
    session: state.cart.session,
    isLogin: state.cart.isLogin,
    loading:
      state.loading.effects['cart/changeGoods'] ||
      state.loading.effects['cart/changeGoodsSku'] ||
      state.loading.effects['cart/syncCart'] ||
      state.loading.effects['cart/checkGoods'] ||
      state.loading.effects['cart/checkStore'] ||
      state.loading.effects['cart/checkCart'] ||
      state.loading.effects['cart/getSubmit']
    //state.loading.effects['cart/getCategoryList']
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    getCategoryList: (payload) => {
      // console.log(payload);
      dispatch({
        type: 'cart/getCategoryList'
      });
    },
    getSubmit: (payload) => {
      dispatch({
        type: 'cart/getSubmit',
        payload
      });
    },
    syncCart: () => {
      dispatch({
        type: 'cart/syncCart'
      });
    },
    checkStore: (payload) => {
      dispatch({
        type: 'cart/checkStore',
        payload
      });
    },
    checkCart: (payload) => {
      dispatch({
        type: 'cart/checkCart',
        payload
      });
    },
    savePost: (payload) => {
      dispatch({
        type: 'cart/savePost',
        payload
      });
    },
    saveSession: (payload) => {
      dispatch({
        type: 'cart/saveSession',
        payload
      });
    },
    submitBefore: () => {
      dispatch({
        type: 'cart/submitBefore'
      });
    },
    deleteShopCartPro: (payload) => {
      dispatch({
        type: 'cart/submitBefore',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
