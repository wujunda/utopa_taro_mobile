import { SafeAreaView } from 'react-native';
import './index.scss';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';

interface IProps {}
const Index: Taro.FC<IProps> = ({}) => {
  return (
    <View className="m-safe-rn">
      <SafeAreaView>
        <View className="m-safe-rn">{this.props.children}</View>
      </SafeAreaView>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
