import Taro, { useState } from '@tarojs/taro';
import { View, ScrollView, Swiper, SwiperItem, Image, Text } from '@tarojs/components';

import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Bar from '../../../components/bar';
import Img from '../../../components/Img';
import ItemHomeIcon from '../../../components/ItemHomeIcon';
import Goods1 from '../../../components/Goods1';

import goods from '../../../assets/goods.png';
import quan from '../../../assets/FreeChildren/quan.png';
import icon_duihuandingdan from '../../../assets/integral/icon_duihuandingdan.png';
import icon_jifenmingxi from '../../../assets/integral/icon_jifenmingxi.png';
import icon_jifenshangcheng from '../../../assets/integral/icon_jifenshangcheng.png';
import { images } from '../../../images';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  const [PlayInfo] = useState([
    {
      title: '儿童教育',
      src: icon_jifenmingxi
    },
    {
      title: '儿童摄影',
      src: icon_duihuandingdan
    },
    {
      title: '早教中心',
      src: icon_jifenshangcheng
    },
    {
      title: '少儿才艺',
      src: icon_jifenmingxi
    },
    {
      title: '幼儿电竞',
      src: icon_duihuandingdan
    }
  ]);
  const [playGoodsTab, setplayGoodsTab] = useState(0);
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          <Bar title="儿童免费玩" />
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page utp-freeVoucher"
            scrollY
          >
            <View className="utp-freeVoucher-swipeBox">
              <Swiper
                className="utp-freeVoucher-swipe"
                indicatorColor="#999"
                indicatorActiveColor="#333"
                circular
                indicatorDots
                autoplay
              >
                <SwiperItem>
                  <Img src={images.big_text_banner} width={1000}></Img>
                </SwiperItem>
                <SwiperItem>
                  <Img src={goods} width={1000}></Img>
                </SwiperItem>
                <SwiperItem>
                  <Img src={images.big_text_banner} width={1000}></Img>
                </SwiperItem>
              </Swiper>
            </View>
            <View className="utp-freeVoucher-iconText">
              <Wrap type={3}>
                {PlayInfo.map((item, index) => {
                  return (
                    <View className="utp-ico1-box" key={index}>
                      <ItemHomeIcon tips={item.title} src={item.src} tipsSize={24} Width={88} />
                    </View>
                  );
                })}
              </Wrap>
            </View>
            <View className="utp-freeVoucher-goodsTab">
              {[
                '猜你喜欢',
                '儿童乐园',
                '儿童摄影',
                '儿童乐园',
                '儿童摄影',
                '儿童乐园',
                '儿童摄影'
              ].map((item, index) => {
                return (
                  <View
                    className={
                      index === playGoodsTab
                        ? 'utp-freeVoucher-goodsTab-1-active'
                        : 'utp-freeVoucher-goodsTab-1'
                    }
                    onClick={() => {
                      // console.log('我切换', index);
                      setplayGoodsTab(index);
                    }}
                  >
                    {item}
                  </View>
                );
              })}
            </View>
            <View className="utp-freeVoucher-goodsList">
              <View className="utp-freeVoucher-goodsList-Box">
                <View className="utp-freeVoucher-goodsList-quanBox">
                  <Image src={quan} className="utp-freeVoucher-goodsList-BoxImg"></Image>
                  <Text className="utp-freeVoucher-goodsList-BoxText">券</Text>
                </View>
                <Goods1 />
              </View>
              <View className="utp-freeVoucher-goodsList-Box">
                <View className="utp-freeVoucher-goodsList-quanBox">
                  <Image src={quan} className="utp-freeVoucher-goodsList-BoxImg"></Image>
                  <Text className="utp-freeVoucher-goodsList-BoxText">券</Text>
                </View>
                <Goods1 />
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};
export default Index;
