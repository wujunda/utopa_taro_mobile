import Taro, { useState } from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import './index.scss';
import { images } from '../../images';

type PageStateProps = {
  cart: any;
};

type PageDispatchProps = {
  addQty: (id) => void;
  decQty: (id) => void;
};

type PageOwnProps = {
  type?: 1 | 2;
  qty?: boolean;
  goodsId?: string;
  onGetQty?: (qty) => void;
  onAdd?: () => void;
  onMinus?: (qty) => void;
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;
const Index: Taro.FC<IProps> = ({ type = 1, qty = 1, onAdd, onMinus }) => {
  // type:1 :商品详情页的增减，默认num=1 ; type:2:购物车商品增减，传入该商品数量qty ;
  const [num, setNum] = useState(1);
  const doAdd = (e) => {
    e.stopPropagation();
    if (e.nativeEvent && e.nativeEvent.stopImmediatePropagation) {
      e.nativeEvent.stopImmediatePropagation();
    }
    if (onAdd && qty > 0) {
      onAdd();
    }
  };
  const doMinus = (e) => {
    e.stopPropagation();
    if (e.nativeEvent && e.nativeEvent.stopImmediatePropagation) {
      e.nativeEvent.stopImmediatePropagation();
    }

    if (onMinus && qty > 1) {
      onMinus(qty);
    }
  };
  return (
    <View className="flex utp-step utp-div">
      <View className="flex utp-step-minus" onClick={doMinus}>
        <Image src={images.minus} className="utp-step-img" />
      </View>
      <Text className="flex utp-step-num">{qty ? qty : num}</Text>
      <View className="flex  utp-step-add" onClick={doAdd}>
        <Image src={images.add} className="utp-step-img" />
      </View>
    </View>
  );
};

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion
Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    cart: state.cart
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    addQty: (goodsId) => {
      dispatch({
        type: 'cart/addQty',
        goodsId: goodsId
      });
      dispatch({
        type: 'cart/TotalCountAndPrice'
      });
    },
    decQty: (goodsId) => {
      dispatch({
        type: 'cart/decQty',
        goodsId: goodsId
      });
      dispatch({
        type: 'cart/TotalCountAndPrice'
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
