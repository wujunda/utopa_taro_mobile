import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import './index.scss';
import Wrap from '../../components/Wrap';
import Txt from '../../components/Txt';
import Img from '../../components/Img';
import { images } from '../../images/index';

interface Props {}
const Index: Taro.FC<Props> = ({}) => {
  return (
    <View className="utp-loading utp-cnt" style={{}}>
      <Wrap>
        <Img src={images.loading} width={30} />
        <View style={{ width: Taro.pxTransform(12) }}></View>
        <Txt size={24} title="获取数据中..." />
      </Wrap>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
