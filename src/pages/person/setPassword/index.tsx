import Taro, { useState, useRouter } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View, Input } from '@tarojs/components';
import { connect } from '@tarojs/redux';
// import useRequest from '../../../hooks/useRequest';
// import { IUse, getItem } from '../../../services/user';

import Img from '../../../components/Img';
import Bton from '../../../components/Bton';
import { images } from '../../../images';
import './index.scss';
import eye from '@/assets/eye.png';
import eye1 from '@/assets/eye2.png';
import { getResetPassword } from '../../../services/user';
import Router from '../../../utils/router';

function Index({}) {
  const router = useRouter();
  const [focus, setFocus] = useState(false);
  const [focus1, setFocus1] = useState(false);
  const [password, setPassword] = useState('');
  const [password1, setPassword1] = useState('');
  const [see, setSee] = useState(false);
  const [see1, setSee1] = useState(false);
  const onNext = async () => {
    let data = await getResetPassword({
      checkCode: router.params.code,
      mobile: router.params.phone,
      password: password
    });
    if (data.code === 0) {
      Router.goResetRst('1');
    } else {
      Router.goResetRst('0');
      setTimeout(() => {
        Taro.showToast({
          title: data.msg,
          icon: 'none'
        });
      }, 100);
    }
  };
  let next = false;
  if (password.length > 5 && password === password1) {
    next = true;
  }

  return (
    <View className="utp-password">
      <View className="utp-password-box">
        <View
          className="utp-password-ipts"
          onClick={() => {
            setTimeout(() => {
              setFocus(true);
            }, 30);
          }}
        >
          <View className="utp-password-ipts-ico utp-cnt">
            {!(focus || password) && <Img src={images.set_lock} width={36} />}
            {(focus || password) && <Img src={images.set_lock_crt} width={36} />}
          </View>
          <View className="utp-password-ipts-ipt">
            <Input
              password={!see}
              onInput={(e) => {
                let value = e.detail.value;
                setPassword(value);
              }}
              value={password}
              focus={focus}
              onFocus={() => {
                setFocus(true);
              }}
              onBlur={() => {
                setFocus(false);
              }}
              placeholder="新密码"
            />
          </View>
          {focus && password && (
            <View
              className="utp-password-ipts-clo utp-cnt"
              onClick={() => {
                setPassword('');
              }}
            >
              <Img src={images.set_clo} width={28} />
            </View>
          )}

          <View
            className="utp-password-ipts-ico utp-cnt"
            onClick={() => {
              setSee(!see);
            }}
          >
            <Img src={see ? eye : eye1} width={48} />
          </View>
        </View>

        <View
          className="utp-password-ipts"
          onClick={() => {
            setTimeout(() => {
              setFocus1(true);
            }, 30);
          }}
        >
          <View className="utp-password-ipts-ico utp-cnt">
            {!(focus1 || password1) && <Img src={images.set_lock} width={36} />}
            {(focus1 || password1) && <Img src={images.set_lock_crt} width={36} />}
          </View>
          <View className="utp-password-ipts-ipt">
            <Input
              password={!see1}
              onInput={(e) => {
                let value = e.detail.value;
                setPassword1(value);
              }}
              value={password1}
              focus={focus1}
              onFocus={() => {
                setFocus1(true);
              }}
              onBlur={() => {
                setFocus1(false);
              }}
              type="text"
              placeholder="再次输入新密码"
            />
          </View>
          {focus1 && password1 && (
            <View
              className="utp-password-ipts-clo utp-cnt"
              onClick={() => {
                setPassword1('');
              }}
            >
              <Img src={images.set_clo} width={28} />
            </View>
          )}

          <View
            className="utp-password-ipts-ico utp-cnt"
            onClick={() => {
              setSee1(!see1);
            }}
          >
            <Img src={see1 ? eye : eye1} width={48} />
          </View>
        </View>
      </View>
      <View className="utp-password-box">
        {!next && (
          <Bton type={'none'} size="mini">
            下一步
          </Bton>
        )}
        {next && (
          <View onClick={onNext}>
            <Bton size="mini">下一步</Bton>
          </View>
        )}
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '设置新密码',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ffffff'
};
// @ts-ignore
export default connect((state) => state)(Index);
