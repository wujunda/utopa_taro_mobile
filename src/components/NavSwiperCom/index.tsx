/* eslint-disable react/jsx-no-undef */
import Taro from '@tarojs/taro';
// import { View } from '@tarojs/components';
import { Swiper, SwiperItem, Image } from '@tarojs/components';
import './index.scss';

interface ImageItem {
  id?: string;
  src: string;
  width?: number; // 百分比
  height?: number | string;
}

interface Props {
  dataSource: ImageItem[];
}

const NavSwiperCom: Taro.FC<Props> = ({ dataSource }) => {
  // console.log(dataSource);
  return (
    <Swiper
      className="test-h"
      indicatorColor="#999"
      indicatorActiveColor="#333"
      vertical={false}
      circular
      indicatorDots
      autoplay
    >
      {Array.isArray(dataSource) &&
        dataSource.map((item, idx) => {
          return (
            // eslint-disable-next-line react/jsx-key
            <SwiperItem>
              <Image className="swiper-img" mode="widthFix" key={idx} src={item.src}></Image>
            </SwiperItem>
          );
        })}
    </Swiper>
  );
};

export default NavSwiperCom;
