/* eslint-disable react/react-in-jsx-scope */
import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';

import './index.scss';

// #region 书写注意
// #endregion

interface Props {
  loading?: boolean;
  type?: number;
  children: any;
  barList?: string[];
  defaultActiveKey?: number;
  style?: any;
  fixed?: boolean;
  onChange?: (number) => void;
}
const Index: Taro.FC<Props> = ({
  onChange,
  defaultActiveKey,
  children,
  type,
  loading,
  style,
  fixed = true,
  barList = ['全部', '进行中', '拼团成功', '拼团失败']
}) => {
  const handleClick = (index) => {
    if (loading) {
      return;
    }
    if (onChange) {
      onChange(index);
    }
  };
  return (
    <View className="utp-tab utp-div">
      <View className={`utp-tab-box ${!fixed ? 'utp-tab-box-1' : ''}`} style={style ? style : null}>
        {barList.map((item, index) => {
          return (
            <View
              className={
                defaultActiveKey === index
                  ? 'utp-tab-box-item utp-tab-box-active'
                  : 'utp-tab-box-item'
              }
              onClick={() => {
                handleClick(index);
              }}
              key={index}
            >
              <Text>{item}</Text>
            </View>
          );
        })}
      </View>
      <View className={'utp-tab-bd ' + (type === 1 ? 'utp-tab-bd-1' : '')}>{children}</View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
