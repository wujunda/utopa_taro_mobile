import Taro from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
import useStore from '../../../components/useNum';
import { connect } from '@tarojs/redux';
// import useRequest from '../../../hooks/useRequest';
// import { IUse, getItem } from '../../../services/user';

import Box1 from '../../../components/Box1';
import More from '../../../components/More';
import Wrap from '../../../components/Wrap';
import Txt from '../../../components/Txt';
import './index.scss';
import  Router  from '../../../utils/router';

function Index(props) {
  let num = useStore({ name: '' })[0];
  console.log('jjjjj');
  console.log(num);
  console.log('props');
  console.log(props);
  // const [state, update, loading] = useRequest<IUse>('name=a&age=1', getItem);
  // console.log('更多');
  // console.log(state);
  // console.log(loading);
  // console.log(update);

  return (
    <View>
      <View className="utp-account-title">
        <Wrap type={1}>
          <Txt title="账号设置" bold size={30} color="deep" />
        </Wrap>
      </View>
      <Box1 type={1}>
        <View
          onClick={() => {
            Taro.navigateTo({
              url: '/pages/person/unsubscribe/index'
            });
          }}
        >
          <More title="注销账号" />
        </View>
      </Box1>
      <View className="utp-account-title">
        <Wrap type={1}>
          <Txt title="密码设置" bold size={30} color="deep" />
        </Wrap>
      </View>
      <Box1 type={1}>
        <View
          onClick={() => {
            Router.goPassword();
          }}
        >
          <More title="修改登陆密码" />
        </View>
        <View
          onClick={() => {
            Router.goPayPassword();
          }}
        >
          <More title="设置支付密码" />
        </View>
      </Box1>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '账号和密码设置',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};
// @ts-ignore
export default connect((state) => state)(Index);
