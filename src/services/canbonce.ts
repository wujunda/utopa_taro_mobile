import request from '../utils/request';
import { Glo } from '../utils/utils';

// 获取积分商城信息
export const getCanBonce = (params: any) => {
  // console.log(params);
  return request.post<any>(
    '/igc/app/offline/request/create', 
    {
      paymentPicKey: params.paymentPicKey,
      voucherPicKey: params.voucherPicKey,
      uid: Glo.store.getState().cart.isLogin.uid
    },
    request.API_TYPE.CRM_CLOUD
  );
};

// 用户积分信息 
export const userIntegralMsg = () => {
  return request.get(
    `/igc/app/account/user?uid=${Glo.store.getState().cart.isLogin.uid}`,
    {},
    request.API_TYPE.CRM_CLOUD
  );
};

// 用户积分信息 
export const userIntegralDetails = (param: any, pst: any) => {
  return request.get(
    '/igc/app/trade/user/list',
    {
      ...param,
      pageSize: pst.per_page,
      pageNo: pst.page,
      ownerUid: Glo.store.getState().cart.isLogin.uid
    },
    request.API_TYPE.CRM_CLOUD
  );
};