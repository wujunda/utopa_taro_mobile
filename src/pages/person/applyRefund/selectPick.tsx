/* eslint-disable import/first */
import Taro, { useState, useEffect } from '@tarojs/taro';
import { View, Text, Picker } from '@tarojs/components';
import Wrap from '../../../components/Wrap';
import Img from '@/components/Img';
import goRight from '../../../assets/mine/personal_icon_forward.png';

// eslint-disable-next-line import/prefer-default-export
const ViewPickReason = (props: any) => {
  const { data, onChange, defaultValue, leftText, binKey } = props;
  const [reason, setReaSon] = useState(defaultValue || '');
  useEffect(() => {
    defaultValue && setReaSon(defaultValue);
  }, [defaultValue]);
  const idx = data && data.findIndex((item) => item.reason === reason);
  return (
    leftText && (
      <View>
        <Picker
          mode="selector"
          range={data}
          rangeKey={binKey || 'reason'}
          value={idx}
          onChange={(v: any) => {
            onChange(data[v.detail.value]);
            setReaSon(binKey ? data[v.detail.value][binKey] : data[v.detail.value].reason);
          }}
        >
          <Wrap type={1} justifyContent="space-between" flexWrap="nowrap">
            <Text className="apply-refund-shop-text">{leftText}</Text>
            <View style={{ color: '#999999' }}>
              <Wrap type={2} flexDirection="row">
                {reason || '请选择'}
                {/* <Img src={goRight}></Img> */}
                {idx < 0 ? <Img src={goRight}></Img> : null}
              </Wrap>
            </View>
          </Wrap>
        </Picker>
      </View>
    )
  );
};
export default ViewPickReason;
