import Taro from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { View } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import Wrap from '../../components/Wrap';
import Txt from '../../components/Txt';
import Img from '../../components/Img';
import right from '../../assets/right.png';
import './index.scss';
import  Router  from '../../utils/router';

function Index() {
  const list = [
    { name: '账号问题', id: 5 },
    { name: '功能建议', id: 6 },
    { name: 'APP报错', id: 1 },
    { name: '订单问题', id: 8 },
    { name: '停车问题', id: 9 }
  ];
  return (
    <View className="utp-help" onClick={() => {}}>
      {list.map((item) => {
        return (
          <View
            className="utp-help-item"
            key={item.id}
            onClick={() => {
              Router.goSendMsg(item.id);
            }}
          >
            <Wrap Myheight={100} justifyContent="space-between">
              <Txt title={item.name} size={30} color="deep" />
              <Img src={right} width={48} />
            </Wrap>
          </View>
        );
      })}
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '帮助反馈',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ccc'
};
// @ts-ignore
export default connect((state) => state)(Index);
