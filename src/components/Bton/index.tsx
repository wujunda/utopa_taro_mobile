import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import './index.scss';

interface Props {
  children: any;
  type?: 1 | 2 | 3 | 7 | 'none' | 'defalut';
  size?: 'mini' | 'normal';
  borderRadius?: number | 80;
  onClick?:()=>void;
}
const Index: Taro.FC<Props> = ({
  children,
  type = 'default',
  size = 'normal',
  borderRadius = 80,
  onClick
}) => {
  let classView = 'utp-btn utp-solid';
  let classText = 'utp-btn-txt';
  switch (type) {
    case 1:
      classView += ' utp-btn-1';
      classText += ' utp-btn-txt-1';
      break;
    case 2:
      classView += ' utp-btn-2';
      classText += ' utp-btn-txt-2';
      break;
    case 3:
      classView += ' utp-btn-3';
      break;
    case 7:
      classView += ' utp-btn-7';
      break;
    case 'none':
      classView += ' utp-btn-none';
      break;
  }
  if (size === 'mini') {
    classView += ' utp-btn-mini';
    classText += ' utp-btn-txt-mini';
  }
  return (
    <View
      className={classView}
      onClick={onClick}
      style={{
        borderRadius: Taro.pxTransform(borderRadius)
      }}
    >
      <Text className={classText}>{children}</Text>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
