/* eslint-disable no-unused-vars */
import Taro, { useRouter, useMemo, useCallback, useEffect } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { WebView } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import qs from 'query-string';
import { ConnectState } from '../../models/connect';

type PageStateProps = {
  loading: boolean;
};

type PageDispatchProps = {
  getOpenToken: Function;
};

type PageOwnProps = {
  openToken: string;
  isLogin: boolean;
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = ({ isLogin, getOpenToken, openToken }) => {
  const { params } = useRouter();

  useEffect(() => {
    if (isLogin && !openToken) {
      getOpenToken();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLogin]);

  // 处理url
  const src = useMemo(() => {
    let url = params && params.url ? decodeURIComponent(params.url) : '';
    console.log('WEB View');
    console.log(url);
    if (url.indexOf('https') === -1) {
      url = url.replace('http://', 'https://');
    }

    if (url.indexOf('#') > -1 && openToken) {
      // 把openToken塞进去
      const urlArr = url.split('#');
      const { url: path, query } = qs.parseUrl(urlArr[1]);
      const stringifyUrl = qs.stringifyUrl({ url: path, query: { ...query, openToken } });
      console.log({ query, path, stringifyUrl, urlArr, url });
      url = `${urlArr[0]}#${stringifyUrl}`;
    }
    console.log('webview url: ', url);
    return url;
  }, [params, openToken]);

  const onMessage = useCallback((e) => {
    const { data } = e.detail;
    console.log('webView收到消息', data);

    // const type = data.type;
    // const params = data.params;
  }, []);

  return <WebView src={src} onMessage={onMessage} />;
};

Index.config = {
  navigationBarTitleText: '',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state: ConnectState) => {
  return {
    inApp: state.user.inApp,
    openToken: state.user.openToken,
    isLogin: !!state.cart.isLogin
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    getOpenToken: (payload) => {
      dispatch({
        type: 'user/getOpenToken',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
