import request from '../utils/request';

export interface Cate {
  name: string;
  id: number;
}

export interface ICate {
  data: {
    list: Cate[];
  };
}
export const getSendMsg = (params) => {
  return request.post<any>('/app.do', {
    method: 'account.help.saveFeedback',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};

// 获取上传token
export const getToken = (param) => {
  // console.log(param);
  return request.post<any>('/app.do', {
    method: 'common.upload.getUpToken',
    ver: '1.0',
    params: JSON.stringify({ sysId: 1 })
  });
};
export const getImgUrl = (param) => {
  // console.log(param);
  return request.post<any>('/app.do', {
    method: 'common.upload.getUrlByKey',
    ver: '1.0',
    params: JSON.stringify({ key: param })
  });
};
