import { View } from '@tarojs/components';
import Taro, { useEffect, useRouter } from '@tarojs/taro';
import { connect } from '@tarojs/redux';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import { getColumnProduct } from '../../../services/integral';
import { wxRegister, shareCallbackList } from '../../../utils/wxApi';
import IntegralCard from '../../../components/integral/card';
import './index.scss';

function IngegralList(props) {
  let router = useRouter();
  let payload = {
    id: router.params.id,
    dataType: router.params.dataType,
    sortRule: router.params.sortRule,
    longitude: router.params.longitude || '',
    latitude: router.params.latitude || ''
  };
  //     let payload = {
  //       id:5213, //栏目id
  //       dataType: 5, // 数据类型 1：商品 2：抵用卷  3：拼团，5积分商品
  //       sortRule: 1, //排序规则 1: 商品权重 2：店铺距离 3：大数据推荐
  //       longitude:'',
  //       latitude:''
  //     };
  // eslint-disable-next-line no-unused-vars
  const [state, hasMore, run, getMoreData, loading] = useRequestWIthMore(payload, getColumnProduct);

  var goods = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.data.records.map((items) => {
        goods.push(items);
      });
    });
  }
  // 分享功能
  useEffect(() => {
    if (props.user.inApp) {
      // JSBridge.Bridge.WebViewJavascriptBridge.setup(() => {
      //   let params = {
      //     shareTitle: '积分商城',
      //     subTitle: '积分商城的描述',
      //     shareIcon: shareImg,
      //     shareUrl: window.location.href
      //   };
      //   JSBridge.Common.GTBridge_Common_Base_getShareInfo(JSON.stringify(params),(res) => {
      //   });
      // })
    }
    if (process.env.TARO_ENV == 'h5') {
      if (props.user.isWeiXinFalg) {
        wxRegister(
          shareCallbackList({
            title: '积分商城',
            desc: '',
            link: window.location.href,
            imgUrl:
              'https://image.myutopa.com/Fr0Vlg2LNy2owURS5bDu1sNxfrHl?e=1614217121&token=WNtUYaMiCdpTDzanlE03SjKXLinbmTzkLGXEBYC_:RSZxw6kzc_1z1ua7yQifGs1YLkg='
          })
        );
      }
    }
  }, [props.user.inApp, props.user.isWeiXinFalg]);
  return (
    <View className="utp-integral">
      {loading && goods.length === 0 && <Loading />}
      {!loading && goods.length === 0 && <LoadingNone />}
      <View className="utp-integral-list">
        {goods &&
          goods.length > 0 &&
          goods.map((item, index) => {
            return <IntegralCard data={item} key={index} />;
          })}
      </View>
      {loading && goods.length !== 0 && <LoadingMore />}
      {!loading && !hasMore && <LoadingNoneMore />}
    </View>
  );
}

IngegralList.config = {
  navigationBarTitleText: '积分商城',
  enablePullDownRefresh: true,
  disableScroll: false,
  onReachBottomDistance: 50
};
// @ts-ignore
export default connect((state) => state)(IngegralList);
