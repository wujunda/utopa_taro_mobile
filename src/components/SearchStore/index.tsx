import Taro, { useState, useRouter, useReachBottom } from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import './index.scss';
import right from '../../assets/right.png';
import Tag from '../Tag';
import Wrap from '../Wrap';
import Img from '../Img';
import Txt from '../Txt';
import Cnt from '../Cnt';
import Score from '../Score';
import Yuan from '../Yuan';
import { ProductStoreInfo } from '../../interface/product';
import store_icons_down from '../../assets/shoppingStore/store_icons_down.png';
import store_tag2 from '../../assets/shoppingStore/store-tag2.png';
import store_tag1 from '../../assets/shoppingStore/store-tag1.png';
import store_tag3 from '../../assets/shoppingStore/store-tag3@2x.png';
import store_tag4 from '../../assets/shoppingStore/store-tag4.png';
import Router from './../../utils/router';

interface IProps {
  item: ProductStoreInfo;
  like?: () => void;
}
const Index: Taro.FC<IProps> = ({ item, like }) => {
  return (
    <View className="utp-search-store" onClick={() => {}}>
      <View className="utp-search-store-hd">
        <Wrap type={1}>
          <View className="utp-search-store-hd-imgs">
            <Img src={item && item.logoUrl} width={90} />
          </View>
          <View className="utp-search-store-hd-item utp-search-store-hd-itemspe">
            <View className="utp-search-store-hd-item">
              <View>
                <View className="utp-search-store-hd-titles">
                  <Wrap type={1}>
                    <Wrap>
                      {item && item.selfSupport ? <Tag>自营</Tag> : null}
                      {item && item.selfSupport ? (
                        <View style={{ width: Taro.pxTransform(8) }} />
                      ) : null}
                      <Text
                        className="utp-search-store-hd-txt"
                        style={{
                          width:
                            item && item.selfSupport ? Taro.pxTransform(322) : Taro.pxTransform(390)
                        }}
                      >
                        {item && item.storeName}
                      </Text>
                    </Wrap>
                  </Wrap>
                </View>
                <View className="utp-search-store-hd-tips">
                  <Score num={4} />
                  {/* <Wrap type={1}>
                      <Wrap>
                        <Txt title={item && item.storeMainCategory + ' |'} size={24} />
                        <View style={{ width: Taro.pxTransform(8) }} />
                        <Txt title="人气值：" size={24} />
                        <Score num={4} />
                      </Wrap>
                    </Wrap> */}
                </View>
              </View>
              {item && (
                <View
                  className={
                    item.isFavorite
                      ? 'utp-search-store-hd-concern'
                      : 'utp-search-store-hd-unconcern'
                  }
                  onClick={like}
                >
                  <Txt
                    title={item.isFavorite ? '已关注' : '关注'}
                    size={26}
                    color={item.isFavorite ? 'low' : 'active'}
                    height={44}
                  />
                </View>
              )}
            </View>

            <View className="utp-search-store-hd-item1">
              <Txt title={item && item.storeMainCategory} size={24} height={40} />

              {item &&
                item.logisticsModel.map((i, ii) => {
                  return (
                    <View className="utp-search-store-hd-label" key={ii}>
                      <View className="utp-search-store-hd-labelbefore"></View>
                      <Cnt>
                        <Img
                          src={
                            i === '即时达' || i === '周边送'
                              ? store_tag1
                              : i === '扫码自取'
                              ? store_tag3
                              : i === '门店自提'
                              ? store_tag4
                              : i === '全国送'
                              ? store_tag2
                              : store_icons_down
                          }
                          width={24}
                        />
                        <Txt title={i} color="deep" size={24} />
                      </Cnt>
                    </View>
                  );
                })}
            </View>
          </View>
        </Wrap>
      </View>
      <View className="utp-search-store-bd">
        <View className="utp-search-store-bd-imgs">
          {item &&
            item.productList &&
            item.productList.map((item1) => {
              return (
                <View key={item1.productId} className="utp-search-store-bd-img utp-solid">
                  <Img src={item1.productImage} width={208} />
                  <View className="utp-search-store-bd-img-money">
                    <Cnt>
                      <Yuan price={item1.salePrice} color="#FFF"></Yuan>
                    </Cnt>
                  </View>
                </View>
              );
            })}
        </View>
      </View>
      {item && item.labels && item.labels.length && (
        <View className="utp-search-store-bd" style={{ marginBottom: Taro.pxTransform(20) }}>
          <Wrap Myheight={36}>
            {item.labels.map((item2) => {
              return (
                <View key={item2.labelId} className="utp-search-store-bd-labelsbox">
                  {item2.labelName}
                </View>
              );
            })}
          </Wrap>
        </View>
      )}

      <View className="utp-search-store-ft">
        <Wrap
          type={1}
          Myheight={44}
          justifyContent={
            item && item.coupons && item.coupons != null ? 'space-between' : 'flex-end'
          }
        >
          {item && item.coupons && item.coupons != null && (
            <View className="utp-search-store-ft-card">
              {item.coupons.map((u, uu) => {
                return (
                  <Text className="utp-search-store-ft-card-items" key={uu}>
                    {u.couponName}
                  </Text>
                );
              })}
            </View>
          )}
          <Wrap
            click={() => {
              Router.goStore(item.storeTemplateCode, item.storeId, item.businessId);
            }}
          >
            <Txt title="进店" size={22} color="low" />
            <Img src={right} width={38} />
          </Wrap>
        </Wrap>
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
