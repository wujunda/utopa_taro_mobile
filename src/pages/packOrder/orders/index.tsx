import Taro, { useState, useRouter, useReachBottom } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { connect } from '@tarojs/redux';
import { View } from '@tarojs/components';
import Bton from '../../../components/Bton';
import Wrap from '../../../components/Wrap';
import Img from '../../../components/Img';
import Tag from '../../../components/Tag';
import Txt from '../../../components/Txt';
import Tab from '../../../components/Tab';
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import right from '../../../assets/right.png';
import Safe from '../../../components/safe';
import useSession from '../../../hooks/useSession';
import Yuan from '../../../components/Yuan';
// import useLogin from '../../../hooks/useLogin';
import { myProductOrders, Rprops } from '../../../services/orders';
import useResetCount from '../../../hooks/useResetCount';
// import { ProductBaiscData } from '../../../interface/product';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore';
import Router from '../../../utils/router';
import { goPayPage, successUrl } from '../../../utils/utils';
import './index.scss';

type PageStateProps = {
  loading: boolean;
  submit: any;
  inApp: boolean;
  isWeiXinFalg: boolean;
};
type IProps = PageStateProps;
const Index:Taro.FC<IProps> = (props) =>  {
  const router = useRouter();
  let pass = Number(router.params.status);
  const [inn, setInn] = useState(pass);
  const [haveSession] = useSession();
  // 请求列表数据
  const [state, hasMore, run, getMoreData, loading] = useRequestWIthMore(inn, myProductOrders);
  const [can, update1] = useResetCount();
  if (
    haveSession &&
    // isLogin &&
    (state === null || (state != null && state.length == 0)) &&
    run &&
    !loading
  ) {
    if (can) {
      run();
      update1();
    }
  }
  var goods: Rprops[] = [];
  if (state && state.length > 0 && state[0].data.records) {
    state.map((item) => {
      item.data.records.map((it) => {
        goods.push(it);
      });
    });
  }
  useReachBottom(() => {
    console.warn('onReachBottom');
  });
  let newKey = 0;
  if (inn === 110) {
    newKey = 0;
  }
  if (inn === 0) {
    newKey = 1;
  }
  if (inn === 1) {
    newKey = 2;
  }
  if (inn === 2) {
    newKey = 3;
  }
  if (inn === 3) {
    newKey = 4;
  }

  return (
    <Safe>
      <View className="utp-orders" style={{ flex:1,backgroundColor: '#f5f5f5' }}>
        <Tab
          loading={loading}
          barList={['全部', '待支付', '待发货', '待收货', '已完成']}
          defaultActiveKey={newKey}
          onChange={(i) => {
            if (i === 0) {
              setInn(110);
            }
            if (i === 1) {
              setInn(0);
            }
            if (i === 2) {
              setInn(1);
            }
            if (i === 3) {
              setInn(2);
            }
            if (i === 4) {
              setInn(3);
            }
            if (run) {
              run({});
            }
          }}
        >
          <View className="utp-orders-bd">
            {loading && goods.length === 0 && <Loading />}
            {!loading && goods.length === 0 && <LoadingNone />}
            {goods.map((i, ii) => {
              return (
                <View
                  className="utp-orders-wrap"
                  key={ii}
                  onClick={() => {
                    Taro.navigateTo({
                      url: `/pages/packOrder/order/index?orderId=${i.orderId}&orderNo=${i.orderNo}&serviceStatus=${i.serviceStatus}`
                    });
                  }}
                >
                  <View className="utp-orders-bd-box utp-orders-bd-box-1">
                    <View className="utp-orders-bd-store">
                      <Wrap type={1} justifyContent="space-between">
                        <Wrap>
                          {
                            i.selfSupport && (
                              <Tag>自营</Tag>
                            )
                          }
                          <View className="utp-orders-bd-store-title">
                            <Wrap>
                              <Txt title={i.businessName} size={28} color="black" />
                            </Wrap>
                          </View>
                        </Wrap>
                        {i.cancleReason !== null && (
                          <Txt title={i.cancleReason} size={24} color="red" />
                        )}
                        {i.serviceStatus === 2 && <Txt title="待收货" size={24} color="red" />}
                        {i.serviceStatus === 3 && <Txt title="已完成" size={24} color="red" />}
                        {i.serviceStatus === 0 && <Txt title="待支付" size={24} color="red" />}
                        {i.serviceStatus === 8 && <Txt title="待接单" size={24} color="red" />}
                        {i.serviceStatus === 1 && <Txt title="待发货" size={24} color="red" />}
                      </Wrap>
                    </View>
                    {i.orderProducts.length === 1 && (
                      <View className="utp-orders-bd-goods utp-solid">
                        <Wrap type={1}>
                          <View className="utp-orders-bd-goods-imgs utp-solid">
                            <Img src={i.orderProducts[0].pic} width={168} />
                          </View>
                          <View className="utp-orders-bd-goods-titles">
                            {i.orderProducts[0].name}
                          </View>
                          <Wrap>
                            <View className="utp-orders-bd-goods-tips">
                              <Txt title={`共${i.goodsNum}件`} />
                            </View>
                            <Img src={right} width={44} />
                          </Wrap>
                        </Wrap>
                      </View>
                    )}
                    {i.orderProducts.length > 1 && (
                      <View className="utp-orders-bd-goods utp-orders-bd-goods-1 utp-solid">
                        <Wrap type={1} justifyContent="space-between">
                          <Wrap>
                            {i.orderProducts.map((o, oo) => {
                              if (oo < 3) {
                                return (
                                  <View
                                    className="utp-orders-bd-goods-imgs utp-orders-bd-goods-imgs-1 utp-solid"
                                    key={oo}
                                  >
                                    <Img src={o.pic} width={128} />
                                  </View>
                                );
                              }
                            })}
                          </Wrap>
                          <Wrap>
                            <View className="utp-orders-bd-goods-tips">
                              <Txt title={`共${i.goodsNum}件`} />
                            </View>
                            <Img src={right} width={44} />
                          </Wrap>
                        </Wrap>
                      </View>
                    )}

                    <View
                      style={{
                        height: Taro.pxTransform(100)
                      }}
                    >
                      <Wrap type={1} justifyContent="space-between">
                        <Wrap>
                          <Txt
                            size={28}
                            title={i.serviceStatus === 0 ? '应付：' : '合计 ：'}
                            color="black"
                            bold
                          />
                          <Txt
                            size={28}
                            color="active"
                            title={
                              i.exchangeTotalPoint && i.exchangeTotalPoint !== null
                                ? `${i.exchangeTotalPoint} 积分 +`
                                : ''
                            }
                          />
                          <View className="utp-orders-prices">
                            <Yuan price={i.orderTotalMoney} size={28} color="#FF2F7B"></Yuan>
                          </View>
                        </Wrap>
                        {
                          //等待支付
                          i.serviceStatus === 0 && (
                            <View className="utp-orders-btns">
                              <Wrap type={1}>
                                <View
                                  className="utp-orders-btns-btn"
                                  onClick={() => {
                                    if (process.env.TARO_ENV === 'weapp') {
                                      Router.goPay(
                                        i.orderId,
                                        i.orderTotalMoney / 100,
                                        i.orderNo,
                                        i.payOrderNo
                                      );
                                    } else {
                                      let channelId = 1001;
                                      console.log(props.inApp,props.isWeiXinFalg,'==kkk')
                                      if (props.inApp) {
                                        channelId = 1;
                                      } else if (props.isWeiXinFalg) {
                                        channelId = 1002;
                                      }
                                      console.log(channelId)
                                      goPayPage({
                                        sysId: channelId,
                                        payOrderNo: i.payOrderNo,
                                        paySucceesHref: successUrl(),
                                        payFailHref: successUrl()
                                      });
                                    }
                                  }}
                                >
                                  <Bton size="mini" type={7}>
                                    去支付
                                  </Bton>
                                </View>
                              </Wrap>
                            </View>
                          )
                        }
                        {/* {
                          //待收货
                          i.serviceStatus === 2 && (
                            <View className="utp-orders-btns">
                              <Wrap type={1}>
                                <View className="utp-orders-btns-btn">
                                  <Bton size="mini" type={1}>
                                    查看物流
                                  </Bton>
                                </View>
                                <View style={{ width: Taro.pxTransform(20) }}></View>
                                <View className="utp-orders-btns-btn">
                                  <Bton size="mini">确认收货</Bton>
                                </View>
                              </Wrap>
                            </View>
                          )
                        } */}
                        {
                          // 已完成
                          i.serviceStatus === 3 && (
                            <View className="utp-orders-btns">
                              <Wrap type={1}>
                                <View
                                  className="utp-orders-btns-btn"
                                  onClick={(e) => {
                                    e.stopPropagation();
                                    Router.goConfig(i.orderId);
                                  }}
                                >
                                  <Bton size="mini" type={1}>
                                    核销订单
                                  </Bton>
                                </View>
                                <View style={{ width: Taro.pxTransform(20) }}></View>
                                <View
                                  className="utp-orders-btns-btn"
                                  onClick={(e) => {
                                    e.stopPropagation();
                                    Router.goAfterSale();
                                  }}
                                >
                                  <Bton size="mini" type={1}>
                                    申请售后
                                  </Bton>
                                </View>
                              </Wrap>
                            </View>
                          )
                        }
                      </Wrap>
                    </View>
                  </View>
                </View>
              );
            })}
            {loading && goods.length !== 0 && <LoadingMore />}
            {!loading && !hasMore && goods.length > 0 && <LoadingNoneMore />}
          </View>
        </Tab>
      </View>
    </Safe>
  );
}

Index.config = {
  navigationBarTitleText: '我的订单',
  disableScroll: false
  // enablePullDownRefresh: true,
  // backgroundColor: '#f5f5f5'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight,
    inApp: state.user.inApp,
    isWeiXinFalg: state.user.isWeiXinFalg
  };
};
// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
