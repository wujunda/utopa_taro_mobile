
import { View, ScrollView } from '@tarojs/components';
import Taro, { useEffect } from '@tarojs/taro';
import { connect } from '@tarojs/redux';
import useRequestWIthMore from '../../../hooks/useRequestWIthMore'
import Loading from '../../../components/Loading';
import LoadingMore from '../../../components/LoadingMore';
import LoadingNone from '../../../components/LoadingNone';
import LoadingNoneMore from '../../../components/LoadingNoneMore';
import { integralList } from '../../../services/integral';
import { wxRegister, shareCallbackList } from '../../../utils/wxApi';
import IntegralCard from '../../../components/integral/card';
import './index.scss';


function IngegralList(props) {
  // eslint-disable-next-line no-unused-vars
  const [state, hasMore, run, getMoreData, loading] = useRequestWIthMore(
    {},
    integralList
  );

  var goods = [];
  if (state && state.length > 0) {
    state.map((item) => {
      item.data.items.map((items) => {
        goods.push(items);
      });
    });
  }
  
  // 分享功能
  useEffect(() => {
    wxRegister(shareCallbackList({
      title: '积分商城',
      desc: '积分商城',
      link: window.location.href,
      imgUrl: 'https://image.myutopa.com/Fr0Vlg2LNy2owURS5bDu1sNxfrHl?e=1614217121&token=WNtUYaMiCdpTDzanlE03SjKXLinbmTzkLGXEBYC_:RSZxw6kzc_1z1ua7yQifGs1YLkg='
    }))
  }, [])
  return (
    <ScrollView className="utp-integral" >
      {loading && goods.length === 0 && <Loading />}
      {!loading && goods.length === 0 && <LoadingNone />}
      <View className="utp-integral-list">
        {goods && goods.length > 0 && goods.map((item, index) => {
          return (
            <IntegralCard data={item} key={index}/>
          )
        })}
      </View>
      {loading && goods.length !== 0 && <LoadingMore />}
      {!loading && !hasMore && <LoadingNoneMore />}
    </ScrollView>
  );
}

IngegralList.config = {
  navigationBarTitleText: '积分商城',
  enablePullDownRefresh: true,
  disableScroll: false,
  onReachBottomDistance: 50
};
// @ts-ignore
export default connect((state) => state)(IngegralList);