import Taro from '@tarojs/taro';
import qs from 'query-string';
import { Glo } from '../utils/utils';

interface ParsedQuery<T = string> {
  [key: string]: T | T[] | null | undefined;
}
interface GoPageOption {
  url?: string;
  cmd?: string;
  params?: ParsedQuery;
  redirect?: boolean;
}

const STORE_TYPE = {
  FOOD: 'FASHION', // 餐饮类商家
  FASHION: 'FASHION', // 时尚类商家
  NORMAL: 'NORMAL', // 通用类商家
  FOOD_NOPAY: 'FOOD_NOPAY', // 餐饮类商家(无订购)
  FOOD_LBZ: 'FOOD_LBZ', // 乐巴扎
  TEMPLATE_ONE: 'TEMPLATE_ONE', // 5.3新增模版1
  TEMPLATE_TWO: 'TEMPLATE_TWO', // 5.3新增模版2
  TEMPLATE_THREE: 'TEMPLATE_THREE' // 5.3新增模版3
};

export default class Router {
  static whiteList = [
    // webview业务域名白名单
    'www-dev.myutopa.com',
    'www-pre.myutopa.com',
    'www-test.myutopa.com',
    'www.myutopa.com'
  ];

  // 处理CMD码
  static CMD = {
    // 首页
    C0000020101: '/pages/home/index',
    C0010001: '/pages/home/index',
    // 扫码页面
    C0000011001: Router.goScan,
    // 停车
    C0080100: Router.useAppOpen,
    // 积分需要登陆
    C0050303: '/pages/service/integral/index',
    // 小票积分
    C0050090101: '/pages/packOrder/canbonce/index',
    // 店铺专区
    C0040020100: ({ params = {} }: GoPageOption) => ({
      url: `/pages/store/storeHome/index?id=${params.zoneId}`
    }),
    // liber
    C0040020200: ({ params = {} }: GoPageOption) => ({
      url: `/pages/store/liber/index?id=${params.zoneId}`
    }),
    // 优惠可用商品
    C0040010400: ({ params = {} }: GoPageOption) => ({
      url: `/pages/service/canuse/index?activeType=${params.activeType}&activityId=${params.activityId}&activityGroupId=${params.activityGroupId}`
    }),
    // liber品牌索引
    C0040020203: ({ params = {} }: GoPageOption) => ({
      url: `/pages/store/liberBoth/index?id=${params.zoneId}`
    }),
    // liber品牌分类
    C0040020202: ({ params = {} }: GoPageOption) => ({
      url: `/pages/store/liberCate/index?id=${params.zoneId}`
    }),
    // liber品牌广告
    C0040020201: ({ params = {} }: GoPageOption) => ({
      url: `/pages/store/liberAd/index?id=${params.zoneId}`
    }),
    // 小q
    C0010010000: Router.useAppOpen,
    // 店铺
    C0040010300: ({ params = {} }: GoPageOption) => {
      let url = '';
      if (params.storeType === STORE_TYPE.TEMPLATE_THREE) {
        url = '/pages/store/shoppingStore/index';
      } else if (params.storeType === STORE_TYPE.TEMPLATE_ONE) {
        url = '/pages/store/clothingBusiness/index';
      }

      Router.goPage({ url, params });
    },
    // 商品详情
    C0020030000: '/pages/details/index',
    // 购物车
    C0020020000: () => {
      Taro.switchTab({ url: '/pages/cart/index' });
    },
    // 秒杀专区
    C0040020300: '/pages/specialArea/secondKill/index'
  };

  static async goPage(option: GoPageOption) {
    const { url = '', cmd = '', redirect = false } = option;
    const goToFunc = redirect ? Taro.redirectTo : Taro.navigateTo;

    let params = {};
    if (typeof option.params === 'string') {
      try {
        params = JSON.parse(option.params);
      } catch (err) {}
    } else {
      params = option.params || {};
    }
    if (url && /^https?:\/\//i.test(url)) {
      console.log(url);
      // 是网页地址
      if (Router.whiteList.find((u) => url.indexOf(u) > -1)) {
        // 去webview页面
        Router.goWeb(url);
        return true;
      } else {
        console.warn(`地址【${url}】不在小程序后台的业务域名，请联系管理员`);
      }
      // 不在白名单里面的就不用跳转了
      return;
    } else if (url && url.indexOf('page') > -1) {
      Router.goPage({
        cmd: url
      });
    }

    // 处理是cmd码
    if (cmd && /^c\d+$/i.test(cmd)) {
      const match = Router.CMD[cmd];

      if (match) {
        if (typeof match === 'string') {
          return goToFunc({ url: `${match}?${qs.stringify(params)}` });
        } else if (typeof match === 'function') {
          const result = match({ ...option, params });
          // 可以返回一个对象
          if (typeof result === 'object' && result.url) {
            return goToFunc(result);
          }
        } else {
          // 没识别到的cmd码也不处理
          console.warn(`无法处理CMD码【${cmd}】; 参数: ${JSON.stringify(params)}`);
          Router.useAppOpen();
        }
      }

      return false;
    }

    if (url) {
      return goToFunc({ url: qs.stringifyUrl({ url, query: params || {} }) });
    }
    return false;
  }

  static useAppOpen() {
    Taro.showToast({
      title: '请在优托邦app内打开',
      icon: 'none'
    });
  }

  static goHome(option) {
    return Router.goPage(option);
  }
  // 首页连接跳转
  // static goHome(json) {
  //   if (json.url) {
  //     Router.goWeb(json.url);
  //     return;
  //   }
  //   let cmd = json.cmd;
  //   if (cmd === 'C0040010300') {
  //     // 店铺
  //     let obj = JSON.parse(json.params);
  //     if (obj.storeType === 'TEMPLATE_THREE') {
  //       Taro.navigateTo({
  //         url: `/pages/store/shoppingStore/index?storeId=${obj.storeId}&businessId=${obj.businessId}&storeType=${obj.storeType}`
  //       });
  //     }

  //     if (obj.storeType === 'TEMPLATE_ONE') {
  //       Taro.navigateTo({
  //         url: `/pages/store/clothingBusiness/index?storeId=${obj.storeId}&businessId=${obj.businessId}&storeType=${obj.storeType}`
  //       });
  //     }
  //   } else if (cmd === 'C0080100') {
  //     // 停车缴费
  //     Taro.showToast({
  //       title: '请在优托邦app内打开',
  //       icon: 'none'
  //     });
  //   } else if (cmd === 'C0050303') {
  //     // 积分需要登陆
  //     Taro.navigateTo({
  //       url: '/pages/service/integral/index'
  //     });
  //   } else if (cmd === 'C0050090101') {
  //     // 小票积分
  //     Taro.showToast({
  //       title: '请在优托邦app内打开',
  //       icon: 'none'
  //     });
  //   } else if (cmd === 'C0040020100') {
  //     let obj = JSON.parse(json.params);
  //     // 店铺专区
  //     Taro.navigateTo({
  //       url: `/pages/storeHome/index?id=${obj.zoneId}`
  //     });
  //   } else if (cmd === 'C0040020200') {
  //     // liber
  //     Taro.showToast({
  //       title: '请在优托邦app内打开',
  //       icon: 'none'
  //     });
  //   } else if (cmd === 'C0010010000') {
  //     // 小q亲子
  //     Taro.showToast({
  //       title: '请在优托邦app内打开',
  //       icon: 'none'
  //     });
  //   } else {
  //     Taro.showToast({
  //       title: '请在优托邦app内打开',
  //       icon: 'none'
  //     });
  //   }
  // }
  static goWeb(url: string) {
    console.log('url编码');
    console.log(encodeURIComponent(url));
    if (url.indexOf('myutopa.com/m/mall/#/') > -1) {
      return Taro.navigateTo({
        url: url.split('#')[1]
      });
    }
    Taro.navigateTo({
      url: '/pages/web/index?url=' + encodeURIComponent(url)
    });
  }
  static goEditUse() {
    Taro.navigateTo({
      url: '/pages/use/index'
    });
  }
  static goResetRst(id) {
    Taro.navigateTo({
      url: `/pages/person/result/index?type=${id}`
    });
  }
  static goPassword() {
    Taro.navigateTo({
      url: '/pages/person/password/index'
    });
  }
  static goGuize() {
    Taro.navigateTo({
      url: '/pages/integral/rule/index'
    });
  }
  static goPayPassword() {
    Taro.navigateTo({
      url: '/pages/person/payPassword/index'
    });
  }

  static goSetPassword(code, phone) {
    Taro.navigateTo({
      url: `/pages/person/setPassword/index?code=${code}&phone=${phone}`
    });
  }

  static goSendMsg(id) {
    Taro.navigateTo({
      url: `/pages/packOrder/sendMsg/index?id=${id}`
    });
  }

  static goMsgInfo(name, id) {
    name = encodeURI(name);
    Taro.navigateTo({
      url: `/pages/message/msgNav/index?name=${name}&id=${id}`
    });
  }
  static goLogin() {
    Taro.navigateTo({
      url: '/pages/login/index'
    });
  }
  static goPayRstReplace(id, orderNo) {
    Taro.redirectTo({
      url: '/pages/payResult/result/index?id=' + id + '&orderNo=' + orderNo
    });
  }

  static goPay(id, money, parentNo, payOrderNo, isXuni?) {
    Taro.navigateTo({
      url:
        '/pages/payResult/cashier/index?id=' +
        id +
        '&money=' +
        money +
        '&parentNo=' +
        parentNo +
        '&payOrderNo=' +
        payOrderNo +
        '&isScan=' +
        (isXuni ? '1' : '')
    });
  }
  static goSearch(id?, name?, isLiber?) {
    name = encodeURI(name);
    if (id) {
      if (isLiber) {
        Taro.navigateTo({
          url: '/pages/packOrder/search/index?isLiber=1&id=' + id + '&name=' + name
        });
      } else {
        Taro.navigateTo({
          url: '/pages/packOrder/search/index?id=' + id + '&name=' + name
        });
      }
    }
  }

  static goDetails() {
    Taro.navigateTo({
      url: '/pages/details/index?id=66666'
    });
  }
  static goAddress(id?: number) {
    Taro.navigateTo({
      url: '/pages/packOrder/address/index?id=' + id
    });
  }
  static goAddAddress() {
    Taro.navigateTo({
      url: '/pages/packOrder/addAddress/index'
    });
  }
  static goHistory() {
    Taro.navigateTo({
      url: '/pages/service/browsingHistory/index'
    });
  }
  static goAddressLocation(type) {
    if (type) {
      Taro.navigateTo({
        url: '/pages/packOrder/location/index?id=1'
      });
    } else {
      Taro.navigateTo({
        url: '/pages/packOrder/location/index'
      });
    }
  }
  static goLikes() {
    Taro.navigateTo({
      url: '/pages/service/likes/index'
    });
  }
  static goSubmit(type?) {
    Taro.navigateTo({
      url: '/pages/submit/index?type=' + (type ? type : '0')
    });
  }
  static goSubmit1() {
    Taro.navigateTo({
      url: '/pages/submit/index?isScan=1'
    });
  }
  static goStore(storeType, storeId, businessId) {
    if (['FASHION', 'TEMPLATE_ONE'].includes(storeType)) {
      Taro.navigateTo({
        url: `/pages/store/clothingBusiness/index?storeId=${storeId}&businessId=${businessId}&storeType=${storeType}`
      });
    } else {
      Taro.navigateTo({
        url: `/pages/store/shoppingStore/index?storeId=${storeId}&businessId=${businessId}&storeType=${storeType}`
      });
    }
  }
  static goStoreInfo(storeId, latitude, longitude) {
    Taro.navigateTo({
      url: `/pages/store/storeInfo/index?storeId=${storeId}&longitude=${longitude}&latitude=${latitude}`
    });
  }
  static goAfterSale() {
    Taro.navigateTo({
      url: '/pages/person/applyService/index'
    });
  }
  //代发货 选择退款类型
  static selectRefundType() {
    Taro.navigateTo({
      url: '/pages/person/selectRefundType/index'
    });
  }

  //选择 仅退款
  static applyRefund() {
    Taro.navigateTo({
      url: '/pages/person/applyRefund/index?type=1'
    });
  }
  // 退款详情 参数applyRefund
  static refundDetail(num) {
    Taro.navigateTo({
      url: `/pages/person/refundDetail/index?refundOrderNo=${num}`
    });
  }
  static goBack() {
    Taro.navigateBack();
  }
  static goCanBonce() {
    // 小票积分
    Taro.navigateTo({
      url: '/pages/packOrder/canbonce/index'
    });
  }
  static goConfig(id) {
    // 小票积分
    Taro.navigateTo({
      url: `/pages/packOrder/config/index?id=${id}`
    });
  }
  static goScan() {
    Glo.scan(0);
  }
}
