
import Taro, { useState, useRouter, useDidShow } from '@tarojs/taro';
import { View, Swiper, SwiperItem, Text, Image, RichText } from '@tarojs/components';
import { connect } from '@tarojs/redux';
import useRequest from '../../../hooks/useRequest';
import JSBridge from '../../../utils/jsbridge/index';
import { voucherDetail, getPointExchangeGoodsDetail, binConfirmOrder } from '../../../services/integral';
// import { _fetchToken } from '../../../services/login';
import call from '../../../assets/call.png'
import Message from '../../../components/Message/index'
import { goPayPage, inAppIsLogin, successUrl } from '../../../utils/utils'
import './index.scss';


type PageStateProps = {
  user: {
    inApp: boolean;
    isWeiXinFalg: boolean;
  };
}

type PageDispatchProps = {
  setToken: () => void;
};

function VoucherDetail(props) {
  const router = useRouter()
  const [currentIndex, setCurrentIndex] = useState(0)
  const [operateStatus, setOperateStatus] = useState(0)
  const [messageShow, setMessageShow] = useState(false)

  useDidShow(() => {
    if (!props.user.inApp) {
      let data = {
        businessId: router.params.businessId,
        shopId: router.params.shopId,
        goodsId: router.params.goodsId
      }
      getPointExchangeGoodsDetail(data).then(res => {
        if (res.code == 0) {
          setOperateStatus(res.data.operateStatus)
        } else {
          Taro.showToast({
            title: res.msg,
            icon: 'none'
          })
        }
      }) 
    } else {
      inAppIsLogin().then(res => {
        if (res == true) {
          let data = {
            businessId: router.params.businessId,
            shopId: router.params.shopId,
            goodsId: router.params.goodsId
          }
          getPointExchangeGoodsDetail(data).then(response => {
            if (response.code == 0) {
              setOperateStatus(response.data.operateStatus)
            } else {
              Taro.showToast({
                title: response.msg || '系统错误',
                icon: 'none'
              })
            }
          }) 
        }  
      })
    }
  })
  let param = {
    shopGoodsId: router.params.shopGoodsId
  }
  // eslint-disable-next-line no-unused-vars
  const [state, update, loading] = useRequest<any>(param, voucherDetail);
  const swiperChange = (e) => {
    setCurrentIndex(e.detail.current)
  }

  const onSubmit = () => {
    if (operateStatus == 1) {
      return false
    }
    if (props.user.inApp) {
      setMessageShow(true)
      // JSBridge.Bridge.WebViewJavascriptBridge.setup(() => {
      //   // 判断是否登录
      //   JSBridge.Common.Caller_Common_Base_getLoginStatus('', response => {
      //     if (response == 0) {
      //       JSBridge.Common.GTBridge_Common_Base_goLogin();
      //     } else {
      //       // 拿token
      //       JSBridge.Common.Caller_Common_Base_getOpenToken('', responsed => {
      //         if (responsed) {
      //           _fetchToken({
      //               openToken: responsed,
      //             }).then(res => {
      //             if (res.code === 0) {
      //               Taro.setStorage({
      //                 key: 'isLogin',
      //                 data: {
      //                   accessToken: res.data.accessToken
      //                 }
      //               });
      //               props.setToken({accessToken: res.data.accessToken})
      //               setMessageShow(true)
      //             } else {
      //               Taro.showToast({
      //                 title: res.msg,
      //                 icon: 'none'
      //               });
      //             }
      //           })
      //         }
      //       });
      //     }
      //   });
      // })
    } else {
      if (!Taro.getStorageSync('isLogin')) {
        Taro.navigateTo({
          url: '/pages/login/index'
        })
      } else {
        setMessageShow(true)
      }
    }
  }
  const onCancel = () => {
    setMessageShow(false)
  }
  const submit = async () => {
    setMessageShow(false)
    Taro.showLoading({
      title: ''
    });
    let channelId = 1001;
    let ordersource = 2
    if (props.user.inApp) {
      channelId = 1
      ordersource = 1
    } else if (props.user.isWeiXinFalg) {
      channelId = 1002
    }
    let paramData = {
      binComits: [
        {
          ordersource: ordersource,
          storeId: (router.params.storeId != 'null' && router.params.storeId != 'undefined') ? router.params.storeId : '',
          businessId: (router.params.businessId != 'null' && router.params.businessId != 'undefined') ? router.params.businessId : '',
          sysId: channelId,
          pointExchangeGoodsParam: {
            activityId: router.params.activityId,
            shopId: router.params.shopId,
            goodsId: router.params.goodsId,
            businessId: (router.params.businessId != 'null' && router.params.businessId != 'undefined') ? router.params.businessId : '',
            shopGoodsId: router.params.shopGoodsId
          }
        }
      ],
      comitType: 6
    }
    binConfirmOrder(paramData).then(res => {
      if (res.code == 0) {
        if (router.params.exchangePrice && router.params.exchangePrice > 0) {
          let channelId = 1001;
          if (props.user.inApp) {
            channelId = 1
          } else if (props.user.isWeiXinFalg) {
            channelId = 1002
          }
          if (props.user.inApp) {
            const query = {
              payOrderNo: res.data.payOrderNo
            }
            let cmdParams = {
              cmd: 'C0060010000',
              params: JSON.stringify(query)
            };
            JSBridge.Common.GTBridge_Common_Base_CMD(cmdParams, res => {
              console.log(res)
            });
          } else {
            let dataParam = {
              payOrderNo: res.data.payOrderNo,
              sysId: channelId,
              paySucceesHref: successUrl(),
              payFailHref: successUrl()
            }
            goPayPage(dataParam)
          }
        } else {
          Taro.navigateTo({
            url: `/pages/integral/paySuccess/index?point=${router.params.exchangePoint}`
          })
        }
      } else {
        Taro.showToast({
          title: res.msg,
          icon: 'none'
        });
      }
      Taro.hideLoading();
    }) 
    
    
  }
  const callPhone = () => {
    if (props.user.inApp) {
      let params = {
        phoneNum: JSON.stringify(state.data.goods.phone)
      }
      JSBridge.Common.GTBridge_Common_Base_Make_Call(params, res => {})
    } else {
      Taro.makePhoneCall({
        phoneNumber: state && state.data && state.data.goods && String(state.data.goods.phone)
      })
    }
  }
  return (
    <View className="utp-integral-voucher">
      <View className="utp-integral-voucher-imgBox">
        <Swiper circular
                indicatorDots
                indicatorColor="#999"
                indicatorActiveColor="#333"
                current={ currentIndex } 
                onChange={(e) => swiperChange(e)}
               >
          {state && state.data && state.data.goods && state.data.goods.picUrls &&
            state.data.goods.picUrls.length > 0 &&
            state.data.goods.picUrls.map((item, index) => {
              return (
                <SwiperItem key={index} className="utp-integral-voucher-imgBox-index">
                    <Image src={item} className="utp-integral-voucher-imgBox-index-img"/>
                </SwiperItem>
              );
            })
          }
        </Swiper>
        <View className="utp-integral-voucher-imgBox-pedometer">{currentIndex+1}/{state && state.data && state.data.goods && state.data.goods.picUrls.length}</View>
      </View>
      <View className="utp-integral-voucher-good">
        <View className="utp-integral-voucher-good-name">
          {state && state.data && state.data.goods && state.data.goods.name}
        </View>
        <View className="utp-integral-voucher-good-sales">
          已售
          <Text className="utp-integral-voucher-good-sales-num">{state && state.data && state.data.goods && state.data.goods.qtySale}</Text>
        </View>
      </View>
       <View className="utp-integral-voucher-store">
          <View className="utp-integral-voucher-store-message">
            <View className="utp-integral-voucher-store-message-name">{state && state.data && state.data.goods && state.data.goods.shopName}</View>
            <View className="utp-integral-voucher-store-message-address">{state && state.data && state.data.goods && state.data.goods.address}</View>
          </View>
          <View className="utp-integral-voucher-store-call">
            <Image src={call} className="utp-integral-voucher-store-call-img" onClick={() => callPhone()}/>
          </View>
       </View>
       <View className="utp-integral-voucher-message">
          <View className="utp-integral-voucher-message-box">
            <View className="utp-integral-voucher-message-box-title">有效期</View>
            {
              state && state.data && state.data.goods && state.data.goods.validDate ?
              <View className="utp-integral-voucher-message-box-field">{state && state.data && state.data.goods && state.data.goods.validDate}</View> :
              <View className="utp-integral-voucher-message-box-field">暂无相关信息</View>
            }
          </View>
          <View className="utp-integral-voucher-message-box">
            <View className="utp-integral-voucher-message-box-title">使用时间</View>
            {
              state && state.data && state.data.goods && state.data.goods.useDate ?
              <View className="utp-integral-voucher-message-box-field">{state && state.data && state.data.goods && state.data.goods.useDate}</View> :
              <View className="utp-integral-voucher-message-box-field">暂无相关信息</View>
            }
          </View>
          <View className="utp-integral-voucher-message-box">
            <View className="utp-integral-voucher-message-box-title">使用说明</View>
            {
              state && state.data && state.data.goods && state.data.goods.useExplain ?
              <View className="utp-integral-voucher-message-box-field">
                <RichText nodes={ state.data.goods.useExplain }/>
              </View> :
              <View className="utp-integral-voucher-message-box-field">暂无相关信息</View>
            }
          </View>
          <View className="utp-integral-voucher-message-box">
            <View className="utp-integral-voucher-message-box-title">购物须知</View>
            {
              state && state.data && state.data.goods && state.data.goods.saleNotice ?
              <View className="utp-integral-voucher-message-box-field">
                {state.data.goods.saleNotice}
              </View> :
              <View className="utp-integral-voucher-message-box-field">暂无相关信息</View>
            }
          </View>
          <View className="utp-integral-voucher-message-box">
            <View className="utp-integral-voucher-message-box-title">服务介绍</View>
            {
              state && state.data && state.data.goods && state.data.goods.serviceExplain ?
              <View className="utp-integral-voucher-message-box-field">
                { state.data.goods.serviceExplain }
              </View> :
              <View className="utp-integral-voucher-message-box-field">暂无相关信息</View>
            }
          </View>
       </View>
       {
          !router.params.exchangePrice || router.params.exchangePrice == 0 ? 
          <View className="utp-integral-voucher-bottom" onClick={() => onSubmit()} style={operateStatus == 1 ? {backgroundColor: '#ccc'} : ''}>
            <View  className="utp-integral-voucher-bottom-lable">立即兑换（{router.params.exchangePoint}积分）</View>
          </View> : 
          <View className="utp-integral-voucher-bottom" onClick={() => onSubmit()} style={operateStatus == 1 ? {backgroundColor: '#ccc'} : ''}>
            <View  className="utp-integral-voucher-bottom-lable">立即兑换（{router.params.exchangePoint}积分+¥{router.params.exchangePrice / 100}）</View>
          </View> 
       }
       <Message show={messageShow} type={1} title="积分使用" onClose={() => onCancel()} onConfirm={() => submit()} >本次消费将消耗积分账户中{router.params.exchangePoint}积分， 是否继续提交订单？</Message>
    </View>
  );
}

VoucherDetail.config = {
  navigationBarTitleText: '积分兑换抵用券',
  disableScroll: false,
};
const mapStateToProps: PageStateProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    }
  };
};
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    setToken: (params) => {
      dispatch({
        type: 'cart/saveLogin',
        payload: {
          data: params
        }
      })
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(VoucherDetail);