import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import Wrap from '../../components/Wrap';
import Cnt from '../../components/Cnt';
import Txt from '../../components/Txt';
import { connect } from '@tarojs/redux';

import './index.scss';
import { images } from '../../images';

type PageStateProps = {
  closeWidth: number;
  white: boolean;
  num: number;
  // counter: {
  //   num: number;
  // };
};

type PageDispatchProps = {
  // add: () => void;
  // dec: () => void;
  // asyncAdd: () => any;
};

type PageOwnProps = {
  title?: string;
  leftIcon?: any;
  rightIcon?: any;
  className?: string;
  bg?: any;
  rightTitle?: string;
  goBack?: boolean;
  onClick?: Function;
};

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

const Index: Taro.FC<IProps> = ({
  rightTitle = '',
  title,
  leftIcon,
  rightIcon,
  bg = 'white',
  className,
  goBack = false,
  onClick,
  closeWidth,
  white = false,
  num = 0
}) => {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */

  return (
    <View style={{ background: bg }} className="utp-bar">
      <Wrap type={1}>
        {leftIcon ? (
          <View
            className="utp-bar-back utp-cnt"
            onClick={() => {
              // console.log(11);
            }}
          >
            <Cnt>
              <Image src={leftIcon} className="utp-bar-ico" />
            </Cnt>
          </View>
        ) : (
          <View
            className="utp-bar-back utp-cnt"
            onClick={() => {
              Taro.navigateBack();
            }}
          >
            {goBack && (
              <Cnt>
                <Image src={white ? images.backWhite : images.back} className="utp-bar-ico" />
              </Cnt>
            )}
          </View>
        )}

        <View className="utp-bar-tits utp-cnt">
          <Text
            className={'utp-bar-title ' + className}
            style={{ color: white ? '#ffffff' : '#000000' }}
          >
            {title}
          </Text>
        </View>
        <View
          className="utp-cnt"
          style={{ position: 'absolute', top: 0, bottom: 0, right: closeWidth + 'px' }}
          onClick={() => {
            if (onClick) {
              onClick();
            }
          }}
        >
          {rightIcon ? (
            <Cnt>
              <Image src={rightIcon} className="utp-bar-ico" />
            </Cnt>
          ) : (
            <Text className={'utp-bar-tips ' + className}>{rightTitle}</Text>
          )}
          {!!num && (
            <View
              style={{
                position: 'absolute',
                zIndex: 1,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                top: Taro.pxTransform(0),
                right: Taro.pxTransform(-10),
                left: Taro.pxTransform(30)
              }}
            >
              <View
                className="utp-div utp-solid"
                style={{
                  height: Taro.pxTransform(32),
                  minWidth: Taro.pxTransform(32),
                  borderWidth: Taro.pxTransform(2),
                  borderColor: '#ff2f7b',
                  backgroundColor: '#ff2f7b',
                  borderRadius: Taro.pxTransform(32),
                  paddingLeft: Taro.pxTransform(18),
                  paddingRight: Taro.pxTransform(18),
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Txt size={23} color={'white'} title={num}></Txt>
              </View>
            </View>
          )}
        </View>
      </Wrap>
    </View>
  );
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight,
    closeWidth: state.cart.closeWidth
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
