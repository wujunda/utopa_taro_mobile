import Taro, { useState, useEffect, useRouter } from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';
import Safe from '@/components/safe';
import './index.scss';
import { getHistoryList } from '../../../services/afterSale';
import { handleRequest, Glo } from '@/utils/utils';
import ViewHistoryItem from './ViewHistoryItem';

function NegotiationHistory() {
  const router = useRouter();
  const [historyList, setList] = useState<any>([]);
  useEffect(() => {
    _getHistoryList();
  }, []);

  const _getHistoryList = async () => {
    Glo.loading('正在加载...');
    try {
      let res = await getHistoryList({ refundOrderId: router.params.refundOrderId });
      if (handleRequest(res as any) && Array.isArray(res.data)) {
        // setList(res.data.reverse());
        setList(res.data);
      }
    } catch (e) {
    } finally {
      Glo.hideLoading();
    }
  };

  return (
    <Safe>
      {/* <Bar title="协商历史" /> */}
      <View className="history">
        <ScrollView
          refresherEnabled
          refresherBackground="#333"
          className="utp-bd utp-bd-page"
          scrollY
        >
          {historyList &&
            historyList.map((item, idx) => {
              return <ViewHistoryItem item={item} key={idx} />;
            })}
          {(!historyList || historyList.length === 0) && (
            <View
              style={{
                textAlign: 'center',
                fontSize: Taro.pxTransform(30),
                marginTop: Taro.pxTransform(120)
              }}
            >
              暂无数据
            </View>
          )}
        </ScrollView>
      </View>
    </Safe>
  );
}

NegotiationHistory.config = {
  navigationBarTitleText: '协商历史',
  enablePullDownRefresh: true,
  disableScroll: false,
  navigationStyle: 'default'
};
// @ts-ignore
// eslint-disable-next-line no-undef
export default NegotiationHistory;
