type Size = number | string;

interface Margin {
  margin: Size;

  marginTop: Size;

  marginRight: Size;

  marginBottom: Size;

  marginLeft: Size;
}

interface Padding {
  padding: Size;

  paddingTop: Size;

  paddingRight: Size;

  paddingBottom: Size;

  paddingLeft: Size;
}

interface Border {
  borderWidth: Size;

  borderStyle: 'solid' | 'dash';

  borderRadius: Size;
}

interface Position {
  position: 'absolute' | 'fixed' | undefined;

  top: Size;

  right: Size;

  bottom: Size;

  left: Size;
}

export interface ComStyle extends Margin, Padding, Border, Position {
  width: Size;

  heigth: Size;

  opacity: number;

  backgroundColor: string;

  backgroundImage: string;
}

export interface ComponentData<T extends object> {
  // 组件id
  uid: string;

  // 组件名称
  name: string;

  // 组件标识
  type: string;

  // 组件唯一码
  code: string;

  // 组件基础数据
  data: T;

  // 数据标识，表明数据源来自哪里
  source: string;

  // 数据标识
  sourceParams: object;

  // 组件样式
  style: ComStyle;

  // 埋点统计
  trackStat: {};

  // 父组件id
  parentUid: number;

  // 子组件
  childs: ComponentData<any>[];
}
