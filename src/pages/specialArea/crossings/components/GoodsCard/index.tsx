import Taro, { useRouter, pxTransform } from '@tarojs/taro';
import { View, Text, Image } from '@tarojs/components';
import JSBridge from '@/utils/jsbridge/index';
import { connect } from '@tarojs/redux';
import { PriceFormat } from '@/utils/utils';
import lightning from '@/assets/special/lightning.png';

import './index.scss';
import { COMMON_IMAGES } from '../../../common/images';

const GoodsCard = (props) => {
  const router = useRouter();
  const crm_channel_id = +router.params.crm_channel_id || null;
  const { goods, type } = props;

  const toDetails = (item) => {
    if (props.user.inApp) {
      const cmd = item.command;
      if (crm_channel_id) {
        const cmdParams = JSON.parse(cmd.productCommand);
        cmdParams['crm_channel_id'] = crm_channel_id;
        cmd.params = JSON.stringify(cmdParams);
      }
      JSBridge.Common.GTBridge_Common_Base_CMD(cmd, () => {
        console.log('执行', cmd);
      });
    } else {
      if (crm_channel_id) {
        Taro.navigateTo({
          url: `/pages/details/index?id=${item.productId}&storeId=${item.storeId}&skuId=${item.skuId}&crm_channel_id=${crm_channel_id}`
        });
      } else {
        Taro.navigateTo({
          url: `/pages/details/index?id=${item.productId}&storeId=${item.storeId}&skuId=${item.skuId}`
        });
      }
    }
  };

  return (
    <View className="goodsCard">
      <Image className="goodsCard-logo" src={goods.productImage} onClick={() => toDetails(goods)} />
      <View>
        {goods.selfSupport === 1 && <View className="goodsCard-selfTag">自营</View>}
        <View
          className="goodsCard-title"
          style={goods.selfSupport === 1 ? { textIndent: pxTransform(70) } : {}}
        >
          {goods.productName}
        </View>
      </View>
      <View>{goods.activeLabel && <View className="goodsCard-tag">{goods.activeLabel}</View>}</View>
      {!!goods.tags[0] && (
        <View
          className="goodsCard-lightningTag"
          style={goods.activeLabel && { left: pxTransform(110) }}
        >
          <Image src={lightning} className="goodsCard-lightningTag-icon" />
          <Text className="goodsCard-lightningTag-text">{goods.tags[0].tagText}</Text>
        </View>
      )}
      <View style={{ position: 'absolute', bottom: pxTransform(16), left: pxTransform(16) }}>
        <View className="goodsCard-salePrice">{PriceFormat(goods.salePrice)}</View>
        {!!goods.originPrice  && (
          <View className="goodsCard-originPrice">¥{PriceFormat(goods.originPrice)}</View>
        )}
      </View>
      {type === 1 && (
        <View
          className="goodsCard-add"
          onClick={() => {
            if (props.user.inApp) {
              JSBridge.Common.Caller_Common_Base_getLoginStatus({}, (resp) => {
                if (resp == 0) {
                  JSBridge.Common.GTBridge_Common_Base_goLogin();
                  return;
                }
              });
            }
            props.addToCart({
              productId: goods.productId,
              number: 1,
              skuId: goods.skuId,
              storeId: goods.storeId,
              businessId: goods.businessId
            });
          }}
        >
          <Image
            src={COMMON_IMAGES.add_card}
            style={{
              width: Taro.pxTransform(44),
              height: Taro.pxTransform(44)
            }}
          ></Image>
        </View>
      )}
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    user: {
      inApp: state.user.inApp,
      isWeiXinFalg: state.user.isWeiXinFalg
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (payload) => {
      dispatch({
        type: 'cart/addCart',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(GoodsCard as any);
