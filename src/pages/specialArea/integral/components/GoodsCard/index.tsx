import Taro, { useRouter, pxTransform } from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import JSBridge from '@/utils/jsbridge/index';
import { PriceFormat } from '@/utils/utils';

import './index.scss';

const GoodsCard = (props) => {
  const router = useRouter();
  const crm_channel_id = +router.params.crm_channel_id || null;
  const { goods, inApp } = props;

  function toGoods(item) {
    if (process.env.TARO_ENV === 'h5') {
      if (inApp) {
        let appBar = { isHidenNaviBar: false };
        let data = {
          cmd: '',
          url: crm_channel_id
            ? `${item.pointH5Url}&crm_channel_id=${crm_channel_id}`
            : item.pointH5Url,
          h5Params: JSON.stringify(appBar)
        };
        JSBridge.Common.GTBridge_Common_Base_CMD(data, () => {
          console.log('执行');
        });
      } else {
        if (crm_channel_id) {
          // Taro.navigateTo({
          //   url: `${item.pointH5Url}&crm_channel_id=${crm_channel_id}`
          // });
          window.location.href = `${item.pointH5Url}&crm_channel_id=${crm_channel_id}`;
        } else {
          window.location.href = item.pointH5Url;
        }
      }
    } else {
      if (item && item.exchangeType === 1) {
        // 积分详情
        Taro.navigateTo({
          url: `/pages/details/index?id=${item.productId}&storeId=${item.storeId}&skuId=${item.skuId}&jifen=1`
        });
      } else if (item && item.exchangeType === 2) {
        // 优惠券详情
        if (item.exchangePrice) {
          Taro.navigateTo({
            url: `/pages/integral/exchangeCouponDetail/index?couponId=${item.productId}&activityId=${item.activityId}&storeId=${item.storeId}&businessId=${item.businessId}&exchangePoint=${item.exchangePoint}&exchangePrice=${item.exchangePrice}`
          });
        } else {
          Taro.navigateTo({
            url: `/pages/integral/exchangeCouponDetail/index?couponId=${item.productId}&activityId=${item.activityId}&storeId=${item.storeId}&businessId=${item.businessId}&exchangePoint=${item.exchangePoint}`
          });
        }
      } else if (item && item.exchangeType === 3) {
        // 抵用券详情
        if (item.exchangePrice) {
          Taro.navigateTo({
            url: `/pages/integral/exchangeVoucherDetail/index?goodsId=${item.productId}&shopId=${item.shopId}&shopGoodsId=${item.shopGoodId}&activityId=${item.activityId}&businessId=${item.businessId}&storeId=${item.storeId}&exchangePoint=${item.exchangePoint}&exchangePrice=${item.exchangePrice}`
          });
        } else {
          Taro.navigateTo({
            url: `/pages/integral/exchangeVoucherDetail/index?goodsId=${data.productId}&shopId=${data.shopId}&shopGoodsId=${data.shopGoodId}&activityId=${data.activityId}&businessId=${data.businessId}&storeId=${data.storeId}&exchangePoint=${data.exchangePoint}`
          });
        }
      }
    }
  }

  return (
    <View className="integralGoods" onClick={() => toGoods(goods)}>
      <Image className="integralGoods-logo" src={goods.productImage} />
      <View style={{ paddingLeft: pxTransform(12), paddingRight: pxTransform(12) }}>
        <Text className="integralGoods-title">{goods.productName}</Text>
        {goods.pointLabel && <View className="integralGoods-tag">{goods.pointLabel}</View>}
        <View style={{ position: 'absolute', bottom: pxTransform(20) }}>
          <View className="integralGoods-discount">
            {goods.exchangePoint && (
              <Text className="discount-details">{goods.exchangePoint}积分</Text>
            )}
            {goods.exchangePrice > 0 && (
              <Text className="discount-details">+{PriceFormat(goods.exchangePrice)}元</Text>
            )}
          </View>
          {goods.originPrice && (
            <View className="integralGoods-price">市场价：¥{PriceFormat(goods.originPrice)}</View>
          )}
        </View>
      </View>
    </View>
  );
};

export default GoodsCard;
