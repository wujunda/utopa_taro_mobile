/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-key */
/* eslint-disable import/first */
import Taro, { useState, useEffect, useRouter, useDidShow } from '@tarojs/taro';
import { View, Text, Picker, Input } from '@tarojs/components';
import './index.scss';
import Wrap from '@/components/Wrap';
import { getRefundOrderExpressRoute } from '../../../services/common';
import { handleRequest, Glo } from '@/utils/utils';

import useSession from '@/hooks/useSession';
import useLogin from '@/hooks/useLogin';
import ViewPickReason from '../applyRefund/selectPick';
import Router from '@/utils/router';
import { isEmpty } from 'lodash';

const testData = [
  {
    time: '2020-07-05 17:24:13',
    context: '【广州市】您的包裹已签收，期待下次为您服务，感谢使用丹鸟，期待再次为您服务',
    ftime: '2020-07-05 17:24:13'
  },
  {
    time: '2020-07-05 15:25:30',
    context: '【广州市】广州大沙站Z派件员：唐庆延 电话：18928778022 当前正在为您派件',
    ftime: '2020-07-05 15:25:30'
  },
  {
    time: '2020-07-05 14:37:02',
    context: '【广州市】您的包裹已到达【广州大沙站Z】，准备分配派送员',
    ftime: '2020-07-05 14:37:02'
  },
  {
    time: '2020-07-05 12:11:12',
    context: '干线已揽收：干线司机【喻金芳】',
    ftime: '2020-07-05 12:11:12'
  },
  {
    time: '2020-07-05 10:03:03',
    context: '【广州市】您的订单已从【广州增城分拨中心】发出',
    ftime: '2020-07-05 10:03:03'
  },
  { time: '2020-07-05 10:02:11', context: '包裹已完成分拨集包', ftime: '2020-07-05 10:02:11' },
  {
    time: '2020-07-05 08:08:03',
    context: '【广州市】您的订单已进入【广州增城分拨中心】，开始分拣',
    ftime: '2020-07-05 08:08:03'
  },
  { time: '2020-07-05 08:08:03', context: '包裹已揽收完成', ftime: '2020-07-05 08:08:03' }
];

const Index: Taro.FC<any> = (props: any) => {
  const [haveSession] = useSession();
  const router = useRouter();
  const [list, setList] = useState<any>([]);
  const [info, setInfo] = useState<any>({});

  useEffect(() => {
    Glo.loading('正在获取数据...');
    setTimeout(() => {
      _getRefundOrderExpressRoute();
    }, 1200);
  }, []);
  useDidShow(() => {});
  // 物流信息

  const _getRefundOrderExpressRoute = async () => {
    // companyCode*	string
    // 快递公司编码
    // expressNo*	string
    // 快递单号
    // companyCode=zhongtong&expressNo=751213841320
    try {
      let params = {
        companyCode: router.params.companyCode,
        expressNo: router.params.expressNo
      };
      let res: any = await getRefundOrderExpressRoute(params);
      if (handleRequest(res)) {
        setList(res.data.routingDTOList);
        setInfo(res.data);
      }
    } catch (e) {
      console.log(e);
    } finally {
      Glo.hideLoading();
    }
  };

  /***
   * 获取物流信息
   * ***/

  //eslint-disable-next-line react/no-multi-comp
  return (
    <View className="logis-box">
      <View className="head-info">
        <View className="head-title">退货信息</View>
        <View className="head-content">
          <Wrap type={1}>
            <View>承运公司:</View>
            <View>{info.companyCode}</View>
          </Wrap>
          <Wrap type={1}>
            <View>运单编号:</View>
            <View>{info.expressCode}</View>
          </Wrap>
          <Wrap type={1}>
            <View>发货时间:</View>
            <View>{info.updateTime}</View>
          </Wrap>
        </View>
      </View>
      <View className="list-box">
        <View className="list-head"> 物流信息</View>
        <View className="list-content">
          {list &&
            list.length > 0 &&
            list.map((item, index) => {
              return (
                <View className="list-item">
                  {index === 0 && <View className="tag"> </View>}
                  <View className="time" style={{ display: 'inline-block' }}>
                    {item.ftime}
                  </View>
                  <View className="content"> {item.context} </View>
                </View>
              );
            })}
          {list.length === 0 && <View style={{ textAlign: 'center' }}>暂无数据</View>}
        </View>
      </View>
    </View>
  );
};

Index.config = {
  navigationBarTitleText: '查看物流',
  enablePullDownRefresh: true,
  disableScroll: false
};

export default Index;
// @ts-ignore
// export default Index;
