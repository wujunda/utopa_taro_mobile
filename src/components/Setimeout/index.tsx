import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';

import './index.scss';
import Wrap from '../Wrap';
import Txt from '../Txt';
import useEndTime from '../../hooks/useEndTime';

interface IProps {
  seconds?: number | null;
  background?: string;
  type?: number;
}
const Index: Taro.FC<IProps> = ({ seconds = 8800, background, type = 2 }) => {
  const [s] = useEndTime(seconds);
  function getDay(s: number) {
    let tmp: any = Math.floor(s / (60 * 60 * 24));
    return tmp;
  }

  function getHour(s: number) {
    let tmp: any = Math.floor((s % (60 * 60 * 24)) / (60 * 60));
    return getZero(tmp);
  }

  function getMinutes(s: number) {
    let tmp = (Number(Math.floor(s / 60)) % 60).toString();
    return getZero(tmp);
  }

  function getsettimerond(s: number) {
    let tmp = (s % 60).toString();
    return getZero(tmp);
  }
  function getZero(i: number | string) {
    if (Number(i) < 10) {
      return '0' + i;
    } else {
      return i;
    }
  }

  return (
    <View className="utp-settimer">
      {/* 无边距无背景 */}
      {type == 1 && (
        <View className="utp-settimer-re">
          <Wrap>
            <Txt title="剩余" size={28} />
            <View className="utp-settimer-lableBox" style={{ background }}>
              <Text className="utp-settimer-lableBox-lable">{getDay(s)}天</Text>
            </View>
            <View className="utp-settimer-lableBox" style={{ background }}>
              <Text className="utp-settimer-lableBox-lable"> {getHour(s)}:</Text>
            </View>
            <View className="utp-settimer-lableBox" style={{ background }}>
              <Text className="utp-settimer-lableBox-lable"> {getMinutes(s)}:</Text>
            </View>
            <View className="utp-settimer-lableBox" style={{ background }}>
              <Text className="utp-settimer-lableBox-lable"> {getsettimerond(s)} </Text>
            </View>
          </Wrap>
        </View>
      )}
      {/* 有背景有边距 */}
      {type === 2 && (
        <View className="utp-settimer-re">
          <Wrap>
            <Txt title="剩余" size={28} />
            <View className="utp-settimer-box" style={{ background }}>
              <Text className="utp-settimer-box-txt">{getDay(s)}</Text>
            </View>
            <Txt title="天" size={24} />
            <View className="utp-settimer-box" style={{ background }}>
              <Text className="utp-settimer-box-txt">{getHour(s)}</Text>
            </View>
            <Txt title=":" color="red" size={28} />
            <View className="utp-settimer-box" style={{ background }}>
              <Text className="utp-settimer-box-txt">{getMinutes(s)}</Text>
            </View>
            <Txt title=":" color="red" size={28} />
            <View className="utp-settimer-box" style={{ background }}>
              <Text className="utp-settimer-box-txt">{getsettimerond(s)}</Text>
            </View>
          </Wrap>
        </View>
      )}

      <View className="utp-settimer-re" style={{ display: type === 3 ? 'block' : 'none' }}>
        <View className="utp-settimer-type3">
          {'剩余时间 : ' + getMinutes(s) + '分' + getsettimerond(s) + '秒'}
        </View>
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
