import Taro, { useState, useDidShow } from '@tarojs/taro';
import { View, Text, ScrollView } from '@tarojs/components';
import { connect } from '@tarojs/redux';

import Safe from '../../components/safe';
import Sch from '../../components/sch';
import useRequest from '../../hooks/useRequest';
import useResetCount from '../../hooks/useResetCount';
import useSession from '../../hooks/useSession';
import { ICate, getCate } from '../../services/cate';
import './index.scss';
import SubCate from './subCate/index';

interface Props {
  statusHeight: number;
  closeWidth: number;
  unread: number;
}
const Index: Taro.FC<Props> = (props) => {
  useDidShow(() => {});
  const [crtCate, setCrtCate] = useState<any>(false);
  const [haveSession] = useSession();
  const [can, update1] = useResetCount();
  // 一级分类
  const [state, update, loading] = useRequest<ICate>('name=a&age=1', getCate);
  if (haveSession && state === null && update && !loading) {
    if (can) {
      update();
      update1();
    }
  }
  // 切换分类
  const toggleCate = (id: number) => {
    setCrtCate(0);
    setTimeout(() => {
      setCrtCate(id);
    }, 0);
  };
  // 默认选中第一个
  if (crtCate === false && state != null && state.code === 0 && state.data.list.length > 0) {
    var id = state.data.list[0].foreCategoryId || state.data.list[0].id;
    toggleCate(id);
  }

  return (
    <Safe>
      {!!state && !!state.data && (
        <View className="utp-html">
          <View className="utp-cate-box" style={{ top: props.statusHeight + 'px' }}>
            <View className="utp-sch-box1">
              <Sch
                unread={props.unread}
                closeWidth={props.closeWidth}
                onScan={() => {
                  if (process.env.TARO_ENV === 'weapp') {
                    Taro.navigateTo({ url: '/pages/scan/scanCode/index' });
                  }
                }}
              />
            </View>
            <ScrollView className="utp-cate-l" scrollY refresherEnabled refresherBackground="#333">
              {state.data.list.map((item, index) => {
                return (
                  <View
                    className={
                      (item.foreCategoryId || item.id) === crtCate
                        ? 'utp-cate utp-cate-act'
                        : 'utp-cate'
                    }
                    key={index}
                    onClick={() => {
                      toggleCate(item.foreCategoryId || item.id);
                    }}
                  >
                    <View
                      className={
                        (item.foreCategoryId || item.id) === crtCate
                          ? 'utp-cate-g utp-cate-g-crt'
                          : 'utp-cate-g'
                      }
                    ></View>
                    <Text
                      className={
                        (item.foreCategoryId || item.id) === crtCate
                          ? 'utp-txt utp-txt-crt'
                          : 'utp-txt'
                      }
                    >
                      {item.name}
                    </Text>
                  </View>
                );
              })}
            </ScrollView>

            <ScrollView className="utp-cate-r" scrollY>
              <View className="utp-cates">
                {!!crtCate && <SubCate id={crtCate} id1={crtCate} />}
              </View>
            </ScrollView>
            <View className="cart"></View>
          </View>
        </View>
      )}
    </Safe>
  );
};
Index.config = {
  navigationBarTitleText: '购物车',
  disableScroll: true,
  navigationStyle: 'custom',
  usingComponents: {
    Bar: '../../components/bar' // 书写第三方组件的相对路径
  }
};

Index.defaultProps = {};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    statusHeight: state.cart.statusHeight,
    closeWidth: state.cart.closeWidth,
    unread: state.cart.unread
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {};
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
