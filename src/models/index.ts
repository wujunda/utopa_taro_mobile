import example from './example';
import user from './user';
import cart from './cart';


export default [example, user, cart];
