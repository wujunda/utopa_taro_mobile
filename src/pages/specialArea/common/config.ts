/* eslint-disable no-unused-vars */
import { urlToObject } from '../../../utils/utils';
// eslint-disable-next-line import/first
import Taro from '@tarojs/taro';
// import Taro from '@tarojs/taro';
import { wxRegister, shareCallbackList } from '../../../utils/wxApi';
import JSBridge from '../../../utils/jsbridge/index';

// 专区类型
export const ZONE_TYPES = {
  SAME_CITY: 1,
  S_KILL: 2,
  COLLAGE: 3,
  BRAND: 4,
  NEWBORN_ZONE: 5,
  INTEGRAL_MALL: 6,
  CROSSINGS_1: 7,
  CROSSINGS_2: 8,
  CROSSINGS_3: 9
};

// eslint-disable-next-line import/prefer-default-export
export enum SPECIAL_AREA_TYPE {
  '同城好货' = ZONE_TYPES.SAME_CITY,
  '秒杀专区' = ZONE_TYPES.S_KILL,
  '拼团专区' = ZONE_TYPES.COLLAGE,
  '品牌馆' = ZONE_TYPES.BRAND,
  '新人专享' = ZONE_TYPES.NEWBORN_ZONE,
  '积分商城' = ZONE_TYPES.INTEGRAL_MALL,
  'CROSSINGS专区_1' = ZONE_TYPES.CROSSINGS_1,
  'CROSSINGS专区_2' = ZONE_TYPES.CROSSINGS_2,
  'CROSSINGS专区_3' = ZONE_TYPES.CROSSINGS_3
}

// 统一刷新下拉加载文案

export const INDIC_TEXT = { release: '加载中', activate: '下拉刷新', deactivate: '释放刷新' };

const url_com = {
  prod: 'http://www.myutopa.com/m/campaign/#/activity/shopDetail',
  pred: 'http://www-pre.myutopa.com/m/campaign/#/activity/shopDetail',
  dev: 'http://www-dev.myutopa.com/m/campaign/#/activity/shopDetail',
  test: 'http://www-test.myutopa.com/m/campaign/#/activity/shopDetail'
};
// 旧商品 链接
export const getShopDetailUrl = () => {
  let env = process.env.SERVER_ENV;
  return url_com[env];
};

// 跳转旧系统
export const goGoodsDetail = (id: any, storeId: any) => {
  let URL = getShopDetailUrl() + `?id=${id}&storeId=${storeId}`;
  let GET_PARAMS: any = urlToObject();
  if (GET_PARAMS.crm_channel_id) {
    URL = URL + '&crm_channel_id=' + GET_PARAMS.crm_channel_id;
  }
  window.location.href = URL;
};

interface ShareProps {
  title: string;
  link?: string;
  desc?: string | number;
  imgUrl?: string;
}

// URL 参数截取 转对象
const url_params: any = urlToObject();

// 跳转页
export const commonNavigateTo = (page: any) => {
  if (process.env.TARO_ENV !== 'h5') return;
  // eslint-disable-next-line react-hooks/rules-of-hooks
  if (url_params.crm_channel_id) {
    page = page + '&crm_channel_id=' + url_params.crm_channel_id;
  }
  Taro.redirectTo({
    url: page
  });
};
// type:1 默认 H5分享 2 app 内掉起 原生分享弹框
export const shareAppMessage = (shareParams: ShareProps, type = 1, cb?: any) => {
  if (process.env.TARO_ENV !== 'h5') return;
  const { title, link, desc, imgUrl } = shareParams;
  let shareLink = link || window.location.href;
  if (url_params.crm_channel_id) {
    shareLink = shareLink + '&crm_channel_id=' + url_params.crm_channel_id;
  }
  const DEF_IMG =
    'https://image.myutopa.com/Fr0Vlg2LNy2owURS5bDu1sNxfrHl?e=1614217121&token=WNtUYaMiCdpTDzanlE03SjKXLinbmTzkLGXEBYC_:RSZxw6kzc_1z1ua7yQifGs1YLkg=';
  // eslint-disable-next-line no-undef
  const source = Taro.getEnv() === Taro.ENV_TYPE.WEAPP ? 'wx' : 'h5';
  // h5 微信分享
  switch (type) {
    case 1:
      wxRegister(
        shareCallbackList({
          title: title || '分享标题',
          desc: desc || '分享描述',
          link: shareLink,
          imgUrl: imgUrl || DEF_IMG
        })
      );
      break;
    //H5页面调起原生分享弹框
    case 2:
      let share = {
        shareTitle: title || '分享标题',
        subTitle: desc || '',
        shareIcon: imgUrl || DEF_IMG,
        shareUrl: shareLink
      };
      JSBridge.Common.GTBridge_Common_Base_showShare(share, (res) => {
        if (res) {
          //分享成功回调
          cb && cb(res);
        }
      });
      break;
    default:
  }
  // eslint-disable-next-line react-hooks/rules-of-hooks
  //   useShareAppMessage((res) => {
  //     console.log(res, 'res');
  //     if (res.from === 'button') {
  //       // 来自页面内转发按钮
  //       console.log(res.target);
  //     }
  //     return {
  //       title: title || '自定义转发标题',
  //       path: link || '/page/user?id=123'
  //     };
  //   });
  // eslint-disable-next-line react-hooks/rules-of-hooks
};
