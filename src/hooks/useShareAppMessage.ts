import Taro, { useRouter } from '@tarojs/taro';
// import Taro from '@tarojs/taro';
import { wxRegister, shareCallbackList } from '@/utils/wxApi';
import JSBridge from '@/utils/jsbridge/index';

interface ShareProps {
  title: string;
  link?: string;
  desc?: string | number;
  imgUrl?: string;
}
var urlToObject = function (url) {
  var urlObject = {};
  if (/\?/.test(url)) {
    var urlString = url.substring(url.indexOf('?') + 1);
    var urlArray = urlString.split('&');
    for (var i = 0, len = urlArray.length; i < len; i++) {
      var urlItem = urlArray[i];
      var item = urlItem.split('=');
      urlObject[item[0]] = item[1];
    }
    return urlObject;
  }
};
// URL 参数截取 转对象
const url_params: any = urlToObject(window.location.href);

// 跳转页
export const commonNavigateTo = (page: any) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  if (url_params.crm_channel_id) {
    page = page + '&crm_channel_id=' + url_params.crm_channel_id;
  }
  Taro.navigateTo({
    url: page
  });
};

// type:1 默认 H5分享 2 app 内掉起 原生分享弹框
export const shareAppMessage = (shareParams: ShareProps, type = 1, cb?: any) => {
  if (process.env.TARO_ENV !== 'h5') return;
  const { title, link, desc, imgUrl } = shareParams;
  let shareLink = link || window.location.href;
  if (url_params.crm_channel_id) {
    shareLink = shareLink + '&crm_channel_id=' + url_params.crm_channel_id;
  }
  const DEF_IMG =
    'https://image.myutopa.com/Fr0Vlg2LNy2owURS5bDu1sNxfrHl?e=1614217121&token=WNtUYaMiCdpTDzanlE03SjKXLinbmTzkLGXEBYC_:RSZxw6kzc_1z1ua7yQifGs1YLkg=';
  // eslint-disable-next-line no-undef
  const source = Taro.getEnv() === Taro.ENV_TYPE.WEAPP ? 'wx' : 'h5';
  // h5 微信分享
  switch (type) {
    case 1:
      wxRegister(
        shareCallbackList({
          title: title || '分享标题',
          desc: desc || '分享描述',
          link: shareLink,
          imgUrl: imgUrl || DEF_IMG
        })
      );
      break;
    //H5页面调起原生分享弹框
    case 2:
      let share = {
        shareTitle: title || '分享标题',
        subTitle: desc || '',
        shareIcon: imgUrl || DEF_IMG,
        shareUrl: shareLink
      };
      JSBridge.Common.GTBridge_Common_Base_showShare(share, (res) => {
        if (res) {
          //分享成功回调
          cb && cb(res);
        }
      });
      break;
    default:
  }
  // eslint-disable-next-line react-hooks/rules-of-hooks
  //   useShareAppMessage((res) => {
  //     console.log(res, 'res');
  //     if (res.from === 'button') {
  //       // 来自页面内转发按钮
  //       console.log(res.target);
  //     }
  //     return {
  //       title: title || '自定义转发标题',
  //       path: link || '/page/user?id=123'
  //     };
  //   });
  // eslint-disable-next-line react-hooks/rules-of-hooks
};

export const share = () => {};
