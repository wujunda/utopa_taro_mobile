import Taro, { useState, useEffect, useRouter } from '@tarojs/taro';
import { View, Button, Input, Image } from '@tarojs/components';
import { connect } from '@tarojs/redux';

import eye from '@/assets/eye.png';
import eye1 from '@/assets/eye2.png';
import uncheck from '@/assets/submit/uncheck.png';
import check from '@/assets/cart/check.png';
import weChatIcon from '@/assets/login/icon_wechat.png';
import Wrap from '../../components/Wrap';
import Safe from '../../components/safe';
import Img from '../../components/Img';
import wechatLogo from '../../assets/login/wechat_logo.png';
import personal_pic_portrait from '../../assets/mine/personal_pic_portrait.png';
import {
  getLoginNone,
  getPhone,
  getLoginOther,
  getLogin,
  sendLoginCheckCode,
  getOauthUrl
} from '../../services/login';
import Router from '../../utils/router';
import Txt from '../../components/Txt';

import './index.scss';

interface IProps {
  isLogin: any;
  isWeiXin: Boolean;
  weChatParam: String;
  saveLogin: (any) => void;
  getUnread: () => void;
}

let code1 = '';
let uid = '';
let USER: any = {};
const Index: Taro.FC<IProps> = (props) => {
  const router = useRouter();
  const [needPhone, setNeedPhone] = useState(false);
  const [status, setStatus] = useState(false);
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [agreen, setAgreen] = useState(true);
  const [seconds, setSeconds] = useState(60);
  const [isSend, setIsSend] = useState(false);
  const [code, setCode] = useState(false);
  const [type, setType] = useState(true);
  const [checkCode, setCheckCode] = useState(['', '', '', '', '', '']);
  const [actCode, setActCode] = useState('');
  const [isWchat, setIsWchat] = useState(false);
  useEffect(() => {
    setIsWchat(props.isWeiXin);
  }, [props.isWeiXin]);

  const onLogin = async (pay) => {
    if (!testPhone(phone)) {
      Taro.showToast({
        title: '手机号有误',
        icon: 'none'
      });
      return;
    }
    //走密码
    if (!code) {
      if (!password) {
        Taro.showToast({
          title: '输入密码',
          icon: 'none'
        });
        return;
      }
      if (!agreen) {
        Taro.showToast({
          title: '同意协议',
          icon: 'none'
        });
        return;
      }
    }
    let payload;
    if (pay) {
      payload = {
        mobile: phone,
        checkCode: pay,
        loginType: 1
      };
    } else {
      payload = {
        account: phone,
        password: password,
        loginType: 2
      };
    }
    let data = await getLogin(payload);
    if (data.code === 0) {
      onSus(data.data);
    } else {
      Taro.showToast({
        title: data.msg || '系统错误',
        icon: 'none'
      });
    }
  };
  const onSus = (data) => {
    // console.log('登陆成功');
    // console.log(code1);
    props.saveLogin({
      data: data
    });

    Taro.setStorage({
      key: 'isLogin',
      data: data
    });
    props.getUnread();
    //if (process.env.TARO_ENV !== 'weapp') {
    Taro.showToast({
      title: '登陆成功'
    });
    setTimeout(() => {
      Router.goBack();
    }, 2000);
    //}
  };

  function tobegin() {
    // this.setState({
    //   isOpened: true
    // });
    Taro.getUserInfo()
      .then((res) => {
        Taro.setStorage({
          key: 'userInfo',
          data: res.userInfo
        });

        Taro.navigateTo({
          url: '/pages/home/index'
        }).catch((error) => {
          Taro.showToast({
            title: '授权成功',
            icon: 'success'
          });

          /* 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面 */
          wx.switchTab({ url: '/pages/home/index' });
        });
      })
      .catch((err) => {});
  }

  const testPhone = (payload) => {
    const mobileReg = /^[1][3-9][0-9]{9}$/;
    if (mobileReg.test(payload)) {
      return true;
    }
    Taro.showToast({
      title: '请输入正确手机号'
    });
    return false;
  };
  const compute = (e) => {
    setActCode(e.detail.value);
    let temp = [...checkCode];
    temp.forEach((i, ii) => {
      temp[ii] = e.detail.value.charAt(ii);
    });
    setCheckCode(temp);
    if (e.detail.value.length === 6) {
      onLogin(e.detail.value);
    }
  };
  const sendCode = async () => {
    let res = await sendLoginCheckCode({ mobile: phone, deviceId: '1' });
    if (res.code === 0) {
      Taro.showToast({
        title: '验证码已发送 请注意查收'
      });
    }
  };
  useEffect(() => {
    let timer = 0;
    if (isSend && seconds != 0 && testPhone(phone)) {
      timer = setInterval(() => {
        // 这时候的num由于闭包的原因，一直是0，所以这里不能用setNum(num-1)
        setSeconds((n) => {
          if (n == 1) {
            setIsSend(false);
            clearInterval(timer);
          }
          return n - 1;
        });
      }, 1000);
    }
    return () => {
      // 组件销毁时，清除定时器
      clearInterval(timer);
    };
  }, [isSend, phone, seconds]);
  // 检查code是否可用
  const onMiniLogin = (user) => {
    console.log('用户信息');
    console.log(user);
    if (user.errMsg === 'getUserInfo:ok') {
      USER = user;
      let code = Taro.getStorageSync('code');
      if (code) {
        Taro.checkSession({
          success: function () {
            console.log('未过期');
            getMiniCode();
            //getMiniLoginCode(code);
          },
          fail: function () {
            console.log('已过期');
            getMiniCode();
          }
        });
      } else {
        console.log('未保存code');
        getMiniCode();
      }
    }
  };
  // 获取code
  const getMiniCode = async () => {
    Taro.login({
      success: async (res) => {
        console.log('获取code成功');
        if (res.code) {
          getMiniLoginCode(res.code);

          Taro.setStorage({
            key: 'code',
            data: res.code
          });
        } else {
          console.log('登录失败！' + res.errMsg);
        }
      }
    });
  };
  // 通过code获取用户信息
  const getMiniLoginCode = async (code) => {
    Taro.showLoading({
      title: ''
    });
    let data = await getLoginOther({
      clientType: 1,
      thirdType: 5,
      keepAccess: 2,
      code: code
    });
    Taro.hideLoading();
    if (data.code === 0) {
      code1 = data.data.grantCode;
      uid = data.data.uid;
      if (data && data && data.data.openId) {
        Taro.setStorage({
          key: 'openId',
          data: data.data.openId
        });
      } else {
        Taro.showToast({
          title: '获取用户信息失败',
          icon: 'none'
        });
        return;
      }

      if (data.data.isNeedMobile === 1) {
        // 需要手机号
        //onLoignMiniUser(user, 'user', data.data.grantCode);
        Taro.showToast({
          title: '需要绑定手机',
          icon: 'none'
        });
        setNeedPhone(true);
      } else {
        onLoignMiniUser(USER, 'user', data.data.grantCode);
      }
    } else {
      Taro.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
  };
  // 获取用户openID
  const onGetId = async (obj) => {
    Taro.showLoading({
      title: ''
    });
    let data = await getPhone({
      encryptedData: obj.encryptedData,
      iv: obj.iv,
      appId: 'wx2e4dfbd030e791a6',
      uid: uid
    });
    Taro.hideLoading();
    if (data.code === 0) {
      let msg = JSON.parse(data.data.decryptedData);
      let openId = msg.openId;

      Taro.setStorage({
        key: 'openId',
        data: openId
      });

      if (process.env.TARO_ENV === 'weapp') {
        Router.goBack();
      }
    } else {
      Taro.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
  };

  //登陆
  const onLoignMiniUser = async (user, type, code) => {
    let data: any;
    if (type === 'user') {
      Taro.showLoading({
        title: ''
      });
      data = await getLoginNone({
        grantCode: code,
        clientType: 1,
        keepAccess: 2,
        passwordType: 0,
        nickName: user.userInfo.nickName,
        avatarUrl: user.userInfo.avatarUrl,
        uid: uid
      });
      Taro.hideLoading();
    } else {
      Taro.showLoading({
        title: ''
      });
      data = await getLoginNone({
        uid: uid,
        grantCode: code,
        clientType: 1,
        keepAccess: 2,
        passwordType: 0,
        nickName: USER.userInfo.nickName,
        avatarUrl: USER.userInfo.avatarUrl,
        mobile: user
      });
      Taro.hideLoading();
    }
    if (data.code === 0) {
      onSus(data.data);
      //onGetId(USER, data);
    } else {
      Taro.showToast({
        title: data.msg,
        icon: 'none'
      });
    }
  };
  // 绑定手机
  const onGetPhone = async (obj) => {
    console.log('获取手机号');
    console.log(obj);
    if (obj.errMsg.indexOf('fail user deny') === -1) {
      Taro.showLoading({
        title: ''
      });
      let data = await getPhone({
        encryptedData: obj.encryptedData,
        iv: obj.iv,
        appId: 'wx2e4dfbd030e791a6',
        uid: uid,
        nickName: USER.userInfo.nickName,
        avatarUrl: USER.userInfo.avatarUrl
      });
      Taro.hideLoading();
      if (data.code === 0) {
        let msg = JSON.parse(data.data.decryptedData);
        onLoignMiniUser(msg.phoneNumber, 'phone', code1);
      } else {
        Taro.showToast({
          title: data.msg,
          icon: 'none'
        });
      }
    }
  };
  const getWeChatUrl = () => {
    const param = {
      pageUrl: Taro.getStorageSync('backRoute').weChatUrl + '&clientType=1',
      clientType: 1,
      crm_channel_id: Taro.getStorageSync('channel_id') || null
    };
    getOauthUrl(param).then((res) => {
      if (res.code === 0) {
        if (process.env.TARO_ENV === 'h5') {
          window.location.href = res.data.url;
        }
      } else {
        Taro.showToast({
          title: res.msg || '系统错误',
          icon: 'none'
        });
      }
    });
  };

  const useragreement = () => {
    Taro.navigateTo({
      url: 'https://www.myutopa.com/m/utopa/#/signIn/useragreement'
    });
  };
  const pricacy = () => {
    Taro.navigateTo({
      url: 'https://www.myutopa.com/m/utopa/#/signIn/pricacy'
    });
  };

  let isMini = false;

  if (process.env.TARO_ENV === 'weapp') {
    isMini = true;
  }
  //isMini = false;
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          {isMini && (
            <View className="utp-miniLogin">
              <View className="utp-miniLogin-logo">
                <Img src={personal_pic_portrait} width={200} />
              </View>
              {!needPhone && (
                <Button
                  className="utp-miniLogin-btn"
                  openType="getUserInfo"
                  onGetUserInfo={(e) => {
                    // console.log('获取用户信息');
                    // console.log(e.detail);
                    // console.log('用户信息');
                    onMiniLogin(e.detail);
                  }}
                >
                  微信登陆
                </Button>
              )}
              {needPhone && (
                <Button
                  className="utp-miniLogin-btn"
                  openType="getPhoneNumber"
                  onGetPhoneNumber={(e) => {
                    // console.log('获取电话号码');
                    // console.log(e.detail);
                    onGetPhone(e.detail);
                  }}
                >
                  绑定手机号
                </Button>
              )}
              <View className="utp-miniLogin-tips">请完成微信授权以继续使用</View>
              <View className="utp-miniLogin-ft">
                <View className="utp-login-prot">
                  <Wrap justifyContent="center">
                    <View
                      className="utp-login-prot-img"
                      onClick={() => {
                        setAgreen(!agreen);
                      }}
                    >
                      <Img src={agreen ? check : uncheck} width={24} />
                    </View>
                    <Txt title="登录注册代表同意" size={22} color="low" />
                    <Txt title=" 用户协议 " size={22} color="blue" />
                    <Txt title="及" size={22} color="low" />
                    <Txt title=" 隐私政策 " size={22} color="blue" />
                  </Wrap>
                </View>
              </View>
            </View>
          )}
          {/* && !isWchat */}
          {!isMini && !isWchat && (
            <View className="utp-login">
              <View className="utp-login-title ">
                <Txt
                  title={status ? '账号密码登录' : code ? '输入验证码' : '欢迎登录优托邦'}
                  size={50}
                  color="deep"
                  bold
                  justifyContent="flex-start"
                />
              </View>

              {code && (
                <View className="utp-login-title ">
                  <Txt
                    title={`验证码发送至${phone}`}
                    size={28}
                    color="morelow"
                    bold
                    justifyContent="flex-start"
                  />
                </View>
              )}
              {!code && (
                <View className="utp-login-input">
                  <Input
                    placeholder="输入11位手机号码"
                    value={phone}
                    onInput={(e) => {
                      setPhone(e.detail.value);
                    }}
                    className="utp-login-num"
                  />
                </View>
              )}
              {status && (
                <View className="utp-login-input">
                  <Input
                    placeholder="输入密码"
                    onInput={(e) => {
                      setPassword(e.detail.value);
                    }}
                    value={password}
                    type={!type ? 'password' : 'text'}
                    className="utp-login-password utp-login-num"
                  />
                  <Wrap>
                    <View
                      onClick={() => {
                        setType(!type);
                      }}
                    >
                      <Img src={type ? eye : eye1} width={48} />
                    </View>
                    {/* <Txt title="忘记密码" color="low" size={28} /> */}
                  </Wrap>
                </View>
              )}

              {code && (
                <View className="utp-login-btmBox">
                  <Wrap justifyContent="space-between">
                    <Input
                      className="utp-login-bigInput"
                      value={actCode}
                      maxLength={6}
                      onInput={compute}
                    />
                    {checkCode.map((i, ii) => {
                      return (
                        <View className="utp-login-codes" key={ii}>
                          <Input
                            className="utp-login-codes-item"
                            disabled
                            value={i}
                            maxLength={1}
                            onInput={(e) => {
                              let a = [...checkCode];
                              a[ii] = e.detail.value;
                              setCheckCode(a);
                            }}
                          />
                        </View>
                      );
                    })}
                  </Wrap>
                </View>
              )}
              {code && seconds > 0 && (
                <View className="utp-login-btmBox">
                  <Txt
                    title={`${seconds}后重新获取验证码`}
                    size={28}
                    color="morelow"
                    bold
                    justifyContent="flex-start"
                  />
                </View>
              )}
              {code && seconds == 0 && (
                <View
                  className="utp-login-btmBox"
                  onClick={() => {
                    setSeconds(60);
                    setIsSend(true);
                    sendCode();
                  }}
                >
                  <Txt
                    title="重新获取验证码"
                    size={28}
                    color="active"
                    bold
                    justifyContent="flex-start"
                  />
                </View>
              )}

              <View className="utp-login-btmBox">
                <Wrap justifyContent="space-between">
                  {!code && (
                    <View
                      onClick={() => {
                        setStatus(!status);
                        setIsSend(false);
                      }}
                    >
                      <Txt
                        title={status ? '验证码登录' : '账户密码登录'}
                        color="active"
                        size={28}
                      />
                    </View>
                  )}
                  <View
                    className="utp-login-btn"
                    onClick={() => {
                      if (status && agreen) {
                        onLogin();
                      } else {
                        console.log(agreen, 'agreen');
                        if (!agreen) {
                          // Taro.showToast({ title: '请先同意协议', icon: 'none' });
                          return false;
                        } else if (testPhone(phone)) {
                          setSeconds(60);
                          setIsSend(true);
                          setCode(true);
                          sendCode();
                        }
                      }
                    }}
                    style={{
                      display: code ? 'none' : 'block',
                      background: !agreen
                        ? 'rgba(221,221,221,1)'
                        : 'linear-gradient(90deg, rgba(255, 47, 61, 1) 0%, rgba(255, 0, 232, 1) 100%)'
                    }}
                  >
                    {status && !code && (
                      <Txt color="white" size={32} title="登录" bold height={100} />
                    )}
                    {!status && isSend && !code && (
                      <Txt
                        color="white"
                        size={32}
                        title={`获取验证码(${seconds}s)`}
                        bold
                        height={100}
                      />
                    )}
                    {!status && !isSend && !code && (
                      <Txt color="white" size={32} title="获取验证码" bold height={100} />
                    )}
                  </View>
                </Wrap>
              </View>

              {!code && (
                <View className="utp-login-prot">
                  <Wrap justifyContent="center">
                    <View
                      className="utp-login-prot-img"
                      onClick={() => {
                        setAgreen(!agreen);
                      }}
                    >
                      <Img src={agreen ? check : uncheck} width={24} />
                    </View>
                    <Txt title="登录注册代表同意" size={22} color="low" />
                    <Txt
                      title=" 用户协议 "
                      size={22}
                      color="blue"
                      onClick={() => useragreement()}
                    />
                    <Txt title="及" size={22} color="low" />
                    <Txt title=" 隐私政策 " size={22} color="blue" onClick={() => pricacy()} />
                  </Wrap>
                </View>
              )}
              {props.isWeiXin && (
                <View className="utp-login-weChatLogo">
                  <View className="utp-login-weChatLogo-text">
                    <View className="utp-login-weChatLogo-text-line"></View>
                    <View className="utp-login-weChatLogo-text-content">其他登录方式</View>
                    <View className="utp-login-weChatLogo-text-line"></View>
                  </View>
                  <View
                    className="utp-login-weChatLogo-logo"
                    onClick={() => {
                      setIsWchat(true);
                    }}
                  >
                    <Image src={weChatIcon} className="utp-login-weChatLogo-logo-img" />
                  </View>
                </View>
              )}
            </View>
          )}
          {!isMini && isWchat && (
            <View className="utp-wechat">
              <View className="utp-wechat-logo">
                <Image src={wechatLogo} className="utp-wechat-logo-img" />
                <View className="utp-wechat-logo-button" onClick={() => getWeChatUrl()}>
                  微信账号登陆
                </View>
              </View>
              <View
                className="utp-wechat-check"
                onClick={() => {
                  setIsWchat(false);
                }}
              >
                已有账号登陆
              </View>
            </View>
          )}
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};

Index.config = {
  navigationBarTitleText: '登陆',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};

// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    isLogin: state.cart.isLogin,
    isWeiXin: state.user.isWeiXinFalg,
    weChatParam: state.example.weChatUrl
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    saveLogin: (payload) => {
      dispatch({
        type: 'cart/saveLogin',
        payload
      });
    },
    getUnread: () => {
      dispatch({
        type: 'cart/getUnread',
        payload: {}
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
