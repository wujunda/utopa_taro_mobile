import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import './index.scss';
import Img from '../Img';
import Txt from '../Txt';
import Wrap from '../Wrap';
import store from '../../assets/msg/store.png';
import top from '../../assets/msg/top.png';
import dell from '../../assets/msg/dell.png';

interface IProps {}
const Index: Taro.FC<IProps> = ({}) => {
  return (
    <View className="utp-msg-center utp-solid">
      <View className="utp-msg-center-read"></View>
      <View className="utp-msg-center-top">
        <Img src={top} width={24} />
      </View>
      <View className="utp-msg-center-imgs">
        <Img
          src="https://dss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3256100974,305075936&fm=26&gp=0.jpg"
          width={100}
        />
      </View>
      <View className="utp-msg-center-info">
        <View className="utp-msg-center-info-item">
          <Wrap type={1}>
            <Wrap>
              <View className="utp-msg-center-hid">
                <Txt title="标题标题标题标题标题" dian size={30} color="deep" />
              </View>
              <View className="utp-msg-center-info-item-icos">
                <Img src={store} width={30} />
              </View>
              <Txt title="欧尚超市" color="active" size={28} />
            </Wrap>
            <Txt title="2012-12-12" color="low" />
          </Wrap>
        </View>
        <View className="utp-msg-center-info-item">
          <Wrap type={1}>
            <View className="utp-msg-center-hid">
              <Txt title="新品特惠新品特惠新品特惠新品特惠新品特惠" size={22} dian color="low" />
            </View>
            <Wrap>
              <Img src={dell} width={26} />
              <View
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <View
                  className="utp-div utp-solid"
                  style={{
                    height: Taro.pxTransform(32),
                    minWidth: Taro.pxTransform(32),
                    borderWidth: Taro.pxTransform(2),
                    borderColor: '#ff2f7b',
                    backgroundColor: '#ff2f7b',
                    borderRadius: Taro.pxTransform(32),
                    paddingLeft: Taro.pxTransform(5),
                    paddingRight: Taro.pxTransform(5),
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <Txt size={23} color="white" title="8"></Txt>
                </View>
              </View>
            </Wrap>
          </Wrap>
        </View>
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
