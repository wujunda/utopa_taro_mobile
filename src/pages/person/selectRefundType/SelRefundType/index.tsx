import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import Wrap from '../../../../components/Wrap';
import Img from '../../../../components/Img';
import goRight from '../../../../assets/mine/personal_icon_forward.png';
import './index.scss';

interface IProps {
  goList: any;
  orderId: any;
}

const isBorderStyle = {
  borderBottom: '1px solid #f5f5f5'
};
const Index: Taro.FC<IProps> = ({ goList, orderId }) => {
  const goPath = (item: any) => {
    if (item.path) {
      Taro.navigateTo({
        url: item.path + '&orderId=' + orderId
      });
    }
  };
  console.log('goList');
  console.log(goList);
  return (
    <View>
      <View className="refund-box-type">
        {!!goList &&
          goList.map((item, index) => {
            if (!item.isHide) {
              return (
                <View
                  className="refund-box-type-item"
                  style={(index === goList.length - 1 ? null : isBorderStyle) as any}
                  key={index}
                  onClick={() => goPath(item)}
                >
                  <Wrap type={2} flexDirection="row">
                    <View className="refund-box-type-item-icon">
                      <Img src={item.icon} width={Taro.pxTransform(56)}></Img>
                    </View>
                    <View className="refund-box-type-item-center">
                      <View className="refund-box-type-item-center-title">{item.title}</View>
                      <View className="refund-box-type-item-center-text">{item.text}</View>
                    </View>
                    <View className="refund-box-type-item-right">
                      <Img src={goRight} width={48} height={48}></Img>
                    </View>
                  </Wrap>
                </View>
              );
            }
          })}
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
