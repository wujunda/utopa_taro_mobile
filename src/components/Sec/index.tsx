import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';

import './index.scss';
import Img from '../Img';
import Wrap from '../Wrap';
import Txt from '../Txt';
import sec from '../../assets/info/sec.jpg';
import useEndTime from '../../hooks/useEndTime';
import Yuan from '../Yuan';
import { Glo } from '../../utils/utils';

interface IProps {
  price?: number;
  second?: number;
  type?: number;
  orPrice?: number;
  lable?: string;
  point?: number;
}

const Index: Taro.FC<IProps> = ({ point, second = 280000, type = 1, price, orPrice, lable }) => {
  const [s] = useEndTime(second);
  function getDay(s: number) {
    let tmp: any = Math.floor(s / (60 * 60 * 24));
    return tmp;
  }

  function getHour(s: number) {
    let tmp: any = Math.floor((s % (60 * 60 * 24)) / (60 * 60));
    return getZero(tmp);
  }

  function getMinutes(s: number) {
    let tmp = (Number(Math.floor(s / 60)) % 60).toString();
    return getZero(tmp);
  }

  function getSecond(s: number) {
    let tmp = (s % 60).toString();
    return getZero(tmp);
  }

  function getZero(i: number | string) {
    if (Number(i) < 10) {
      return '0' + i;
    } else {
      return i;
    }
  }
  // type= 0  折扣 秒杀  type=1  拼团  type= 2  积分
  return (
    <View className="utp-sec">
      <View className="utp-sec-bg">
        <Img src={sec} width={750} height={100} />
      </View>
      <View className="utp-sec-re" style={{ display: type === 0 ? 'block' : 'none' }}>
        <Wrap>
          <View className="utp-sec-tips utp-div">
            <Text className="utp-sec-tips-txt">{lable}</Text>
          </View>
          <View>
            <View className="utp-sec-prices">
              <View className="utp-sec-price">
                <Yuan price={price} size={30} color="#fff"></Yuan>
              </View>
            </View>
            <View className="utp-sec-dell">
              <Yuan price={orPrice} size={22} color="#fff"></Yuan>
            </View>
          </View>
        </Wrap>
      </View>
      <View className="utp-sec-re" style={{ display: type === 2 ? 'block' : 'none' }}>
        <Wrap>
          <Txt title={point} size={46} color="white" bold={500} />
          <View style={{ width: 4 }}></View>
          {!!orPrice && orPrice !== 0 && (
            <Txt title={'积分+¥' + Glo.formatPrice(orPrice)} size={32} color="white" bold={500} />
          )}
          {!orPrice && orPrice === 0 && <Txt title="积分" size={32} color="white" bold={500} />}
        </Wrap>
        <Wrap>
          {price && (
            <Txt title={'市场价：¥' + Glo.formatPrice(price)} size={24} color="white" bold />
          )}
        </Wrap>
      </View>
      <View className="utp-sec-re" style={{ display: type === 0 || type === 2 ? 'block' : 'none' }}>
        <Wrap>
          <Txt title={type === 0 ? '距离活动结束' : '距结束'} size={22} color="white" />
          <View className="utp-sec-box">
            <Text className="utp-sec-box-txt">{getDay(s)}</Text>
          </View>
          <Txt title="天" color="white" size={22} />
          <View className="utp-sec-box">
            <Text className="utp-sec-box-txt">{getHour(s)}</Text>
          </View>
          <Txt title=":" color="white" size={22} />
          <View className="utp-sec-box">
            <Text className="utp-sec-box-txt">{getMinutes(s)}</Text>
          </View>
          <Txt title=":" color="white" size={22} />
          <View className="utp-sec-box">
            <Text className="utp-sec-box-txt">{getSecond(s)}</Text>
          </View>
        </Wrap>
      </View>

      <View
        className="utp-sec-re utp-sec-assemblebox"
        style={{ display: type === 1 ? 'flex' : 'none' }}
      >
        <Wrap type={2}>
          <Txt title={lable} size={38} bold color="white" />
          <Text className="utp-sec-rules">规则</Text>
        </Wrap>
        <Wrap>
          <Txt title="距结束" size={22} color="white" />
          <View className="utp-sec-box">
            <Text className="utp-sec-box-txt">{getDay(s)}</Text>
          </View>
          <Txt title="天" color="white" size={22} />
          <View className="utp-sec-box">
            <Text className="utp-sec-box-txt">{getHour(s)}</Text>
          </View>
          <Txt title=":" color="white" size={22} />
          <View className="utp-sec-box">
            <Text className="utp-sec-box-txt">{getMinutes(s)}</Text>
          </View>
          <Txt title=":" color="white" size={22} />
          <View className="utp-sec-box">
            <Text className="utp-sec-box-txt">{getSecond(s)}</Text>
          </View>
        </Wrap>
      </View>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
