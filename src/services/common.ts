import request from '../utils/request';

// 积分商品列表
// eslint-disable-next-line import/prefer-default-export
export const getWxConfig = (url) => {
  return request.post<any>('/app.do', {
    method: 'base.wechat.getWxConfig',
    ver: '1.0',
    params: JSON.stringify({
      url: url
    })
  });
};

// 获取物流列表
export const getExpressCompanys = () => {
  return request.get(
    '/service/app/user/expresscompany/getExpressCompanys',
    {},
    request.API_TYPE.BOSS_ESHOP
  );
};

// 保存物流信息
export const saveExpressCompanys = (data) => {
  return request.post(
    '/service/app/user/refundOrder/addRefundOrderPostMsg',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};
// 物流信息查询
export const getRefundOrderExpressRoute = (data) => {
  return request.post(
    '/service/app/user/refundOrder/getRefundOrderExpressRoute',
    data,
    request.API_TYPE.BOSS_ESHOP
  );
};
