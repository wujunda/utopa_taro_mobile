import Taro from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { connect } from '@tarojs/redux';
import { View, Text } from '@tarojs/components';
import useStore from '../../components/useNum';

import './index.scss';

// #region 书写注意
//
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

function Index(props) {
  let num = useStore({ name: '' })[0];
  console.log('jjjjj');
  console.log(num);
  //useDidShow(() => {
  //console.log("componentDidShow");
  //});
  //useDidHide(() => {
  //console.log("componentDidHide");
  //});
  //usePullDownRefresh(() => {
  //console.log("onPullDownRefresh");
  //});
  //useReachBottom(() => {
  //console.log("onReachBottom");
  //});
  //usePageScroll(res => {
  //console.log(res.scrollTop);
  //});
  //const router = useRouter();
  //console.log(router);
  console.log('props');
  console.log(props);
  //const [state, update, loading] = useRequest<IUse>('name=a&age=1', getItem);

  return (
    <View
      onClick={() => {
        Taro.navigateTo({
          url: '/pages/page3/index'
        });
      }}
    >
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
      <View>
        <Text>hhhhhhhhhhhhhhhhk</Text>
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '标题',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#ccc'
};
// @ts-ignore
export default connect((state) => state)(Index);
