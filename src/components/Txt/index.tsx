import Taro from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import './index.scss';

interface Props {
  color?:
    | 'orange'
    | 'morelow'
    | 'low'
    | 'normal'
    | 'deep'
    | 'active'
    | 'white'
    | 'black'
    | 'red'
    | 'blue'
    | '#816958';
  size?: number;
  height?: number;
  bold?: any;
  title?: number | string | null;
  dian?: boolean;
  time?: boolean;
  backgroundColor?: 'none' | 'white' | '#D0D0D0';
  borderRadius?: number;
  paddingLeftRight?: number;
  justifyContent?: 'center' | 'flex-start' | 'flex-end';
  className?: any;
  onClick?: () => void;
}
const Index: Taro.FC<Props> = ({
  color = 'normal',
  size = 24,
  bold = false,
  height,
  title,
  dian = false,
  justifyContent = 'center',
  className,
  onClick
}) => {
  let sColor = '#666';
  switch (color) {
    case 'morelow':
      sColor = '#ccc';
      break;
    case 'low':
      sColor = '#999';
      break;
    case 'normal':
      sColor = '#666';
      break;
    case 'deep':
      sColor = '#333';
      break;
    case 'active':
      sColor = '#ff3881';
      break;
    case 'white':
      sColor = 'white';
      break;
    case 'black':
      sColor = '#000000';
      break;
    case 'red':
      sColor = '#ff317c';
      break;
    case 'blue':
      sColor = '#25A6FA';
    case 'orange':
      sColor = '#ff4a2a';
    case '#816958':
      sColor = '#816958';
      break;
  }
  let iHeight = size + 4;
  if (height !== undefined) {
    iHeight = height;
  }
  let classText = '';
  if (dian) {
    classText = 'utp-txt-oneline';
  }

  // console.log("我看看这个------", justifyContent);
  return (
    <View
      className={className}
      style={{
        display: 'flex',
        justifyContent: justifyContent,
        alignItems: 'center'
      }}
      onClick={() => {
        if (onClick) {
          onClick();
        }
      }}
    >
      <Text
        style={{
          color: sColor,
          fontWeight: bold === 500 ? 500 : bold ? 'bold' : 'normal',
          fontSize: Taro.pxTransform(size),
          lineHeight: Taro.pxTransform(iHeight)
        }}
        className={classText}
      >
        {title}
      </Text>
    </View>
  );
};
Index.defaultProps = {};

export default Index;
