import Taro, { useShareAppMessage, useDidShow, useState, useRouter, useEffect } from '@tarojs/taro'; //useRouter //usePageScroll, //useReachBottom, //usePullDownRefresh, //useDidHide, //useDidShow,
import { ScrollView, View, Swiper, SwiperItem, Text, Image } from '@tarojs/components';
//import useStore from '../../components/useNum';
import { connect } from '@tarojs/redux';
import Box1 from '../../components/Box1';
import Wrap from '../../components/Wrap';
import Tag from '../../components/Tag';
import Txt from '../../components/Txt';
import Loading from '../../components/Loading';
import Img from '../../components/Img';
import Goods3 from '../../components/Goods3';
import Score from '../../components/Score';
import Cnt from '../../components/Cnt';
import Sec from '../../components/Sec';
import AddCart from '../../components/AddCart';
import right from '../../assets/right.png';
import like from '../../assets/details/like.png';
import unlike from '../../assets/details/unlike.png';
import person from '../../assets/details/person.png';
import bixin from '../../assets/details/bixin.png';
//import Lable from '../../components/AssembleGood/Lable';
import { images } from '../../images';
import Sku from '../../components/Sku';
import Model from '../../components/Model';
import Card from '../../components/Card';
import useRequest from '../../hooks/useRequest';
import useSession from '../../hooks/useSession';
import Goods2s from '../../components/Goods2s';
// eslint-disable-next-line no-unused-vars
import { IGoods } from '../../interface/home';
import Router from '../../utils/router';
import { Glo, inAppIsLogin } from '../../utils/utils';
import {
  // eslint-disable-next-line no-unused-vars
  IDetails,
  getGoodsDetails,
  getStoreRecommend,
  getGoodsSku,
  discountCardDetailPage,
  subscribe,
  cancleSubscribe,
  getQuan
} from '../../services/details';
import seckill from '../../assets/details/seckill.png';
import './index.scss';
import { getAddLike, getUnLike } from '../../services/likes';
import { getLogin } from '../../services/login';
import Youhui from './youhui';

function Index(props) {
  useDidShow(() => {
    if (router.params.jifen === '1') {
      Taro.setNavigationBarTitle({
        title: '积分兑换商品'
      });
    } else {
      Taro.setNavigationBarTitle({
        title: '商品详情'
      });
    }
  });
  //let num = useStore({ name: '' })[0];
  const [demo, setDemo] = useState(0);
  const [show, setShow] = useState(false);
  const [open, setOpen] = useState(false);
  const [skuId, setSkuId] = useState('');
  const [showJifen, setShowJifen] = useState(false);
  //const [list, setList] = useState([]);
  const router = useRouter();
  let isJifen = false;

  if (router.params.jifen === '1') {
    isJifen = true;
  }
  const [skuObj, setSkuObj] = useState<any>('');
  const onChangeSku = (obj) => {
    setSkuObj(obj);
  };
  const productId = router.params.id || router.params.productId;
  // let params = {
  //   productId,
  //   storeId: router.params.storeId,
  //   businessId: router.params.businessId,
  //   type: router.params.type
  // };

  let payload = {
    productId,
    storeId: router.params.storeId,
    type: isJifen ? 2 : 0
    //channelType: 2
  };

  if (process.env.TARO_ENV === 'weapp') {
    useShareAppMessage((res) => {
      if (res.from === 'button') {
        // 来自页面内转发按钮
        console.log(res.target);
      }
      return {
        title: state ? state.data.name : '优托邦GO',
        path: `/pages/details/index?productId=${productId}&storeId=${
          router.params.storeId
        }&isJifen=${isJifen ? '1' : ''}`
      };
    });
  }
  const [haveSession] = useSession();
  // 商品详情
  const [state, update, loading] = useRequest<IDetails>(payload, getGoodsDetails);
  if (haveSession && state === null && update && !loading) {
    update();
  }
  let isXuni = false;
  if (state && state.code === 0 && state.data.productType === 2) {
    isXuni = true;
  }
  const arr = () => {
    let a =
      state != null &&
      state.data != null &&
      state.data.recommendProducts != null &&
      state.data.recommendProducts.length > 0 &&
      Math.ceil(state.data.recommendProducts.length / 6);
    let list1: Array<any> = [];
    for (let i = 0; i < a; i++) {
      let b: Array<IGoods> = [];
      if (state && state.data.recommendProducts.length > 0) {
        b = state.data.recommendProducts.slice(i * 6, 6 * (i + 1)) as Array<IGoods>;
      }
      list1.push(b);
    }
    return list1;
  };
  // 商品sku
  const [skuState, update2, loading2] = useRequest<IDetails>(payload, getGoodsSku);
  if (haveSession && skuState === null && update2 && !loading2) {
    update2();
  }
  const doSubscribe = async () => {
    if (!Taro.getStorageSync('isLogin')) {
      Taro.showToast({ title: '请先登入', icon: 'none' });
    } else {
      // Taro.showLoading({ title: '' });
      let payload = {
        // 秒杀活动分组
        activityGroupId: skuObj.seckillAdvance && skuObj.seckillAdvance.activityGroupId,
        //活动分组开始时间
        activityGroupStartTime: skuObj.seckillAdvance && skuObj.seckillAdvance.startTimestamp,
        activityId: skuObj.seckillAdvance && skuObj.seckillAdvance.activityId,
        businessId: skuObj.businessId,
        productId: skuObj.productId,
        skuId: skuObj.id,
        storeId: router.params.storeId
      };
      if (
        skuObj.seckillAdvance &&
        (skuObj.seckillAdvance.subscribe === 0 || skuObj.seckillAdvance.subscribe == null)
      ) {
        //订阅
        let res = await subscribe(payload);
        if (res.code === 0) {
          Taro.showToast({ title: '成功订阅', icon: 'none' });
          update2 && update2();
          setSkuId(skuObj.id);
        } else {
          Taro.showToast({ title: res.msg, icon: 'none' });
        }
      } else {
        //取消订阅
        let rep = await cancleSubscribe(payload);
        if (rep.code === 0) {
          Taro.showToast({ title: '成功取消订阅', icon: 'none' });
          setSkuId(skuObj.id);
          update2 && update2();
        } else {
          Taro.showToast({ title: rep.msg, icon: 'none' });
        }
      }
    }
  };
  // 处理订阅取消订阅渲染
  useEffect(() => {
    if (skuState && skuState.code === 0 && skuState.data && skuState.data != null && skuId) {
      if (skuState.data.productSkus) {
        let index = skuState.data.productSkus.findIndex((i) => i.id === skuId);
        if (index > -1) {
          onChangeSku(skuState.data.productSkus[index]);
          setSkuId('');
        }
      }
    }
  }, [skuState, skuId]);

  const [isCan, setIsCan] = useState(false);
  useEffect(() => {
    if (isCan && update3) {
      update3();
    }
  }, [isCan]);
  //优惠券
  const [coupinsState, update3, loading3] = useRequest<IDetails>(
    {
      businessId: state ? state.data.businessId : '',
      productId: payload.productId,
      skuId: skuObj.id,
      storeId: payload.storeId
    },
    discountCardDetailPage,
    {
      isFetch: isCan
    }
  );
  if (haveSession && coupinsState === null && update3 && !loading3) {
    update3();
  }

  // 店铺推荐商品
  const [recommendState, setRecommendState] = useState([]);
  const getRecomendList = async (businessId) => {
    let res = await getStoreRecommend({ ...payload, businessId });
    if (res.code === 0) {
      setRecommendState(res.data.items);
    }
  };
  useEffect(() => {
    if (state && state.data && state.data.businessId) {
      getRecomendList(state.data.businessId);
    }
  }, [state]);

  // 收藏
  const toggleLike = async () => {
    if (!Taro.getStorageSync('login')) {
      let res = await getLogin('');
      if (res.code === 0) {
        //含登入状态 可以收藏
        Taro.setStorage({
          key: 'login',
          data: res.data.accessToken
        });
      }
    }
    if (state && !state.data.isFavorite) {
      Taro.showLoading({ title: '' });
      let rst = await getAddLike('', {
        favoriteId: state.data.id,
        storeId: state.data.storeId,
        type: 0
      });
      Taro.hideLoading();
      if (rst.code === 0) {
        Taro.showToast({
          title: '商品收藏成功~',
          icon: 'none'
        });
        update && update();
      }
    } else {
      if (state && state.data) {
        Taro.showLoading({ title: '' });
        let rst = await getUnLike('', {
          favoriteId: state.data.id,
          storeId: state.data.storeId,
          type: 0
        });
        Taro.hideLoading();
        if (rst.code === 0) {
          Taro.showToast({
            title: '取消收藏成功~',
            icon: 'none'
          });
          update && update();
        }
      }
    }
  };
  // 提交积分订单
  const onJifen = async (num?: number, sku?: any) => {
    console.log('skuUUu');
    console.log(skuObj);
    // console.log(props.inApp, 'props.inApp');
    if (props.inApp && !props.isLogin) {
      inAppIsLogin().then((r) => {
        if (r) {
          onJifen(num, sku);
        }
      });
      return;
    }
    if (!props.isLogin) {
      Router.goLogin();
      return;
    }
    if (!state || !skuState) {
      return;
    }
    let goods: any = [];
    let obj: any = {};
    obj.businessId = state.data.businessId;
    obj.isUseOriginPrice = 0;
    obj.sysId = process.env.TARO_ENV === 'weapp' ? 1004 : props.isWeiXinFalg ? 1002 : 1001;
    obj.orderSource = 2;
    if (props.inApp) {
      obj.sysId = 1;
      obj.orderSource = 1;
    }
    obj.storeId = state.data.storeId;
    if (isJifen) {
      obj.orderType = '6';
    }
    obj.isSelfPickup = 0;
    let goodsArr: any = [];
    let obj1: any = {};
    obj1.skuId = sku ? sku.id : skuObj.id;
    obj1.skuNum = num || 1;
    goodsArr.push(obj1);
    if (goodsArr.length > 0) {
      obj.products = goodsArr;
      goods.push(obj);
    }
    let post: any;
    if (goods.length > 0) {
      post = {
        binComits: goods,
        clientType: 1,
        sysId: 1
      };
      Taro.showLoading({
        title: ''
      });
      props.getSubmit({ post, type: 1 });
      props.savePost({
        data: post
      });
    } else {
      // 没有选中商品
    }
  };
  const [crtIndex, setCrtIndex] = useState(0);
  let skuArr = [];
  if (skuObj && skuObj.activityRule && isJifen) {
    let str = skuObj.activityRule.replace(/\n/g, '%%%%');
    skuArr = str.split('%%%%');
  }

  return (
    <View className="utp-details" style={{ backgroundColor: '#f5f5f5' }}>
      {loading && <Loading />}
      {!!state && state.code === 0 && (
        <View>
          <View className="utp-details-bd">
            {
              // 轮播start
            }
            <View
              className="utp-details-swipe"
              onClick={() => {
                //Router.goBack();
              }}
            >
              <Swiper
                className="utp-details-swipe"
                indicatorColor="#999"
                indicatorActiveColor="#333"
                circular
                autoplay
                onChange={(e) => {
                  let index = e.detail.current;
                  setCrtIndex(index);
                }}
              >
                {state != null &&
                  state.data != null &&
                  state.data.picUrls != null &&
                  state.data.picUrls.length > 0 &&
                  state.data.picUrls.map((item, index) => {
                    if (!item) {
                      return;
                    }
                    return (
                      <SwiperItem key={index}>
                        <View className="utp-details-swipe-item utp-cnt utp-details-swipe-item-1">
                          <Image style={{ width: '100%', height: '100%' }} src={item} />
                        </View>
                      </SwiperItem>
                    );
                  })}
              </Swiper>

              {state != null &&
                state.data != null &&
                state.data.picUrls != null &&
                state.data.picUrls.length > 0 && (
                  <View className="utp-details-swipe-nums">
                    <Cnt>
                      <Txt title={`${crtIndex + 1}/${state.data.picUrls.length}`} color="white" />
                    </Cnt>
                  </View>
                )}
            </View>
            {
              // 轮播end
            }
            {
              // 0  普通   1 限时折扣  2秒杀   3拼团
            }
            {skuObj && skuObj.skuType === 0 && (
              <View className="utp-details-price1">
                <Txt size={36} title="¥" color="active" />
                <Txt size={60} title={Glo.formatPrice(skuObj.price).split('.')[0]} color="active" />
                <Txt
                  size={42}
                  title={'.' + Glo.formatPrice(skuObj.price).split('.')[1]}
                  color="active"
                />
              </View>
            )}
            {skuObj && skuObj.endDate && skuObj.skuType === 1 && (
              <Sec
                type={0}
                lable={skuObj.lable}
                second={Number((skuObj.endDate / 1000).toFixed(0))}
                price={skuObj.price}
                orPrice={skuObj.actuallyPrice}
              />
            )}
            {skuObj && skuObj.endDate && skuObj.skuType === 2 && (
              <Sec
                type={0}
                lable={skuObj.lable}
                second={Number((skuObj.endDate / 1000).toFixed(0))}
                price={skuObj.price}
                orPrice={skuObj.actuallyPrice}
              />
            )}
            {skuObj && skuObj.endDate && skuObj.skuType === 3 && (
              <Sec
                type={1}
                lable={skuObj.lable}
                second={Number((skuObj.endDate / 1000).toFixed(0))}
                price={skuObj.price}
                orPrice={skuObj.actuallyPrice}
              />
            )}
            {skuObj && skuObj.endDate && skuObj.skuType === 4 && (
              <Sec
                type={2}
                lable={skuObj.lable}
                second={Number((skuObj.endDate / 1000).toFixed(0))}
                price={skuObj.actuallyPrice}
                orPrice={skuObj.exchangePrice}
                point={skuObj.exchangePoint}
              />
            )}

            {
              // 商品标题和心愿单start
            }
            {skuObj && skuObj.seckillAdvance && (
              <View className="utp-details-bd-seckillBox">
                <View className="utp-details-bd-seckill">
                  <Image src={seckill} className="utp-details-bd-seckillBox-img" />
                  <Txt title={skuObj.seckillAdvance.seckillStartDate} size={24} />
                  <View style={{ width: 5 }}></View>
                  <Txt
                    title={`秒杀价¥${
                      skuObj.seckillAdvance.seckillPrice && skuObj.seckillAdvance.seckillPrice / 100
                    }`}
                    size={24}
                    color="active"
                    bold
                  />
                </View>
                {/* 取消预约 */}
                <View
                  onClick={doSubscribe}
                  className={
                    !skuObj.seckillAdvance.subscribe
                      ? 'utp-details-bd-remind'
                      : 'utp-details-bd-remind utp-details-bd-dark'
                  }
                >
                  {!skuObj.seckillAdvance.subscribe ? '提醒我' : '取消提醒'}
                </View>
              </View>
            )}

            <View className="utp-details-bd-titles">
              {state.data.selfSupport && (
                <View
                  className={`utp-details-bd-titles-tags ${
                    isJifen ? 'utp-details-bd-titles-tags-1' : ''
                  }`}
                >
                  <Tag>自营</Tag>
                </View>
              )}
              <View className="utp-details-bd-titles-1">
                <Wrap>
                  <View className="utp-details-bd-titles-isSelf">
                    <Text className="utp-details-bd-titles-title">{state.data.name}</Text>
                  </View>
                  {!isJifen && (
                    <View
                      className="utp-details-bd-titles-likes utp-cnt"
                      onClick={() => {
                        // if (state && state.data.isFavorite) {
                        //   //取消收藏
                        toggleLike();
                        // } else {
                        //   toggleLike(true);
                        //   // 收藏
                        // }
                      }}
                    >
                      <View>
                        <View style={{ height: Taro.pxTransform(50) }}>
                          <Cnt>
                            <Img src={state && state.data.isFavorite ? like : unlike} width={38} />
                          </Cnt>
                        </View>
                        <Txt title={state && state.data.isFavorite ? '已收藏' : '收藏'} size={18} />
                      </View>
                    </View>
                  )}
                </Wrap>
              </View>
              {!isJifen && <View style={{ height: Taro.pxTransform(20) }}></View>}
              {!isJifen && (
                <Wrap>
                  <Wrap>
                    <Img src={person} width={24} />
                    <View style={{ width: Taro.pxTransform(12) }}></View>
                    <Txt
                      title={state.data.wantPersonNum ? state.data.wantPersonNum : 0 + '想要'}
                      size={24}
                      height={44}
                      color="low"
                    />
                    <View style={{ width: Taro.pxTransform(36) }}></View>
                    <Img src={bixin} width={24} />
                    <View style={{ width: Taro.pxTransform(12) }}></View>
                    <Txt title="添加到心愿单" size={24} height={44} color="low" />
                  </Wrap>
                </Wrap>
              )}
            </View>
            {
              // 商品标题和心愿单end
            }
            {
              // 商品活动start
            }
            {false &&
              !!skuObj &&
              !!skuObj.discountCardOutlines &&
              !!skuObj.discountCardOutlines.length && (
                <View className="utp-details-bd-acts">
                  <Wrap
                    click={() => {
                      setIsCan(true);
                      setOpen(true);
                    }}
                  >
                    <Txt title="优惠" color="deep" size={26} height={52} />
                    <View style={{ flex: 1 }}>
                      {skuObj.discountCardOutlines.map((i, ii) => {
                        if (!i) {
                          return;
                        }
                        return (
                          <Wrap key={ii}>
                            <Youhui item={i} />
                          </Wrap>
                        );
                      })}
                    </View>
                    <View style={{ height: Taro.pxTransform(52) }}>
                      <Cnt>
                        <Img src={right} width={44} />
                      </Cnt>
                    </View>
                  </Wrap>
                </View>
              )}
            {
              // 商品活动end
            }
            {/* 拼团start */}
            {/* <Lable /> */}
            {/* 拼团end */}
            {
              // 退货和运费start
            }
            <View className="utp-details-bd-tabs">
              <Box1 type={2}>
                {isJifen && (
                  <View
                    className="utp-details-bd-tab utp-solid"
                    onClick={() => {
                      setShowJifen(true);
                      //Router.goGuize();
                    }}
                  >
                    <Wrap type={1} justifyContent="space-between">
                      <Txt title="兑换规则" color="deep" size={26} />
                      <Wrap>
                        <Txt title="查看详情" color="deep" size={26} />
                        <Img src={images.right} width={32} />
                      </Wrap>
                    </Wrap>
                  </View>
                )}
                {!isJifen && (
                  <View className="utp-details-bd-tab utp-solid">
                    <Wrap type={1} justifyContent="space-between">
                      <Txt title="退货" color="deep" size={26} />
                      <Txt title={state.data.returnPolicyValue} color="deep" size={26} />
                    </Wrap>
                  </View>
                )}
                <View className="utp-details-bd-tab utp-solid">
                  <Wrap type={1} justifyContent="space-between">
                    <Txt title="运费" color="deep" size={26} />
                    <Txt title={state.data.freightTemplateName} color="deep" size={26} />
                  </Wrap>
                </View>
              </Box1>
            </View>
            {
              // 退货和运费end
            }
            {
              // 规格start
            }
            <View
              className="utp-details-bd-tabs"
              onClick={() => {
                setShow(true);
              }}
            >
              <Box1 type={2}>
                <View className="utp-details-bd-tab utp-solid">
                  <Wrap type={1} justifyContent="space-between">
                    <Txt title="商品规格" color="deep" size={26} />
                    <Wrap>
                      <Txt
                        title={`已选"${skuObj && skuObj.productConfirm}"`}
                        color="deep"
                        size={26}
                      />
                      <Img src={images.right} width={32} />
                    </Wrap>
                  </Wrap>
                </View>
              </Box1>
            </View>

            {
              // 规格end
            }
            {
              // 店铺推荐start
            }
            {!isJifen && state && state.data && (
              <View className="utp-details-bd-stores">
                <Wrap>
                  <Txt title="店铺信息" height={90} color="deep" size={28}></Txt>
                </Wrap>
                <Wrap top>
                  <View className="utp-details-bd-stores-imgs">
                    <Img src={state.data.businessLogoUrl} width={78} />
                  </View>
                  <View className="utp-details-bd-stores-title">
                    <Wrap>
                      <Txt title={state.data.storeName} size={30} color="deep" />
                    </Wrap>
                    <View style={{ height: Taro.pxTransform(50) }}>
                      <Wrap type={1}>
                        <Wrap>
                          <Score num={state.data.star} />
                        </Wrap>
                      </Wrap>
                    </View>
                  </View>
                  <View
                    className="utp-details-bd-stores-btn utp-solid utp-div"
                    onClick={() => {
                      let temp = state.data.params && JSON.parse(state.data.params);
                      if (temp) {
                        Router.goStore(temp.storeType, temp.storeId, temp.businessId);
                      }
                    }}
                  >
                    <Cnt>
                      <Txt title="进店" size={24} color="active" />
                    </Cnt>
                  </View>
                </Wrap>
                <Wrap justifyContent="space-between">
                  {state &&
                    state.data &&
                    state.data.hotProducts &&
                    state.data.hotProducts.map((it) => {
                      if (!it) {
                        return;
                      }
                      return <Goods3 data={it} key={it.id} />;
                    })}
                </Wrap>
              </View>
            )}
            {
              // 店铺推荐end
            }
            {
              // 为你推荐start
            }
            {!isJifen &&
              state &&
              state.data &&
              state.data.recommendProducts &&
              state.data.recommendProducts.length > 0 && (
                <View className="utp-details-bd-likes">
                  <View style={{ paddingLeft: Taro.pxTransform(20) }}>
                    <Wrap>
                      <Txt title="为你推荐" size={28} color="deep" height={82} />
                    </Wrap>
                  </View>
                  <View className="utp-details-swipe utp-details-swipe-1">
                    <Swiper
                      className="utp-details-swipe"
                      indicatorColor="#999"
                      indicatorActiveColor="#333"
                      circular
                      indicatorDots
                      autoplay
                    >
                      {arr().map((item, index) => {
                        if (!item) {
                          return;
                        }
                        return (
                          <SwiperItem key={index}>
                            <View className="utp-details-swipe-item utp-cnt utp-details-swipe-item-8">
                              <Wrap justifyContent="space-between">
                                {item &&
                                  item.map((i) => {
                                    if (!i) {
                                      return;
                                    }
                                    return <Goods3 data={i} key={i.id} />;
                                  })}
                              </Wrap>
                            </View>
                          </SwiperItem>
                        );
                      })}
                    </Swiper>
                  </View>
                </View>
              )}
            {
              // 为你推荐end
            }
            {
              // 商品评价start
            }
            {!isJifen && state && state.data && (
              <View className="utp-details-bd-evaluation">
                <Wrap justifyContent="space-between">
                  <Txt title="商品评价" color="deep" size={28} height={98} />
                  <Wrap>
                    <Txt title="查看评价" size={24} height={98} />
                    <Img src={right} width={44} />
                  </Wrap>
                </Wrap>
                <View
                  style={{
                    display:
                      state &&
                      state.data &&
                      state.data.productCommentList &&
                      state.data.productCommentList.total !== 0
                        ? 'block'
                        : 'none'
                  }}
                >
                  {state &&
                    state.data &&
                    state.data.productCommentList &&
                    state.data.productCommentList.records &&
                    state.data.productCommentList.records.map((i, ii) => {
                      if (!i) {
                        return;
                      }
                      return (
                        <View key={ii} className="utp-details-bd-comentbox">
                          <Wrap>
                            <Wrap>
                              <View className="utp-details-bd-evaluation-icos">
                                <Img src={i.avatarUrl} width={64} />
                              </View>
                              <Txt title={i.nickname} color="deep" size={24} />
                            </Wrap>
                          </Wrap>
                          <View style={{ height: Taro.pxTransform(58) }}>
                            <Wrap type={1} justifyContent="space-between">
                              <Score num={i.score} />
                              <Txt title="2012-12-12 12:00:00" color="low" size={22} />
                            </Wrap>
                          </View>
                          <Wrap>
                            <Txt title={i.comment} size={26} height={45} color="deep" />
                          </Wrap>
                          <View></View>
                          <Wrap justifyContent="space-between">
                            {i &&
                              i.commentUrls &&
                              i.commentUrls.map((j, jj) => {
                                if (!j) {
                                  return;
                                }
                                return <Img src={j} width={200} key={jj} />;
                              })}
                          </Wrap>
                          {/* <View className="utp-details-bd-evaluation-box">
                        <Wrap>
                          <Txt title="店铺回复" size={28} height={56} />
                        </Wrap>
                        <Txt
                          title="评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价评价"
                          size={26}
                          height={45}
                          color="deep"
                        />
                        <View style={{ height: Taro.pxTransform(58) }}>
                          <Wrap type={1}>
                            <View></View>
                            <Txt title="2012-12-12 12:00:00" color="low" size={22} />
                          </Wrap>
                        </View>
                      </View> */}
                        </View>
                      );
                    })}
                </View>
                <View
                  style={{
                    display:
                      state &&
                      state.data &&
                      state.data.productCommentList &&
                      state.data.productCommentList.total !== 0
                        ? 'none'
                        : 'block'
                  }}
                  className="utp-details-empty"
                >
                  <Cnt>
                    <Txt title="还没有用户发表过评价呢~" size={24} color="low" />
                  </Cnt>
                </View>
              </View>
            )}
            {
              // 商品评价end
            }
            {
              //商品详情
            }
            {true && (
              <Wrap Myheight={88} justifyContent="center">
                <Txt title="- 商品详情 -" size={30} />
              </Wrap>
            )}
            {state &&
              state.data &&
              state.data.goodsDescImgUrls &&
              state.data.goodsDescImgUrls.map((k, kk) => {
                if (!k) {
                  return;
                }
                return (
                  <View key={kk} className="utp-details-bd-desimg">
                    <Image
                      style={{
                        width: Taro.pxTransform(750),
                        height: Taro.pxTransform((750 * k.height) / k.width)
                      }}
                      src={k.url}
                    />
                  </View>
                );
              })}
            {/*  同店推荐*/}
            {!isJifen && (
              <Wrap Myheight={88} justifyContent="center">
                <Txt title="- 同店推荐 -" size={30} />
              </Wrap>
            )}
            {!isJifen && recommendState && <Goods2s data={recommendState as IGoods[]} />}

            <View
              className="utp-details-ft"
              onClick={() => {
                setDemo(demo + 1);
              }}
            >
              {!!skuObj && !isJifen && (
                <AddCart
                  isXuni={isXuni}
                  storeId={state && state.data.storeId}
                  businessId={state && state.data.businessId}
                  addCart={() => {
                    setShow(true);
                  }}
                  type={skuObj.stock === 0 ? 'none' : ''}
                  buyGoods={() => {
                    // console.log(555);

                    //Taro.navigateTo({
                    //url: '/pages/cart/index?type=1'
                    //});
                    setShow(true);
                  }}
                />
              )}
              {isJifen && skuObj && skuObj.stock > 0 && (
                <View className="utp-details-jifen utp-cnt" onClick={() => onJifen()}>
                  <Txt
                    title={
                      '立即兑换' +
                      (skuObj
                        ? `（${skuObj.exchangePoint}积分+¥${Glo.formatPrice(
                            skuObj.exchangePrice
                          )}）`
                        : '')
                    }
                    color="white"
                    size={30}
                  ></Txt>
                </View>
              )}
              {isJifen && skuObj && skuObj.stock === 0 && (
                <View className="utp-details-jifen utp-cnt utp-details-jifen-1">
                  <Txt title="已兑完" color="white"></Txt>
                </View>
              )}
            </View>
            {skuState && skuState.code === 0 && skuState.data && (
              <Sku
                isXuni={isXuni}
                onChangeSku={onChangeSku}
                onJifen={onJifen}
                type={skuObj && skuState.data.productSkus[0].skuType}
                show={show}
                goods={state.data}
                data={skuState && skuState.data}
                onClose={() => {
                  setShow(false);
                }}
                closeOverlay={() => {
                  setShow(false);
                }}
              />
            )}
            <Model
              title="积分规则"
              show={showJifen}
              onClose={() => {
                setShowJifen(false);
              }}
              height={1200}
            >
              {skuObj && (
                <ScrollView
                  style={{ marginLeft: Taro.pxTransform(24), marginRight: Taro.pxTransform(24) }}
                >
                  {skuArr.map((item) => {
                    if (!item) {
                      return;
                    }
                    return (
                      <Wrap justifyContent="space-between" key={item}>
                        <Txt title={item} color="deep" />
                      </Wrap>
                    );
                  })}
                </ScrollView>
              )}
            </Model>

            <Model
              title="优惠"
              show={open}
              onClose={() => {
                setOpen(false);
              }}
              height={1200}
            >
              <View className="utp-details-disbox">
                {coupinsState &&
                  coupinsState.data &&
                  coupinsState.data.discountCardDetails &&
                  coupinsState.data.discountCardDetails.length > 0 && (
                    <View>
                      <Wrap Myheight={60} top>
                        <Txt
                          title={
                            coupinsState &&
                            coupinsState.data &&
                            coupinsState.data.discountCardDetails &&
                            coupinsState.data.discountCardDetails.length > 0 &&
                            coupinsState.data.discountCardDetails[0].lable
                          }
                          size={28}
                          color="low"
                        />
                      </Wrap>
                      <Wrap Myheight={60} top>
                        <Txt
                          title={
                            coupinsState &&
                            coupinsState.data.discountCardDetails.length > 0 &&
                            coupinsState.data.discountCardDetails[0].description
                          }
                          size={28}
                          color="deep"
                        />
                      </Wrap>
                      <Wrap Myheight={60} top>
                        <View
                          style={{ width: Taro.pxTransform(710) }}
                          onClick={() => {
                            Router.goHome(coupinsState.data.discountCardDetails[0].cmd);
                          }}
                        >
                          <Wrap justifyContent="space-between">
                            <Txt
                              title={`活动时间：${
                                coupinsState &&
                                coupinsState.data.discountCardDetails.length > 0 &&
                                coupinsState.data.discountCardDetails[0].activityTime
                              }`}
                              size={28}
                              color="deep"
                            />
                            <Img src={images.right} width={28} />
                          </Wrap>
                        </View>
                      </Wrap>
                    </View>
                  )}

                {!!coupinsState &&
                  !!coupinsState.data &&
                  !!coupinsState.data.coupons &&
                  coupinsState.data.coupons.length > 0 && (
                    <View>
                      <View className="utp-details-disbox-title">
                        <Wrap>
                          <Txt title="优惠券" size={28} color="low" />
                        </Wrap>
                      </View>
                      {coupinsState.data.coupons.map((i, ii) => {
                        if (!i) {
                          return;
                        }
                        return (
                          <Card
                            onQuan={async () => {
                              let data = await getQuan({
                                activityId: i.activityId,
                                businessId: state.data.businessId,
                                clientType: 1,
                                couponCode: i.couponCode,
                                storeId: payload.storeId
                              });
                              if (data.code === 0 && update3) {
                                update3();
                              }
                            }}
                            source="details"
                            key={ii}
                            data={i}
                          />
                        );
                      })}
                    </View>
                  )}
              </View>
            </Model>
          </View>
        </View>
      )}
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '商品详情',
  disableScroll: false,
  enablePullDownRefresh: true,
  backgroundColor: '#f5f5f5'
};
// @ts-ignore
const mapStateToProps: PageStateProps = (state) => {
  return {
    isLogin: state.cart.isLogin,
    isWeiXinFalg: state.user.isWeiXinFalg,
    inApp: state.user.inApp
  };
};

// @ts-ignore
const mapDispatchToProps: PageDispatchProps = (dispatch: any) => {
  return {
    getSubmit: (payload) => {
      dispatch({
        type: 'cart/getSubmit',
        payload
      });
    },
    syncCart: () => {
      dispatch({
        type: 'cart/syncCart'
      });
    },
    savePost: (payload) => {
      dispatch({
        type: 'cart/savePost',
        payload
      });
    }
  };
};

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Index);
