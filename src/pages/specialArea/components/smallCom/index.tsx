/* eslint-disable import/first */
/* eslint-disable no-shadow */
import Taro, { useState } from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import Wrap from '@/components/Wrap';
import { InitEndTime } from '../../common/tool';
import './index.scss';

// todo useMemo 倒计时
/* 
  endTime -> 结束时间搓
  endText -> 倒计时结束呈现内容 可 jsx 或者字符串文字
   onEnd  -> 结束后发出通知 事件
*/
// eslint-disable-next-line import/prefer-default-export
export const ViewCountDownCom = (props: any) => {
  const { endTime, endText, onEnd, itemStyle, boxWidth } = props;
  const [timeInfo, setTimeInfo] = useState<any>(InitEndTime(endTime));
  let dayNum = Number(timeInfo.dd * 24);
  return Taro.useMemo(() => {
    if (timeInfo) {
      setTimeout(() => {
        setTimeInfo(InitEndTime(endTime));
      }, 1000);
    }
    if (!timeInfo) {
      onEnd && onEnd('结束拉么么叽');
    }
    return (
      endTime && (
        <View
          className="count-down"
          style={{
            width: boxWidth || Taro.pxTransform(300),
            margin: '0 auto',
            display: 'inline-block'
          }}
        >
          {timeInfo ? (
            <Wrap type={3}>
              {/* {timeInfo.dd != 'NaN' && (
                <View className="count-down-item" style={itemStyle}>
                  {timeInfo.dd * 24}
                </View>
              )} */}
              {timeInfo.hh != 'NaN' && (
                <View className="count-down-item" style={itemStyle}>
                  {dayNum + Number(timeInfo.hh)}
                </View>
              )}
              :
              {timeInfo.mm != 'NaN' && (
                <View className="count-down-item" style={itemStyle}>
                  {timeInfo.mm}
                </View>
              )}
              :
              {timeInfo.ss != 'NaN' && (
                <View className="count-down-item" style={itemStyle}>
                  {timeInfo.ss}
                </View>
              )}
            </Wrap>
          ) : (
            <Text className="count-down-item" style={itemStyle}>
              {endText ? endText : '已结束'}
            </Text>
          )}
        </View>
      )
    );
  }, [timeInfo]);
};
