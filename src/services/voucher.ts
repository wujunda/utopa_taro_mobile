import request from '../utils/request';

export interface IQuan {
  goodsTypeName: string;
  statusDesc: string;
  name: string;
  logo: string;
  placeOrderTime: number;
  orderNo:string;
  goodsNum:number;
  orderAmount:number;
  statusCode:number
}
export interface IVoucher {
  code: number;
  data: {
    orders: IQuan[];
  };
}
// 详情
export const getPay = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.goodsOrder.addGoodsOrder',
    ver: '1.0',
    params: JSON.stringify(params)
  });
};

// 详情
export const getFree = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.goodsOrder.obtainCouponFree',
    ver: '1.0',
    params: JSON.stringify({
      goodsCode: params.goodsCode,
      goodsNum: 1,
      goodsPrice: 0,
      shopGoodsId: params.shopGoodsId
    })
  });
};

// 详情
export const getInfo = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.goodsSaleWay.getGoods',
    ver: '1.0',
    params: JSON.stringify({
      shopGoodsId: params
    })
  });
};

// 详情
export const getInfo1 = (params: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.goodsOrder.getGoodsOrderDetail',
    ver: '1.0',
    params: JSON.stringify({
      isCheckPay: 1,
      orderNo: params
    })
  });
};
// 列表
export const getVoucher = (params: any, pst: any) => {
  // console.log(params);
  return request.post<any>('/app.do', {
    method: 'eshop.order.myOrderNew',
    ver: '1.0',
    params: JSON.stringify({
      serviceStatus: params.split('%%')[1],
      pageSize: pst.per_page,
      pageNo: pst.page
    })
  });
};
