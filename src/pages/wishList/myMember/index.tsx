import Taro from '@tarojs/taro';
import { View, ScrollView } from '@tarojs/components';

import Img from '../../../components/Img';
import Wrap from '../../../components/Wrap';
import Safe from '../../../components/safe';
import Bton from '../../../components/Bton';

import bgCode from '../../../assets/mine/bg_wode.png';
import erWeiMa from '../../../assets/mine/erweima.jpg';

import './index.scss';

interface IProps {}

const Index: Taro.FC<IProps> = ({}) => {
  return (
    <Safe>
      <View className="utp-html">
        <View className="utp-body">
          {/* <Bar /> */}
          <ScrollView
            refresherEnabled
            refresherBackground="#333"
            className="utp-bd utp-bd-page"
            scrollY
          >
            <View
              className="utp-myMember-bgWrap"
              style={{
                background: `url(${bgCode})`
              }}
            ></View>
            <View className="utp-myMember-content">
              <Wrap type={1} flexDirection="column">
                <View>
                  <Img src={erWeiMa} width={500}></Img>
                </View>

                <View className="utp-myMember-content-tel">
                  <Bton type={7} borderRadius={0}>
                    我的会员码：138****2433
                  </Bton>
                </View>
              </Wrap>
            </View>
          </ScrollView>
        </View>
      </View>
    </Safe>
  );
};

Index.defaultProps = {};

export default Index;
